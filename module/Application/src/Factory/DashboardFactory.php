<?php
namespace Application\Factory;

//related controller
use Application\Controller\DashboardController;

//models to load
use Application\Model\CommonMethodsModel;
use Application\Model\MailModel;
use Application\Model\TripModel;
use Application\Model\DashboardModel;
//configuration classes
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class DashboardFactory implements FactoryInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        
        $dbAdapter = $container->get('db_adapter');
        $controllerData['configs'] = $container->get('config');
        $controllerData['models'] = array(
            array('name' => 'CommonMethodsModel', 'obj' => new CommonMethodsModel($dbAdapter)),
            array('name' => 'MailModel', 'obj' => new MailModel($dbAdapter,$controllerData['configs'])),
            array('name' => 'TripModel', 'obj' => new TripModel($dbAdapter,$controllerData['configs'])),
             array('name' => 'DashboardModel', 'obj' => new DashboardModel($dbAdapter,$controllerData['configs']))
        );
        $controllerData['configs'] = $container->get('config');
        return new DashboardController($controllerData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        return $this($container, $requestedName, []);
    }
}