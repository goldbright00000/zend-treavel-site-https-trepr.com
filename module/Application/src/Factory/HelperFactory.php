<?php
namespace Application\Factory;

//related controller
use Application\View\Helper\Common;

//models to load
use Application\Model\CommonMethodsModel;

//configuration classes
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use ZendMvc\Router\RouteMatch;
use Zend\Session\Container;
// use	ZendMvc\Router\RouteMatch::getParams()
class HelperFactory implements FactoryInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
        $helperData['configs'] = $container->get('config');
        $helperData['comSessObj'] = new Container('comSessObj');
        $helperData['models'] = array(
            array('name' => 'CommonMethodsModel', 'obj' => new CommonMethodsModel($dbAdapter))
        );
        return new Common($helperData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        $dbAdapter = $container->get('db_adapter');
        $helperData['configs'] = $container->get('config');
        $helperData['models'] = array(
            array('name' => 'CommonMethodsModel', 'obj' => new CommonMethodsModel($dbAdapter))
        );
        $currentController = $container->get('application')->getMvcEvent()->getRouteMatch()->getParams('controller');
        $currentController = array_pop(explode('\\',$currentController['controller']));
        $helperData['currentController'] = $currentController;
        //return $this($container, $requestedName, []);
        return new Common($helperData);
    }
}