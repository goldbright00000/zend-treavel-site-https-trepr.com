<?php
namespace Application\Factory;

//related controller
use Application\Controller\MyaccountController;

//models to load
use Application\Model\CommonMethodsModel;
use Application\Model\Myaccount;

//configuration classes
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class MyaccountFactory implements FactoryInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
        $controllerData['configs'] = $container->get('config');
        $controllerData['models'] = array(
            array('name' => 'CommonMethodsModel', 'obj' => new CommonMethodsModel($dbAdapter)),
            array('name' => 'Myaccount', 'obj' => new Myaccount($dbAdapter)),
        );
        $controllerData['configs'] = $container->get('config');
        return new MyaccountController($controllerData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        return $this($container, $requestedName, []);
    }
}