<?php
namespace Application\Factory;
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
//related controller
use Application\Controller\UserprofileController;

//models to load
use Application\Model\CommonMethodsModel;
use Application\Model\Currencyrate;
use Application\Model\MailModel;
use Application\Model\TripModel;
use Application\Model\ProfileModel;
//configuration classes
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class UserprofileFactory implements FactoryInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
        $controllerData['configs'] = $container->get('config');
        $controllerData['models'] = array(
            array('name' => 'CommonMethodsModel', 'obj' => new CommonMethodsModel($dbAdapter)),
            array('name' => 'MailModel', 'obj' => new MailModel($dbAdapter,$controllerData['configs'])),
            array('name' => 'TripModel', 'obj' => new TripModel($dbAdapter,$controllerData['configs'])),
             array('name' => 'Currencyrate', 'obj' => new Currencyrate($dbAdapter,$controllerData['configs'])),
             array('name' => 'ProfileModel', 'obj' => new ProfileModel($dbAdapter,$controllerData['configs']))
            
        );
        $controllerData['configs'] = $container->get('config');
        
        return new UserprofileController($controllerData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        return $this($container, $requestedName, []);
    }
}