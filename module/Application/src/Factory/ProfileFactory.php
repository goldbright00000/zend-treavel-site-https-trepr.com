<?php
namespace Application\Factory;


//models to load
use Application\Model\CommonMethodsModel;
use Application\Model\Currencyrate;
use Application\Model\MailModel;
use Application\Model\TripModel;
use Application\Model\ProfileModel;
//configuration classes
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


use Application\Controller\ProfileController;

class ProfileFactory implements FactoryInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
        $controllerData['configs'] = $container->get('config');
        
       
        return new ProfileController($controllerData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        return $this($container, $requestedName, []);
    }
}
?>