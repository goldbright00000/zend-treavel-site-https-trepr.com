<?php
namespace Application\Factory;
use Application\Controller\TravellerController;
use Application\Model\CommonMethodsModel;
use Application\Model\Currencyrate;
use Application\Model\TripModel;
use Application\Model\MailModel;
use Application\Model\ProfileModel;
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
class TravellerFactory implements FactoryInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
          $controllerData['configs'] = $container->get('config');
        $controllerData['models'] = array(
            array('name' => 'CommonMethodsModel', 'obj' => new CommonMethodsModel($dbAdapter)),
            array('name' => 'Currencyrate', 'obj' => new Currencyrate($dbAdapter)),
            array('name' => 'TripModel', 'obj' => new TripModel($dbAdapter)),
			array('name' => 'MailModel', 'obj' => new MailModel($dbAdapter,$controllerData['configs'])),
			array('name' => 'ProfileModel', 'obj' => new ProfileModel($dbAdapter)),
        );
          $controllerData['configs'] = $container->get('config');
        return new TravellerController($controllerData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        return $this($container, $requestedName, []);
    }
}