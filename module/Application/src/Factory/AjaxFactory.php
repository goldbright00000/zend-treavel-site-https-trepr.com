<?php
namespace Application\Factory;
/*error_reporting(E_ALL);
ini_set('display_errors', 1);*/
//related controller
use Application\Controller\AjaxController;

//models to load
use Application\Model\CommonMethodsModel;
use Application\Model\Currencyrate;
use Application\Model\TripModel;
use Application\Model\Myaccount;

//configuration classes
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class AjaxFactory implements FactoryInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
        
        $controllerData['models'] = array(
            array('name' => 'CommonMethodsModel', 'obj' => new CommonMethodsModel($dbAdapter)),
            array('name' => 'Currencyrate', 'obj' => new Currencyrate($dbAdapter)),
            array('name' => 'TripModel', 'obj' => new TripModel($dbAdapter)),
            array('name' => 'Myaccount', 'obj' => new Myaccount($dbAdapter)),
        );
        return new AjaxController($controllerData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        return $this($container, $requestedName, []);
    }
}