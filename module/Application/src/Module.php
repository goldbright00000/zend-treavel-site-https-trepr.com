<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\ModuleManager\Feature\ServiceProviderInterface;
use Application\Model\CommonMethodsModel;
use Zend\Db\Adapter\Adapter;
use Zend\Mvc\MvcEvent;

use Interop\Container\ContainerInterface;
use Zend\Session\Container;

class Module
{
    const VERSION = '3.0.0dev';
    var $container;
    
    public function getConfig(){
        return include __DIR__ . '/../config/module.config.php';
    }
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $this->container = $container;
        //var_dump($this->container); exit;
        //return $sessionManager;
    }
    
    public function onBootstrap(MvcEvent $e) {
        $session = new Container('comSessObj');
        $viewModel = $e->getApplication()->getMvcEvent()->getViewModel();
        $viewModel->comSessObj = $session;
    }
    
    public function getServiceConfig() {
        return array(
            'factories' => array(
                'Application\Model\CommonMethodsModel' => function($sm) {
                    $dbAdapter = $sm->get('Zend\Db\Adapter\Adapter');
                    $table = new CommonMethodsModel($dbAdapter);
                    return $table;
                },
                'db_adapter' =>  function($sm) {
                    $config = $sm->get('Configuration');
                    $dbAdapter = new \Zend\Db\Adapter\Adapter($config['db']);
                    return $dbAdapter;
                }
            )
        );
    }
	public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        if(isset($this->sessionObj->userid) && $this->sessionObj->userid != ''){
			$isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			if(!$isUserLoggedIn){
				return $this->redirect()->toUrl('http://trepr.com/logout');
			}
		}

        return parent::onDispatch($e);
    }
}