<?php namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Validator\File\Size;
use Zend\Mvc\MvcEvent;
use Application\Model\Currencyrate;
use Zend\Mail;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class TravellerController extends AbstractActionController
{

    var $sessionObj;
    var $TripPlugin;
	var $stripeConfig;

    public function __construct($data = false)
    {
        /* Loading models from factory
         * Call the model object using it's class name
        */
        if ($data['models'] && is_array($data['models'])) {
            foreach ($data['models'] as $model) {
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
		$this->stripeConfig = $data['configs']['stripe_keys'];
        $this->basePath = $this->siteConfigs['mail_url'];
        $this->workCategory = array(
            '1' => 'Obtain, Travel & Deliver',
            '2' => 'Arrange, Assist & Accomplish',
            '3' => 'Personal Services',
            '4' => 'Local Travel Services',
            '5' => 'Auto & Device Services',
            '6' => 'Home & Surroundings',
            '7' => 'Entertainment & Leisure'
        );
    }

    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        if (isset($this->sessionObj->userid) && $this->sessionObj->userid != '') {
            $isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
            if (!$isUserLoggedIn) {
                $this->redirect()->toRoute('logout');
            }
        }
        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->sessionObj->userrole = 'traveller';
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $viewArray['current_upcoming_trips'] = $this->TripPlugin->getTrips('current_upcoming', $this->sessionObj->userrole, $this->sessionObj->userid);
        $viewArray['completed_trips'] = $this->TripPlugin->getTrips('completed', $this->sessionObj->userrole, $this->sessionObj->userid);
        $viewArray['cancelled_trips'] = $this->TripPlugin->getTrips('cancelled', $this->sessionObj->userrole, $this->sessionObj->userid);
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
		
		$calEvent = array();
		if(is_array($viewArray['current_upcoming_trips'])){
			foreach($viewArray['current_upcoming_trips'] as $cuTrip){
				$cuTrip['departure_date'] = explode(' ',$cuTrip['departure_date']);
				$cuTrip['arrival_date'] = explode(' ',$cuTrip['arrival_date']);
				$date_from = strtotime($cuTrip['departure_date'][0]);
				$date_to = strtotime($cuTrip['arrival_date'][0]);
				for($i=$date_from; $i<=$date_to; $i+=86400) {  
					$newDepDate = date("Y-m-d", $i);
					@$calEvent[] = array(
					'eventName'=> $cuTrip['origin_location_city'].' ('.$cuTrip['origin_location_code'].') - '.$cuTrip['destination_location_city'].' ('.$cuTrip['destination_location_code'].')',
					'calendar'=>$cuTrip['service'],
					'color'=>($cuTrip['service'] == 'People'?'pink':($cuTrip['service'] == 'Package'?'skyblue':'blue')),
					'date'=>date('d',strtotime($newDepDate))."/".date('m',strtotime($newDepDate))."/".date('Y',strtotime($newDepDate))
					);
				
					

				}
			}
		}
		$viewArray['cal_events'] = $calEvent;

        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Traveller',
                'title' => 'Traveller',
                'active' => false,
                'redirect' => '/traveller'
            ),
            array(
                'label' => 'My Travel Plans',
                'title' => 'My Travel Plans',
                'active' => true,
                'redirect' => false //'/traveller'
            )
        );
        $viewArray['comSessObj'] = $this->sessionObj;
        $this->layout()->pageController = 'traveller';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function addNewAddressAction()
    {
        $user_id = $this->sessionObj->userid;
        $_POST['user'] = $user_id;
        $data = $_POST;
        $checkIfExistTrip = $this->TripModel->addAddressNew($data);
        echo json_encode($checkIfExistTrip);
        exit;
    }

    public function addPeopleServiceAction()
    { 
       
        if(isset($_REQUEST['betrip']) && $_REQUEST['betrip'] =='yes')
        {
            
            $betrip = $_REQUEST['betrip'];
        }else {
            $betrip='';
        }
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->sessionObj->userrole = 'traveller';
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $viewArray['disabledProp'] = '';
        $viewArray['nonEditableProp'] = '';
        if ($_POST) {
            $_POST['type_of_service'] = 'people';
			
            extract($_POST);
			$user_mailer_datails = $this->CommonMethodsModel->getUser($this->sessionObj->userid);

			$origin_location = $_POST['origin_location'];
		    $destination_location= $_POST['destination_location'];
		    $departure_date = $_POST['departure_date'];
			$departure_time =$_POST['departure_time'];
			$firstname = $user_mailer_datails['first_name'];
			$create_date = date("Y-m-d H:i:s");
			$to = $user_mailer_datails['email_address'];
            $exitsTrip = false;
            if (isset($travel_plan_type) && $travel_plan_type == 'existing_plan') {
                $Exist_trip_ID = $this->CommonMethodsModel->cleanQuery(($travel_plan));
                $checkIfExistTrip = $this->TripModel->getTravellerPeopleByTrip($Exist_trip_ID);
                if ($checkIfExistTrip) {
                    $exitsTrip = true;
                    $editTripUrl = '/traveller/add-people-service/' . $this->encrypt($checkIfExistTrip['trip']) . '#service';
                    echo json_encode(array('result' => 'exist', 'serviceUrl' => $editTripUrl, 'msg' => "Already a people service trip listing exists for this trip. Would you like to edit?"));
                    exit;
                }
            }
            $user_location = $this->TripPlugin->addTripOrginLocation();
            $trip_ID = $this->TripPlugin->addTripDetail($user_location);
            $verifiedIdentityStatus = $this->TripModel->checkverifiedIdentityStatus($this->sessionObj->userid);
            if ($verifiedIdentityStatus == true) {
                $ticketStatus = $this->TripModel->checkTicketStatus($trip_ID);
                if ($ticketStatus == true) {
                    $this->TripModel->setTripStatus($trip_ID, 3);
                } else {
                    $this->TripModel->setTripStatus($trip_ID, 4);
                }
            } else {
                $identityStatus = $this->TripModel->checkIdentityStatus($this->sessionObj->userid);
                if ($identityStatus == true) {
                    $this->TripModel->setTripStatus($trip_ID, 2);
                } else {
                    $this->TripModel->setTripStatus($trip_ID, 0);
                }
            }
            $this->TripPlugin->addTravellerPeopleDetail($trip_ID);
            if (isset($gotourl) && !empty($gotourl)) {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array
                (
                    'user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has uploaded ticket for people service trip listing. Please take a decision at the earliest.",
                    'create_date' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToAdminNotifications($hugedata);
                
                
                 if ($ticketOption == "uploadTicket") {
                    $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_upload',
                        'notification_subject' => 'Ticket Upload',
                        'notification_message' => "Thanks for creating people service trip listing. We will review the uploaded ticket and approve at the earliest",
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                     $this->TripModel->addToNotifications($hugedata1);
                 }
                if ($ticketOption == "emailTicket") {
                    $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_email',
                        'notification_subject' => 'Ticket Email',
                        'notification_message' => "Thanks for creating people service trip listing. We see you haven't emailed the ticket for approval. Please email the ticket with subject line " . $_POST['tripIdNumber'] . "",
                        'notification_added' => date("Y-m-d H:i:s")
                    );

                    $this->TripModel->addToNotifications($hugedata1);
                }
                $requestURL = $gotourl . '&traveller_people_request_id=' . $this->encrypt($trip_ID);
				
				try

                {

                    $this->sendmail = $this->MailModel->sendCreateListEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $to);

                }

                catch (\Exception $e)

                {

                    error_log($e);

                }
				
				
                echo json_encode(array('result' => 'success', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => '', 'requestURL' => $requestURL, 'msg' => " People service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
            } else if (isset($trip_type) && $trip_type == '2') {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array('user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has uploaded ticket for people service trip listing. Please take a decision at the earliest.",
                    'create_date' => date("Y-m-d H:i:s")

                );
                $this->TripModel->addToAdminNotifications($hugedata);
                if ($ticketOption == "emailTicket") {
                    $hugedata1 = array('user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_email',
                        'notification_subject' => 'Ticket Email',
                        'notification_message' => "Thanks for creating people service trip listing. We see you haven't emailed the ticket for approval. Thus, please email the ticket with subject line " . $_POST['tripIdNumber'] . "",
                        'notification_added' => date("Y-m-d H:i:s")
                    );

                    $this->TripModel->addToNotifications($hugedata1);
                }
				try
                {

						$this->sendmail = $this->MailModel->sendCreateListEmail($firstname,$origin_location,$destination_location,$departure_date,$create_date,$to);

                }

                catch (\Exception $e)
                {

                    error_log($e);

                }
				
                echo json_encode(array('result' => 'success', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => '', 'requestURL' => '', 'trip_ID' => $this->encrypt($trip_ID), 'msg' => "People service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
            } else {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array('user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has uploaded ticket for people service trip listing. Please take a decision at the earliest.",
                    'create_date' => date("Y-m-d H:i:s")

                );
                $this->TripModel->addToAdminNotifications($hugedata);
                
                
                 if ($ticketOption == "uploadTicket") {
                    $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_upload',
                        'notification_subject' => 'Ticket Upload',
                        'notification_message' => "Thanks for creating people service trip listing. We will review the uploaded ticket and approve at the earliest.",
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                     $this->TripModel->addToNotifications($hugedata1);
                 }
                 
                if ($ticketOption == "emailTicket") {
                    $hugedata11 = array('user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_email',
                        'notification_subject' => 'Ticket Email',
                        'notification_message' => "Thanks for creating people service trip listing. We see you haven't emailed the ticket for approval. Thus, please email the ticket with subject line " . $_POST['tripIdNumber'] . "",
                        'notification_added' => date("Y-m-d H:i:s")
                    );

                    $this->TripModel->addToNotifications($hugedata11);
                }
               /* echo json_encode(array('result' => 'success', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => $this->encrypt($trip_ID), 'requestURL' => '', 'trip_ID' => '', 'msg' => " People service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;*/
                
                    
                
                 if(isset($_REQUEST['return_trip'])  && $_REQUEST['return_trip'] == 'round_trip'){
                   // echo json_encode(array('result' => 'success','return_trip' => 'yes', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => '', 'requestURL' => '', 'trip_ID' => $this->encrypt($trip_ID), 'msg' => "Package service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
				try

                {

                    $this->sendmail = $this->MailModel->sendCreateListEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $to);

                }

                catch (\Exception $e)

                {

                    error_log($e);

                }

				   
                    echo json_encode(array('result' => 'success', 'return_trip' => 'yes','TID' => $this->encrypt($trip_ID), 'seacrhAction' => $this->encrypt($trip_ID), 'requestURL' => '', 'trip_ID' => '', 'msg' => " People service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
                
                    
                    
                    
                }else {
					
				try

                {

                    $this->sendmail = $this->MailModel->sendCreateListEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $to);

                }

                catch (\Exception $e)

                {

                    error_log($e);

                }
                    echo json_encode(array('result' => 'success', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => $this->encrypt($trip_ID), 'requestURL' => '', 'trip_ID' => '', 'msg' => " People service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
                }
                
            }
        } else {

            $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
           
            $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
            $viewArray['travel_plans'] = $this->TripModel->getTravelTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid, false);
            $ID = $this->params('ID');
            $TYPE = $this->params('TYPE');
            $GOTOT_URL = isset($_GET['g']) ? urldecode($_GET['g']) : '';
            $viewArray['selectedFlightData'] = '';
            $viewArray['gotourl'] = $GOTOT_URL;
            if ($ID) {
                $tripId = $this->decrypt($ID);
                $tripDetails = $this->TripModel->getTrip($tripId);
                $viewArray['tripId'] = $tripId;
                $trip = isset($tripDetails) ? $tripDetails : false;
                $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid, 'no', $tripDetails['user_location']);
 
                $viewArray['trip_type'] = $TYPE != ''?'round_trip':'one_way';
                $trip_traveller_details = $this->TripModel->getTripPeople($tripId);
                $trip_traveller = isset($trip_traveller_details) ? $trip_traveller_details : false;
                $viewArray['airport_details'] = $this->TripModel->getFlightsDetails($tripId);
                $origin_location = $this->TripModel->getAirPort($tripDetails['origin_location']);
                $destination_location = $this->TripModel->getAirPort($tripDetails['destination_location']);
                $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $origin_location_country_code = isset($origin_location['country_code']) ? $origin_location['country_code'] : '';
                $destination_location_country_code = isset($destination_location['country_code']) ? $destination_location['country_code'] : '';
                $viewArray['origin_location_country_code'] = $origin_location_country_code;
                $viewArray['destination_location_country_code'] = $destination_location_country_code;
                $viewArray['origin_location'] = $origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;
                $viewArray['destination_location'] = $destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;
                $viewArray['origin_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';
                $viewArray['destination_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';
                $viewArray['selectedFlightData'] = !empty($viewArray['airport_details']) ? htmlspecialchars(json_encode($viewArray['airport_details'])) : '';
                $viewArray['TYPE'] = $TYPE;
                $viewArray['editAddr'] = true;
                $viewArray['disabledProp'] = 'readonly="readonly"';
                $viewArray['nonEditableProp'] = 'readonly="readonly"';
                $editAddOptions = '<option value="' . $origin_location_country_code . '">' . $origin_location_country . '</option>';
                if ($origin_location_country_code != $destination_location_country_code)
                    $editAddOptions .= '<option value="' . $destination_location_country_code . '">' . $destination_location_country . '</option>';
                if (isset($TYPE) && !empty($TYPE)) {
                    $viewArray['origin_location'] = '('.$destination_location['code'].') '.$destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;;
                    $viewArray['destination_location'] = '('.$origin_location['code'].') '.$origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;;
                    $viewArray['origin_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';;
                    $viewArray['destination_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';;
                    $trip['origin_location'] = $tripDetails['destination_location'];
                    $trip['destination_location'] = $tripDetails['origin_location'];
                    $viewArray['selectedFlightData'] = '';
                    $trip['user_location'] = 0;
                    $viewArray['disabledProp'] = 'readonly="readonly"';
                    $viewArray['nonEditableProp'] = '';
                    $trip_traveller = '';
                    $viewArray['airport_details'] = '';
                    $editAddOptions = '<option value="' . $destination_location_country_code . '">' . $destination_location_country . '</option>';
                    if ($origin_location_country_code != $destination_location_country_code)
                        $editAddOptions .= '<option value="' . $origin_location_country_code . '">' . $origin_location_country . '</option>';
                    unset($tripDetails['trip_id_number']);
                }
                $editAddOptions .= '<option value="more" class="more_sel_text">More</option>';
                $viewArray['editAddOptions'] = $editAddOptions;
                $viewArray['trip'] = $trip;
                $viewArray['trip_traveller'] = $trip_traveller;
            }
        }
        $viewArray['currency'] = $this->CommonMethodsModel->getCurrency();
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Traveller',
                'title' => 'Traveller',
                'active' => false,
                'redirect' => '/traveller'
            ),
            array(
                'label' => 'Add Travel Plans',
                'title' => 'Add Travel Plans',
                'active' => false,
                'redirect' => '/traveller/add-people-service'
            ),
            array(
                'label' => 'People Service',
                'title' => 'People Service',
                'active' => true,
                'redirect' => false //'/traveller/add-people-service'
            )
        );
        
        $viewArray['betrip'] = $betrip;
        $viewArray['comSessObj'] = $this->sessionObj;
        $this->layout()->pageController = 'traveller';
        $this->layout()->pageFunction = 'addPeopleService';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function encrypt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        /*pls set your unique hashing key*/
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        /* hash*/
        $key = hash('sha256', $secret_key);
        /* iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning*/
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        /*do the encyption given text/string/number*/
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }

    public function decrypt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        /*pls set your unique hashing key*/
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        /* hash*/
        $key = hash('sha256', $secret_key);
        /* iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning*/
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        /*decrypt the given text/string/number*/
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    public function detailAction()
    { 
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $this->TripPlugin = $this->plugin('TripPlugin');
        /* $ID = $this->_helper->Common->decrypt($this->params('ID')); */
        $ID = $this->params('ID');
        $service = isset($_GET['service']) ? $_GET['service'] : '';
        $trip = $this->TripModel->getTrip($ID);
        $seeker_people_request_id = isset($_GET['seeker_people_request_id']) ? $_GET['seeker_people_request_id'] : '';
        $seeker_package_request_id = isset($_GET['seeker_package_request_id']) ? $_GET['seeker_package_request_id'] : '';
        $seeker_project_request_id = isset($_GET['seeker_project_request_id']) ? $_GET['seeker_project_request_id'] : '';
        
		$viewArray['tripid'] = $ID;
        $viewArray['trip'] = $trip;
		$viewArray['seeker_fee'] = $trip['total_cost'];

        $viewArray['seeker_people_request_id'] = $seeker_people_request_id;
        $viewArray['seeker_package_request_id'] = $seeker_package_request_id;
        $viewArray['seeker_project_request_id'] = $seeker_project_request_id;
        $viewArray['trip_user'] = $this->CommonMethodsModel->getUser($trip['user']);
        $viewArray['userTrustVerification'] = $this->CommonMethodsModel->getUserTrustVerifyTypes($trip['user']);
        $viewArray['looking_for'] = 'traveller';
		
		$viewArray['trip_user']['languages'] = array();
		$lngIds = explode(',',$viewArray['trip_user']['language']);
		foreach($lngIds as $lngId){
			$viewArray['trip_user']['languages'][] = $this->CommonMethodsModel->getLanguagesName($lngId);
		}
		$viewArray['trip_user']['language'] = implode(',',$viewArray['trip_user']['languages']);
		
        $dob = strtotime($viewArray['trip_user']['dob']);
        $todaydate = strtotime(date('Y-m-d'));
        $viewArray['age'] = $this->CommonPlugin->calculateAge($dob, $todaydate);
        $primary = 'yes';
        $user_verify_docs = $this->TripModel->checkVerificationDocument($this->sessionObj->userid);
        $documentThere = $approvedDocs = false;
        if (!empty($user_verify_docs)) {
            $documentThere = true;
            foreach ($user_verify_docs as $verifydoc_row) {
                if ($verifydoc_row['approved'] == 1) {
                    $approvedDocs = true;
                }
            }
        }
        $user_cards_details = $this->TripModel->getcarddetails($this->sessionObj->userid);
        $viewArray['user_location'] = $this->CommonMethodsModel->getUserLocationByAddrId($trip['user_location'], $trip['user'], $primary);
        $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
        $viewArray['trip_seeker'] = $this->TripModel->getPeopleDetailByTripID($ID);
        $viewArray['trip_seeker_packages'] = $this->TripModel->getPackageDetailByTripID($ID);
        $viewArray['trip_seeker_projects'] = $this->TripModel->getTravellerProjectByTrip($ID);
        $trip_seeker = $this->TripPlugin->getTripDetails($ID, 'traveller', false, $service);
        $project_tasks = $viewArray['trip_seeker_projects']['task'];
        $project_tasks = explode(',', $project_tasks);
        if (count($project_tasks) > 0) {
            foreach ($project_tasks as $project_task) {
                $task = $this->TripModel->getTripProjectTask($project_task);
                $arr_tasks[] = $task['name'];
            }
        } else {
            $task = $this->TripModel->getTripProjectTask($project_task);
            $arr_tasks[] = $task['name'];
        }
        $viewArray['trip_travaller_projects_tasks'] = $arr_tasks;
        $viewArray['project_tasks_categories'] = $this->TripModel->getProjectTasksCategory($project_task);
		$viewArray['reviews'] = $this->TripModel->getTripreviewall($this->sessionObj->userid,true);
		$viewArray['avg_rating'] = 0;
		if($viewArray['reviews']){
			foreach($viewArray['reviews'] as $rKey=>$rev){
				$flightdetails = $this->TripModel->getFlightsDetails($rev['trip_id']);
				$viewArray['reviews'][$rKey]['airport_details'] = array('orgin_city'=>'','destination_city'=>'');
				if(is_array($flightdetails) && count($flightdetails) > 0){
					$viewArray['reviews'][$rKey]['airport_details'] = $flightdetails[0];
				}
				$viewArray['avg_rating'] += $viewArray['reviews'][$rKey]['rating'];
			}
			$viewArray['avg_rating'] = number_format($viewArray['avg_rating'] / count($viewArray['reviews']),0);
		}
        $traveller_upcoming_trips = $this->TripPlugin->getTrips('upcoming', 'seeker', $this->sessionObj->userid);

        $viewArray['seeker_upcoming_trips'] = $traveller_upcoming_trips;
        if (count($traveller_upcoming_trips) > 0) {

            foreach ($traveller_upcoming_trips as $traveller_trip) {
                //print_r($traveller_trip);
                if ($traveller_trip['trip_people']['ID']) {
                    if (($traveller_trip['origin_location_country'] == $trip_seeker['origin_location_country']) && ($traveller_trip['destination_location_country'] == $trip_seeker['destination_location_country']))
                        $viewArray['seeker_exiting_people_trip'] = 'yes';
                }
                if ($traveller_trip['trip_package']['ID']) {
                    if (($traveller_trip['origin_location_country'] == $trip_seeker['origin_location_country']) && ($traveller_trip['destination_location_country'] == $trip_seeker['destination_location_country']))
                        $viewArray['seeker_exiting_package_trip'] = 'yes';
                }
                if ($traveller_trip['trip_project']['ID']) {
                    //echo "check";
                    if (($traveller_trip['origin_location_country'] == $trip_seeker['origin_location_country']) && ($traveller_trip['destination_location_country'] == $trip_seeker['destination_location_country']))
                        $viewArray['seeker_exiting_project_trip'] = 'yes';
                }
            }
        }
		
		if(isset($_REQUEST['from']) && $_REQUEST['from'] == 'inquired'){
			$this->TripModel->inactiveTripInquired($_REQUEST['tID'],$_REQUEST['sID'],'seeker');
		}
		
		$viewArray['reqInfo'] = array();
		if(isset($_REQUEST['seeker_req_id']) && $_REQUEST['seeker_req_id'] != ''){
			$viewArray['reqInfo'] = $this->TripModel->getSeekerRequestInfo($_REQUEST['seeker_req_id'],strtolower($_REQUEST['service']));
		} else if(isset($_REQUEST['traveller_req_id']) && $_REQUEST['traveller_req_id'] != ''){
			$viewArray['reqInfo'] = $this->TripModel->getTravellerRequestInfo($_REQUEST['traveller_req_id'],strtolower($_REQUEST['service']));
		}
		
        $viewArray['project_cats'] = $this->TripModel->getPackageTasksCategory();
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Seeker',
                'title' => 'Seeker',
                'active' => false,
                'redirect' => false
            ),
            array(
                'label' => 'My Travel Plans',
                'title' => 'My Travel Plans',
                'active' => true,
                'redirect' => '/seeker'
            )
        );
        $viewArray['comSessObj'] = $this->sessionObj;
		$viewArray['stripeConfig'] = $this->stripeConfig;
        $viewArray['trip'] = $trip_seeker;       
        $viewArray['user'] = $this->sessionObj->user;
        $viewArray['trip_id'] = $ID;
        $viewArray['documentThere'] = $documentThere;
        $viewArray['approvedDocs'] = $approvedDocs;
        $viewArray['workCategory'] = $this->workCategory;
        $viewArray['user_cards_details'] = $user_cards_details;
        $viewArray['project_tasks_categories'] = $this->TripModel->getProjectTasksCategory();
        $viewArray['project_tasks_categories_type'] = $this->TripModel->getProjectTasksCategorytype();
		$viewArray['reward_details'] = $this->CommonMethodsModel->getRewards($this->sessionObj->userid);
        // echo '<pre>';print_r($viewArray);exit();
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function receiptAction()
    {
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $this->TripPlugin = $this->plugin('TripPlugin');
        $ID = $this->decrypt($this->params('ID'));

        $trip_pay_row = $this->TripModel->getSeekerTripPaymentById($ID);
        $trip = $this->TripModel->getTrip($trip_pay_row['pay_trip_id']);
        $service = isset($_GET['service']) ? $_GET['service'] : '';
        $viewArray['trip'] = $trip;
        $viewArray['trip_user'] = $this->CommonMethodsModel->getUser($trip['user']);
        $dob = strtotime($viewArray['trip_user']['dob']);
        $todaydate = strtotime(date('Y-m-d'));
        $viewArray['age'] = $this->CommonPlugin->calculateAge($dob, $todaydate);
        $primary = 'yes';
        $user_verify_docs = $this->TripModel->checkVerificationDocument($this->sessionObj->userid);
        $documentThere = $approvedDocs = false;
        if (!empty($user_verify_docs)) {
            $documentThere = true;
            foreach ($user_verify_docs as $verifydoc_row) {
                if ($verifydoc_row['approved'] == 1) {
                    $approvedDocs = true;
                }
            }
        }
        $user_cards_details = $this->TripModel->getcarddetails($this->sessionObj->userid);
        $viewArray['user_location'] = $this->CommonMethodsModel->getUserLocationByAddrId(FALSE, $trip['user'], $primary);

        $viewArray['trip_seeker'] = $this->TripModel->getPeopleDetailByTripID($trip_pay_row['pay_trip_id']);
        $viewArray['trip_seeker_packages'] = $this->TripModel->getPackageDetailByTripID($trip_pay_row['pay_trip_id']);
        $viewArray['trip_seeker_projects'] = $this->TripModel->getTravellerProjectByID($trip_pay_row['pay_trip_id']);
        $trip_seeker = $this->TripPlugin->getTripDetails($trip_pay_row['pay_trip_id'], 'traveller', false, $service);
        //$trip_seeker      = $this->TripPlugin->getTripDetails($trip_pay_row['pay_trip_id'], 'seeker', false, $service);
        $project_tasks = $viewArray['trip_seeker_projects']['task'];
        $project_tasks = explode(',', $project_tasks);
        if (count($project_tasks) > 0) {
            foreach ($project_tasks as $project_task) {
                $task = $this->TripModel->getTripProjectTask($project_task);
                $arr_tasks[] = $task['name'];
            }
        } else {
            $task = $this->TripModel->getTripProjectTask($project_task);
            $arr_tasks[] = $task['name'];
        }
        $viewArray['trip_travaller_projects_tasks'] = $arr_tasks;
        $viewArray['project_tasks_categories'] = $this->TripModel->getProjectTasksCategory($project_task);
        $traveller_upcoming_trips = $this->TripPlugin->getTrips('upcoming', 'seeker', $this->sessionObj->userid);

        $viewArray['seeker_upcoming_trips'] = $traveller_upcoming_trips;

        if (count($traveller_upcoming_trips) > 0) {
            foreach ($traveller_upcoming_trips as $traveller_trip) {
                if ($traveller_trip['trip_people']['ID']) {
                    if (($traveller_trip['origin_location'] == $trip['origin_location']) && ($traveller_trip['destination_location'] == $trip['destination_location']))
                        $viewArray['seeker_exiting_people_trip'] = 'yes';
                }
                if ($traveller_trip['trip_package']['ID']) {
                    if (($traveller_trip['origin_location'] == $trip['origin_location']) && ($traveller_trip['destination_location'] == $trip['destination_location']))
                        $viewArray['seeker_exiting_package_trip'] = 'yes';
                }
                if ($traveller_trip['trip_project']['ID']) {
                    if (($traveller_trip['origin_location'] == $trip['origin_location']) && ($traveller_trip['destination_location'] == $trip['destination_location']))
                        $viewArray['seeker_exiting_project_trip'] = 'yes';
                }
            }
        }
        $viewArray['project_cats'] = $this->TripModel->getPackageTasksCategory();
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Seeker',
                'title' => 'Seeker',
                'active' => false,
                'redirect' => false
            ),
            array(
                'label' => 'My Travel Plans',
                'title' => 'My Travel Plans',
                'active' => true,
                'redirect' => '/traveller'
            )
        );

        $viewArray['comSessObj'] = $this->sessionObj;
        $viewArray['trip'] = $trip_seeker;
        $viewArray['user'] = $this->sessionObj->user;
        $viewArray['trip_id'] = $ID;
        $viewArray['documentThere'] = $documentThere;
        $viewArray['approvedDocs'] = $approvedDocs;
        $viewArray['workCategory'] = $this->workCategory;
        $viewArray['user_cards_details'] = $user_cards_details;
        $viewArray['project_tasks_categories'] = $this->TripModel->getProjectTasksCategory();
        $viewArray['project_tasks_categories_type'] = $this->TripModel->getProjectTasksCategorytype();
        $viewArray['trip_pay_row'] = $trip_pay_row;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function addPackageServiceAction()
    {
        
          if(isset($_REQUEST['betrip']) && $_REQUEST['betrip'] =='yes')
        {
            
            $betrip = $_REQUEST['betrip'];
        }else {
            $betrip='';
        }
        
        
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        } 
        $this->sessionObj->userrole = 'traveller';
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $viewArray['disabledProp'] = '';
        $viewArray['nonEditableProp'] = '';
        if ($_POST) {
			$_POST['type_of_service'] = 'package';
            extract($_POST);
			
			$user_mailer_datails = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			$origin_location = $_POST['origin_location'];
		    $destination_location= $_POST['destination_location'];
		    $departure_date = $_POST['departure_date'];
			$departure_time =$_POST['departure_time'];
			$firstname = $user_mailer_datails['first_name'];
			$create_date = date("Y-m-d H:i:s");
			$to = $user_mailer_datails['email_address'];
            $exitsTrip = false;
            if (isset($travel_plan_type) && $travel_plan_type == 'existing_plan') {
                $Exist_trip_ID = $this->CommonMethodsModel->cleanQuery(($travel_plan));
                $checkIfExistTrip = $this->TripModel->getTravellerPackageByTrip($Exist_trip_ID);
                if ($checkIfExistTrip) {
                    $exitsTrip = true;
                    $editTripUrl = '/traveller/add-package-service/' . $this->encrypt($checkIfExistTrip['trip']) . '#service';
                    echo json_encode(array('result' => 'exist', 'serviceUrl' => $editTripUrl, 'msg' => "Already a package service trip listing exists for this trip. Would you like to edit?"));
                    exit;
                }
            }
            $user_location = $this->TripPlugin->addTripOrginLocation();
            $trip_ID = $this->TripPlugin->addTripDetail($user_location);
            $verifiedIdentityStatus = $this->TripModel->checkverifiedIdentityStatus($this->sessionObj->userid);
            if ($verifiedIdentityStatus == true) {
                $ticketStatus = $this->TripModel->checkTicketStatus($trip_ID);
                if ($ticketStatus == true) {
                    $this->TripModel->setTripStatus($trip_ID, 3);
                } else {
                    $this->TripModel->setTripStatus($trip_ID, 4);
                }
            } else {
                $identityStatus = $this->TripModel->checkIdentityStatus($this->sessionObj->userid);
                if ($identityStatus == true) {
                    $this->TripModel->setTripStatus($trip_ID, 2);
                } else {
                    $this->TripModel->setTripStatus($trip_ID, 0);
                }
            }
            $this->TripPlugin->addTravellerPackageDetail($trip_ID);
            if (isset($gotourl) && !empty($gotourl)) {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array('user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has uploaded ticket for package service trip listing. Please take a decision at the earliest.",
                    'create_date' => date("Y-m-d H:i:s")

                );
                $this->TripModel->addToAdminNotifications($hugedata);
                
                 if ($ticketOption == "uploadTicket") {
                    $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_upload',
                        'notification_subject' => 'Ticket Upload',
                        'notification_message' => "Thanks for creating package service trip listing. We will review the uploaded ticket and approve at the earliest.",
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                     $this->TripModel->addToNotifications($hugedata1);
                 }
                if ($_POST['ticketOption'] == "emailTicket") {
                    $hugedata1 = array('user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_email',
                        'notification_subject' => 'Ticket Email',
                        'notification_message' => "Thanks for creating package service trip listing. We see you haven't emailed the ticket for approval. Please email the ticket with subject line " . $_POST['tripIdNumber'] . "",
                        'notification_added' => date("Y-m-d H:i:s")
                    );

                    $this->TripModel->addToNotifications($hugedata1);
                }
                
				try

                {

                    $this->sendmail = $this->MailModel->sendCreateListEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $to);

                }

                catch (\Exception $e)

                {

                    error_log($e);

                }
				$requestURL = $gotourl . '&traveller_package_request_id=' . $this->encrypt($trip_ID);
		        echo json_encode(array('result' => 'success', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => '', 'requestURL' => $requestURL, 'msg' => " Package service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
            } else if (isset($trip_type) && $trip_type == '2') {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array('user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has uploaded ticket for package service trip listing. Please take a decision at the earliest.",
                    'create_date' => date("Y-m-d H:i:s")

                );
                $this->TripModel->addToAdminNotifications($hugedata);
                
                
                 if ($ticketOption == "uploadTicket") {
                    $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_upload',
                        'notification_subject' => 'Ticket Upload',
                        'notification_message' => "Thanks for creating package service trip listing. We will review the uploaded ticket and approve at the earliest.",
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                     $this->TripModel->addToNotifications($hugedata1);
                 }
                if ($_POST['ticketOption'] == "emailTicket") {
                    $hugedata1 = array('user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_email',
                        'notification_subject' => 'Ticket Email',
                        'notification_message' => "Thanks for creating package service trip listing. We see you haven't emailed the ticket for approval. Please email the ticket with subject line " . $_POST['tripIdNumber'] . "",
                        'notification_added' => date("Y-m-d H:i:s")
                    );

                    $this->TripModel->addToNotifications($hugedata1);
                }
				
				
                if(isset($_REQUEST['return_trip'])  && $_REQUEST['return_trip'] =='round_trip'){
					
				try

                {

                    $this->sendmail = $this->MailModel->sendCreateListEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $to);

                }

                catch (\Exception $e)

                {

                    error_log($e);

                }
                        echo json_encode(array('result' => 'success','return_trip' => 'yes', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => $this->encrypt($trip_ID), 'requestURL' => '', 'trip_ID' => '', 'msg' => "Package service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                        exit;
                }else {
                    
					
				try

                {

                    $this->sendmail = $this->MailModel->sendCreateListEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $to);

                }

                catch (\Exception $e)

                {

                    error_log($e);

                }
                        echo json_encode(array('result' => 'success', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => $this->encrypt($trip_ID), 'requestURL' => '', 'trip_ID' => '', 'msg' => "Package service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                        exit; 
                }
            } else {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array('user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has uploaded ticket for package service trip listing. Please take a decision at the earliest.",
                    'create_date' => date("Y-m-d H:i:s")

                );
                $this->TripModel->addToAdminNotifications($hugedata);
                if ($_POST['ticketOption'] == "emailTicket") {
                    $hugedata1 = array('user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_email',
                        'notification_subject' => 'Ticket Email',
                        'notification_message' => "Thanks for creating package service trip listing. We see you haven't emailed the ticket for approval. Please email the ticket with subject line " . $_POST['tripIdNumber'] . "",
                        'notification_added' => date("Y-m-d H:i:s")
                    );

                    $this->TripModel->addToNotifications($hugedata1);
                }
                echo json_encode(array('result' => 'success', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => $this->encrypt($trip_ID), 'requestURL' => '', 'trip_ID' => '', 'msg' => "Package service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
            }

        } else {
            $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
            $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
            $viewArray['travel_plans'] = $this->TripModel->getTravelTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid, false);
             $ID = $this->params('ID');
             $TYPE = $this->params('TYPE');
            $GOTOT_URL = isset($_GET['g']) ? urldecode($_GET['g']) : '';
            $viewArray['selectedFlightData'] = '';
            $viewArray['gotourl'] = $GOTOT_URL;
            if ($ID) {
                $tripId = $this->decrypt($ID);
                $tripDetails = $this->TripModel->getTrip($tripId);
                $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid, 'no', $tripDetails['user_location']);
                $viewArray['trip_type'] = 'round_trip';
                $trip_traveller_details = $this->TripModel->getTripPackage($tripId);
                $viewArray['airport_details'] = $this->TripModel->getFlightsDetails($tripId);
                $trip = isset($tripDetails) ? $tripDetails : false;
                $trip_traveller = isset($trip_traveller_details) ? $trip_traveller_details : false;
                $origin_location = $this->TripModel->getAirPort($tripDetails['origin_location']);
                $destination_location = $this->TripModel->getAirPort($tripDetails['destination_location']);
                $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $origin_location_country_code = isset($origin_location['country_code']) ? $origin_location['country_code'] : '';
                $destination_location_country_code = isset($destination_location['country_code']) ? $destination_location['country_code'] : '';
                $viewArray['origin_location_country_code'] = $origin_location_country_code;
                $viewArray['destination_location_country_code'] = $destination_location_country_code;
                $viewArray['origin_location'] = $origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;
                $viewArray['destination_location'] = $destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;
                $viewArray['origin_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';
                $viewArray['destination_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';
                $viewArray['selectedFlightData'] = !empty($viewArray['airport_details']) ? htmlspecialchars(json_encode($viewArray['airport_details'])) : '';
                $viewArray['TYPE'] = $TYPE;
                $editAddOptions = '<option value="' . $origin_location_country_code . '">' . $origin_location_country . '</option>';
                if ($origin_location_country_code != $destination_location_country_code)
                    $editAddOptions .= '<option value="' . $destination_location_country_code . '">' . $destination_location_country . '</option>';
                $viewArray['nonEditableProp'] = 'readonly="readonly"';
                $viewArray['disabledProp'] = 'readonly="readonly"';
                if (isset($TYPE) && !empty($TYPE)) {
                  $viewArray['origin_location'] = $destination_location['code'].','.$destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;;
                  $viewArray['destination_location'] = $origin_location['code'].','.$origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;;
                    
                    $viewArray['origin_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';;
                    $viewArray['destination_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';;
                    $trip['origin_location'] = $tripDetails['destination_location'];
                    $trip['destination_location'] = $tripDetails['origin_location'];
                    $viewArray['selectedFlightData'] = '';
                    $viewArray['airport_details'] = '';
                    $trip['user_location'] = 0;
                    $viewArray['disabledProp'] = 'readonly="readonly"';
                    $viewArray['nonEditableProp'] = '';
                    $trip_traveller = '';
                    $editAddOptions = '<option value="' . $destination_location_country_code . '">' . $destination_location_country . '</option>';
                    if ($origin_location_country_code != $destination_location_country_code)
                        $editAddOptions .= '<option value="' . $origin_location_country_code . '">' . $origin_location_country . '</option>';
                    unset($tripDetails['trip_id_number']);
                }
                $editAddOptions .= '<option value="more" class="more_sel_text">More</option>';
                $viewArray['editAddOptions'] = $editAddOptions;
                $viewArray['trip'] = $trip;
                $viewArray['trip_traveller'] = $trip_traveller;
            }

        }
        $viewArray['currency'] = $this->CommonMethodsModel->getCurrency();
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Traveller',
                'title' => 'Traveller',
                'active' => false,
                'redirect' => '/traveller'
            ),
            array(
                'label' => 'Add Package Service',
                'title' => 'Add Package Service',
                'active' => false,
                'redirect' => '/traveller/add-package-service'
            ),
            array(
                'label' => 'Package Service',
                'title' => 'Package Service',
                'active' => true,
                'redirect' => false //'/traveller/add-package-service'
            )
        );
         $viewArray['betrip'] = $betrip;
        $viewArray['comSessObj'] = $this->sessionObj;
        $this->layout()->pageController = 'traveller';
        $this->layout()->pageFunction = 'addPackageService';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;


    }

    public function addTripLocationAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $user_location = $this->TripPlugin->addTripOrginLocation();

        echo $user_location;
        exit;
    }

    public function addProjectServiceAction()
    {
        if(isset($_REQUEST['betrip']) && $_REQUEST['betrip'] =='yes')
        {
            
            $betrip = $_REQUEST['betrip'];
        }else {
            $betrip='';
        }
        
        
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->sessionObj->userrole = 'traveller';
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $viewArray['disabledProp'] = '';
        $viewArray['nonEditableProp'] = '';
        if ($_POST) {
            $_POST['type_of_service'] = 'project';
            extract($_POST);
			
			$user_mailer_datails = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			$origin_location = $_POST['origin_location'];
		    $destination_location= $_POST['destination_location'];
		    $departure_date = $_POST['departure_date'];
			$departure_time =$_POST['departure_time'];
			$firstname = $user_mailer_datails['first_name'];
			$create_date = date("Y-m-d H:i:s");
			$to = $user_mailer_datails['email_address'];
			
            $exitsTrip = false;
            if (isset($travel_plan_type) && $travel_plan_type == 'existing_plan') {
                $Exist_trip_ID = $this->CommonMethodsModel->cleanQuery(($travel_plan));
                $checkIfExistTrip = $this->TripModel->getTravellerProjectByTrip($Exist_trip_ID);
                if ($checkIfExistTrip) {
                    $exitsTrip = true;
                    $editTripUrl = '/traveller/add-project-service/' . $this->encrypt($checkIfExistTrip['trip']) . '#service';
                    echo json_encode(array('result' => 'exist', 'serviceUrl' => $editTripUrl, 'msg' => "Already a product service trip listing exists for this trip. Would you like to edit?"));
                    exit;
                }
            }
            $user_location = $this->TripPlugin->addTripOrginLocation();
            $trip_ID = $this->TripPlugin->addTripDetail($user_location);
            $verifiedIdentityStatus = $this->TripModel->checkverifiedIdentityStatus($this->sessionObj->userid);
            if ($verifiedIdentityStatus == true) {
                $ticketStatus = $this->TripModel->checkTicketStatus($trip_ID);
                if ($ticketStatus == true) {
                    $this->TripModel->setTripStatus($trip_ID, 3);
                } else {
                    $this->TripModel->setTripStatus($trip_ID, 4);
                }
            } else {
                $identityStatus = $this->TripModel->checkIdentityStatus($this->sessionObj->userid);
                if ($identityStatus == true) {
                    $this->TripModel->setTripStatus($trip_ID, 2);
                } else {
                    $this->TripModel->setTripStatus($trip_ID, 0);
                }
            }
            $this->TripPlugin->addTravellerProjectDetail($trip_ID);
            if (isset($gotourl) && !empty($gotourl)) {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array('user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has uploaded ticket for product service trip listing. Please take a decision at the earliest.",
                    'create_date' => date("Y-m-d H:i:s")

                );
                $this->TripModel->addToAdminNotifications($hugedata);
                
                 if ($ticketOption == "uploadTicket") {
                    $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_upload',
                        'notification_subject' => 'Ticket Upload',
                        'notification_message' => "Thanks for creating product service trip listing. We will review the uploaded ticket and approve at the earliest.",
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                     $this->TripModel->addToNotifications($hugedata1);
                 }
                if ($_POST['ticketOption'] == "emailTicket") {
                    $hugedata1 = array('user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_email',
                        'notification_subject' => 'Ticket Email',
                        'notification_message' => "Thanks for creating product service trip listing. We see you haven't emailed the ticket for approval. Please email the ticket with subject line " . $_POST['tripIdNumber'] . "",
                        'notification_added' => date("Y-m-d H:i:s")
                    );

                    $this->TripModel->addToNotifications($hugedata1);
                }
				try

                {

                    $this->sendmail = $this->MailModel->sendCreateListEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $to);

                }

                catch (\Exception $e)

                {

                    error_log($e);

                }
				
                $requestURL = $gotourl . '&traveller_project_request_id=' . $this->encrypt($trip_ID);
                echo json_encode(array('result' => 'success', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => '', 'requestURL' => $requestURL, 'msg' => "Product service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
            } else if (isset($trip_type) && $trip_type == '2') {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array('user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has uploaded ticket for product service trip listing. Please take a decision at the earliest.",
                    'create_date' => date("Y-m-d H:i:s")

                );
                $this->TripModel->addToAdminNotifications($hugedata);
                
                
                 if ($ticketOption == "uploadTicket") {
                    $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_upload',
                        'notification_subject' => 'Ticket Upload',
                        'notification_message' => "Thanks for creating product service trip listing. We will review the uploaded ticket and approve at the earliest.",
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                     $this->TripModel->addToNotifications($hugedata1);
                 }
                 
                if ($_POST['ticketOption'] == "emailTicket") {
                    $hugedata1 = array('user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_email',
                        'notification_subject' => 'Ticket Email',
                        'notification_message' => "Thanks for creating product service trip listing. We see you haven't emailed the ticket for approval. Please email the ticket with subject line " . $_POST['tripIdNumber'] . "",
                        'notification_added' => date("Y-m-d H:i:s")
                    );

                    $this->TripModel->addToNotifications($hugedata1);
                }
				
				try

                {

                    $this->sendmail = $this->MailModel->sendCreateListEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $to);

                }

                catch (\Exception $e)

                {

                    error_log($e);

                }
                echo json_encode(array('result' => 'success', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => '', 'trip_ID' => $this->encrypt($trip_ID), 'msg' => "Product service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
            } else {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array('user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has uploaded ticket for product service trip listing. Please take a decision at the earliest.",
                    'create_date' => date("Y-m-d H:i:s")

                );
                $this->TripModel->addToAdminNotifications($hugedata);
                if ($_POST['ticketOption'] == "emailTicket") {
                    $hugedata1 = array('user_id' => $this->sessionObj->userid,
                        'notification_type' => 'ticket_email',
                        'notification_subject' => 'Ticket Email',
                        'notification_message' => "Thanks for creating product service trip listing. We see you haven't emailed the ticket for approval. Please email the ticket with subject line " . $_POST['tripIdNumber'] . "",
                        'notification_added' => date("Y-m-d H:i:s")
                    );

                    $this->TripModel->addToNotifications($hugedata1);
                }
                
                if(isset($_REQUEST['return_trip'])  && $_REQUEST['return_trip'] =='round_trip'){
					
					
				try

                {

                    $this->sendmail = $this->MailModel->sendCreateListEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $to);

                }

                catch (\Exception $e)

                {

                    error_log($e);

                }
                    
                    echo json_encode(array('result' => 'success', 'return_trip' => 'yes', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => $this->encrypt($trip_ID), 'trip_ID' => '', 'msg' => "Product service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
                }else {
					
					try

					{

						$this->sendmail = $this->MailModel->sendCreateListEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $to);

					}

					catch (\Exception $e)

					{

						error_log($e);

					}


                        echo json_encode(array('result' => 'success', 'TID' => $this->encrypt($trip_ID), 'seacrhAction' => $this->encrypt($trip_ID), 'trip_ID' => '', 'msg' => "Product service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                        exit;
                }
            }

        } else {
            $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
            $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
            /*$viewArray['projectcat'] =$this->CommonMethodsModel->Listprojectcategory();*/
            $viewArray['projectcat'] = $this->TripModel->getProjectTasksCategorytype(0);

            $viewArray['project_tasks_categories_type'] = $this->TripModel->getProjectTasksCategorytype(1);
            $viewArray['travel_plans'] = $this->TripModel->getTravelTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid, false);

            $ID = $this->params('ID');
            $TYPE = $this->params('TYPE');

            $GOTOT_URL = isset($_GET['g']) ? urldecode($_GET['g']) : '';
            $viewArray['selectedFlightData'] = '';
            $viewArray['gotourl'] = $GOTOT_URL;

            if ($ID) {
                $tripId = $this->decrypt($ID);
                $tripDetails = $this->TripModel->getTrip($tripId);

                $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid, 'no', $tripDetails['user_location']);
                $viewArray['trip_type'] = 'round_trip';
                $trip_traveller_details = $this->TripModel->getTripProject($tripId);
                $trip = isset($tripDetails) ? $tripDetails : false;
                $trip_traveller = isset($trip_traveller_details) ? $trip_traveller_details : false;
                $viewArray['airport_details'] = $this->TripModel->getFlightsDetails($tripId);
                $origin_location = $this->TripModel->getAirPort($tripDetails['origin_location']);
                $destination_location = $this->TripModel->getAirPort($tripDetails['destination_location']);
                $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $origin_location_country_code = isset($origin_location['country_code']) ? $origin_location['country_code'] : '';
                $destination_location_country_code = isset($destination_location['country_code']) ? $destination_location['country_code'] : '';
                $viewArray['origin_location_country_code'] = $origin_location_country_code;
                $viewArray['destination_location_country_code'] = $destination_location_country_code;

                $viewArray['origin_location'] = $origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;
                $viewArray['destination_location'] = $destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;
                $viewArray['origin_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';
                $viewArray['destination_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';
                $viewArray['selectedFlightData'] = !empty($viewArray['airport_details']) ? htmlspecialchars(json_encode($viewArray['airport_details'])) : '';
                $viewArray['TYPE'] = $TYPE;
                $editAddOptions = '<option value="' . $origin_location_country_code . '">' . $origin_location_country . '</option>';
                if ($origin_location_country_code != $destination_location_country_code)
                    $editAddOptions .= '<option value="' . $destination_location_country_code . '">' . $destination_location_country . '</option>';
                $viewArray['disabledProp'] = 'readonly="readonly"';
                $viewArray['nonEditableProp'] = 'readonly="readonly"';
                if (isset($TYPE) && !empty($TYPE)) {
                   $viewArray['origin_location'] = $destination_location['code'].','.$destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;;
                    $viewArray['destination_location'] = $origin_location['code'].','.$origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;;
                    
                    $viewArray['origin_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';;
                    $viewArray['destination_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';;
                    $trip['origin_location'] = $tripDetails['destination_location'];
                    $trip['destination_location'] = $tripDetails['origin_location'];
                    $viewArray['selectedFlightData'] = '';
                    $viewArray['airport_details'] = '';
                    $trip['user_location'] = 0;
                    $viewArray['disabledProp'] = 'readonly="readonly"';
                    $viewArray['nonEditableProp'] = '';
                    $trip_traveller = '';
                    $editAddOptions = '<option value="' . $destination_location_country_code . '">' . $destination_location_country . '</option>';
                    if ($origin_location_country_code != $destination_location_country_code)
                        $editAddOptions .= '<option value="' . $origin_location_country_code . '">' . $origin_location_country . '</option>';

                    unset($tripDetails['trip_id_number']);
                }
                $editAddOptions .= '<option value="more" class="more_sel_text">More</option>';
                $viewArray['trip'] = $trip;
                $viewArray['editAddOptions'] = $editAddOptions;
                $viewArray['trip_traveller'] = $trip_traveller;

            }

            $viewArray['currency'] = $this->CommonMethodsModel->getCurrency();
            $viewArray['workCategory'] = $this->workCategory;
        }
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Traveller',
                'title' => 'Traveller',
                'active' => false,
                'redirect' => '/traveller'
            ),
            array(
                'label' => 'Add Product Service',
                'title' => 'Add Product Service',
                'active' => false,
                'redirect' => '/traveller/add-package-service'
            ),
            array(
                'label' => 'Product Service',
                'title' => 'Product Service',
                'active' => true,
                'redirect' => false //'/traveller/add-package-service'
            )
        );
        $viewArray['betrip'] = $betrip;
        $viewArray['comSessObj'] = $this->sessionObj;
        $this->layout()->pageController = 'traveller';
        $this->layout()->pageFunction = 'addProjectService';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    function projectserviceonlocationAction()
    {

        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        if ($_POST) {
            /*print_r($_POST);
            print_r($_FILES);
            exit;*/

            extract($_POST);

            $user_location = $this->TripPlugin->addTripOrginLocation();
            $trip_ID = $this->TripPlugin->addTripDetail($user_location);
            $this->TripPlugin->addTravellerProjectDetail($trip_ID);
            if ($trip_type == '2') {
                echo json_encode(array('trip_ID' => $trip_ID, 'msg' => "People service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
            } else {
                echo json_encode(array('trip_ID' => '', 'msg' => "People service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
            }
        } else {
            $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
            $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
            $viewArray['travel_plans'] = $this->TripModel->getTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid);
            /*/* For Round Trip Selection we bringing all data of added round trip*/

            $viewArray['projectcat'] = $this->CommonMethodsModel->Listprojectcategory();
            $ID = isset($_REQUEST['ID']) ? $_REQUEST['ID'] : 0;
            if ($ID) {
                $viewArray['trip_type'] = 'round_trip';
                $trip = $this->trip_mod->getTrip($ID);
                $viewArray['trip'] = $trip;
                $viewArray['trip_traveller'] = $this->TripModel->getTripPeople($ID);
            }
        }
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Traveller',
                'title' => 'Traveller',
                'active' => false,
                'redirect' => '/traveller'
            ),
            array(
                'label' => 'Add Product Service',
                'title' => 'Add Product Service',
                'active' => false,
                'redirect' => false
            ),
            array(
                'label' => 'Product Service',
                'title' => 'Product Service',
                'active' => true,
                'redirect' => '/traveller/project-service-on-location'
            )
        );

        $viewArray['comSessObj'] = $this->sessionObj;
        $this->layout()->pageController = 'traveller';
        $this->layout()->pageFunction = 'projectserviceonlocation';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }
	
	public function requestexpcronAction(){

		/** TRAVELLER PEOPLE REQUEST EXPIR NOTIFICATION **/
		$travellerPeople = $this->TripModel->getExpiredRequests('traveller_people');
		foreach($travellerPeople as $tp){
			$packageInfo = $this->TripModel->getTravellerPeopleByID($tp['service_id']);
			$trip = $this->TripModel->getTrip($packageInfo['trip'],'traveller');
			$origin_location = $this->TripModel->getAirPort($trip['origin_location']);
			$destination_location = $this->TripModel->getAirPort($trip['destination_location']);
			
			$origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
			$destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
				
			$hugedata1 = array('user_id' => $trip['user'],
				'notification_type' => 'request_expired',
				'notification_subject' => 'Request Expired',
				'notification_message' => "Your people service request from ".$origin_location_city." to ".$destination_location_city.' is expired.',
				'notification_added' => date("Y-m-d H:i:s")
			);
			$this->TripModel->addToNotifications($hugedata1);
		}
		
		/** TRAVELLER PACKAGE REQUEST EXPIR NOTIFICATION **/
		$travellerPackages = $this->TripModel->getExpiredRequests('traveller_package');
		foreach($travellerPackages as $tp){
			$packageInfo = $this->TripModel->getTravellerPackageByID($tp['service_id']);
			$trip = $this->TripModel->getTrip($packageInfo['trip'],'traveller');
			$origin_location = $this->TripModel->getAirPort($trip['origin_location']);
			$destination_location = $this->TripModel->getAirPort($trip['destination_location']);
			
			$origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
			$destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
				
			$hugedata1 = array('user_id' => $trip['user'],
				'notification_type' => 'request_expired',
				'notification_subject' => 'Request Expired',
				'notification_message' => "Your package service request from ".$origin_location_city." to ".$destination_location_city.' is expired.',
				'notification_added' => date("Y-m-d H:i:s")
			);
			$this->TripModel->addToNotifications($hugedata1);
		}
		
		/** TRAVELLER PROJECT REQUEST EXPIR NOTIFICATION **/
		$travellerProjects = $this->TripModel->getExpiredRequests('traveller_project');
		foreach($travellerProjects as $tp){
			$packageInfo = $this->TripModel->getTravellerProjectByID($tp['service_id']);
			$trip = $this->TripModel->getTrip($packageInfo['trip'],'traveller');
			$origin_location = $this->TripModel->getAirPort($trip['origin_location']);
			$destination_location = $this->TripModel->getAirPort($trip['destination_location']);
			
			$origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
			$destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
				
			$hugedata1 = array('user_id' => $trip['user'],
				'notification_type' => 'request_expired',
				'notification_subject' => 'Request Expired',
				'notification_message' => "Your product service request from ".$origin_location_city." to ".$destination_location_city.' is expired.',
				'notification_added' => date("Y-m-d H:i:s")
			);
			$this->TripModel->addToNotifications($hugedata1);
		}
		
		/** SEEKER PEOPLE REQUEST EXPIR NOTIFICATION **/
		$seekerPeople = $this->TripModel->getExpiredRequests('seeker_people');
		foreach($seekerPeople as $tp){
			$packageInfo = $this->TripModel->getTravellerPeopleByID($tp['service_id']);
			$trip = $this->TripModel->getTrip($packageInfo['trip'],'seeker');
			$origin_location = $this->TripModel->getAirPort($trip['origin_location']);
			$destination_location = $this->TripModel->getAirPort($trip['destination_location']);
			
			$origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
			$destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
				
			$hugedata1 = array('user_id' => $trip['user'],
				'notification_type' => 'request_expired',
				'notification_subject' => 'Request Expired',
				'notification_message' => "Your people service request from ".$origin_location_city." to ".$destination_location_city.' is expired.',
				'notification_added' => date("Y-m-d H:i:s")
			);
			$this->TripModel->addToNotifications($hugedata1);
		}
		
		/** SEEKER PACKAGE REQUEST EXPIR NOTIFICATION **/
		$seekerPackages = $this->TripModel->getExpiredRequests('seeker_package');
		foreach($seekerPackages as $tp){
			$packageInfo = $this->TripModel->getTravellerPackageByID($tp['service_id']);
			$trip = $this->TripModel->getTrip($packageInfo['trip'],'seeker');
			$origin_location = $this->TripModel->getAirPort($trip['origin_location']);
			$destination_location = $this->TripModel->getAirPort($trip['destination_location']);
			
			$origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
			$destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
				
			$hugedata1 = array('user_id' => $trip['user'],
				'notification_type' => 'request_expired',
				'notification_subject' => 'Request Expired',
				'notification_message' => "Your package service request from ".$origin_location_city." to ".$destination_location_city.' is expired.',
				'notification_added' => date("Y-m-d H:i:s")
			);
			$this->TripModel->addToNotifications($hugedata1);
		}
		
		/** SEEKER PROJECT REQUEST EXPIR NOTIFICATION **/
		$seekerProjects = $this->TripModel->getExpiredRequests('seeker_project');
		foreach($seekerProjects as $tp){
			$packageInfo = $this->TripModel->getTravellerProjectByID($tp['service_id']);
			$trip = $this->TripModel->getTrip($packageInfo['trip'],'seeker');
			$origin_location = $this->TripModel->getAirPort($trip['origin_location']);
			$destination_location = $this->TripModel->getAirPort($trip['destination_location']);
			
			$origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
			$destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
				
			$hugedata1 = array('user_id' => $trip['user'],
				'notification_type' => 'request_expired',
				'notification_subject' => 'Request Expired',
				'notification_message' => "Your product service request from ".$origin_location_city." to ".$destination_location_city.' is expired.',
				'notification_added' => date("Y-m-d H:i:s")
			);
			$this->TripModel->addToNotifications($hugedata1);
		}
		
		$this->TripModel->requestExpire();

		/** TRAVELLER TRIP EXPIR NOTIFICATION **/
		$tExpTrips = $this->TripModel->getExpiredTrips('traveller');
		foreach($tExpTrips as $trip){
			$trip_people_request = $this->TripModel->getSeekerPeopleRequest($trip['ID'])?count($this->TripModel->getSeekerPeopleRequest($trip['ID'])):0;
			$trip_package_request = $this->TripModel->getSeekerPackageRequest($trip['ID'])?count($this->TripModel->getSeekerPackageRequest($trip['ID'])):0;
			$trip_project_request = $this->TripModel->getSeekerProjectRequest($trip['ID'])?count($this->TripModel->getSeekerProjectRequest($trip['ID'])):0;

			if(!$trip_people_request && !$trip_package_request && !$trip_project_request){
				$origin_location = $this->TripModel->getAirPort($trip['origin_location']);
            	$destination_location = $this->TripModel->getAirPort($trip['destination_location']);
				
				$origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
				$destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
				
				$hugedata1 = array('user_id' => $trip['user'],
                    'notification_type' => 'trip_expired',
                    'notification_subject' => 'Trip Expired',
                    'notification_message' => "Your trip from ".$origin_location_city." to ".$destination_location_city.' is expired.',
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
			}
			$this->TripModel->setTripExpired('traveller',$trip['ID']);
		}
		
		/** SEEKER TRIP EXPIR NOTIFICATION **/
		$sExpTrips = $this->TripModel->getExpiredTrips('seeker');	
		foreach($sExpTrips as $trip){
			$trip_people_request = $this->TripModel->getTravellerPeopleRequest($trip['ID'])?count($this->TripModel->getTravellerPeopleRequest($trip['ID'])):0;
			$trip_package_request = $this->TripModel->getTravellerPackageRequest($trip['ID'])?count($this->TripModel->getTravellerPackageRequest($trip['ID'])):0;
			$trip_project_request = $this->TripModel->getTravellerProjectRequest($trip['ID'])?count($this->TripModel->getTravellerProjectRequest($trip['ID'])):0;
			
			if(!$trip_people_request && !$trip_package_request && !$trip_project_request){
				$origin_location = $this->TripModel->getAirPort($trip['origin_location']);
            	$destination_location = $this->TripModel->getAirPort($trip['destination_location']);
				
				$origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
				$destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
				
				$hugedata1 = array('user_id' => $trip['user'],
                    'notification_type' => 'trip_expired',
                    'notification_subject' => 'Trip Expired',
                    'notification_message' => "Your service trip from ".$origin_location_city." to ".$destination_location_city.' is expired.',
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
			}
			$this->TripModel->setTripExpired('seeker',$trip['ID']);
		}
		
		$completedTTrips = $this->TripModel->getArrivaledTrips('traveller');
		foreach($completedTTrips as $trip){
			$origin_location = $this->TripModel->getAirPort($trip['origin_location']);
			$destination_location = $this->TripModel->getAirPort($trip['destination_location']);
			
			$origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
			$destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
				
			$hugedata1 = array('user_id' => $trip['user'],
                    'notification_type' => 'trip_completion_pending',
                    'notification_subject' => 'Trip Completion Pending',
                    'notification_message' => "Your trip from ".$origin_location_city." to ".$destination_location_city.' appears to be completed. Please rate your experience and share feedback.',
                    'notification_added' => date("Y-m-d H:i:s")
               	);
                $this->TripModel->addToNotifications($hugedata1);
				$this->TripModel->updateCompletionNotificationSent('traveller',$trip['ID']);
		}
		
		$completedSTrips = $this->TripModel->getArrivaledTrips('seeker');
		foreach($completedSTrips as $trip){
			$origin_location = $this->TripModel->getAirPort($trip['origin_location']);
			$destination_location = $this->TripModel->getAirPort($trip['destination_location']);
			
			$origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
			$destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
				
			$hugedata1 = array('user_id' => $trip['user'],
                    'notification_type' => 'trip_completion_pending',
                    'notification_subject' => 'Trip Completion Pending',
                    'notification_message' => "Your trip from ".$origin_location_city." to ".$destination_location_city.' appears to be completed. Please rate your experience and share feedback.',
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
				$this->TripModel->updateCompletionNotificationSent('seeker',$trip['ID']);
		}

		echo 'Completed';die;
	}

    public function tripcontendersAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $viewArray = array();
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $ID = $this->TripPlugin->decrypt($this->params('ID'));
        $trip = $this->TripModel->getTrip($ID);
        $service = isset($_GET['service']) ? $_GET['service'] : '';
		$viewArray['trip_reviews']= $this->TripModel->getTripreview($ID,$this->sessionObj->userid,true,$this->sessionObj->userrole);
		$viewArray['trip_reviewed_ids'] = array();
		foreach($viewArray['trip_reviews'] as $tripReviews){
			$viewArray['trip_reviewed_ids'][] = $tripReviews['seeker_trip_id'];
		}
        $viewArray['trip_id'] = $ID;
		$viewArray['product_categories'] = $this->TripModel->getProjectTasksCategorytype();
        $viewArray['product_package_categories'] = $this->TripModel->getPackageCategorytype();
        $viewArray['product_subcategories'] = $this->TripModel->getProjectTasksCategorylisttypeAll();
        $viewArray['package_subcategories'] = $this->TripModel->getPackageTasksCategorylisttypeAll();
        $viewArray['task_categories'] = $this->TripModel->getProjectTasksCategorytype();
        $viewArray['trip'] = $trip;
        $trip_seeker = $this->TripPlugin->getTripDetails($ID, 'traveller', false, $service);
        $viewArray['user'] = $this->CommonMethodsModel->getUser($trip['user']);
        $user_cards_details = $this->TripModel->getcarddetails($this->sessionObj->userid);
        $viewArray['trip'] = $trip_seeker;
        $trip_traveller = $this->TripModel->getTripPeople($ID);
        $viewArray['trip_traveller'] = $trip_traveller;
        $trip_travaller_packages = $this->TripModel->getTripPackage($ID);
        $viewArray['trip_travaller_packages'] = $trip_travaller_packages;
        $trip_travaller_projects = $this->TripModel->getTripProject($ID);
        $viewArray['trip_travaller_projects'] = $trip_travaller_projects;
        $viewArray['trip_people_requests'] = $this->TripModel->getTripPeopleRequests($ID);
        $viewArray['trip_package_requests'] = $this->TripModel->getTripPackageRequests($ID);
        $viewArray['trip_project_requests'] = $this->TripModel->getTripProjectRequests($ID);

        $traveller_people_requests = '';
        if (isset($trip_traveller['ID'])) {
            $traveller_people_requests = $this->TripModel->getTravellerPeopleRequestsByTraveller($trip_traveller['ID']);
        }

        $traveller_package_requests = '';
        if (isset($trip_travaller_packages['ID'])) {
            $traveller_package_requests = $this->TripModel->getTravellerPackageRequestsByTraveller($trip_travaller_packages['ID']);
        }
        $traveller_project_requests = '';
        if (isset($trip_travaller_projects['ID'])) {

            $traveller_project_requests = $this->TripModel->getTravellerProjectRequestsByTraveller($trip_travaller_projects['ID']);
        }

        $viewArray['traveller_people_requests'] = $traveller_people_requests;
        $viewArray['traveller_package_requests'] = $traveller_package_requests;
        $viewArray['traveller_project_requests'] = $traveller_project_requests;
        $viewArray['user_cards_details'] = $user_cards_details;
		
		if(isset($_REQUEST['from']) && $_REQUEST['from'] == 'wishlist'){
			$this->layout()->CommonMethodsModel     = $this->CommonMethodsModel;
			$this->layout()->breadCrumbArr = array(
				array(
					'label' => 'Wishlist',
					'title' => 'Wishlist',
					'active' => false,
					'redirect' => '/wishlist'
				),
				array(
					'label' => 'Wishlisted Request',
					'title' => 'Wishlisted Request',
					'active' => true,
					'redirect' => false
				)
			);
		} else {
			$this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
			$this->layout()->breadCrumbArr = array(
				array(
					'label' => 'Trip',
					'title' => 'Trip',
					'active' => false,
					'redirect' => '/traveller'
				),
				array(
					'label' => 'Add Product Service',
					'title' => 'Add Product Service',
					'active' => true,
					'redirect' => false
				)
			);
		}
		$viewArray['comSessObj'] = $this->sessionObj;
		$viewArray['stripeConfig'] = $this->stripeConfig;

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    /*public function approveProjectRequestAction() {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        $trip_project_request = $this->TripModel->getTripProjectRequestUser($requestid);
        
        if($status==1){
            $primary_location = '';
            $user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false,$trip_project_request['ID'],$primary_location);
            $primary_card = 'yes';
             $projectservice       = $this->TripModel->getTripProject($tripid);
            $user_card = $this->CommonMethodsModel->getUserCardDetails($trip_project_request['card_id']);
            $country = $this->CommonMethodsModel->getCountry($user_location['country']);
            $arrDates = explode('/',$user_card['card_valid']);
            $param = array(
                'amount'        => $projectservice['project_service_fee'],
                'cc_type'       => $user_card['card_type'],
                'pay_trip_id'       =>$trip_project_request['tripId'],
                'pay_to_trip_id'       =>  $tripid,
                'pay_service_id' =>  $trip_project_request['serviceId'],
                'pay_card_type'       => $user_card['payment_card_type'],
                'pay_type'       => 'Hold',
                'pay_user'       => $user_card['user'],
                'pay_to_user'       => $this->sessionObj->userid,
                'pay_user_type'       => 'seeker',
                'pay_service' => 'project',
                'pay_trip_table'       => 'trips',
                'pay_request_id'       => $requestid,
                'cc_number'     => $this->decrypt($user_card['card_no']),
                'expirty_month'  => $arrDates[0],
                'expirty_year'  => $arrDates[1],
                
                'security_code' => $user_card['card_cvv'],
                'first_name'    => $user_card['holder_name'],
                'last_name'     => $user_card['holder_last_name'],
                'email_address' => $trip_project_request['email_address'],
                'address'       => $user_card['card_billing_address'],
                'city'          => $user_location['city'],
                'phone_no'          => $trip_project_request['phone'],
                'state'         => $user_location['state'],
                'zip'           => $user_card['card_billing_zipcode'],
                'country_code'  => $user_card['card_billing_country']
            );
            $response = array();
            if(isset($response['ACK']) && $response['ACK']=='Success'){            
                $this->TripModel->approveProjectRequest($tripid,$requestid,$status);
                $msg_status = 'success';
                $pay_id = $response['pay_id'];
                $msg = $trip_project_request['first_name']." ".$trip_project_request['last_name'].' has been approved for your project request';
                  $s = $this->TripModel->checkSeekerTripDetailsViaID($tripid);
             //print_r($s);die;
                   $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
                    $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
                  $note_reciever_id = $s['user'];
                  $note_sender_id = $this->sessionObj->userid; 
                  $note_origin = '('.$note_origin_all['city_code'].') '.$note_origin_all['city'].', '.$note_origin_all['country'];
                  $note_destination =  '('.$note_destination_all['city_code'].') '.$note_destination_all['city'].', '.$note_destination_all['country'];
                 

                  $uD = $this->CommonMethodsModel->getUser($note_sender_id);
                  if(strtolower($uD['gender']) =='male'){
                    $nname = 'Mr. '.$uD['first_name'];
                    $him_her='him';
                  }elseif(strtolower($uD['gender']) =='female'){
                    $nname = 'Ms./Mrs. '.$uD['first_name'];
                    $him_her='her';
                  }
                  
                
                 
                   $hugedata1=array(  'user_id'=>$note_reciever_id,
                                        'notification_type'=> 'Request Accept',
                                        'notification_subject'=>'Request Accept',
                                        'notification_message'=>" For the ".$note_origin." to ".$note_destination.", ".$nname." has accepted project request.Please contact him/her for further coordination.",
                                        'notification_added'=>date("Y-m-d H:i:s")
                                    );
                  $this->TripModel->addToNotifications($hugedata1);
            }else{
                  
                $pay_id = 0;
                $msg_status = 'failed';
                $msg = $trip_project_request['first_name']." ".$trip_project_request['last_name'].' has been not approved, due to issue in processing payment.';
            }
             echo json_encode(array('msg_status' => $msg_status, 'msg' => $msg,'pay_id'=>$this->encrypt($pay_id))); exit;
        }else{
            $s = $this->TripModel->checkTripDetailsViaID($tripid);
             //print_r($s);die;
                   $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
                    $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
                  $note_reciever_id = $s['user'];
                  $note_sender_id = $this->sessionObj->userid; 
                  $note_origin = '('.$note_origin_all['city_code'].') '.$note_origin_all['city'].', '.$note_origin_all['country'];
                  $note_destination =  '('.$note_destination_all['city_code'].') '.$note_destination_all['city'].', '.$note_destination_all['country'];
                 

                  $uD = $this->CommonMethodsModel->getUser($note_sender_id);
                  if(strtolower($uD['gender']) =='male'){
                    $nname = 'Mr. '.$uD['first_name'];
                    $him_her='him';
                  }elseif(strtolower($uD['gender']) =='female'){
                    $nname = 'Ms./Mrs. '.$uD['first_name'];
                    $him_her='her';
                  }
                  
                
                 
                   $hugedata1=array(  'user_id'=>$note_reciever_id,
                                        'notification_type'=> 'Request Decline',
                                        'notification_subject'=>'Request Decline',
                                        'notification_message'=>" For the ".$note_origin." to ".$note_destination.", ".$nname." has declined project request.Please contact him/her for further coordination.",
                                        'notification_added'=>date("Y-m-d H:i:s")
                                    );
                  $this->TripModel->addToNotifications($hugedata1);
            $this->TripModel->approveProjectRequest($tripid,$requestid,$status);
            $msg = $trip_project_request['first_name']." ".$trip_project_request['last_name'].' has been disapproved for your project request';
            echo json_encode(array('msg_status' => 'failure', 'msg' => $msg)); exit;
        }                
    }*/

    /*** Start This is the section for approve seeker's Request for Traveller's Trip***/
    public function approvePeopleRequestAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);

        $trip_people_request = $this->TripModel->getApprovedTripPeopleRequestUser($_POST['requestid']);

        if ($status == 1) {

            $primary_location = '';
            $user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false, $trip_people_request['ID'], $primary_location);
            $primary_card = 'yes';


            $user_card = $this->CommonMethodsModel->getUserCardDetails($trip_people_request['card_id']);

            $country = $this->CommonMethodsModel->getCountry($user_location['country']);
            $arrDates = explode('/', $user_card['card_valid']);
            $param = array(
                'amount' => $trip_people_request['people_service_fee'],
                /*'cc_type' => $user_card['card_type'],*/
                'pay_to_trip_id' => $tripid,
                'pay_user' => $user_card['user'],
                /*'pay_card_type' => $user_card['payment_card_type'],*/
                'pay_service_id' => $trip_people_request['serviceId'],
                'pay_type' => 'Hold',
                'pay_to_user' => $this->sessionObj->userid,
                'pay_trip_id' => $trip_people_request['tripId'],
                'pay_user_type' => 'seeker',
                'pay_service' => 'people',
                'pay_trip_table' => 'trips',
                'pay_request_id' => $requestid,
                /*'cc_number' => $this->decrypt($user_card['card_no']),
                'expirty_month' => $arrDates[0],
                'expirty_year' => $arrDates[1],
                'security_code' => $user_card['card_cvv'],*/
                'first_name' => $user_card['holder_name'],
                'last_name' => $user_card['holder_last_name'],
                'email_address' => $trip_people_request['email_address'],
                'address' => $user_location['street_address_1'],
                'city' => $user_location['city'],
                'phone_no' => $trip_people_request['phone'],
                'state' => $user_location['state'],
                'zip' => $user_location['zip_code'],
                'country_code' => $country['code'],
				'stripe_src_id'=> $user_card['stripe_src_id'],
				'stripe_customer_id'=>$customerInfo['cus_id']
            );
            /**/
            /*$response = array();
            $response['ACK'] = 'Success';
            $response['pay_id'] = '123';*/
			
			$response = array();
            $response = $this->TripPlugin->doPayPalPayment($param);
			
            if (isset($response['ACK']) && ($response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning')) {
                $this->TripModel->approvePeopleRequest($tripid, $requestid, $status);
				$msg_status = 'success';
				$pay_id = $response['pay_id'];
				$msg = $trip_people_request['first_name'] . " " . $trip_people_request['last_name'] . ' has been approved for your people service request.';
					
				$tr_user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false, $trip_people_request['ID'], $primary_location);
                $primary_card = 'yes';
                $tr_user_card = $this->CommonMethodsModel->getUserCardDetails($trip_people_request['card_id']);
                if (!empty($tr_user_card)) {
					
					$userInfo = $this->CommonMethodsModel->getUser($tr_user_card['user']);
					$customerInfo['cus_id'] = $userInfo['stripe_id'];
					
                    $tr_country = $this->CommonMethodsModel->getCountry($tr_user_location['country']);
                    $arrDates = explode('/', $tr_user_card['card_valid']);
                    $tr_param = array(
                        'amount' => $trip_people_request['package_service_fee'],
                        /*'cc_type' => $tr_user_card['card_type'],*/
                        'pay_service_id' => $trip_people_request['serviceId'],
                        'pay_trip_id' => $trip_people_request['tripId'],
                        'pay_to_trip_id' => $tripid,
                        /*'pay_card_type' => $user_card['payment_card_type'],*/
                        'pay_type' => 'Hold',
                        'pay_user' => $tr_user_card['user'],
                        'pay_to_user' => $this->sessionObj->userid,
                        'pay_user_type' => 'seeker',
                        'pay_service' => 'package',
                        'pay_trip_table' => 'trips',
                        'pay_request_id' => $requestid,
                        /*'cc_number' => $this->decrypt($tr_user_card['card_no']),
                        'expirty_month' => $arrDates[0],
                        'expirty_year' => $arrDates[1],
                        'security_code' => $tr_user_card['card_cvv'],*/
                        'first_name' => $tr_user_card['holder_name'],
                        'last_name' => $tr_user_card['holder_last_name'],
                        'email_address' => $trip_people_request['email_address'],
                        'address' => $tr_user_location['street_address_1'],
                        'city' => $tr_user_location['city'],
                        'state' => $tr_user_location['state'],
                        'phone_no' => $trip_people_request['phone'],
                        'zip' => $tr_user_location['zip_code'],
                        'country_code' => $tr_country['code'],
						'stripe_src_id'=> $tr_user_card['stripe_src_id'],
						'stripe_customer_id'=>$customerInfo['cus_id']
                    );
                    $tr_response = array();
                    $tr_response = $this->TripPlugin->doPayPalPayment($tr_param);
					$msg_status = 'success';
					$pay_id = $response['pay_id'];
					$msg = $trip_people_request['first_name'] . " " . $trip_people_request['last_name'] . ' has been approved for your people service request.';
				}
                $s = $this->TripModel->checkTripDetailsViaID($tripid);

                $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
                $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
                $note_reciever_id = $s['user'];
                $note_sender_id = $this->sessionObj->userid;
                $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
                $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];

                $uD = $this->CommonMethodsModel->getUser($note_sender_id);
                if (strtolower($uD['gender']) == 'male') {
                    $nname = 'Mr. ' . $uD['first_name'];
                    $him_her = 'him';
                } elseif (strtolower($uD['gender']) == 'female') {
                    $nname = 'Ms./Mrs. ' . $uD['first_name'];
                    $him_her = 'her';
                }
				
                $hugedata1 = array('user_id' => $note_reciever_id,
                    'notification_type' => 'request_accept',
                    'notification_subject' => 'Request Accept',
                    'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted your people service request. Please contact him/her for further coordination.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);

            } else {
                $pay_id = 0;
                $msg_status = 'failed';
                $msg = $trip_people_request['first_name'] . " " . $trip_people_request['last_name'] . ' has not been approved, due to payment failure. Please verify your payment method and try again. <br/> ' . $response["L_LONGMESSAGE0"];
            }
            echo json_encode(array('msg_status' => $msg_status, 'msg' => $msg, 'pay_id' => $this->encrypt($pay_id)));
            exit;
        } else {
            $s = $this->TripModel->checkTripDetailsViaID($tripid);
            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];


            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }


            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_declined',
                'notification_subject' => 'Request Declined',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has declined your people service request. Please send request to other potential seekers.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
            $this->TripModel->approvePeopleRequest($tripid, $requestid, $status);
            $msg = $trip_people_request['first_name'] . " " . $trip_people_request['last_name'] . ' has declined your people service request.';
            echo json_encode(array('msg_status' => 'failure', 'msg' => $msg));
            exit;
        }
        exit;
    }

    /*** END This is the section for approve seeker's Request for Traveller's Trip ***/

    public function approvePackageRequestAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        extract($_POST);
		
		$userInfo = $this->CommonMethodsModel->getUser($this->sessionObj->userid);

        if (isset($cardtype) && $cardtype == 'existing_card') {
            $cardId = $existing_card;
			$customerInfo['cus_id'] = $userInfo['stripe_id'];
        } else {
			$sourceInfo = json_decode($sourceInfo,true);		
			if(isset($userInfo['stripe_id']) && $userInfo['stripe_id'] != ''){
				$customerInfo = $this->TripPlugin->attachStripeSource($sourceInfo['source']['id'],$userInfo['stripe_id']);
			} else {
				$customerInfo = $this->TripPlugin->createStripeCustomer($sourceInfo);
				$this->ProfileModel->updateprofile(array('stripe_id'=>$customerInfo['cus_id']),$this->sessionObj->userid);
			}
			
			if($customerInfo['err_msg'] != ''){
				echo json_encode(array('msg_status'=>'failure','msg'=>"We couldn't authorize this transaction due to
which your service request hasn't been sent to seeker. Reason: ".$customerInfo['err_msg']));exit;
			}
            $arr_peopleabs_detail = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'holder_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_name']),
                'holder_last_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_last_name']),
                'card_billing_address' => $this->CommonMethodsModel->cleanQuery(isset($_POST['card_billing_address']) ? $_POST['card_billing_address'] : ''),
                'card_billing_country' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_country']),
                'card_billing_zipcode' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_zipcode']),
				'invisibile' => isset($_POST['save_card']) && $_POST['save_card'] == 1?0:1,
				'stripe_src_id' => $this->CommonMethodsModel->cleanQuery($customerInfo['src_id']),
                'modified' => date('Y-m-d H:i:s')
            );
            $cardId = $this->TripModel->Addcarddetails($arr_peopleabs_detail);
        }
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        /*$trip_package_request = $this->trip_mod->getApprovedTripPackageRequestUser();*/
        $trip_package_request = $this->TripModel->getTripPackageRequestUser($requestid);
        $packageservice = $this->TripModel->getTripPackage($tripid);
        if ($status == 1) {
            $primary_location = '';
            $user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false, $trip_package_request['ID'], $primary_location);
            $primary_card = 'yes';
            $user_card = $this->CommonMethodsModel->getUserCardDetails($cardId);
            $country = $this->CommonMethodsModel->getCountry($user_location['country']);
            $arrDates = explode('/', $user_card['card_valid']);
            $param = array(
                'amount' => $packageservice['package_service_fee'],
                /*'cc_type' => $user_card['card_type'],*/
                'pay_user' => $this->sessionObj->userid,
                'pay_to_user' => $trip_package_request['ID'],
                /*'pay_card_type' => $user_card['payment_card_type'],*/
                'pay_service_id' => $trip_package_request['serviceId'],
                'pay_type' => 'Hold',
                'pay_trip_id' => $tripid,
                'pay_to_trip_id' => $trip_package_request['tripId'],
                'pay_user_type' => 'traveller',
                'pay_trip_table' => 'trips',
                'pay_service' => 'package',
                'pay_request_id' => $requestid,
                /*'cc_number' => $this->decrypt($user_card['card_no']),
                'expirty_month' => $arrDates[0],
                'expirty_year' => $arrDates[1],
                'security_code' => $user_card['card_cvv'],*/
                'first_name' => $user_card['holder_name'],
                'last_name' => $user_card['holder_last_name'],
                'email_address' => $trip_package_request['email_address'],
                'address' => $user_location['street_address_1'],
                'city' => $user_location['city'],
                'phone_no' => $trip_package_request['phone'],
                'state' => $user_location['state'],
                'zip' => $user_location['zip_code'],
                'country_code' => $country['code'],
				'stripe_src_id'=> $user_card['stripe_src_id'],
				'stripe_customer_id'=>$customerInfo['cus_id']
            );
            $response = array();
            $response = $this->TripPlugin->doPayPalPayment($param);

            //print_r($response);die;
            if (isset($response['ACK']) && ($response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning')) {
                $this->TripModel->approvePackageRequest($tripid, $requestid, $status);
                $tr_user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false, $trip_package_request['ID'], $primary_location);
                $primary_card = 'yes';
                $tr_user_card = $this->CommonMethodsModel->getUserCardDetails($trip_package_request['card_id']);
                if (!empty($tr_user_card)) {
					
					$userInfo = $this->CommonMethodsModel->getUser($tr_user_card['user']);
					$customerInfo['cus_id'] = $userInfo['stripe_id'];
					
                    $tr_country = $this->CommonMethodsModel->getCountry($tr_user_location['country']);
                    $arrDates = explode('/', $tr_user_card['card_valid']);
                    $tr_param = array(
                        'amount' => $trip_package_request['package_service_fee'],
                        /*'cc_type' => $tr_user_card['card_type'],*/
                        'pay_service_id' => $trip_package_request['serviceId'],
                        'pay_trip_id' => $trip_package_request['tripId'],
                        'pay_to_trip_id' => $tripid,
                        /*'pay_card_type' => $user_card['payment_card_type'],*/
                        'pay_type' => 'Hold',
                        'pay_user' => $tr_user_card['user'],
                        'pay_to_user' => $this->sessionObj->userid,
                        'pay_user_type' => 'seeker',
                        'pay_service' => 'package',
                        'pay_trip_table' => 'trips',
                        'pay_request_id' => $requestid,
                        /*'cc_number' => $this->decrypt($tr_user_card['card_no']),
                        'expirty_month' => $arrDates[0],
                        'expirty_year' => $arrDates[1],
                        'security_code' => $tr_user_card['card_cvv'],*/
                        'first_name' => $tr_user_card['holder_name'],
                        'last_name' => $tr_user_card['holder_last_name'],
                        'email_address' => $trip_package_request['email_address'],
                        'address' => $tr_user_location['street_address_1'],
                        'city' => $tr_user_location['city'],
                        'state' => $tr_user_location['state'],
                        'phone_no' => $trip_package_request['phone'],
                        'zip' => $tr_user_location['zip_code'],
                        'country_code' => $tr_country['code'],
						'stripe_src_id'=> $tr_user_card['stripe_src_id'],
						'stripe_customer_id'=>$customerInfo['cus_id']
                    );
                    $tr_response = array();
                    $tr_response = $this->TripPlugin->doPayPalPayment($tr_param);
                    $msg_status = 'success';
                    $pay_id = $response['pay_id'];
                    $msg = $trip_package_request['first_name'] . " " . $trip_package_request['last_name'] . ' has been approved for your package service request';
                }

                $s = $this->TripModel->checkTripDetailsViaID($tripid);
                //print_r($s);die;
                $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
                $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
                $note_reciever_id = $s['user'];
                $note_sender_id = $this->sessionObj->userid;
                $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
                $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];


                $uD = $this->CommonMethodsModel->getUser($note_sender_id);
                if (strtolower($uD['gender']) == 'male') {
                    $nname = 'Mr. ' . $uD['first_name'];
                    $him_her = 'him';
                } elseif (strtolower($uD['gender']) == 'female') {
                    $nname = 'Ms./Mrs. ' . $uD['first_name'];
                    $him_her = 'her';
                }


                $hugedata1 = array('user_id' => $note_reciever_id,
                    'notification_type' => 'request_accept',
                    'notification_subject' => 'Request Accept',
                    'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted your package service request. Please contact him/her for further coordination.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
            } else {

                $pay_id = 0;
                $msg_status = 'failed';
                $msg = $trip_package_request['first_name'] . " " . $trip_package_request['last_name'] . ' has not been approved, due to payment failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
            }
            echo json_encode(array('msg_status' => $msg_status, 'msg' => $msg, 'pay_id' => $this->encrypt($pay_id)));
            exit;
        } else {
            $s = $this->TripModel->checkTripDetailsViaID($tripid);
            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];


            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }


            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_declined',
                'notification_subject' => 'Request Declined',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has declined your package service request. Please send request to other potential seekers.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
            $this->TripModel->approvePackageRequest($tripid, $requestid, $status);
            $msg = $trip_package_request['first_name'] . " " . $trip_package_request['last_name'] . ' has declined your package service request';
            echo json_encode(array('msg_status' => 'failure', 'msg' => $msg));
            exit;
        }
    }

    public function approveProjectRequestAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        extract($_POST);
		
		$userInfo = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
		
        if (isset($cardtype) && $cardtype == 'existing_card') {
            $cardId = $existing_card;
			$customerInfo['cus_id'] = $userInfo['stripe_id'];
        } else {
			
			$sourceInfo = json_decode($sourceInfo,true);		
			if(isset($userInfo['stripe_id']) && $userInfo['stripe_id'] != ''){
				$customerInfo = $this->TripPlugin->attachStripeSource($sourceInfo['source']['id'],$userInfo['stripe_id']);
			} else {
				$customerInfo = $this->TripPlugin->createStripeCustomer($sourceInfo);
				$this->ProfileModel->updateprofile(array('stripe_id'=>$customerInfo['cus_id']),$this->sessionObj->userid);
			}
			
			if($customerInfo['err_msg'] != ''){
				echo json_encode(array('msg_status'=>'failure','msg'=>"We couldn't authorize this transaction due to
which your service request hasn't been sent to seeker. Reason: ".$customerInfo['err_msg']));exit;
			}

            $arr_peopleabs_detail = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'holder_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_name']),
                'holder_last_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_last_name']),
                'card_billing_address' => $this->CommonMethodsModel->cleanQuery(isset($_POST['card_billing_address']) ? $_POST['card_billing_address'] : ''),
                'card_billing_country' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_country']),
                'card_billing_zipcode' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_zipcode']),
				'invisibile' => isset($_POST['save_card']) && $_POST['save_card'] == 1?0:1,
				'stripe_src_id' => $this->CommonMethodsModel->cleanQuery($customerInfo['src_id']),
                'modified' => date('Y-m-d H:i:s')
            );
            $cardId = $this->TripModel->Addcarddetails($arr_peopleabs_detail);
        }
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        /*$trip_package_request = $this->trip_mod->getApprovedTripPackageRequestUser();*/
        $trip_project_request = $this->TripModel->getTripProjectRequestUser($requestid);
        $projectservice = $this->TripModel->getTripProject($tripid);
        if ($status == 1) {
            $primary_location = '';
            $user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false, $trip_project_request['ID'], $primary_location);
            $primary_card = 'yes';
            $user_card = $this->CommonMethodsModel->getUserCardDetails($cardId);
            $country = $this->CommonMethodsModel->getCountry($user_location['country']);
            $arrDates = explode('/', $user_card['card_valid']);
            $param = array(
                'amount' => $projectservice['project_service_fee'],
                /*'cc_type' => $user_card['card_type'],*/
                'pay_user' => $this->sessionObj->userid,
                'pay_to_user' => $trip_project_request['ID'],
                /*'pay_card_type' => $user_card['payment_card_type'],*/
                'pay_service_id' => $trip_project_request['serviceId'],
                'pay_type' => 'Hold',
                'pay_trip_id' => $tripid,
                'pay_to_trip_id' => $trip_project_request['tripId'],
                'pay_user_type' => 'traveller',
                'pay_trip_table' => 'trips',
                'pay_service' => 'project',
                'pay_request_id' => $requestid,
                /*'cc_number' => $this->decrypt($user_card['card_no']),
                'expirty_month' => $arrDates[0],
                'expirty_year' => $arrDates[1],
                'security_code' => $user_card['card_cvv'],*/
                'first_name' => $user_card['holder_name'],
                'last_name' => $user_card['holder_last_name'],
                'email_address' => $trip_project_request['email_address'],
                'address' => $user_location['street_address_1'],
                'city' => $user_location['city'],
                'phone_no' => $trip_project_request['phone'],
                'state' => $user_location['state'],
                'zip' => $user_location['zip_code'],
                'country_code' => $country['code'],
				'stripe_src_id'=> $user_card['stripe_src_id'],
				'stripe_customer_id'=>$customerInfo['cus_id']
            );
            $response = array();
            $response = $this->TripPlugin->doPayPalPayment($param);
            // print_r($response);die;
            if (isset($response['ACK']) && ($response['ACK'] == 'Success' || $response['ACK'] == 'SuccessWithWarning')) {
                $this->TripModel->approveProjectRequest($tripid, $requestid, $status);
                $tr_user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false, $trip_project_request['ID'], $primary_location);
                $primary_card = 'yes';
                $tr_user_card = $this->CommonMethodsModel->getUserCardDetails($trip_project_request['card_id']);
                if (!empty($tr_user_card)) {
					
					$userInfo = $this->CommonMethodsModel->getUser($tr_user_card['user']);
					$customerInfo['cus_id'] = $userInfo['stripe_id'];
		
                    $tr_country = $this->CommonMethodsModel->getCountry($tr_user_location['country']);
                    $arrDates = explode('/', $tr_user_card['card_valid']);
                    $tr_param = array(
                        'amount' => $trip_project_request['package_service_fee'],
                        /*'cc_type' => $tr_user_card['card_type'],*/
                        'pay_service_id' => $trip_project_request['serviceId'],
                        'pay_trip_id' => $trip_project_request['tripId'],
                        'pay_to_trip_id' => $tripid,
                        /*'pay_card_type' => $user_card['payment_card_type'],*/
                        'pay_type' => 'Hold',
                        'pay_user' => $tr_user_card['user'],
                        'pay_to_user' => $this->sessionObj->userid,
                        'pay_user_type' => 'seeker',
                        'pay_service' => 'project',
                        'pay_trip_table' => 'trips',
                        'pay_request_id' => $requestid,
                        /*'cc_number' => $this->decrypt($tr_user_card['card_no']),
                        'expirty_month' => $arrDates[0],
                        'expirty_year' => $arrDates[1],
                        'security_code' => $tr_user_card['card_cvv'],*/
                        'first_name' => $tr_user_card['holder_name'],
                        'last_name' => $tr_user_card['holder_last_name'],
                        'email_address' => $trip_package_request['email_address'],
                        'address' => $tr_user_location['street_address_1'],
                        'city' => $tr_user_location['city'],
                        'state' => $tr_user_location['state'],
                        'phone_no' => $trip_package_request['phone'],
                        'zip' => $tr_user_location['zip_code'],
                        'country_code' => $tr_country['code'],
						'stripe_src_id'=> $tr_user_card['stripe_src_id'],
						'stripe_customer_id'=>$customerInfo['cus_id']
                    );
                    $tr_response = array();
                    $tr_response = $this->TripPlugin->doPayPalPayment($tr_param);
                    $msg_status = 'success';
                    $pay_id = $response['pay_id'];
                    $msg = $trip_project_request['first_name'] . " " . $trip_project_request['last_name'] . ' has been approved for your product service request';
                }

                $s = $this->TripModel->checkTripDetailsViaID($tripid);
                //print_r($s);die;
                $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
                $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
                $note_reciever_id = $s['user'];
                $note_sender_id = $this->sessionObj->userid;
                $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
                $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];


                $uD = $this->CommonMethodsModel->getUser($note_sender_id);
                if (strtolower($uD['gender']) == 'male') {
                    $nname = 'Mr. ' . $uD['first_name'];
                    $him_her = 'him';
                } elseif (strtolower($uD['gender']) == 'female') {
                    $nname = 'Ms./Mrs. ' . $uD['first_name'];
                    $him_her = 'her';
                }


                $hugedata1 = array('user_id' => $note_reciever_id,
                    'notification_type' => 'request_accept',
                    'notification_subject' => 'Request Accept',
                    'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted your product service request. Please contact him/her for further coordination.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
            } else {

                $pay_id = 0;
                $msg_status = 'failed';
                $msg = $trip_project_request['first_name'] . " " . $trip_project_request['last_name'] . ' has not been approved, due to payment failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
            }
            echo json_encode(array('msg_status' => $msg_status, 'msg' => $msg, 'pay_id' => $this->encrypt($pay_id)));
            exit;
        } else {
            $s = $this->TripModel->checkTripDetailsViaID($tripid);
            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];


            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }


            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_declined',
                'notification_subject' => 'Request Declined',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has declined project request. Please contact him/her for further coordination.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
            $this->TripModel->approveProjectRequest($tripid, $requestid, $status);
            $msg = $trip_project_request['first_name'] . " " . $trip_project_request['last_name'] . ' has declined your product service request. Please send request to other potential seekers.';
            echo json_encode(array('msg_status' => 'failure', 'msg' => $msg));
            exit;
        }
    }

    /*** START This is the section for send Request to seeker's trips by traveller ***/
    public function addTravellerPeopleRequestAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');

        extract($_POST);

        if ($traveller_trip_id) {

            $travellerPeopleRequest = $this->TripModel->getTravellerPeopleRequest($traveller_trip_id);
            if ($travellerPeopleRequest['persons_travelling']) {
                echo json_encode(array('msg' => 'You have already sent people service request to this seeker.'));
                exit;
            } else {
                $traveller_people_request_id = $this->TripPlugin->addTravellerPeopleDetail($traveller_trip_id);
                echo json_encode(array('traveller_trip_id' => $traveller_people_request_id, 'traveller_people_request_id' => $traveller_people_request_id, 'msg' => ' Your people service request has been sent successfully!'));
                exit;
            }
        } else {
            $user_primary_location = $this->TripModel->getUserPrimaryLocation($this->sessionObj->userid);
            $traveller_trip_exist = $this->TripModel->checkTravellerTripExistBySeekerTrip($seeker_trip_id, $this->sessionObj->userid);
            if ($traveller_trip_exist['ID'] == '') {
                $new_traveller_trip_id = $this->TripModel->addTravellerTripBySeekerTrip($seeker_trip_id, $this->sessionObj->userid, $user_primary_location['ID']);
                $traveller_people_request_id = $this->TripPlugin->addTravellerPeopleDetail($new_traveller_trip_id);
                echo json_encode(array('traveller_trip_id' => $new_traveller_trip_id, 'traveller_people_request_id' => $traveller_people_request_id, 'msg' => ' Your people service request has been sent successfully!'));
                exit;
            } else {
                $traveller_trip_people_service_exist = $this->TripModel->getTravellerPeople($traveller_trip_exist['ID']);
                if ($traveller_trip_people_service_exist['ID'] == '') {
                    $traveller_people_request_id = $this->TripPlugin->addTravellerPeopleDetail($traveller_trip_exist['ID']);
                    echo json_encode(array('traveller_trip_id' => $traveller_trip_exist['ID'], 'traveller_people_request_id' => $traveller_people_request_id, 'msg' => ' Your people service request has been sent successfully!'));
                    exit;
                } else {
                    echo json_encode(array('msg' => ' People service information is already existing for your trip!'));
                    exit;
                }
            }
        }
    }

    public function uploadTicketAction()
    {
       /*print_r($_FILES);
        print_r($_POST);
        die;*/
        $tripId = $this->CommonMethodsModel->cleanQuery($_POST['tripIdNumber']);

        if (!empty($_FILES)) {

            $image_path = "tickets";
            $field_name = "file";
            $file_name_prefix = $this->CommonMethodsModel->cleanQuery($_POST['tripIdNumber']) . '_' . time();
            $uploadResult = $this->CommonMethodsModel->uploadFiles( $field_name,$file_name_prefix,$image_path );
            $ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			$targetpath = ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext;
            if (@move_uploaded_file($_FILES['file']['tmp_name'],$targetpath )) {
                $uploadResult['result'] = 'success';
            }
            if ($uploadResult['result'] == 'success') {
                $ticket_file = $file_name_prefix . '.' . $ext;//$uploadResult['fileName'];
                // print_r($_POST); die;
				
				
			
					$arr_ticket_detail = array(
						'trip_id_number' 	=> $tripId,
						'ticket_image' => $this->CommonMethodsModel->cleanQuery($ticket_file),
						'ticket_option' => 'uploadTicket'
					);
					
					if(isset($_POST['sepUpload']) && $_POST['sepUpload'] == 1){
						$arr_ticket_detail['trip_status'] = 3;
					}
					
					$this->TripModel->updateTrip($arr_ticket_detail, $tripId);   
					
				
				//print_r($arr_ticket_detail);die;
                $userDetails = $this->sessionObj->user;

                try
                {
                    $this->MailModel->sendTripRequestMailtoAdmin($userDetails['first_name'], $userDetails['last_name'], $_POST['tripIdNumber']);
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }

                

            }
        }else {
            
                        $arr_ticket_detail = array(
						
						'ticket_image' => $this->CommonMethodsModel->cleanQuery($_REQUEST['return_trip_image']),
						
					);
					
					$this->TripModel->updateTrip($arr_ticket_detail, $tripId);   
            
        }
        echo json_encode(array('result' => 'success'));
        exit;
    }

    function composeEmailAction()
    {
        $userDetails = $this->sessionObj->user;

        try
        {
            $this->MailModel->sendTicketRequestMailtoAdmin($userDetails['first_name'], $userDetails['last_name'], $_POST['subject'], $_POST['message']);
        }
        catch (\Exception $e)
        {
            error_log($e);
        }

        echo 'Your email has been sent successfully.';
        exit;
    }

    public function sendreportmailAction()
    {
        extract($_POST);
        $userDetails = $this->sessionObj->user;
        $tripidnumber = $this->TripModel->getTrip($tripid);
        $reportdata = array(
            'report_username' => $userDetails['first_name'] . " " . $userDetails['last_name'],
            'report_tripid' => $tripidnumber['trip_id_number'],
            'report_message' => $report,
            'report_created' => time(),
        );
        $this->CommonMethodsModel->addReport($reportdata);

        try
        {
            $this->MailModel->sendReportListMail($userDetails['first_name'], $userDetails['last_name'], $tripidnumber['trip_id_number'], $report);
        }
        catch (\Exception $e)
        {
            error_log($e);
        }

        echo 'Your report has been sent successfully.';
        exit;
    }

    public function sendmessagemailAction()
    {
        extract($_POST);
        $userDetails = $this->sessionObj->user;
        $tripidnumber = $this->TripModel->getTrip($tripid);
		$seekertripId = $this->decrypt($seekerTripId);
        $tripuserdetails = $this->CommonMethodsModel->getUser($tripidnumber['user']);
        $tripuserfullname = $tripuserdetails['first_name'] . " " . $tripuserdetails['last_name'];
        $fullname = $userDetails['first_name'] . " " . $userDetails['last_name'];
        $messagedata = array(
            'message_from' => $fullname,
            'message_to' => $tripuserfullname,
            'message_tripid' => $tripidnumber['trip_id_number'],
			'traveller_trip_id'=> $tripid,
            'message_subject' => $subject,
            'message_content' => $message,
            'message_created' => time(),
        );
        $this->CommonMethodsModel->addMessage($messagedata);
		
		$hugeData = array
		(
			'recevier_id' => $tripidnumber['user'],
			'sender_id' => $userDetails['ID'],
			'traveller_trip_id'=> $tripid,
			'seeker_trip_id'=> $seekertripId,
			'message' => $message,
			'create_date' => date("Y-m-d H:i:s"),
			'modified_date' => date("Y-m-d H:i:s"),
			'active' => 1
		);
		$this->TripModel->addToTravellerEnquiry($hugeData);
		
		$hugedata1 = array
		(
			'user_id' => $tripidnumber['user'],
			'notification_type' => 'traveller_inquires',
			'notification_subject' => 'Traveller Inquired',
			'notification_message' => 'Traveller service Inquired. Message: '.$message,
			'app_redirect' => '/seeker/detail/'.$seekertripId.'?service='.$service.'&tID='.$tripid.'&sID='.$seekertripId.'&from=inquired',
			'notification_added' => date("Y-m-d H:i:s")
		);
		$this->TripModel->addToNotifications($hugedata1);

        try
        {
            $this->MailModel->sendMemberMail($fullname, $tripuserfullname, $tripuserdetails['email_address'], $tripidnumber['trip_id_number'], $subject, $message, $userDetails['email_address'], 'SEEKER');
        }
        catch (\Exception $e)
        {
            error_log($e);
        }

        echo 'Your message has been sent successfully.';
        exit;
    }

    public function addTravellerPackageRequestAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        extract($_POST);
        if ($traveller_trip_id) {
            $travellerPackageRequest = $this->TripModel->getTravellerPackageRequest($traveller_trip_id);
            if ($travellerPackageRequest['package_request']) {
                echo json_encode(array('msg' => 'You have already sent package service request to this seeker.'));
                exit;
            } else {
                $traveller_package_request_id = $this->TripPlugin->addTravellerPackageDetail($traveller_trip_id);
                echo json_encode(array('traveller_trip_id' => $traveller_trip_id, 'traveller_package_request_id' => $traveller_package_request_id, 'msg' => ' Your package service request has been sent successfully!'));
                exit;
            }
        } else {
            $user_primary_location = $this->TripModel->getUserPrimaryLocation($this->sessionObj->userid);
            $traveller_trip_exist = $this->TripModel->checkTravellerTripExistBySeekerTrip($seeker_trip_id, $this->sessionObj->userid);

            if ($traveller_trip_exist['ID'] == '') {
                $new_traveller_trip_id = $this->TripModel->addTravellerTripBySeekerTrip($seeker_trip_id, $this->sessionObj->userid, $user_primary_location['ID']);
                $traveller_package_request_id = $this->TripPlugin->addTravellerPackageDetail($new_traveller_trip_id);
                echo json_encode(array('traveller_trip_id' => $new_traveller_trip_id, 'traveller_package_request_id' => $traveller_package_request_id, 'msg' => ' Your package service request has been sent successfully!'));
                exit;
            } else {
                $traveller_trip_package_service_exist = $this->TripModel->getTravellerPackage($traveller_trip_exist['ID']);
                if ($traveller_trip_package_service_exist['ID'] == '') {

                    $traveller_package_request_id = $this->TripPlugin->addTravellerPackageDetail($traveller_trip_exist['ID']);
                    echo json_encode(array('traveller_trip_id' => $traveller_trip_exist['ID'], 'traveller_package_request_id' => $traveller_package_request_id, 'msg' => ' Your package service request has been sent successfully!'));
                    exit;
                } else {
                    echo json_encode(array('msg' => ' Package service information is already existing for your trip!'));
                    exit;
                }
            }
        }
    }

    public function addTravellerProjectRequestAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        extract($_POST);
        $userDetails = $this->sessionObj->user;
        $seeker_trip = $this->TripModel->getSeekerTrip($seeker_trip_id);
        if ($seeker_trip['user'] != $userDetails['ID']) {
            if ($traveller_trip_id) {

                $travellerProjectRequest = $this->TripModel->getTravellerProjectRequest($traveller_trip_id);
                if ($travellerProjectRequest['packages']) {
                    echo json_encode(array('msg' => 'You have already sent product service request to this seeker.'));
                    exit;
                } else {
                    $traveller_project_request_id = $this->TripPlugin->addTravellerProjectDetail($traveller_trip_id);
                    echo json_encode(array('traveller_trip_id' => $traveller_trip_id, 'traveller_project_request_id' => $traveller_project_request_id, 'msg' => ' Your product service request has been sent successfully!'));
                    exit;
                }
            } else {
                $user_primary_location = $this->TripModel->getUserPrimaryLocation($this->sessionObj->userid);
                $traveller_trip_exist = $this->TripModel->checkTravellerTripExistBySeekerTrip($seeker_trip_id, $this->sessionObj->userid);
                if ($traveller_trip_exist['ID'] == '') {
                    $new_traveller_trip_id = $this->TripModel->addTravellerTripBySeekerTrip($seeker_trip_id, $this->sessionObj->userid, $user_primary_location['ID']);
                    $traveller_project_request_id = $this->TripPlugin->addTravellerProjectDetail($new_traveller_trip_id);
                    echo json_encode(array('traveller_trip_id' => $new_traveller_trip_id, 'traveller_project_request_id' => $traveller_project_request_id, 'msg' => ' Your product service request has been sent successfully!'));
                    exit;
                } else {
                    $traveller_trip_project_service_exist = $this->TripModel->getTravellerProject($traveller_trip_exist['ID']);
                    if ($traveller_trip_project_service_exist['ID'] == '') {
                        $traveller_project_request_id = $this->TripPlugin->addTravellerProjectDetail($traveller_trip_exist['ID']);
                        echo json_encode(array('traveller_trip_id' => $traveller_trip_exist['ID'], 'traveller_project_request_id' => $traveller_project_request_id, 'msg' => ' Your product service request has been sent successfully!'));
                        exit;
                    } else {
                        echo json_encode(array('msg' => ' Product service information is already existing for your trip!'));
                        exit;
                    }
                }
            }
        } else {
            echo " You can't send request to your own trip.";
        }

    }

    public function addTravellerTripRequestAction()
    {

        extract($_POST);
        $this->TripPlugin = $this->plugin('TripPlugin');
		
		$cardId = 0;
		
		if($type_of_service == 'Package'){
			$userInfo = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			
			if (isset($cardtype) && $cardtype == 'existing_card') {
				$cardId = $existing_card;
				$customerInfo['cus_id'] = $userInfo['stripe_id'];
			} else if (isset($_POST['addUserCard']) && !empty($_POST['addUserCard'])) {
				
				$sourceInfo = json_decode($sourceInfo,true);			
				if(isset($userInfo['stripe_id']) && $userInfo['stripe_id'] != ''){
					$customerInfo = $this->TripPlugin->attachStripeSource($sourceInfo['source']['id'],$userInfo['stripe_id']);
				} else {
					$customerInfo = $this->TripPlugin->createStripeCustomer($sourceInfo);
					$this->ProfileModel->updateprofile(array('stripe_id'=>$customerInfo['cus_id']),$this->sessionObj->userid);
				}
				
				if($customerInfo['err_msg'] != ''){
					echo json_encode(array('msg'=>"We couldn't authorize this transaction due to
which your service request hasn't been sent to seeker. Reason: ".$customerInfo['err_msg']));exit;
				}
	
				$arr_peopleabs_detail = array(
					'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
					'holder_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_name']),
					'holder_last_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_last_name']),
					'card_billing_address' => $this->CommonMethodsModel->cleanQuery(isset($_POST['card_billing_address']) ? $_POST['card_billing_address'] : ''),
					'card_billing_country' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_country']),
					'card_billing_zipcode' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_zipcode']),
					'stripe_src_id' => $this->CommonMethodsModel->cleanQuery($customerInfo['src_id']),
					'invisibile' => isset($_POST['save_card']) && $_POST['save_card'] == 1?0:1,
					'modified' => date('Y-m-d H:i:s')
				);
				$cardId = $this->TripModel->Addcarddetails($arr_peopleabs_detail);
			}
		}
        $userDetails = $this->sessionObj->user;
        $seeker_service_request_exist = $service_request_exist = '';
        $seeker_trip = $this->TripModel->getSeekerTrip($seeker_trip_id);
		$traveler_trip = 0;
        if ($seeker_trip['user'] != $userDetails['ID']) {
            if ($traveller_people_request_id) {
                $traveller_people = $this->TripModel->getTravellerPeopleByID($traveller_people_request_id);
                $seekerSentPeopleRequestAlready = $this->TripModel->checkSeekerPeopleRequestSentAlready($seeker_trip_id, $traveller_people['trip']);
				
				$traveler_trip = $traveller_people['trip'];
                if ($seekerSentPeopleRequestAlready['ID'] == '') {
                    $travellerPeopleRequestExist = $this->TripModel->checkTravellerPeopleRequestExist($seeker_trip_id, $traveller_people_request_id);
                    if ($travellerPeopleRequestExist['ID'] == '') {
                        $trip_traveller_people_request = array(
                            'trip' => $seeker_trip_id,
                            'people_request' => $traveller_people_request_id,
                            'service_id' => $traveller_people_service_id,
                            'card_id' => $cardId,
                            'approved' => 0,
                            'modified' => date('Y-m-d H:i:s')
                        );
                        $this->TripModel->addTripTravellerPeopleRequest($trip_traveller_people_request);
                    } else {
                        $service_request_exist = 'people';
                    }
                } else {
                    $seeker_service_request_exist = 'people';
                }
            }

            if ($traveller_package_request_id) {
                //
                $traveller_package = $this->TripModel->getTravellerPackageByID($traveller_package_request_id);
                $seekerSentPackageRequestAlready = $this->TripModel->checkSeekerPackageRequestSentAlready($seeker_trip_id, $traveller_package['trip']);
				
				$traveler_trip = $traveller_package['trip'];
                if ($seekerSentPackageRequestAlready['ID'] == '') {
                    $travellerPackageRequestExist = $this->TripModel->checkTravellerPackageRequestExist($seeker_trip_id, $traveller_package_request_id);
                    if ($travellerPackageRequestExist['ID'] == '') {
                        $trip_traveller_package_request = array(
                            'trip' => $seeker_trip_id,
                            'package_request' => $traveller_package_request_id,
                            'service_id' => $traveller_package_service_id,
                            'card_id' => $cardId,
                            'approved' => 0,
                            'modified' => date('Y-m-d H:i:s')
                        );
                        //print_r($trip_traveller_package_request);die('add');
                        $primary_location = '';
                        $tr_user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false, $this->sessionObj->userid, $primary_location);
                        $primary_card = 'yes';
                        $tr_user_card = $this->CommonMethodsModel->getUserCardDetails($cardId);
                        if (!empty($tr_user_card) && !empty($traveller_package_amount)) {

                            $tr_country = $this->CommonMethodsModel->getCountry($tr_user_location['country']);
                            $arrDates = explode('/', $tr_user_card['card_valid']);
                            $tr_param = array(
                                'amount' => $traveller_package_amount,
                                /*'cc_type' => $tr_user_card['card_type'],*/
                                'pay_trip_id' => $seeker_trip_id,
                                'pay_to_trip_id' => $traveller_package['trip'],
                                'pay_user' => $this->sessionObj->userid,
                                'pay_to_user' => $seeker_trip['user'],
                                /*'pay_card_type' => $tr_user_card['payment_card_type'],*/
                                'pay_service_id' => $traveller_package_service_id,
                                'pay_type' => 'Hold',
                                'pay_user_type' => 'traveller',
                                'pay_service' => 'package',
                                'pay_trip_table' => 'seeker_trips',
                                'pay_request_id' => $traveller_package_request_id,
                                /*'cc_number' => $this->decrypt($tr_user_card['card_no']),
                                'expirty_month' => $arrDates[0],
                                'expirty_year' => $arrDates[1],
                                'security_code' => $tr_user_card['card_cvv'],*/
                                'first_name' => $userDetails['first_name'],
                                'last_name' => $userDetails['last_name'],
                                'email_address' => $userDetails['email_address'],
                                'address' => $tr_user_location['street_address_1'],
                                'city' => $tr_user_location['city'],
                                'state' => $tr_user_location['state'],
                                'phone_no' => $userDetails['phone'],
                                'zip' => $tr_user_location['zip_code'],
                                'country_code' => $tr_country['code'],
								'stripe_src_id'=> $tr_user_card['stripe_src_id'],
								'stripe_customer_id'=>$customerInfo['cus_id']
                            );
                            $tr_response = $this->TripPlugin->doPayPalPayment($tr_param);
                            if (isset($tr_response['ACK']) && $tr_response['ACK'] == 'Success') {
                                $this->TripModel->addTripTravellerPackageRequest($trip_traveller_package_request);

                            } else {
                                echo json_encode(array('msg'=>' Package service request has not been sent due to payment authorization failure. Please verify your payment method and try again.<br/> ' . $tr_response["L_LONGMESSAGE0"]));
                                exit;
                            }
                        }

                    } else {
                        if ($service_request_exist)
                            $service_request_exist = $service_request_exist . ',';
                        $service_request_exist = $service_request_exist . ' package';
                    }
                } else {
                    if ($seeker_service_request_exist)
                        $seeker_service_request_exist = $seeker_service_request_exist . ',';
                    $seeker_service_request_exist = $seeker_service_request_exist . 'package';
                }
            }

            if ($traveller_project_request_id) {
                $traveller_project = $this->TripModel->getTravellerProjectByID($traveller_project_request_id);
                $seekerSentProjectRequestAlready = $this->TripModel->checkSeekerProjectRequestSentAlready($seeker_trip_id, $traveller_project['trip']);
				
				$traveler_trip = $traveller_project['trip'];
                if ($seekerSentProjectRequestAlready['ID'] == '') {
                    $travellerProjectRequestExist = $this->TripModel->checkTravellerProjectRequestExist($seeker_trip_id, $traveller_project_request_id);
                    if ($travellerProjectRequestExist['ID'] == '') {
                        $trip_traveller_project_request = array(
                            'trip' => $seeker_trip_id,
                            'project_request' => $traveller_project_request_id,
                            'service_id' => $traveller_project_service_id,
                            'card_id' => $cardId,
                            'approved' => 0,
                            'modified' => date('Y-m-d H:i:s')
                        );
                        $this->TripModel->addTripTravellerProjectRequest($trip_traveller_project_request);
                    } else {
                        if ($service_request_exist)
                            $service_request_exist = $service_request_exist . ',';
                        $service_request_exist = $service_request_exist . ' project';
                    }
                } else {
                    if ($seeker_service_request_exist)
                        $seeker_service_request_exist = $seeker_service_request_exist . ',';
                    $seeker_service_request_exist = $seeker_service_request_exist . 'project';
                }
            }

            if ($seeker_service_request_exist){
                echo json_encode(array('msg'=>" This Seeker has already sent $seeker_service_request_exist service request to your trip, please check Menu -> Seeker Requests page for more details<br><br>"));exit;
			} else {
                if ($service_request_exist){
                    echo json_encode(array('msg'=>" You have already sent $service_request_exist service request to this seeker"));exit;
				} else {
                    $s = $this->TripModel->checkTripDetailsViaID($seeker_trip_id);
				}
                $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
                $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
                $note_reciever_id = $s['user'];
                $note_sender_id = $this->sessionObj->userid;
                $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
                $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
                $tos = $_POST['type_of_service'];

                $uD = $this->CommonMethodsModel->getUser($note_sender_id);
                if (strtolower($uD['gender']) == 'male') {
                    $nname = 'Mr. ' . $uD['first_name'];
                } elseif (strtolower($uD['gender']) == 'female') {
                    $nname = 'Ms./Mrs. ' . $uD['first_name'];
                }

                if ($tos == 'People')
                {
                    $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for people companionship, " . $nname . " (Traveler) has sent you people service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
                    $msg = "For the trip from " . $note_origin . " to " . $note_destination . ", " . $nname . "  has sent you people service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
                }
                elseif ($tos == 'Package')
                {
                    $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for package delivery, " . $nname . " (Traveler) has sent you package service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
                    $msg = "For the trip from " . $note_origin . " to " . $note_destination . ", " . $nname . "  has sent you package service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
                }
                if ($tos == 'Product')
                {
                    $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for product procurement, " . $nname . " (Traveler) has sent you product service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
                    $msg = "For the trip from " . $note_origin . " to " . $note_destination . ", " . $nname . "  has sent you product service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
                }

                $hugeData = array
                (
                    'recevier_id' => $note_reciever_id,
                    'sender_id' => $note_sender_id,
                    'message' => $message_for_user,
                    'create_date' => date("Y-m-d H:i:s"),
                    'modified_date' => date("Y-m-d H:i:s"),
                    'active' => 1
                );
                $this->TripModel->addToTravellerEnquiry($hugeData);

                $hugedata1 = array
                (
                    'user_id' => $note_reciever_id,
                    'notification_type' => 'traveller_request',
                    'notification_subject' => 'Traveller Request',
                    'notification_message' => $msg,
                    'app_redirect' => '/seeker/tripcontenders/'.$this->encrypt($seeker_trip_id).'?action=toyou&service='.$tos,
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
                echo json_encode(array('msg'=>"We have authorized this transaction, and your service request has been sent to seeker successfully.",'redirect'=>'/traveler/tripcontenders/'.$this->encrypt($traveler_trip).'?action=toyou&service='.$tos));exit;
            }
        } else {
            echo json_encode(array('msg'=>" You can't send request for your own trip."));exit;
        }
        return true;
        //exit;
    }

    /*** END This is the section for send Request to seeker's trips by traveller ***/

    public function seekerrequestsAction()
    {
        /*  $this->view->tripser = $this;
        $this->view->mytripcnt = $this->CommonMethodsModel->mytriphistorycnt($userid);*/
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        // $viewArray['upcoming_trips'] = $this->TripPlugin->getTrips('upcoming',$this->sessionObj->userrole, $this->sessionObj->userid);
        $viewArray['upcoming_trips'] = $this->TripPlugin->getTrips('request_list', $this->sessionObj->userrole, $this->sessionObj->userid);
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Dashboard',
                'title' => 'Dashboard',
                'active' => false,
                'redirect' => '/dashboard'
            ),
            array(
                'label' => 'Seeker Request',
                'title' => 'Seeker Request',
                'active' => true,
                'redirect' => false
            )
        );
        $viewArray['comSessObj'] = $this->sessionObj;

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;

    }

    public function seekerrequestscrollAction()
    {
        $this->_helper->layout->disableLayout();
        $rec_limit = 5;
        if ($_REQUEST['page'] > 1) {
            $page = $_REQUEST['page'] - 1;
            $offset = $rec_limit * $page;
        } else {
            $page = 0;
            $offset = 0;
        }
        $userid = $this->sessionObj->userid;
        $mytripcnt = $this->trip_mod->travellregdetcnt($userid);

        $mytripv = $this->trip_mod->travellregdet($userid, $offset, $rec_limit);
        $asary = '';
        if (!empty($mytripv)) {
            foreach ($mytripv as $val) {
                $hk = date("d-F-Y", strtotime($val['departure_date']));
                $servl = $this->trip_mod->travellerservicetrip($val['ID']);
                $origin_location = $this->trip_mod->getAirPort($val['origin_location']);

                $destination_location = $this->trip_mod->getAirPort($val['destination_location']);
                $asary .= '<div class="booking-item">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="cus-shadow">
                                <div class="pull-left"><i class="fa fa-calendar box-icon-big box-icon-inverse"></i></div>
                                <span class="tra-date">' . $hk . '</span>
                                </div>

                                <div class="cus-shadow">
                                <div class="pull-left"><i class="fa fa-briefcase box-icon-big box-icon-inverse"></i></div>
                                <span class="tra-date">' . $servl . '</span>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <h5 class="booking-item-title">' . $origin_location['name'] . ' (' . $origin_location['code'] . ')</h5>
                                <p class="booking-item-address"><i class="fa fa-map-marker"></i> ' . $origin_location['city'] . ', ' . $origin_location['country'] . '</p>
                                <br />
                                <h5 class="booking-item-title">' . $destination_location['name'] . '(' . $destination_location['code'] . ')</h5>
                                <p class="booking-item-address"><i class="fa fa-map-marker"></i> ' . $destination_location['city'] . ', ' . $destination_location['country'] . '</p>
                            </div>
                            <div class="col-md-3">
                                <div class="servive-block servive-block-dark-blue"><a href="' . $this->view->baseUrl() . '/travelertripcontenders"><h5>Request Sent by You</h5></a></div>
                                <div class="servive-block servive-block-dark-blue"><a href="' . $this->view->baseUrl() . '/traveller/tripcontenders/' . $val['ID'] . '"><h5>Request Sent to You</h5></a></div>
                                <div class="servive-block servive-block-dark-blue"><a href="' . $this->view->baseUrl() . '/travelertripcontenders"><h5>Profiles you shortlisted</h5></a></div>
                            </div>
                        </div>
                    </div>
                    ';
            }
        }

        $pagcnt = ($_REQUEST['page'] * 5);
        if ($mytripcnt > $pagcnt) {

            $asary .= '<a href="javascript:void(0);" onclick="javascript:TravellerrequestClinkload();" class="ladtg" sty>Load More</a>';

        }
        echo $asary;
    }

    public function mytriphistoryAction()
    {
        $userid = $this->sessionObj->userid;
        if ($_REQUEST['page'] == '') {
            $this->view->mytrip = $this->CommonMethodsModel->mytriphistory($userid);
            $this->view->tripser = $this;
        } else {
            return $this->view->mytrip = $this->CommonMethodsModel->mytriphistoryscroll($userid);
        }
    }

    public function mytriphistoryscrollAction()
    {
        $this->_helper->layout->disableLayout();
        $rec_limit = 5;
        if ($_REQUEST['page'] > 1) {
            $page = $_REQUEST['page'] - 1;
            $offset = $rec_limit * $page;
        } else {
            $page = 0;
            $offset = 0;
        }

        $userid = $this->sessionObj->userid;
        $mytripv = $this->CommonMethodsModel->mytriphistoryscroll($userid, $offset, $rec_limit);

        $this->view->mytripcnt = $this->CommonMethodsModel->mytriphistorycnt($userid);
        $aryasgn = array();
        foreach ($mytripv as $val) {
            $hk = date("d-l-F-Y", strtotime($val['departure_date']));
            $dtsp = explode("-", $hk);
            $from = date("Y-m-d", strtotime($val['departure_date']));
            $to = date("Y-m-d");
            $dfc = $this->_helper->Common->datediffcal($from, $to);
            if ($dfc < 0) {
                $shcls = "warning";
                $shmsg = "Expired Trips";
            } else {
                $shcls = "success";
                $shmsg = "Active Trips";
            }
            $aryasgn[] = array("title" => $dtsp[0], "subtitle" => $dtsp[1], "calender" => $dtsp[2] . ' ' . $dtsp[3], "classty" => $shcls, "classmsg" => $shmsg, "name" => $val['name'], "origin_location" => $val['origin_location'], "service" => $this->CommonMethodsModel->servicetrip($val['ID']));
        }
        $this->view->retval = $aryasgn;

    }

    public function viewservicetrip($id)
    {
        return $this->CommonMethodsModel->servicetrip($id);
    }

    public function getTravellerPeopleServiceNeedAction()
    {
        $viewArray = array();

        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $viewArray['trip_traveller_people'] = $this->TripModel->getTravellerPeople($tripid);
        $viewArray['trip_traveller'] = $this->TripModel->getTrip($tripid);
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)->setTerminal(true);
        echo $viewArray['trip_traveller']['total_cost'];
        return $viewModel;

    }

    public function checkTravellerPeopleServiceNeedAction()
    {

        $seeker_trip_id = $this->CommonMethodsModel->cleanQuery($_POST['seeker_trip_id']);
        $tripid = $this->CommonMethodsModel->cleanQuery($_POST['tripid']);
        $resp = $this->TripModel->checkTravellerPeopleNeed($seeker_trip_id, $tripid);
        if (count($resp) > 0) {
            echo $resp['approved'];
            exit();
        } else {
            echo 'nothing';
            exit();
        }
    }

    public function checkTravellerProjectServiceNeedAction()
    {

        $seeker_trip_id = $this->CommonMethodsModel->cleanQuery($_POST['seeker_trip_id']);
        $tripid = $this->CommonMethodsModel->cleanQuery($_POST['tripid']);
        $resp = $this->TripModel->checkTravellerProjectNeed($seeker_trip_id, $tripid);
        if (count($resp) > 0) {
            echo $resp['approved'];
            exit();
        } else {
            echo 'nothing';
            exit();
        }
    }

    public function checkTravellerPackageServiceNeedAction()
    {

        $seeker_trip_id = $this->CommonMethodsModel->cleanQuery($_POST['seeker_trip_id']);
        $tripid = $this->CommonMethodsModel->cleanQuery($_POST['tripid']);
        $resp = $this->TripModel->checkTravellerPackageNeed($seeker_trip_id, $tripid);
        if (count($resp) > 0) {
            echo $resp['approved'];
            exit();
        } else {
            echo 'nothing';
            exit();
        }
    }

    public function getTravellerPackageServiceNeedAction()
    {
        //$this->TripPlugin = $this->plugin('TripPlugin');
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $viewArray['trip_traveller_package'] = $this->TripModel->getTravellerPackage($tripid);
        $viewArray['trip_traveller'] = $this->TripModel->getTrip($tripid);
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)->setTerminal(true);
        echo $viewArray['trip_traveller']['total_cost'];
        return $viewModel;
    }

    public function getTravellerProjectServiceNeedAction()
    {
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $trip_project = $this->TripModel->getTravellerProject($tripid);
        if (!empty($trip_project)) {
            if (!empty($trip_project['task'])) {
                $trip_project_task = $this->CommonMethodsModel->getSubCatById($trip_project['task']);
                $trip_project['catname'] = $trip_project_task['catname'];
                $trip_project['subcatname'] = $trip_project_task['subcatname'];
            }
            if (!empty($trip_project['product_type'])) {
                $trip_project_task_type = $this->CommonMethodsModel->getSubCatById($trip_project['product_type']);
                $trip_project['product_catname'] = $trip_project_task_type['catname'];
                $trip_project['product_subcatname'] = $trip_project_task_type['subcatname'];
            }
        }
        $viewArray['trip_traveller_project'] = $trip_project;
        $viewArray['trip_traveller'] = $this->TripModel->getTrip($tripid);
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)->setTerminal(true);
        echo $viewArray['trip_traveller']['total_cost'];
        return $viewModel;
    }

    public function getprojectsubcatAction()
    {
        if ($_POST) {
            $subcat = $this->CommonMethodsModel->Listprojectsubcat($_POST['catid']);
            $subCatId = isset($_POST['subcatid']) ? $_POST['subcatid'] : '';
            $retv = '';
            $jk = 1;
            $retv .= '<option value=""> -- Select --</option>';
            foreach ($subcat as $vall) {
                /*$selected = (isset($subCatId) &&  $subCatId == $vall['ID'])?'selected="selected" ':'';
                if($jk==1){ $retv.='<option value="">Select</option><option '.$selected.'value="'.$vall['ID'].'">'.$vall['subcatname'].'</option>';}else{
                    $retv.='<option>'.$vall['subcatname'].'</option>';
                }
                $jk++;*/
                $selected = (isset($subCatId) && $subCatId == $vall['ID']) ? 'selected="selected" ' : '';
                $retv .= '<option ' . $selected . 'value="' . $vall['ID'] . '">' . $vall['subcatname'] . '</option>';
            }
            if ($retv == '') {
                $retv = '<option value="">Not Found</option>';
            }
            echo $retv;
            exit;
        }
    }

    public function getproductsubcatAction()
    {
        if ($_POST) {
			$proCatIds = explode(',',$_POST['product_catid']);
            $subCatId = isset($_POST['product_subcatid']) ? explode(',',$_POST['product_subcatid']) : '';
            $retv = '';
            $jk = 1;
			foreach($proCatIds as $proCatId){
				$catInfo = $this->CommonMethodsModel->Listprojectcategory($proCatId);
				$subcat = $this->CommonMethodsModel->Listprojectsubcat($proCatId);
				@$retv .= '<optgroup label="'.$catInfo[0]['catname'].'">';
				foreach ($subcat as $vall) {
					$selected = (isset($subCatId) && in_array($vall['ID'],$subCatId)) ? 'selected="selected" ' : '';
					$retv .= '<option ' . $selected . 'value="' . $vall['ID'] . '">' . $vall['subcatname'] . '</option>';
				}
				$retv .= '</optgroup>';
			}
            if ($retv == '') {
                $retv = '<option value="">Not Found</option>';
            }
            echo $retv;
            exit;
        }
    }

    public function addBecameTravellerServiceAction()
    {
        
      //print_r($_POST);die;
        $this->sessionObj->userrole = 'traveller';
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $viewArray['disabledProp'] = '';
        if ($_POST) {
            extract($_POST);
		//echo $slect_flight_key1;die;
            $exitsTrip = false;
            if (isset($travel_plan_type) && $travel_plan_type == 'existing_plan') {
                $Exist_trip_ID = $this->CommonMethodsModel->cleanQuery(($travel_plan));
                $checkIfExistTrip = $this->TripModel->getTravellerPeopleByTrip($Exist_trip_ID);
                if ($checkIfExistTrip) {
                    $exitsTrip = true;
                    $editTripUrl = '/traveller/add-people-service/' . $this->encrypt($checkIfExistTrip['trip']) . '#service';
                    echo json_encode(array('result' => 'exist', 'serviceUrl' => $editTripUrl, 'msg' => "Already a people service trip listing exists for this trip. Would you like to edit?"));
                    exit;
                }
            }
            $user_location = 0;
            if (isset($action) && $action == 'traval') {
                //echo '<pre>'; print_r($user_location); exit;
                $trip_ID = $this->TripPlugin->addTripDetail($user_location);
            }
            // echo "hi"; print_r($_POST);die;
            if (isset($action) && $action == 'service') {  
               // print_r($_POST);die;
                 $user_location = $this->TripPlugin->addTripOrginLocation();
               
		
                $arr_trip_detail = array(
                    'user_location' => $this->CommonMethodsModel->cleanQuery(isset($user_location) ? $user_location : ''),
                );

                //$tripId = $this->CommonMethodsModel->cleanQuery($_POST['tripId']);
                $tripId = $_POST['tripId'];
                $trip_ID = $tripId;
                
                
                $arr_trip_cost_detail= array(
                     'user_location' => $user_location,
                    'total_cost' => $total_cost_ajax_became,
                    'conversion_rate' =>$currency_conversion_rate_ajax_became,
                    'total_cost_currency' =>$total_cost_currency_ajax_became
                    
                );
                
                
                $this->TripModel->updateCostTrip($arr_trip_cost_detail, $tripId);


                 $verifiedIdentityStatus = $this->TripModel->checkverifiedIdentityStatus($user_id);
                if ($verifiedIdentityStatus == true) { 
                    $ticketStatus = $this->TripModel->checkTicketStatus($trip_ID);
                    if ($ticketStatus == true) {
                        $this->TripModel->setTripStatus($trip_ID, 3);
                    } else {
                        $this->TripModel->setTripStatus($trip_ID, 4);
                    }
                } else {   
                    $identityStatus = $this->TripModel->checkIdentityStatus($user_id);
                    if ($identityStatus == true) { 
                        $this->TripModel->setTripStatus($trip_ID, 2);
                    } else { 
                        
                         $this->TripModel->setTripStatus($trip_ID, 0);
                        
                    }
                }
                
                $this->TripModel->updateTrip($arr_trip_detail, $tripId);
                if (isset($service_type) && $service_type == 'people')
                   
                    $this->TripPlugin->addTravellerPeopleDetail($trip_ID);
                if (isset($service_type) && $service_type == 'package')
                    $this->TripPlugin->addTravellerPackageDetail($trip_ID);
                if (isset($service_type) && $service_type == 'project')
                    $this->TripPlugin->addTravellerProjectDetail($trip_ID);
                
               
                $this->CommonMethodsModel->setApprovalIdentities($user_id, 1);
                $user = $this->CommonMethodsModel->getUser($user_id);
                $this->sessionObj->offsetSet('user', $user);
                
               
            }
            if (isset($action) && $action == 'traval') {
               // $requestURL = 'becometraveler/service?tripId='.$this->encrypt($trip_ID)."&totalcost_people=".$this->encrypt($total_cost_ajax);
                
             if(isset($travel_trip_type)&& $travel_trip_type == '2' )   
             {
                 
           $requestURL = 'becometraveler/service?tripId='.$this->encrypt($trip_ID)."&flight_key=".$flight_key."&proposal_key=".$proposal_key."&travel_trip_type_value=".$travel_trip_type_value;
           
             }
             else
             {
          $requestURL = 'becometraveler/service?tripId='.$this->encrypt($trip_ID)."&flight_key=".$flight_key."&proposal_key=".$proposal_key."&travel_trip_type_value= ";
           
                     
             }
                echo json_encode(array('result' => 'success', 'seacrhAction' => '', 'requestURL' => $requestURL, 'msg' => " Loading..."));
                exit;
            }
            else if (isset($action) && $action == 'service') {
                
                $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($user_id);

                $hugedata2 = array('user_id' => $user_id,
                    'notification_type' => 'become_traveller',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => "Hi " . $this->sessionObj->user['first_name'] . ", Welcome to Trepr an Online Deputation Platform for all your transportation needs.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata2);
                $hugedata1 = array('user_id' => $user_id,
                    'notification_type' => 'become_traveller',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => "Refer your friends & family and earn 20 times worth reward points",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
                $hugedata3 = array('user_id' => $user_id,
                    'notification_type' => 'become_traveller',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => "You have successfully created people service trip listing. Please add package and product services to your trip and make more money in your travel.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata3);

                $hugedata4 = array
                (
                    'user_id' => $user_id,
                    'notification_type' => 'awaiting_document',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => 'We are awaiting your ID documents for verification and approval. Please upload your valid ID documents to improve your trust scale in Trepr.',
                    'app_redirect' => '/myprofile/trustVerification',
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata4);

                if(isset($travel_trip_type_value) && $travel_trip_type_value == 'roundtrip')
                    
                    
                {
                    
                    $requestURL = 'search?trip='.$this->encrypt($trip_ID).'&service='.ucfirst($service_type);
                  
                  
                //json_encode(array(array('result' =>'success','seacrhAction' => '','requestURL' => $requestURL, 'msg' => "".ucfirst($service_type)." service saved successfully. Please check status in Menu -> My Travel Needs")));
                   $array = array(
                array('result' =>'success','trip_ID' => $this->encrypt($trip_ID),'return_service' => $service_type,'msg'=>'Loading..'));
               
                echo json_encode($array);
                exit();
                    
                }else {
                    /*$requestURL = 'becometraveler/confirm?tripId='.$this->encrypt($trip_ID).'&service='.ucfirst($service_type);*/
                      $requestURL = 'search?trip='.$this->encrypt($trip_ID).'&service='.ucfirst($service_type);


                    //json_encode(array(array('result' =>'success','seacrhAction' => '','requestURL' => $requestURL, 'msg' => "".ucfirst($service_type)."service saved successfully")));
                  

					/* send become traveller email  */


				try
                {
                    $this->sendmail = $this->MailModel->sendBecomeTravellerEmail('test', 'test', 'karisdft456@gmail.com', '2');
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }


				  $array = array(
                                array('result' =>'success','seacrhAction' => $this->encrypt($trip_ID),'requestURL' => $requestURL,'msg' => "".ucfirst($service_type)." service is saved successfully. Please check status in Menu -> My Travel Needs"));

                   echo json_encode($array);
                    exit();
                }    
              
                 
            }
            else if (isset($trip_type) && $trip_type == '2') {
                $hugedata2 = array('user_id' => $this->sessionObj->userid,
                    'notification_type' => 'become_traveller',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => "Hi " . $this->sessionObj->user['first_name'] . ", Welcome to Trepr an Online Deputation Platform for all your transportation needs.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata2);
                $hugedata1 = array('user_id' => $this->sessionObj->userid,
                    'notification_type' => 'become_traveller',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => "Refer your friends & family and earn 20 times worth reward points",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
                $hugedata3 = array('user_id' => $this->sessionObj->userid,
                    'notification_type' => 'become_traveller',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => "You have successfully created package service trip listing. Please add people and product services to your trip and make more money in your travel.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata3);

                $hugedata4 = array
                (
                    'user_id' => $this->sessionObj->userid,
                    'notification_type' => 'awaiting_document',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => 'We are awaiting your ID documents for verification and approval. Please upload your valid ID documents to improve your trust scale in Trepr.',
                    'app_redirect' => '/myprofile/trustVerification',
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata4);

                echo json_encode(array('result' => 'success', 'seacrhAction' => '', 'requestURL' => '', 'trip_ID' => $this->encrypt($trip_ID), 'msg' => " People service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
            }
            else {
                $hugedata2 = array('user_id' => $this->sessionObj->userid,
                    'notification_type' => 'become_traveller',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => "Hi " . $this->sessionObj->user['first_name'] . ", Welcome to Trepr an Online Deputation Platform for all your transportation needs.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata2);
                $hugedata1 = array('user_id' => $this->sessionObj->userid,
                    'notification_type' => 'become_traveller',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => "Refer your friends & family and earn 20 times worth reward points",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
                $hugedata3 = array('user_id' => $this->sessionObj->userid,
                    'notification_type' => 'become_traveller',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => "You have successfully created product service trip listing. Please add people and package services to your trip and make more money in your travel.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata3);

                $hugedata4 = array
                (
                    'user_id' => $this->sessionObj->userid,
                    'notification_type' => 'awaiting_document',
                    'notification_subject' => 'Become a traveller',
                    'notification_message' => 'We are awaiting your ID documents for verification and approval. Please upload your valid ID documents to improve your trust scale in Trepr.',
                    'app_redirect' => '/myprofile/trustVerification',
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata4);

                echo json_encode(array('result' => 'success', 'seacrhAction' => $this->encrypt($trip_ID), 'requestURL' => '', 'trip_ID' => '', 'msg' => " People service trip listing has been created successfully. Please check status in Menu -> My Travel Plans."));
                exit;
            }
        }
        $viewArray['currency'] = $this->CommonMethodsModel->getCurrency();
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewArray['comSessObj'] = $this->sessionObj;
        $this->layout()->pageController = 'traveller';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)->setTerminal(true);
        return $viewModel;
    }

    public function changeTripStatusAction()
    {
        //print_r($_POST);exit();
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $service = $this->CommonMethodsModel->cleanQuery($_POST['service']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        $this->TripModel->changeTripStatusByTraveller($requestid, $status, $service);
        echo 1;
        exit();
    }

    public function addTravellerTripRequestByPaypalAction()
    {

        //print_r($_POST);die;

        $seeker_service_request_exist = $service_request_exist = '';
        $transactionid = $this->CommonMethodsModel->cleanQuery($_POST['pay_tran_id']);
        $payerid = $this->CommonMethodsModel->cleanQuery($_POST['payerID']);
        $tripid = $this->CommonMethodsModel->cleanQuery($_POST['seeker_trip_id']);
        $packageservice = $this->TripModel->getSeekerTripPackage($tripid);
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['traveller_package_request_id']);
        $serviceid = $this->CommonMethodsModel->cleanQuery($_POST['traveller_package_service_id']);
        //$traveller_package_request = $this->TripModel->getTravellerPackageRequestUser($requestid);
        $seeker_trip = $this->TripModel->getSeekerTrip($tripid);
        $traveller_package = $this->TripModel->getTravellerPackageByID($requestid);


        $tr_param = array(
            'pay_user' => $this->sessionObj->userid,
            'pay_to_user' => $seeker_trip['user'],
            'pay_trip_table' => 'seeker_trips',
            'pay_trip_id' => $tripid,
            'pay_to_trip_id' => $traveller_package['trip'],
            'pay_user_type' => 'seeker',
            'pay_service' => 'package',
            'pay_service_id' => $serviceid,
            'pay_request_id' => $requestid,
            'pay_card_type' => 'PayPal',
            'pay_type' => 'Hold',
            'pay_amount' => $packageservice['package_service_fee'],
            'pay_currency' => 'USD',
            'pay_trans_id' => $transactionid,
            'pay_cor_id' => $payerid,
            'pay_status' => '1',
            'pay_added' => time()
        );
        $tr_response = $this->TripModel->updatePaymants($tr_param);
        $trip_traveller_package_request = array(
            'trip' => $tripid,
            'package_request' => $requestid,
            'service_id' => $serviceid,
            'card_id' => 0,
            'approved' => 0,
            'modified' => date('Y-m-d H:i:s')
        );
        $this->TripModel->addTripTravellerPackageRequest($trip_traveller_package_request);
        $s = $this->TripModel->checkTripDetailsViaID($tripid);
        //print_r($_POST);die;
        $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
        $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
        $note_reciever_id = $s['user'];
        $note_sender_id = $this->sessionObj->userid;
        $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
        $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
        $tos = $_POST['type_of_service'];

        $uD = $this->CommonMethodsModel->getUser($note_sender_id);
        if (strtolower($uD['gender']) == 'male') {
            $nname = 'Mr. ' . $uD['first_name'];
        } elseif (strtolower($uD['gender']) == 'female') {
            $nname = 'Ms./Mrs. ' . $uD['first_name'];
        }

        if ($_POST['traveller_people_request_id'] != '') {
            $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for people companionship, " . $nname . " (Traveler) has sent you people service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
            $msg = "For the trip from " . $note_origin . " to " . $note_destination . "," . $nname . "  has sent you people service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
        } else if ($_POST['traveller_package_request_id'] != '') {
            $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for package delivery, " . $nname . " (Traveler) has sent you package service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
            $msg = "For the trip from " . $note_origin . " to " . $note_destination . "," . $nname . "  has sent you package service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
        } else if ($_POST['traveller_project_request_id'] != '') {
            $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for product procurement, " . $nname . " (Traveler) has sent you product service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
            $msg = "For the trip from " . $note_origin . " to " . $note_destination . "," . $nname . "  has sent you product service request. Please check Menu -> Traveler Requests page and respond to the request within 24 hours.";
        }

        $hugeData = array('recevier_id' => $note_reciever_id,
            'sender_id' => $note_sender_id,
            'message' => $message_for_user,
            'create_date' => date("Y-m-d H:i:s"),
            'modified_date' => date("Y-m-d H:i:s"),
            'active' => 1);
        $this->TripModel->addToTravellerEnquiry($hugeData);
        $hugedata1 = array('user_id' => $note_reciever_id,
            'notification_type' => 'request_accept',
            'notification_subject' => 'Request Accept',
            'notification_message' => $msg,
            'notification_added' => date("Y-m-d H:i:s")
        );
        $this->TripModel->addToNotifications($hugedata1);
        echo "Your request has been sent successfully";
        exit;
    }

    public function paypalpeopleSuccessPaymentTravellerAction()
    {
        //print_r($_POST);die;
        //echo "in paypal succes payment action seeker controller";exit;
        $this->TripPlugin = $this->plugin('TripPlugin');
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        // $traveller_people_request = $this->TripModel->getTravellerPeopleRequestUser($requestid);
        $seeker_trip_user = $this->TripModel->getSeekerTripUser($tripid);
        $peopleservice = $this->TripModel->getSeekerTripPeople($tripid);
        $transactionid = $this->CommonMethodsModel->cleanQuery($_POST['paymentID']);
        $payerid = $this->CommonMethodsModel->cleanQuery($_POST['payerID']);

        $traveller_people_request = $this->TripModel->getApprovedTripPeopleRequestUser($_POST['requestid']);
        $param = array(
            'pay_user' => $this->sessionObj->userid,
            'pay_to_user' => $traveller_people_request['ID'],
            'pay_trip_table' => 'trips',
            'pay_trip_id' => $tripid,
            'pay_to_trip_id' => $traveller_people_request['tripId'],
            'pay_user_type' => 'traveller',
            'pay_service' => 'people',
            'pay_service_id' => $traveller_people_request['serviceId'],
            'pay_request_id' => $requestid,
            'pay_card_type' => 'PayPal',
            'pay_type' => 'Hold',
            'pay_amount' => $peopleservice['people_service_fee'],
            'pay_currency' => 'USD',
            'pay_trans_id' => $transactionid,
            'pay_cor_id' => $payerid,
            'pay_status' => $status,
            'pay_added' => time()

        );
        $pay_id = $this->TripModel->updatePaymants($param);
        //echo $pay_id;die;
        if ($pay_id != 0) {

            $this->TripModel->approvePeopleRequest($tripid, $requestid, $status);
            $msg_status = 'success';
            $pay_id = $pay_id;
            $msg = $trip_people_request['first_name'] . " " . $trip_people_request['last_name'] . ' has been approved for your people service request.';
            $s = $this->TripModel->checkTripDetailsViaID($tripid);
            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
            $tos = $_POST['requestService'];

            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }

            if ($tos == 'people') {
                $message_for_user = "For the people companionship service request you sent to " . $nname . " (traveler) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else if ($tos == 'package') {
                $message_for_user = "For the package delivery service request you sent to " . $nname . " (traveler) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else {
                $message_for_user = "For the product procurement service request you sent to " . $nname . " (traveler) has accepted the request. Please contact " . $him_her . " for further coordination.";
            }

            $hugeData = array('user_id' => $note_reciever_id,
                'message' => $message_for_user,
                'msg_type' => 'm',
                'create_date' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToUserMessages($hugeData);
            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_accept',
                'notification_subject' => 'Request Accept',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted your people service request. Please contact him/her for further coordination.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
            //here  
        } else {
            $msg_status = 'failed';
            $msg = $trip_people_request['first_name'] . " " . $trip_people_request['last_name'] . ' has not been approved, due to payment authorization failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
        }
        echo json_encode(array(
            'msg_status' => $msg_status,
            'msg' => $msg,
            'pay_id' => $this->encrypt($pay_id)
        ));
        exit;


    }

    public function paypalpackageSuccessPaymentTravellerAction()
    {
        //print_r($_POST);die;
        //echo "in paypal succes payment action seeker controller";exit;
        $this->TripPlugin = $this->plugin('TripPlugin');
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        // $traveller_people_request = $this->TripModel->getTravellerPeopleRequestUser($requestid);
        $seeker_trip_user = $this->TripModel->getSeekerTripUser($tripid);
        $packageservice = $this->TripModel->getSeekerTripPackage($tripid);
        $transactionid = $this->CommonMethodsModel->cleanQuery($_POST['paymentID']);
        $payerid = $this->CommonMethodsModel->cleanQuery($_POST['payerID']);

        $traveller_package_request = $this->TripModel->getTripPackageRequestUser($_POST['requestid']);
        $param = array(
            'pay_user' => $this->sessionObj->userid,
            'pay_to_user' => $traveller_package_request['ID'],
            'pay_trip_table' => 'trips',
            'pay_trip_id' => $tripid,
            'pay_to_trip_id' => $traveller_package_request['tripId'],
            'pay_user_type' => 'traveller',
            'pay_service' => 'package',
            'pay_service_id' => $traveller_package_request['serviceId'],
            'pay_request_id' => $requestid,
            'pay_card_type' => 'PayPal',
            'pay_type' => 'Hold',
            'pay_amount' => $packageservice['package_service_fee'],
            'pay_currency' => 'USD',
            'pay_trans_id' => $transactionid,
            'pay_cor_id' => $payerid,
            'pay_status' => $status,
            'pay_added' => time()

        );
        $pay_id = $this->TripModel->updatePaymants($param);
        //echo $pay_id;die;
        if ($pay_id != 0) {

            $this->TripModel->approvePackageRequest($tripid, $requestid, $status);
            $msg_status = 'success';
            $pay_id = $pay_id;
            $msg = $trip_package_request['first_name'] . " " . $trip_package_request['last_name'] . ' has been approved for your package service request';
            $s = $this->TripModel->checkTripDetailsViaID($tripid);
            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
            $tos = $_POST['requestService'];

            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }

            if ($tos == 'people') {
                $message_for_user = "For the people companionship service request you sent to " . $nname . " (traveler) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else if ($tos == 'package') {
                $message_for_user = "For the package delivery service request you sent to " . $nname . " (traveler) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else {
                $message_for_user = "For the product procurement service request you sent to " . $nname . " (traveler) has accepted the request. Please contact " . $him_her . " for further coordination.";
            }

            $hugeData = array('user_id' => $note_reciever_id,
                'message' => $message_for_user,
                'msg_type' => 'm',
                'create_date' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToUserMessages($hugeData);
            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_accept',
                'notification_subject' => 'Request Accept',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted package service request. Please contact him/her for further coordination.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
            //here  
        } else {
            $msg_status = 'failed';
            $msg = $trip_package_request['first_name'] . " " . $trip_package_request['last_name'] . ' has not been approved, due to payment authorization failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
        }
        echo json_encode(array(
            'msg_status' => $msg_status,
            'msg' => $msg,
            'pay_id' => $this->encrypt($pay_id)
        ));
        exit;


    }

    public function paypalprojectSuccessPaymentTravellerAction()
    {
        //print_r($_POST);die;
        //echo "in paypal succes payment action seeker controller";exit;
        $this->TripPlugin = $this->plugin('TripPlugin');
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        // $traveller_people_request = $this->TripModel->getTravellerPeopleRequestUser($requestid);
        $seeker_trip_user = $this->TripModel->getSeekerTripUser($tripid);
        $projectservice = $this->TripModel->getSeekerTripProject($tripid);
        $transactionid = $this->CommonMethodsModel->cleanQuery($_POST['paymentID']);
        $payerid = $this->CommonMethodsModel->cleanQuery($_POST['payerID']);

        $traveller_project_request = $this->TripModel->getTripProjectRequestUser($_POST['requestid']);
        $param = array(
            'pay_user' => $this->sessionObj->userid,
            'pay_to_user' => $traveller_project_request['ID'],
            'pay_trip_table' => 'trips',
            'pay_trip_id' => $tripid,
            'pay_to_trip_id' => $traveller_project_request['tripId'],
            'pay_user_type' => 'traveller',
            'pay_service' => 'project',
            'pay_service_id' => $traveller_project_request['serviceId'],
            'pay_request_id' => $requestid,
            'pay_card_type' => 'PayPal',
            'pay_type' => 'Hold',
            'pay_amount' => $projectservice['project_service_fee'],
            'pay_currency' => 'USD',
            'pay_trans_id' => $transactionid,
            'pay_cor_id' => $payerid,
            'pay_status' => $status,
            'pay_added' => time()

        );
        $pay_id = $this->TripModel->updatePaymants($param);
        //echo $pay_id;die;
        if ($pay_id != 0) {

            $this->TripModel->approveProjectRequest($tripid, $requestid, $status);
            $msg_status = 'success';
            $pay_id = $pay_id;
            $msg = $trip_project_request['first_name'] . " " . $trip_project_request['last_name'] . ' has been approved for your project service request';
            $s = $this->TripModel->checkTripDetailsViaID($tripid);
            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
            $tos = $_POST['requestService'];

            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }

            if ($tos == 'people') {
                $message_for_user = "For the people companionship service request you sent to " . $nname . " (traveler) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else if ($tos == 'package') {
                $message_for_user = "For the package delivery service request you sent to " . $nname . " (traveler) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else {
                $message_for_user = "For the product procurement service request you sent to " . $nname . " (traveler) has accepted the request. Please contact " . $him_her . " for further coordination.";
            }

            $hugeData = array('user_id' => $note_reciever_id,
                'message' => $message_for_user,
                'msg_type' => 'm',
                'create_date' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToUserMessages($hugeData);
            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_accept',
                'notification_subject' => 'Request Accept',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted product service request. Please contact him/her for further coordination.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
            //here  
        } else {
            $msg_status = 'failed';
            $msg = $trip_project_request['first_name'] . " " . $trip_project_request['last_name'] . ' has not been approved, due to payment authorization failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
        }
        echo json_encode(array(
            'msg_status' => $msg_status,
            'msg' => $msg,
            'pay_id' => $this->encrypt($pay_id)
        ));
        exit;
    }
	
	public function serviceRequestCancelAction(){
		if($_POST['from'] == 'traveller'){
			$this->TripModel->changeTripStatusByTraveller($_POST['id'],5,$_POST['type']);
		} else if($_POST['from'] == 'seeker'){
			$this->TripModel->changeTripStatusBySeeker($_POST['id'],5,$_POST['type']);
		}
		echo json_encode(array('msg'=>'success'));exit;
	}
	public function checkticketstatusAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');

		$tripId = $this->decrypt($this->CommonMethodsModel->cleanQuery($_REQUEST['trip_id']));
		$trip = $this->TripModel->getTrip($tripId, $this->sessionObj->userrole);
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewArray['tripStatus'] = isset($trip['trip_status'])?$trip['trip_status']:0;
		$viewArray['trip_id_number'] = isset($trip['trip_id_number'])?$trip['trip_id_number']:0;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)->setTerminal(true);
        return $viewModel;
    }
	
	 public function checkrewardsAction()
       { 

        $reward_details = $this->CommonMethodsModel->getRewards($_REQUEST['user_id']);
        
        if ($reward_details) {
            echo $reward_details['balance'];
            exit();
        } else {
            echo 0;
            exit();
        }
    }
    
}



