<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager;
use Zend\View\Helper\ServerUrl;
use Zend\Uri\Uri;
use Zend\Mvc\MvcEvent;

/*require_once(ACTUAL_ROOTPATH . 'hybridauth/Hybrid/Auth.php');*/
require_once(ACTUAL_ROOTPATH.'hyb/src/autoload.php');

require_once(ACTUAL_ROOTPATH . "Upload/src/Upload.php");

use Hybridauth\Exception\Exception;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
use Hybridauth\Storage\Session;

class IndexController extends AbstractActionController
{
    var $sessionObj;
    var $CommonPlugin;
    var $GoogleplusPlugin;
    var $siteConfigs;
    var $basePath;
    var $hybridConfig;
    var $CommonMethodsModel;
    protected $serviceLocator;

    public function __construct($data = false)
    {
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if ($data['models'] && is_array($data['models'])) {
            foreach ($data['models'] as $model) {
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }

        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->basePath = $this->siteConfigs['mail_url'];
       // $this->hybridConfig = include(ACTUAL_ROOTPATH . 'hybridauth/config.php');
         $this->hybridConfig = include(ACTUAL_ROOTPATH . 'hyb/src/config.php');
        $this->workCategory = array(
            '1' => 'Obtain, Travel & Deliver',
            '2' => 'Arrange, Assist & Accomplish',
            '3' => 'Personal Services',
            '4' => 'Local Travel Services',
            '5' => 'Auto & Device Services',
            '6' => 'Home & Surroundings',
            '7' => 'Entertainment & Leisure'
        );

    }

    public function indexAction()
    {
        if (isset($this->sessionObj->userid) && $this->sessionObj->userid != '') {
            $isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
            if (!$isUserLoggedIn) {
                return $this->redirect()->toUrl('http://trepr.co.uk/logout');
            } else {
                $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                if ($actual_link == "http://trepr.co.uk/" || $actual_link == "https://trepr-dev.com/") {
                    return $this->redirect()->toUrl('http://trepr.co.uk/' . md5($this->sessionObj->user['uniqueID']));
                }
            }
        }
        /* $this->CommonPlugin = $this->plugin('CommonPlugin');
         echo $this->CommonPlugin->decrypt('ajdDay9rV0RmSHFyUTExNjBaTWtxdz09');exit;*/
        $viewArray = array(
            'countries' => $this->CommonMethodsModel->getCountries(),
            'currency' => $this->CommonMethodsModel->getCurrencies(),
            'notifications' => $this->CommonMethodsModel->getNotifications($this->sessionObj->userid),
            'sessionObj' => $this->sessionObj,
            'flashMessagesForConfirmation' => $this->flashMessenger()->getMessages(),

        );

        if (isset($_GET['referrer']) && $_GET['referrer']) {
            $this->sessionObj->user_referrer = $_GET['referrer'];
        }
        if (isset($_GET['refTo']) && $_GET['refTo']) {
            $this->sessionObj->user_ref_to = base64_decode($_GET['refTo']);
        }
        //$isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
        $viewArray['notification'] = $this->CommonMethodsModel->getLatestNotifications($this->sessionObj->userid);
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(true);
        return $viewModel;
    }
    
    public function indexnewdesignAction()
    {
        if (isset($this->sessionObj->userid) && $this->sessionObj->userid != '') {
            $isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
            if (!$isUserLoggedIn) {
                return $this->redirect()->toUrl('http://trepr.co.uk/logout');
            } else {
                $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                if ($actual_link == "http://trepr.co.uk/" || $actual_link == "https://trepr-dev.com/") {
                    return $this->redirect()->toUrl('http://trepr.co.uk/' . md5($this->sessionObj->user['uniqueID']));
                }
            }
        }
        /* $this->CommonPlugin = $this->plugin('CommonPlugin');
         echo $this->CommonPlugin->decrypt('ajdDay9rV0RmSHFyUTExNjBaTWtxdz09');exit;*/
        $viewArray = array(
            'countries' => $this->CommonMethodsModel->getCountries(),
            'currency' => $this->CommonMethodsModel->getCurrencies(),
            'notifications' => $this->CommonMethodsModel->getNotifications($this->sessionObj->userid),
            'sessionObj' => $this->sessionObj,
            'flashMessagesForConfirmation' => $this->flashMessenger()->getMessages(),

        );

        if (isset($_GET['referrer']) && $_GET['referrer']) {
            $this->sessionObj->user_referrer = $_GET['referrer'];
        }
        if (isset($_GET['refTo']) && $_GET['refTo']) {
            $this->sessionObj->user_ref_to = base64_decode($_GET['refTo']);
        }
        //$isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
        $viewArray['notification'] = $this->CommonMethodsModel->getLatestNotifications($this->sessionObj->userid);
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(true);
        return $viewModel;
    }

    public function switchuserAction()
    {
        $userrole = $this->params('userrole');
        $goto = $this->CommonMethodsModel->cleanQuery(isset($_GET['goto']) ? $_GET['goto'] : '');
        if (empty($goto)) $goto = $userrole;

        $this->sessionObj->offsetSet('userrole', $userrole);
        if ($userrole == 'seeker') {
            $this->sessionObj->searchfor = 'traveller';
        } else {
            $this->sessionObj->searchfor = 'seeker';
        }
        return $this->redirect()->toRoute($goto);
    }

    public function searchAction()
    {

        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => ucfirst($this->sessionObj->userrole),
                'title' => ucfirst($this->sessionObj->userrole),
                'active' => false,
                'redirect' => '/' . $this->sessionObj->userrole
            ),
            array(
                'label' => 'Search',
                'title' => 'Search',
                'active' => false,
                'redirect' => false //'/search'
            )
        );
        if (isset($_GET['trip']) && !empty($_GET['trip']) && isset($_GET['service'])) {
            $tripId = $this->decrypt($_GET['trip']);
            $tripDetails = $this->TripModel->getTrip($tripId, $this->sessionObj->userrole);
            if (!empty($tripDetails)) {

                $origin_location = $this->TripModel->getAirPort($tripDetails['origin_location']);

                $destination_location = $this->TripModel->getAirPort($tripDetails['destination_location']);
                $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $origin_location_country_code = isset($origin_location['country_code']) ? $origin_location['country_code'] : '';
                $destination_location_country_code = isset($destination_location['country_code']) ? $destination_location['country_code'] : '';
                $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                $date_flexibility = isset($tripDetails['date_flexibility']) ? $tripDetails['date_flexibility'] : '';
                $viewArray['origin_location_country_code'] = $origin_location_country_code;
                $viewArray['destination_location_country_code'] = $destination_location_country_code;
                $departure_date = isset($tripDetails['departure_date']) ? $tripDetails['departure_date'] : '';
                $origin_id = isset($tripDetails['origin_location']) ? $tripDetails['origin_location'] : '';
                $destination_id = isset($tripDetails['destination_location']) ? $tripDetails['destination_location'] : '';
                $_POST['origin'] = $origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name . '(' . $origin_location_code . ')';
                $_POST['destination'] = $destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name . '(' . $destination_location_code . ')';;
                $_POST['origin_id'] = $origin_id;
                $_POST['destination_id'] = $destination_id;
                $_POST['origin_iata_code'] = $origin_location_code;
                $_POST['destination_iata_code'] = $destination_location_code;
                $_POST['service'] = strtolower($_GET['service']);
                $_POST['date'] = date('m/d/Y', strtotime($departure_date));
                $_POST['date_flexibility'] = $date_flexibility;

            }
        }
        if ($_POST) {
            //print_r($_POST);die;
            //echo "hello";
            if (isset($_POST['days'])) {
                $days = explode('-', $_POST['days']);
            }
            //print_r($days);
            $origin = $this->CommonMethodsModel->cleanQuery(isset($_POST['origin']) ? $_POST['origin'] : '');
            $viewArray = array();
            $destination = $this->CommonMethodsModel->cleanQuery(isset($_POST['destination']) ? $_POST['destination'] : '');
            $locationType = $this->CommonMethodsModel->cleanQuery(isset($_POST['locationType']) ? $_POST['locationType'] : '');
            $date = $this->CommonMethodsModel->cleanQuery(isset($_POST['date']) ? $_POST['date'] : '');
            $days = $this->CommonMethodsModel->cleanQuery(isset($_POST['days']) ? $days : '');
            $origin_id = $this->CommonMethodsModel->cleanQuery(isset($_POST['origin_id']) ? $_POST['origin_id'] : '');
            $destination_id = $this->CommonMethodsModel->cleanQuery(isset($_POST['destination_id']) ? $_POST['destination_id'] : '');

            $origin_iata_code = $this->CommonMethodsModel->cleanQuery(isset($_POST['origin_iata_code']) ? $_POST['origin_iata_code'] : '');
            $destination_iata_code = $this->CommonMethodsModel->cleanQuery(isset($_POST['destination_iata_code']) ? $_POST['destination_iata_code'] : '');

            $looking_for = $this->CommonMethodsModel->cleanQuery(isset($_POST['looking_for']) ? $_POST['looking_for'] : 'traveller');
            $search_type = $this->CommonMethodsModel->cleanQuery(isset($_POST['search_type']) ? $_POST['search_type'] : '');
            $service = $this->CommonMethodsModel->cleanQuery(isset($_POST['service']) ? $_POST['service'] : '');
            $radius = $this->CommonMethodsModel->cleanQuery(isset($_POST['radius']) ? $_POST['radius'] : '');
            $airline_name = $this->CommonMethodsModel->cleanQuery(isset($_POST['airline_name']) ? $_POST['airline_name'] : '');
            $flight_number = $this->CommonMethodsModel->cleanQuery(isset($_POST['flight_number']) ? $_POST['flight_number'] : '');
            $current_latitude = $this->CommonMethodsModel->cleanQuery(isset($_POST['current_latitude']) ? $_POST['current_latitude'] : '');
            $current_longitude = $this->CommonMethodsModel->cleanQuery(isset($_POST['current_longitude']) ? $_POST['current_longitude'] : '');
            $noofstops = '';
            if (isset($_POST['noofstops'])) {
                $noofstops = $this->CommonMethodsModel->cleanQuery(isset($_POST['noofstops']) ? $_POST['noofstops'] : '');
            }
            $origin_route_code = $this->CommonMethodsModel->cleanQuery(isset($_POST['origin_route']) ? $_POST['origin_route'] : '');
            $destination_route_code = $this->CommonMethodsModel->cleanQuery(isset($_POST['destination_route_code']) ? $_POST['destination_route_code'] : '');
            $origin_route = $this->CommonMethodsModel->cleanQuery(isset($_POST['origin_route']) ? $_POST['origin_route'] : '');
            $destination_route = $this->CommonMethodsModel->cleanQuery(isset($_POST['destination_route']) ? $_POST['destination_route'] : '');
            $sortBy = $this->CommonMethodsModel->cleanQuery(isset($_POST['sortBy']) ? $_POST['sortBy'] : '');
            $package_weight = $this->CommonMethodsModel->cleanQuery(isset($_POST['package_weight']) ? $_POST['package_weight'] : '');
            $package_worth = $this->CommonMethodsModel->cleanQuery(isset($_POST['package_worth']) ? $_POST['package_worth'] : '');
            $task = $this->CommonMethodsModel->cleanQuery(isset($_POST['task']) ? $_POST['task'] : '');
            $projsubcat = $this->CommonMethodsModel->cleanQuery(isset($_POST['projsubcat']) ? $_POST['projsubcat'] : '');
            $noofpassanger = $this->CommonMethodsModel->cleanQuery(isset($_POST['noofpassanger']) ? $_POST['noofpassanger'] : '');
            $ticket_status = $this->CommonMethodsModel->cleanQuery(isset($_POST['ticket_status']) ? $_POST['ticket_status'] : '');

            $routeSearch = false;
            if (!empty($origin_route)) $routeSearch = true;


            $catSearch = false;
            if (!empty($task)) $catSearch = true;
            if (!empty($projsubcat)) $catSearch = true;

            $arr_filters = array(
                'origin' => $origin,
                'destination' => $destination,
                'date' => $date,
                'days' => $days,
                'service' => $service,
                'looking_for' => $looking_for,
                'search_type' => $search_type,
                'radius' => $radius,
                'locationType' => $locationType,
                'airline_name' => $airline_name,
                'flight_number' => $flight_number,
                'current_latitude' => $current_latitude,
                'current_longitude' => $current_longitude,
                'origin_id' => $origin_id,
                'destination_id' => $destination_id,
                'user' => $this->sessionObj->offsetGet('userid'),
                'noofstops' => $noofstops,
                'origin_route_code' => $origin_route_code,
                'destination_route_code' => $destination_route_code,
                'routeSearch' => $routeSearch,
                'sortBy' => $sortBy,
                'package_worth' => $package_worth,
                'package_weight' => $package_weight,
                'task' => $task,
                'projsubcat' => $projsubcat,
                'catSearch' => $catSearch,
                'noofpassanger' => $noofpassanger,
                'ticket_status' => $ticket_status,
                'origin_iata_code' => $origin_iata_code,
                'destination_iata_code' => $destination_iata_code,
            );

            if ($search_type == 'ajax') {
            } else {
                $viewArray['origin'] = $origin;
                $viewArray['destination'] = $destination;
                $viewArray['service'] = $service;
                $viewArray['looking_for'] = $looking_for;
                $viewArray['date'] = $date;
                $viewArray['origin_id'] = $origin_id;
                $viewArray['destination_id'] = $destination_id;
                $viewArray['service'] = $service;
                $viewArray['origin_iata_code'] = $origin_iata_code;
                $viewArray['destination_iata_code'] = $destination_iata_code;
            }

            if ($search_type != 'ajax')
                $arrLatLangValues = $this->getLatLong($origin);
            $date = date('Y-m-d H:i:s', strtotime($date));

            $trips = $this->TripModel->getTrips($origin, $destination, $date);

            if ($service == '' || $service == '0')
                $service = 'people';
            if ($service == 'people' && $looking_for == 'traveller') {

                if ($search_type == 'ajax') {

                    $travellers = $this->TripModel->getTravellers($arr_filters);
                    //print_r($travellers);
                    $arrMoreInfo = array();
                    if ($travellers) {
                        foreach ($travellers as $trip) {

                            $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
                            $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
                            $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                            $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                            $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                            $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                            $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                            $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                            $arrString = '<p class="thumb-list-item-desciption">Travel Date:' . $trip['departure_date'] . '</p>';
                            $arrString .= '<p class="thumb-list-item-desciption"> Flight Routesss: ' . $trip['trip_route'] . '</p>';
                            $persons_travelling = (isset($trip['persons_travelling']) && $trip['persons_travelling'] == 1) ? 'Alone' : $trip['persons_travelling'];
                            $arrString .= '<p class="thumb-list-item-desciption">Total No of Travellers: ' . $persons_travelling . '</p>';
                            $arrMoreInfo[$trip['ID']] = $arrString;
                        }
                    }
                    // print_r($travellers);die;
                    $checkIdentityStatus = $this->TripModel->checkIdentityStatus($this->sessionObj->userid);
                    $data = array('service' => 'People', 'userrole' => $this->sessionObj->userrole, 'users' => $travellers, 'arrMoreInfo' => $arrMoreInfo, 'checkIdentityStatus' => $checkIdentityStatus);
                    echo json_encode($data);
                    exit;
                }

            } elseif ($service == 'package' && $looking_for == 'traveller') {

                if ($search_type == 'ajax') {
                    $travellers = $this->TripModel->getTravellersForPackage($arr_filters);
                    $arrMoreInfo = array();
                    if ($travellers) {
                        foreach ($travellers as $trip) {
                            $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
                            $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
                            $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                            $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                            $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                            $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                            $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                            $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                            $arrString = '<p class="thumb-list-item-desciption">Travel Date: ' . $trip['departure_date'] . '</p>';
                            $arrString .= '<p class="thumb-list-item-desciption"> Flight Route: ' . $trip['trip_route'] . '</p>';
                            $persons_travelling = '';
                            $arrString .= '<p class="thumb-list-item-desciption">Weight to adapt: ' . $trip['weight_carried'] . ' Kgs</p>';
                            $arrString .= '<p class="thumb-list-item-desciption">Worth to adapt: ' . $trip['item_worth'] . '</p>';
                            $arrMoreInfo[$trip['ID']] = $arrString;

                        }
                    }
                    $checkIdentityStatus = $this->TripModel->checkIdentityStatus($this->sessionObj->userid);
                    $data = array('service' => 'Package', 'userrole' => $this->sessionObj->userrole, 'users' => $travellers, 'arrMoreInfo' => $arrMoreInfo, 'checkIdentityStatus' => $checkIdentityStatus);
                    echo json_encode($data);
                    exit;
                }
            } elseif ($service == 'project' && $looking_for == 'traveller') {
                if ($search_type == 'ajax') {
                    $travellers = $this->TripModel->getTravellerProjects($arr_filters);
                    $arrMoreInfo = array();
                    if ($travellers) {
                        foreach ($travellers as $trip) {
                            $trip_project_task = $this->CommonMethodsModel->getSubCatById($trip['task']);
                            $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
                            $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
                            $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                            $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                            $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                            $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                            $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                            $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                            $arrString = '<p class="thumb-list-item-desciption"> Travel Date: ' . $trip['departure_date'] . '</p>';
                            $arrString .= ' <p class="thumb-list-item-desciption">Flight Route: ' . $trip['trip_route'] . '</p>';
                            $persons_travelling = '';
                            $arrString .= '<p class="thumb-list-item-desciption">Work category: ' . $trip_project_task['catname'] . '-' . $trip_project_task['subcatname'] . '</p>';
                            $arrMoreInfo[$trip['ID']] = $arrString;

                        }
                    }
                    $checkIdentityStatus = $this->TripModel->checkIdentityStatus($this->sessionObj->userid);
                    $data = array('service' => 'Product', 'userrole' => $this->sessionObj->userrole, 'users' => $travellers, 'arrMoreInfo' => $arrMoreInfo, 'checkIdentityStatus' => $checkIdentityStatus);
                    echo json_encode($data);
                    exit;
                } else {
                    $travellers = $this->TripModel->getTravellerProjects($arr_filters);
                    $viewArray['travellers'] = json_encode($travellers);

                }
            } elseif ($service == 'people' && $looking_for == 'seeker') {
                if ($search_type == 'ajax') {

                    $seekers = $this->TripModel->getSeekersPeopleForSearch($arr_filters);
                    // print_r($seekers);
                    $arrMoreInfo = array();
                    if ($seekers) {
                        foreach ($seekers as $trip) {
                            //print_r($trip);
                            $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
                            $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
                            $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                            $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                            $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                            $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                            $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                            $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                            $arrString = '<p class="thumb-list-item-desciption">Exact Date : ' . $trip['departure_date'] . '</p>';
                            $arrString .= '<p class="thumb-list-item-desciption"> Flight Route: ' . $trip['trip_route'] . '</p>';
                            $passengers = (isset($trip['passengers']) && $trip['passengers'] == 1) ? 'Alone' : $trip['passengers'];
                            $arrString .= '<p class="thumb-list-item-desciption">No of passengers: ' . $passengers . '</p>';
                            $arrMoreInfo[$trip['ID']] = $arrString;

                        }
                    }
                    $checkIdentityStatus = $this->TripModel->checkIdentityStatus($this->sessionObj->userid);
                    $data = array('service' => 'People', 'userrole' => $this->sessionObj->userrole, 'users' => $seekers, 'arrMoreInfo' => $arrMoreInfo, 'checkIdentityStatus' => $checkIdentityStatus);
                    echo json_encode($data);
                    exit;
                }
            } elseif ($service == 'package' && $looking_for == 'seeker') {
                //$trips = $this->TripModel->getTrips($origin, $destination, $date);
                //print_r($_POST);die;
                //$strips=$this->TripModel->getSeekersTrips($_POST['origin_iata_code'],$_POST['destination_iata_code'],$_POST['project_start_date'],$_POST['project_end_date'],$this->sessionObj->userid);
                if ($search_type == 'ajax') {
                    $seekers = $this->TripModel->getSeekersPackageForSearch($arr_filters);
                    $arrMoreInfo = array();
                    if ($seekers) {
                        foreach ($seekers as $trip) {
                            $arrString = '<p class="thumb-list-item-desciption">Shipping Date : ' . $trip['departure_date'] . '</p>';
                            $arrString .= '<p class="thumb-list-item-desciption">Weight to carry: ' . $trip['total_weight'] . ' Kgs</p>';
                            $arrString .= '<p class="thumb-list-item-desciption">Worth to carry: ' . $trip['total_worth'] . '</p>';
                            $arrMoreInfo[$trip['ID']] = $arrString;
                        }
                    }
                    $checkIdentityStatus = $this->TripModel->checkIdentityStatus($this->sessionObj->userid);
                    $data = array('service' => 'Package', 'userrole' => $this->sessionObj->userrole, 'users' => $seekers, 'arrMoreInfo' => $arrMoreInfo, 'checkIdentityStatus' => $checkIdentityStatus);
                    echo json_encode($data);
                    exit;
                }
            } elseif ($service == 'project' && $looking_for == 'seeker') {
                if ($search_type == 'ajax') {
                    $seekers = $this->TripModel->getSeekersProjectForSearch($arr_filters);
                    $arrMoreInfo = array();
                    if ($seekers) {
                        foreach ($seekers as $trip) {
                            $workCat = ($trip['work_category'] == 1) ? 'Choose products and ship worldwide' : 'Personal concierge for your tasks';
                            $arrString = '<p class="thumb-list-item-desciption">Delivery Date or Date Range : ' . $trip['departure_date'] . '</p>';
                            $arrString .= '<p class="thumb-list-item-desciption">Work category: ' . $workCat . '</p>';
                            $arrString .= '<p class="thumb-list-item-desciption">Task Reward: --</p>';
                            $arrMoreInfo[$trip['ID']] = $arrString;
                        }
                    }
                    $checkIdentityStatus = $this->TripModel->checkIdentityStatus($this->sessionObj->userid);
                    $data = array('service' => 'Product', 'userrole' => $this->sessionObj->userrole, 'users' => $seekers, 'arrMoreInfo' => $arrMoreInfo, 'checkIdentityStatus' => $checkIdentityStatus);
                    echo json_encode($data);
                    exit;
                }
            }
        }
        $viewArray['map_center_lat'] = isset($arrLatLangValues['latitude']) ? $arrLatLangValues['latitude'] : '';
        $viewArray['map_center_long'] = isset($arrLatLangValues['longitude']) ? $arrLatLangValues['longitude'] : '';
        $looking_for = $this->CommonMethodsModel->cleanQuery(isset($looking_for) ? $looking_for : 'traveller');
        $viewArray['airlines'] = $this->TripModel->getAvailableAirlines($looking_for);
        $viewArray['projectcat'] = $this->CommonMethodsModel->Listprojectcategory();
        $viewArray['comSessObj'] = $this->sessionObj;
        $this->layout()->pageFunction = 'search';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function decrypt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        /*pls set your unique hashing key*/
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        /* hash*/
        $key = hash('sha256', $secret_key);
        /* iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning*/
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        /*decrypt the given text/string/number*/
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    public function getLatLong($address)
    {
        if (!empty($address)) {

            $formattedAddr = str_replace(' ', '+', $address);

            $geocodeFromAddr = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . $formattedAddr . '&sensor=false');
            $output = json_decode($geocodeFromAddr);

            $data['latitude'] = isset($output->results[0]->geometry->location->lat) ? $output->results[0]->geometry->location->lat : '';
            $data['longitude'] = isset($output->results[0]->geometry->location->lng) ? $output->results[0]->geometry->location->lng : '';

            if (!empty($data)) {
                return $data;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function logoutAction()
    {
        if ($this->sessionObj->offsetGet('userid')) {
            $this->sessionObj->offsetSet('userid', '');
            $this->sessionObj->offsetSet('login_try_email_exist', '');
            $this->sessionObj->offsetSet('social_login_signup', '');
            $this->sessionObj->offsetSet('userrole', '');
            $this->sessionObj->offsetSet('user_rating', '');
            $this->sessionObj->offsetSet('bt', '');
        }
        if ($this->sessionObj->offsetGet('new_userid')) {
            //$this->sessionObj->offsetSet('new_userid','');
            $this->sessionObj->offsetSet('social_login_signup', '');
            $this->sessionObj->offsetSet('social_login_signup_first_form', '0');
            $this->sessionObj->offsetSet('social_login_signup_photo_form', '0');
            $this->sessionObj->offsetSet('social_login_signup_phone_form', '0');
            $this->sessionObj->offsetSet('bt', '');
        }
        return $this->redirect()->toRoute('home');
    }

    public function checkuseremailAction()
    {
        $from = $_GET['from'];
        if ($from == 'signup') {
            $checkUserEmailExists = $this->CommonMethodsModel->checkUserEmailExists($_GET['email_address']);
            if ($checkUserEmailExists === false) {
                echo 'true';
                exit;
            } else {
                echo 'false';
                exit;
            }
        } else {
            $email_address = $_GET['reset_email'];
            if ($email_address == '')
                $email_address = $_GET['reset_password_email'];
            $checkUserEmailExists = $this->CommonMethodsModel->checkUserEmailExists($email_address);
            if ($checkUserEmailExists === false) {
                echo 'false';
                exit;
            } else {
                echo 'true';
                exit;
            }
        }
    }

    public function signupOldAction()
    {
        //print_r($refUserId);die;
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        if ($_POST) {
            $type = '1';
            $middle_name = "";
            $active = '0';
            $dob = strtotime($_POST['dob']);

            //$refUserId = 0;
            if (isset($_POST['referrer']) && $_POST['referrer'] != '') {
                $referredEmail = base64_decode($_GET['refTo']);
                $uKey = $this->CommonMethodsModel->cleanQuery($_POST['referrer']);
                $refUser = $this->CommonMethodsModel->getUserByKey($uKey);

                if ($refUser) {
                    //$userRefExist = $this->CommonMethodsModel->checkInReferredEmails($refUser['ID'],$this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])));
                    $userRefExist = $this->CommonMethodsModel->checkInReferredEmails(base64_decode($_POST['senderID']), $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])));
                    //print_r($userRefExist);die;
                    if ($userRefExist && $referredEmail == $_POST['email']) {
                        $refUserId = base64_decode($_POST['senderID']);//$refUser['ID'];

                    }
                    $refUserId = base64_decode($_POST['senderID']);
                }
            } else {
                $refUserId = 0;
            }
            $dob = ($_POST['dob'] != '') ? strtotime($_POST['dob']) : '';
            if ($dob != '') {
                $dobYear = date("Y", $dob);
                $now = date("Y");
                $age = $now - $dobYear;
            }
            $user = array(
                "first_name" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['first_name']))),
                "middle_name" => $this->CommonMethodsModel->cleanQuery($middle_name),
                "last_name" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['last_name']))),
                "email_address" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])),
                "password" => md5($_POST['password']),
                "user_referrer" => $refUserId,
                "user_key" => md5($this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])) . '_' . time()),
                "dob" => $dob,
                "age" => $age,
                "gender" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['gender']))),
                "subscribe" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['subscribe'])),
                "location" => $this->CommonMethodsModel->cleanQuery($_POST['location']),
                "active" => $this->CommonMethodsModel->cleanQuery($active),
                "created" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s')),
                "modified" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s'))
            );

            $min = strtotime('+18 years', $dob);
            if (time() < $min) {
                echo 'You must be above 18 years to register!@SPLIT@EmailExist';
                exit;
            }
            $userid = $this->CommonMethodsModel->addUsersUser($user);
            $this->sessionObj->social_login_signup = '0';
            if ($userid != 'emailExist') {
                if ($user['user_referrer'] != 0) {
                    $arrRefDet = array(
                        'user_id' => $user['user_referrer'],
                        'invited_email' => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])),
                        'signed_user_id' => $userid
                    );
                    $this->CommonMethodsModel->updateSignedUpStatus($arrRefDet);
                }

                //$this->sessionObj->offsetSet('userid',$userid);
                $enctuser = $this->CommonPlugin->encrypt($userid);

                try
                {
                    $this->sendmail = $this->MailModel->sendwelcomeMail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $enctuser);
                    $this->sendmail = $this->MailModel->sendVerifyIdMailToUser($_POST['first_name'], $_POST['last_name'], $_POST['email'], $enctuser);
                    $this->sendmail = $this->MailModel->sendVerifyUserMailId($_POST['first_name'], $_POST['last_name'], $_POST['email'], base64_encode($userid));
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }

                $userdetail = $this->CommonMethodsModel->getUser($userid);
                //$this->sessionObj->offsetSet('user',$userdetail);
                $first_name = strtoupper($userdetail['first_name']);
                if ($userdetail['image'])
                    $img = $this->basePath . '/' . $userdetail['image'];
                else
                    $img = $this->basePath . '/' . 'default_user.png';
                echo "Your account has been created successfully.@SPLIT@" . '<img class="hidden-xs origin round" width="40" height="40" src="' . $img . '" alt="' . $user['first_name'] . '" /> Hi, ' . $user['first_name'];
                exit;
            } else {
                echo 'Email ID already exists!@SPLIT@EmailExist';
                exit;
            }
        }
    }

    public function signupAction()
    {
        //print_r($_POST);die;
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        if ($_POST) {
            $type = '1';
            $middle_name = "";
            $active = '0';
            $dob = strtotime($_POST['dob']);
            if (isset($_POST['referrer']) && $_POST['referrer'] != '') {
                $referredEmail = base64_decode($_GET['refTo']);
                $uKey = $this->CommonMethodsModel->cleanQuery($_POST['referrer']);
                $refUser = $this->CommonMethodsModel->getUserByKey($uKey);

                if ($refUser) {
                    $userRefExist = $this->CommonMethodsModel->checkInReferredEmails(base64_decode($_POST['senderID']), $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])));
                    if ($userRefExist && $referredEmail == $_POST['email']) {
                        $refUserId = base64_decode($_POST['senderID']);//$refUser['ID'];
                    }
                    $refUserId = base64_decode($_POST['senderID']);
                }
            } else {
                $refUserId = 0;
            }
            $dob = ($_POST['dob'] != '') ? strtotime($_POST['dob']) : '';
            if ($dob != '') {
                $dobYear = date("Y", $dob);
                $now = date("Y");
                $age = $now - $dobYear;
            }
            if (isset($_POST['location'])) {
                $as = explode(',', $_POST['location']);
                $res = array_slice($as, -3, 3, true);
                $mylocation = implode($res, ',');
                $addresss = '' . $_POST['location'] . '';
                $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($addresss) . '&sensor=false');
                $geo = json_decode($geo, true);
                if ($geo['status'] = 'OK') {
                    $latitude = $geo['results'][0]['geometry']['location']['lat'];
                    $longitude = $geo['results'][0]['geometry']['location']['lng'];
                    $timestamp = time();
                    $timezoneAPI = "https://maps.googleapis.com/maps/api/timezone/json?location=" . $latitude . "," . $longitude . "&sensor=false&timestamp=" . $timestamp . "";
                    $response = file_get_contents($timezoneAPI);
                    $response = json_decode($response);
                    $myzone = $response->timeZoneId;
                }
            }
            $user = array(
                "first_name" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['first_name']))),
                "middle_name" => $this->CommonMethodsModel->cleanQuery($middle_name),
                "last_name" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['last_name']))),
                "email_address" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])),
                "password" => md5($_POST['password']),
                "user_referrer" => $refUserId,
                "user_key" => md5($this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])) . '_' . time()),
                "dob" => $dob,
                "age" => $age,
                "gender" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['gender']))),
                "subscribe" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['subscribe'])),
                "location" => $this->CommonMethodsModel->cleanQuery($mylocation),
                "active" => $this->CommonMethodsModel->cleanQuery($active),
                "created" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s')),
                "modified" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s')),
                "timezone" => $this->CommonMethodsModel->cleanQuery($myzone)
            );

            $min = strtotime('+18 years', $dob);
            if (time() < $min) {
                //echo 'Your age should have above 18 to complete signup process!@SPLIT@EmailExist';exit;
                echo 'You must be above 18 years to register!@SPLIT@UnderAge';
                exit;
            } else {
                $userid = $this->CommonMethodsModel->addUsersUser($user);
                $unikID = strtolower(str_replace(' ', '', $_POST['first_name'] . $_POST['last_name'] . decoct($userid)));
                $this->CommonMethodsModel->updateReferancesOfNewUser($_POST['email'], $userid);
                $this->CommonMethodsModel->updateUserUniqueID($unikID, $userid);
                $this->sessionObj->offsetSet('need_user_profile', '');
                $this->sessionObj->social_login_signup = '0';

                if ($userid != 'emailExist') {
                    if ($_POST['steps'] == "personal") {
                        $BT = 1;
                    } else {
                        $BT = 0;
                    }
                    if ($user['user_referrer'] != 0) {
                        $arrRefDet = array(
                            'user_id' => $user['user_referrer'],
                            'invited_email' => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])),
                            'signed_user_id' => $userid
                        );
                        $this->CommonMethodsModel->updateSignedUpStatus($arrRefDet);
                    }

                    //$this->sessionObj->offsetSet('userid',$userid);
                    $enctuser = $this->CommonPlugin->encrypt($userid);

                    try
                    {
                        $this->sendmail = $this->MailModel->sendwelcomeMail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $enctuser);
                        $this->sendmail = $this->MailModel->sendVerifyIdMailToUser($_POST['first_name'], $_POST['last_name'], $_POST['email'], $enctuser);
                        $this->sendmail = $this->MailModel->sendVerifyUserMailId($_POST['first_name'], $_POST['last_name'], $_POST['email'], base64_encode($userid), $BT);
                    }
                    catch (\Exception $e)
                    {
                        error_log($e);
                    }

                    $userdetail = $this->CommonMethodsModel->getUser($userid);
                    $messages = array(array('message' => 'Hi ' . $_POST['first_name'] . ', Welcome to Trepr an Online Deputation Platform for all your transportation needs.'),
                        array('message' => 'Refer your friends & family and earn 20 times worth reward points.')
                    );
                    foreach ($messages as $msgs) {
                        $signupnotifyData = array('user_id' => $this->CommonMethodsModel->cleanQuery($userid),
                            'notification_type' => 'signup',
                            'notification_message' => $this->CommonMethodsModel->cleanQuery($msgs['message']),
                            'notification_subject' => 'Signup success',
                            'notification_added' => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s'))
                        );
                        $this->CommonMethodsModel->addEditNotifications($signupnotifyData, '', 'add');
                    }
                    //$this->sessionObj->offsetSet('user',$userdetail);
                    $first_name = strtoupper($userdetail['first_name']);
                    if ($userdetail['image'])
                        $img = $this->basePath . '/' . $userdetail['image'];
                    else
                        $img = $this->basePath . '/' . 'default_user.png';
                    echo "Your account has been created successfully.@SPLIT@" . '<img class="hidden-xs origin round" width="40" height="40" src="' . $img . '" alt="' . $user['first_name'] . '" /> Hi, ' . $user['first_name'];
                    exit;
                } else {
                    if ($_POST['steps'] == "personal") {
                        $BT = 1;
                    } else {
                        $BT = 0;
                    }
                    $userdetail = $this->CommonMethodsModel->getUserByEmailId($_POST['email']);
                    $hrs = strtotime(date('Y-m-d H:i:s')) - strtotime($userdetail['created']);
                    $hourss = round($hrs / 3600);
                    if ($hourss >= 24) {

                        try
                        {
                            $this->sendmail = $this->MailModel->sendVerifyUserMailId($_POST['first_name'], $_POST['last_name'], $_POST['email'], base64_encode($userdetail['ID']), $BT);
                        }
                        catch (\Exception $e)
                        {
                            error_log($e);
                        }

                        echo 'Email ID already exists!@SPLIT@EmailExist';
                        exit;
                    } else {
                        echo 'Email ID already exists!@SPLIT@EmailExist';
                        exit;
                    }
                }
            }
        }
    }

    public function clearsessionAction()
    {

        $this->sessionObj->login_try_email_exist = "0";
        $this->sessionObj->social_login_signup = "0";
        echo json_encode(array("code" => "200"));
    }

    public function socialsignupAction()
    {

        $this->CommonPlugin = $this->plugin('CommonPlugin');

        if (isset($_POST['action']) && $_POST['action'] == 'profilesdetails') {
            $type = '1';
            $middle_name = "";
            $checkUserEmailExists = $this->CommonMethodsModel->checkUserEmailExists($_REQUEST['email']);

            if ($checkUserEmailExists) {
                echo json_encode(array("code" => "201", "message" => "Email ID already exists."));
                exit;
            }
            $active = '0';
            $dob = $_POST['dob'];

           /* if ($dob != '') {
                $dobYear = date("Y", $dob);
                $now = date("Y");
                $age = $now - $dobYear;
            }*/
        $dob_explode = explode('-',$_POST['dob']);
            if ($dob != '') {
                $dobYear =  $dob_explode[2];
                $now = date("Y");
                $age = $now - $dobYear;
            }

                 
                  $passowrd="";
            $min = 18;
            if ($age<$min) {
                echo json_encode(array("code" => "201", "message" => "You must be above 18 years to register!"));
                exit;
            } else {
                $refUserId = 0;
                if (isset($this->sessionObj->user_referrer) && $this->sessionObj->user_referrer != '') {
                    $uKey = $this->CommonMethodsModel->cleanQuery($this->sessionObj->user_referrer);

                    $refUser = $this->CommonMethodsModel->getUserByKey($uKey);
                    if ($refUser) {
                        $userRefExist = $this->CommonMethodsModel->checkInReferredEmails($refUser['ID'], $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])));
                        if ($userRefExist) {
                            $refUserId = $refUser['ID'];
                        }
                    }
                }
                /*if (isset($_POST['location'])) {
                    $as = explode(',', $_POST['location']);
                    $res = array_slice($as, -3, 3, true);
                    $mylocation = implode($res, ',');
                    $addresss = '' . $_POST['location'] . '';
                    $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($addresss) . '&sensor=false');
                    $geo = json_decode($geo, true);
                    if ($geo['status'] = 'OK') {
                        $latitude = $geo['results'][0]['geometry']['location']['lat'];
                        $longitude = $geo['results'][0]['geometry']['location']['lng'];
                        $timestamp = time();
                        $timezoneAPI = "https://maps.googleapis.com/maps/api/timezone/json?location=" . $latitude . "," . $longitude . "&sensor=false&timestamp=" . $timestamp . "";
                        $response = file_get_contents($timezoneAPI);
                        $response = json_decode($response);
                        $myzone = $response->timeZoneId;
                    }
                }*/
                $filedType = (isset($_POST['social_type']) && $_POST['social_type'] == 'facebook') ? 'facebook_id' : 'google_id';
                $user = array(
                    "first_name" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['first_name']))),
                    "middle_name" => $this->CommonMethodsModel->cleanQuery($middle_name),
                    "last_name" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['last_name']))),
                    "email_address" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])),
                    "password" => $passowrd,
                    "user_key" => md5($this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])) . '_' . time()),
                    "user_referrer" => $refUserId,
                    "social_id" => $this->CommonMethodsModel->cleanQuery($_POST['social_id']),
                    "social_type" => $this->CommonMethodsModel->cleanQuery($_POST['social_type']),
                    "dob" => $this->CommonMethodsModel->cleanQuery($dob),
                    "age" => '35',
                    "gender" => $this->CommonMethodsModel->cleanQuery($_POST['gender']),
                   // "location" => $this->CommonMethodsModel->cleanQuery($mylocation),
                    "active" => $this->CommonMethodsModel->cleanQuery($active),
                    "created" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s')),
                    "modified" => $this->CommonMethodsModel->cleanQuery(time()),
                    //"timezone" => $this->CommonMethodsModel->cleanQuery($myzone),
                    $filedType => $this->CommonMethodsModel->cleanQuery($_POST['social_id'])
                );
                $userid = $this->CommonMethodsModel->addUsersUser($user);
                $this->CommonMethodsModel->updateReferancesOfNewUser($_POST['email'], $userid);
                $unikID = strtolower(str_replace(' ', '', $_POST['first_name'] . $_POST['last_name'] . decoct($userid)));
                $this->CommonMethodsModel->updateUserUniqueID($unikID, $userid);
                $this->sessionObj->offsetSet('need_user_profile', '');
                if ($userid != 'emailExist') {
                    $frndsList = $this->ProfileModel->getTempFriendsList($_COOKIE["PHPSESSID"], $this->sessionObj->user_frnd_identifier);
                    $contactData = array();
                    if (!empty($frndsList)) {
                        foreach ($frndsList as $frndRow) {
                            $contactData[] = array(
                                'user_id' => $userid,
                                'oauth_provider_type' => $this->CommonMethodsModel->cleanQuery($_POST['social_type']),
                                'friend_oauth_id' => $frndRow['friend_oauth_id'],
                                'friend_website_url' => $frndRow['friend_website_url'],
                                'friend_profile_url' => $frndRow['friend_profile_url'],
                                'friend_photo_url' => $frndRow['friend_photo_url'],
                                'friend_photo_location' => $frndRow['friend_photo_location'],
                                'friend_display_name' => $frndRow['friend_display_name'],
                                'friend_description' => $frndRow['friend_description'],
                                'friend_email' => $frndRow['friend_email'],
                                'friend_added' => time()
                            );
                        }
                    }
                    if (!empty($contactData)) {
                        $this->ProfileModel->addUpdateFriendsList($contactData);
                    }
                    $this->ProfileModel->removeTempFriendsList($_COOKIE["PHPSESSID"], $this->sessionObj->user_frnd_identifier);
                    $enctuser = $this->CommonPlugin->encrypt($userid);

                    try
                    {
                        $sendmail = $this->MailModel->sendwelcomeMail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $enctuser);
                    }
                    catch (\Exception $e)
                    {
                        error_log($e);
                    }

                    $userdetail = $this->CommonMethodsModel->getUser($userid);

                    $this->sessionObj->new_userid = $userid;
                    $this->sessionObj->user = $userdetail;
                    //$this->sessionObj->social_login_signup = '0';
                    $this->sessionObj->social_login_signup_first_form = '0';
                    $this->sessionObj->social_login_signup_photo_form = '1';
                    $this->sessionObj->social_login_signup_phone_form = '0';
                }

                if ($userid != "" && $userid != "0") {
                    if ($userdetail['image']) {
                        if (strpos($userdetail['image'], "http") !== false) {
                            $img = $userdetail['image'];
                        } else {
                            $img = $this->basePath . '/' . $userdetail['image'];
                        }
                    } else {
                        $img = $this->basePath . '/' . 'default_user.png';
                    }

                    $message = 'User information successfully added.@SPLIT@'
                        . '<li class="hidden-xs nav-drop notification-drop"> <span id="notification_count"> 0  <a class="notification" href="#"> <i class="fa fa-bell"> </i> </a> <div id="notificationContainer"> <div id="notificationTitle">Notifications <span class="pull-right"> <a href="<?php echo $this->basePath(); ?>/dashboard"> View Dashboard </a>  </div><div id="notificationsBody" class="notifications"> </div><div id="notificationFooter"> <a href="#">See All </a> </div></div></p>'
                        . '<img class="hidden-xs origin round" width="40" height="40" src="' . $img . '" alt="' . $userdetail['first_name'] . '" /> Hi, ' . $userdetail['first_name'];

                    /*  // echo "Your account has been successfully created.@SPLIT@" . '<img class="hidden-xs origin round" width="40" height="40" src="'.$img.'" alt="' . $user['first_name'] . '" /> Hi, ' . $user['first_name'];exit;*/
                    $response = array("code" => "200", "message" => $message, 'email_address' => $userdetail['email_address']);
                } else if ($userid == 'emailExist') {
                    $response = array("code" => "204", "message" => "Email ID already exists!");
                } else {
                    $response = array("code" => "203", "message" => "Something went wrong. Please try again.");
                }
            }
        }

        if (isset($_POST['action']) && $_POST['action'] == 'profile_photo') {

            if (isset($_FILES['new_profile_photo'])) {

                $errors = array();
                $file_name = $_FILES['new_profile_photo']['name'];
                $file_size = $_FILES['new_profile_photo']['size'];
                $file_tmp = $_FILES['new_profile_photo']['tmp_name'];
                $file_type = $_FILES['new_profile_photo']['type'];
                $file_ext = strtolower(explode('.', $_FILES['new_profile_photo']['name'])['1']);
                $expensions = array("jpeg", "jpg", "png");
                if (in_array($file_ext, $expensions) === false) {
                    /*$errors[]="extension not allowed, please choose a JPEG or PNG file.";*/
                    $errors[] = "Please choose a JPEG or PNG file.";
                }
                if ($file_size > 2097152) {
                    $errors[] = 'File size must be less than 2 MB';
                }
                if (empty($errors) == true) {

                    $image_path = "profile_picture";
                    $field_name = 'new_profile_photo';
                    $file_name_prefix = 'USER_DP_' . time();
                    $configArray = array(
                        array(
                            'image_x' => 150,
                            'sub_dir_name' => 'medium'
                        ),
                        array(
                            'image_x' => 100,
                            'sub_dir_name' => 'small'
                        )
                    );
                    // $imgResult = $this->CommonMethodsModel->uploadImage($field_name,$file_name_prefix,$image_path,$configArray);
                    //$file_name = $imgResult['fileName'];
                    $ext = pathinfo($_FILES['new_profile_photo']['name'], PATHINFO_EXTENSION);
                    if (move_uploaded_file($_FILES['new_profile_photo']['tmp_name'], ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext)) {
                        copy(ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext, ACTUAL_ROOTPATH . 'uploads/profile_picture/medium/' . $file_name_prefix . '.' . $ext);
                        copy(ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext, ACTUAL_ROOTPATH . 'uploads/profile_picture/small/' . $file_name_prefix . '.' . $ext);
                    }
                    $file_name = $file_name_prefix . '.' . $ext;
                } else {
                    echo json_encode(array("code" => "201", "message" => $errors['0']));
                    exit;
                }
                $arr_app_data = array(
                    'image' => $this->CommonMethodsModel->cleanQuery($file_name)
                );
            } else {
                $fileName = $_POST['profile_photo'];
                /* social dp upload start */
                $fileNameBody = 'USER_DP_' . time();
                $tempSrcLoc = ACTUAL_ROOTPATH . 'uploads/temp_dp/';
                $actDesLoc = ACTUAL_ROOTPATH . 'uploads/profile_picture/';
                $photoURL = $_POST['profile_photo'];
                //echo $actDesLoc;die;

                $explode_imgtype=explode("/", image_type_to_mime_type(exif_imagetype($photoURL)));
                $extension = end($explode_imgtype);
                $imageContent = file_get_contents($photoURL);
                $fileName = $fileNameBody . '.' . $extension;
                $fileNameWithLoc = $tempSrcLoc . $fileName;

                $ax = file_put_contents($fileNameWithLoc, $imageContent);
                $configArray = array(
                    array(
                        'image_x' => 150,
                        'sub_dir_name' => 'medium'
                    ),
                    array(
                        'image_x' => 100,
                        'sub_dir_name' => 'small'
                    )
                );
                //echo $fileNameWithLoc;die;
                if (copy($fileNameWithLoc, ACTUAL_ROOTPATH . 'uploads/profile_picture/' . $fileName)) {
                    copy(ACTUAL_ROOTPATH . 'uploads/profile_picture/' . $fileName, ACTUAL_ROOTPATH . 'uploads/profile_picture/medium/' . $fileName);
                    copy(ACTUAL_ROOTPATH . 'uploads/profile_picture/' . $fileName, ACTUAL_ROOTPATH . 'uploads/profile_picture/small/' . $fileName);
                    // copy(ACTUAL_ROOTPATH.'uploads/profile_picture/'.$fileNameBody.'.'.$ext, ACTUAL_ROOTPATH.'uploads/temp_dp/'.$fileName);
                }
                //$imgResult = $this->socialDPUpload($fileNameWithLoc, $actDesLoc, $fileNameBody, $configArray );
                $imgResult['result'] = 'success';
                if ($imgResult['result'] == 'success') {
                    $idverfy_image = $fileName;//$imgResult['fileName'];
                    $arr_photo_detail = array(
                        'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('new_userid')),
                        'user_photo_name' => $this->CommonMethodsModel->cleanQuery($idverfy_image),
                        'user_photo_added' => time()
                    );
                    $existingCount = $this->ProfileModel->getUserPhotos($this->sessionObj->offsetGet('new_userid'), 'count');
                    if ($existingCount < $this->siteConfigs['max_user_photo_upload']) {
                        $this->ProfileModel->addUserPhoto($arr_photo_detail, $this->sessionObj->offsetGet('new_userid'));
                    }
                    $this->sessionObj->social_login_signup_first_form = '0';
                    $this->sessionObj->social_login_signup_photo_form = '0';
                    $this->sessionObj->social_login_signup_phone_form = '1';
                }
                /* social dp upload end */
                $arr_app_data = array("image" => $fileName);
            }
            $this->Myaccount->updateuser($arr_app_data, $this->sessionObj->new_userid);
            $userdetail = $this->CommonMethodsModel->getUser($this->sessionObj->new_userid);
            $this->sessionObj->user = $userdetail;

            $response = array("code" => "200");
        }

        if (isset($_POST['action']) && $_POST['action'] == 'phone') {
            $arr_app_data = array("phone" => $_POST['phone'], "co_code" => $_POST['my_con_code']);
            $this->Myaccount->updateuser($arr_app_data, $this->sessionObj->new_userid);
            $userdetail = $this->CommonMethodsModel->getUser($this->sessionObj->new_userid);
            $this->sessionObj->user = $userdetail;
            $this->sessionObj->social_login_signup_first_form = '0';
            $this->sessionObj->social_login_signup_photo_form = '0';
            $this->sessionObj->social_login_signup_phone_form = '1';
            $response = array("code" => "200");
        }

        if (isset($_POST['action']) && $_POST['action'] == 'final_submit') {

            $res = $this->CommonMethodsModel->completesignup(base64_encode($this->sessionObj->new_userid));
            $userdetail = $this->CommonMethodsModel->getUser($this->sessionObj->new_userid);
            $this->sessionObj->userid = $userdetail['ID'];
            $this->sessionObj->user = $userdetail; /*print_r($userdetail);die;*/
            $this->sessionObj->social_login_signup = '0';
            $this->sessionObj->social_login_signup_first_form = '0';
            $this->sessionObj->social_login_signup_photo_form = '0';
            $this->sessionObj->social_login_signup_phone_form = '0';
            $messages = array(array('message' => 'Hi ' . $userdetail['first_name'] . ', Welcome to Trepr an Online Deputation Platform for all your transportation needs.'),
                array('message' => 'Refer your friends & family and earn 20 times worth reward points')
            );
            foreach ($messages as $msgs) {
                $signupnotifyData = array('user_id' => $this->CommonMethodsModel->cleanQuery($userdetail['ID']),
                    'notification_type' => 'signup',
                    'notification_message' => $this->CommonMethodsModel->cleanQuery($msgs['message']),
                    'notification_subject' => 'Signup success',
                    'notification_added' => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s'))
                );
                $this->CommonMethodsModel->addEditNotifications($signupnotifyData, '', 'add');
            }
            if ($userdetail['image']) {
                if (strpos($userdetail['image'], "http") !== false) {
                    $img = $userdetail['image'];
                } else {
                    $img = $this->basePath . '/' . $userdetail['image'];
                }
            } else {
                $img = $this->basePath . '/' . 'default_user.png';
            }

            $message = 'User information successfully added.@SPLIT@'
                . '<li class="hidden-xs nav-drop notification-drop"> <span id="notification_count"> 0  <a class="notification" href="#"> <i class="fa fa-bell"> </i> </a> <div id="notificationContainer"> <div id="notificationTitle">Notifications <span class="pull-right"> <a href="<?php echo $this->basePath(); ?>/dashboard"> View Dashboard </a>  </div><div id="notificationsBody" class="notifications"> </div><div id="notificationFooter"> <a href="#">See All </a> </div></div></p>'
                . '<img class="hidden-xs origin round" width="40" height="40" src="' . $img . '" alt="' . $userdetail['first_name'] . '" /> Hi, ' . $userdetail['first_name'];
            $response = array("code" => "200");
        }

        if (isset($_POST['action']) && $_POST['action'] == 'sendVerfication') {
            if (empty($_POST['email_address'])) {
                echo json_encode(array("code" => "201", "message" => "Please enter a valid email ID."));
                exit;
            }
            $checkUserEmailExists = $this->CommonMethodsModel->checkUserEmailExists($_REQUEST['email_address'], 'list', $this->sessionObj->userid);
            if ($checkUserEmailExists) {
                echo json_encode(array("code" => "201", "message" => "Email ID already exists."));
                exit;
            }
            $arr_app_data = array("email_address" => $_POST['email_address']);
            $this->Myaccount->updateuser($arr_app_data, $this->sessionObj->userid);
            $enctuser = $this->CommonPlugin->encrypt($this->sessionObj->userid);
            $userdetail = $this->CommonMethodsModel->getUser($this->sessionObj->userid);

            try
            {
                $sendmail = $this->MailModel->sendsignupMail($_POST['first_name'], $_POST['last_name'], $_POST['email_address'], $enctuser);
            }
            catch (\Exception $e)
            {
                error_log($e);
            }

            $this->sessionObj->user = $userdetail;
            $response = array("code" => "200");
        }

        if (isset($_POST['action']) && $_POST['action'] == 'resendVerfication') {
            $enctuser = $this->CommonPlugin->encrypt($this->sessionObj->userid);
            $userdetail = $this->CommonMethodsModel->getUser($this->sessionObj->userid);

            try
            {
                $sendmail = $this->MailModel->sendsignupMail($_POST['first_name'], $_POST['last_name'], $_POST['email_address'], $enctuser);
            }
            catch (\Exception $e)
            {
                error_log($e);
            }

            $this->sessionObj->user = $userdetail;
            $response = array("code" => "200");
        }

        echo json_encode($response);
        die;
    }

    public function uploadUserDPMultiple($file, $file_name, $path, $configArray)
    {

        if (isset($file)) {
            $filename = $file['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $file_name = $file_name;
            $handle = new \upload($file);
            if ($handle->uploaded) {
                $handle->file_new_name_body = $file_name;

                $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/');
                if ($configArray && !empty($configArray)) {
                    foreach ($configArray as $config) {
                        $handle->file_new_name_body = $file_name;
                        $handle->image_resize = true;
                        $handle->image_x = $config['image_x'];
                        $handle->image_ratio_y = true;
                        $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/' . $config['sub_dir_name'] . '/');
                    }
                    if ($handle->processed) {
                        $handle->clean();
                        $retArr = array('result' => 'success', 'fileName' => $file_name . '.' . $ext);
                    } else {
                        $retArr = array('result' => 'failed', 'error' => $handle->error);
                    }
                }
            } else {
                $retArr = array('result' => 'failed', 'error' => $handle->error);
            }
            return $retArr;
        }
    }

    public function signuplightboxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        if ($_POST) {
            $type = '1';
            $middle_name = "";
            $active = '1';
            $user = array(
                "type" => $this->CommonMethodsModel->cleanQuery($type),
                "first_name" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['first_name']))),
                "middle_name" => $this->CommonMethodsModel->cleanQuery($middle_name),
                "last_name" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['last_name']))),
                "email_address" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])),
                "password" => md5($_POST['passwordsignup']),
                "user_key" => md5($this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])) . '_' . time()),
                "tax" => 1,
                "mail_subscribe" => 0,
                "active" => $this->CommonMethodsModel->cleanQuery($active),
                "created" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s')),
                "modified" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s'))
            );
            $userid = $this->CommonMethodsModel->addUsersUser($user);
            $this->sessionObj->userid = $userid;
            $enctuser = $this->CommonPlugin->encrypt($userid);

            try
            {
                $this->sendmail = $this->MailModel->sendsignupMail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $enctuser);
            }
            catch (\Exception $e)
            {
                error_log($e);
            }

            /*  //$userdetail = $this->CommonMethodsModel->getUser($userid);
              //$first_name = strtoupper($userdetail[first_name]);
              //echo "Your account has been successfully created.@SPLIT@WELCOME $first_name";*/
            if ($_POST['from'] == "addFav") {
                $type = $this->generalvar['listing']['listing']['typeid'];
                $cfx = $this->search_mod->checkFavoriteExists($_POST['listingID'], $userid, $type);
                $listing = $this->search_mod->getListing($_POST['listingID']);
                if (count($cfx) > 0) {
                    echo $listing['name'] . " already exists in your My Favorites@SPLIT@" . $userid;
                } else {
                    $fields = array("module" => $_POST['listingID'], "type" => $type, "user" => $userid, "date" => 0);
                    $this->search_mod->addFavorites($fields);
                    echo $listing['name'] . " has been added to My Favorites@SPLIT@" . $userid;
                }
            } else if ($_POST['from'] == "addTrip") {
                echo "addToTrip@SPLIT@" . $userid;
            }
            exit;
        }
    }

    public function mailactiveAction()
    {
        $this->CommonPlugin = $this->plugin('CommonPlugin');

        $verfiyid = $_REQUEST['md'];
        $userid = $this->CommonPlugin->decrypt($verfiyid);

        $arr_app_data = array("email_verify" => 1);
        $this->Myaccount->updateuser($arr_app_data, $userid);
        $this->sessionObj->offsetSet('userid', $userid);
        $this->sessionObj->offsetSet('user', $userdetail);
        $this->flashMessenger()->addMessage(array('emailverify' => '1'));
        return $this->redirect()->toRoute('home');
    }

    public function resetpasswordAction()
    {
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $email = $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email']));
        $customer = $this->CommonMethodsModel->getUserByEmail($email);
        $customerid = $customer['ID'];
        if ($customerid) {
            $token = md5(uniqid(rand(), 1));
            $un = substr($token, 0, 3);
            $newpassword = "visitalton" . $un;
            $newpwd = md5($newpassword);
            $this->CommonMethodsModel->changeUserPassword($customerid, $newpwd);
            $fields = array('first_name' => $customer['first_name'], 'last_name' => $customer['last_name'], 'newpwd' => $newpassword);
            $to = $email;
            $enctuser = $this->CommonPlugin->encrypt($customerid);

            try
            {
                $this->MailModel->sendResetPasswordMail($to, $fields, $enctuser);
            }
            catch (\Exception $e)
            {
                error_log($e);
            }

            $msg = 2;
            echo $msg;
            exit;
        } else {
            $msg = 1;
            echo $msg;
            exit;
        }
    }

    public function checkloginAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            echo '@SPLIT@no@SPLIT@';
            exit;
        } else {
            echo '@SPLIT@yes@SPLIT@';
            exit;
        }
    }

    public function resetpasswordsubmitAction()
    {
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $verfiyid = $_REQUEST['md'];
        $userid = $this->CommonPlugin->decrypt($verfiyid);
        $user = $this->CommonMethodsModel->getUser($userid);
        $ermsg = '';
        if (isset($_POST['npassword']) && $_POST['npassword'] != '' && $_POST['cpassword'] != '') {
            if ($_POST['npassword'] == $_POST['cpassword']) {
                $this->CommonMethodsModel->updateUserPassword($userid, $_POST['cpassword']);
                $ermsg = 'updsuc';
                return $this->redirect()->toUrl('/');
                exit;
            } else {
                $ermsg = 'passmis';
            }
        }
        $viewArray = array(
            'user' => $this->CommonMethodsModel->getUser($userid),
            'ermsg' => $ermsg,
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(true);
        return $viewModel;
    }

    public function signinAction()
    {


        $this->basePath = $this->getEvent()->getRouter()->assemble(array(), array('name' => 'home', 'force_canonical' => true));
        if ($_POST) {
            $email = strtolower($_POST['email']);
            $password = md5($this->CommonMethodsModel->cleanQuery($_POST['password']));
            $findmail = $this->CommonMethodsModel->checkuseremailexistsornot($email);
            if ($findmail) {
                $user = $this->CommonMethodsModel->checkuserexists($email, $password);

                if ($user['ID'] != '') {
                    $loccity = @unserialize(file_get_contents('http://ip-api.com/php/' . $_SERVER['REMOTE_ADDR']));
                    $this->CommonMethodsModel->updatelocationtimebrowser($loccity['city'], $_SERVER['HTTP_USER_AGENT'], $user['ID']);
                    if (isset($_GET['social'])) {
                        $this->sessionObj->login_try_email_exist = 0;

                        $this->sessionObj->offsetSet('social_login_signup', 0);
                    }
                    $this->sessionObj->offsetSet('userid', $user['ID']);
                    $user = $this->CommonMethodsModel->getUser($user['ID']);
                    $this->sessionObj->offsetSet('user', $user);
                    $first_name = strtoupper($user['first_name']);
                    /*$curycode = $this->Currencyrate->asigncountrycurrency($user['country']);
                    $this->sessionObj->offsetSet('country',$user['country']);
                    $this->sessionObj->offsetSet('currency',$curycode['currency_code']);*/

                    if (isset($_POST['link_social_id'])) {
                        $colName = ($_POST['link_social_type'] == 'facebook') ? 'facebook_id' : 'google_id';
                        $arr_app_data = array(
                            $colName => $this->CommonMethodsModel->cleanQuery($_POST['link_social_id'])
                        );
                        $this->Myaccount->updateuser($arr_app_data, $this->sessionObj->userid);
                        $frndsList = $this->ProfileModel->getTempFriendsList($_COOKIE["PHPSESSID"], $this->sessionObj->user_frnd_identifier);
                        $contactData = array();
                        if (!empty($frndsList)) {
                            foreach ($frndsList as $frndRow) {
                                $contactData[] = array(
                                    'user_id' => $userid,
                                    'oauth_provider_type' => $this->CommonMethodsModel->cleanQuery($_POST['social_type']),
                                    'friend_oauth_id' => $frndRow['friend_oauth_id'],
                                    'friend_website_url' => $frndRow['friend_website_url'],
                                    'friend_profile_url' => $frndRow['friend_profile_url'],
                                    'friend_photo_url' => $frndRow['friend_photo_url'],
                                    'friend_photo_location' => $frndRow['friend_photo_location'],
                                    'friend_display_name' => $frndRow['friend_display_name'],
                                    'friend_description' => $frndRow['friend_description'],
                                    'friend_email' => $frndRow['friend_email'],
                                    'friend_added' => time()
                                );
                            }
                        }
                        if (!empty($contactData)) {
                            $this->ProfileModel->addUpdateFriendsList($contactData);
                        }
                        $this->ProfileModel->removeTempFriendsList($_COOKIE["PHPSESSID"], $this->sessionObj->user_frnd_identifier);

                        /* social dp upload start */
                        $fileNameBody = 'USER_DP_' . time();
                        $tempSrcLoc = ACTUAL_ROOTPATH . 'uploads/temp_dp/';
                        $actDesLoc = ACTUAL_ROOTPATH . 'uploads/profile_picture/';
                        $photoURL = $_POST['social_dp'];

                        $extension = end(explode("/", image_type_to_mime_type(exif_imagetype($photoURL))));
                        $imageContent = file_get_contents($photoURL);
                        $fileName = $fileNameBody . '.' . $extension;
                        $fileNameWithLoc = $tempSrcLoc . $fileName;

                        file_put_contents($fileNameWithLoc, $imageContent);
                        $configArray = array(
                            array(
                                'image_x' => 150,
                                'sub_dir_name' => 'medium'
                            ),
                            array(
                                'image_x' => 100,
                                'sub_dir_name' => 'small'
                            )
                        );
                        $imgResult = $this->socialDPUpload($fileNameWithLoc, $actDesLoc, $fileNameBody, $configArray);

                        if ($imgResult['result'] == 'success') {
                            $idverfy_image = $imgResult['fileName'];
                            $arr_photo_detail = array(
                                'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')),
                                'user_photo_name' => $this->CommonMethodsModel->cleanQuery($idverfy_image),
                                'user_photo_added' => time()
                            );
                            $existingCount = $this->ProfileModel->getUserPhotos($this->sessionObj->offsetGet('userid'), 'count');
                            if ($existingCount < $this->siteConfigs['max_user_photo_upload']) {
                                $this->ProfileModel->addUserPhoto($arr_photo_detail, $this->sessionObj->offsetGet('userid'));
                            }
                        }
                        /* social dp upload end */
                    }

                    if ($user['image'])
                        $img = $this->basePath . '/temp/' . $user['image'];
                    else
                        $img = 'default_user.png';

                    if (strpos($img, "http") !== false) {
                        $img = $img;
                    } else {

                    }
                    $this->sessionObj->offsetSet('need_user_profile', '');
                    if (isset($_POST['btform']) && $_POST['btform'] == '1') {
                        echo '@@@@loginsuccessbt@@@@'
                            . '<li class="hidden-xs nav-drop notification-drop"> <span id="notification_count"> 0  <a class="notification" href="#"> <i class="fa fa-bell"> </i> </a> <div id="notificationContainer"> <div id="notificationTitle">Notifications <span class="pull-right"> <a href="<?php echo $this->basePath(); ?>/dashboard"> View Dashboard </a>  </div><div id="notificationsBody" class="notifications"> </div><div id="notificationFooter"> <a href="#">See All </a> </div></div></p>'
                            . '<img class="hidden-xs origin round" width="40" height="40" src="' . $img . '" alt="' . $user['first_name'] . '" /> Hi, ' . $user['first_name'];
                        exit;
                    } else {
                        echo '@@@@loginsuccess@@@@'
                            . '<li class="hidden-xs nav-drop notification-drop"> <span id="notification_count"> 0  <a class="notification" href="#"> <i class="fa fa-bell"> </i> </a> <div id="notificationContainer"> <div id="notificationTitle">Notifications <span class="pull-right"> <a href="<?php echo $this->basePath(); ?>/dashboard"> View Dashboard </a>  </div><div id="notificationsBody" class="notifications"> </div><div id="notificationFooter"> <a href="#">See All </a> </div></div></p>'
                            . '<img class="hidden-xs origin round" width="40" height="40" src="' . $img . '" alt="' . $user['first_name'] . '" /> Hi, ' . $user['first_name'];
                        exit;
                    }

                } else {
                    echo "@@@@loginfailed@@@@";
                    exit;
                }
            } else {
                echo "@@@@mailnotfound@@@@";
                exit;
            }
        }
    }

    public function socialDPUpload($srcPath, $newPath, $file_name, $configArray)
    {

        if (isset($srcPath)) {
            $handle = new \upload($srcPath);
            if ($handle->uploaded) {

                $handle->file_new_name_body = $file_name;
                $handle->image_resize = true;
                $handle->process($newPath);
                if ($configArray && !empty($configArray)) {
                    foreach ($configArray as $config) {
                        $handle->file_new_name_body = $file_name;
                        $handle->image_resize = true;
                        $handle->image_x = $config['image_x'];
                        $handle->image_ratio_y = true;
                        $handle->process($newPath . $config['sub_dir_name'] . '/');
                    }
                    if ($handle->processed) {
                        $handle->clean();
                        $retArr = array('result' => 'success', 'fileName' => $handle->file_src_name);
                    } else {
                        $retArr = array('result' => 'failed', 'error' => $handle->error);
                    }

                }
            } else {
                $retArr = array('result' => 'failed', 'error' => $handle->error);
            }
            return $retArr;
        }
    }

    /*public function socialSignupAction(){
        $provider_array = array('Facebook'=>'facebook_id','Google'=>'google_id','Twitter'=>'twitter_id','LinkedIn'=>'linkedin_id');
        $provider = $_GET['provider'];
        $hybridauth = new \Hybrid_Auth( $this->hybridConfig );
        $authProvider = $hybridauth->authenticate($provider);
        $user_profile_obj = $authProvider->getUserProfile();
        $this->sessionObj->login_try_email_exist = "0";
        $this->sessionObj->social_login_signup = '0';
        if($user_profile_obj){
        }        
    }*/

    public function signinlightboxAction()
    {
        $this->_helper->layout->disableLayout();
        $this->view->from = $_GET['from'];
        $this->view->listingID = $_GET['listingID'];
        $this->view->eventID = $_GET['eventID'];
        $this->view->eventDateID = $_GET['eventDateID'];
        $this->view->commentID = $_GET['commentID'];
        $this->view->productID = $_GET['productID'];
        $this->view->url = $_GET['url'];
        if ($_POST) {
            $email = strtolower($_POST['signin_lightbox_email_address']);
            $password = md5($this->CommonMethodsModel->cleanQuery($_POST['signin_lightbox_password']));
            $user = $this->CommonMethodsModel->checkuserexists($email, $password);
            if ($user['ID'] != '') {
                $this->sessionObj->userid = $user['ID'];
                $user = $this->CommonMethodsModel->getUser($user['ID']);
                $login_status = "loginsuccess";

                $cartitems = $this->CommonMethodsModel->getCartBySessionId();
                if (count($cartitems) > 0) {
                    echo "loginsuccesstocheckout@SPLIT@";
                }
                if ($_POST['from'] == "addFav") {
                    $type = $this->generalvar['listing']['listing']['typeid'];
                    $cfx = $this->search_mod->checkFavoriteExists($_POST['listingID'], $user['ID'], $type);
                    $listing = $this->search_mod->getListing($_POST['listingID']);
                    if (count($cfx) > 0) {
                        echo $listing['name'] . " already exists in your My Favorites@SPLIT@" . $user['ID'];

                    } else {
                        if ($_POST['listingID'])
                            $moduleid = $_POST['listingID'];
                        elseif ($_POST['eventID'])
                            $moduleid = $_POST['eventID'];
                        if ($_POST['eventDateID'])
                            $eventDateID = $_POST['eventDateID'];
                        else
                            $eventDateID = 0;
                        $fields = array("module" => $_POST['listingID'], "type" => $type, "user" => $user['ID'], "date" => $eventDateID);
                        $this->search_mod->addFavorites($fields);
                        echo $listing['name'] . " has been added to My Favorites@SPLIT@" . $user['ID'];

                    }
                } else if ($_POST['from'] == "addEventToFav") {
                    $type = $this->generalvar['event']['event']['typeid'];
                    $eventID = $_POST['eventID'];
                    $eventDateID = $_POST['eventDateID'];
                    $cfx = $this->search_mod->checkFavoriteExists($_POST['eventID'], $user['ID'], $type, $eventDateID);
                    $event = $this->events_mod->getEventByDateID($eventID, $eventDateID, $type);
                    if (count($cfx) > 0) {
                        echo $event['title'] . " already exists in your My Favorites@SPLIT@" . $user['ID'];
                        //exit;
                    } else {
                        $eventID = $_POST['eventID'];
                        if ($_POST['eventDateID'])
                            $eventDateID = $_POST['eventDateID'];
                        else
                            $eventDateID = 0;
                        $fields = array("module" => $eventID, "type" => $type, "user" => $user['ID'], "date" => $eventDateID);
                        $this->search_mod->addFavorites($fields);
                        echo $event['title'] . " has been added to My Favorites@SPLIT@" . $user['ID'];

                    }
                } else if ($_POST['from'] == "addTrip") {
                    echo "addToTrip@SPLIT@" . $user['ID'];
                    //exit;
                } else if ($_POST['from'] == "addEventToTrip") {
                    echo "addEventToTrip@SPLIT@" . $user['ID'];
                } else if ($_POST['from'] == "addToWishlist") {
                    echo "addToWishlist@SPLIT@";
                } else if ($_POST['from'] == "addevent") {
                    echo "addevent@SPLIT@" . $user['ID'];
                    //exit;
                } else if ($_POST['from'] == "profile") {
                    echo "profile@SPLIT@";
                    //exit;
                } else if ($_POST['from'] == "blogscomment") {
                    echo "blogscomment@SPLIT@";
                } else if ($_POST['from'] == "articlescomment") {
                    echo "articlescomment@SPLIT@";
                } else if ($_POST['from'] == "storycomment") {
                    echo "storycomment@SPLIT@";
                } else if ($login_status != "loginsuccess") {
                    echo "loginfailed@SPLIT@";
                    //exit;
                }
                $this->CommonPlugin->loginBannerEventsAndTrips();
            } else {
                echo "loginfailed@SPLIT@";
            }
        }
    }

    public function requestfacebookAction()
    {
        if (isset($_GET['bt']) && $_GET['bt'] == 1) {
            $this->sessionObj->offsetSet('btform', 1);
        }
        $provider_array = array('Facebook' => 'facebook_id', 'Google' => 'google_id',
            'Twitter' => 'twitter_id', 'LinkedIn' => 'linkedin_id');
        $provider = 'Facebook';
       // $hybridauth = new \Hybrid_Auth($this->hybridConfig);

        $config = [  'callback' => 'https://trepr.co.uk/requestfacebook',
                      'providers' => [
                        'Twitter' => [
                          'enabled' => true,
                          'keys' => [
                            'key' => '...',
                            'secret' => '...',
                          ],
                        ],
                        'LinkedIn' => [
                          'enabled' => true,
                          'keys' => [
                            'id' => '...',
                            'secret' => '...',
                          ],
                        ],
                        'Facebook' => [
                          'enabled' => true,
                          'keys' => [
                            'id' => '844394145662070',
                            'secret' => 'c2791bcab0338117c95140d3f8b3f15b',
                          ],
                        ],
                      ],
                    ];
        $hybridauth = new Hybridauth($config);
        $authProvider = $hybridauth->authenticate($provider);
        $user_profile_obj = $authProvider->getUserProfile();
        $user_contacts = $authProvider->getUserContacts();

        $contactsData = array();
        if (!empty($user_contacts)) {
            foreach ($user_contacts as $contact) {
                $contactsData[] = array(
                    'user_id' => $user_profile_obj->identifier,
                    'user_session_id' => $_COOKIE["PHPSESSID"],
                    'oauth_provider_type' => $this->CommonMethodsModel->cleanQuery($provider),
                    'friend_oauth_id' => $this->CommonMethodsModel->cleanQuery($contact->identifier),
                    'friend_website_url' => $this->CommonMethodsModel->cleanQuery($contact->webSiteURL),
                    'friend_profile_url' => $this->CommonMethodsModel->cleanQuery($contact->profileURL),
                    'friend_photo_url' => $this->CommonMethodsModel->cleanQuery($contact->photoURL),
                    'friend_photo_location' => '',
                    'friend_display_name' => $this->CommonMethodsModel->cleanQuery($contact->displayName),
                    'friend_description' => $this->CommonMethodsModel->cleanQuery($contact->description),
                    'friend_email' => $this->CommonMethodsModel->cleanQuery($contact->email),
                    'friend_added' => time()
                );
            }
            if (!empty($contactsData)) {
                $res = $this->ProfileModel->addUpdateTempFriendsList($contactsData);
            }
        }
        $this->sessionObj->user_frnd_identifier = $user_profile_obj->identifier;
        $this->sessionObj->photo_url = $user_profile_obj->photoURL;

        //print_r($user_profile_obj);exit;
        $this->sessionObj->login_try_email_exist = "0";
        $this->sessionObj->social_login_signup = '0';

        if ($user_profile_obj) {
            $user_profile = (array)$user_profile_obj;

            $checkUserEmailExists = $this->CommonMethodsModel->checkUserEmailExists($user_profile['email'], 'count');

            if ($checkUserEmailExists === false) {

                $dateOfBirth = '';
                $randpassword = mt_rand();

                if (!empty($user_profile['birthDay'])) {
                    $dateOfBirth = strtotime($user_profile['birthDay'] . '-' . $user_profile['birthMonth'] . '-' . $user_profile['birthYear']);
                }

                $arr_user = array(
                    'first_name' => $user_profile['firstName'],
                    'last_name' => $user_profile['lastName'],
                    /*'facebook_authrization_code' => $code,
                    'facebook_access_token' => $access_token,*/
                    "dob" => $dateOfBirth,
                    "image" => $user_profile['photoURL'],
                    "gender" => $user_profile['gender'],
                    'email_address' => $user_profile['email'],
                    'password' => md5($randpassword),
                    'active' => '1',
                    'created' => time(),
                    'social_type' => "facebook",
                    'social_id' => $user_profile['identifier']
                );

                if (!empty($dateOfBirth)) {
                    $dob = date("Y", $dateOfBirth);
                    $now = date("Y");
                    $age = $now - $dob;
                    $arr_user['age'] = $age;
                }

                $ltype = "facebook";

                $this->sessionObj->login_try_email_exist = "0";
                $this->sessionObj->social_login_signup = '1';

                $this->sessionObj->social_login_with = 'facebook';
                $this->sessionObj->profile = $arr_user;
            } else {
                //$this->sessionObj->user_id = $checkUserEmailExists['0']['ID'];

                $this->sessionObj->profile = $checkUserEmailExists;
                $this->sessionObj->login_try_email_exist = "1";
                $this->sessionObj->social_login_signup = '0';
                $this->sessionObj->social_login_with = 'facebook';
                //print_R($this->sessionObj->social_profile);die("0000");
                if ($checkUserEmailExists['social_type'] == 'facebook' || !empty($checkUserEmailExists['facebook_id'])) {

                    $this->sessionObj->offsetSet('userid', $checkUserEmailExists['ID']);
                    $this->sessionObj->offsetSet('user', $checkUserEmailExists);
                    $first_name = strtoupper($checkUserEmailExists['first_name']);
                    $curycode = $this->Currencyrate->asigncountrycurrency($checkUserEmailExists['country']);
                    $this->sessionObj->offsetSet('country', $checkUserEmailExists['country']);
                    $this->sessionObj->offsetSet('currency', $curycode['currency_code']);


                }
            }
        }
        //$authProvider->logout();
        // return $this->redirect()->toRoute('admin');
        /*echo "<script>window.close();;window.opener.location.reload();</script>";*/
        return $this->redirect()->toRoute('home');
    }

    public function requestlinkedinAction()
    {
        $this->_helper->layout->disableLayout();
        $this->_helper->viewRenderer->setNoRender(true);
        $app_request_type = $_REQUEST['app_request_type'];
        if ($app_request_type == 'linkedin_login') {
            $this->sessionObj->app_request_type = $app_request_type;
            setcookie("app_request_type", $app_request_type);
        }
        $checkLinkedinAppUser = $this->_helper->Linkedin->checkLinkedinAppUser();
        //$this->_redirect($checkLinkedinAppUser);
        //$this->_helper->viewRenderer->setNoRender(true);
        //$this->_helper->Linkedin->getAccessToken();
        //if($this->sessionObj->app_request_type=='facebook_login' || $this->sessionObj->app_request_type=='linkedin_login' || $this->sessionObj->app_request_type=='googleplus_login'){
        if ($_COOKIE["app_request_type"] == 'facebook_login' || $_COOKIE["app_request_type"] == 'linkedin_login' || $_COOKIE["app_request_type"] == 'googleplus_login') {
            echo "<script>window.opener.location.href = '/beta/myaccount';window.close();</script>";
        } else {
            echo "<script>window.opener.reloadApps();window.close();</script>";
            //echo "Thanks for installation of our app on linkedin.";
        }
        //echo "Thanks for installation of our app on linkedin. if you like to reload the search result, please <a href='javascript:void(0);' onclick='window.opener.location.reload(true);self.close();'  >click here</a>";
    }

    /* ========================== Only Page Functions ========================== */

    public function requestgoogleplusAction_old()
    {
        $this->GoogleplusPlugin = $this->plugin('GoogleplusPlugin');
        $app_request_type = $_REQUEST['app_request_type'];

        if ($app_request_type == 'googleplus_login') {
            $this->sessionObj->app_request_type = $app_request_type;
            setcookie("app_request_type", $app_request_type);
        }

        $user_profile = $this->GoogleplusPlugin->checkGoogleplusAppUser();

        /*$gData = $this->GoogleplusPlugin->getGooglePlusFriends();
        var_dump($gData);exit;*/
        //print_r($user_profile);die;
        if ($user_profile['emails']['0']['value'] == "") {
            $user_profile = $this->GoogleplusPlugin->checkGoogleplusAppUser();
            die;
        }
        $checkUserEmailExists = $this->CommonMethodsModel->checkUserEmailExists($user_profile['emails']['0']['value']);

        if (count($checkUserEmailExists) == 0) {

            $randpassword = mt_rand();
            $arr_user = array(
                'first_name' => $user_profile['name']['givenName'],
                'last_name' => $user_profile['name']['familyName'],
                /*'facebook_user_id' => $user_profile->id,
                'facebook_authrization_code' => $code,
                'facebook_access_token' => $access_token,*/
                "image" => $user_profile['image']['url'],
                'email_address' => $user_profile['emails']['0']['value'],
                //'password' => md5($randpassword),
                'active' => '1',
                'created' => date("Y-m-d H:i:s"),
                'social_type' => "google",
                'social_id' => $user_profile['id']
            );
            // print_r($arr_user);
            // $userid = $this->myaccount_mod->adduser($arr_user,$this->sessionObj->user_id);
            //$userid = $this->myaccount_mod->adduser($arr_user);
            $ltype = "facebook";
            // $this->sendmail = $this->MailModel->sendsignupMail($user_profile->first_name,$user_profile->last_name,$user_profile->email,$randpassword,$ltype,$userid);
            //$this->sessionObj->user_id = $userid;
            //  $this->sessionObj->user = $this->myaccount_mod->getuser($this->sessionObj->user_id);
            $this->sessionObj->social_login_signup = '1';
            $this->sessionObj->login_try_email_exist = '0';
            $this->sessionObj->social_login_with = 'google';
            $this->sessionObj->profile = $arr_user;
        } else {
            //$this->sessionObj->user_id = $checkUserEmailExists['0']['ID'];
            $this->sessionObj->profile = $checkUserEmailExists['0'];
            $this->sessionObj->login_try_email_exist = "1";
            $this->sessionObj->social_login_signup = '0';
            $this->sessionObj->social_login_with = 'google';
            //print_R($this->sessionObj->social_profile);die("0000");
        }
        //var_dump($_GET);exit;
        $this->_redirect($_GET['rurl']);
        /*echo "<script>
        opener.location.reload(); // or opener.location.href = opener.location.href;
        window.close(); // or self.close();
        </script>";*/
    }

    public function requestgoogleplusAction()
    {
        /*unset($_SESSION['HA::CONFIG']);
        unset($_SESSION['HA::STORE']);
        unset($_SESSION['user_connected']);
        exit;*/
        if (isset($_GET['bt']) && $_GET['bt'] == 1) {
            $this->sessionObj->offsetSet('btform', 1);
        }
        $provider_array = array('Facebook' => 'facebook_id', 'Google' => 'google_id',
            'Twitter' => 'twitter_id', 'LinkedIn' => 'linkedin_id');
        $provider = 'Google';
        $hybridauth = new \Hybrid_Auth($this->hybridConfig);
        $authProvider = $hybridauth->authenticate($provider);
        //$this->sessionObj->hybridAuthProvider = $authProvider;
        $user_profile_obj = $authProvider->getUserProfile();
        $user_contacts = $authProvider->getUserContacts();
        $contactsData = array();
        if (!empty($user_contacts)) {
            foreach ($user_contacts as $contact) {
                $contactsData[] = array(
                    'user_id' => $user_profile_obj->identifier,
                    'user_session_id' => $_COOKIE["PHPSESSID"],
                    'oauth_provider_type' => $this->CommonMethodsModel->cleanQuery($provider),
                    'friend_oauth_id' => $this->CommonMethodsModel->cleanQuery($contact->identifier),
                    'friend_website_url' => $this->CommonMethodsModel->cleanQuery($contact->webSiteURL),
                    'friend_profile_url' => $this->CommonMethodsModel->cleanQuery($contact->profileURL),
                    'friend_photo_url' => $this->CommonMethodsModel->cleanQuery($contact->photoURL),
                    'friend_photo_location' => '',
                    'friend_display_name' => $this->CommonMethodsModel->cleanQuery($contact->displayName),
                    'friend_description' => $this->CommonMethodsModel->cleanQuery($contact->description),
                    'friend_email' => $this->CommonMethodsModel->cleanQuery($contact->email),
                    'friend_added' => time()
                );
            }
            if (!empty($contactsData)) {
                $res = $this->ProfileModel->addUpdateTempFriendsList($contactsData);
            }
        }
        $this->sessionObj->user_frnd_identifier = $user_profile_obj->identifier;
        $this->sessionObj->photo_url = $user_profile_obj->photoURL;

        $this->sessionObj->login_try_email_exist = "0";
        $this->sessionObj->social_login_signup = '0';

        if ($user_profile_obj) {
            $user_profile = (array)$user_profile_obj;
            $checkUserEmailExists = $this->CommonMethodsModel->checkUserEmailExists($user_profile['email'], 'count');

            if ($checkUserEmailExists === false) {
                $dateOfBirth = '';
                $randpassword = mt_rand();
                if (!empty($user_profile['birthDay']))
                    $dateOfBirth = strtotime($user_profile['birthDay'] . '-' . $user_profile['birthMonth'] . '-' . $user_profile['birthYear']);

                $arr_user = array(
                    'first_name' => $user_profile['firstName'],
                    'last_name' => $user_profile['lastName'],
                    /*'facebook_authrization_code' => $code,
                    'facebook_access_token' => $access_token,*/
                    "dob" => $dateOfBirth,
                    "image" => $user_profile['photoURL'],
                    "gender" => $user_profile['gender'],
                    'email_address' => $user_profile['email'],
                    'password' => md5($randpassword),
                    'active' => '1',
                    'created' => time(),
                    'social_type' => "google",
                    'social_id' => $user_profile['identifier']
                );

                if (!empty($dateOfBirth)) {
                    $dob = date("Y", $dateOfBirth);
                    $now = date("Y");
                    $age = $now - $dob;
                    $arr_user['age'] = $age;
                }
                // print_r($arr_user);
                // $userid = $this->myaccount_mod->adduser($arr_user,$this->sessionObj->user_id);
                //$userid = $this->myaccount_mod->adduser($arr_user);
                $ltype = "google";
                // $this->sendmail = $this->MailModel->sendsignupMail($user_profile->first_name,$user_profile->last_name,$user_profile->email,$randpassword,$ltype,$userid);
                //$this->sessionObj->user_id = $userid;
                //  $this->sessionObj->user = $this->myaccount_mod->getuser($this->sessionObj->user_id);
                $this->sessionObj->login_try_email_exist = "0";
                $this->sessionObj->social_login_signup = '1';
                $this->sessionObj->social_login_signup_first_form = '1';

                $this->sessionObj->social_login_with = 'google';
                $this->sessionObj->profile = $arr_user;
            } else {
                $this->sessionObj->profile = $checkUserEmailExists;
                $this->sessionObj->login_try_email_exist = "1";
                $this->sessionObj->social_login_signup = '0';
                $this->sessionObj->social_login_with = 'google';
                if ($checkUserEmailExists['social_type'] == 'google' || !empty($checkUserEmailExists['google_id'])) {
                    $this->sessionObj->offsetSet('userid', $checkUserEmailExists['ID']);
                    $this->sessionObj->offsetSet('user', $checkUserEmailExists);
                    $first_name = strtoupper($checkUserEmailExists['first_name']);
                    $curycode = $this->Currencyrate->asigncountrycurrency($checkUserEmailExists['country']);
                    $this->sessionObj->offsetSet('country', $checkUserEmailExists['country']);
                    $this->sessionObj->offsetSet('currency', $curycode['currency_code']);
                }
            }
        }
        $authProvider->logout();
        // return $this->redirect()->toRoute('admin');
        /*echo "<script>window.close();;window.opener.location.reload();</script>";*/
        return $this->redirect()->toRoute('home');
    }

    public function aboutAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'About_Us',
                'title' => 'About_Us',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'pageRow' => $this->CommonMethodsModel->getstaticPageRow(1),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;

    }

    public function usertermsAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'user terms',
                'title' => 'userterms',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'pageRow' => $this->CommonMethodsModel->getstaticPageRow(2),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function serviceratecalculationsAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->hideSwitchUser = true;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Service Rate Calculations',
                'title' => 'Service Rate Calculations',
                'active' => true,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'pageRow' => $this->CommonMethodsModel->getstaticPageRow(2),
            'actionname' => $this->params('action'),
            'project_cats' => $this->TripModel->getPackageTasksCategory(),
            'project_tasks_categories_type' => $this->TripModel->getProjectTasksCategorytype()
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function serviceratecalculationstravAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->hideSwitchUser = true;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Service Rate Calculations',
                'title' => 'Service Rate Calculations',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'pageRow' => $this->CommonMethodsModel->getstaticPageRow(2),
            'actionname' => $this->params('action'),
            'project_cats' => $this->TripModel->getPackageTasksCategory(),
            'project_tasks_categories_type' => $this->TripModel->getProjectTasksCategorytype()
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function customsAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Customs excise duty',
                'title' => 'Customs excise duty',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'custom' => $this->CommonMethodsModel->getCustomsRow(),
        );

        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function weightanddimensionsAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'weight and dimensions',
                'title' => 'weight and dimensions',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function privacyAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Privacy Policy',
                'title' => 'Privacy Policy',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'pageRow' => $this->CommonMethodsModel->getstaticPageRow(3),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function copyrightAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Copyright',
                'title' => 'Copyright',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'pageRow' => $this->CommonMethodsModel->getstaticPageRow(4),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function securitypolicyAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Security Policy',
                'title' => 'Security Policy',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'pageRow' => $this->CommonMethodsModel->getstaticPageRow(5),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function prohibitedAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Prohibited or Restricted Items',
                'title' => 'Prohibited or Restricted Items',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function merchantsAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function helpAction()
    {

        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Help',
                'title' => 'Help',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'categories' => $this->CommonMethodsModel->getQACategories(),
            'pageRowIntro' => $this->CommonMethodsModel->getstaticPageRow(7),
            'pageRowHow' => $this->CommonMethodsModel->getstaticPageRow(8),
            'pageRowPeo' => $this->CommonMethodsModel->getstaticPageRow(9),
            'pageRowPack' => $this->CommonMethodsModel->getstaticPageRow(10),
            'pageRowPro' => $this->CommonMethodsModel->getstaticPageRow(11),
            'pageRowSeek' => $this->CommonMethodsModel->getstaticPageRow(12),
            'pageRowTra' => $this->CommonMethodsModel->getstaticPageRow(13),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function contactusAction()
    {

        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Contact Us',
                'title' => 'Contact Us',
                'active' => false,
            ),

        );
        if (isset($_POST['contactSubBtn'])) {

            $page_id = $this->params()->fromPost('page_id');

            //$insData['template_subject'] = $this->params()->fromPost('template_subject');
            $insData['contactname'] = $this->params()->fromPost('contactname');
            $insData['contactemail'] = $this->params()->fromPost('contactemail');
            $insData['contactphone'] = $this->params()->fromPost('contactphone');
            $insData['contactquestion'] = $this->params()->fromPost('contactquestion');
            $insData['contactaddress'] = $this->params()->fromPost('contactaddress');
            $insData['contactdeadline'] = $this->params()->fromPost('contactdeadline');
            $insData['contactmsg'] = $this->params()->fromPost('contactmsg');

            try
            {
                $updateAccount = $this->MailModel->sendConatctUsMail($insData);
            }
            catch (\Exception $e)
            {
                error_log($e);
            }

            $message = 'Request sent successfully';
            if ($updateAccount) {
                $this->flashMessenger()->addMessage(array('alert-success' => $message));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to sent request. Please try again'));
            }
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('contact-us');
        }
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function newsroomsAction()
    {

        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'News Rooms',
                'title' => 'News Rooms',
                'active' => false,
            ),

        );
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'pageRow' => $this->CommonMethodsModel->getstaticPageRow(6),
        );
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function mytripsAction()
    {
    }

    public function mytriphistoryAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }

        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
        );
        $arrServices = $this->TripPlugin->getServiceExpensesTrips($this->sessionObj->userrole);
        $viewArray['serviceExpenses'] = $arrServices['arr_trips'];
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;

        $status = isset($_GET['filterby'])?$_GET['filterby']:'';
        $viewArray['all_trips'] = $this->TripPlugin->getTrips($status,$this->sessionObj->userrole,$this->sessionObj->userid);
        
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Dashboard',
                'title' => 'Dashboard',
                'active' => false,
                'redirect' => '/dashboard'
            ),
            array(
                'label' => $this->sessionObj->userrole == 'traveller'?'Travel Plan History':'Travel Need History',
                'title' => $this->sessionObj->userrole == 'traveller'?'Travel Plan History':'Travel Need History',
                'active' => true,
                'redirect' => false
            )
        );

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function searchtripsAction()
    {
    }

    public function wishlistoldAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
        );
        $ulrgoesto = 'seeker';
        if ($this->sessionObj->userrole == 'seeker') $ulrgoesto = 'traveller';
        $viewArray['ulrgoesto'] = $ulrgoesto;
        $viewArray['wishlists'] = $this->TripPlugin->getWishlistTrips($this->sessionObj->userrole);
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }
    public function wishlistrequestAction(){
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $viewArray = array();
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $ID = $this->TripPlugin->decrypt($_REQUEST['id']);
        $wishlistRequest = $this->TripModel->getTripRequestWishlist($ID,$this->sessionObj->userrole);

        $viewArray['wishListedReq'] = array();
        foreach($wishlistRequest as $wishReq){
            $viewArray['wishListedReq'][] = $wishReq['request_id'];
        }
        
        if($this->sessionObj->userrole == 'traveller'){
            $trip = $this->TripModel->getTrip($ID);
            $service = isset($_GET['service']) ? $_GET['service'] : '';
            $viewArray['trip_id'] = $ID;
            $viewArray['product_categories'] = $this->TripModel->getProjectTasksCategorytype();
            $viewArray['product_package_categories'] = $this->TripModel->getPackageCategorytype();
            $viewArray['product_subcategories'] = $this->TripModel->getProjectTasksCategorylisttypeAll();
            $viewArray['package_subcategories'] = $this->TripModel->getPackageTasksCategorylisttypeAll();
            $viewArray['task_categories'] = $this->TripModel->getProjectTasksCategorytype();
            $viewArray['trip'] = $trip;
            $trip_seeker = $this->TripPlugin->getTripDetails($ID, 'traveller', false, $service);
            $viewArray['user'] = $this->CommonMethodsModel->getUser($trip['user']);
            $user_cards_details = $this->TripModel->getcarddetails($this->sessionObj->userid);
            $viewArray['trip'] = $trip_seeker;
            
            $trip_traveller = $this->TripModel->getTripPeople($ID);
            $trip_travaller_packages = $this->TripModel->getTripPackage($ID);
            $trip_travaller_projects = $this->TripModel->getTripProject($ID);
            
            $viewArray['trip_traveller'] = $trip_traveller;
            $viewArray['trip_travaller_packages'] = $trip_travaller_packages;
            $viewArray['trip_travaller_projects'] = $trip_travaller_projects;
            
            $viewArray['trip_people_requests'] = $this->TripModel->getTripPeopleRequests($ID);
            $viewArray['trip_package_requests'] = $this->TripModel->getTripPackageRequests($ID);
            $viewArray['trip_project_requests'] = $this->TripModel->getTripProjectRequests($ID);
    
            $traveller_people_requests = '';
            if (isset($trip_traveller['ID'])) {
                $traveller_people_requests = $this->TripModel->getTravellerPeopleRequestsByTraveller($trip_traveller['ID']);
            }
            $traveller_package_requests = '';
            if (isset($trip_travaller_packages['ID'])) {
                $traveller_package_requests = $this->TripModel->getTravellerPackageRequestsByTraveller($trip_travaller_packages['ID']);
            }
            $traveller_project_requests = '';
            if (isset($trip_travaller_projects['ID'])) {
                $traveller_project_requests = $this->TripModel->getTravellerProjectRequestsByTraveller($trip_travaller_projects['ID']);
            }
            $viewArray['traveller_people_requests'] = $traveller_people_requests;
            $viewArray['traveller_package_requests']= $traveller_package_requests;
            $viewArray['traveller_project_requests']= $traveller_project_requests;
        } else {
            $trip = $this->TripModel->getSeekerTrip($ID);
            $service = isset($_GET['service']) ? $_GET['service'] : '';
            $viewArray['trip'] = $trip;
            $viewArray['trip_id'] = $ID;
            $viewArray['user'] = $this->CommonMethodsModel->getUser($trip['user']);
            $dob = strtotime($viewArray['user']['dob']);
            $todaydate = strtotime(date('Y-m-d'));
            $viewArray['age'] = $this->CommonPlugin->calculateAge($dob, $todaydate);
            $viewArray['product_categories'] = $this->TripModel->getProjectTasksCategorytype();
            $viewArray['product_subcategories'] = $this->TripModel->getProjectTasksCategorylisttypeAll();
            $viewArray['task_categories'] = $this->TripModel->getProjectTasksCategorytype();
            $primary = 'yes';
            $viewArray['user_location'] = $this->CommonMethodsModel->getUserLocations($trip['user'], $primary);
            $trip_seeker = $this->TripPlugin->getTripDetails($ID, 'seeker', false, $service);
            $user_cards_details = $this->TripModel->getcarddetails($this->sessionObj->userid);
            $viewArray['trip'] = $trip_seeker;
            $trip_seeker_people = $this->TripModel->getSeekerTripPeople($ID);
            $viewArray['trip_seeker'] = $trip_seeker_people;
            $trip_seeker_packages = $this->TripModel->getSeekerTripPackage($ID);
            $viewArray['trip_seeker_packages'] = $trip_seeker_packages;
            $trip_seeker_projects = $this->TripModel->getSeekerTripProject($ID);
            $viewArray['trip_seeker_projects'] = $trip_seeker_projects;
            $viewArray['traveller_people_requests'] = $this->TripModel->getTravellerPeopleRequests($ID);
            $viewArray['traveller_package_requests'] = $this->TripModel->getTravellerPackageRequests($ID);
            $viewArray['traveller_project_requests'] = $this->TripModel->getTravellerProjectRequests($ID);
            $seeker_people_requests = '';
            if (isset($trip_seeker_people['ID'])) {
                $seeker_people_requests = $this->TripModel->getSeekerPeopleRequestsBySeeker($trip_seeker_people['ID']);
            }
            $seeker_package_requests = '';
            if (isset($trip_seeker_packages['ID'])) {
                $seeker_package_requests = $this->TripModel->getSeekerPackageRequestsBySeeker($trip_seeker_packages['ID']);
            }
            $seeker_project_requests = '';
            if (isset($trip_seeker_projects['ID'])) {
                $seeker_project_requests = $this->TripModel->getSeekerProjectRequestsBySeeker($trip_seeker_projects['ID']);
            }
            $viewArray['seeker_people_requests'] = $seeker_people_requests;
            $viewArray['seeker_package_requests'] = $seeker_package_requests;
            $viewArray['seeker_project_requests'] = $seeker_project_requests;
            $viewArray['workCategory'] = $this->workCategory;
            $viewArray['user_cards_details'] = $user_cards_details;
            $viewArray['comSessObj'] = $this->sessionObj;
        }

        $viewArray['user_cards_details']        = $user_cards_details;
        $this->layout()->CommonMethodsModel     = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Wishlist',
                'title' => 'Wishlist',
                'active' => false,
                'redirect' => '/wishlist'
            ),
            array(
                'label' => 'Wishlisted Request',
                'title' => 'Wishlisted Request',
                'active' => true,
                'redirect' => false
            )
        );
        $viewArray['comSessObj'] = $this->sessionObj;
        
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }
    public function wishlistAction(){
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        $viewArray['upcoming_trips'] = $this->TripPlugin->getTrips('request_list', $this->sessionObj->userrole, $this->sessionObj->userid);
        $viewArray['unplanned_trips'] = $this->TripPlugin->getTrips('', $this->sessionObj->userrole, $this->sessionObj->userid,false,true);

        if(is_array($viewArray['upcoming_trips']) && count($viewArray['upcoming_trips']) > 0){
            foreach($viewArray['upcoming_trips'] as $tKey=>$trips){
                $ID = $this->TripPlugin->decrypt($trips['ID']);
                $wishlistRequest = $this->TripModel->getTripRequestWishlist($ID,$this->sessionObj->userrole);
                $viewArray['upcoming_trips'][$tKey]['wishlist_count'] = count($wishlistRequest);
            }
        }
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Dashboard',
                'title' => 'Dashboard',
                'active' => false,
                'redirect' => '/dashboard'
            ),
            array(
                'label' => 'Wishlist',
                'title' => 'Wishlist',
                'active' => true,
                'redirect' => false
            )
        );
        $viewArray['comSessObj'] = $this->sessionObj;

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function travelertripcontendersAction()
    {
    }

    public function myexpensesoldAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $viewArray = array();
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
        );
        $ulrgoesto = 'seeker';
        if ($this->sessionObj->userrole == 'seeker') $ulrgoesto = 'traveller';
        $viewArray['ulrgoesto'] = $ulrgoesto;
        $arrServices = $this->TripPlugin->getServiceExpensesTrips($this->sessionObj->userrole);
        $viewArray['serviceExpenses'] = $arrServices['arr_trips'];
        $toalPayAmount = $arrServices['toalPayAmount'];
        $toalPayAmount = $this->CommonMethodsModel->convertCurrency($toalPayAmount, $this->CommonMethodsModel->getSiteCurrency(), $this->CommonMethodsModel->getUserCurrency($this->sessionObj->currency));
        $viewArray['toalPayAmount'] = $toalPayAmount;

        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function myexpensesAction(){
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        $viewArray['completed_trips'] = $this->TripPlugin->getTrips('completed', $this->sessionObj->userrole, $this->sessionObj->userid);

        if(is_array($viewArray['completed_trips']) && count($viewArray['completed_trips']) > 0)
        {
            foreach($viewArray['completed_trips'] as $tKey=>$trips)
            {
                if($trips['service'] == 'People'){
                    $totalExpenses = $trips['trip_people']['people_service_fee'];
                }
                else if($trips['service'] == 'Package'){
                    $totalExpenses = $trips['trip_package']['package_service_fee'];
                }
                else if($trips['service'] == 'Product'){
                    $totalExpenses = $trips['trip_project']['project_service_fee'];
                }

                $totalExpenses = $this->CommonMethodsModel->convertCurrency($totalExpenses, $this->CommonMethodsModel->getSiteCurrency(), $this->CommonMethodsModel->getUserCurrency($this->sessionObj->currency));
                $viewArray['completed_trips'][$tKey]['totalExpenses'] = $totalExpenses;
            }
        }

        //echo '<pre>'; print_r($viewArray); exit;

        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Dashboard',
                'title' => 'Dashboard',
                'active' => false,
                'redirect' => '/dashboard'
            ),
            array(
                'label' => 'My Service Expenses',
                'title' => 'My Service Expenses',
                'active' => true,
                'redirect' => false
            )
        );
        $viewArray['comSessObj'] = $this->sessionObj;

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function myearningsAction(){
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        $viewArray['completed_trips'] = $this->TripPlugin->getTrips('completed', $this->sessionObj->userrole, $this->sessionObj->userid);

        if(is_array($viewArray['completed_trips']) && count($viewArray['completed_trips']) > 0)
        {
            foreach($viewArray['completed_trips'] as $tKey=>$trips)
            {
                if($trips['service'] == 'People'){
                    $totalExpenses = $trips['trip_people']['people_service_fee'];
                }
                else if($trips['service'] == 'Package'){
                    $totalExpenses = $trips['trip_package']['package_service_fee'];
                }
                else if($trips['service'] == 'Product'){
                    $totalExpenses = $trips['trip_project']['project_service_fee'];
                }

                $totalExpenses = $this->CommonMethodsModel->convertCurrency($totalExpenses, $this->CommonMethodsModel->getSiteCurrency(), $this->CommonMethodsModel->getUserCurrency($this->sessionObj->currency));
                $viewArray['completed_trips'][$tKey]['totalExpenses'] = $totalExpenses;
            }
        }

        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Dashboard',
                'title' => 'Dashboard',
                'active' => false,
                'redirect' => '/dashboard'
            ),
            array(
                'label' => 'My Service Earnings',
                'title' => 'My Service Earnings',
                'active' => true,
                'redirect' => false
            )
        );
        $viewArray['comSessObj'] = $this->sessionObj;

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    /* ========================== END Only Page Functions ========================== */

    public function becometravelerAction()
    {
        $this->sessionObj->offsetSet('btform', 0);
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $this->TripPlugin = $this->plugin('TripPlugin');
        $viewArray = array(
            'comSessObj' => $this->sessionObj,
            'countries' => $this->CommonMethodsModel->getCountries(),
            'currency' => $this->CommonMethodsModel->getCurrencies(),
            'user_locations' => $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid)
        );
        $viewArray['idCounts'] = $this->ProfileModel->getIdentityCount($this->sessionObj->offsetGet('userid'));
        $steps = $this->params('steps', 'personal');
        $viewArray['steps'] = $steps;
        $viewArray['prof'] = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
        $viewArray['lang'] = $this->ProfileModel->getlang($this->sessionObj->offsetGet('userid'));
        $viewArray['abtus'] = $this->ProfileModel->getabtus($this->sessionObj->offsetGet('userid'));
        if ($steps == 'identify') {
            $viewArray['idTypes'] = $this->ProfileModel->getIdentityTypes($this->sessionObj->offsetGet('userid'));
        }
        if ($steps == 'travelinfo') {
            $ID = $this->params('ID');
            $TYPE = $this->params('TYPE');
            $GOTOT_URL = isset($_GET['g']) ? urldecode($_GET['g']) : '';
            $viewArray['selectedFlightData'] = '';
            $viewArray['gotourl'] = $GOTOT_URL;
            $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
            $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
            $viewArray['travel_plans'] = $this->TripModel->getTravelTrips('upcoming', 'traveller', $this->sessionObj->userid, false);
            $viewArray['currency'] = $this->CommonMethodsModel->getCurrencies();
        }
        if ($steps == 'service') {
            $tripId = isset($_GET['tripId']) ? $this->decrypt($_GET['tripId']) : '';
            $tripDetails = $this->TripModel->getTrip($tripId);
            $trip = isset($tripDetails) ? $tripDetails : false;
            $viewArray['airport_details'] = $this->TripModel->getFlightsDetails($tripId);
            $origin_location = $this->TripModel->getAirPort($tripDetails['origin_location']);

            $destination_location = $this->TripModel->getAirPort($tripDetails['destination_location']);
            $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
            $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
            $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
            $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
            $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
            $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
            $origin_location_country_code = isset($origin_location['country_code']) ? $origin_location['country_code'] : '';
            $destination_location_country_code = isset($destination_location['country_code']) ? $destination_location['country_code'] : '';
            $viewArray['origin_location_country_code'] = $origin_location_country_code;
            $viewArray['destination_location_country_code'] = $destination_location_country_code;

            $viewArray['origin_location'] = $origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;
            $viewArray['destination_location'] = $destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;
            $viewArray['origin_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';
            $viewArray['destination_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';
            $viewArray['selectedFlightData'] = !empty($viewArray['airport_details']) ? htmlspecialchars(json_encode($viewArray['airport_details'])) : '';

            $viewArray['editAddr'] = true;
            $editAddOptions = '<option value="' . $origin_location_country_code . '">' . $origin_location_country . '</option>';
            if ($origin_location_country_code != $destination_location_country_code)
                $editAddOptions .= '<option value="' . $destination_location_country_code . '">' . $destination_location_country . '</option>';

            $editAddOptions .= '<option value="more" class="more_sel_text">More</option>';
            $viewArray['editAddOptions'] = $editAddOptions;
            $viewArray['trip'] = $trip;
            $viewArray['tripId'] = $tripId;
            $viewArray['workCategory'] = $this->workCategory;
            $viewArray['projectcat'] = $this->TripModel->getProjectTasksCategorytype(0);
            $viewArray['project_tasks_categories_type'] = $this->TripModel->getProjectTasksCategorytype(1);
        }
        if ($steps == 'confirm') {
            $tripId = isset($_GET['tripId']) ? $this->decrypt($_GET['tripId']) : '';
            $service = isset($_GET['service']) ? $_GET['service'] : '';
            $primary = 'yes';
            $viewArray['tripId'] = $tripId;
            $trip_seeker = $this->TripPlugin->getTripDetails($tripId, 'traveller', true, $service);
            $trip = $this->TripModel->getTrip($tripId);
            $viewArray['trip_user'] = $this->CommonMethodsModel->getUser($trip['user']);
            $viewArray['user'] = $this->sessionObj->user;
            $viewArray['user_location'] = $this->CommonMethodsModel->getUserLocationByAddrId($trip['user_location'], $trip['user'], $primary);
            $viewArray['trip'] = $trip_seeker;


            $viewArray['finshUrl'] = '/search?trip=' . $_GET['tripId'] . '&service=' . $service;
        }
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(true);
        return $viewModel;
    }

    public function signupBTAction()
    {
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        if ($_POST)
        {
            //print_r($_POST);die;
            $type = '1';
            $middle_name = "";
            $active = '0';
            $dob = strtotime($_POST['dob']);


            $dob = ($_POST['dob'] != '') ? strtotime($_POST['dob']) : '';
            if ($dob != '') {
                $dobYear = date("Y", $dob);
                $now = date("Y");
                $age = $now - $dobYear;
            }
            if (isset($_POST['country'])) {
                $as = explode(',', $_POST['country']);
                $res = array_slice($as, -3, 3, true);
                $mylocation = implode($res, ',');
                $addresss = '' . $_POST['country'] . '';
                $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($addresss) . '&sensor=false');
                $geo = json_decode($geo, true);
                if ($geo['status'] = 'OK') {
                    $latitude = $geo['results'][0]['geometry']['location']['lat'];
                    $longitude = $geo['results'][0]['geometry']['location']['lng'];
                    $timestamp = time();
                    $timezoneAPI = "https://maps.googleapis.com/maps/api/timezone/json?location=" . $latitude . "," . $longitude . "&sensor=false&timestamp=" . $timestamp . "";
                    $response = file_get_contents($timezoneAPI);
                    $response = json_decode($response);
                    $myzone = $response->timeZoneId;
                }
            }
            $user = array(
                "first_name" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['first_name']))),
                "last_name" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['last_name']))),
                "email_address" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])),
                "user_key" => md5($this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])) . '_' . time()),
                //"phone" => $this->CommonMethodsModel->cleanQuery($_POST['phone']),
                "gender" => $this->CommonMethodsModel->cleanQuery($_POST['gender']),
                //"country" => $this->CommonMethodsModel->cleanQuery($_POST['country']),
                "location" => $this->CommonMethodsModel->cleanQuery($mylocation),
                "city" => $this->CommonMethodsModel->cleanQuery($_POST['city']),
                "dob" => $dob,
                "age" => $age,
                "subscribe" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['subscribe'])),
                "active" => $this->CommonMethodsModel->cleanQuery($active),
                "modified" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s')),
                "timezone" => $this->CommonMethodsModel->cleanQuery($myzone)
            );

            if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
                if (isset($_POST['password']) && !empty($_POST['password']))
                    $user['password'] = md5($_POST['password']);
                $user['created'] = $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s'));
            }
            $min = strtotime('+18 years', $dob);
            if (time() < $min) {
                echo 'You must be above 18 years to register!@SPLIT@EmailExist';
                exit;
            }
            if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
                $userid = $this->CommonMethodsModel->addUsersUser($user);
            } else {
                $userid = $this->CommonMethodsModel->checkEmailUser($user, $this->sessionObj->offsetGet('userid'));
                if ($userid === true) {
                    echo 'Email ID already exists!@SPLIT@EmailExist';
                    exit;
                } else {
                    $this->ProfileModel->updateprofile($user, $this->sessionObj->offsetGet('userid'));
                    $userid = $this->sessionObj->offsetGet('userid');
                    $this->sessionObj->userrole = 'traveller';
                }
            }
            if ($userid != 'emailExist') {
                $this->sessionObj->userrole = 'traveller';
                //$this->sessionObj->offsetSet('userid',$userid);
                $this->sessionObj->offsetSet('useridBT', $userid);
                $enctuser = $this->CommonPlugin->encrypt($userid);

                try
                {
                    $this->sendmail = $this->MailModel->sendwelcomeMail($_POST['first_name'], $_POST['last_name'], $_POST['email'], $enctuser);
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }

                $userdetail = $this->CommonMethodsModel->getUser($userid);
                // $this->sessionObj->offsetSet('user',$userdetail);
                $first_name = strtoupper($userdetail['first_name']);
                if ($userdetail['image'])
                    $img = $this->basePath . '/' . $userdetail['image'];
                else
                    $img = $this->basePath . '/' . 'default_user.png';
                echo "Your account has been created successfully.@SPLIT@" . '<img class="hidden-xs origin round" width="40" height="40" src="' . $img . '" alt="' . $user['first_name'] . '" /> Hi, ' . $user['first_name'];
                exit;
            } else {
                echo 'Email ID already exists!@SPLIT@EmailExist';
                exit;
            }

        }
    }

    public function searchairportAction()
    {
        $q = $_REQUEST['term'];
        $airports = $this->CommonMethodsModel->getAirportsBySearch($q);
        if (count($airports) > 0) {
            foreach ($airports as $airport) {

                $airport_name = '(' . $airport['code'] . ') ' . $airport['city'] . ', ' . $airport['country'] . ' - ' . $airport['name'];
                $arr_airport[] = array('ID' => $airport['ID'], 'value' => $airport_name, 'code' => $airport['code'], 'country_code' => $airport['country_code'], 'country' => $airport['country']);
            }
        }
        echo json_encode($arr_airport);
        exit;
    }

    public function searchairlinesAction()
    {
        $q = $_REQUEST['term'];
        $airports = $this->CommonMethodsModel->getAirlinesBySearch($q);
        if (count($airports) > 0) {
            foreach ($airports as $airport) {
                $airline_name = '(' . $airport['airline_carrier'] . ') ' . $airport['airline_name'];
                $arr_airport[] = array('value' => $airline_name, 'airline_name' => $airport['airline_name'], 'airline_carrier' => $airport['airline_carrier']);
            }
        }
        echo json_encode($arr_airport);
        exit;
    }

    public function importAirportsAction()
    {
        $airports = file_get_contents('https://iatacodes.org/api/v6/airports?api_key=fa039de9-5614-4c46-9e1d-57fb7b19af45');
        $airports = json_decode($airports);

        foreach ($airports->response as $airport) {
            if ($airport->lat)
                $lat = $airport->lat;
            else
                $lat = '';

            if ($airport->lng)
                $lng = $airport->lng;
            else
                $lng = '';

            if ($airport->timezone)
                $timezone = $airport->timezone;
            else
                $timezone = '';

            if ($airport->gmt)
                $gmt = $airport->gmt;
            else
                $gmt = '';

            $arr_airport = array(
                'name' => $airport->name,
                'city_code' => $airport->city_code,
                'country_code' => $airport->country_code,
                'code' => $airport->code,
                'latitude' => $lat,
                'longitude' => $lng,
                'time_zone' => $timezone,
                'gmt' => $gmt
            );
            //$this->CommonMethodsModel->addAirport($arr_airport);
        }
        echo "DONE";
        exit;
    }

    public function updateAirportCityAction()
    {
        $airports = file_get_contents('https://iatacodes.org/api/v6/cities?api_key=fa039de9-5614-4c46-9e1d-57fb7b19af45');
        $airports = json_decode($airports);

        foreach ($airports->response as $airport) {
            $arr_airport_city = array(
                'city' => $airport->name
            );
            //$this->CommonMethodsModel->updateAirportCity($arr_airport_city,$airport->code);
        }
        echo "DONE";
        exit;
    }

    public function updateAirportCountryAction()
    {
        $airports = file_get_contents('https://iatacodes.org/api/v6/countries?api_key=fa039de9-5614-4c46-9e1d-57fb7b19af45');
        $airports = json_decode($airports);

        foreach ($airports->response as $airport) {
            $arr_airport_city = array(
                'country' => $airport->name
            );
            /*    //$this->CommonMethodsModel->updateAirportCountry($arr_airport_city,$airport->code); */
        }
        echo "DONE";
        exit;
    }

    public function getcountryAction()
    {     
        if($_REQUEST['cur_country']!="" && $_REQUEST['cur_country']!='undefined')
        {
            $country_name=$_REQUEST['cur_country'];
            $type=$_REQUEST['type'];
        }
        else{
             $country_name=$this->siteConfigs['countryDefault'];
             $type="country";
            
        }
        
    
        $country = $this->CommonMethodsModel->getCountryByName($country_name, $type);
        if ($country['ID'] > 0) {
            $countryCurrency = $this->CommonMethodsModel->getCurrenciesCode($country['currency_code']);
            $this->sessionObj->currency = $country['currency_code'];
            $this->sessionObj->country = $country['name'];
            $this->sessionObj->countryid = $country['ID'];
            $this->sessionObj->countrycode = $country['code'];
            $this->sessionObj->currencycode = $country['currency_symbol'];
            $country['currency_symbol'] = $country['currency_symbol'];
        }
        echo json_encode($country);
        exit;
    }

    public function airlinedetailAction()
    {
        echo "hello";

    }

    public function onewayAction()
    {
        $api_token = '4bc8339331bef86019688e0bf0244e22';

        $segments[] = array("date" => "2016-06-18", "destination" => "DEL", "origin" => "MAA");
        $data = array(
            "marker" => "52726",
            "host" => "biztrove.com",
            "user_ip" => "127.0.0.1",
            "locale" => "en",
            "trip_class" => "Y",
            "passengers" => array("adults" => 1, "children" => 0, "infants" => 0),
            "segments" => $segments
        );
        ksort($data);


        foreach ($data as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $kk => $vv) {
                    if (is_array($vv)) {
                        foreach ($vv as $kk => $vvv) {
                            if ($sign_string)
                                $sign_string .= ':';
                            $sign_string .= $vvv;
                        }
                    } else {
                        if ($sign_string)
                            $sign_string .= ':';
                        $sign_string .= $vv;
                    }
                }
            } else {
                if ($sign_string)
                    $sign_string .= ':';
                $sign_string .= $v;
            }
        }

        $sign_string = $api_token . ':' . $sign_string;


        $signature = md5($sign_string);

        $arr_data = array(
            "signature" => $signature,
            "marker" => "52726",
            "host" => "biztrove.com",
            "user_ip" => "127.0.0.1",
            "locale" => "en",
            "trip_class" => "Y",
            "passengers" => array("adults" => 1, "children" => 0, "infants" => 0),
            "segments" => $segments
        );

        $data_string = json_encode($arr_data);


        $ch = curl_init('http://api.travelpayouts.com/v1/flight_search');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data_string))
        );

        $result = curl_exec($ch);
        $result = json_decode($result);


        $ch = curl_init('http://api.travelpayouts.com/v1/flight_search_results?uuid=' . $result->search_id);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json')
        );

        $result = curl_exec($ch);
        $result = json_decode($result);
        echo "<pre>";
        print_r($result);
        exit;
    }

    public function payAction()
    {
    }

    public function paysendAction()
    {
        $payment_type = 'Sale';
        $request = 'METHOD=DoDirectPayment';
        $request .= '&VERSION=51.0';
        $request .= '&USER=treprmerchant_api1.gmail.com';
        $request .= '&PWD=GPFED7KDK3CMA6UY';
        $request .= '&SIGNATURE=Awvs7YEHzlyQGEq37PlLlZdR3xMrAnP5jbO1WYtHn7MS-8JijJ3YYNJf';
        $request .= '&CUSTREF=' . (int)$_SESSION['order_id'];
        $request .= '&PAYMENTACTION=' . $payment_type;
        $request .= '&AMT=' . $_POST['amount'];
        $request .= '&CREDITCARDTYPE=' . $_POST['cc_type'];
        $request .= '&ACCT=' . urlencode(str_replace(' ', '', $_POST['cc_number']));
        $request .= '&CARDSTART=' . urlencode($_POST['cc_start_date_month'] . $_POST['cc_start_date_year']);
        $request .= '&EXPDATE=' . urlencode($_POST['cc_expire_date_month'] . $_POST['cc_expire_date_year']);
        $request .= '&CVV2=' . urlencode($_POST['cc_cvv2']);
        if ($_POST['cc_type'] == 'SWITCH' || $_POST['cc_type'] == 'SOLO') {
            $request .= '&CARDISSUE=' . urlencode($_POST['cc_issue']);
        }
        $request .= '&FIRSTNAME=' . urlencode($_POST['first_name']);
        $request .= '&LASTNAME=' . urlencode($_POST['last_name']);
        $request .= '&EMAIL=' . urlencode($_POST['email_address']);
        $request .= '&PHONENUM=' . urlencode($_POST['phone_no']);
        $request .= '&IPADDRESS=' . urlencode($_SERVER['REMOTE_ADDR']);
        $request .= '&STREET=' . urlencode($_POST['address']);
        $request .= '&CITY=' . urlencode($_POST['city']);
        $request .= '&STATE=' . urlencode($_POST['state']);
        $request .= '&ZIP=' . urlencode($_POST['zip']);
        $request .= '&COUNTRYCODE=' . urlencode($_POST['country_code']);
        $request .= '&CURRENCYCODE=' . urlencode('USD');
        /*
        $request .= '&SHIPTONAME=' . urlencode($order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname']);
        $request .= '&SHIPTOSTREET=' . urlencode($order_info['shipping_address_1']);
        $request .= '&SHIPTOCITY=' . urlencode($order_info['shipping_city']);
        $request .= '&SHIPTOSTATE=' . urlencode(($order_info['shipping_iso_code_2'] != 'US') ? $order_info['shipping_zone'] : $order_info['shipping_zone_code']);
        $request .= '&SHIPTOCOUNTRYCODE=' . urlencode($order_info['shipping_iso_code_2']);
        $request .= '&SHIPTOZIP=' . urlencode($order_info['shipping_postcode']);
        */
        /*
        $curl = curl_init('https://api-3t.paypal.com/nvp'); // This is for live account
        $curl = curl_init('https://api-3t.sandbox.paypal.com/nvp'); // This is for sandbox account
        */
        $curl = curl_init('https://api-3t.sandbox.paypal.com/nvp');
        curl_setopt($curl, CURLOPT_PORT, 443);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        $response = curl_exec($curl);
        curl_close($curl);
        $filename = time() . 'data.txt';
        $fp = fopen($filename, 'w');
        fwrite($fp, $response);
        /*
        if (!$response) {
            write curl error to log file
            $fp = fopen('data.txt', 'DoDirectPayment failed: ' . curl_error($curl) . '(' . curl_errno($curl) . ')');
            fwrite($fp, $request);
            fclose($fp);
        }
        */
        $response_info = array();
        parse_str($response, $response_info);
        $json = array();
        echo json_encode($response_info);
        exit;
        if ((($response_info['ACK'] == 'Success') || ($response_info['ACK'] == 'SuccessWithWarning')) && $_POST['amount'] == $response_info['AMT']) {
            $message = '';
            if (isset($response_info['AVSCODE'])) {
                $message .= 'AVSCODE: ' . $response_info['AVSCODE'] . "\n";
            }
            if (isset($response_info['CVV2MATCH'])) {
                $message .= 'CVV2MATCH: ' . $response_info['CVV2MATCH'] . "\n";
            }
            if (isset($response_info['TRANSACTIONID'])) {
                $message .= 'TRANSACTIONID: ' . $response_info['TRANSACTIONID'] . "\n";
            }
            if (isset($response_info['AMT'])) {
                $message .= 'AMOUNT: ' . $response_info['AMT'] . "\n";
            }
            fwrite($fp, $message);
            $sql = "CREATE TABLE IF NOT EXISTS `paypal_pro` (
              `id` int(11) NOT NULL AUTO_INCREMENT,
              `Order_Id` varchar(255) NOT NULL,
              `Name` varchar(255) NOT NULL,
              `Email` varchar(255) NOT NULL,
              `Address` varchar(255) NOT NULL,
              `Phone_No` varchar(255) NOT NULL,
              `City` varchar(255) NOT NULL,
              `State` varchar(255) NOT NULL,
              `Country_code` varchar(255) NOT NULL,
              `Currency` varchar(255) NOT NULL,
              `Amount` decimal(10,0) NOT NULL,
              `Message` text NOT NULL,
              `ip` varchar(255) NOT NULL,
               PRIMARY KEY (`id`)
            )";
            mysql_query($sql) or mysql_error();
            $name = $_POST['first_name'] . ' ' . $_POST['last_name'];
            $sql = 'INSERT INTO `paypal_pro` SET `Order_Id` ="' . $_SESSION['order_id'] . '", `Name` = "' . $name . '", `Email` = "' . $_POST['email_address'] . '", `Address` = "' . $_POST['address'] . '", `Phone_No` ="' . $_POST['phone_no'] . '", `City` = "' . $_POST['city'] . '", `State` = "' . $_POST['state'] . '", `Country_code` = "' . $_POST['country_code'] . '", `Currency` = "USD", `Amount` = "' . $_POST['amount'] . '", `Message` ="' . $message . '", `ip` = "' . $_SERVER['REMOTE_ADDR'] . '"';
            mysql_query($sql) or mysql_error();

            $json['success'] = 'success.php';
        } else {
            $json['error'] = $response_info['L_LONGMESSAGE0'];
        }
        fclose($fp);
        echo(json_encode($json));
    }

    public function testAction()
    {
        echo 'test';
    }

    public function mailtemplateAction()
    {
        $viewModel = new ViewModel();
        $viewModel->setTerminal(true);
        return $viewModel;
    }

    public function callbackAuthAction()
    {
        require_once(ACTUAL_ROOTPATH . "hybridauth/Hybrid/Auth.php");
        require_once(ACTUAL_ROOTPATH . "hybridauth/Hybrid/Endpoint.php");
        \Hybrid_Endpoint::process();
    }

}