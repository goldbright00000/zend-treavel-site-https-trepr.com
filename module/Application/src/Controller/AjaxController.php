<?php
namespace Application\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Validator\File\Size;
use Application\Model\Currencyrate;
require_once(ACTUAL_ROOTPATH . "Upload/src/Upload.php");
class AjaxController extends AbstractActionController
{
    var $sessionObj;
    var $TripPlugin;
    var $CommonPlugin;
    public function __construct($data = false)
    {
        /*Loading models from factory         * Call the model object using it's class name         */
        if ($data['models'] && is_array($data['models'])) {
            foreach ($data['models'] as $model) {
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
    }
    public function getLatLngByAddressIdAction()
    {
        $addrId = $this->CommonMethodsModel->cleanQuery($_POST['addressId']);
        if ($_POST['addressId'] == '') {
            $resMessage = array(
                'result' => 'failure',
                'message' => 'address id is missing'
            );
        } else {
            $addrData = $this->CommonMethodsModel->getUserLocationByAddrId($addrId);
            if ($addrData) {
                $resMessage = array(
                    'result' => 'success',
                    'addressData' => $addrData
                );
            } else {
                $resMessage = array(
                    'result' => 'failure',
                    'message' => 'no address found'
                );
            }
        }
        echo json_encode($resMessage);
        exit;
    }
    public function projectTaskFileAction()
    {
        $addrId = $this->CommonMethodsModel->cleanQuery($_POST['addressId']);
        if ($_POST['addressId'] == '') {
            $resMessage = array(
                'result' => 'failure',
                'message' => 'address id is missing'
            );
        } else {
            $addrData = $this->CommonMethodsModel->getUserLocationByAddrId($addrId);
            if ($addrData) {
                $resMessage = array(
                    'result' => 'success',
                    'addressData' => $addrData
                );
            } else {
                $resMessage = array(
                    'result' => 'failure',
                    'message' => 'no address found'
                );
            }
        }
        echo json_encode($resMessage);
        exit;
    }
    public function uploadPassengerAttachmentAction()
    {
        $fileName         = 'uploadfile';
        $image_path       = "seeker_people_attachment";
        $file_name_prefix = 'seeker_passpeople_' . time();
        $result           = $this->CommonMethodsModel->uploadFileToServer($fileName, $file_name_prefix, $image_path);
        if (isset($result['success']) && $result['success'] == true) {
            $this->sessionObj->UPLOADS = array(
                $this->params()->fromPost('qquuid') => array(
                    'fileName' => $result['uploadName']
                )
            );
        }
        echo json_encode($result);
        exit;
    }
    public function downloadAction()
    {
        $file     = ACTUAL_ROOTPATH . 'uploads/' . $this->params('folder') . '/' . $this->params('file');
        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen($file, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName(basename($file));
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . basename($file) . '"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($file),
            'Expires' => '@0',
            /* @0, because zf2 parses date as string to \DateTime() object*/
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);
        return $response;
        exit;
    }
    public function deletePassengerAttachmentAction()
    {
        if ($this->params()->fromPost('qquuid') != '' && isset($this->sessionObj->UPLOADS[$this->params()->fromPost('qquuid')]['fileName'])) {
            $fileName = $this->sessionObj->UPLOADS[$this->params()->fromPost('qquuid')]['fileName'];
            if (unlink(ACTUAL_ROOTPATH . 'uploads/seeker_people_attachment/' . $fileName)) {
                echo 'Deleted';
                exit;
            } else {
                echo 'Error';
                exit;
            }
        }
    }
    public function deletePassengerAttachmentMannuallyAction()
    {
        if ($this->params()->fromPost('imageFilename') != '') {
            if (unlink(ACTUAL_ROOTPATH . 'uploads/seeker_people_attachment/' . $this->params()->fromPost('imageFilename'))) {
                echo 'Deleted';
                exit;
            } else {
                echo 'Error';
                exit;
            }
        }
    }
    public function importPayoutMethodAction()
    {
        $row = 1;
        if (($handle = fopen(ACTUAL_ROOTPATH . "data/payout_methods.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                if ($row != 1 && $data[3] != '' && $data[0] != '') {
                    $sqlValues      = "(" . "'" . mysql_real_escape_string($data[0]) . "'," . "'" . mysql_real_escape_string($data[1]) . "'," . "'" . mysql_real_escape_string($data[2]) . "'," . "'" . mysql_real_escape_string($data[3]) . "'," . "'" . mysql_real_escape_string($data[4]) . "'," . "'" . mysql_real_escape_string($data[5]) . "'," . "'" . mysql_real_escape_string($data[6]) . "'" . ")";
                    $sql            = "INSERT INTO payout_method_settings (country_code,country_name,payout_method,processing_time,additional_fees,currencies,details)" . " VALUES " . $sqlValues;
                    $inserted_id    = $this->CommonMethodsModel->runQuery($sql, 'insert');
                    $explodedFields = explode("\n", $data[7]);
                    $sqlFields      = array();
                    if (!empty($explodedFields)) {
                        /* $labelName, $fieldName, $conditionTxt, $madatory */
                        foreach ($explodedFields as $field) {
                            $explodedString = explode('(', $field);
                            preg_match_all("/\([^\]]*\)/", $field, $matches);
                            $conditionTxt = isset($matches[0][0]) ? $matches[0][0] : '';
                            $labelName    = '';
                            $fieldName    = '';
                            $madatory     = 0;
                            if (trim($explodedString[0]) != '') {
                                $madatory  = ($explodedString[0][0] == '*') ? 1 : 0;
                                $labelName = str_replace('* ', '', trim($explodedString[0]));
                                $fieldName = str_replace(' ', '_', strtolower($labelName));
                            }
                            if ($fieldName != '') {
                                $sqlFields[] = "(" . "'" . mysql_real_escape_string($inserted_id) . "'," . "'" . mysql_real_escape_string($labelName) . "'," . "'" . mysql_real_escape_string($fieldName) . "'," . "'" . mysql_real_escape_string($conditionTxt) . "'," . $madatory . ")";
                            }
                        }
                        if (!empty($sqlFields)) {
                            $fieldSql = "INSERT INTO payout_method_fields (payout_method_setting_id,field_label,field_name,field_condition,field_mandatory  )" . " VALUES " . implode(',', $sqlFields);
                            $this->CommonMethodsModel->runQuery($fieldSql, 'insert');
                        }
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        exit;
        return true;
        exit;
    }
    public function getPayoutMethodSettingsAction()
    {
        $countryCode = $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('country_code', false));
        if ($countryCode && $countryCode == '') {
            $resMessage = array(
                'result' => 'failure',
                'message' => 'Please select country'
            );
        } else {
            $filterPayoutSetting = array(
                'where' => array(
                    'country_code' => $countryCode
                )
            );
            $payoutSettingsData  = $this->Myaccount->getPayoutMethodSettingsByCountry($filterPayoutSetting);
            if (!empty($payoutSettingsData)) {
                $fromEncoding = 'ISO-8859-1';
                /*array_walk_recursive($payoutSettingsData, function (&$value, $key, $fromEncoding) {                    if (is_string($value)) {                        $value = iconv($fromEncoding, 'UTF-8', $value);                    }                }, $fromEncoding);*/
                foreach ($payoutSettingsData as $keyIndex => $eachSett) {
                    foreach ($eachSett as $key => $value) {
                        if (is_string($value)) {
                            $payoutSettingsData[$keyIndex][$key] = iconv($fromEncoding, 'UTF-8', $value);
                        }
                        $fieldsFilter                            = array(
                            'where' => array(
                                'payout_method_setting_id' => $eachSett['payout_method_setting_id']
                            )
                        );
                        $payoutSettingsData[$keyIndex]['fields'] = $this->Myaccount->getPayoutMethodFields($fieldsFilter);
                    }
                }
                $resMessage = array(
                    'result' => 'success',
                    'payoutSettingsData' => $payoutSettingsData
                );
            } else {
                $resMessage = array(
                    'result' => 'failure',
                    'message' => 'No payout methods found'
                );
            }
        }
        echo json_encode((object) $resMessage);
        exit;
    }
    public function getUserPayoutMethodsListAction()
    {
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Type' => 'application/octet-stream'
        ));
        $userId = $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('user_id', false));
        if ($userId) {
            $filterPayoutMethod    = array(
                'where' => array(
                    'user_id' => $userId
                )
            );
            $userPayoutMethodsData = $this->Myaccount->getUserPayoutMethods($filterPayoutMethod);
            $resMessage            = array(
                'result' => 'success',
                'payoutMethodData' => $userPayoutMethodsData
            );
        } else {
            $resMessage = array(
                'result' => 'failure',
                'message' => 'user id missing!'
            );
        }
        echo json_encode((object) $resMessage);
        exit;
    }
    public function getUserPayoutMethodAction()
    {
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Type' => 'application/octet-stream'
        ));
        $payoutAddrId = $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('user_payout_address_id', false));
        if ($payoutAddrId && $payoutAddrId == '') {
            $resMessage = array(
                'result' => 'failure',
                'message' => 'User payout address id is required'
            );
        } else {
            $filterPayoutMethod    = array(
                'where' => array(
                    'user_payout_address_id' => $payoutAddrId
                )
            );
            $userPayoutMethodsData = $this->Myaccount->getUserPayoutMethods($filterPayoutMethod, 'row');
            if (!empty($userPayoutMethodsData)) {
                $filterPayoutMethodFileds        = array(
                    'where' => array(
                        'user_payout_address_id' => $payoutAddrId
                    )
                );
                $userPayoutMethodsData['fields'] = $this->Myaccount->getUserPayoutMethodFields($filterPayoutMethodFileds);
                $fromEncoding                    = 'ISO-8859-1';
                array_walk_recursive($userPayoutMethodsData, function(&$value, $key, $fromEncoding)
                {
                    if (is_string($value)) {
                        $value = iconv($fromEncoding, 'UTF-8', $value);
                    }
                }, $fromEncoding);
                $resMessage = array(
                    'result' => 'success',
                    'payoutMethodData' => $userPayoutMethodsData
                );
            } else {
                $resMessage = array(
                    'result' => 'failure',
                    'message' => 'No payout methods found'
                );
            }
        }
        echo json_encode((object) $resMessage);
        exit;
    }
    public function deleteUserPayoutMethodAction()
    {
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Type' => 'application/octet-stream'
        ));
        $payoutAddrId = $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('user_payout_address_id', false));
        if ($payoutAddrId && $payoutAddrId != false && $payoutAddrId != '') {
            $this->Myaccount->deleteUserPayoutMethod($payoutAddrId);
            $this->Myaccount->deleteUserPayoutMethodFields($payoutAddrId);
            $this->flashMessenger()->addMessage(array(
                'alert-success' => 'Payout method has been deleted successfully.'
            ));
        } else {
            $this->flashMessenger()->addMessage(array(
                'alert-danger' => 'An error has occurred while deleting payout method. Please try again.'
            ));
        }
        exit;
    }
    public function utf8ize($d)
    {
        if (is_array($d)) {
            foreach ($d as $k => $v) {
                $d[$k] = utf8ize($v);
            }
        } else if (is_string($d)) {
            return utf8_encode($d);
        }
        return $d;
    }
    public function getpassengersfeesrcAction()
    {
    	//print_r($_POST);die;
        $coutryPrice      = $noofstopsPrice = $distancePrice = 0;
        $userrole         = $_POST['userrole'];
        /*$passengers_count = 1;
        if ($this->sessionObj->userrole == 'seeker') {*/
            $passengers_count = (int) $_POST['passengers_count'];
        //}
        $passengers_count = (!empty($passengers_count)) ? $passengers_count : 1;
        if (isset($_POST['origin_location_country_code']) && isset($_POST['destination_location_country_code'])) {
            $origin_location_country_code      = $_POST['origin_location_country_code'];
            $destination_location_country_code = $_POST['destination_location_country_code'];
            $arrCoutryPrice                    = $this->TripModel->getCountryPrice($origin_location_country_code, $destination_location_country_code, 'people');
        }
        if (isset($_POST['noofstops'])) {
            $noofstops = $_POST['noofstops'];
        }
        if (isset($_POST['distance'])) {
            $distance = (int) $_POST['distance'];
        }
        if (!empty($arrCoutryPrice)) {
            $coutryPrice = $arrCoutryPrice['country_price'];
        }
        if (isset($noofstops) && $noofstops != '') {
            $noofstops         = ($noofstops == 0) ? 'direct' : $noofstops;
            $arrNoofStopsPrice = $this->TripModel->getNoofstopsPrice($noofstops);
            if (!empty($arrNoofStopsPrice)) {
                $noofstopsPrice = $arrNoofStopsPrice['stop_price'];
            }
        }
        if (!empty($distance)) {
            $arrDistancePrice = $this->TripModel->getDistancePrice($distance, 'people');
            if (!empty($arrDistancePrice)) {
                $distancePrice = $arrDistancePrice['distance_price'];
            }
        }

        $peopleserviceFee = (float) ($coutryPrice + $noofstopsPrice + $distancePrice) * $passengers_count;
		$original = $peopleserviceFee;
        if ($userrole == 'traveller') {
            $adminProfit      = round((20 / 100) * $peopleserviceFee);
            $peopleserviceFee = $peopleserviceFee - $adminProfit;
        }
        $peopleserviceFee = $this->CommonMethodsModel->convertCurrency($peopleserviceFee, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);

		$final['orginalPrice'] = number_format(ceil($original));
		$final['convertedPrice'] = number_format(ceil($peopleserviceFee));
		echo json_encode($final);
        exit;
    }
    public function getpackagesfeesrcAction()
    { 	
		$userrole	= $_POST['userrole'];
		if (isset($_POST)){
            $distance = $_POST['distance'];
            $type_of_service = 'package';
            $distancePrice = $this->TripModel->getDistancePrice(round($distance), $type_of_service);

            $departureAirport = $_POST['origin_location_country_code'];
            $arrivalAirport = $_POST['destination_location_country_code'];
            $countryPrice   = $this->TripModel->getCountryPrice($departureAirport, $arrivalAirport, $type_of_service);

            $package_total_fee = array();

            for ($i = 1; $i <= 1; $i++)
            {
                $tot_prc = $this->TripModel->getItemCategoryPrice($_POST['category_' . $i] . $_POST['item_category_' . $i], 'package');

                if(!empty($_POST['item_weight_' . $i])){
                    $item_weight = $_POST['item_weight_' . $i];
                }
                else{
                    $item_weight = '0.5';
                }

                $tot_weight_prc = $this->TripModel->getWeightPrice($item_weight, 'package');

                $money = $this->CommonMethodsModel->convertCurrency($_POST['item_worth_' . $i], $this->sessionObj->currency, $this->CommonMethodsModel->getSiteCurrency());
                $itemPricePrice = $this->TripModel->getItemPricePrice($money, 'package');

                $package_total_fee[] = (float)($countryPrice['country_price'] + $distancePrice['distance_price'] + ($tot_prc['tot_item_price'] * $tot_weight_prc['weight_carried_price']) + $itemPricePrice['price_item_price']);
            }
            $packageserviceFee = array_sum($package_total_fee);
			$originals = $packageserviceFee;
			if ($userrole == 'traveller') {
				$adminProfit       = round((20 / 100) * $packageserviceFee);
				$packageserviceFee = $packageserviceFee - $adminProfit;
			}
			$packageserviceFee = $this->CommonMethodsModel->convertCurrency($packageserviceFee, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);
			
			$final['orginalPrice'] = number_format(ceil($originals));
			$final['convertedPrice'] = number_format(ceil($packageserviceFee));
			echo json_encode($final);
			exit;
        }
		/*$kg = 0.45359237;
		if($_POST['weight_type'] =='kg'){
			$weight = $_POST['total_weight'];
		}elseif($_POST['weight_type'] =='lb'){
			$weight = $_POST['total_weight']*$kg;
		}
	
			$origin_location_country_code      = $_POST['origin_location_country_code'];
			$destination_location_country_code = $_POST['destination_location_country_code'];
			$total_weight                      = (float) $weight;//$_POST['total_weight'];
			$total_worth                       = (float) $_POST['total_worth'];
			$item_categories                   = $_POST['item_categories'];
			$userrole                          = $_POST['userrole'];
			$distance                          = isset($_POST['distance']) ? (int) $_POST['distance'] : '';
			$coutryPrice                       = $itemPrice = $totalItemCatPrice = $weightPrice = $distancePrice = $totalSeekerPrice = 0;
			$arrCoutryPrice                    = $this->TripModel->getCountryPrice($origin_location_country_code, $destination_location_country_code, 'package');
			if (!empty($arrCoutryPrice)) {
				$coutryPrice = $arrCoutryPrice['country_price'];
			}
			if (!empty($distance)) {
				$arrDistancePrice = $this->TripModel->getDistancePrice($distance, 'package');
				if (!empty($arrDistancePrice)) {
					$distancePrice = $arrDistancePrice['distance_price'];
				}
			}
			if ($userrole == 'traveller') {
				// $total_weight = 1;
				// $total_worth  = 1;
				$total_weight                      = (float) $weight;//$_POST['total_weight'];
				$total_worth                       = (float) $_POST['total_worth'];
				
			} else { 
				if (!empty($item_categories)) {
					$arr_item_categories = explode(',', $item_categories);
					foreach ($arr_item_categories as $key => $val) {
						$arr_itemData = explode('_', $val);
						if (isset($arr_itemData[0]) && !empty($arr_itemData[0])) {
							$arrItemCatPrice = $this->TripModel->getItemCategoryPrice($arr_item_categories[0], 'package');
							if (!empty($arrItemCatPrice)) {
								$totalItemCatPrice = $arrItemCatPrice['tot_item_price'];
							}
						}
						if (isset($total_weight) && !empty($total_weight)) {
							$arrWeightPrice = $this->TripModel->getWeightPrice($total_weight, 'package');
							if (!empty($arrWeightPrice)) {
								$weightPrice = $arrWeightPrice['weight_carried_price'];
							}
						}
						if (isset($total_worth) && !empty($total_worth)) {
							$arrItemPrice = $this->TripModel->getItemPricePrice($total_worth, 'package');
							if (!empty($arrItemPrice)) {
								$itemPrice = $arrItemPrice['price_item_price'];
							}
						}
						$totalSeekerPrice += (float) (($coutryPrice + $totalItemCatPrice + $itemPrice + $distancePrice) * ($total_weight));
						
					}
					//echo $coutryPrice .', '.$totalItemCatPrice.', '.$itemPrice.', '.$distancePrice.', '.$weightPrice;
				}
				$original = $totalSeekerPrice;
				  $totalSeekerPrice = $this->CommonMethodsModel->convertCurrency($totalSeekerPrice, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);
					$final['orginalPrice'] = number_format($original, 2);
					$final['convertedPrice'] = number_format($totalSeekerPrice, 2);
				echo json_encode($final);
				exit;
			}
			if (!empty($item_categories)) {
				$arrItemCatPrice = $this->TripModel->getItemCategoryPrice($item_categories, 'package');
				if (!empty($arrItemCatPrice)) {
					$totalItemCatPrice = $arrItemCatPrice['tot_item_price'];
				}
			}
			if (!empty($total_weight)) {
				$arrWeightPrice = $this->TripModel->getWeightPrice($total_weight, 'package');
				if (!empty($arrWeightPrice)) {
					$weightPrice = $arrWeightPrice['weight_carried_price'];
				}
			}
			if (!empty($total_worth)) {
				$arrItemPrice = $this->TripModel->getItemPricePrice($total_worth, 'package');
				if (!empty($arrItemPrice)) {
					$itemPrice = $arrItemPrice['price_item_price'];
				}
			}
			$packageserviceFee = (float) ($coutryPrice + $totalItemCatPrice + $itemPrice + $distancePrice) * $weightPrice;
			if ($userrole == 'traveller') {
				$adminProfit       = round((20 / 100) * $packageserviceFee);
				$packageserviceFee = $packageserviceFee - $adminProfit;
			}
			$originals = $packageserviceFee;
			 $packageserviceFee = $this->CommonMethodsModel->convertCurrency($packageserviceFee, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);
			$final['orginalPrice'] = number_format($originals, 2);
					$final['convertedPrice'] = number_format($packageserviceFee, 2);
				echo json_encode($final);
			exit;*/
    }
    public function getprojectsfeesrcAction()
    {
        $userrole = $_POST['userrole'];
		if (isset($_POST['work_category']) && $_POST['work_category'] == 1)
        {
			$distance = $_POST['distance'];
			$type_of_service = 'project';
			$distancePrice = $this->TripModel->getDistancePrice(round($distance), $type_of_service);

			$countryPrice   = $this->TripModel->getCountryPrice($_POST['origin_location_country_code'], $_POST['destination_location_country_code'], $type_of_service);
		
            $tot_item_price = array();
			
			//$countryPrice['country_price'] = $distancePrice['distance_price'] = 0;
			
			$project_fees = array();
            for ($i = 1; $i <= 1; $i++) {
                
				$tot_prc = $this->TripModel->getProductCategoryPrice($_POST['category_' . $i] . $_POST['item_category_' . $i], 'project');
				
				$itemWeightPrice = $this->TripModel->getWeightPrice($_POST['item_weight_' . $i], 'project');
				
				$money = $this->CommonMethodsModel->convertCurrency($_POST['item_worth_' . $i], $this->sessionObj->currency, $this->CommonMethodsModel->getSiteCurrency());
            	$itemCategoryPricePrice = $this->TripModel->getItemPricePrice($money, 'project');
				
				$project_fees[] = (float)($countryPrice['country_price'] + $distancePrice['distance_price'] + $tot_prc['tot_item_price'] + $itemWeightPrice['weight_carried_price'] + $itemCategoryPricePrice['price_item_price'] + $money + (($money/100)*5));
            }
			$packageserviceFee = array_sum($project_fees);
        }
        else
        {
            $tot_item_price = array();
            for ($i = 1; $i <= $_POST['selected_project_tasks']; $i++) {
                $tot_prc = $this->TripModel->getTaskCategoryPrice($_POST['task_category_' . $i] . $_POST['additional_requirements_category_' . $i], 'project');
                $tot_item_price[] = $tot_prc['tot_item_price'];
            }
            $itemCategoryPrice = array_sum($tot_item_price);
            if ($userrole == 'traveller') {
				$adminProfit       = round((20 / 100) * $packageserviceFee);
				$packageserviceFee = $packageserviceFee - $adminProfit;
			}
			$packageserviceFee = $packageserviceFee;
        }
		$originals = $packageserviceFee;
		if ($userrole == 'traveller') {
            $adminProfit       = round((20 / 100) * $packageserviceFee);
            $packageserviceFee = $packageserviceFee - $adminProfit;
        }
		$packageserviceFee = $this->CommonMethodsModel->convertCurrency($packageserviceFee, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);
		$final['orginalPrice'] = number_format(ceil($originals));
		$final['convertedPrice'] = number_format(ceil($packageserviceFee));
		echo json_encode($final);exit;
		
		/*$kg = 0.45359237;
		if(isset($_POST['product_weight_typekg'])){
			$weight = $_POST['total_weight'];
		}elseif(isset($_POST['product_weight_typelb'])){
			$weight = $_POST['total_weight']*$kg;
		}
        $origin_location_country_code      = $_POST['origin_location_country_code'];
        $destination_location_country_code = $_POST['destination_location_country_code'];
        $userrole                          = $_POST['userrole'];
        if (isset($_POST['total_weight']))
            $total_weight = (float) $weight;//$_POST['total_weight'];
        if (isset($_POST['total_worth']))
            $total_worth = (float) $_POST['total_worth'];
        if (isset($_POST['item_categories']))
            $item_categories = $_POST['item_categories'];
        $task_category  = $_POST['task_category'];
        $work_category  = $_POST['work_category'];
        $distance       = isset($_POST['distance']) ? (int) $_POST['distance'] : '';
        $coutryPrice    = $itemPrice = $totalItemCatPrice = $weightPrice = $distancePrice = $taskPrice = $totalSeekerPrice = 0;
        $arrCoutryPrice = $this->TripModel->getCountryPrice($origin_location_country_code, $destination_location_country_code, 'project');
        if (!empty($distance)) {
            $arrDistancePrice = $this->TripModel->getDistancePrice($distance, 'project');
            if (!empty($arrDistancePrice)) {
                $distancePrice = $arrDistancePrice['distance_price'];
            }
        }

        if (!empty($arrCoutryPrice)) {
            $coutryPrice = $arrCoutryPrice['country_price'];
        }

        if ($userrole == 'traveller') {
			$total_weight	= (float) $weight;//$_POST['total_weight'];
			$total_worth	= (float) $_POST['total_worth'];
        } else {

            if ($work_category == 1) {
            	
                if (!empty($item_categories)) {
                    $arr_item_categories = explode(',', $item_categories);
                    foreach ($arr_item_categories as $key => $val) {
                        $arr_itemData = explode('_', $val);
                        if (isset($arr_itemData[0]) && !empty($arr_itemData[0])) {
                            $arrItemCatPrice = $this->TripModel->getProductCategoryPrice($arr_item_categories[0], 'project');
                            if (!empty($arrItemCatPrice)) {
                                $totalItemCatPrice = $arrItemCatPrice['tot_item_price'];
                            }
                        }
                        if (isset($total_weight) && !empty($total_weight)) {
                            $arrWeightPrice = $this->TripModel->getWeightPrice($total_weight, 'project');
                            if (!empty($arrWeightPrice)) {
                                $weightPrice = $arrWeightPrice['weight_carried_price'];
                            }
                        }
                        if (isset($total_worth) && !empty($total_worth)) {
                            $arrItemPrice = $this->TripModel->getItemPricePrice($total_worth, 'project');
                            if (!empty($arrItemPrice)) {
                                $itemPrice = $arrItemPrice['price_item_price'];
                            }
                        }
                        $totalSeekerPrice += (float) ($coutryPrice + $totalItemCatPrice + $itemPrice + $distancePrice) * $total_weight;
                    }
                    
                }

            } else {
                if (!empty($task_category)) {
                    $arr_item_categories = explode(',', $task_category);
                    foreach ($arr_item_categories as $key => $val) {
                        $arr_itemData = explode('_', $val);
                        if (isset($arr_itemData[0]) && !empty($arr_itemData[0])) {
                            $arrItemCatPrice = $this->TripModel->getTaskCategoryPrice($arr_itemData[0], 'project');
                            if (!empty($arrItemCatPrice)) {
                                $totalItemCatPrice = $arrItemCatPrice['tot_item_price'];
                            }
                        }
                        if (isset($arr_itemData[1]) && !empty($arr_itemData[1])) {
                            $arrItemPrice = $this->TripModel->getItemPricePrice($arr_itemData[1], 'project');
                            if (!empty($arrItemPrice)) {
                                $itemPrice = $arrItemPrice['price_item_price'];
                            }
                        }
                        $totalSeekerPrice += (float) ($coutryPrice + $totalItemCatPrice + $distancePrice);
                    }
                }
            }
			$original = $totalSeekerPrice;
		   	$totalSeekerPrice = $this->CommonMethodsModel->convertCurrency($totalSeekerPrice, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);
			$final['orginalPrice'] = number_format($original, 2);
			$final['convertedPrice'] = number_format($totalSeekerPrice, 2);
			echo json_encode($final);
            exit;
        }
        if ($work_category == 1) {
            if (!empty($item_categories)) {
                $arrItemCatPrice = $this->TripModel->getProductCategoryPrice($item_categories, 'project');
                if (!empty($arrItemCatPrice)) {
                    $totalItemCatPrice = $arrItemCatPrice['tot_item_price'];
                }
            }
        } else {
            if (!empty($task_category)) {
                $arrTaskPrice = $this->TripModel->getTaskCategoryPrice($task_category, 'project');
                if (!empty($arrTaskPrice)) {
                    $totalItemCatPrice = $arrTaskPrice['tot_item_price'];
                }
            }
        }
        if (!empty($total_weight)) {
            $arrWeightPrice = $this->TripModel->getWeightPrice($total_weight, 'project');
            if (!empty($arrWeightPrice)) {
                $weightPrice = $arrWeightPrice['weight_carried_price'];
            }
        }
        if (!empty($total_worth)) {
            $arrItemPrice = $this->TripModel->getItemPricePrice($total_worth, 'project');
            if (!empty($arrItemPrice)) {
                $itemPrice = $arrItemPrice['price_item_price'];
            }
        }
        if (empty($total_weight)) {
            $packageserviceFee = (float) ($coutryPrice + $totalItemCatPrice + $distancePrice);
        } else {
            $packageserviceFee = (float) ($coutryPrice + $totalItemCatPrice + $itemPrice + $distancePrice) * $weightPrice;
        }
        if ($userrole == 'traveller') {
            $adminProfit       = round((20 / 100) * $packageserviceFee);
            $packageserviceFee = $packageserviceFee - $adminProfit;
        }
		$originals = $packageserviceFee;
         $packageserviceFee = $this->CommonMethodsModel->convertCurrency($packageserviceFee, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);
	   $final['orginalPrice'] = number_format($originals, 2);
				$final['convertedPrice'] = number_format($packageserviceFee, 2);
			echo json_encode($final);
        exit;*/
    }
}