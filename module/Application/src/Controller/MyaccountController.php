<?php
namespace Application\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Stdlib\RequestInterface as Request;
use Zend\Stdlib\ResponseInterface as Response;
use Zend\Mvc\MvcEvent;
class MyaccountController extends AbstractActionController
{
    var $sessionObj;
    var $CommonPlugin;
	var $TripPlugin;
    var $GoogleplusPlugin;
    protected $serviceLocator;
    var $action;
    var $viewData;
    var $generalvar;
    public function __construct($data = false)
    {
        /*Loading models from factory         * Call the model object using it's class name         */
        if ($data['models'] && is_array($data['models'])) {
            foreach ($data['models'] as $model) {
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj             = new Container('comSessObj');
        $this->viewData['controller'] = substr(get_class($this), strrpos(get_class($this), '\\') + 1);
        $this->timeZones              = $data['configs']['timezones'];
        $this->generalvar             = $data['configs']['siteConfigs'];
		$this->stripeConfig 		  = $data['configs']['stripe_keys'];
    }
	public function onDispatch(\Zend\Mvc\MvcEvent $e) 
    {
        if(isset($this->sessionObj->userid) && $this->sessionObj->userid != ''){
			$isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			if(!$isUserLoggedIn){
				$this->redirect()->toRoute('logout');
			}else{
				$this->sessionObj->offsetSet('user_rating', $this->CommonMethodsModel->getreviews($this->sessionObj->userid));
			}
		}
		return parent::onDispatch($e);
    }
    public function indexAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->CommonPlugin             = $this->plugin('CommonPlugin');
        $CtrlName                       = $this->params('controller');
        $CtrlName                       = substr($CtrlName, strrpos($CtrlName, '\\') + 1);
        $CtrlName                       = strtolower(str_replace("Controller", "", $CtrlName));
        $this->viewData['controller']   = $CtrlName;
        $this->viewData['countries']    = $this->CommonMethodsModel->getCountries();
        $this->viewData['user_profile'] = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
        $this->viewData['actnotif']     = $this->Myaccount->getactnotification($this->sessionObj->userid);
		$this->viewData['country_detail']  = $this->CommonMethodsModel->getCountryByCode($this->viewData['user_profile']['co_code']);
		$this->viewData['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
        if ($this->getRequest()->isPost()) {
            if (isset($_POST['mobileno'])) {
                $enbsms = 0;
                if (isset($_POST['enbsms']) && $_POST['enbsms']) {
                    $enbsms = 1;
                }
                $rece_another_person = 0;
                if ($this->params()->fromPost('rece_another_person', false)) {
                    $rece_another_person = 1;
                }
                $outstand_p_service = 0;
                if ($this->params()->fromPost('outstand_p_service', false)) {
                    $outstand_p_service = 1;
                }
                $i_rec_p_serv = 0;
                if ($this->params()->fromPost('i_rec_p_serv', false)) {
                    $i_rec_p_serv = 1;
                }
                $chg_trep_act_listing = 0;
                if ($this->params()->fromPost('chg_trep_act_listing', false)) {
                    $chg_trep_act_listing = 1;
                }
                $ger_prom = 0;
                if ($this->params()->fromPost('ger_prom', false)) {
                    $ger_prom = 1;
                }
                $reser_rem = 0;
                if ($this->params()->fromPost('reser_rem', false)) {
                    $reser_rem = 1;
                }
                $arr_peopleabs_detail = array(
                    'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                    'mobile' => $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('mobileno')),
                    'enable_sms' => $this->CommonMethodsModel->cleanQuery($enbsms),
                    'rece_another_person' => $this->CommonMethodsModel->cleanQuery($rece_another_person),
                    'outstand_p_service' => $this->CommonMethodsModel->cleanQuery($outstand_p_service),
                    'i_rec_p_serv' => $this->CommonMethodsModel->cleanQuery($i_rec_p_serv),
                    'chg_trep_act_listing' => $this->CommonMethodsModel->cleanQuery($chg_trep_act_listing),
                    'ger_prom' => $this->CommonMethodsModel->cleanQuery($ger_prom),
                    'reser_rem' => $this->CommonMethodsModel->cleanQuery($reser_rem)
                );
                $result               = $this->Myaccount->updateaccountnotif($arr_peopleabs_detail, $this->sessionObj->userid);
                if ($result) {
                    $this->flashMessenger()->addMessage(array(
                        'alert-success' => 'Notification settings are updated successfully.'
                    ));
                } else {
                    $this->flashMessenger()->addMessage(array(
                        'alert-danger' => 'An error occured while updating notification settings. Please try again.'
                    ));
                }
                echo $this->redirect()->toRoute('my-account');
            }
        }
        $this->layout()->hideSwitchUser     = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr      = array(
            array(
                'label' => 'My Account',
                'title' => 'My Account',
                'active' => false,
                'redirect' => '/myaccount'
            ),
            array(
                'label' => 'Notification',
                'title' => 'Notification',
                'active' => true,
                'redirect' => false //'/myaccount'
            )
        );
        $this->viewData['flashMessages']    = $this->flashMessenger()->getMessages();
        $this->viewData['active_tab']       = "notification";
        $this->viewData['comSessObj']       = $this->sessionObj;
		
        $viewModel                          = new ViewModel();
        $viewModel->setVariables($this->viewData);
        return $viewModel;
    }
    public function paymentMethodAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->CommonPlugin           = $this->plugin('CommonPlugin');
		$this->TripPlugin             = $this->plugin('TripPlugin');
        $CtrlName                     = $this->params('controller');
        $CtrlName                     = substr($CtrlName, strrpos($CtrlName, '\\') + 1);
        $CtrlName                     = strtolower(str_replace("Controller", "", $CtrlName));
        $this->viewData['controller'] = $CtrlName;
        $this->viewData['countries']  = $this->CommonMethodsModel->getCountries();
        $listcardv                    = $this->CommonMethodsModel->Listusercard($this->sessionObj->userid);
		$this->viewData['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
        $asv                          = array();
        if ($listcardv) {
            foreach ($listcardv as $value) {
				$stripeCardInfo = $this->TripPlugin->retriveStripeSource($value['stripe_src_id']);
				if(isset($stripeCardInfo->card) && $value['stripe_src_id'] != ''){
					$value["card_no"] = 'xxxx xxxx xxxx '.$stripeCardInfo->card->last4;
					$value["card_valid"] = $stripeCardInfo->card->exp_month.' / '.$stripeCardInfo->card->exp_year;
					$value['card_type'] = strtolower($stripeCardInfo->card->brand);
					$value['card_billing_zipcode'] = $stripeCardInfo->owner->address->postal_code;
					$value['card_billing_country'] = $stripeCardInfo->owner->address->country;
					$value['payment_card_type'] = $stripeCardInfo->card->funding;
				} else {
					continue;
				}
                $asv[] = array(
                    "ID" => $value['ID'],
                    "user" => $value['user'],
                    "holder_name" => $value['holder_name'],
                    "holder_last_name" => $value['holder_last_name'],
                    "card_no" => $value['card_no'],
                    "card_valid" => $value['card_valid'],
                    "card_cvv" => $value['card_cvv'],
                    "card_type" => $value['card_type'],
                    "payment_card_type" => $value['payment_card_type'],
                    "default_card" => $value['default_card']
                );
            }
        }
        $this->viewData['listcard']         = $asv;
        $this->layout()->hideSwitchUser     = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr      = array(
            array(
                'label' => 'My Account',
                'title' => 'My Account',
                'active' => false,
                'redirect' => '/myaccount'
            ),
            array(
                'label' => 'Payment Method',
                'title' => 'Payment Method',
                'active' => true,
                'redirect' => false //'/myaccount/payment-method'
            )
        );
        $this->viewData['flashMessages']    = $this->flashMessenger()->getMessages();
        $this->viewData['active_tab']       = "payment_method";
        $this->viewData['comSessObj']       = $this->sessionObj;
		$this->viewData['stripeConfig'] 	= $this->stripeConfig;
        $viewModel                          = new ViewModel();
        $viewModel->setVariables($this->viewData);
        return $viewModel;
    }
    public function payoutMethodAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $CtrlName           = $this->params('controller');
        $CtrlName           = substr($CtrlName, strrpos($CtrlName, '\\') + 1);
        $CtrlName           = strtolower(str_replace("Controller", "", $CtrlName));
        if ($this->getRequest()->isPost()) {
            $action            = $this->params()->fromPost('action', 'add');
            $edit_id           = $this->params()->fromPost('edit_id', false);
            $payoutAddressData = array(
                'user_id' => $this->sessionObj->userid,
                'payout_method_setting_id' => $this->params()->fromPost('payout_method_type'),
                'user_payout_country' => $this->params()->fromPost('payout_country'),
                'user_payout_address_1' => $this->params()->fromPost('payout_address1'),
                'user_payout_address_2' => $this->params()->fromPost('payout_address2'),
                'user_payout_city' => $this->params()->fromPost('payout_city'),
                'user_payout_state' => $this->params()->fromPost('payout_state'),
                'user_payout_zip_code' => $this->params()->fromPost('payout_zip')
            );
            $uPayMethodaddrId  = $this->Myaccount->addEditUserPayoutMethod($payoutAddressData, $edit_id);
            $payoutFields      = $this->params()->fromPost('payout_fields');
            $payoutFieldArray  = isset($payoutFields[$this->params()->fromPost('payout_method_type')]) ? $payoutFields[$this->params()->fromPost('payout_method_type')] : array();
            if ($action == 'edit' && $edit_id != false && !empty($edit_id)) {
                $this->Myaccount->deleteUserPayoutMethodFields($uPayMethodaddrId);
            }
            $resultSuccess = false;
            if (!empty($payoutFieldArray)) {
                foreach ($payoutFieldArray as $key => $payoutFields) {
                    $payoutFieldData = array(
                        'user_payout_address_id' => $uPayMethodaddrId,
                        'user_id' => $this->sessionObj->userid,
                        'payout_method_field_id' => $key,
                        'payout_method_field_value' => $payoutFields,
                        'payout_method_modified' => time()
                    );
                    $resultSuccess   = $this->Myaccount->addUserPayoutMethodFields($payoutFieldData);
                }
            }
            if ($resultSuccess) {
                $this->flashMessenger()->addMessage(array(
                    'alert-success' => 'Payout method has been updated successfully.'
                ));
            } else {
                $this->flashMessenger()->addMessage(array(
                    'alert-danger' => 'An error occured while updating payout method. Please try again.'
                ));
            }
            echo $this->redirect()->toRoute('my-account', array(
                'action' => 'payoutMethod'
            ));
        }
        $payOutFilters = array(
            'where' => array(
                'user_id' => $this->sessionObj->userid
            )
        );
        $userPayouts   = $this->Myaccount->getUserPayoutMethods($payOutFilters);
        if ($userPayouts && !empty($userPayouts)) {
            foreach ($userPayouts as $key => $payout) {
                $payOutFieldsFilters         = array(
                    'where' => array(
                        'user_payout_address_id' => $payout['user_payout_address_id']
                    )
                );
                $userPayouts[$key]['fields'] = $this->Myaccount->getUserPayoutMethodFields($payOutFieldsFilters);
            }
        }
        $this->viewData['userPayouts']      = $userPayouts;
        $this->viewData['controller']       = $CtrlName;
        $this->viewData['countries']        = $this->CommonMethodsModel->getCountries();
		$this->viewData['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
        $this->layout()->hideSwitchUser     = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr      = array(
            array(
                'label' => 'My Account',
                'title' => 'My Account',
                'active' => false,
                'redirect' => '/myaccount'
            ),
            array(
                'label' => 'Payout Method',
                'title' => 'Payout Method',
                'active' => true,
                'redirect' => false //'/myaccount/payout-method'
            )
        );
        $this->viewData['flashMessages']    = $this->flashMessenger()->getMessages();
        $this->viewData['active_tab']       = "payout_method";
        $this->viewData['comSessObj']       = $this->sessionObj;
        $viewModel                          = new ViewModel();
        $viewModel->setVariables($this->viewData);
        return $viewModel;
    }
    public function transactionHistoryAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->CommonPlugin                                     = $this->plugin('CommonPlugin');
        $CtrlName                                               = $this->params('controller');
        $CtrlName                                               = substr($CtrlName, strrpos($CtrlName, '\\') + 1);
        $CtrlName                                               = strtolower(str_replace("Controller", "", $CtrlName));
        $this->viewData['controller']                           = $CtrlName;
        $this->viewData['countries']                            = $this->CommonMethodsModel->getCountries();
		$this->viewData['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
        $filterCompletedTransaction['where']['txn_user']        = $this->sessionObj->userid;
        $filterCompletedTransaction['where']['pay_status != ?'] = 0;
        $userCompletedTransactions                              = $this->Myaccount->getUserTransactions($filterCompletedTransaction);
        $filterFutureTransaction['where']['txn_user']           = $this->sessionObj->userid;
        $filterFutureTransaction['where']['pay_status']         = 0;
        $userFutureTransactions                                 = $this->Myaccount->getUserTransactions($filterFutureTransaction);
        $this->layout()->hideSwitchUser                         = true;
        $this->layout()->CommonMethodsModel                     = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr                          = array(
            array(
                'label' => 'My Account',
                'title' => 'My Account',
                'active' => false,
                'redirect' => '/myaccount'
            ),
            array(
                'label' => 'Transaction History',
                'title' => 'Transaction History',
                'active' => true,
                'redirect' => false //'/myaccount/transaction-history'
            )
        );
        $this->viewData['flashMessages']                        = $this->flashMessenger()->getMessages();
        $this->viewData['active_tab']                           = "transaction_history";
        $this->viewData['comSessObj']                           = $this->sessionObj;
        $this->viewData['userCompletedTransactions']            = $userCompletedTransactions;
        $this->viewData['userFutureTransactions']               = $userFutureTransactions;
        $viewModel                                              = new ViewModel();
        $viewModel->setVariables($this->viewData);
        return $viewModel;
    }
    public function privacyAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->CommonPlugin           = $this->plugin('CommonPlugin');
        $CtrlName                     = $this->params('controller');
        $CtrlName                     = substr($CtrlName, strrpos($CtrlName, '\\') + 1);
        $CtrlName                     = strtolower(str_replace("Controller", "", $CtrlName));
        $this->viewData['controller'] = $CtrlName;
        $this->viewData['countries']  = $this->CommonMethodsModel->getCountries();
        $this->viewData['actnotif']   = $this->Myaccount->getactnotification($this->sessionObj->userid);
		$this->viewData['udetail']   = $this->Myaccount->getcancelaccount($this->sessionObj->userid);
		$this->viewData['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
        if ($this->getRequest()->isPost()) {
            if (isset($_POST['social_cont'])) {
                $social_cont = 0;
                if ($this->params()->fromPost('social_cont', false)) {
                    $social_cont = 1;
                }
                $arr_peopleabs_detail = array(
                    'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                    'social_cont' => $this->CommonMethodsModel->cleanQuery($social_cont)
                );
                $this->Myaccount->updateaccountnotif($arr_peopleabs_detail, $this->sessionObj->userid);
                echo $this->redirect()->toRoute('my-account', array(
                    'action' => 'privacy'
                ));
            }
            if (isset($_POST['search_engine'])) {
                $search_engine = 0;
                if ($this->params()->fromPost('search_engine', false)) {
                    $search_engine = 1;
                }
                $arr_peopleabs_detail = array(
                    'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                    'search_engine' => $this->CommonMethodsModel->cleanQuery($search_engine)
                );
                $this->Myaccount->updateaccountnotif($arr_peopleabs_detail, $this->sessionObj->userid);
                echo $this->redirect()->toRoute('my-account', array(
                    'action' => 'privacy'
                ));
            }
			
			 if (isset($_POST['personal_details'])) {
                $personal_phone_number	 = 0;
				$personal_email_address	 = 0;
				$personal_physical_address = 0;

				
                if ($this->params()->fromPost('personal_phone_number', false)) {
                    $personal_phone_number = 1;
                }
				 if ($this->params()->fromPost('personal_email_address', false)) {
                    $personal_email_address = 1;
                }
				 if ($this->params()->fromPost('personal_physical_address', false)) {
                    $personal_physical_address = 1;
                }
                $arr_peopleabs_detail = array(
                    'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                    'personal_phone_number' => $this->CommonMethodsModel->cleanQuery($personal_phone_number),
                    'personal_email_address' => $this->CommonMethodsModel->cleanQuery($personal_email_address),
					'personal_physical_address' => $this->CommonMethodsModel->cleanQuery($personal_physical_address),
					
                );
                $this->Myaccount->updateaccountnotif($arr_peopleabs_detail, $this->sessionObj->userid);
                echo $this->redirect()->toRoute('my-account', array(
                    'action' => 'privacy'
                ));
            }
			
			
			
			
			
        }
        $this->layout()->hideSwitchUser     = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr      = array(
            array(
                'label' => 'My Account',
                'title' => 'My Account',
                'active' => false,
                'redirect' => '/myaccount'
            ),
            array(
                'label' => 'Privacy',
                'title' => 'Privacy',
                'active' => true,
                'redirect' => false //'/myaccount/privacy'
            )
        );
        $this->viewData['flashMessages']    = $this->flashMessenger()->getMessages();
        $this->viewData['active_tab']       = "privacy";
        $this->viewData['comSessObj']       = $this->sessionObj;
        $viewModel                          = new ViewModel();
        $viewModel->setVariables($this->viewData);
        return $viewModel;
    }
    public function securityAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->CommonPlugin           = $this->plugin('CommonPlugin');
        $CtrlName                     = $this->params('controller');
        $CtrlName                     = substr($CtrlName, strrpos($CtrlName, '\\') + 1);
        $CtrlName                     = strtolower(str_replace("Controller", "", $CtrlName));
        $this->viewData['controller'] = $CtrlName;
        $this->viewData['countries']  = $this->CommonMethodsModel->getCountries();
        $this->viewData['actnotif']   = $this->Myaccount->getactnotification($this->sessionObj->userid);
		$this->viewData['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
        if ($this->getRequest()->isPost()) {
            $login_notif = 0;
            if ($this->params()->fromPost('login_notif', false)) {
                $login_notif = 1;
            }
            $arr_peopleabs_detail = array(
                'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'login_notif' => $this->CommonMethodsModel->cleanQuery($login_notif)
            );
            $result               = $this->Myaccount->updateaccountnotif($arr_peopleabs_detail, $this->sessionObj->userid);
            if ($result) {
                $this->flashMessenger()->addMessage(array(
                    'alert-success' => 'Login notification settings are updated successfully.'
                ));
            } else {
                $this->flashMessenger()->addMessage(array(
                    'alert-danger' => 'An error occured while updating login notification setting. Please try again.'
                ));
            }
            echo $this->redirect()->toRoute('my-account', array(
                'action' => 'security'
            ));
            if (isset($_POST['npass'])) {
                $oldpass    = $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('opass'));
                $conpass    = $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('cpass'));
                $newpass    = $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('npass'));
                $passResult = $this->Myaccount->updateuserpass($oldpass, $conpass, $newpass, $this->sessionObj->userid);
                if ($passResult['result'] == 'success') {
                    $this->flashMessenger()->addMessage(array(
                        'alert-success' => $passResult['msg']
                    ));
                } else if ($passResult['result'] == 'failure') {
                    $this->flashMessenger()->addMessage(array(
                        'alert-danger' => $passResult['msg']
                    ));
                }
                echo $this->redirect()->toRoute('my-account', array(
                    'action' => 'security'
                ));
            }
        }
        $this->layout()->hideSwitchUser     = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr      = array(
            array(
                'label' => 'My Account',
                'title' => 'My Account',
                'active' => false,
                'redirect' => '/myaccount'
            ),
            array(
                'label' => 'Security',
                'title' => 'Security',
                'active' => true,
                'redirect' => false //'/myaccount/setting'
            )
        );
        $this->viewData['flashMessages']    = $this->flashMessenger()->getMessages();
        $this->viewData['active_tab']       = "security";
        $this->viewData['comSessObj']       = $this->sessionObj;
        $viewModel                          = new ViewModel();
        $viewModel->setVariables($this->viewData);
        return $viewModel;
    }
    public function settingAction()
    {
		//print_r($_REQUEST);die;
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->CommonPlugin             = $this->plugin('CommonPlugin');
        $CtrlName                       = $this->params('controller');
        $CtrlName                       = substr($CtrlName, strrpos($CtrlName, '\\') + 1);
        $CtrlName                       = strtolower(str_replace("Controller", "", $CtrlName));
        $this->viewData['controller']   = $CtrlName;
        $this->viewData['countries']    = $this->CommonMethodsModel->getCountries();
        $this->viewData['cancelacount'] = $this->Myaccount->getcancelaccount($this->sessionObj->userid);
        $this->viewData['actnotif']     = $this->Myaccount->getactnotification($this->sessionObj->userid);
		$this->viewData['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
        if ($this->getRequest()->isPost()) {
            if (isset($_POST['tell_us'])) {
                $arr_peopleabs_detail = array(
                    'tell_us_more' => $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('tell_us_more')),
                    'tell_us' => $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('tell_us')),
                    'contact_more' => $this->CommonMethodsModel->cleanQuery($this->params()->fromPost('contact_more')),
                    'deleted' => 1,
					'active' =>0
                );
                $this->Myaccount->updatcanceleaccount($arr_peopleabs_detail, $this->sessionObj->userid);
                echo $this->redirect()->toRoute('logout');
            }
        }
        $this->layout()->hideSwitchUser     = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr      = array(
            array(
                'label' => 'My Account',
                'title' => 'My Account',
                'active' => false,
                'redirect' => false
            ),
            array(
                'label' => 'Setting',
                'title' => 'Setting',
                'active' => true,
                'redirect' => '/setting'
            )
        );
        $this->viewData['active_tab']       = "setting";
        $this->viewData['comSessObj']       = $this->sessionObj;
        $viewModel                          = new ViewModel();
        $viewModel->setVariables($this->viewData);
        return $viewModel;
    }
    public function addcardAction()
    {
        $this->CommonPlugin = $this->plugin('CommonPlugin');
		$this->TripPlugin             = $this->plugin('TripPlugin');
		
        /*$this->_helper->layout->disableLayout();        $this->_helper->viewRenderer->setNoRender(true);*/
        if ($_POST['dtype'] == 'addcard') {
            if (isset($_POST['carddef']) && $_POST['carddef'] == 1) {
                $cardf = $_POST['carddef'];
                $this->Myaccount->updatedefaddressval($this->params()->fromPost('editid', false));
            } else {
                $cardf = 0;
            }
			if(isset($_POST['sourceInfo'])){
			$userInfo = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			$sourceInfo = json_decode($_POST['sourceInfo'],true);
			
			if(isset($userInfo['stripe_id']) && $userInfo['stripe_id'] != ''){
				$customerInfo = $this->TripPlugin->attachStripeSource($sourceInfo['source']['id'],$userInfo['stripe_id']);
			} else {
				$customerInfo = $this->TripPlugin->createStripeCustomer($sourceInfo);
				$this->ProfileModel->updateprofile(array('stripe_id'=>$customerInfo['cus_id']),$this->sessionObj->userid);
			}
			
			if($customerInfo['err_msg'] != ''){
				echo json_encode(array('msg_status'=>'failure','msg'=>$customerInfo['err_msg']));exit;
			}
                        }
            $arr_peopleabs_detail = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'holder_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_name']),
                'holder_last_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_last_name']),
                /*'card_no' => base64_encode($_POST['card_number']),
                'card_type' => $this->CommonMethodsModel->cleanQuery($_POST['card_type']),
                'payment_card_type' => $this->CommonMethodsModel->cleanQuery($_POST['payment_card_type']),
                'issue_date' => $this->CommonMethodsModel->cleanQuery(isset($_POST['issue_date']) ? $_POST['issue_date'] : ''),
                'issue_number' => $this->CommonMethodsModel->cleanQuery(isset($_POST['issue_number']) ? $_POST['issue_number'] : ''),
                'card_valid' => $this->CommonMethodsModel->cleanQuery($_POST['card_valid']),
                'card_cvv' => $this->CommonMethodsModel->cleanQuery($_POST['card_cvv']),*/
                'card_billing_address' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_address']),
                'card_billing_country' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_country']),
                'card_billing_zipcode' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_zipcode']),
		//'stripe_src_id' => $this->CommonMethodsModel->cleanQuery($customerInfo['src_id']),
                'modified' => date('Y-m-d H:i:s')
            );
            $this->Myaccount->Addcarddetails($arr_peopleabs_detail);
        } else if ($_POST['dtype'] == 'delete') {
            $this->Myaccount->deletecarddetails($this->sessionObj->userid, $_POST['delid']);
        } else if ($_POST['dtype'] == 'view') {
            $jsonret = $this->Myaccount->viewcarddetails($this->sessionObj->userid, $_POST['viewid']);
            $jsary   = array(
                "ID" => $jsonret['ID'],
                "user" => $jsonret['user'],
                "holder_name" => $jsonret['holder_name'],
                "holder_last_name" => $jsonret['holder_last_name'],
                "card_no" => base64_encode($jsonret['card_no']),
                "card_valid" => $jsonret['card_valid'],
                "card_cvv" => $jsonret['card_cvv'],
                "card_type" => $jsonret['card_type'],
                'payment_card_type' => $jsonret['payment_card_type'],
                'issue_date' => $jsonret['issue_date'],
                'issue_number' => $jsonret['issue_number'],
                'card_billing_address' => $jsonret['card_billing_address'],
                'card_billing_country' => $jsonret['card_billing_country'],
                'card_billing_zipcode' => $jsonret['card_billing_zipcode']
            );
            echo json_encode($jsary);
        } else if ($_POST['dtype'] == 'update') {
            $arr_peopleabs_detail = array(
                'holder_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_name']),
                'holder_last_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_last_name']),
                'card_no' => base64_encode($_POST['card_number']),
                'card_valid' => $this->CommonMethodsModel->cleanQuery($_POST['card_valid']),
                'card_cvv' => $this->CommonMethodsModel->cleanQuery($_POST['card_cvv']),
                'card_type' => $this->CommonMethodsModel->cleanQuery($_POST['card_type']),
                'payment_card_type' => $this->CommonMethodsModel->cleanQuery($_POST['payment_card_type']),
                'issue_date' => $this->CommonMethodsModel->cleanQuery(isset($_POST['issue_date']) ? $_POST['issue_date'] : ''),
                'issue_number' => $this->CommonMethodsModel->cleanQuery(isset($_POST['issue_number']) ? $_POST['issue_number'] : ''),
                'card_billing_address' => $this->CommonMethodsModel->cleanQuery(isset($_POST['card_billing_address']) ? $_POST['card_billing_address'] : ''),
                'card_billing_country' => $this->CommonMethodsModel->cleanQuery(isset($_POST['card_billing_country']) ? $_POST['card_billing_country'] : ''),
                'card_billing_zipcode' => $this->CommonMethodsModel->cleanQuery(isset($_POST['card_billing_zipcode']) ? $_POST['card_billing_zipcode'] : ''),
                'modified' => date('Y-m-d H:i:s')
            );
            $this->Myaccount->updatecarddetails($arr_peopleabs_detail, $this->sessionObj->userid, $_POST['editid']);
        }
        exit;
    }
    function checkLoginStatus()
    {
        if ($this->sessionObj->userid != '' || $this->action == 'detail') {
            $user                   = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
            $this->viewData['user'] = $user;
            $this->user             = $user;
        } else {
            $this->sessionObj->triggerlogin = 'yes';
            $url                            = $this->generalvar['nosslurl'];
            echo $this->redirect()->toRoute($url);
        }
    }
	public function extrainnAction(){
		//print_r($this->sessionObj->userid);die;
		$arr_detail = array(
                'show_phone' => $this->CommonMethodsModel->cleanQuery($_POST['show_phone']),
                'show_email' => $this->CommonMethodsModel->cleanQuery($_POST['show_email']),
                'show_address' => $this->CommonMethodsModel->cleanQuery($_POST['show_address'])
            );
			$res = $this->CommonMethodsModel->abcdefg($arr_detail, $this->sessionObj->userid);
           echo '1';
		   exit;
	}
	public function addremovecardasdefaultAction(){
		$id = $this->CommonMethodsModel->cleanQuery($_POST['id']);
        $type = $this->CommonMethodsModel->cleanQuery($_POST['type']);
        $res = $this->Myaccount->addremovecardasdefault($id, $this->sessionObj->userid,$type);
         echo '1';
		 exit;
	}
    
	
}
?>