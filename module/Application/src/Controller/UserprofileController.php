<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;
 
use Zend\View\Helper\AbstractHelper;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Renderer\RendererInterface;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager;
use Zend\Uri\Uri;
use Zend\Http\Client;
use Zend\Http\Client\Adapter\Curl;
use Zend\Json\Json;
use Zend\Mvc\MvcEvent;
use Zend\Mail;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

/* use Application\Controller\MailModel; */
require_once(ACTUAL_ROOTPATH . "Upload/src/Upload.php");
require_once(ACTUAL_ROOTPATH . 'hybridauth/Hybrid/Auth.php');
require_once(ACTUAL_ROOTPATH.'hyb/src/autoload.php');
/*require_once(ACTUAL_ROOTPATH . 'social-login/facebook/src/facebook.php');
require_once(ACTUAL_ROOTPATH . 'facebook-graph-sdk/src/Facebook/autoload.php');
require_once(ACTUAL_ROOTPATH . 'hybridauth/Hybrid/thirdparty/OAuth/OAuth2Client.php');
require_once(ACTUAL_ROOTPATH . 'OAuth2/Client.php');
require_once(ACTUAL_ROOTPATH . 'traity/jwt/src/JWT.php');
use \Firebase\JWT\JWT;*/
use Hybridauth\Exception\Exception;
use Hybridauth\Hybridauth;
use Hybridauth\HttpClient;
use Hybridauth\Storage\Session;

/* require_once ACTUAL_ROOTPATH.'facebook-graph-sdk/src/Facebook/FacebookRequest.php';  */

class UserprofileController extends AbstractActionController
{
    var $sessionObj;
    var $CommonPlugin;
    var $GoogleplusPlugin;
    var $siteConfigs;
    var $basePath;
    var $hybridConfig;
    var $facebook;
    var $fb;
    protected $serviceLocator;

    public function __construct($data = false)
    {
        /*Loading models from factory
         * Call the model object using it's class name
         */
         /*$this->hybridConfig = include(ACTUAL_ROOTPATH . 'hybridauth/config.php');*/
         $this->hybridConfig = include(ACTUAL_ROOTPATH . 'hyb/src/config.php');

       /* $this->facebook = include(ACTUAL_ROOTPATH . 'social-login/facebook/fbconfig.php');
        $this->fb = new \Facebook\Facebook([
            'app_id' => '1729839597282171',
            'app_secret' => '11ce794c85a6907dcbdc9eb1c2da38e1',
            'default_graph_version' => 'v2.5',
        ]);*/
        /*var_dump($this->hybridConfig);exit; */
        if ($data['models'] && is_array($data['models'])) {
            foreach ($data['models'] as $model) {
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->timeZones = $data['configs']['timezones'];
    }

    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        if (isset($this->sessionObj->userid) && $this->sessionObj->userid != '') {
            $isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
            if (!$isUserLoggedIn) {
                $this->redirect()->toRoute('logout');
            }
        }
        return parent::onDispatch($e);
    }

    /*public function onDispatch(\Zend\Mvc\MvcEvent $e){
        if(!($this->sessionObj->userid) || $this->sessionObj->userid == ''){
            $this->redirect()->toRoute('home');
        }
        return parent::onDispatch($e);
    }*/

    public function indexAction()
    { 
        /*echo '<pre>';print_r($this->sessionObj->user);exit; */
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        /* echo '<pre>';var_dump(unserialize(file_get_contents("http://vimeo.com/api/v2/video/8733915.php")));exit; */
        $viewArray = array();
        $viewModel = new ViewModel();
        /*//$this->_helper->layout->disableLayout();
        //$this->view->upcoming_trips =  $this->_helper->Trip->getTrips('upcoming');
        //$this->view->completed_trips =  $this->_helper->Trip->getTrips('completed');
        //$this->view->cancelled_trips =  $this->_helper->Trip->getTrips('cancelled');*/
        if (isset($_POST['subabs'])) {
            $this->sessionObj->personalInforTabSelected = 'profile_detail';
           
            if(isset($_POST['aboutus']))
            {
                $arr_peopleabs_detail = array(
                    'aboutus' => $this->CommonMethodsModel->cleanQuery($_POST['aboutus']),
                    'modified' => time()
                );

            $this->ProfileModel->updateprofileabus($arr_peopleabs_detail, $this->sessionObj->offsetGet('userid'));
            }
            $this->redirect()->toRoute('myprofile');
        }

        if (isset($_POST['photoUpload'])) {

            $this->sessionObj->personalInforTabSelected = 'photoVideo';
            if ($_FILES['upfile']['name']) {
//print_r($_FILES);die;
                $upFiles = array();
                foreach ($_FILES['upfile'] as $k => $l) {
                    foreach ($l as $i => $v) {
                        if (!array_key_exists($i, $upFiles)) $upFiles[$i] = array();
                        $upFiles[$i][$k] = $v;
                    }
                }
                $successCount = 0;
                $errCount = 0;
                $maxError = 0;
                $image_path = "profile_picture";
                $field_name = 'upfile';
                foreach ($upFiles as $file) {
                    $file_name_prefix = 'USER_DP_' . time();
                    $configArray = array(
                        array(
                            'image_x' => 150,
                            'sub_dir_name' => 'medium'
                        ),
                        array(
                            'image_x' => 100,
                            'sub_dir_name' => 'small'
                        )
                    );
                    $imgResult['result'] = 'success';//$this->uploadUserDPMultiple($file,$file_name_prefix,$image_path,$configArray);
                    $ext = pathinfo($_FILES['upfile']['name'][0], PATHINFO_EXTENSION);
                    if (move_uploaded_file($_FILES['upfile']['tmp_name'][0], ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext)) {
                        copy(ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext, ACTUAL_ROOTPATH . 'uploads/profile_picture/medium/' . $file_name_prefix . '.' . $ext);
                    }
                    //move_uploaded_file($_FILES['upfile']['tmp_name'][0], ACTUAL_ROOTPATH.'uploads/profile_picture/medium/'.$file_name_prefix.'.'.$ext);
                    //move_uploaded_file($_FILES['upfile']['tmp_name'][0], ACTUAL_ROOTPATH.'uploads/'.$image_path.'/small/'.$file_name_prefix.'.'.$ext);
                    if ($imgResult['result'] == 'success') {
                        $idverfy_image = $file_name_prefix . '.' . $ext;//$imgResult['fileName'];
                        //echo ACTUAL_ROOTPATH.'uploads/'.$image_path.'/'.$file_name_prefix.'.'.$ext.','. ACTUAL_ROOTPATH.'uploads/profile_picture/small/'.$idverfy_image;die;
                        copy(ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext, ACTUAL_ROOTPATH . 'uploads/profile_picture/small/' . $idverfy_image);
                        $arr_photo_detail = array(
                            'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')),
                            'user_photo_name' => $this->CommonMethodsModel->cleanQuery($idverfy_image),
                            'user_photo_added' => time()
                        );
						
						$arr_photo_detail_marked = array(
                            'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')),
                            'user_photo_name' => $this->CommonMethodsModel->cleanQuery($idverfy_image),
							 'marked_as_profile_photo' =>1,
														 
                             'user_photo_added' => time()
                        );
						
								$userArr = array(
								'image' => $idverfy_image,
								'modified' => time()
                );
                         $existingCount = $this->ProfileModel->getUserPhotos($this->sessionObj->offsetGet('userid'), 'count');
					   
                        if ($existingCount < $this->siteConfigs['max_user_photo_upload']) {
								
									if($existingCount == '0')
									{
										$this->ProfileModel->addUserPhoto($arr_photo_detail_marked, $this->sessionObj->offsetGet('userid'));
										
										$this->ProfileModel->updateprofile($userArr, $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')));										


									} else {
										$this->ProfileModel->addUserPhoto($arr_photo_detail, $this->sessionObj->offsetGet('userid'));
									}
										$successCount++;
                        } else {
                            if ($maxError == 0) {
                                $this->flashMessenger()->addMessage(array('alert-danger' => 'Sorry! You can only add maximum ' . $this->siteConfigs['max_user_photo_upload'] . ' photos.'));
                                $maxError++;
                            }
                            $this->redirect()->toRoute('myprofile');
                        }
                    } else {
                        $errCount++;
                    }
                }

                if ($successCount > 0) {
                    $this->flashMessenger()->addMessage(array('alert-success' => 'Your uploaded photo(s) have been added successfully.'));
                    // echo 'hello';die;
                } else if ($maxError > 0) {
                    $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while uploading photo. Please try again.'));
                }
                $this->redirect()->toRoute('myprofile');
            }

            /*if($_FILES['upfile']['name']){
                //echo '<pre>';print_r($_FILES['upfile']);exit;
                //$image_path = $this->generalvar['temp'];
                $image_path = "profile_picture";
                $field_name = 'upfile';
                $file_name_prefix = 'USER_DP_'.time();
                $configArray = array(
                    array(
                        'image_x' => 150,
                        'sub_dir_name' => 'medium'
                    ),
                    array(
                        'image_x' => 100,
                        'sub_dir_name' => 'small'
                    )
                );
                $imgResult = $this->uploadUserDP($field_name,$file_name_prefix,$image_path,$configArray);
                if($imgResult['result'] == 'success'){
                    $idverfy_image = $imgResult['fileName'];
                    $arr_photo_detail = array(
                        'user_id' 	=> $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')),
                        'user_photo_name'	=> $this->CommonMethodsModel->cleanQuery($idverfy_image),
                        'user_photo_added' 	=> time()
                    );
                    $this->ProfileModel->addUserPhoto($arr_photo_detail, $this->sessionObj->offsetGet('userid'));
                    $this->sessionObj->offsetSet('successMsg', 'Your uploaded photo has been added successfully.');
                }
                else{
                    $this->sessionObj->offsetSet('errorMsg', 'An error has occurred while uploading photo. Please try again.');   
                }
            }
            $this->redirect()->toRoute('myprofile');*/
        }


        if (isset($_POST['deletePhtoId'])) {
            $this->sessionObj->personalInforTabSelected = 'photoVideo';
            $photoId = $this->CommonMethodsModel->cleanQuery($_POST['deletePhtoId']);
            $getDPName = $this->ProfileModel->getDPName($photoId);
            $res = $this->ProfileModel->removeUserPhoto($photoId);
            if ($res) {
                $imge = 'default_user.png';
                if ($this->sessionObj->user['image'] == $getDPName[0]['user_photo_name']) {
                    $userArry = array(
                        'image' => $imge,
                        'modified' => time()
                    );
                    $this->ProfileModel->updateprofile($userArry, $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')));
                    $this->sessionObj->user['image'] = $imge;
                }
                //echo $this->sessionObj->user['image'];die;
                $this->flashMessenger()->addMessage(array('alert-success' => 'Photo has been deleted successfully.'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while deleting photo. Please try again.'));
            }
            $this->redirect()->toRoute('myprofile');
        }

        if (isset($_POST['markAsDP'])) {
            $this->sessionObj->personalInforTabSelected = 'photoVideo';

            $photoId = $this->CommonMethodsModel->cleanQuery($_POST['markAsDP']);
            $imageName = $this->CommonMethodsModel->cleanQuery($_POST['photoName']);
            $res = $this->ProfileModel->markAsProfilePhoto($photoId, $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')));

            if ($res) {
                $userArr = array(
                    'image' => $imageName,
                    'modified' => time()
                );
               $this->ProfileModel->updateprofile($userArr, $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')));
                $this->sessionObj->user['image'] = $imageName;
                $this->flashMessenger()->addMessage(array('alert-success' => 'Selected photo has been marked as your profile picture.'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while making this photo as profile picture. Please try again.'));
            }
            $this->redirect()->toRoute('myprofile');
        }

        if (isset($_POST['videoUrl'])) {
            $this->sessionObj->personalInforTabSelected = 'photoVideo';
            $videoUrl = $this->CommonMethodsModel->cleanQuery($_POST['videoUrl']);

            $videoArr = array(
                'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')),
                'user_video_url' => $this->CommonMethodsModel->cleanQuery($videoUrl),
                'video_parser_type' => $this->CommonMethodsModel->cleanQuery($_POST['parserType']),
                'video_url_id' => $this->CommonMethodsModel->cleanQuery($_POST['videoUrlId']),
                'user_video_added' => time()
            );
            $res = $this->ProfileModel->addUserVideo($videoArr, $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')));
            if ($res) {
                $this->sessionObj->user['image'] = false;
                $this->flashMessenger()->addMessage(array('alert-success' => 'Video link has been added successfully.'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while adding video link. Please try again.'));
            }
            $this->redirect()->toRoute('myprofile');
        }

        if (isset($_POST['deleteVideoId'])) {
            $this->sessionObj->personalInforTabSelected = 'photoVideo';
            $videoId = $this->CommonMethodsModel->cleanQuery($_POST['deleteVideoId']);
            $res = $this->ProfileModel->removeUserVideo($videoId);
            if ($res) {
                $this->flashMessenger()->addMessage(array('alert-success' => 'Video has been deleted successfully.'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while deleting video. Please try again.'));
            }
            $this->redirect()->toRoute('myprofile');
        }

        if (isset($_POST['verfyid'])) {
            $image_path = $this->generalvar['trips']['trips']['identy'];
            $field_name = 'idverfy';
            $file_name_prefix = $this->generalvar['trips']['trips']['filename'];
            $idverfy_image = $this->CommonMethodsModel->uploadFile($field_name, $file_name_prefix, $image_path);
            $arr_peoplevery_detail = array(
                'verfiyid' => $this->CommonMethodsModel->cleanQuery($_POST['verfiyid']),
                'nationalid' => $this->CommonMethodsModel->cleanQuery($_POST['nationalid']),
                'user_id' => $this->sessionObj->offsetGet('userid'),
                'image' => $this->CommonMethodsModel->cleanQuery($idverfy_image),
                'modified' => time()
            );
            $this->ProfileModel->updateprofileverfy($arr_peoplevery_detail, $this->sessionObj->userid);
        }

        if (isset($_POST['firstname'])) {
            $this->sessionObj->personalInforTabSelected = 'profile_detail';
            $dateb = str_replace("/", "-", $_POST['brithday']);

            $dateb2 = strtotime($dateb);
            $language = implode(",", $_POST['language']);
            $birthDate = date("m/d/Y", strtotime($dateb));
            $birthDate = explode("/", $birthDate);
            $age = (
            date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md")
                ? ((date("Y") - $birthDate[2]) - 1)
                : (date("Y") - $birthDate[2])
            );

            $arr_people_detail = array(
                'first_name' => $this->CommonMethodsModel->cleanQuery($_POST['firstname']),
                'last_name' => $this->CommonMethodsModel->cleanQuery($_POST['lastname']),
                'dob' => $this->CommonMethodsModel->cleanQuery($dateb2),
                'age' => $this->CommonMethodsModel->cleanQuery($age),
                'phone' => $this->CommonMethodsModel->cleanQuery($_POST['phone']),
                'country' => $this->CommonMethodsModel->cleanQuery(($_POST['country']) ? $_POST['country'] : NULL),
                'city' => $this->CommonMethodsModel->cleanQuery(($_POST['city']) ? $_POST['city'] : NULL),
                'school' => $this->CommonMethodsModel->cleanQuery(($_POST['school']) ? $_POST['school'] : NULL),
                'work' => $this->CommonMethodsModel->cleanQuery(($_POST['work']) ? $_POST['work'] : NULL),
                'timezone' => $this->CommonMethodsModel->cleanQuery(($_POST['timezone']) ? $_POST['timezone'] : NULL),
                'emergency_full_name' => $this->CommonMethodsModel->cleanQuery(($_POST['emergency_full_name']) ? $_POST['emergency_full_name'] : NULL),
                'emergency_phone' => $this->CommonMethodsModel->cleanQuery(($_POST['emergency_phone']) ? $_POST['emergency_phone'] : NULL),
                'emergency_email' => $this->CommonMethodsModel->cleanQuery(($_POST['emergency_email']) ? $_POST['emergency_email'] : NULL),
                'emergency_relationship' => $this->CommonMethodsModel->cleanQuery(($_POST['emergency_relationship']) ? $_POST['emergency_relationship'] : NULL),
                'aboutus' => $this->CommonMethodsModel->cleanQuery($_POST['aboutus']),
                'language' => $this->CommonMethodsModel->cleanQuery($language),
                'gender' => $this->CommonMethodsModel->cleanQuery($_POST['gender']),
                'modified' => time()
            );
            $maildef = $this->CommonMethodsModel->cleanQuery($_POST['mailaddr']);
            $this->ProfileModel->updateprofile($arr_people_detail, $this->sessionObj->offsetGet('userid'));
            $this->sessionObj->user = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
            $redirectUri = $this->params()->fromPost('redirectUrl');
            $uriArr = (explode('/', $redirectUri));
            if ($redirectUri != '' && $uriArr[1] == 'listingandconfirm') {
                return $this->redirect()->toRoute($uriArr[0] . '-listingandconfirm', array('trip_id' => $uriArr[2]));
                exit;
            }
            $this->redirect()->toRoute('myprofile');
        }

        /* referral section */
        if (isset($_POST['referralEmails'])) {
            $currProf = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
            $referralEmails = $emailIds = array_filter(explode(';', $this->CommonMethodsModel->cleanQuery($_POST['referralEmails'])));
            $alreadyExistMails = $this->ProfileModel->getInviteMails($this->sessionObj->offsetGet('userid'));
            if ($alreadyExistMails) {
                foreach ($alreadyExistMails as $exist) {
                    $existMails[] = $exist['invited_email'];
                }
            }
			
            if (isset($currProf['image']) && !empty($currProf['image'])) {
                if (strpos($currProf['image'], "http") !== false) {
                    $referrerPhotoUrl = $currProf['image'];
                } else {
                    $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'public/uploads/profile_picture/medium/' . $currProf['image'];
                }
            } else {
                $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'default_user.png';
            }
			
            // $signUpLink = $this->siteConfigs['mail_url'] . '?signup=true&referrer=' . $currProf['user_key'];
            // $signUpLink = 'http://trepr.com?signup=true&referrer=' . $currProf['user_key'].'&senderID='.base64_encode($this->sessionObj->offsetGet('userid'));
            $signUpLink = $this->siteConfigs['mail_url'] . '?signup=true&referrer=' . $currProf['user_key'] . '&senderID=' . base64_encode($this->sessionObj->offsetGet('userid'));
            $userId = $this->sessionObj->offsetGet('userid');
			
			$refemail_exist = array_intersect($referralEmails, $existMails);
			
			if (count($refemail_exist) == '0') {
                $count = 0;
                foreach ($referralEmails as $mailId) { 	

					$queryData[] = array(
                            'user_id' => $userId,
                            'invited_email' => $mailId,
                            'invite_added' => time()
                        );

                   /* if (!in_array($mailId, $existMails)) { 
                        $queryData[] = array(
                            'user_id' => $userId,
                            'invited_email' => $mailId,
                            'invite_added' => time()
                        );
                    } else  {
								
						$this->flashMessenger()->addMessage(array('alert-danger' => 'Email already exist'));
						$this->redirect()->toRoute('myprofile');
					
					}*/
					
					
                    $mailData = array(
                        'FULL_NAME' => $currProf['first_name'],
                        'REFERRED_PHOTO_URL' => $referrerPhotoUrl,
                        'REFERRAL_EMAIL' => $mailId,
                        'SIGNUP_LINK' => $signUpLink . '&refTo=' . base64_encode($mailId),
                    );

                    try
                    {
                        $this->MailModel->sendReferralMail($mailData);
                    }
                    catch (\Exception $e)
                    {
                        error_log($e);
                    }

                    $count++;
                }
            } else {
						$this->flashMessenger()->addMessage(array('alert-danger' => 'You have already invited this email'));
						$this->redirect()->toRoute('myprofile');
						
		
			}
            if (!empty($queryData)) {
                $lastId = $this->ProfileModel->addInviteMails($queryData);
            }
            if ($count) {
                $this->flashMessenger()->addMessage(array('alert-success' => 'Your referral invite  e-mail(s) have been sent successfully!'));
            }
            $this->redirect()->toRoute('myprofile');
        }

        /*
        if(isset($_POST['reference_response_id'])){
            $reference_id = $_POST['reference_response_id'];
            if(isset($_POST['writeReference'])){
                $responseData['reference_message'] = $this->CommonMethodsModel->cleanQuery($_POST['referenceMessage']);
            }
            $resId = $this->ProfileModel->updateReferenceResponse($reference_id, $responseData);
            if($resId){
                $this->flashMessenger()->addMessage(array('alert-success' => 'Your reference has been edited successfully!'));
            }
            else{
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to edit your reference. Please try again.'));
            }
            $this->redirect()->toRoute('myprofile');
        }
        */

        $this->layout()->hideSwitchUser = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'My Profile',
                'title' => 'My Profile',
                'active' => false,
                'redirect' => '/myprofile'
            ),
            array(
                'label' => 'Personal Info',
                'title' => 'Personal Info',
                'active' => true,
                'redirect' => false //'/myprofile'
            )
        );
        $viewArray['profile_model'] = $this->ProfileModel;
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        $viewArray['queryString'] = $this->getRequest()->getQuery();
        $viewArray['fb_app_id'] = $this->siteConfigs['facebook_appId'];
        $viewArray['timeZones'] = $this->timeZones;
        $viewArray['prof'] = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));

        $this->sessionObj['user']['image'] = $viewArray['prof']['image'];
        $viewArray['country_detail'] = $this->CommonMethodsModel->getCountryByCode($viewArray['prof']['co_code']);
        $viewArray['profFormAction'] = ($this->params()->fromQuery('profAction') != '') ? $this->params()->fromQuery('profAction') : 'view';
        $viewArray['profPhotos'] = $this->ProfileModel->getUserPhotos($this->sessionObj->offsetGet('userid'));
		$viewArray['profPhotosMarked'] = $this->ProfileModel->getUserPhotosMarked($this->sessionObj->offsetGet('userid'));

        $viewArray['profVideos'] = $this->ProfileModel->getUserVideos($this->sessionObj->offsetGet('userid'));
        $viewArray['profaddr'] = $this->ProfileModel->getprofileaddr($this->sessionObj->offsetGet('userid'));
        $viewArray['profaddrmail'] = $this->ProfileModel->getprofileaddrmail($this->sessionObj->offsetGet('userid'));
        $viewArray['lang'] = $this->ProfileModel->getlang($this->sessionObj->offsetGet('userid'));
        /*var_dump($viewArray['lang']);exit;*/
        $viewArray['languages'] = $this->CommonMethodsModel->getLanguages();
        $viewArray['time_zone'] = $this->CommonMethodsModel->time_zone();
        $viewArray['abtus'] = $this->ProfileModel->getabtus($this->sessionObj->offsetGet('userid'));
        $viewArray['reviews'] = $this->ProfileModel->getreviews($this->sessionObj->offsetGet('userid'));
        $viewArray['notiferr'] = $this->ProfileModel->getprofilenotification($this->sessionObj->offsetGet('userid'));
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewArray['userInvites'] = $this->ProfileModel->getUserInviteDetails($this->sessionObj->offsetGet('userid'));
        $viewArray['flashMessages'] = $this->flashMessenger()->getMessages();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }


    /* my addresses section */
    public function myAddressesAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        /* referral section */
        if (isset($_POST['referralEmails'])) {

            $currProf = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
            $referralEmails = $emailIds = array_filter(explode(';', $this->CommonMethodsModel->cleanQuery($_POST['referralEmails'])));
            $alreadyExistMails = $this->ProfileModel->getInviteMails($this->sessionObj->offsetGet('userid'));
            if ($alreadyExistMails) {
                foreach ($alreadyExistMails as $exist) {
                    $existMails[] = $exist['invited_email'];
                }
            }
            /*if(!empty($existMails)){
                $referralEmails = array_unique(array_merge($emailIds,$existMails));
            }
            else{
                $referralEmails = $emailIds;
            }*/
            if (isset($currProf['image']) && !empty($currProf['image'])) {
                if (strpos($currProf['image'], "http") !== false) {
                    $referrerPhotoUrl = $currProf['image'];
                } else {
                    $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'public/uploads/profile_picture/medium/' . $currProf['image'];
                }
            } else {
                $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'default_user.png';
            }
            $signUpLink = $this->siteConfigs['mail_url'] . '?signup=true&referrer=' . $currProf['user_key'];
            $userId = $this->sessionObj->offsetGet('userid');
            if (!empty($referralEmails)) {
                $count = 0;
                foreach ($referralEmails as $mailId) {
                    if (!in_array($mailId, $existMails)) {
                        $queryData[] = array(
                            'user_id' => $userId,
                            'invited_email' => $mailId,
                            'invite_added' => time()
                        );
                    }
                    $mailData = array(
                        'FULL_NAME' => $currProf['first_name'],
                        'REFERRED_PHOTO_URL' => $referrerPhotoUrl,
                        'REFERRAL_EMAIL' => $mailId,
                        'SIGNUP_LINK' => $signUpLink . '&refTo=' . base64_encode($mailId),
                    );

                    try
                    {
                        $this->MailModel->sendReferralMail($mailData);
                    }
                    catch (\Exception $e)
                    {
                        error_log($e);
                    }

                    $count++;
                }
            }
            if (!empty($queryData)) {
                $lastId = $this->ProfileModel->addInviteMails($queryData);
            }
            if ($count) {
                $this->flashMessenger()->addMessage(array('alert-success' => 'Your referral invite e-mail(s) have been sent successfully!'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while sending your referral e-mail(s). Please try again.'));
            }
            $this->redirect()->toRoute('myprofile', array('action' => 'myAddresses'));
        }
        $this->layout()->hideSwitchUser = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'My Profile',
                'title' => 'My Profile',
                'active' => false,
                'redirect' => '/myprofile'
            ),
            array(
                'label' => 'My Addresses',
                'title' => 'My Addresses',
                'active' => true,
                'redirect' => false //'/myprofile/myAddresses'
            )
        );
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        $viewArray['profaddrmail'] = $this->ProfileModel->getprofileaddrmail($this->sessionObj->offsetGet('userid'));
        $viewArray['addrbook'] = $this->CommonMethodsModel->LoadUsersAddressBook($this->sessionObj->offsetGet('userid'));
        $viewArray['prof'] = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
        $viewArray['reviews'] = $this->ProfileModel->getreviews($this->sessionObj->userid);
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewArray['flashMessages'] = $this->flashMessenger()->getMessages();
        $viewArray['userInvites'] = $this->ProfileModel->getUserInviteDetails($this->sessionObj->userid);
		$viewArray['profPhotosMarked'] = $this->ProfileModel->getUserPhotosMarked($this->sessionObj->offsetGet('userid'));

        $viewModel = new ViewModel();
        $this->layout()->pageFunction = 'myAddresses';
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }
    /* my addresses section */

	
	public function widget_signature($app_key, $app_secret, $current_user_id,$options = array()) {
          $payload = array_merge(array(
            'time' => time(),
            'current_user_id' => $current_user_id), $options);

          
}
	
    /* trust verification section*/
    public function trustVerificationAction()
    { 
	
	
		/*$options   = array('verified' => array('phone', 'email'));
	echo $signature =$this->widget_signature('soisc6a4gvxffsnid6hf', '3v0y1lct8a8svyvqi5zii1rl2gmuf1qs', 2,                          $options);die;*/
	
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        /*referral section*/
        if (isset($_POST['referralEmails'])) {

            $currProf = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
			

            $referralEmails = $emailIds = array_filter(explode(';', $this->CommonMethodsModel->cleanQuery($_POST['referralEmails'])));
            $alreadyExistMails = $this->ProfileModel->getInviteMails($this->sessionObj->offsetGet('userid'));
            if ($alreadyExistMails) {
                foreach ($alreadyExistMails as $exist) {
                    $existMails[] = $exist['invited_email'];
                }
            }
            /*if(!empty($existMails)){
                $referralEmails = array_unique(array_merge($emailIds,$existMails));
            }
            else{
                $referralEmails = $emailIds;
            }*/
            if (isset($currProf['image']) && !empty($currProf['image'])) {
                if (strpos($currProf['image'], "http") !== false) {
                    $referrerPhotoUrl = $currProf['image'];
                } else {
                    $referrerPhotoUrl = $this->siteConfigs['mail_url'] . '/uploads/profile_picture/medium/' . $currProf['image'];
                }
            } else {
                $referrerPhotoUrl = $this->siteConfigs['mail_url'] . '/default_user.png';
            }
            $signUpLink = $this->siteConfigs['mail_url'] . '?signup=true&referrer=' . $currProf['user_key'];
            $referrerPhotoUrl = $this->siteConfigs['mail_url'];
            $userId = $this->sessionObj->offsetGet('userid');
            if (!empty($referralEmails)) {
                $count = 0;
                foreach ($referralEmails as $mailId) {
                    if (!in_array($mailId, $existMails)) {
                        $queryData[] = array(
                            'user_id' => $userId,
                            'invited_email' => $mailId,
                            'invite_added' => time()
                        );
                    }
                    $mailData = array(
                        'FULL_NAME' => $currProf['first_name'],
                        'REFERRED_PHOTO_URL' => $referrerPhotoUrl,
                        'REFERRAL_EMAIL' => $mailId,
                        'SIGNUP_LINK' => $signUpLink . '&refTo=' . base64_encode($mailId),
                    );

                    try
                    {
                        $this->MailModel->sendReferralMail($mailData);
                    }
                    catch (\Exception $e)
                    {
                        error_log($e);
                    }

                    $count++;
                }
            }
            if (!empty($queryData)) {
                $lastId = $this->ProfileModel->addInviteMails($queryData);
            }
            if ($count) {
                $this->flashMessenger()->addMessage(array('alert-success' => 'Your referral invite e-mail(s) have been sent successfully!'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while sending your referral e-mail(s). Please try again.'));
            }
            $this->redirect()->toRoute('myprofile', array('action' => 'trustVerification'));
        }

        $app_key = 'IL8KFOnvyHZwTfYkPJIPg';//$this->siteConfigs['traity_api_key'];
        $app_secret = 'bd6hUNAiy2etdTedE36glqRI2b11SpIk2OA';//$this->siteConfigs['traity_secret_key'];

        $TRAITY_API = 'https://api.traity.com';
        $CURRENT_USER_ID = $this->sessionObj->offsetGet('userid');

     //   $api = new \OAuth2\Client($app_key, $app_secret);

        //$result = $api->getAccessToken($TRAITY_API.'/oauth/token',
        // 'client_credentials', array());
        //print_r($api);die;
        // $token = $result['result']['access_token'];
        // $api->setAccessToken($token);
        //$api->setAccessTokenType(\OAuth2\Client::ACCESS_TOKEN_BEARER);

       
        $this->layout()->hideSwitchUser = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'My Profile',
                'title' => 'My Profile',
                'active' => false,
                'redirect' => '/myprofile'
            ),
            array(
                'label' => 'Trust & Verification',
                'title' => 'Trust & Verification',
                'active' => true,
                'redirect' => false //'/myprofile/trustVerification'
            )
        );
		
		//    echo $this->sessionObj->offsetGet('userid');die;
        $viewArray['prof'] = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
        $viewArray['reviews'] = $this->ProfileModel->getreviews($this->sessionObj->offsetGet('userid'));
        $viewArray['userVerfication'] = $this->ProfileModel->getVerifyDetails($this->sessionObj->offsetGet('userid'));
        $viewArray['idTypes'] = $this->ProfileModel->getIdentityTypes($this->sessionObj->offsetGet('userid'));
       // $viewArray['widget_signature'] = $JWT->encode($signature, '');
        $viewArray['prof_user_id'] = $this->sessionObj->offsetGet('userid');
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewArray['flashMessages'] = $this->flashMessenger()->getMessages();
        $viewArray['userInvites'] = $this->ProfileModel->getUserInviteDetails($this->sessionObj->userid);
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
		$viewArray['profPhotosMarked'] = $this->ProfileModel->getUserPhotosMarked($this->sessionObj->offsetGet('userid'));

        $options   = array('verified' => array('phone', 'email'));
        $traity_signature =$this->widget_signature(
                                            'soisc6a4gvxffsnid6hf', 
                                            '3v0y1lct8a8svyvqi5zii1rl2gmuf1qs', 
                                            $this->sessionObj->offsetGet('userid'),                          
                                            $options
                                    );

        $viewArray['widget_signature_traity'] =  $traity_signature;
        $viewArray['prof_user_id_traity'] = $this->sessionObj->offsetGet('userid');


        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }
    /* trust verification section */

    /* my circle section */
    public function myCircleAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        /* referral section */
        if (isset($_POST['referralEmails'])) {

            $currProf = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
            $referralEmails = $emailIds = array_filter(explode(';', $this->CommonMethodsModel->cleanQuery($_POST['referralEmails'])));
            $alreadyExistMails = $this->ProfileModel->getInviteMails($this->sessionObj->offsetGet('userid'));
            if ($alreadyExistMails) {
                foreach ($alreadyExistMails as $exist) {
                    $existMails[] = $exist['invited_email'];
                }
            }
            /*if(!empty($existMails)){
                $referralEmails = array_unique(array_merge($emailIds,$existMails));
            }
            else{
                $referralEmails = $emailIds;
            }*/
            if (isset($currProf['image']) && !empty($currProf['image'])) {
                if (strpos($currProf['image'], "http") !== false) {
                    $referrerPhotoUrl = $currProf['image'];
                } else {
                    $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'public/uploads/profile_picture/medium/' . $currProf['image'];
                }
            } else {
                $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'default_user.png';
            }
            $signUpLink = $this->siteConfigs['mail_url'] . '?signup=true&referrer=' . $currProf['user_key'];
            $userId = $this->sessionObj->offsetGet('userid');
            if (!empty($referralEmails)) {
                $count = 0;
                foreach ($referralEmails as $mailId) {
                    if (!in_array($mailId, $existMails)) {
                        $queryData[] = array(
                            'user_id' => $userId,
                            'invited_email' => $mailId,
                            'invite_added' => time()
                        );
                    }
                    $mailData = array(
                        'FULL_NAME' => $currProf['first_name'],
                        'REFERRED_PHOTO_URL' => $referrerPhotoUrl,
                        'REFERRAL_EMAIL' => $mailId,
                        'SIGNUP_LINK' => $signUpLink . '&refTo=' . base64_encode($mailId),
                    );
                    try
                    {
                        $this->MailModel->sendReferralMail($mailData);
                    }
                    catch (\Exception $e)
                    {
                        error_log($e);
                    }
                    $count++;
                }
            }
            if (!empty($queryData)) {
                $lastId = $this->ProfileModel->addInviteMails($queryData);
            }
            if ($count) {
                $this->flashMessenger()->addMessage(array('alert-success' => 'Your referral invite e-mail(s) have been sent successfully!'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while sending your referral e-mail(s). Please try again.'));
            }
            $this->redirect()->toRoute('myprofile', array('action' => 'myCircle'));
        }
        $this->layout()->hideSwitchUser = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'My Profile',
                'title' => 'My Profile',
                'active' => false,
                'redirect' => '/myprofile'
            ),
            array(
                'label' => 'MY CIRCLES',
                'title' => 'MY CIRCLES',
                'active' => true,
                'redirect' => false //'/myprofile/myCircle'
            )
        );
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        $viewArray['profile_model'] = $this->ProfileModel;
        $viewArray['treprZone'] = $this->ProfileModel->getTempFriendsList($this->sessionObj->offsetGet('userid'));

        $viewArray['prof'] = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
        
          
        
        if($viewArray['prof']['google_id']!=""){
            $this->sessionObj->offsetSet('user_registerd_email', $viewArray['prof']['email_address']);
            $this->sessionObj->offsetSet('user_social_email', $viewArray['prof']['email_address']);
            $this->sessionObj->offsetSet('social_provider_google', 'Google');
        }


        if($viewArray['prof']['facebook_id']!=""){
            $this->sessionObj->offsetSet('user_registerd_email', $viewArray['prof']['email_address']);
            $this->sessionObj->offsetSet('user_social_email', $viewArray['prof']['email_address']);
            $this->sessionObj->offsetSet('social_provider_facebook', 'Facebook');

        }                 
        if($viewArray['prof']['linkedin_id']!=""){
            $this->sessionObj->offsetSet('user_registerd_email', $viewArray['prof']['email_address']);
            $this->sessionObj->offsetSet('user_social_email', $viewArray['prof']['email_address']);
            $this->sessionObj->offsetSet('social_provider_linkedin', 'LinkedIn');
        }

        if($viewArray['prof']['twitter_id']!=""){
            $this->sessionObj->offsetSet('user_registerd_email', $viewArray['prof']['email_address']);
            $this->sessionObj->offsetSet('user_social_email', $viewArray['prof']['email_address']);
            $this->sessionObj->offsetSet('social_provider_twitter', 'Twitter');
        }

              
       // print_r($viewArray['prof']);die;
        $viewArray['reviews'] = $this->ProfileModel->getreviews($this->sessionObj->offsetGet('userid'));
        $viewArray['comSessObj'] = $this->sessionObj;
       
        $viewArray['flashMessages'] = $this->flashMessenger()->getMessages();
        $viewArray['userInvites'] = $this->ProfileModel->getUserInviteDetails($this->sessionObj->userid);
		$viewArray['profPhotosMarked'] = $this->ProfileModel->getUserPhotosMarked($this->sessionObj->offsetGet('userid'));

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }
    /* my circle section */

    /* reviews section */
    public function reviewsAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
		
		$this->TripPlugin = $this->plugin('TripPlugin');

        /* referral section */
        if (isset($_POST['referralEmails'])) {

            $currProf = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
            $referralEmails = $emailIds = array_filter(explode(';', $this->CommonMethodsModel->cleanQuery($_POST['referralEmails'])));
            $alreadyExistMails = $this->ProfileModel->getInviteMails($this->sessionObj->offsetGet('userid'));
            if ($alreadyExistMails) {
                foreach ($alreadyExistMails as $exist) {
                    $existMails[] = $exist['invited_email'];
                }
            }
            /*if(!empty($existMails)){
                $referralEmails = array_unique(array_merge($emailIds,$existMails));
            }
            else{
                $referralEmails = $emailIds;
            }*/
            if (isset($currProf['image']) && !empty($currProf['image'])) {
                if (strpos($currProf['image'], "http") !== false) {
                    $referrerPhotoUrl = $currProf['image'];
                } else {
                    $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'public/uploads/profile_picture/medium/' . $currProf['image'];
                }
            } else {
                $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'default_user.png';
            }
            $signUpLink = $this->siteConfigs['mail_url'] . '?signup=true&referrer=' . $currProf['user_key'];
            $userId = $this->sessionObj->offsetGet('userid');
            if (!empty($referralEmails)) {
                $count = 0;
                foreach ($referralEmails as $mailId) {
                    if (!in_array($mailId, $existMails)) {
                        $queryData[] = array(
                            'user_id' => $userId,
                            'invited_email' => $mailId,
                            'invite_added' => time()
                        );
                    }
                    $mailData = array(
                        'FULL_NAME' => $currProf['first_name'],
                        'REFERRED_PHOTO_URL' => $referrerPhotoUrl,
                        'REFERRAL_EMAIL' => $mailId,
                        'SIGNUP_LINK' => $signUpLink . '&refTo=' . base64_encode($mailId),
                    );
                    try
                    {
                        $this->MailModel->sendReferralMail($mailData);
                    }
                    catch (\Exception $e)
                    {
                        error_log($e);
                    }
                    $count++;
                }
            }
            if (!empty($queryData)) {
                $lastId = $this->ProfileModel->addInviteMails($queryData);
            }
            if ($count) {
                $this->flashMessenger()->addMessage(array('alert-success' => 'Your referral invite e-mail(s) have been sent successfully!'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while sending your referral e-mail(s). Please try again.'));
            }
            $this->redirect()->toRoute('myprofile', array('action' => 'reviews'));
        }
        $this->layout()->hideSwitchUser = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'My Profile',
                'title' => 'My Profile',
                'active' => false,
                'redirect' => '/myprofile'
            ),
            array(
                'label' => 'REVIEWS',
                'title' => 'REVIEWS',
                'active' => true,
                'redirect' => false //'/myprofile/reviews'
            )
        );
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        $viewArray['prof'] = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
        $viewArray['reviews'] = $this->ProfileModel->getreviews($this->sessionObj->offsetGet('userid'));
		
		foreach($viewArray['reviews'] as $rKey=>$rev){
			$viewArray['reviews'][$rKey]['sender_info'] = $this->ProfileModel->getprofile($rev['sender_id']);
			$flightdetails = $this->TripModel->getFlightsDetails($rev['trip_id']);
			$viewArray['reviews'][$rKey]['airport_details'] = array('orgin_city'=>'','destination_city'=>'');
			if(is_array($flightdetails) && count($flightdetails) > 0){
				$viewArray['reviews'][$rKey]['airport_details'] = $flightdetails[0];
			}
		}
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewArray['flashMessages'] = $this->flashMessenger()->getMessages();
        $viewArray['userInvites'] = $this->ProfileModel->getUserInviteDetails($this->sessionObj->userid);
        $app_key = isset($this->siteConfigs['traity_api_key'])?$this->siteConfigs['traity_api_key']:'';
        $app_secret = isset($this->siteConfigs['traity_secret_key'])?$this->siteConfigs['traity_secret_key']:'';
        $TRAITY_API = 'https://api.traity.com';
        $CURRENT_USER_ID = $this->sessionObj->offsetGet('userid');
        //$api = new \OAuth2\Client($app_key, $app_secret);
        //$result = $api->getAccessToken($TRAITY_API.'/oauth/token',
        //    'client_credentials', array());
        //$token = $result['result']['access_token'];
        //$api->setAccessToken($token);
        //$api->setAccessTokenType(\OAuth2\Client::ACCESS_TOKEN_BEARER);

       
        $viewArray['prof_user_id'] = $this->sessionObj->offsetGet('userid');
	    $viewArray['profPhotosMarked'] = $this->ProfileModel->getUserPhotosMarked($this->sessionObj->offsetGet('userid'));


        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }
    /* reviews section */

    /* references section */
    public function referencesAction()
    {

        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }

        /* referral section */
        if (isset($_POST['referralEmails'])) {
            $currProf = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
            $referralEmails = $emailIds = array_filter(explode(';', $this->CommonMethodsModel->cleanQuery($_POST['referralEmails'])));
            $alreadyExistMails = $this->ProfileModel->getInviteMails($this->sessionObj->offsetGet('userid'));
            if ($alreadyExistMails) {
                foreach ($alreadyExistMails as $exist) {
                    $existMails[] = $exist['invited_email'];
                }
            }

            if (isset($currProf['image']) && !empty($currProf['image'])) {
                if (strpos($currProf['image'], "http") !== false) {
                    $referrerPhotoUrl = $currProf['image'];
                } else {
                    $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'public/uploads/profile_picture/medium/' . $currProf['image'];
                }
            } else {
                $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'default_user.png';
            }
            $signUpLink = $this->siteConfigs['mail_url'] . '?signup=true&referrer=' . $currProf['user_key'];
            $userId = $this->sessionObj->offsetGet('userid');
            if (!empty($referralEmails)) {
                $count = 0;
                foreach ($referralEmails as $mailId) {
                    if (!in_array($mailId, $existMails)) {
                        $queryData[] = array(
                            'user_id' => $userId,
                            'invited_email' => $mailId,
                            'invite_added' => time()
                        );
                    }
                    $mailData = array(
                        'FULL_NAME' => $currProf['first_name'],
                        'REFERRED_PHOTO_URL' => $referrerPhotoUrl,
                        'REFERRAL_EMAIL' => $mailId,
                        'SIGNUP_LINK' => $signUpLink . '&refTo=' . base64_encode($mailId),
                    );
                    try
                    {
                        $this->MailModel->sendReferralMail($mailData);
                    }
                    catch (\Exception $e)
                    {
                        error_log($e);
                    }
                    $count++;
                }
            }
            if (!empty($queryData)) {
                $lastId = $this->ProfileModel->addInviteMails($queryData);
            }
            if ($count) {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array('user_id' => $this->sessionObj->userid,
                    'notification_type' => "etc",
                    'notification_subject' => 'reference',
                    'notification_message' => "" . $username . " has provided reference as you requested. Please review and accept the same to show it in your profile.",
                    'notification_added' => date("Y-m-d H:i:s")

                );
                $this->TripModel->addToNotifications($hugedata);

                $this->flashMessenger()->addMessage(array('alert-success' => 'Your referral invite e-mail(s) have been sent successfully!'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while sending your referral e-mail(s). Please try again.'));
            }
            $this->redirect()->toRoute('myprofile', array('action' => 'references'));
        }

        /* referance request by email section */
        if (isset($_POST['refReqEmails'])) {
            $currProf = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
            $refReqEmails = $emailIds = array_filter(explode(';', $this->CommonMethodsModel->cleanQuery($_POST['refReqEmails'])));
            if (isset($currProf['image']) && !empty($currProf['image'])) {
                if (strpos($currProf['image'], "http") !== false) {
                    $referrerPhotoUrl = $currProf['image'];
                } else {
                    $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'public/uploads/profile_picture/medium/' . $currProf['image'];
                }
            } else {
                $referrerPhotoUrl = $this->siteConfigs['mail_url'] . 'default_user.png';
            }
            $userId = $this->sessionObj->offsetGet('userid');
            $currTime = time();
            if (!empty($refReqEmails)) {
                $count = 0;
                foreach ($refReqEmails as $mailId) {
                    $userExist = $this->CommonMethodsModel->checkUserEmailExists($mailId);
                    $tmpData = array(
                        'requested_by_user' => $this->sessionObj->offsetGet('userid'),
                        'requested_to_email' => $mailId,
                        'request_added' => $currTime,
                    );
                    $tmpData['requested_to_user'] = 0;
                    if ($userExist) {
                        $tmpData['requested_to_user'] = $userExist['ID'];
                    }
                    $refId = $this->ProfileModel->sendReferenceRequest($tmpData);
                    //$writeRefLink = $this->siteConfigs['mail_url'].'user/view/'.$currProf['ID'].'?req_id='.base64_encode($refId).'&res_key='.md5($refId);
                    $writeRefLink = 'http://trepr.co.uk/myprofile/checkuserprofilestatus?need_user_profile=' . base64_encode($mailId) . '&need=yes';
                    //$writeRefLink = $this->siteConfigs['mail_url'].'myprofile/checkuserprofilestatus?need_user_profile='.base64_encode($mailId).'&need=yes';
                    if ($refId) {
                        $mailData = array(
                            'FIRST_NAME' => $currProf['first_name'],
                            'REFERRED_PHOTO_URL' => $referrerPhotoUrl,
                            'REQUEST_EMAIL' => $mailId,
                            'REFERENCE_LINK' => $writeRefLink
                        );
                        if ($userExist) {
                            $mailData['REFERENCE_LINK'] = $this->siteConfigs['mail_url'] . 'myprofile/references#references-by-you';
                            //$mailData['REFERENCE_LINK'] =  'http://trepr.com/myprofile/references#references-by-you';
                        }

                        try
                        {
                            $this->MailModel->sendReferenceRequestMail($mailData);
                        }
                        catch (\Exception $e)
                        {
                            error_log($e);
                        }

                        $count++;
                    }
                }
            }
            /*if(!empty($queryData)){
                $lastId = $this->ProfileModel->sendMultipleReferenceRequest($queryData);
            }*/
            if ($count) {
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array('user_id' => $this->sessionObj->userid,
                    'notification_type' => "etc",
                    'notification_subject' => 'reference',
                    'notification_message' => "" . $username . " has provided reference as you requested. Please review and accept the same to show it in your profile.",
                    'notification_added' => date("Y-m-d H:i:s")

                );
                $this->TripModel->addToNotifications($hugedata);
                $this->flashMessenger()->addMessage(array('alert-success' => 'Your reference request e-mail(s) have been sent successfully!'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while sending your referral e-mail(s). Please try again.'));
            }
            $this->redirect()->toRoute('myprofile', array('action' => 'references'));
        }

        if (isset($_POST['reference_request_id'])) {
            $refReqIdGet = $this->CommonMethodsModel->cleanQuery($_POST['reference_request_id']);
            $refReqRow = $this->ProfileModel->getReferenceRequestById($refReqIdGet);
            $refererId = ($refReqRow['requested_by_user']) ? $refReqRow['requested_by_user'] : 0;
            $responseData = array(
                'request_id' => $refReqIdGet,
                'referer_id' => $refererId,
                'reference_response_key' => md5(time()),
                'reference_response_relationship' => 'Friend'
            );
            if (isset($_POST['writeReference'])) {
                $responseData['reference_message'] = $this->CommonMethodsModel->cleanQuery($_POST['referenceMessage']);
                $responseData['reference_added'] = time();
            }
            if (isset($_POST['ignoreReference'])) {
                $responseData['reference_ignored'] = time();
            }
            $resId = $this->ProfileModel->writeReferenceResponse($responseData);
            if ($resId) {
                $this->flashMessenger()->addMessage(array('alert-success' => 'Your reference has been sent successfully!'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while sending your reference. Please try again.'));
            }
            $this->redirect()->toRoute('myprofile', array('action' => 'references'));
        }

        if (isset($_POST['reference_response_id'])) {
            $reference_id = $_POST['reference_response_id'];
            if (isset($_POST['writeReference'])) {
                $responseData['reference_message'] = $this->CommonMethodsModel->cleanQuery($_POST['referenceMessage']);
            }
            $resId = $this->ProfileModel->updateReferenceResponse($reference_id, $responseData);
            if ($resId) {
                $this->flashMessenger()->addMessage(array('alert-success' => 'Your reference has been edited successfully!'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while editing your reference. Please try again.'));
            }
            $this->redirect()->toRoute('myprofile', array('action' => 'references'));
        }

        if (isset($_POST['approve_reference_id'])) {

            $reference_id = $_POST['approve_reference_id'];
            $actionWord = 'accepted';
            $responseData['reference_approved'] = time();
            $resId = $this->ProfileModel->updateReferenceResponse($reference_id, $responseData);
            if ($resId) {
                $this->flashMessenger()->addMessage(array('alert-success' => 'Your reference has been ' . $actionWord . ' successfully!'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to ' . $actionWord . ' your reference. Please try again.'));
            }
            $this->redirect()->toRoute('myprofile', array('action' => 'references'));
        }
        $this->layout()->hideSwitchUser = true;
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'My Profile',
                'title' => 'My Profile',
                'active' => false,
                'redirect' => '/myprofile'
            ),
            array(
                'label' => 'REFERENCES',
                'title' => 'REFERENCES',
                'active' => true,
                'redirect' => false //'/myprofile/references'
            )
        );
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        $viewArray['currency'] = $this->CommonMethodsModel->getCurrencies();
        $viewArray['pending_ref_req'] = $this->ProfileModel->getPendingReferenceRequests($this->sessionObj->offsetGet('userid'));
        $viewArray['completed_ref_req'] = $this->ProfileModel->getWritenReference($this->sessionObj->offsetGet('userid'));
        $viewArray['pending_refs'] = $this->ProfileModel->getUserReferences($this->sessionObj->offsetGet('userid'), true);
        $viewArray['user_refs'] = $this->ProfileModel->getUserReferences($this->sessionObj->offsetGet('userid'));
        $viewArray['profile_model'] = $this->ProfileModel;
        $viewArray['prof'] = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
        $viewArray['reviews'] = $this->ProfileModel->getreviews($this->sessionObj->offsetGet('userid'));
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewArray['flashMessages'] = $this->flashMessenger()->getMessages();
        $viewArray['userInvites'] = $this->ProfileModel->getUserInviteDetails($this->sessionObj->userid);
        $viewArray['CommonMethodsModel'] = $this->CommonMethodsModel;
		$viewArray['profPhotosMarked'] = $this->ProfileModel->getUserPhotosMarked($this->sessionObj->offsetGet('userid'));

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    /* references section */

    public function camingAction()
    {
        /*$upload_dir = "temp/";
        $img = $_POST['camimage'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        $data = base64_decode($img);
        $filename=mktime() . ".png";
        $file = $upload_dir .$filename; 
        $success = file_put_contents($file, $data);
        //print $success ? $file : 'Unable to save the file.';
        $arr_peopleabs_detail = array(
            'image'    => $this->CommonMethodsModel->cleanQuery($filename),
            'modified' => time()
        );*/
        $fileNameBody = 'USER_DP_' . time();
        $tempSrcLoc = ACTUAL_ROOTPATH . 'uploads/temp_dp/';
        $actDesLoc = ACTUAL_ROOTPATH . 'uploads/profile_picture/';
        $img = $_POST['camimage'];
        $img = str_replace('data:image/png;base64,', '', $img);
        $img = str_replace(' ', '+', $img);
        if ($img == '') {
            $this->flashMessenger()->addMessage(array('alert-success' => 'An error has occurred while capturing your photo. Please try again.'));
            exit;
        }
        $imageContent = base64_decode($img);
        $fileName = $fileNameBody . '.jpg';
        $fileNameWithLoc = $tempSrcLoc . $fileName;
        file_put_contents($fileNameWithLoc, $imageContent);
        $configArray = array(
            array(
                'image_x' => 150,
                'sub_dir_name' => 'medium'
            ),
            array(
                'image_x' => 100,
                'sub_dir_name' => 'small'
            )
        );
        //$imgResult = $this->socialDPUpload($fileNameWithLoc, $actDesLoc, $fileNameBody, $configArray );
        if (copy($fileNameWithLoc, ACTUAL_ROOTPATH . 'uploads/profile_picture/' . $fileName)) {
            copy(ACTUAL_ROOTPATH . 'uploads/profile_picture/' . $fileName, ACTUAL_ROOTPATH . 'uploads/profile_picture/medium/' . $fileName);
            copy(ACTUAL_ROOTPATH . 'uploads/profile_picture/' . $fileName, ACTUAL_ROOTPATH . 'uploads/profile_picture/small/' . $fileName);
            // copy(ACTUAL_ROOTPATH.'uploads/profile_picture/'.$fileNameBody.'.'.$ext, ACTUAL_ROOTPATH.'uploads/temp_dp/'.$fileName);
        }
        $imgResult['result'] = 'success';
        if ($imgResult['result'] == 'success') {
            $idverfy_image = $fileName;//$imgResult['fileName'];
            $arr_photo_detail = array(
                'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')),
                'user_photo_name' => $this->CommonMethodsModel->cleanQuery($idverfy_image),
                'user_photo_added' => time()
            );
            $existingCount = $this->ProfileModel->getUserPhotos($this->sessionObj->offsetGet('userid'), 'count');
            if ($existingCount < $this->siteConfigs['max_user_photo_upload']) {
                $this->ProfileModel->addUserPhoto($arr_photo_detail, $this->sessionObj->offsetGet('userid'));
                $this->flashMessenger()->addMessage(array('alert-success' => 'Photo has been saved successfully.'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while saving photo. Please try again.'));
            }
        } else {
            $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while saving your photo. Please try again.'));
        }
        $this->sessionObj->personalInforTabSelected = 'photoVideo';
        exit;
    }

    public function addaddressbookAction()
    {

        if ($_POST['addrtype'] == 'newaddress') {
            if (isset($_POST['setdeflt'])) {
                $seld = 1;
                $this->TripModel->updateTripContactAddresssetdef($this->sessionObj->userid);
            } else {
                $seld = 0;
            }
            $arr_trip_contact = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'street_address_1' => $this->CommonMethodsModel->cleanQuery($_POST['route']),
                'street_address_2' => $this->CommonMethodsModel->cleanQuery($_POST['sublocality']),
                'city' => $this->CommonMethodsModel->cleanQuery($_POST['locality']),
                'state' => $this->CommonMethodsModel->cleanQuery($_POST['administrative_area_level_1']),
                'zip_code' => $this->CommonMethodsModel->cleanQuery($_POST['postal_code']),
                'country' => $this->CommonMethodsModel->cleanQuery($_POST['country_short']),
                'latitude' => $this->CommonMethodsModel->cleanQuery($_POST['lat']),
                'longitude' => $this->CommonMethodsModel->cleanQuery($_POST['lng']),
                'modified' => date('Y-m-d H:i:s'),
                'address_type' => $this->CommonMethodsModel->cleanQuery($_POST['contact_address_type']),
                'set_default' => $this->CommonMethodsModel->cleanQuery($seld)
            );
            $user_location = $this->TripModel->addTripContactAddress($arr_trip_contact);

            $this->flashMessenger()->addMessage(array('alert-success' => 'Address has been added successfully'));
        }
        exit;
    }

    public function editaddressbookAction()
    {
        if ($_POST['vtype'] == 'view') {
            $editresp = $this->CommonMethodsModel->viewUsersAddressBook($_POST['viewid'], $this->sessionObj->userid);
            echo json_encode($editresp);
            exit;
        } elseif ($_POST['vtype'] == 'delete') {
            $user = array(
                "deleted" => $this->CommonMethodsModel->cleanQuery(time())
            );
            $userid = $this->CommonMethodsModel->updteUsersAddressBook($user, $this->sessionObj->userid, $_POST['viewid']);

            /*$editresp=$this->CommonMethodsModel->deleteUsersAddressBook($_POST['viewid'],$this->sessionObj->userid);*/
            $this->flashMessenger()->addMessage(array('alert-success' => 'Address has been deleted successfully.'));
            return true;
        } elseif ($_POST['addrtype'] == 'editaddress') {
            if ($_POST['setdeflt']) {
                $seld = 1;
                $this->TripModel->updateTripContactAddresssetdef($this->sessionObj->userid);
            } else {
                $seld = 0;
            }
            $arr_trip_contact = array(
                'name' => $this->CommonMethodsModel->cleanQuery($_POST['contact_name']),
                'street_address_1' => $this->CommonMethodsModel->cleanQuery($_POST['route']),
                'street_address_2' => $this->CommonMethodsModel->cleanQuery($_POST['sublocality']),
                'city' => $this->CommonMethodsModel->cleanQuery($_POST['locality']),
                'state' => $this->CommonMethodsModel->cleanQuery($_POST['administrative_area_level_1']),
                'zip_code' => $this->CommonMethodsModel->cleanQuery($_POST['postal_code']),
                'country' => $this->CommonMethodsModel->cleanQuery($_POST['country_short']),
                /*'phone' => $this->CommonMethodsModel->cleanQuery($_POST['phone']),
                //'fax' => $this->CommonMethodsModel->cleanQuery($_POST['fax']),*/
                'latitude' => $this->CommonMethodsModel->cleanQuery($_POST['lat']),
                'longitude' => $this->CommonMethodsModel->cleanQuery($_POST['lng']),
                'modified' => date('Y-m-d H:i:s'),
                'set_default' => $this->CommonMethodsModel->cleanQuery($seld)
            );
            $user_location = $this->TripModel->updateTripContactAddress($arr_trip_contact, $_POST['editid'], $this->sessionObj->userid);
            $this->flashMessenger()->addMessage(array('alert-success' => 'Address has been updated successfully.'));
        } elseif ($_POST['eaddress'] != '') {
            $user = array(
                "adrtype" => $this->CommonMethodsModel->cleanQuery((strtolower("home"))),
                "ad_name" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['contact_name'])),
                "address" => $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['eaddress']))),
                "city" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['city'])),
                "state" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['state'])),
                "postcode" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['postal_code'])),
                "country" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['country_short'])),
                "addrdefault" => $this->CommonMethodsModel->cleanQuery($_POST['setdeflt']),
                "longitude" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['longitude'])),
                "latitude" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['latitude']))
            );
            $userid = $this->CommonMethodsModel->updteUsersAddressBook($user, $this->sessionObj->userid, $_POST['editid']);
            $this->flashMessenger()->addMessage(array('alert-success' => 'Address has been deleted successfully.'));
        }
        return $this->redirect()->toRoute('myprofile', array('action' => 'myAddresses'));
        exit;
    }

    public function uploadUserDP($field_name, $file_name, $path, $configArray)
    {
        if (isset($_FILES[$field_name])) {
            $filename = $_FILES[$field_name]['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $file_name = $file_name;
            $handle = new \upload($_FILES[$field_name]);
            if ($handle->uploaded) {
                $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/');
                $handle->file_new_name_body = $file_name;
                $handle->image_resize = true;
                if ($configArray && !empty($configArray)) {
                    foreach ($configArray as $config) {
                        $handle->file_new_name_body = $file_name;
                        $handle->image_resize = true;
                        $handle->image_x = $config['image_x'];
                        $handle->image_ratio_y = true;
                        $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/' . $config['sub_dir_name'] . '/');
                    }
                    if ($handle->processed) {
                        $handle->clean();
                        $retArr = array('result' => 'success', 'fileName' => $file_name . '.' . $ext);
                    } else {
                        $retArr = array('result' => 'failed', 'error' => $handle->error);
                    }
                }
            } else {
                $retArr = array('result' => 'failed', 'error' => $handle->error);
            }
            return $retArr;
        }
    }

    public function uploadUserDPMultiple($file, $file_name, $path, $configArray)
    {
        // print_r($file);die;
        if (isset($file)) {
            $filename = $file['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $file_name = $file_name;
            $handle = new \upload($file);

            if ($handle->uploaded) {

                $handle->file_new_name_body = $file_name;

                $handle->process(ACTUAL_ROOTPATH . 'uploads/' . $path . '/');
                if ($configArray && !empty($configArray)) {
                    foreach ($configArray as $config) {
                        $handle->file_new_name_body = $file_name;
                        $handle->image_resize = true;
                        $handle->image_x = $config['image_x'];
                        $handle->image_ratio_y = true;
                        $handle->process(ACTUAL_ROOTPATH . 'uploads/' . $path . '/' . $config['sub_dir_name'] . '/');
                    }
                    if ($handle->processed) {
                        $handle->clean();
                        $retArr = array('result' => 'success', 'fileName' => $file_name . '.' . $ext);
                    } else {
                        $retArr = array('result' => 'failed', 'error' => $handle->error);
                    }
                }
            } else {
                $retArr = array('result' => 'failed', 'error' => $handle->error);
            }

            return $retArr;
        }
    }

    function getUserVideosAction()
    {
        $userVideos = $this->ProfileModel->getUserVideos($this->params()->fromQuery('userid'));
        if ($userVideos) {
            $html = '';
            foreach ($userVideos as $key => $video) {
                $html .= '<div class="col-xs-4 user_videos">'
                    . '<div class="form-group">'
                    . '<a data-video_id ="' . $video['user_video_id'] . '" href="javascript:;" class="remove label-danger removeVideoBtn" style="z-index:6 !important;"title="Remove Video"><i class="fa fa-times" aria-hidden="true"></i></a>'
                    . '<canvas id="videoThum_' . $key . '"></canvas>'
                    . '<iframe width="220" height="115" src="' . $video['user_video_url'] . '" frameborder="0" allowfullscreen></iframe>'
                    . '</div>'
                    . '</div>';
            }
            $resultArr = array('result' => 'success', 'htmlData' => $html);
        } else {
            $html = '<div class="well text-center">No videos added yet.</div>';
            $resultArr = array('result' => 'failure', 'htmlData' => $html);
        }
        echo $html;
        exit;
    }

    public function socialConnectAction()
    { 
        
        
        if(isset($_REQUEST['error']) ){
            return $this->redirect()->toRoute('myprofile', array('action' => 'myCircle'));
        }
        /*unset($_SESSION['HA::CONFIG']);
        unset($_SESSION['HA::STORE']);
        unset($_SESSION['user_connected']);
        //unset($_SESSION['fb_1729839597282171_access_token']);
        //unset($_SESSION['fb_1729839597282171_user_id']);exit;
        //echo $_SESSION['fb_1729839597282171_access_token'];exit; */
        $provider_array = array('Facebook' => 'facebook_id', 'Google' => 'google_id', 'Twitter' => 'twitter_id', 'LinkedIn' => 'linkedin_id');
        $provider = $_GET['provider'];

         $config = [  'callback' => $this->siteConfigs['clienturl'].'/myprofile/socialConnect?provider='.$provider,
                      'providers' => [
                 
                        'Facebook' => [
                          'enabled' => true,
                          'keys' => [
                            'id' => $this->siteConfigs['facebook_appId'],
                            'secret' => $this->siteConfigs['facebook_secret']
                          ],
                        ],
 
                         'Google' => [
                                        'enabled' => true,
            				 'keys' => [
                		            'id'     => $this->siteConfigs['googleplus_appId'],
               				    'secret' => $this->siteConfigs['googleplus_secret']
           				 ],
            			'scope' => 'email',
        				],

        				 'Twitter' => [
      						'enabled' => true,
      						'keys' => [
        					'key' =>  $this->siteConfigs['twitter_appId'],
       						 'secret' => $this->siteConfigs['twitter_secret']
      						],

    					],


    					'LinkedIn' => [
      						'enabled' => true,
     						 'keys' => [
       						 'id' => $this->siteConfigs['linkedin_appId'],
        					 'secret' =>  $this->siteConfigs['linked_secret']
      						],
                                         
    				],


                      ],
                    ];
         
       
        if (array_key_exists($provider, $provider_array)) {
            
             
           /* $hybridauth = new \Hybrid_Auth($this->hybridConfig);*/
              $hybridauth = new Hybridauth($config);
            $authProvider = $hybridauth->authenticate($provider);
            $user_profile = $authProvider->getUserProfile();

            
            
                //echo "<pre>"; print_r($user_profile);die;
            $updateUserData = array(
                $provider_array[$provider] => $user_profile->identifier
            );
            $userRes = $this->ProfileModel->updateprofile($updateUserData, $this->sessionObj->userid);
            if ($provider == 'LinkedIn') {
                $user_contacts = array();
            } else {
                $user_contacts = $authProvider->getUserContacts();
            }
              
            
            $contactsData = array();
           if (!empty($user_contacts)) {
               
               
                foreach ($user_contacts as $contact) {
                    $contactsData[] = array(
                        'user_id' => $this->sessionObj->userid,
                        'user_session_id' =>'',
                        'oauth_provider_type' => $this->CommonMethodsModel->cleanQuery($provider),
                        'friend_oauth_id' => $contact->identifier,
                        'friend_website_url' => $this->CommonMethodsModel->cleanQuery($contact->webSiteURL),
                        'friend_profile_url' => $this->CommonMethodsModel->cleanQuery($contact->profileURL),
                        'friend_photo_url' => $this->CommonMethodsModel->cleanQuery($contact->photoURL),
                        'friend_photo_location' => '',
                        'friend_display_name' => $this->CommonMethodsModel->cleanQuery($contact->displayName),
                        'friend_description' => $this->CommonMethodsModel->cleanQuery($contact->description),
                        'friend_email' => $this->CommonMethodsModel->cleanQuery($contact->email),
                        'friend_added' => time()
                    );
                }
                
               $res = $this->ProfileModel->addUpdateTempFriendsList($contactsData);
           }                      
              $userdetail = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
              
      if ($hybridauth)   {
              
                                                                                                      
                    if ($userdetail['email_address'] == $user_profile->email) {
                        $this->flashMessenger()->addMessage(array('alert-success' => $provider . ' is connected successfully'));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' =>$provider.' Email ID doesn\'t match with  email ID connected. Please connect using your profile email ID '.$userdetail['email_address']));
                    }
                    
                } else {
                    $this->flashMessenger()->addMessage(array('alert-success' => 'Connected with ' . $provider . ' ' . $provider));
                }
            
      

        } 
        if(isset($userdetail)){
                   if($provider =="Facebook"){
                        $this->sessionObj->offsetSet('user_registerd_email', $userdetail['email_address']);
                        $this->sessionObj->offsetSet('user_social_email', $user_profile->email);
                        $this->sessionObj->offsetSet('social_provider_facebook', 'Facebook');
                    }else if($provider =="Google"){
                        $this->sessionObj->offsetSet('user_registerd_email', $userdetail['email_address']);
                        $this->sessionObj->offsetSet('user_social_email', $user_profile->email);
                        $this->sessionObj->offsetSet('social_provider_google', 'Google');
                    }
                    else if($provider =="LinkedIn"){
                        $this->sessionObj->offsetSet('user_registerd_email', $userdetail['email_address']);
                        $this->sessionObj->offsetSet('user_social_email', $user_profile->email);
                        $this->sessionObj->offsetSet('social_provider_linkedin', 'LinkedIn');
                    }
                    
                    else {
                        $this->sessionObj->offsetSet('user_registerd_email', $userdetail['email_address']);
                        $this->sessionObj->offsetSet('user_social_email', $user_profile->email);
                        $this->sessionObj->offsetSet('social_provider_twitter', 'Twitter');
                    }
        }
        return $this->redirect()->toRoute('myprofile', array('action' => 'myCircle'));
    }

    public function socialConnectDisconnectAction()
    {
        $provider_array = array('Facebook' => 'facebook_id', 'Google' => 'google_id', 'Twitter' => 'twitter_id', 'LinkedIn' => 'linkedin_id');
        $provider = $_GET['provider'];
        $disconnectData = array(
            $provider_array[$provider] => NULL
        );
        if (array_key_exists($provider, $provider_array)) {
            $res = $this->ProfileModel->updateprofile($disconnectData, $this->sessionObj->userid);
            $removed = $this->ProfileModel->removeFriendsList($this->sessionObj->userid, $provider);
            if ($res) {
                
                    if($provider =="Facebook"){
                        
                        $this->sessionObj->offsetSet('social_provider_facebook', '');
                    }else if($provider =="Google"){
                        
                        $this->sessionObj->offsetSet('social_provider_google', '');
                    }
                    else if($provider =="LinkedIn"){
                      
                        $this->sessionObj->offsetSet('social_provider_linkedin', '');
                    }
                    
                    else {
                        
                        $this->sessionObj->offsetSet('social_provider_twitter', '');
                    }
                
                
                $this->flashMessenger()->addMessage(array('alert-success' => $provider . ' account is disconnected successfully.'));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while disconnecting ' . $provider . ' account.'));
            }
        } else {
            $this->flashMessenger()->addMessage(array('alert-danger' => 'This is an invalid social network provider'));
        }
        return $this->redirect()->toRoute('myprofile', array('action' => 'myCircle'));
    }

    public function callbackAuthAction()
    {
        require_once(ACTUAL_ROOTPATH . "hybridauth/Hybrid/Auth.php");
        require_once(ACTUAL_ROOTPATH . "hybridauth/Hybrid/Endpoint.php");
        \Hybrid_Endpoint::process();
    }

    public function fbConnectAction()
    {
        $user = $this->facebook->getUser();

        if (empty($user)) {
            /*login url	*/
            $loginurl = $this->facebook->getLoginUrl(array(
                'scope' => 'email,user_birthday,user_location,user_work_history,user_hometown,user_photos',
                /*'scope'             =>  'public_profile,email,user_friends',*/
                /*'redirect_uri'      =>  'http://trepr.com/myprofile/fbConnect',*/
                //'redirect_uri'      =>  'http://trepr.co.uk.com/myprofile/fbConnect',
                'redirect_uri' => 'http://trepr-dev.com/myprofile/fbConnect',
                /*'display'           =>  'popup'*/
            ));

        }
        header('Location: ' . urldecode($loginurl));
        if ($user) {
            try {

                $user_profile = $this->facebook->api('/me');
                $fb_friends = $this->facebook->api('/me/friends');
                echo '<pre>';
                print_r($user_profile);
                print_r($fb_friends);
                exit;
            } catch (FacebookApiException $e) {
                error_log($e);
                $user = NULL;
            }
        }
        exit;
    }

    public function fbConnectSdkAction()
    {
        $request = $this->fb->request('GET',
            '/1168600209869550/friendlists', 'email');
        var_dump($request);
        exit;
        $response = $request->execute();

        $graphObject = $response->getGraphObject();
    }

    /*public function logoutFbAction(){
        $fbCookie = 'fbm_'.$this->facebook->getAppId();
        unset($_COOKIE[$fbCookie]);
	setcookie($fbCookie, null, -1, '/');
        $this->redirect()->toRoute('myprofile');
    }*/

    public function verifyTraityAction()
    {
        $appKey = isset($this->siteConfigs['traity_api_key'])?$this->siteConfigs['traity_api_key']:'';
        $secretKey = isset($this->siteConfigs['traity_secret_key'])?$this->siteConfigs['traity_secret_key']:'';
        $TRAITY_API = 'https://api.traity.com';
        $api = new \OAuth2\Client($appKey, $secretKey);
        $result = $api->getAccessToken($TRAITY_API . '/oauth/token',
            'client_credentials', array());
        echo '<pre>';
        var_dump($result);
        exit;
        $token = $result['result']['access_token'];
        $api->setAccessToken($token);
        $api->setAccessTokenType(\OAuth2\Client::ACCESS_TOKEN_BEARER);
    }

    public function sendReferenceRequestAction()
    {
        $refs = $this->ProfileModel->getReferenceRequests($_GET['requestedTo']);
        if (isset($_GET['requestedBy']) && (isset($_GET['requestedTo']) || isset($_GET['requestedEmail']))) {
            $requestedBy = $_GET['requestedBy'];
            $requestedTo = 0;
            $requestedToEmail = NULL;
            $requestedByUser = $this->ProfileModel->getprofile($requestedBy);
            if (isset($_GET['requestedTo'])) {
                $requestedTo = $_GET['requestedTo'];
                $requestedToUser = $this->ProfileModel->getprofile($requestedTo);
                $requestedToEmail = $requestedToUser['email_address'];
            }
            if (isset($_GET['requestedEmail'])) {
                $requestedToEmail = $_GET['requestedEmail'];
                $emailExist = $this->CommonMethodsModel->checkUserEmailExists($_GET['requestedEmail']);
                if ($emailExist) {
                    $requestedTo = $emailExist['ID'];
                }
            }
            $requestData = array(
                'requested_to_user' => $requestedTo,
                'requested_to_email' => $requestedToEmail,
                'requested_by_user' => $requestedBy,
                'request_added' => time()
            );
            $requestId = $this->ProfileModel->sendReferenceRequest($requestData);
            if (isset($_GET['requestedEmail']) && $requestedTo != 0 && $requestId) {
                $ref_key = md5(time());
                $referenceLink = $this->siteConfigs['mail_url'] . '?user/profile/' . $_GET['requestedBy'] . '/?write_ref=true&ref_key=' . $ref_key . '&req_id=' . $requestId;
                $mailData = array(
                    'FULL_NAME' => $requestedByUser['first_name'] . ' ' . $requestedByUser['last_name'],
                    'REFERENCE_LINK' => $referenceLink,
                    'REQUEST_EMAIL' => $requestedToEmail,
                );
                try
                {
                    $this->MailModel->sendReferenceRequestMail($mailData);
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }
            }
        }
        if ($requestId) {
            $this->flashMessenger()->addMessage(array('alert-success' => 'Reference request has been sent successfully.'));
        } else if ($requestId) {
            $this->flashMessenger()->addMessage(array('alert-danger' => 'An error has occurred while sending reference request.'));
        }
        return $this->redirect()->toRoute('myprofile');
        exit;
    }

    public function traityTestAction()
    {
        /*$viewArray = array();
        $viewModel = new ViewModel();
        
        $app_key = $this->siteConfigs['traity_api_key'];
        $app_secret = $this->siteConfigs['traity_secret_key'];
        $TRAITY_API = 'https://api.traity.com';
        $CURRENT_USER_ID = $this->sessionObj->offsetGet('userid');
        $api = new \OAuth2\Client($app_key, $app_secret);
        $result = $api->getAccessToken($TRAITY_API.'/oauth/token',
            'client_credentials', array());
        $token = $result['result']['access_token'];
        $api->setAccessToken($token);
        $api->setAccessTokenType(\OAuth2\Client::ACCESS_TOKEN_BEARER);
        $result = $api->fetch('https://traity.com/apps/connect?signature=SIGNATURE&success=SUCCESS');
        //$result = $api->fetch($TRAITY_API.'/1.0/app-users/'.$CURRENT_USER_ID);
        echo '<pre>';
        print_r($result);
        echo '</pre>';
        /*$appKey = $this->siteConfigs['traity_api_key'];
        $secretKey = $this->siteConfigs['traity_secret_key'];
        $TRAITY_API = 'https://api.traity.com';
        $api = new \OAuth2\Client($appKey, $secretKey);
        $result = $api->getAccessToken($TRAITY_API.'/oauth/token',
            'client_credentials', array());
        $token = $result['result']['access_token'];
        $api->setAccessToken($token);
        $api->setAccessTokenType(\OAuth2\Client::ACCESS_TOKEN_BEARER);
        
        $APP_KEY = $this->siteConfigs['traity_api_key'];
        $APP_SECRET = $this->siteConfigs['traity_secret_key'];
        $CURRENT_USER_ID = $this->sessionObj->offsetGet('userid');
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $traitySignature = $this->CommonPlugin->widget_signature($APP_KEY, $APP_SECRET, $CURRENT_USER_ID);
        $api->fetch($TRAITY_API.'/1.0/app-users/'.$CURRENT_USER_ID);
        //echo '<pre>';var_dump($traitySignature);exit;
        //echo $this->params()->fromRoute('user_id');
        //echo 'Hi exit;';
        $viewArray['traity_signature'] = $traitySignature;
        $viewArray['taity_app_key'] = $APP_KEY;
        $viewArray['traity_secret_key'] = $APP_SECRET;
        $viewArray['user_id'] = $CURRENT_USER_ID;*/

        /*$JWT = new \Firebase\JWT\JWT();
        $current_user_id = $this->sessionObj->offsetGet('userid');
        $options   = array('verified' => array('phone', 'email'));
        $payload = array_merge(array('time' => time(),'current_user_id' => $current_user_id), $options);
        $signature = array(
            'key' => $app_key,
            'payload' => $JWT->encode($payload, $app_secret)
        );
        $viewArray['widget_signature'] =  $JWT->encode($signature, '');
        $viewArray['user_id'] = $this->sessionObj->offsetGet('userid');
        $resultFetch = $api->fetch('https://traity.com/apps/connect?signature=SIGNATURE&success=SUCCESS');
        echo '<pre>';print_r($resultFetch);exit;
        $viewModel->setVariables($viewArray)
                ->setTerminal(true);
        return $viewModel;*/
        echo 'traity-test';
    }

    public function showuserAction()
    {
        $viewUserId = $this->CommonMethodsModel->cleanQuery($this->params('user_id'));
        $viewUserProfile = $this->ProfileModel->getprofile($viewUserId);

        $requestData = array();
        $refAction = false;
        $isResWriten = false;
        if (isset($_GET['req_id']) && isset($_GET['res_key']) && $_GET['res_key'] != '') {
            $isResWriten = $this->ProfileModel->getReferenceResponseByKey($this->CommonMethodsModel->cleanQuery($_GET['res_key']));
            $refAction = (!$isResWriten) ? 'add' : 'edit';

            $requestId = base64_decode($_GET['req_id']);
            $requestData = $this->ProfileModel->getReferenceRequestById($this->CommonMethodsModel->cleanQuery($requestId));

            $res_key = $this->CommonMethodsModel->cleanQuery($_GET['res_key']);
        }
        if ($viewUserProfile) {
            if (isset($_POST['referenceWriten'])) {
                $redirectTo = 'home';
                if ($this->sessionObj->userid != '' && $requestData['requested_to_user'] == 0) {
                    $reqUpData = array(
                        'requested_to_user' => $this->sessionObj->userid
                    );
                    $this->ProfileModel->updateReferenceRequest($requestData['request_id'], $reqUpData);
                }

                $responseArr = array(
                    'request_id' => $requestData['request_id'],
                    'referer_id' => $requestData['requested_by_user'],
                    'reference_message' => $this->CommonMethodsModel->cleanQuery($_POST['referenceWriten']),
                    'reference_response_key' => $res_key,
                    'reference_response_relationship' => $this->CommonMethodsModel->cleanQuery($_POST['resRelationShip'])
                );

                if ($refAction == 'add') {
                    $responseArr['reference_added'] = time();
                    $addActionCompleted = $this->ProfileModel->writeReferenceResponse($responseArr);
                    if ($addActionCompleted)
                        $this->flashMessenger()->addMessage(array('alert-success' => 'Reference request has been writen successfully.'));
                    else
                        $this->flashMessenger()->addMessage(array('alert-success' => 'Reference request has been writen successfully.'));
                } else if ($refAction == 'edit') {
                    $editActionCompleted = $this->ProfileModel->updateReferenceResponse($isResWriten['reference_id'], $responseArr);
                    if ($editActionCompleted)
                        $this->flashMessenger()->addMessage(array('alert-success' => 'Reference request has been updated successfully.'));
                    else
                        $this->flashMessenger()->addMessage(array('alert-success' => 'Reference request has been updated successfully.'));
                }
                if ($this->sessionObj->userid != '') {
                    return $this->redirect()->toRoute($redirectTo);
                } else {
                    return $this->redirect()->toRoute($redirectTo);
                }
            }
            $viewArray = array();
            $viewModel = new ViewModel();
            $allowEdit = false;
            $daysLeft = false;
            if ($refAction == 'edit') {
                $editableDays = 15;
                $dateAddedTime = strtotime(date('d-m-Y', $isResWriten['reference_added']));
                $currentDateTime = strtotime(date('d-m-Y'));
                $diffDays = ($currentDateTime - $dateAddedTime);
                $daysLeft = ((($editableDays - 1) * 86400) - $diffDays) / 86400;
                $addDaysSeconds = (($editableDays) * 86400) + $dateAddedTime;
                if ($addDaysSeconds > strtotime(date('d-m-Y'))) {
                    $allowEdit = true;
                }
            }
            $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
            $viewArray['allowReferenceEdit'] = $allowEdit;
            $viewArray['daysLeft'] = ($daysLeft) ? $daysLeft : 0;
            $viewArray['referenceData'] = $isResWriten;
            $viewArray['refAction'] = $refAction;
            $viewArray['profile_model'] = $this->ProfileModel;
            $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
            $viewArray['currency'] = $this->CommonMethodsModel->getCurrencies();
            $viewArray['queryString'] = $this->getRequest()->getQuery();
            $viewArray['fb_app_id'] = $this->siteConfigs['facebook_appId'];
            $viewArray['timeZones'] = $this->timeZones;
            $viewArray['prof'] = $viewUserProfile;
            $viewArray['profPhotos'] = $this->ProfileModel->getUserPhotos($viewUserId);
            $viewArray['profVideos'] = $this->ProfileModel->getUserVideos($viewUserId);
            $viewArray['profaddr'] = $this->ProfileModel->getprofileaddr($viewUserId);
            $viewArray['profaddrmail'] = $this->ProfileModel->getprofileaddrmail($viewUserId);
            $viewArray['lang'] = $this->ProfileModel->getlang($viewUserId);
            $viewArray['abtus'] = $this->ProfileModel->getabtus($viewUserId);
            $viewArray['reviews'] = $this->ProfileModel->getreviews($viewUserId);
            $viewArray['notiferr'] = $this->ProfileModel->getprofilenotification($viewUserId);
            $viewArray['comSessObj'] = $this->sessionObj;
            $viewArray['flashMessages'] = $this->flashMessenger()->getMessages();
            $viewModel->setVariables($viewArray);
            return $viewModel;
        } else {
            $this->redirect()->toRoute('home');
        }
    }

    function uploadverficationAction()
    {
        if (isset($_POST['action']) && $_POST['action'] == 'verify')
        {
            $userRow = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
            if (isset($_POST['identity_type_id']) && !empty($_POST['identity_type_id']))
            {
                foreach ($_POST['identity_type_id'] as $keyId => $valId)
                {
                    $resultids = $this->ProfileModel->getVerifyRow($valId);
                    if (!empty($resultids))
                    {
                        $result = $resultids['fetched_text'];
                        if ($_POST['imageVerify'] == 1)
                        {
                            if (!empty($resultids['image'])) {
                                require_once(ACTUAL_ROOTPATH . 'ocr-lib/hodclient.php');
                                $hodClient = new \HODClient("34a54d30-ddaa-4294-8e45-ebe07eefe55e");
                                $paramArr = array(
                                    'file' => ACTUAL_ROOTPATH . "uploads/user_verification/" . $resultids['image'],
                                    'mode' => "document_photo"
                                );
                                $jobID = $hodClient->PostRequest($paramArr, \HODApps::OCR_DOCUMENT, \REQ_MODE::ASYNC);
                                if ($jobID == null) {
                                    $errors = $hodClient->getLastError();
                                    $err = $errors[0];
                                    echo($err->error . " / " . $err->reason . " / " . $err->detail);
                                } else {
                                    $response = $hodClient->GetJobResult($jobID);
                                    if ($response == null) {
                                        $errors = $hodClient->getLastError();
                                        $err = $errors[0];
                                        /* echo ("Error code: " . $err->error."</br>Error reason: " . $err->reason . "</br>Error detail: " .  $err->detail . "JobID: " . $err->jobID);*/
                                    } else {
                                        $result = "";
                                        $textBlocks = $response->text_block;
                                        for ($i = 0; $i < count($textBlocks); $i++) {
                                            $block = $textBlocks[$i];
                                            $result .= preg_replace("/\n+/", "</br>", $block->text);
                                        }
                                    }
                                }
                            }
                        }

                        $verfiyid = $resultids['verfiyid'];
                        $results = $this->CommonMethodsModel->cleanQuery($result);
                        $string = 'Whis string contains word "cheese" and "tea".';
                        $array = array('burger', 'melon', 'cheese', 'milk');

                        $userFirstName = $userRow['first_name'];
                        $userLastName = $userRow['last_name'];
                        $userDOB = $userRow['dob'];
                        $userGender = $userRow['gender'];
                        $arrVerifyDetails['first_name'] = 0;
                        $arrVerifyDetails['last_name'] = 0;
                        $arrVerifyDetails['image'] = 0;
                        $findFirstName = strpos(strtolower($results), strtolower($userFirstName));
                        if ($findFirstName != false) {
                            $arrVerifyDetails['first_name'] = 1;
                        }
                        $findLastName = strpos(strtolower($results), strtolower($userLastName));
                        if ($findLastName != false) {
                            $arrVerifyDetails['last_name'] = 1;
                        }
                        $arrVerifyDetails['gender'] = 0;
                        $findGender = strpos(strtolower($results), strtolower($userGender));
                        if ($findGender != false) {
                            $arrVerifyDetails['gender'] = 1;
                        }
                        $arrVerifyDetails['verfiyid'] = 0;
                        $findGender = strpos(strtolower($results), strtolower($verfiyid));
                        if ($findGender != false) {
                            $arrVerifyDetails['verfiyid'] = 1;
                        }
                        if (!empty($userDOB)) {
                            $array = array(date('d/m/Y', $userDOB), date('m/d/Y', $userDOB), date('m d Y', $userDOB)
                            , date('d m Y', $userDOB), date('d.m.Y', $userDOB), date('m.d.Y', $userDOB)
                            , date('m-d-Y', $userDOB), date('d-m-Y', $userDOB));
                            if ($this->strposa($results, $array, 1)) {
                                $arrVerifyDetails['dob'] = 1;
                            } else {
                                $arrVerifyDetails['dob'] = 0;
                            }
                        }
                        $arrUpdateData['fetched_text'] = $results;
                        $arrUpdateData['verifyid'] = $this->params()->fromPost('verifyid');
                        $arrUpdateData['verify_details'] = serialize($arrVerifyDetails);

                        $resultids = $this->ProfileModel->updateUserVerifyDetails($arrUpdateData, $valId);
                        if (!empty($verfiyid))
                        {
                            try
                            {
                                $sendmail = $this->MailModel->sendVerifyIdMailToUser($userRow['first_name'], $userRow['last_name'], $userRow['email_address'], $verfiyid);
                            }
                            catch (\Exception $e)
                            {
                                error_log($e);
                            }
                        }
                        if ($resultids) echo '1';

                        /*}*/
                    }
                }
                $userFirstName = $userRow['first_name'];
                $userLastName = $userRow['last_name'];

                try
                {
                    $sendmail = $this->MailModel->sendVerifyIdMailToAdmin($userRow['first_name'], $userRow['last_name'], $userRow['email_address']);
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }
            }
            exit;
        }
        elseif (isset($_POST['identity_type']) && !empty($_POST['identity_type']) && isset($_POST['action']) && $_POST['action'] == 'cam')
        {
            $arrData = array();
            $verifyDocument = 0;
            $imageVerify = 0;
            $identity_type = $_POST['identity_type'];
            foreach ($identity_type as $iKey => $iVal)
            {
                $verifyID = $_POST['ID'];
                $arrData['identity_type'][] = $iVal;
                $arrData['ID'][$iVal] = isset($verifyID[$iKey]) ? $verifyID[$iKey] : 0;

                $fileNameBody = 'user_verification_' . time();
                $tempSrcLoc = ACTUAL_ROOTPATH . 'uploads/temp_dp/';
                $actDesLoc = ACTUAL_ROOTPATH . 'uploads/user_verification/';
                $img = $_POST['uploadFile'];

                $img = str_replace('data:image/png;base64,', '', $img);
                $img = str_replace(' ', '+', $img);
                if ($img == '') {
                    $this->flashMessenger()->addMessage(array('alert-success' => 'An error has occurred while capturing your photo.'));
                    exit;
                }


                $imageContent = base64_decode($img);
                $fileName = $fileNameBody . '.jpg';
                $fileNameWithLoc = $tempSrcLoc . $fileName;
                file_put_contents($fileNameWithLoc, $imageContent);
                $configArray = array(
                    array(
                        'image_x' => 150,
                        'sub_dir_name' => 'medium'
                    ),
                    array(
                        'image_x' => 100,
                        'sub_dir_name' => 'small'
                    )
                );
                //$imgResult = $this->socialDPUpload($fileNameWithLoc, $actDesLoc, $fileNameBody, $configArray );
                $arrData['image'][$iVal] = $fileName;//strtolower($imgResult['fileName']);
                if (copy($fileNameWithLoc, ACTUAL_ROOTPATH . 'uploads/user_verification/' . $fileName)) {
                    copy(ACTUAL_ROOTPATH . 'uploads/user_verification/' . $fileName, ACTUAL_ROOTPATH . 'uploads/user_verification/medium/' . $fileName);
                    copy(ACTUAL_ROOTPATH . 'uploads/user_verification/' . $fileName, ACTUAL_ROOTPATH . 'uploads/user_verification/small/' . $fileName);
                    // copy(ACTUAL_ROOTPATH.'uploads/profile_picture/'.$fileNameBody.'.'.$ext, ACTUAL_ROOTPATH.'uploads/temp_dp/'.$fileName);
                }
                $verifyDocument = 1;
                $imageVerify = 1;
            }

            if (!empty($arrData)) {
                $arrData['verifyid'] = $this->params()->fromPost('verifyid');
                $arrVerifyDetails['first_name'] = 0;
                $arrVerifyDetails['last_name'] = 0;
                $arrVerifyDetails['gender'] = 0;
                $arrVerifyDetails['verfiyid'] = 0;
                $arrVerifyDetails['dob'] = 0;
                $arrVerifyDetails['image'] = 0;
                $arrData['verify_details'] = serialize($arrVerifyDetails);
                $resultids = $this->ProfileModel->addEditUserVerfications($arrData, $this->sessionObj->offsetGet('userid'));
                if (!empty($resultids)) {
                    $strHtml = implode(" ", $resultids);
                    echo json_encode(array('results' => 'sucess', 'html' => $strHtml, 'verifyDocument' => $verifyDocument, 'imageVerify' => $imageVerify));
                    exit;
                }
            }
        }
        elseif (isset($_POST['identity_type']) && !empty($_POST['identity_type']))
        {
            $identity_type = $_POST['identity_type'];
            $userRow = $this->ProfileModel->getprofile($this->sessionObj->offsetGet('userid'));
            $arrData = array();
            $verifyDocument = 0;
            $imageVerify = 0;
            foreach ($identity_type as $iKey => $iVal) {

                /* $userVerifyId = $_POST['verify_id']; */
                $verifyID = $_POST['ID'];
                $userVerifyFiles = isset($_FILES['verify_file']['name']) ? $_FILES['verify_file']['name'] : array();
                $arrData['identity_type'][] = $iVal;
                /* $arrData['verfiyid'][$iVal] = isset($userVerifyId[$iKey])?$userVerifyId[$iKey]:0; */
                $arrData['ID'][$iVal] = isset($verifyID[$iKey]) ? $verifyID[$iKey] : 0;
                $ID = isset($verifyID[$iKey]) ? $verifyID[$iKey] : 0;
                $resultRow = $this->ProfileModel->getVerifyRow($ID);
                /* if(strtolower($resultRow['verfiyid']) != strtolower($arrData['verfiyid'][$iVal]))$verifyDocument = 1; */
                if (!empty($userVerifyFiles)) {
                    if (isset($_FILES['verify_file']['error'][$iKey]) && $_FILES['verify_file']['error'][$iKey] == 0) {
                        $image_path = "user_verification";
                        $field_name = 'verify_file';
                        $files[$iKey]['name'] = $_FILES['verify_file']['name'][$iKey];
                        $files[$iKey]['type'] = $_FILES['verify_file']['type'][$iKey];
                        $files[$iKey]['tmp_name'] = $_FILES['verify_file']['tmp_name'][$iKey];
                        $files[$iKey]['error'] = $_FILES['verify_file']['error'][$iKey];
                        $files[$iKey]['size'] = $_FILES['verify_file']['size'][$iKey];
                        $file_name_prefix = 'user_verification_' . time();
                        $configArray = array(
                            array(
                                'image_x' => 150,
                                'sub_dir_name' => 'medium'
                            ),
                            array(
                                'image_x' => 100,
                                'sub_dir_name' => 'small'
                            )
                        );
                        //$imgResult = $this->uploadUserDPMultiple($files[$iKey],$file_name_prefix,$image_path,$configArray);
                        $ext = pathinfo($_FILES['verify_file']['name'][0], PATHINFO_EXTENSION);
                        $arrData['image'][$iVal] = $file_name_prefix . '.' . $ext;//strtolower($imgResult['fileName']);
                        if (move_uploaded_file($_FILES['verify_file']['tmp_name'][0], ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext)) {
                            copy(ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext, ACTUAL_ROOTPATH . 'uploads/user_verification/medium/' . $file_name_prefix . '.' . $ext);
                            copy(ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext, ACTUAL_ROOTPATH . 'uploads/user_verification/small/' . $file_name_prefix . '.' . $ext);
                            $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                            $username = $uD['first_name'];

                            if($iVal == 1){
                                $identity_name = 'Passport';
                            }
                            elseif($iVal == 2){
                                $identity_name = 'Driving Licence';
                            }
                            elseif($iVal == 3){
                                $identity_name = 'Government ID';
                            }

                            $hugedata1 = array
                            (
                                'user_id'               => $this->sessionObj->userid,
                                'notification_type'     => "verify_document",
                                'notification_subject'  => 'Document Verification',
                                'notification_message'  => "" . $username . " has uploaded " . $identity_name . " verification ID. Please take a decision at the earliest",
                                'admin_redirect'        => '/admin/users/usertrustverificationmanager?action=list&id=' . $this->sessionObj->userid . '&listUri=pending',
                                'notification_added'    => date("Y-m-d H:i:s")
                            );
                            $this->TripModel->addToNotifications($hugedata1);

                            $hugedata2 = array
                            (
                                'user_id'               => $this->sessionObj->userid,
                                'notification_type'     => "verify_document_inprogress",
                                'notification_subject'  => 'Document Verification',
                                'notification_message'  => "Thanks for uploading the " . $identity_name . " document, your document will be verified and approved shortly.",
                                'notification_added'    => date("Y-m-d H:i:s")
                            );
                            $this->TripModel->addToNotifications($hugedata2);

                            $hugedata = array
                            (
                                'user_id' => $this->sessionObj->userid,
                                'message' => "" . $username . " has uploaded " . $identity_name . " verification ID. Please take a decision at the earliest",
                                'create_date' => date("Y-m-d H:i:s")
                            );
                            $this->TripModel->addToAdminNotifications($hugedata);
                        }
                        $verifyDocument = 1;
                        $imageVerify = 1;
                    }
                }
            }

            if (!empty($arrData)) {
                $arrData['verifyid'] = $this->params()->fromPost('verifyid');
                $arrVerifyDetails['first_name'] = 0;
                $arrVerifyDetails['last_name'] = 0;
                $arrVerifyDetails['gender'] = 0;
                $arrVerifyDetails['verfiyid'] = 0;
                $arrVerifyDetails['dob'] = 0;
                $arrVerifyDetails['image'] = 0;
                $arrData['verify_details'] = serialize($arrVerifyDetails);
                $resultids = $this->ProfileModel->addEditUserVerfications($arrData, $this->sessionObj->offsetGet('userid'));
                if (!empty($resultids)) {
                    $strHtml = implode(" ", $resultids);
                    echo json_encode(array('results' => 'sucess', 'html' => $strHtml, 'verifyDocument' => $verifyDocument, 'imageVerify' => $imageVerify));
                    exit;
                }
            }
        }
        exit;
    }

    function strposa($haystack, $needle, $offset = 0)
    {
        if (!is_array($needle)) $needle = array($needle);
        foreach ($needle as $query) {
            if (strpos($haystack, $query, $offset) !== false) return true; /* stop on first true result  */
        }
        return false;
    }

    public function socialDPUpload($srcPath, $newPath, $file_name, $configArray)
    {
        if (isset($srcPath)) {
            $handle = new \upload($srcPath);
            if ($handle->uploaded) {
                $handle->file_new_name_body = $file_name;
                $handle->image_resize = true;
                $handle->process($newPath);
                if ($configArray && !empty($configArray)) {
                    foreach ($configArray as $config) {
                        $handle->file_new_name_body = $file_name;
                        $handle->image_resize = true;
                        $handle->image_x = $config['image_x'];
                        $handle->image_ratio_y = true;
                        $handle->process($newPath . $config['sub_dir_name'] . '/');
                    }
                    if ($handle->processed) {
                        $handle->clean();
                        $retArr = array('result' => 'success', 'fileName' => $handle->file_src_name);
                    } else {
                        $retArr = array('result' => 'failed', 'error' => $handle->error);
                    }
                }
            } else {
                $retArr = array('result' => 'failed', 'error' => $handle->error);
            }

            return $retArr;
        }
    }

    function swichUserAction($switchTo = 'seeker')
    {
        $this->sessionObj->userrole = $switchTo;
        return $this->redirect->toRoute($switchTo);
    }

    public function updateUserProfileAction()
    {
        $pn = $this->CommonMethodsModel->cleanQuery($_POST['phone']);
        $ccode = $this->CommonMethodsModel->cleanQuery($_POST['co_code']);
        $check_no = $this->CommonMethodsModel->checkExistingMobile($pn, $ccode, $this->sessionObj->userid);
       
        //print_r($check_no);exit;
        if (isset($_POST['location'])) {
            $as = explode(',', $_POST['location']);
            $res = array_slice($as, -3, 3, true);
            $mylocation = implode($res, ',');
            $addresss = '' . $_POST['location'] . '';

             $dob = explode('-',$_POST['brithday']); 

            $dob_format=$dob[2]."-".$dob[1]."-".$dob[0];

            $geo = file_get_contents('http://maps.googleapis.com/maps/api/geocode/json?address=' . urlencode($addresss) . '&sensor=false&key=AIzaSyCFsglnT8Q6LRNjxn3AO2tzdfyOdqNthq4');
            $geo = json_decode($geo, true);
            if ($geo['status'] = 'OK') {
                $latitude = $geo['results'][0]['geometry']['location']['lat'];
                $longitude = $geo['results'][0]['geometry']['location']['lng'];
                $timestamp = time();
                $timezoneAPI = "https://maps.googleapis.com/maps/api/timezone/json?location=" . $latitude . "," . $longitude . "&sensor=false&timestamp=" . $timestamp . "&key=AIzaSyCFsglnT8Q6LRNjxn3AO2tzdfyOdqNthq4";
                $response = file_get_contents($timezoneAPI);
                $response = json_decode($response);
				$myzone = '';
				if(!isset($response->errorMessage) || $response->errorMessage == ''){
                	$myzone = $response->timeZoneId;
				}
            }
        }
        if ($check_no == 1) {

            $userdata = array(
                "first_name" => $this->CommonMethodsModel->cleanQuery($_POST['firstname']),
                "last_name" => $this->CommonMethodsModel->cleanQuery($_POST['lastname']),
                "dob" => $this->CommonMethodsModel->cleanQuery($dob_format),
                "phone" => $this->CommonMethodsModel->cleanQuery($_POST['phone']),
                "gender" => $this->CommonMethodsModel->cleanQuery($_POST['gender']),
                "language" => $this->CommonMethodsModel->cleanQuery(implode(",", $_POST['language'])),
                //"email_address" => $this->CommonMethodsModel->cleanQuery($_POST['']),    
                "co_code" => $this->CommonMethodsModel->cleanQuery($_POST['co_code']),
                /*"city" => $this->CommonMethodsModel->cleanQuery($_POST['city']),*/
                "location" => $this->CommonMethodsModel->cleanQuery($mylocation),
                "timezone" => $this->CommonMethodsModel->cleanQuery($myzone),
                "aboutus" => $this->CommonMethodsModel->cleanQuery($_POST['aboutus']),
                "interests" => $this->CommonMethodsModel->cleanQuery($_POST['interest']),
                "emergency_full_name" => $this->CommonMethodsModel->cleanQuery($_POST['emergency_full_name']),
                "emergency_phone" => $this->CommonMethodsModel->cleanQuery($_POST['emergency_phone']),
                "emergency_email" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['emergency_email'])),
                "emergency_relationship" => $this->CommonMethodsModel->cleanQuery($_POST['emergency_relationship'])
            );
            $res = $this->CommonMethodsModel->updteUserProfileData($userdata, $this->sessionObj->userid);
			$this->sessionObj->user['first_name'] = $userdata['first_name'];
			$this->sessionObj->user['last_name'] = $userdata['last_name'];
            //print_r($res);die;
            //$this->flashMessenger()->addMessage(array('alert-success' => 'Address has been deleted successfully.'));
            echo 1;//$this->redirect()->toUrl('/myprofile?profAction=edit');
            exit;
        } else {
            echo 0;//$this->redirect()->toUrl('/myprofile?profAction=edit&err='.base64_encode($pn));
            exit;
        }
    }

    public function addOtpCodeAction()
    {
        $otp = mt_rand(100000, 999999);
        $client = new Client('https://api.plivo.com/v1/Account/MAMGY0ZJVLMMU0OTGZYT/Message/', array(
            'maxredirects' => 0,
            'timeout' => 3000
        ));

        $params = array(
            'src' => '+18312011527',
            'dst' => $_POST['phone'],
            'text' => 'TREPR phone verification OTP is: ' . $otp
        );
        $client->setHeaders([
            'Content-Type' => 'application/json',
        ])
            ->setOptions(['sslverifypeer' => false])
            ->setMethod('POST')
            ->setRawBody(Json::encode($params));
        $client->setAuth('MAMGY0ZJVLMMU0OTGZYT', 'NDZlMTIyZWFhMDdiODQxOGE0NmZjYzdkY2FjNmEw');
        $response = $client->send();
        //echo $response->getBody();die;
        $userdata = array(
            "otp" => $this->CommonMethodsModel->cleanQuery($otp),
            "phone" => $this->CommonMethodsModel->cleanQuery($_POST['phone']),
            "otp_added_on" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s'))
        );
        //print_r($userdata);die;
        $res = $this->CommonMethodsModel->addOTPCodesSignUp($userdata);
        echo 1;
        exit;
    }

    public function verifyOtpCodeAction()
    {
        //print_r($_POST);die;
        if ($this->sessionObj->userid != "") {
            $uid = base64_encode($this->sessionObj->userid);
        } else {
            $uid = $this->sessionObj->reqistration_user_id;
        }
        $userdata = array(
            "otp" => $this->CommonMethodsModel->cleanQuery($_POST['otp']),
            "phone" => $this->CommonMethodsModel->cleanQuery($_POST['phone']),
            "co_code" => 'IN',
            "number" => $this->CommonMethodsModel->cleanQuery($_POST['number'])
        );
        $res = $this->CommonMethodsModel->verifyOTPCode($userdata, $uid);
        echo $res;
        exit;
    }

    public function makeAddressAsPrimaryAction()
    {
        //print_r($_POST);die;
        $userdata = array(
            "ID" => $this->CommonMethodsModel->cleanQuery($_POST['id'])
        );
        $res = $this->CommonMethodsModel->makeAddressPrimary($userdata, $this->sessionObj->userid);
        echo $res;
        exit;
    }

    public function getLoginNotificationsAction()
    {
        $userdata = array(
            "ID" => $this->CommonMethodsModel->cleanQuery($_POST['userId'])
        );
        $res = $this->CommonMethodsModel->getLoginNotifications($_POST['userId']);
        echo json_encode($res);
        exit;
    }

    public function getNotificationsAction()
    {
        $userdata = array(
            "ID" => $this->CommonMethodsModel->cleanQuery($_POST['userId'])
        );
        $res = $this->CommonMethodsModel->getNotifications($_POST['userId']);
        $res['count'] = count($res);
        echo json_encode($res);
        exit;
    }

    public function getLatestNotificationsAction()
    {
        $userdata = array(
            "user_id" => $this->CommonMethodsModel->cleanQuery($_POST['userId'])
        );
        $res = $this->CommonMethodsModel->getLatestNotifications($_POST['userId']);
		
        if (!empty($res)) {
            $res['count'] = count($res);
        }
        echo json_encode($res);
        exit;
    }

    public function closeNotificationAction()
    {
        $userdata = array(
            "ID" => $this->CommonMethodsModel->cleanQuery($_POST['userId'])
        );
        $res = $this->CommonMethodsModel->closeNotification($_POST['notification_id']);
        echo $res;
        exit;
    }

    public function getQuestionsAction()
    {
        if (isset($_POST['text'])) {
            $res = $this->CommonMethodsModel->getQuestion($_POST['text'], 'text');
        } elseif (isset($_POST['id'])) {
            $res = $this->CommonMethodsModel->getQuestion($_POST['id'], 'id');
        }
        echo json_encode($res);
        exit;
    }

    public function getAnswerAction()
    {
        //$text = $this->CommonMethodsModel->cleanQuery($_POST['text']);
        $res = $this->CommonMethodsModel->getAnswer($_POST['id']);
        $aray = array();
        foreach ($res['data'] as $r) {
            $aray['qa_answer'] = utf8_encode($r['qa_answer']);
        }
        //print_r($aray);
        echo json_encode(array("data" => $aray));
        exit;
    }

    public function verifyemailtosignupOldAction()
    {
        $id = base64_decode($_GET['user']);

        $res = $this->CommonMethodsModel->verifyemailtosignup($id);
        if ($res) {
            $this->sessionObj->offsetSet('userid', $id);
            $userdetail = $this->CommonMethodsModel->getUser($id);
            $this->sessionObj->offsetSet('user', $userdetail);
            $first_name = strtoupper($userdetail['first_name']);
            if ($userdetail['image'])
                $img = $this->basePath . '/' . $userdetail['image'];
            else
                $img = $this->basePath . '/' . 'default_user.png';
            echo "Your account has been created successfully.@SPLIT@" . '<img class="hidden-xs origin round" width="40" height="40" src="' . $img . '" alt="' . $userdetail['first_name'] . '" /> Hi, ' . $userdetail['first_name'];
            return $this->redirect()->toUrl('/dashboard');
            exit;
        } else {
            return $this->redirect()->toUrl('/');
            exit;
        }
    }

    public function verifyemailtosignupAction()
    {
        $id = base64_decode($_GET['user']);
        $userdetail = $this->CommonMethodsModel->getUser($id);
        if ($userdetail['active'] == 0) {
            $this->sessionObj->offsetSet('verifymail', 1);
            $this->sessionObj->offsetSet('reqistration_user_id', $_GET['user']);
            if ($_GET['bt'] == '1') {
                $this->sessionObj->offsetSet('bt', 1);
            }
            return $this->redirect()->toUrl('/');
            exit;
        } else {
            return $this->redirect()->toUrl('/');
            exit;
        }
    }

    public function completeTripAction()
    {
        $res = $this->CommonMethodsModel->completetrip();
        echo $res;
        exit;
    }

    public function completesignupAction()
    {
        //print_r($_FILES);die;
        if ($_FILES['propic']['name']) {
            $file = 'propic';
            $image_path = "profile_picture";
            $file_name_prefix = 'USER_DP_' . time();
            $configArray = array(
                array(
                    'image_x' => 150,
                    'sub_dir_name' => 'medium'
                ),
                array(
                    'image_x' => 100,
                    'sub_dir_name' => 'small'
                )
            );
            //$imgResult = $this->uploadUserDP($file,$file_name_prefix,$image_path,$configArray);
            $imgResult['result'] = 'success';//$this->uploadUserDPMultiple($file,$file_name_prefix,$image_path,$configArray);
            $ext = pathinfo($_FILES['propic']['name'], PATHINFO_EXTENSION);
            if (move_uploaded_file($_FILES['propic']['tmp_name'], ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext)) {
                copy(ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext, ACTUAL_ROOTPATH . 'uploads/profile_picture/medium/' . $file_name_prefix . '.' . $ext);
                copy(ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext, ACTUAL_ROOTPATH . 'uploads/profile_picture/small/' . $file_name_prefix . '.' . $ext);
            }
            if ($imgResult['result'] == 'success') {
                $idverfy_image = $file_name_prefix . '.' . $ext;//$imgResult['fileName'];
                $arr_photo_detail = array(
                    'user_id' => $this->CommonMethodsModel->cleanQuery(base64_decode($this->sessionObj->reqistration_user_id)),
                    'user_photo_name' => $this->CommonMethodsModel->cleanQuery($idverfy_image),
                    'user_photo_added' => time()
                );
                $photoid = $this->ProfileModel->addUserPhoto($arr_photo_detail, base64_decode($this->sessionObj->reqistration_user_id));
                //$this->ProfileModel->markAsProfilePhoto($photoid,base64_decode($this->sessionObj->reqistration_user_id));
                $this->ProfileModel->addUserProfilePicture($photoid, base64_decode($this->sessionObj->reqistration_user_id));

            }
        }
        $res = $this->CommonMethodsModel->completesignup($this->sessionObj->reqistration_user_id);

        $userdetail = $this->CommonMethodsModel->getUser(base64_decode($this->sessionObj->reqistration_user_id));
        $this->sessionObj->offsetSet('userid', base64_decode($this->sessionObj->reqistration_user_id));
        $this->sessionObj->offsetSet('user', $userdetail);
        //print_r($userdetail);die;
        $this->sessionObj->offsetSet('verifymail', 0);
        $this->sessionObj->offsetSet('reqistration_user_id', '');
        $first_name = strtoupper($userdetail['first_name']);
        if ($userdetail['image']) {
            $img = $this->basePath . '/' . $userdetail['image'];
        } else {
            $img = $this->basePath . '/' . 'default_user.png';
        }
        // echo "Your account has been successfully created.@SPLIT@" . '<img class="hidden-xs origin round" width="40" height="40" src="'.$img.'" alt="' . $userdetail['first_name'] . '" /> Hi, ' . $userdetail['first_name'];
        if ($this->sessionObj->bt == '1') {
            $this->sessionObj->offsetSet('bt', 0);
            return $this->redirect()->toUrl('/becometraveler/travelinfo');
            exit;
        } else {
            return $this->redirect()->toUrl('/dashboard');
            exit;
        }

    }

    public function getqacategoriesAction()
    {
        $id = $_POST['id'];
        $res = $this->CommonMethodsModel->getQASubCategories($id);
        echo json_encode($res);
        exit;
    }

    public function checkuserprofilestatusAction()
    {
        $email = base64_decode($_GET['need_user_profile']);
        $res = $this->CommonMethodsModel->checkuserprofilestatus($email);
        if ($res) {
            return $this->redirect()->toUrl('/dashboard');
            exit;
        } else {
            $this->sessionObj->offsetSet('need_user_profile', '1');
            return $this->redirect()->toUrl('/logout');
            exit;
        }
    }

    public function adduserreviewsAction()
    { //print_r($_POST); die;
        $ID = $this->CommonMethodsModel->cleanQuery($_POST['trip_id']);

        if ($_POST['trip_id']) {
            $revw = $this->CommonMethodsModel->cleanQuery($_POST['review_text']);
            $trip_review = array(
                'trip_id' => $ID,
                'message' => $revw,
                'rating' => $this->CommonMethodsModel->cleanQuery($_POST['rating_val']),
                'recevier_id' => 0,
                'create_by' => $this->sessionObj->userid,
                'sender_id' => $this->sessionObj->userid
            );
            $this->TripModel->addTripreview($trip_review, $ID);
            //$this->view->triprewadd = "Trip Review Successfully submited.";
            echo 1;
        }
        /// $this->view->triprew = $this->TripModel->getTripreview($ID, $this->sessionObj->userid);
        echo 0;
    }

    public function testingmailAction()
    {

        $message = new Message();
        $message->addTo('karisdft456@gmail.com')
            ->addFrom('no-reply@trepr.com')
            ->setSubject('Hi ranjith')
            ->setBody("wish you luck for your trepr.co.uk !");

// Setup SMTP transport using LOGIN authentication
        $transport = new SmtpTransport();
        /*$options   = new SmtpOptions(array(
            'name'              => 'localhost',
            'host'              => 'ssl://smtp.zoho.com:465',
            'connection_class'  => 'login',
            'connection_config' => array(
                'username' => 'no-reply@trepr.com',
                'password' => 'Trepr#2017',
            ),
        ));*/
        /*$options   = new SmtpOptions([
            'name'              => 'trepr.com',
            'host'              => 'smtp.zoho.com',
            'port'              => 465,
            // Notice port change for TLS is 587
            'connection_class'  => 'plain',
            'connection_config' => [
                'username' => 'no-reply@trepr.com',
                'password' => 'Trepr#2017',
                'ssl'      => 'tls',
            ],
        ]);*/
         $options = new SmtpOptions(
         ['name' => 'trepr.co.uk', 
         'host' => 'mail.trepr.co.uk',
         'port' => 25, 'connection_class' => 'plain', 
         'connection_config' => ['username' => 'admin@trepr.co.uk', 'password' => 'Myth#2018', 'ssl' => ''],]);
        $transport->setOptions($options);

        
        $aa = $transport->send($message);
        $msg = "First line of text\nSecond line of text";
        //if(mail("ashok@naxtre.com","My subject",$msg))
        if ($aa) {
            echo 1;
            exit();
        } else {
            echo 0;
            exit();
        }
    }
}