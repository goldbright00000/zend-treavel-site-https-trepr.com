<?php 
namespace Application\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager; 
class ProfileController extends AbstractActionController {
    var $sessionObj;
    var $CommonPlugin;
    var $GoogleplusPlugin;
    protected $serviceLocator;
    var $siteConfigs;
    public function __construct($data=false){ 
	/*Loading models from factory         * Call the model object using it's class name        */          
	if($data['models'] && is_array($data['models'])){
		foreach($data['models'] as $model){ 
		$modelName = $model['name'];
                $this->$modelName = $model['obj'];
		}       
	}        
	$this->sessionObj = new Container('comSessObj');
	$this->siteConfigs = $data['configs']['siteConfigs'];
	$this->timeZones = $data['configs']['timezones'];    
	}	    
	public function indexAction() {
		$viewArray = array();
        $viewModel = new ViewModel();
        //$this->_helper->layout->disableLayout();
        //$this->view->upcoming_trips =  $this->_helper->Trip->getTrips('upcoming');
        //$this->view->completed_trips =  $this->_helper->Trip->getTrips('completed');
        //$this->view->cancelled_trips =  $this->_helper->Trip->getTrips('cancelled');
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
		$viewArray['timeZones'] = $this->timeZones;
        $viewArray['prof']  = $this->ProfileModel->getprofile( $this->sessionObj->offsetGet('userid'));
        //var_dump( $viewArray['prof']);exit;
        $viewArray['profaddr']   = $this->ProfileModel->getprofileaddr( $this->sessionObj->offsetGet('userid'));
        $viewArray['profaddrmail']  = $this->ProfileModel->getprofileaddrmail( $this->sessionObj->offsetGet('userid'));
        $viewArray['lang']   = $this->ProfileModel->getlang( $this->sessionObj->offsetGet('userid'));
		$viewArray['abtus']  = $this->ProfileModel->getabtus( $this->sessionObj->offsetGet('userid'));
        $viewArray['reviews']  = $this->ProfileModel->getreviews( $this->sessionObj->offsetGet('userid')); 
		//echo "<pre>"; print_r($_POST); exit; 
		$viewArray['addrbook'] =$this->CommonMethodsModel->LoadUsersAddressBook( $this->sessionObj->offsetGet('userid'));
        $viewArray['notiferr'] =$this->ProfileModel->getprofilenotification( $this->sessionObj->offsetGet('userid'));
        if(isset($_POST['subabs']))		 {
            //header('Content-type: image/jpg'); 
			//print "<pre>".print_r($_FILES,1)."</pre>";exit; 
			if($_FILES['upfile']['name']){                
			//$image_path = $this->generalvar['temp'];               
			$image_path = "temp/";                
			$field_name = 'upfile';                
			$file_name_prefix = 'user_'.time();                
			$idverfy_image = $this->CommonMethodsModel->uploadFile($field_name,$file_name_prefix,$image_path);                
			$arr_peopleabs_detail = array(                    
			'aboutus'	=> $this->CommonMethodsModel->cleanQuery($_POST['aboutus']),
			'video' 	=> $this->CommonMethodsModel->cleanQuery(isset($_POST['video'])),
			'image' 	=> $this->CommonMethodsModel->cleanQuery($idverfy_image), 
			'modified' 	=> time()               
			);           
			}else{               
			$arr_peopleabs_detail = array(                     
			'aboutus'	=> $this->CommonMethodsModel->cleanQuery($_POST['aboutus']), 
			'video'		=> $this->CommonMethodsModel->cleanQuery($_POST['video']),                     
			'modified'	=> time()               
			);             
			}            
			$this->ProfileModel->updateprofileabus($arr_peopleabs_detail, $this->sessionObj->offsetGet('userid')); 
			$this->redirect()->toRoute('profile');       
			}           
			if(isset($_POST['verfyid'])){    
			$image_path = $this->generalvar['trips']['trips']['identy'];    
			$field_name = 'idverfy';              
			$file_name_prefix = $this->generalvar['trips']['trips']['filename']; 
			$idverfy_image = $this->CommonMethodsModel->uploadFile($field_name,$file_name_prefix,$image_path);  
			$arr_peoplevery_detail = array(               
			'verfiyid'    => $this->CommonMethodsModel->cleanQuery($_POST['verfiyid']), 
			'nationalid'    => $this->CommonMethodsModel->cleanQuery($_POST['nationalid']),  
			'user_id'    =>  $this->sessionObj->offsetGet('userid'),                
			'image' => $this->CommonMethodsModel->cleanQuery($idverfy_image),          
			'modified' => time()            
			);             
			$this->ProfileModel->updateprofileverfy($arr_peoplevery_detail,$this->sessionObj->userid);  
			}      
			if(isset($_POST['firstname'])){   
			/*            $firstname = $this->CommonMethodsModel->cleanQuery($_POST['firstname']);
            $lastname = $this->CommonMethodsModel->cleanQuery($_POST['lastname']);  
			$brithday = $this->CommonMethodsModel->cleanQuery($_POST['brithday']);  
			$gender = $this->CommonMethodsModel->cleanQuery($_POST['gender']);      
			$email = $this->CommonMethodsModel->cleanQuery($_POST['email']);        
			$phone = $this->CommonMethodsModel->cleanQuery($_POST['phone']);        
			$language = $this->CommonMethodsModel->cleanQuery($_POST['language']);  
			$user_id=$this->sessionObj->userid;            */        
			//echo $_POST['brithday'];exit;           
			$dateb=str_replace("/","-",$_POST['brithday']);   
			$dateb2=strtotime($dateb);          
			$language = implode(",", $_POST['language']);  
			$birthDate = date("m/d/Y",strtotime($dateb));  
			$birthDate = explode("/", $birthDate);         
			$age = (date("md", date("U", mktime(0, 0, 0, $birthDate[0], $birthDate[1], $birthDate[2]))) > date("md") ? ((date("Y") - $birthDate[2]) - 1)              : (date("Y") - $birthDate[2]));
            $arr_people_detail = array(      
			'first_name'    => $this->CommonMethodsModel->cleanQuery($_POST['firstname']),         
			'last_name'   	=> $this->CommonMethodsModel->cleanQuery($_POST['lastname']),      
			'dob'       	=> $this->CommonMethodsModel->cleanQuery($dateb2),         
			'age'       	=> $this->CommonMethodsModel->cleanQuery($age),            
			'phone'    		=> $this->CommonMethodsModel->cleanQuery($_POST['phone']),   
			'country'    	=> $this->CommonMethodsModel->cleanQuery(($_POST['country'])?$_POST['country']:NULL),  
			'city'              => $this->CommonMethodsModel->cleanQuery(($_POST['city'])?$_POST['city']:NULL),    
			'school'            => $this->CommonMethodsModel->cleanQuery(($_POST['school'])?$_POST['school']:NULL), 
			'work'              => $this->CommonMethodsModel->cleanQuery(($_POST['work'])?$_POST['work']:NULL),     
			'timezone'          => $this->CommonMethodsModel->cleanQuery(($_POST['timezone'])?$_POST['timezone']:NULL),   
			'emergency_full_name'=> $this->CommonMethodsModel->cleanQuery(($_POST['emergency_full_name'])?$_POST['emergency_full_name']:NULL),  
			'emergency_phone'   => $this->CommonMethodsModel->cleanQuery(($_POST['emergency_phone'])?$_POST['emergency_phone']:NULL),  
			'emergency_email'   => $this->CommonMethodsModel->cleanQuery(($_POST['emergency_email'])?$_POST['emergency_email']:NULL), 
			'emergency_relationship'=> $this->CommonMethodsModel->cleanQuery(($_POST['emergency_relationship'])?$_POST['emergency_relationship']:NULL), 
			'aboutus'    	=> $this->CommonMethodsModel->cleanQuery($_POST['aboutus']),   
			'language' 		=> $this->CommonMethodsModel->cleanQuery($language),      
			'gender' 		=> $this->CommonMethodsModel->cleanQuery($_POST['gender']), 
			'modified' 		=> time()        
			);		
			// echo "<pre>";
			// print_r($_POST['language']);		
			// echo "</pre>";		
			// exit;			
			$maildef=$this->CommonMethodsModel->cleanQuery($_POST['mailaddr']);	
			/* if($maildef==1){			
			$arr_address_detail = array(
			'street_address_1'	=> $this->CommonMethodsModel->cleanQuery($_POST['address']),		
			'country'			=> $this->CommonMethodsModel->cleanQuery($_POST['city']),		
			'zip_code'			=> $this->CommonMethodsModel->cleanQuery($_POST['zipcode']),	
			'user'				=> $this->sessionObj->userid,				
			'address_type'		=> "home"			
			);			
			$arr_addressmail_detail = array(	
			'street_address_1'	=> $this->CommonMethodsModel->cleanQuery($_POST['address']),
			'country'			=> $this->CommonMethodsModel->cleanQuery($_POST['city']),	
			'zip_code'			=> $this->CommonMethodsModel->cleanQuery($_POST['zipcode']),
			'user'				=> $this->sessionObj->userid,				
			'address_type'		=> "mail"		
			);			
			}else {			
			$arr_address_detail = array(
			'street_address_1'	=> $this->CommonMethodsModel->cleanQuery($_POST['address']),
			'country'			=> $this->CommonMethodsModel->cleanQuery($_POST['city']),	
			'zip_code'			=> $this->CommonMethodsModel->cleanQuery($_POST['zipcode']),
			'user'				=> $this->sessionObj->userid,				
			'address_type'		=> "home"		
			);				
			$arr_addressmail_detail = array(	
			'street_address_1'	=> $this->CommonMethodsModel->cleanQuery($_POST['maddress']),	
			'country'			=> $this->CommonMethodsModel->cleanQuery($_POST['mcity']),	
			'zip_code'			=> $this->CommonMethodsModel->cleanQuery($_POST['mzipcode']),	
			'user'				=> $this->sessionObj->userid,	
			'address_type'		=> "mail"			
			);			
			} */		
			$this->ProfileModel->updateprofile($arr_people_detail,$this->sessionObj->offsetGet('userid'));
			//$this->ProfileModel->updateprofileaddhome($arr_address_detail,$this->sessionObj->userid);	
			//$this->ProfileModel->updateprofileaddmail($arr_addressmail_detail,$this->sessionObj->userid); 
			$this->redirect()->toRoute('profile');			 	
			}                                              
			// Traity Verification                $current_user_id = $this->sessionObj->offsetGet('userid');    
            $app_key = 'IL8KFOnvyHZwTfYkPJIPg';                $app_secret = 'bd6hUNAiy2etdTedE36glqRI2b11SpIk2OA';    
			include_once ACTUAL_ROOTPATH."traity/php-jwt-master/src/JWT.php";            
			$JWT = new \Firebase\JWT\JWT();           
			$options   = array('verified' => array('phone', 'email'));   
			$payload = array_merge(array('time' => time(),'current_user_id' => $current_user_id), $options); 
			$signature = array(            
			'key' => $app_key,               
			'payload' => $JWT->encode($payload, $app_secret)); 
			$viewArray['widget_signature']  =  $JWT->encode($signature, '');      
			//$viewArray['flashMessages'] =  $this->flashMessenger()->getMessages();   
			$viewModel->setVariables($viewArray)                    ->setTerminal(true);       
			return $viewModel;	
			}
			public function camingAction(){		
			//print_r($_REQUEST); exit;		
			$upload_dir = "temp/";	
			$img = $_POST['camimage'];		
			$img = str_replace('data:image/png;base64,', '', $img);	
			$img = str_replace(' ', '+', $img);		$data = base64_decode($img);	
			$filename=mktime() . ".png";		$file = $upload_dir .$filename; 	
			$success = file_put_contents($file, $data);		
			//print $success ? $file : 'Unable to save the file.';		
			$arr_peopleabs_detail = array(			'image'    => $this->CommonMethodsModel->cleanQuery($filename),	
			'modified' => time()	
			);	
			$this->ProfileModel->updateprofileabus($arr_peopleabs_detail,$this->sessionObj->userid);	
			$this->_redirect("/profile");	
			/* 			if($_REQUEST['image']!=''){	
			//echo $_REQUEST['image']; exit;		
			}			
			$image_path = $this->generalvar['temp'];
			$field_name = 'image';		
			//$file_name_prefix = 'profile_image'.$this->sessionObj->userid;
			$file_name_prefix = $this->generalvar['profile']['img']['filename'];	
			$ticket_image = $this->CommonMethodsModel->uploadFile($field_name,$file_name_prefix,$image_path);	
			*/	
			}
			public function profileAction() {		
			$origin = $this->CommonMethodsModel->cleanQuery($_POST['origin']);	
			//$origin = explode(',',$origin);		
			//$origin_city = $origin[0];	
			$destination = $this->CommonMethodsModel->cleanQuery($_POST['destination']);
			//$destination = explode(',',$destination);		
			//$destination_city = $destination[0];		$date = $this->CommonMethodsModel->cleanQuery($_POST['start_date']);	
			$date = date('Y-m-d H:i:s',  strtotime($date));	
			$service = $this->CommonMethodsModel->cleanQuery($_POST['service']);
			$looking_for = $this->CommonMethodsModel->cleanQuery($_POST['looking_for']);
			if($service == 'people' && $looking_for=='traveller'){	
			$trips = $this->TripModel->getTrips($origin,$destination,$date);	
			$travellers = $this->TripModel->getTravellers($origin,$destination,$date);		
			$this->view->travellers = json_encode($travellers);		
			$this->view->map_center_lat = $trips[0]['origin_latitude'];	
			$this->view->map_center_long = $trips[0]['origin_latitude'];
			//echo "<pre>";print_r($travellers); exit;
			}elseif($service == 'people' && $looking_for=='traveller'){    
			$travellers = $this->TripModel->getTravellersForPackage($origin,$destination,$date);
			$this->view->travellers = json_encode($travellers);		
			$this->view->map_center_lat = $trips[0]['origin_latitude'];		
			$this->view->map_center_long = $trips[0]['origin_latitude'];    
    		}	
			}	
			public function logoutAction(){	
			$this->_helper->layout->disableLayout();
			if($this->sessionObj->userid){			
			$this->sessionObj->userid = "";			
			Zend_Session::destroy();		
			}	
			}	
			public function checkuseremailAction(){	
			$this->_helper->layout->disableLayout();	
			$this->_helper->viewRenderer->setNoRender(true);	

			$from = $_REQUEST['from'];
			if($from == 'signup'){		
			$checkUserEmailExists = $this->CommonMethodsModel->checkUserEmailExists($_REQUEST['email_address']);	
			if(count($checkUserEmailExists) == 0)	
				echo 'true';	
			else			
				echo 'false';	
			} else {		
			$email_address = $_REQUEST['reset_email'];
			if($email_address=='')		
				$email_address = $_REQUEST['reset_password_email'];  
			$checkUserEmailExists = $this->CommonMethodsModel->checkUserEmailExists($email_address);
			if(count($checkUserEmailExists) == 0)		
				echo 'false';			
			else			
				echo 'true';	
			}    
			}	
			public function signupAction(){		
			$this->_helper->layout->disableLayout();    
			$this->_helper->viewRenderer->setNoRender(true);	
			if($_POST){		
			$type = '1';	
			$middle_name = "";	
			/*					$date_of_birth = "0000-00-00 00:00:00";		
			$gender = '1';				
			$mail_subscribe = '0';	
			*/		
			$active = '1';	
			$dob = date('Y-m-d H:i:s',strtotime($_POST['dob']));	
			//$dob = explode('/', $dob);	
			//$dob = $dob[2].'-'.$dob[0].'-'.$dob;	
			$user = array(		
			"first_name"=> $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['first_name']))),
			"middle_name"=> $this->CommonMethodsModel->cleanQuery($middle_name),	
			"last_name"=> $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['last_name']))),	
			"email_address" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])),		
			"password" => md5($_POST['password']),		
			/*    						"date_of_birth" => $this->CommonMethodsModel->cleanQuery($date_of_birth),		
			"gender"=>$this->CommonMethodsModel->cleanQuery($gender),		
			"mail_subscribe"=>$this->CommonMethodsModel->cleanQuery($mail_subscribe),		
			*/		
			"dob" => $this->CommonMethodsModel->cleanQuery($dob),	
			"subscribe" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['subscribe'])),		
			"active" => $this->CommonMethodsModel->cleanQuery($active),		
			"modified" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s'))		
			);		
			$userid = $this->CommonMethodsModel->addUsersUser($user);
			$this->sessionObj->userid = $userid;		
			$user = $this->CommonMethodsModel->getUser($userid);
			$this->sessionObj->user = $userdetail;		
			$first_name = strtoupper($userdetail[first_name]);	
			if($user['image'])		
				$img = $user['image'];	
			else			
				$img = 'default_user.png'; 
   			echo "Your account has been created successfully.@SPLIT@".'<img class="hidden-xs origin round" width="40" height="40" src="'. $this->view->baseUrl().'/temp/'.$img.'" alt="'.$user['first_name'].'" /> Hi, '.$user['first_name'];
			}
			}
			public function addaddressbookAction(){  
			if ($_POST['addrtype'] == 'newaddress') {  
			if(isset($_POST['setdeflt'])){         
			$seld=1;                   
			$this->TripModel->updateTripContactAddresssetdef($this->sessionObj->userid);  
			}else{              
			$seld=0;           
			}              
			$arr_trip_contact = array(   
			'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),    
			'name' => $this->CommonMethodsModel->cleanQuery($_POST['contact_name']),      
			'street_address_1' => $this->CommonMethodsModel->cleanQuery($_POST['route']),     
			'street_address_2' => $this->CommonMethodsModel->cleanQuery($_POST['sublocality']),  
			'city' => $this->CommonMethodsModel->cleanQuery($_POST['locality']),     
			'state' => $this->CommonMethodsModel->cleanQuery($_POST['administrative_area_level_1']),  
			'zip_code' => $this->CommonMethodsModel->cleanQuery($_POST['postal_code']),     
			'country' => $this->CommonMethodsModel->cleanQuery($_POST['country']),       
			//'phone' => $this->CommonMethodsModel->cleanQuery($_POST['phone']),         
			//'fax' => $this->CommonMethodsModel->cleanQuery($_POST['fax']),             
			'latitude' => $this->CommonMethodsModel->cleanQuery($_POST['lat']),          
			'longitude' => $this->CommonMethodsModel->cleanQuery($_POST['lng']),         
			'modified' => date('Y-m-d H:i:s'),                
			'set_default' => $this->CommonMethodsModel->cleanQuery($seld)        
			);       
			$user_location = $this->TripModel->addTripContactAddress($arr_trip_contact); 
			//$this->flashMessenger()->addMessage(array('alert-success' => 'Address added successfully!'));  
			$this->sessionObj->offsetSet('successMsg', 'Address has been added successfully!');       
			}           
			exit;	
			}	
			public function editaddressbookAction(){   
			if($_POST['vtype']=='view'){         
			$editresp=$this->CommonMethodsModel->viewUsersAddressBook($_POST['viewid'],$this->sessionObj->userid);    
			echo json_encode($editresp);    
			}         
			elseif($_POST['vtype']=='delete'){      
			//echo 'delete-here';exit;  
			$editresp=$this->CommonMethodsModel->deleteUsersAddressBook($_POST['viewid'],$this->sessionObj->userid);  
			//$this->flashMessenger()->addMessage(array('alert-success' => 'Address added successfully!'));   
			$this->sessionObj->offsetSet('successMsg', 'Address has been deleted successfully!');   
			return true;       
			}         
			elseif($_POST['addrtype']=='editaddress'){     
			if($_POST['setdeflt']){         
			$seld=1;                  
			$this->TripModel->updateTripContactAddresssetdef($this->sessionObj->userid);
			}else{           
			$seld=0;          
			}           
			$arr_trip_contact = array(   
			'name' => $this->CommonMethodsModel->cleanQuery($_POST['contact_name']),
			'street_address_1' => $this->CommonMethodsModel->cleanQuery($_POST['route']),  
			'street_address_2' => $this->CommonMethodsModel->cleanQuery($_POST['sublocality']),  
			'city' => $this->CommonMethodsModel->cleanQuery($_POST['locality']),            
			'state' => $this->CommonMethodsModel->cleanQuery($_POST['administrative_area_level_1']),  
			'zip_code' => $this->CommonMethodsModel->cleanQuery($_POST['postal_code']),       
			'country' => $this->CommonMethodsModel->cleanQuery($_POST['country']),        
            'phone' => $this->CommonMethodsModel->cleanQuery($_POST['phone']),            
			'fax' => $this->CommonMethodsModel->cleanQuery($_POST['fax']),                
			'latitude' => $this->CommonMethodsModel->cleanQuery($_POST['lat']),           
			'longitude' => $this->CommonMethodsModel->cleanQuery($_POST['lng']),          
			'modified' => date('Y-m-d H:i:s'),         
			'set_default' => $this->CommonMethodsModel->cleanQuery($seld)          
			);           
			$user_location = $this->TripModel->updateTripContactAddress($arr_trip_contact,$_POST['editid'],$this->sessionObj->userid);   
			$this->sessionObj->offsetSet('successMsg', 'Address has been updated successfully!');      
			//$userid = $this->CommonMethodsModel->addUsersAddressBook($user);     
			}         
			elseif($_POST['eaddress']!=''){   
			$user = array(        
            "adrtype"=> $this->CommonMethodsModel->cleanQuery((strtolower("home"))),   
			"ad_name"=> $this->CommonMethodsModel->cleanQuery(strtolower($_POST['contact_name'])),    
			"address"=> $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['eaddress']))),  
			"city" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['city'])),       
			"state" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['state'])),     
			"postcode" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['postal_code'])),          
			"country" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['country'])),        
			"addrdefault" => $this->CommonMethodsModel->cleanQuery($_POST['setdeflt']),       
			"longitude" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['longitude'])),  
			"latitude" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['latitude']))   
			);             
			$userid = $this->CommonMethodsModel->updteUsersAddressBook($user,$this->sessionObj->userid,$_POST['editid']); 
			$this->sessionObj->offsetSet('successMsg', 'Address has been updated successfully!');     
			//print_r($userid);         
			}  
			exit;    
            //$this->redirect()->toRoute('profile');          
			/*                $viewModel = new ViewModel();     
			$viewModel->setTerminal(true);           
			return $viewModel;*/	
			}	
			public function signuplightboxAction(){	
			$this->_helper->layout->disableLayout();
    		$this->_helper->viewRenderer->setNoRender(true);
			if($_POST){		
			$type = '1';	
			$middle_name = "";	
			$active = '1';		
			$user = array(		
			"type" => $this->CommonMethodsModel->cleanQuery($type),	
			"first_name"=> $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['first_name']))),		
			"middle_name"=> $this->CommonMethodsModel->cleanQuery($middle_name),	
			"last_name"=> $this->CommonMethodsModel->cleanQuery(ucwords(strtolower($_POST['last_name']))),	
			"email_address" => $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email'])),		
			"password" => md5($_POST['passwordsignup']),	
			"tax" => 1,			
			"mail_subscribe" => 0,   
			"active" => $this->CommonMethodsModel->cleanQuery($active),		
			"modified" => $this->CommonMethodsModel->cleanQuery(date('Y-m-d H:i:s'))	
			);		
			$userid = $this->CommonMethodsModel->addUsersUser($user);
			$this->sessionObj->userid = $userid;		
			//$userdetail = $this->CommonMethodsModel->getUser($userid);
			//$first_name = strtoupper($userdetail[first_name]);	
			//echo "Your account has been successfully created.@SPLIT@WELCOME $first_name";	
			if($_POST['from']=="addFav"){		
			$type = $this->generalvar['listing']['listing']['typeid'];		
			$cfx = $this->search_mod->checkFavoriteExists($_POST['listingID'],$userid,$type);	
			$listing = $this->search_mod->getListing($_POST['listingID']);		
			if(count($cfx)>0){				
			echo $listing['name']." already exist in your My Favorites@SPLIT@".$userid; 
			}else {				
			$fields = array("module"=>$_POST['listingID'], "type"=> $type, "user" => $userid, "date" => 0);	
			$this->search_mod->addFavorites($fields);		
			echo $listing['name']." has been added to My Favorites@SPLIT@".$userid;		
			}			
			}else if($_POST['from']=="addTrip"){
				echo "addToTrip@SPLIT@".$userid;	
				}		
				exit;	
				}	
				}
		public function resetpasswordAction() {	
				$this->_helper->layout->disableLayout();
				$this->_helper->viewRenderer->setNoRender(true);	
				$email = $this->CommonMethodsModel->cleanQuery(strtolower($_POST['email']));
				$customer = $this->CommonMethodsModel->getUserByEmail($email);	
				$customerid = $customer['ID'];	
				if($customerid){		
				$token = md5(uniqid(rand(),1));	
				$un = substr($token, 0, 3);		
				$newpassword = "visitalton".$un;	
				$newpwd = md5($newpassword);	
				$this->CommonMethodsModel->changeUserPassword($customerid,$newpwd);	
				$fields = array(
				'first_name' => $customer['first_name'],
				'last_name' => $customer['last_name'],
				'newpwd' => $newpassword);	
				$to = $email;		
				$from = $this->generalvar['notifyun'];	
				$subject = $this->generalvar['clientname']." - Password Reset";	
				$this->_helper->Mails->sendResetPasswordMail($to,$from,$subject,$fields);	
				$msg = "A link to reset password will be emailed to you shortly.";	
				echo $msg;	
				}else{	
				$msg = "Please enter a valid email ID";	
				echo $msg;	
				}
				}
		public function signinAction(){	
		$this->_helper->layout->disableLayout();    
		$this->_helper->viewRenderer->setNoRender(true);   
 		if($_POST){ 		
		$email = strtolower($_POST['email']);	
		$password = md5($this->CommonMethodsModel->cleanQuery($_POST['password']));	
		$user = $this->CommonMethodsModel->checkuserexists($email,$password);	
		if($user['ID']!=''){	
		$this->sessionObj->userid = $user['ID'];	
		$user = $this->CommonMethodsModel->getUser($user['ID']);
		$this->sessionObj->user = $user;		
		$first_name = strtoupper($user[first_name]);			
		if($user['image'])		
			$img = $user['image'];		
		else			
			$img = 'default_user.png';    	
		echo 'loginsuccess@@@@<img class="hidden-xs origin round" width="40" height="40" src="'. $this->view->baseUrl().'/temp/'.$img.'" alt="'.$user['first_name'].'" /> Hi, '.$user['first_name'];		
		exit;	
		}		
		else {	
		echo "loginfailed@@@@";		
		exit;		
		}		
		}	
		}	
		public function signinlightboxAction(){	
		$this->_helper->layout->disableLayout();
		$this->view->from = $_GET['from'];	
		$this->view->listingID = $_GET['listingID'];	
		$this->view->eventID = $_GET['eventID'];
		$this->view->eventDateID = $_GET['eventDateID'];
		$this->view->commentID = $_GET['commentID'];	
		$this->view->productID = $_GET['productID'];	
		$this->view->url = $_GET['url'];	
		if($_POST){		
		$email = strtolower($_POST['signin_lightbox_email_address']);
		$password = md5($this->CommonMethodsModel->cleanQuery($_POST['signin_lightbox_password']));	
		$user = $this->CommonMethodsModel->checkuserexists($email,$password);	
		if($user['ID']!=''){	
		$this->sessionObj->userid = $user['ID'];		
		$user = $this->CommonMethodsModel->getUser($user['ID']);			
		$login_status = "loginsuccess";	
		//echo "loginsuccess";			
		$cartitems = $this->CommonMethodsModel->getCartBySessionId();		
		if(count($cartitems)>0){				
		echo "loginsuccesstocheckout@SPLIT@";			
		}			
		if($_POST['from']=="addFav") {				
		$type = $this->generalvar['listing']['listing']['typeid'];		
		$cfx = $this->search_mod->checkFavoriteExists($_POST['listingID'],$user['ID'],$type);			
		$listing = $this->search_mod->getListing($_POST['listingID']);				
		if(count($cfx)>0){	
		echo $listing['name']." already exist in your My Favorites@SPLIT@".$user['ID'];			
		} else {				
		if($_POST['listingID'])   
			$moduleid = $_POST['listingID'];	
		elseif($_POST['eventID'])    			
		$moduleid = $_POST['eventID'];			
		if($_POST['eventDateID'])				
			$eventDateID = $_POST['eventDateID'];
		else							
			$eventDateID = 0;			
		$fields = array("module"=>$_POST['listingID'], "type"=> $type, "user" => $user['ID'], "date" => $eventDateID);	
		$this->search_mod->addFavorites($fields);			
		echo $listing['name']." has been added to My Favorites@SPLIT@".$user['ID'];		
		}			
		}			
		else if($_POST['from']=="addEventToFav"){	
		$type = $this->generalvar['event']['event']['typeid'];	
		$eventID = $_POST['eventID'];					
		$eventDateID = $_POST['eventDateID'];			
		$cfx = $this->search_mod->checkFavoriteExists($_POST['eventID'],$user['ID'],$type,$eventDateID);
		$event = $this->events_mod->getEventByDateID($eventID,$eventDateID,$type);			
		if(count($cfx)>0){						
		echo $event['title']." already exist in your My Favorites@SPLIT@".$user['ID'];				
		} else {				
		$eventID = $_POST['eventID'];	
		if($_POST['eventDateID'])		
			$eventDateID = $_POST['eventDateID'];		
		else							
			$eventDateID = 0;			
		$fields = array("module"=>$eventID, "type"=> $type, "user" => $user['ID'], "date" => $eventDateID);	
		$this->search_mod->addFavorites($fields);				
		echo $event['title']." has been added to My Favorites@SPLIT@".$user['ID'];						}					}					else if($_POST['from']=="addTrip"){						echo "addToTrip@SPLIT@".$user['ID'];					} else if($_POST['from']=="addEventToTrip"){						echo "addEventToTrip@SPLIT@".$user['ID'];    					} else if($_POST['from']=="addToWishlist"){						echo "addToWishlist@SPLIT@";					} else if($_POST['from']=="addevent"){						echo "addevent@SPLIT@".$user['ID'];					} else if($_POST['from']=="profile") {						echo "profile@SPLIT@";					}else if($_POST['from']=="blogscomment"){						echo "blogscomment@SPLIT@";    					}else if($_POST['from']=="articlescomment"){						echo "articlescomment@SPLIT@";    					}else if($_POST['from']=="storycomment"){						echo "storycomment@SPLIT@";    					}else if($login_status!="loginsuccess"){						echo "loginfailed@SPLIT@";					}				$this->_helper->Common->loginBannerEventsAndTrips();			} else {				echo "loginfailed@SPLIT@";			}		}	}}?>