<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Validator\File\Size;
use Application\Model\Currencyrate;
use Zend\Mvc\MvcEvent;

class SeekerController extends AbstractActionController
{
    var $sessionObj;
    var $TripPlugin;
    var $CommonPlugin;
    var $workCategory;
	var $stripeConfig;

    public function __construct($data = false)
    {
        /*echo '<pre>';print_r($_SERVER);exit;*/
        /*Loading models from factory         * Call the model object using it's class name         */
        if ($data['models'] && is_array($data['models'])) {
            foreach ($data['models'] as $model) {
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->generalvar = $data['configs']['siteConfigs'];
		$this->stripeConfig = $data['configs']['stripe_keys'];
        $this->workCategory = array(
            '1' => 'Obtain, Travel & Deliver',
            '2' => 'Arrange, Assist & Accomplish',
            '3' => 'Personal Services',
            '4' => 'Local Travel Services',
            '5' => 'Auto & Device Services',
            '6' => 'Home & Surroundings',
            '7' => 'Entertainment & Leisure'
        );
        $this->sessionObj = new Container('comSessObj');
    }

    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        if (isset($this->sessionObj->userid) && $this->sessionObj->userid != '') {
            $isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
            if (!$isUserLoggedIn) {
                $this->redirect()->toRoute('logout');
            }
        }
        return parent::onDispatch($e);
    }

    public function indexAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->sessionObj->userrole = 'seeker';
        $viewArray = array();
        $viewArray = array(
            'countries' => $this->CommonMethodsModel->getCountries(),
            'currency' => $this->CommonMethodsModel->getCurrencies()
        );
        $viewArray['current_upcoming_trips'] = $this->TripPlugin->getTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid);
       // $viewArray['upcoming_trips'] = $this->TripPlugin->getTrips('upcoming',$this->sessionObj->userrole,$this->sessionObj->userid);
        $viewArray['completed_trips'] = $this->TripPlugin->getTrips('completed', $this->sessionObj->userrole, $this->sessionObj->userid,true);
        $viewArray['cancelled_trips'] = $this->TripPlugin->getTrips('cancelled', $this->sessionObj->userrole, $this->sessionObj->userid,true);
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
		$calEvent = array();
		if(is_array($viewArray['current_upcoming_trips'])){
			foreach($viewArray['current_upcoming_trips'] as $cuTrip){
				$cuTrip['departure_date'] = explode(' ',$cuTrip['departure_date']);
				$cuTrip['arrival_date'] = explode(' ',$cuTrip['arrival_date']);
				$date_from = strtotime($cuTrip['departure_date'][0]);
				$date_to = strtotime($cuTrip['arrival_date'][0]);
				for($i=$date_from; $i<=$date_to; $i+=86400) {  
					$newDepDate = date("Y-m-d", $i);
					@$calEvent[] = array(
					'eventName'=> $cuTrip['origin_location_city'].' ('.$cuTrip['origin_location_code'].') - '.$cuTrip['destination_location_city'].' ('.$cuTrip['destination_location_code'].')',
					'calendar'=>$cuTrip['service'],
					'color'=>($cuTrip['service'] == 'People'?'pink':($cuTrip['service'] == 'Package'?'skyblue':'blue')),
					'date'=>date('d',strtotime($newDepDate))."/".date('m',strtotime($newDepDate))."/".date('Y',strtotime($newDepDate))
					);
				
					

				}
			}
		}
		$viewArray['cal_events'] = $calEvent;

		
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Seeker',
                'title' => 'Seeker',
                'active' => false,
                'redirect' => '/seeker'
            ),
            array(
                'label' => 'My Travel Needs',
                'title' => 'My Travel Needs',
                'active' => true,
                'redirect' => false //'/seeker'
            )
        );
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function checkSeekerPeopleServiceNeedAction()
    {
        //print_r($_POST);die;
        $traveller_trip_id = $this->CommonMethodsModel->cleanQuery($_POST['traveller_trip_id']);
        $tripid = $this->CommonMethodsModel->cleanQuery($_POST['tripid']);
        $resp = $this->TripModel->checkSeekerPeopleNeed($traveller_trip_id, $tripid);
        if (count($resp) > 0) {
            echo $resp['approved'];
            exit();
        } else {
            echo 'nothing';
            exit();
        }
    }

    public function checkSeekerPackageServiceNeedAction()
    {

        $traveller_trip_id = $this->CommonMethodsModel->cleanQuery($_POST['traveller_trip_id']);
        $tripid = $this->CommonMethodsModel->cleanQuery($_POST['tripid']);
        $resp = $this->TripModel->checkSeekerPackageNeed($traveller_trip_id, $tripid);
        if (count($resp) > 0) {
            echo $resp['approved'];
            exit();
        } else {
            echo 'nothing';
            exit();
        }
    }

    public function checkSeekerProjectServiceNeedAction()
    {

        $traveller_trip_id = $this->CommonMethodsModel->cleanQuery($_POST['traveller_trip_id']);
        $tripid = $this->CommonMethodsModel->cleanQuery($_POST['tripid']);
        $resp = $this->TripModel->checkSeekerProjectNeed($traveller_trip_id, $tripid);
        if (count($resp) > 0) {
            echo $resp['approved'];
            exit();
        } else {
            echo 'nothing';
            exit();
        }
    }

    public function checkdocumentstatusAction()
    {
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $this->TripPlugin = $this->plugin('TripPlugin');
        $user_verify_docs = $this->TripModel->checkVerificationDocument($this->sessionObj->userid);
        $documentThere = $approvedDocs = false;
        if (!empty($user_verify_docs)) {
            $documentThere = true;
            foreach ($user_verify_docs as $verifydoc_row) {
                if ($verifydoc_row['approved'] == 1) {
                    $approvedDocs = true;
                }
            }
        }
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewArray['documentThere'] = $documentThere;
        $viewArray['approvedDocs'] = $approvedDocs;
        $viewArray['idTypes'] = $this->ProfileModel->getIdentityTypes($this->sessionObj->offsetGet('userid'));
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)->setTerminal(true);
        return $viewModel;
    }

    public function detailAction()
    {
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $this->TripPlugin = $this->plugin('TripPlugin');
        /* $ID = $this->_helper->Common->decrypt($this->params('ID')); */
        $ID = $this->params('ID');
        $trip = $this->TripModel->getSeekerTrip($ID);
		$_GET['service'] = $_GET['service'] == 'Project' ? 'Product' : $_GET['service'];
		
        $service = isset($_GET['service']) ? $_GET['service'] : '';
        $traveller_people_request_id = isset($_GET['traveller_people_request_id']) ? $_GET['traveller_people_request_id'] : '';
        $traveller_package_request_id = isset($_GET['traveller_package_request_id']) ? $_GET['traveller_package_request_id'] : '';
        $traveller_project_request_id = isset($_GET['traveller_project_request_id']) ? $_GET['traveller_project_request_id'] : '';
        $viewArray['tripid'] = $ID;
        $viewArray['trip'] = $trip;



		$viewArray['traveller_earnings'] = $trip['total_cost'] - (($trip['total_cost']/10)*2);
		
        $viewArray['traveller_people_request_id'] = $traveller_people_request_id;
        $viewArray['traveller_package_request_id'] = $traveller_package_request_id;
        $viewArray['traveller_project_request_id'] = $traveller_project_request_id;
        $viewArray['trip_user'] = $this->CommonMethodsModel->getUser($trip['user']);
		
		$viewArray['trip_user']['languages'] = array();
		$lngIds = explode(',',$viewArray['trip_user']['language']);
		foreach($lngIds as $lngId){
			$viewArray['trip_user']['languages'][] = $this->CommonMethodsModel->getLanguagesName($lngId);
		}
		$viewArray['trip_user']['language'] = implode(',',$viewArray['trip_user']['languages']);
		
        $viewArray['userTrustVerification'] = $this->CommonMethodsModel->getUserTrustVerifyTypes($trip['user']);
        $dob = $viewArray['trip_user']['dob'];
        $viewArray['looking_for'] = 'seeker';
        $todaydate = strtotime(date('Y-m-d'));
        $viewArray['age'] = $this->CommonPlugin->calculateAge($dob, $todaydate);
        $viewArray['language'] = $this->CommonMethodsModel->getLanguagesName($viewArray['trip_user']['language']);
        $primary = 'yes';
        $viewArray['user_location'] = $this->CommonMethodsModel->getUserLocationByAddrId($trip['user_location'], $trip['user'], $primary);
        $trip_seeker = $this->TripPlugin->getTripDetails($ID, 'seeker', false, $service);
        $user_verify_docs = $this->TripModel->checkVerificationDocument($this->sessionObj->userid);
        $documentThere = $approvedDocs = false;
        if (!empty($user_verify_docs)) {
            $documentThere = true;
            foreach ($user_verify_docs as $verifydoc_row) {
                if ($verifydoc_row['approved'] == 1) {
                    $approvedDocs = true;
                }
            }
        }
        $user_cards_details = $this->TripModel->getcarddetails($this->sessionObj->userid);
        $trip_seeker_packages = $this->TripModel->getSeekerTripPackage($ID);
        $viewArray['trip_seeker_packages'] = $trip_seeker_packages;
        $trip_seeker_projects = $this->TripModel->getSeekerTripProject($ID);
        $viewArray['trip_seeker_projects'] = $trip_seeker_projects;
        $project_tasks = isset($viewArray['trip_seeker_projects']['task']) ? $viewArray['trip_seeker_projects']['task'] : 0;
        $project_tasks = explode(',', $project_tasks);
        if (count($project_tasks) > 0) {
            foreach ($project_tasks as $project_task) {
                $task = $this->TripModel->getTripProjectTask($project_task);
                $arr_tasks[] = $task['name'];
            }
        } else {
            $task = $this->TripModel->getTripProjectTask($project_task);
            $arr_tasks[] = $task['name'];
        }
        $viewArray['trip_travaller_projects_tasks'] = $arr_tasks;
        $viewArray['projectcat'] = $this->TripModel->getProjectTasksCategoryType();
        /*$viewArray['projectcat'] =$this->CommonMethodsModel->Listprojectcategory();*/
        $viewArray['project_tasks_categories'] = $this->TripModel->getProjectTasksCategoryType($project_task);
		$viewArray['reviews'] = $this->TripModel->getTripreviewall($this->sessionObj->userid,true);
		$viewArray['avg_rating'] = 0;
		if($viewArray['reviews']){
			foreach($viewArray['reviews'] as $rKey=>$rev){
				$flightdetails = $this->TripModel->getFlightsDetails($rev['trip_id']);
				$viewArray['reviews'][$rKey]['airport_details'] = array('orgin_city'=>'','destination_city'=>'');
				if(is_array($flightdetails) && count($flightdetails) > 0){
					$viewArray['reviews'][$rKey]['airport_details'] = $flightdetails[0];
				}
				$viewArray['avg_rating'] += $viewArray['reviews'][$rKey]['rating'];
			}
			$viewArray['avg_rating'] = number_format($viewArray['avg_rating'] / count($viewArray['reviews']),0);
		}
        $traveller_upcoming_trips = $this->TripPlugin->getTrips('upcoming', 'traveller', $this->sessionObj->userid);
        $viewArray['traveller_upcoming_trips'] = $traveller_upcoming_trips;
        if (count($traveller_upcoming_trips) > 0) {
            foreach ($traveller_upcoming_trips as $traveller_trip) {
                if ($traveller_trip['trip_people']['ID']) {
                    if (($traveller_trip['origin_location_country'] == $trip_seeker['origin_location_country']) && ($traveller_trip['destination_location_country'] == $trip_seeker['destination_location_country']))
                        $viewArray['traveller_exiting_people_trip'] = 'yes';
                }
                if ($traveller_trip['trip_package']['ID']) {
                    if (($traveller_trip['origin_location_country'] == $trip_seeker['origin_location_country']) && ($traveller_trip['destination_location_country'] == $trip_seeker['destination_location_country']))
                        $viewArray['traveller_exiting_package_trip'] = 'yes';
                }
                if ($traveller_trip['trip_project']['ID']) {
                    if (($traveller_trip['origin_location_country'] == $trip_seeker['origin_location_country']) && ($traveller_trip['destination_location_country'] == $trip_seeker['destination_location_country']))
                        $viewArray['traveller_exiting_project_trip'] = 'yes';
                }
            }
        }
		
		if(isset($_REQUEST['from']) && $_REQUEST['from'] == 'inquired'){
			$this->TripModel->inactiveTripInquired($_REQUEST['tID'],$_REQUEST['sID'],'traveller');
		}
		
		$viewArray['reqInfo'] = array();
		if(isset($_REQUEST['seeker_req_id']) && $_REQUEST['seeker_req_id'] != ''){
			$viewArray['reqInfo'] = $this->TripModel->getSeekerRequestInfo($_REQUEST['seeker_req_id'],strtolower($_REQUEST['service']));
		} else if(isset($_REQUEST['traveller_req_id']) && $_REQUEST['traveller_req_id'] != ''){
			$viewArray['reqInfo'] = $this->TripModel->getTravellerRequestInfo($_REQUEST['traveller_req_id'],strtolower($_REQUEST['service']));
		}
		
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Seeker',
                'title' => 'Seeker',
                'active' => false,
                'redirect' => false
            ),
            array(
                'label' => 'My Travel Plans',
                'title' => 'My Travel Plans',
                'active' => true,
                'redirect' => '/seeker'
            )
        );
        $estimatedEarnings = 0;
        if (isset($trip_seeker['trip_people']['people_service_fee'])) {
            $estimatedEarnings = $trip_seeker['trip_people']['people_service_fee'];
            $adminProfit = round((20 / 100) * $estimatedEarnings);
            $estimatedEarnings = $estimatedEarnings - $adminProfit;
        } elseif (isset($trip_seeker_packages['package_service_fee'])) {
            $estimatedEarnings = $trip_seeker_packages['package_service_fee'];
            $adminProfit = round((20 / 100) * $estimatedEarnings);
            $estimatedEarnings = $estimatedEarnings - $adminProfit;
        } else if (isset($trip_seeker_projects['project_service_fee'])) {
            $estimatedEarnings = $trip_seeker_projects['project_service_fee'];
            $adminProfit = round((20 / 100) * $estimatedEarnings);
            $estimatedEarnings = $estimatedEarnings - $adminProfit;
        }
        //$estimatedEarnings = $this->CommonMethodsModel->convertCurrency($estimatedEarnings, $this->CommonMethodsModel->getSiteCurrency(), $this->CommonMethodsModel->getUserCurrency($this->sessionObj->currency));
        $viewArray['completed_ref_req'] = $this->ProfileModel->getUserReferences($trip['user']);
        $viewArray['comSessObj'] = $this->sessionObj;
		$viewArray['stripeConfig'] = $this->stripeConfig;
        $viewArray['trip'] = $trip_seeker;
        //echo "<pre>";print_r($trip_seeker);exit();

        $viewArray['trip_id'] = $ID;
        $viewArray['estimatedEarnings'] = $estimatedEarnings;
        $viewArray['documentThere'] = $documentThere;
        $viewArray['approvedDocs'] = $approvedDocs;
        $viewArray['workCategory'] = $this->workCategory;
        $viewArray['user_cards_details'] = $user_cards_details;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function receiptAction()
    {
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $this->TripPlugin = $this->plugin('TripPlugin');
        $ID = $this->decrypt($this->params('ID'));
		$service = isset($_GET['service']) ? $_GET['service'] : '';

        //echo $ID;die;
        $trip_pay_row = $this->TripModel->getSeekerTripPaymentById($ID);

		$tmpTripId = $trip_pay_row['pay_trip_id'];
		//$tmpTripId = $ID;
		
        $trip = $this->TripPlugin->getTripDetails($tmpTripId, 'seeker', false, $service);
        $viewArray['trip'] = $trip;

        $seeker_trips = $this->TripModel->getTrip($tmpTripId, 'seeker');

        

        $travellerID =  $this->TripModel->getReceiptTravellerTripId($seeker_trips['traveller_trip_id']);
        
        $traveller_trip = $this->TripPlugin->getTripDetails($travellerID['ID'], 'traveller', false, $service);

        $viewArray['traveller_trip'] = $traveller_trip;

        //echo '<pre>';print_r($traveller_trip);exit();  

        $viewArray['trip_user'] = $this->CommonMethodsModel->getUser($travellerID['user']);
		
        $dob = $viewArray['trip_user']['dob'];
        $todaydate = strtotime(date('Y-m-d'));
        $viewArray['age'] = $this->CommonPlugin->calculateAge($dob, $todaydate);

        $viewArray['language'] = $this->CommonMethodsModel->getLanguagesName($viewArray['trip_user']['language']);
        $primary = 'yes';
        $viewArray['user_location'] = $this->CommonMethodsModel->getUserLocationByAddrId(false, $trip['user'], $primary);
        $pay_trip_id = $tmpTripId;
        if ($trip_pay_row['pay_trip_table'] == 'trips') {
            $pay_trip_id = $trip_pay_row['pay_to_trip_id'];
        }
        $trip_traveller = $this->TripPlugin->getTripDetails($pay_trip_id, 'traveller', false, $service);
        $user_verify_docs = $this->TripModel->checkVerificationDocument($this->sessionObj->userid);
        $documentThere = $approvedDocs = false;
        if (!empty($user_verify_docs)) {
            $documentThere = true;
            foreach ($user_verify_docs as $verifydoc_row) {
                if ($verifydoc_row['approved'] == 1) {
                    $approvedDocs = true;
                }
            }
        }
        $user_cards_details = $this->TripModel->getcarddetails($this->sessionObj->userid);
        $viewArray['trip_seeker_packages'] = $this->TripModel->getSeekerTripPackage($tmpTripId);
        $trip_seeker_projects = $this->TripModel->getSeekerTripProject($tmpTripId);
        $viewArray['trip_seeker_projects'] = $trip_seeker_projects;
        $project_tasks = isset($viewArray['trip_seeker_projects']['task']) ? $viewArray['trip_seeker_projects']['task'] : 0;
        $project_tasks = explode(',', $project_tasks);
        if (count($project_tasks) > 0) {
            foreach ($project_tasks as $project_task) {
                $task = $this->TripModel->getTripProjectTask($project_task);
                $arr_tasks[] = $task['name'];
            }
        } else {
            $task = $this->TripModel->getTripProjectTask($project_task);
            $arr_tasks[] = $task['name'];
        }
        $viewArray['trip_travaller_projects_tasks'] = $arr_tasks;
        $viewArray['projectcat'] = $this->TripModel->getProjectTasksCategoryType();
        $viewArray['project_tasks_categories'] = $this->TripModel->getProjectTasksCategoryType($project_task);
        $traveller_upcoming_trips = $this->TripPlugin->getTrips('upcoming', 'traveller', $this->sessionObj->userid);
        $viewArray['traveller_upcoming_trips'] = $traveller_upcoming_trips;
        if (count($traveller_upcoming_trips) > 0) {
            foreach ($traveller_upcoming_trips as $traveller_trip) {
                if ($traveller_trip['trip_people']['ID']) {
                    if (($traveller_trip['origin_location'] == $trip['origin_location']) && ($traveller_trip['destination_location'] == $trip['destination_location']))
                        $viewArray['traveller_exiting_people_trip'] = 'yes';
                }
                if ($traveller_trip['trip_package']['ID']) {
                    if (($traveller_trip['origin_location'] == $trip['origin_location']) && ($traveller_trip['destination_location'] == $trip['destination_location']))
                        $viewArray['traveller_exiting_package_trip'] = 'yes';
                }
                if ($traveller_trip['trip_project']['ID']) {
                    if (($traveller_trip['origin_location'] == $trip['origin_location']) && ($traveller_trip['destination_location'] == $trip['destination_location']))
                        $viewArray['traveller_exiting_project_trip'] = 'yes';
                }
            }
        }
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Seeker',
                'title' => 'Seeker',
                'active' => false,
                'redirect' => false
            ),
            array(
                'label' => 'My Travel Plans',
                'title' => 'My Travel Plans',
                'active' => true,
                'redirect' => '/seeker'
            )
        );
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewArray['trip'] = $traveller_trip;
		$viewArray['trip_seeker'] = $trip;
        $viewArray['trip_id'] = $tmpTripId;
        $viewArray['documentThere'] = $documentThere;
        $viewArray['approvedDocs'] = $approvedDocs;
        $viewArray['workCategory'] = $this->workCategory;
        $viewArray['user_cards_details'] = $user_cards_details;
        $viewArray['trip_pay_row'] = $trip_pay_row;
		
		//echo '<pre>';print_r($traveller_trip);die;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function decrypt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        /*pls set your unique hashing key*/
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        /* hash*/
        $key = hash('sha256', $secret_key);
        /* iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning*/
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        /*decrypt the given text/string/number*/
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    public function getallairportsAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }

        $airports = file_get_contents('https://airport.api.aero/airport?user_key=ec3a1d7f6300f765056b72438f96f81a');
        $airports = str_replace('callback({"processingDurationMillis":0,"authorisedAPI":true,"success":true,"airline":null,"errorMessage":null,"airports":', '', $airports);
        $airports = str_replace('})', '', $airports);
        $flights = json_decode($airports);
        foreach ($flights as $flight) {
            if ($flight->name)
                $name = $flight->name;
            else
                $name = '';
            if ($flight->city)
                $city = $flight->city;
            else
                $city = '';
            if ($flight->country)
                $country = $flight->country;
            else
                $country = '';
            if ($flight->code)
                $code = $flight->code;
            else
                $code = '';
            if ($flight->timezone)
                $timezone = $flight->timezone;
            else
                $timezone = '';
            if ($flight->lat)
                $lat = $flight->lat;
            else
                $lat = '';
            if ($flight->lng)
                $lng = $flight->lng;
            else
                $lng = '';
            $arr_flight = array(
                'name' => $name,
                'city' => $city,
                'country' => $country,
                'code' => $code,
                'time_zone' => $timezone,
                'latitude' => $lat,
                'longitude' => $lng
            );
            $this->TripModel->addAirports($arr_flight);
        }
        exit;
    }

    public function addPeopleServiceAction()
    {  
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->sessionObj->userrole = 'seeker';
        /*$flights = file_get_contents('http://partners.api.skyscanner.net/apiservices/browseroutes/v1.0/UK/GBP/en-GB/MAA/NYC/2016-04-25?apiKey=ra846927894945552494219565854033');        $flights = file_get_contents('http://partners.api.skyscanner.net/apiservices/pricing/v1.0/123?apiKey=ra846927894945552494219565854033');        $flights = json_decode($flights);       */
        $this->TripPlugin = $this->plugin('TripPlugin');
        $viewArray = array();
        $viewArray['action'] = 'add';
        if ($_POST){
			$_POST['type_of_service'] = 'people';
            $user_location = $this->TripPlugin->addTripOrginLocation();
            $trip_ID = $this->TripPlugin->addTripDetail($user_location);
            $reqId = $this->TripPlugin->addSeekerPeopleService($trip_ID);
            $image_path = "tickets";
            $field_name = "ticket_image";
            $file_name_prefix = $this->CommonMethodsModel->cleanQuery($_POST['tripIdNumber']) . '_' . time();
			$uploadResult['result']  = 'fail';
			if(isset($_FILES['file'])){
				$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
				if (move_uploaded_file($_FILES['file']['tmp_name'], ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext)) {
					$uploadResult['result'] = 'success';
				}
			}
            if ($uploadResult['result'] == 'success') {
                $ticket_file = $file_name_prefix . '.' . $ext;//$imgResult['fileName'];
                $arr_ticket_detail = array(
                    'ticket_image' => $this->CommonMethodsModel->cleanQuery($ticket_file)
                );
                $this->TripModel->updateSeekerTrip($arr_ticket_detail, $trip_ID);
            }
			$tC = 0;
            if (isset($gotourl) && !empty($gotourl)) {
				
				try
                {
                    $this->sendmail = $this->MailModel->sendPeopleServiceEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], '1');
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }
                $requestURL = $gotourl . '&seeker_people_request_id=' . $this->encrypt($trip_ID);
                $retArr = (array(
                    'result' => 'success',
                    'requestURL' => $requestURL,
                    'msg' => " People service trip listing has been created successfully. Please check status in Menu -> My Travel Needs."
                ));
                
                $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'seeker_trip',
                        'notification_subject' => 'Trip Creatde',
                        'notification_message' => "Thanks for creating people service trip listing.",
                        'app_redirect' => 'notifications',
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                     $this->TripModel->addToNotifications($hugedata1);
                
				$tC = 1;
            } else if ($trip_ID) {
				
				try
                {
                    $this->sendmail = $this->MailModel->sendPeopleServiceEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], '1');
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }
                $retArr = array(
                    'msg' => 'People service trip listing has been created successfully. Please check status in Menu -> My Travel Needs.',
                    'result' => 'success',
                    'trip_id' => $this->encrypt($trip_ID)
                );
                
                
                  $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'seeker_trip',
                        'notification_subject' => 'Trip Creatde',
                        'notification_message' => "Thanks for creating people service trip listing.",
                        'app_redirect' => 'notifications',
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                     $this->TripModel->addToNotifications($hugedata1);
				$tC = 1;
            } else {
                $retArr = array(
                    'msg' => 'People service trip listing has not been created successfully. Please try again.',
                    'result' => 'failure'
                );
            }
			
			if($tC == 1){
				$uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
				$username = $uD['first_name'];
				$hugedata = array('user_id' => $this->sessionObj->userid,
					'message' => "" . $username . " has uploaded ticket for people service trip listing. Please take a decision at the earliest.",
					'create_date' => date("Y-m-d H:i:s")
				);
				$this->TripModel->addToAdminNotifications($hugedata);
			}
			
            echo json_encode($retArr);
            exit;
            //$this->redirect()->toRoute('seeker');
        }
        else
        {
            $tripId = $this->decrypt($this->params('ID'));
            $tripDetails = $this->TripModel->getSeekerTrip($tripId);
            $GOTOT_URL = isset($_GET['g']) ? urldecode($_GET['g']) : '';
            $viewArray['selectedFlightData'] = '';
            $viewArray['gotourl'] = $GOTOT_URL;
            $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
            if ($tripDetails) {
                $trip_seeker_details = $this->TripModel->getSeekerTripPeople($tripId);
                $trip_seeker_passenger_details = $this->TripModel->getSeekerTripPeoplePassengers($trip_seeker_details['ID']);
                if ($trip_seeker_passenger_details) {
                    foreach ($trip_seeker_passenger_details as $key => $passenger) {
                        $pass_att = $this->TripModel->getSeekerTripPeoplePassengersAttachment($passenger['ID']);
                        if ($pass_att) {
                            $trip_seeker_passenger_details[$key]['attachments'] = $pass_att;
                        }
                    }
                }
                $viewArray['airport_details'] = $this->TripModel->getFlightsDetails($tripId);
                $origin_location = $this->TripModel->getAirPort($tripDetails['origin_location']);
                $destination_location = $this->TripModel->getAirPort($tripDetails['destination_location']);
                $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $origin_location_country_code = isset($origin_location['country_code']) ? $origin_location['country_code'] : '';
                $destination_location_country_code = isset($destination_location['country_code']) ? $destination_location['country_code'] : '';
                $viewArray['origin_location_country_code'] = $origin_location_country_code;
                $viewArray['destination_location_country_code'] = $destination_location_country_code;
                $viewArray['origin_location'] = $origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;
                $viewArray['destination_location'] = $destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;
                $viewArray['origin_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';
                $viewArray['destination_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';
                $viewArray['selectedFlightData'] = !empty($viewArray['airport_details']) ? htmlspecialchars(json_encode($viewArray['airport_details'])) : '';
                $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid, 'no', $tripDetails['user_location']);
                $viewArray['action'] = 'edit';
                $viewArray['disabledProp'] = ' readonly="readonly"';
                $editAddOptions = '<option value="' . $origin_location_country_code . '">' . $origin_location_country . '</option>';
                if ($origin_location_country_code != $destination_location_country_code)
                    $editAddOptions .= '<option value="' . $destination_location_country_code . '">' . $destination_location_country . '</option>';
                $editAddOptions .= '<option value="more" class="more_sel_text">More</option>';
                $viewArray['editAddOptions'] = $editAddOptions;
            }
            $trip = isset($tripDetails) ? $tripDetails : false;
            $trip_seeker = isset($trip_seeker_details) ? $trip_seeker_details : false;
            $trip_seeker_passenger = isset($trip_seeker_passenger_details) ? $trip_seeker_passenger_details : false;
            $viewArray['trip'] = $trip;
            $viewArray['trip_seeker'] = $trip_seeker;
            $viewArray['trip_seeker_passenger'] = $trip_seeker_passenger;
            $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
            $viewArray['currency'] = $this->CommonMethodsModel->getCurrency();
            $viewArray['travel_plans'] = $this->TripModel->getTravelTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid, false);
        }
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Seeker',
                'title' => 'Seeker',
                'active' => false,
                'redirect' => '/seeker'
            ),
            array(
                'label' => 'Add Travel Needs',
                'title' => 'Add Travel Needs',
                'active' => false,
                'redirect' => '/seeker/add-people-service'
            ),
            array(
                'label' => 'People Service',
                'title' => 'People Service',
                'active' => true,
                'redirect' => false //'/seeker/add-people-service'
            )
        );
        /*echo '<pre>';
        print_r($viewArray);
        exit;*/
        $viewArray['flashMessages'] = $this->flashMessenger()->getMessages();
        $viewArray['comSessObj'] = $this->sessionObj;
        $this->layout()->pageController = 'seeker';
        $this->layout()->pageFunction = 'addPeopleService';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function addPackageServiceAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->sessionObj->userrole = 'seeker';
        $this->TripPlugin = $this->plugin('TripPlugin');
        $viewArray = array();
        $viewArray['action'] = 'add';
        if ($_POST) {
			$_POST['type_of_service'] = 'package';
            extract($_POST);
            $user_location = $this->TripPlugin->addTripOrginLocation();
            $trip_ID = $this->TripPlugin->addTripDetail($user_location);
            $this->TripPlugin->addSeekerPackageService($trip_ID);
            if (isset($gotourl) && !empty($gotourl)) {
                $requestURL = $gotourl . '?seeker_package_request_id=' . $this->encrypt($trip_ID);
                return $this->redirect()->toRoute('traveller-details-param', array(
                    'controller' => 'application',
                    'action' => 'detail',
                    'ID' => $gotourl,
                    'seeker_package_request_id' => 'seeker_package_request_id=' . $this->encrypt($trip_ID) . '&service=Package'
                ));
            }
            
              $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'seeker_trip',
                        'notification_subject' => 'Trip Creatde',
                        'notification_message' => "Thanks for creating package service trip listing.",
                        'app_redirect' => 'notifications',
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                     $this->TripModel->addToNotifications($hugedata1);
					 
				try
                {
                    $this->sendmail = $this->MailModel->sendPackageServiceEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], '1');
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }
            return $this->redirect()->toRoute('seacrh-details-param', array(
                'controller' => 'application',
                'action' => 'search',
                'trip' => 'trip=' . $this->encrypt($trip_ID) . '&service=package'
            ));
            /* echo "The package service has been saved successfully";exit;*/
        }
        else {
            $tripId = $this->decrypt($this->params('ID'));
            $tripDetails = $this->TripModel->getSeekerTrip($tripId);
            $GOTOT_URL = isset($_GET['g']) ? urldecode($_GET['g']) : '';
            $viewArray['selectedFlightData'] = '';
            $viewArray['gotourl'] = $GOTOT_URL;
            $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
            if ($tripDetails) {
                $trip_seeker_details = $this->TripModel->getSeekerTripPackage($tripId);
                $trip_seeker_packages_details = $this->TripModel->getSeekerTripPackagePackages($trip_seeker_details['ID']);
                if ($trip_seeker_packages_details) {
                    foreach ($trip_seeker_packages_details as $key => $package) {
                        $trip_seeker_packages_details[$key]['subCategory'] = $this->TripModel->getPackageTasksCategorylisttype($package['item_category']);
                    }
                }
                $origin_location = $this->TripModel->getAirPort($tripDetails['origin_location']);
                $destination_location = $this->TripModel->getAirPort($tripDetails['destination_location']);
                $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $origin_location_country_code = isset($origin_location['country_code']) ? $origin_location['country_code'] : '';
                $destination_location_country_code = isset($destination_location['country_code']) ? $destination_location['country_code'] : '';
                $viewArray['origin_location_country_code'] = $origin_location_country_code;
                $viewArray['destination_location_country_code'] = $destination_location_country_code;
                $viewArray['origin_location'] = $origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;
                $viewArray['destination_location'] = $destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;
                $viewArray['origin_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';
                $viewArray['destination_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';
                $viewArray['selectedFlightData'] = !empty($viewArray['airport_details']) ? htmlspecialchars(json_encode($viewArray['airport_details'])) : '';
                $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid, 'no', $tripDetails['user_location']);
                $viewArray['trip'] = $tripDetails;
                $viewArray['trip_seeker_packages'] = $trip_seeker_packages_details;
                $viewArray['trip_seeker'] = $trip_seeker_details;
                $viewArray['action'] = 'edit';
                $viewArray['disabledProp'] = ' readonly="readonly"';
                $editAddOptions = '<option value="' . $origin_location_country_code . '">' . $origin_location_country . '</option>';
                if ($origin_location_country_code != $destination_location_country_code)
                    $editAddOptions .= '<option value="' . $destination_location_country_code . '">' . $destination_location_country . '</option>';
                $editAddOptions .= '<option value="more" class="more_sel_text">More</option>';
                $viewArray['editAddOptions'] = $editAddOptions;
            }
            $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
            $viewArray['currency'] = $this->CommonMethodsModel->getCurrency();
            $viewArray['project_cats'] = $this->TripModel->getPackageTasksCategory();
            /*echo '<pre>';print_r($trip_seeker_packages_details);exit;*/
            $viewArray['travel_plans'] = $this->TripPlugin->getTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid);
            $viewArray['comSessObj'] = $this->sessionObj;
        }
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Seeker',
                'title' => 'Seeker',
                'active' => false,
                'redirect' => '/seeker'
            ),
            array(
                'label' => 'Add Package Service',
                'title' => 'Add Package Service',
                'active' => false,
                'redirect' => '/seeker/add-package-service'
            ),
            array(
                'label' => 'Package Service',
                'title' => 'Package Service',
                'active' => true,
                'redirect' => false //'/seeker/add-package-service'
            )
        );
        $this->layout()->pageController = 'seeker';
        $this->layout()->pageFunction = 'addPackageService';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function encrypt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        /*pls set your unique hashing key*/
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        /* hash*/
        $key = hash('sha256', $secret_key);
        /* iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning*/
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        /*do the encyption given text/string/number*/
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }

    public function addProjectServiceAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $this->sessionObj->userrole = 'seeker';
        $viewArray = array();
        if ($_POST) {
			$_POST['type_of_service'] = 'project';
            extract($_POST);
            $user_location = $this->TripPlugin->addTripOrginLocation();
            $trip_ID = $this->TripPlugin->addTripDetail($user_location);
            $this->TripPlugin->addSeekerProjectService($trip_ID);
            if (isset($gotourl) && !empty($gotourl)) {
				
				try
                {
                    $this->sendmail = $this->MailModel->sendProductServiceEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], '1');
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }
                $requestURL = $gotourl . '&seeker_project_request_id=' . $this->encrypt($trip_ID);
				
				try
                {
                    $this->sendmail = $this->MailModel->sendProductServiceEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], '1');
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }
                $retArr = (array(
                    'result' => 'success',
                    'requestURL' => $requestURL,
                    'msg' => " Product service trip listing has been created successfully. Please check status in Menu -> My Travel Needs."
                ));
                
                 $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'seeker_trip',
                        'notification_subject' => 'Trip Creation',
                        'notification_message' => "Thanks for creating Product service trip listing.",
                        'app_redirect' => 'notifications',
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                 $this->TripModel->addToNotifications($hugedata1);
            } else {
				try
                {
                    $this->sendmail = $this->MailModel->sendProductServiceEmail($_POST['first_name'], $_POST['last_name'], $_POST['email'], '1');
                }
                catch (\Exception $e)
                {
                    error_log($e);
                }
                $retArr = array(
                    'msg' => 'Product service trip listing has been created successfully. Please check status in Menu -> My Travel Needs.',
                    'result' => 'success',
                    'trip_id' => $this->encrypt($trip_ID)
                );
                $hugedata1 = array
                    (
                        'user_id' => $this->sessionObj->userid,
                        'notification_type' => 'seeker_trip',
                        'notification_subject' => 'Trip Creation',
                        'notification_message' => "Thanks for creating Product service trip listing.",
                        'app_redirect' => 'notifications',
                        'notification_added' => date("Y-m-d H:i:s")
                    );
                 $this->TripModel->addToNotifications($hugedata1);
            }
            echo json_encode($retArr);
            exit;
        } else {
            $viewArray['action'] = 'add';
            $GOTOT_URL = isset($_GET['g']) ? urldecode($_GET['g']) : '';
            $viewArray['selectedFlightData'] = '';
            $viewArray['gotourl'] = $GOTOT_URL;
            $viewArray['project_tasks_categories_type'] = $this->TripModel->getProjectTasksCategorytype();
            $tripId = $this->decrypt($this->params('ID'));
            $tripDetails = $this->TripModel->getSeekerTrip($tripId);
            $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
            if ($tripDetails) {
                $viewArray['action'] = 'edit';
                $trip_seeker_details = $this->TripModel->getSeekerTripProject($tripId);
                $trip_seeker_project_details = $this->TripModel->getSeekerTripProjectTasks($trip_seeker_details['ID']);
                $tripWorkCategory = ($trip_seeker_details['work_category'] == 1) ? '1' : '0';
                $viewArray['project_tasks_categories_type'] = $this->TripModel->getProjectTasksCategorytype($tripWorkCategory);
                if ($trip_seeker_project_details) {
                    foreach ($trip_seeker_project_details as $key => $project) {
                        $trip_seeker_project_details[$key]['subCategory'] = $this->TripModel->getProjectTasksCategorylisttype($project['category']);
                        $pro_att = $this->TripModel->getSeekerTripProjectTaskAttachment($project['project_task_id']);
                        if ($pro_att) {
                            $trip_seeker_project_details[$key]['attachments'] = $pro_att;
                        }
                    }
                }
                $origin_location = $this->TripModel->getAirPort($tripDetails['origin_location']);
                $destination_location = $this->TripModel->getAirPort($tripDetails['destination_location']);
                $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $origin_location_country_code = isset($origin_location['country_code']) ? $origin_location['country_code'] : '';
                $destination_location_country_code = isset($destination_location['country_code']) ? $destination_location['country_code'] : '';
                $viewArray['origin_location_country_code'] = $origin_location_country_code;
                $viewArray['destination_location_country_code'] = $destination_location_country_code;
                $viewArray['origin_location'] = $origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;
                $viewArray['destination_location'] = $destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;
                $viewArray['origin_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';
                $viewArray['destination_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';
                $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid, 'no', $tripDetails['user_location']);
                $viewArray['action'] = 'edit';
                $viewArray['disabledProp'] = ' readonly="readonly"';
                $editAddOptions = '<option value="' . $origin_location_country_code . '">' . $origin_location_country . '</option>';
                if ($origin_location_country_code != $destination_location_country_code)
                    $editAddOptions .= '<option value="' . $destination_location_country_code . '">' . $destination_location_country . '</option>';
                $editAddOptions .= '<option value="more" class="more_sel_text">More</option>';
                $viewArray['editAddOptions'] = $editAddOptions;
            }
            $trip = isset($tripDetails) ? $tripDetails : false;
            $trip_seeker = isset($trip_seeker_details) ? $trip_seeker_details : false;
            $trip_seeker_project = isset($trip_seeker_project_details) ? $trip_seeker_project_details : false;
            $viewArray['trip'] = $trip;
            $viewArray['trip_seeker'] = $trip_seeker;
            $viewArray['trip_seeker_project'] = $trip_seeker_project;
            $viewArray['currency'] = $this->CommonMethodsModel->getCurrency();
            $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
            $viewArray['travel_plans'] = $this->TripPlugin->getTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid);
            $viewArray['project_tasks_categories'] = $this->TripModel->getProjectTasksCategory();
            $viewArray['project_service_type'] = 'travel_location';
            $viewArray['workCategory'] = $this->workCategory;
            $viewArray['comSessObj'] = $this->sessionObj;
        }
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Seeker',
                'title' => 'Seeker',
                'active' => false,
                'redirect' => '/seeker'
            ),
            array(
                'label' => 'Add Product Service',
                'title' => 'Add Product Service',
                'active' => false,
                'redirect' => '/seeker/add-project-service'
            ),
            array(
                'label' => 'Travel Location',
                'title' => 'Travel Location',
                'active' => true,
                'redirect' => false //'/seeker/add-project-service'
            )
        );
        $this->layout()->pageController = 'seeker';
        $this->layout()->pageFunction = 'addProjectService';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function addProjectServiceOnLocationAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $viewArray = array();
        if ($_POST) {
            $user_location = $this->TripPlugin->addTripOrginLocation();
            /*if($this->sessionObj->userrole!='seeker')                $trip_ID = $this->TripPlugin->addTripDetail($user_location);            else                $trip_ID = 0;*/
            $trip_ID = $this->TripPlugin->addTripDetail($user_location);
            $this->TripPlugin->addSeekerProjectService($trip_ID);
            $retArr = array(
                'msg' => 'Product service trip listing has been created successfully. Please check status in Menu -> My Travel Needs.',
                'result' => 'success',
                'trip_id' => $trip_ID
            );
            echo json_encode($retArr);
            exit;
        } else {
            $viewArray['user_locations'] = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
            $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
            $viewArray['travel_plans'] = $this->TripPlugin->getTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid);
            $viewArray['project_tasks_categories'] = $this->TripModel->getProjectTasksCategory();
            $viewArray['project_tasks_categories_type'] = $this->TripModel->getProjectTasksCategorytype();
            $viewArray['project_service_type'] = 'on_location';
        }
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Seeker',
                'title' => 'Seeker',
                'active' => false,
                'redirect' => '/seeker'
            ),
            array(
                'label' => 'Add Product Service',
                'title' => 'Add Product Service',
                'active' => false,
                'redirect' => '/seeker'
            ),
            array(
                'label' => 'On Location',
                'title' => 'On Location',
                'active' => true,
                'redirect' => false //'/seeker/add-project-service-on-location'
            )
        );
        $this->layout()->pageController = 'seeker';
        $this->layout()->pageFunction = 'addProjectService';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function tripcontendersAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
		
		$_GET['service'] = $_GET['service'] == 'Project'?'Product':$_GET['service'];
		
        $viewArray = array();
        $this->CommonPlugin = $this->plugin('CommonPlugin');
        $this->TripPlugin = $this->plugin('TripPlugin');
        $ID = $this->TripPlugin->decrypt($this->params('ID'));
        $trip = $this->TripModel->getSeekerTrip($ID);
        $service = isset($_GET['service']) ? $_GET['service'] : '';
        $viewArray['trip'] = $trip;
		$viewArray['trip_id'] = $ID;
        $viewArray['user'] = $this->CommonMethodsModel->getUser($trip['user']);
        $dob = strtotime($viewArray['user']['dob']);
        $todaydate = strtotime(date('Y-m-d'));
        $viewArray['age'] = $this->CommonPlugin->calculateAge($dob, $todaydate);
        $viewArray['product_categories'] = $this->TripModel->getProjectTasksCategorytype();
        $viewArray['product_subcategories'] = $this->TripModel->getProjectTasksCategorylisttypeAll();
        $viewArray['task_categories'] = $this->TripModel->getProjectTasksCategorytype();
        $primary = 'yes';
        $viewArray['user_location'] = $this->CommonMethodsModel->getUserLocations($trip['user'], $primary);
        $trip_seeker = $this->TripPlugin->getTripDetails($ID, 'seeker', false, $service);
        $user_cards_details = $this->TripModel->getcarddetails($this->sessionObj->userid);
        $viewArray['trip'] = $trip_seeker;
        $trip_seeker_people = $this->TripModel->getSeekerTripPeople($ID);
        $viewArray['trip_seeker'] = $trip_seeker_people;
        $trip_seeker_packages = $this->TripModel->getSeekerTripPackage($ID);
        $viewArray['trip_seeker_packages'] = $trip_seeker_packages;
        $trip_seeker_projects = $this->TripModel->getSeekerTripProject($ID);
        $viewArray['trip_seeker_projects'] = $trip_seeker_projects;
        $viewArray['traveller_people_requests'] = $this->TripModel->getTravellerPeopleRequests($ID);
        $viewArray['traveller_package_requests'] = $this->TripModel->getTravellerPackageRequests($ID);
        $viewArray['traveller_project_requests'] = $this->TripModel->getTravellerProjectRequests($ID);
        $seeker_people_requests = '';
        if (isset($trip_seeker_people['ID'])) {
            $seeker_people_requests = $this->TripModel->getSeekerPeopleRequestsBySeeker($trip_seeker_people['ID']);
        }
        $seeker_package_requests = '';
        if (isset($trip_seeker_packages['ID'])) {
            $seeker_package_requests = $this->TripModel->getSeekerPackageRequestsBySeeker($trip_seeker_packages['ID']);
        }
        $seeker_project_requests = '';
        if (isset($trip_seeker_projects['ID'])) {
            $seeker_project_requests = $this->TripModel->getSeekerProjectRequestsBySeeker($trip_seeker_projects['ID']);
        }
        $viewArray['seeker_people_requests'] = $seeker_people_requests;
        $viewArray['seeker_package_requests'] = $seeker_package_requests;
        $viewArray['seeker_project_requests'] = $seeker_project_requests;
        $viewArray['workCategory'] = $this->workCategory;
        $viewArray['user_cards_details'] = $user_cards_details;
        $viewArray['comSessObj'] = $this->sessionObj;
		
		if(isset($_REQUEST['from']) && $_REQUEST['from'] == 'wishlist'){
			$this->layout()->CommonMethodsModel     = $this->CommonMethodsModel;
			$this->layout()->breadCrumbArr = array(
				array(
					'label' => 'Wishlist',
					'title' => 'Wishlist',
					'active' => false,
					'redirect' => '/wishlist'
				),
				array(
					'label' => 'Wishlisted Request',
					'title' => 'Wishlisted Request',
					'active' => true,
					'redirect' => false
				)
			);
		} else {
			$this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
			$this->layout()->breadCrumbArr = array(
				array(
					'label' => 'Trip',
					'title' => 'Trip',
					'active' => false,
					'redirect' => '/seeker'
				),
				array(
					'label' => 'Add Product Service',
					'title' => 'Add Product Service',
					'active' => true,
					'redirect' => false
				)
			);
		}
		$viewArray['stripeConfig'] = $this->stripeConfig;
		
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    /*** START This is the section for send Request to traveller's trips by seeker ***/
    public function addSeekerPeopleRequestAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $userDetails = $this->sessionObj->user;
        extract($_POST);
        $traveller_trip = $this->TripModel->getTrip($traveller_trip_id);
        if ($traveller_trip['user'] != $userDetails['ID']) {
            if (isset($seeker_trip_id) && !empty($seeker_trip_id)) {
                $seekerPeopleRequest = $this->TripModel->getSeekerPeopleRequest($seeker_trip_id);
                if (!empty($seekerPeopleRequest)) {
                    echo json_encode(array(
                        'msg' => 'You have already sent people service request to this traveler.'
                    ));
                    exit;
                } else {
                    $this->TripPlugin->addSeekerPeopleService($seeker_trip_id);
                }
            } else {
                if (isset($trip_orgin_location) && !empty($trip_orgin_location)) {
                    $locationId = $trip_orgin_location;
                } else {
                    $user_primary_location = $this->TripModel->getUserPrimaryLocation($this->sessionObj->userid);
                    $locationId = $user_primary_location['ID'];
                }
                $seeker_trip_exist = $this->TripModel->checkSeekerTripExistByTravellerTrip($traveller_trip_id, $this->sessionObj->userid);
                if ($seeker_trip_exist['ID'] == '') {
                    $new_seeker_trip_id = $this->TripModel->addSeekerTripByTravellerTrip($traveller_trip_id, $this->sessionObj->userid, $locationId);
                    $seeker_people_request_id = $this->TripPlugin->addSeekerPeopleService($new_seeker_trip_id);
                    echo json_encode(array(
                        'seeker_trip_id' => $new_seeker_trip_id,
                        'seeker_people_request_id' => $seeker_people_request_id,
                        'msg' => ' People service request has been sent successfully!'
                    ));
                    exit;
                } else {
                    $seeker_trip_people_service_exist = $this->TripModel->getSeekerTripPeople($seeker_trip_exist['ID']);
                    if ($seeker_trip_people_service_exist['ID'] == '') {
                        $seeker_people_request_id = $this->TripPlugin->addSeekerPeopleService($seeker_trip_exist['ID']);
                        echo json_encode(array(
                            'seeker_trip_id' => $seeker_trip_exist['ID'],
                            'seeker_people_request_id' => $seeker_people_request_id,
                            'msg' => ' People service request has been sent successfully!'
                        ));
                        exit;
                    } else {
                        echo json_encode(array(
                            'msg' => ' People service information is already existing for your trip!'
                        ));
                        exit;
                    }
                }
            }
        } else {
            echo json_encode(array(
                'msg' => ' You cant send request to your own trip.'
            ));
            exit;
        }
        exit;
    }

    public function addSeekerPackageRequestAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $userDetails = $this->sessionObj->user;
        extract($_POST);
        $traveller_trip = $this->TripModel->getTrip($traveller_trip_id);
        if ($traveller_trip['user'] != $userDetails['ID']) {
            if ($seeker_trip_id) {
                $seekerPackageRequest = $this->TripModel->getSeekerPackageRequest($seeker_trip_id);
                if ($seekerPackageRequest['package_request']) {
                    echo json_encode(array(
                        'msg' => 'You have already sent package service request to this traveler.'
                    ));
                    exit;
                } else {
                    $this->TripPlugin->addSeekerPackageService($seeker_trip_id);
                }
            } else {
                if (isset($trip_orgin_location) && !empty($trip_orgin_location)) {
                    $locationId = $trip_orgin_location;
                } else {
                    $user_primary_location = $this->TripModel->getUserPrimaryLocation($this->sessionObj->userid);
                    $locationId = $user_primary_location['ID'];
                }
                $seeker_trip_exist = $this->TripModel->checkSeekerTripExistByTravellerTrip($traveller_trip_id, $this->sessionObj->userid);
                if ($seeker_trip_exist['ID'] == '') {
                    $new_seeker_trip_id = $this->TripModel->addSeekerTripByTravellerTrip($traveller_trip_id, $this->sessionObj->userid, $locationId);
                    $seeker_package_request_id = $this->TripPlugin->addSeekerPackageService($new_seeker_trip_id);
                    echo json_encode(array(
                        'seeker_trip_id' => $new_seeker_trip_id,
                        'seeker_package_request_id' => $seeker_package_request_id,
                        'msg' => ' Package service trip listing has been created successfully. Please check status in Menu -> My Travel Needs.'
                    ));
                    exit;
                } else {
                    $seeker_trip_package_service_exist = $this->TripModel->getSeekerTripPackage($seeker_trip_exist['ID']);
                    if ($seeker_trip_package_service_exist['ID'] == '') {
                        $seeker_package_request_id = $this->TripPlugin->addSeekerPackageService($seeker_trip_exist['ID']);
                        echo json_encode(array(
                            'seeker_trip_id' => $seeker_trip_exist['ID'],
                            'seeker_package_request_id' => $seeker_package_request_id,
                            'msg' => ' Package service trip listing has been created successfully. Please check status in Menu -> My Travel Needs.'
                        ));
                        exit;
                    } else {
                        echo json_encode(array(
                            'msg' => ' Package service information is already existing for your trip!'
                        ));
                        exit;
                    }
                }
            }
        } else {
            echo " You can't send request to your own trip.";
        }
        exit;
    }
    /*** END This is the section for send Request to traveller's trips by seeker ***/

    public function addSeekerProjectRequestAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $userDetails = $this->sessionObj->user;
        extract($_POST);
        $traveller_trip = $this->TripModel->getTrip($traveller_trip_id);
        if ($traveller_trip['user'] != $userDetails['ID']) {
            $seeker_trip_id = 0;
            if ($seeker_trip_id) {
                $seekerProjectRequest = $this->TripModel->getSeekerProjectRequest($seeker_trip_id);
                if (isset($seekerProjectRequest[0]['project_request']) && $seekerProjectRequest[0]['project_request']) {
                    echo json_encode(array(
                        'msg' => 'You have already sent product service request to this traveler.'
                    ));
                    exit;
                } else {
                    $this->TripPlugin->addSeekerProjectService($seeker_trip_id);
                }
            } else {
                if (isset($trip_orgin_location) && !empty($trip_orgin_location)) {
                    $locationId = $trip_orgin_location;
                } else {
                    $user_primary_location = $this->TripModel->getUserPrimaryLocation($this->sessionObj->userid);
                    $locationId = $user_primary_location['ID'];
                }
                $seeker_trip_exist = $this->TripModel->checkSeekerTripExistByTravellerTrip($traveller_trip_id, $this->sessionObj->userid);
                if ($seeker_trip_exist['ID'] == '') {
                    $new_seeker_trip_id = $this->TripModel->addSeekerTripByTravellerTrip($traveller_trip_id, $this->sessionObj->userid, $locationId);
                    $seeker_project_request_id = $this->TripPlugin->addSeekerProjectService($new_seeker_trip_id);
                    echo json_encode(array(
                        'seeker_trip_id' => $new_seeker_trip_id,
                        'seeker_project_request_id' => $seeker_project_request_id,
                        'msg' => ' Product service trip listing has been created successfully. Please check status in Menu -> My Travel Needs.'
                    ));
                    exit;
                } else {
                    $seeker_trip_project_service_exist = $this->TripModel->getSeekerTripProject($seeker_trip_exist['ID']);
                    if ($seeker_trip_project_service_exist['ID'] == '') {
                        $seeker_project_request_id = $this->TripPlugin->addSeekerProjectService($seeker_trip_exist['ID']);
                        echo json_encode(array(
                            'seeker_trip_id' => $seeker_trip_exist['ID'],
                            'seeker_project_request_id' => $seeker_project_request_id,
                            'msg' => ' Product service trip listing has been created successfully. Please check status in Menu -> My Travel Needs.'
                        ));
                        exit;
                    } else {
                        echo json_encode(array(
                            'msg' => ' Product service information is already existing for your trip!'
                        ));
                        exit;
                    }
                }
            }
        } else {
            echo " You can't send request to your own trip.";
        }
        exit;
    }

    public function addSeekerTripRequestAction()
    {
        
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        extract($_POST);
		
        if (isset($cardtype) && $cardtype == 'existing_card') {
            $cardId = $existing_card;
        } else if (isset($_POST['holder_name']) && !empty($_POST['holder_name'])) {
			
			$userInfo = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			$sourceInfo = json_decode($sourceInfo,true);
			
			if(isset($userInfo['stripe_id']) && $userInfo['stripe_id'] != ''){
				$customerInfo = $this->TripPlugin->attachStripeSource($sourceInfo['source']['id'],$userInfo['stripe_id']);
			} else {
				$customerInfo = $this->TripPlugin->createStripeCustomer($sourceInfo);
				$this->ProfileModel->updateprofile(array('stripe_id'=>$customerInfo['cus_id']),$this->sessionObj->userid);
			}
			
			if($customerInfo['err_msg'] != ''){
				echo json_encode(array('msg'=>"We couldn't authorize this transaction due to
which your service request hasn’t been sent to traveler/seeker. Reason: ".$customerInfo['err_msg']));exit;
			}

            $arr_peopleabs_detail = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'holder_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_name']),
                'holder_last_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_last_name']),
                'card_billing_address' => $this->CommonMethodsModel->cleanQuery(isset($_POST['card_billing_address']) ? $_POST['card_billing_address'] : ''),
                'card_billing_country' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_country']),
                'card_billing_zipcode' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_zipcode']),
				'invisibile' => isset($_POST['save_card']) && $_POST['save_card'] == 1?0:1,
				'stripe_src_id' => $this->CommonMethodsModel->cleanQuery($customerInfo['src_id']),
                'modified' => date('Y-m-d H:i:s')
            );
            $cardId = $this->TripModel->Addcarddetails($arr_peopleabs_detail);
        }
        $userDetails = $this->sessionObj->user;
        $traveller_service_request_exist = $service_request_exist = '';
        $traveller_trip = $this->TripModel->getTrip($traveller_trip_id);
		$seeker_trip = 0;
        if ($traveller_trip['user'] != $userDetails['ID']) {
            if ($seeker_people_request_id) {
                $seeker_people = $this->TripModel->getSeekerPeopleByID($seeker_people_request_id);
                $travellerSentPeopleRequestAlready = $this->TripModel->checkTravellerPeopleRequestSentAlready($traveller_trip_id, $seeker_people['seeker_trip']);
				
				$seeker_trip = $seeker_people['seeker_trip'];
                if ($travellerSentPeopleRequestAlready['ID'] == '') {
                    $SeekerPeopleRequestExist = $this->TripModel->checkSeekerPeopleRequestExist($traveller_trip_id, $seeker_people_request_id);
                    if ($SeekerPeopleRequestExist['ID'] == '') {
                        $trip_seeker_people_request = array(
                            'trip' => $traveller_trip_id,
                            'people_request' => $seeker_people_request_id,
                            'service_id' => $seeker_people_service_id,
                            'card_id' => $cardId,
                            'approved' => 0,
                            'modified' => date('Y-m-d H:i:s')
                        );
                        $this->TripModel->addTripSeekerPeopleRequest($trip_seeker_people_request);
                    } else {
                        $service_request_exist = 'people';
                    }
                } else {
                    $traveller_service_request_exist = 'people';
                }
            }
            if ($seeker_package_request_id) {
                $seeker_package = $this->TripModel->getSeekerPackageByID($seeker_package_request_id);
                $travellerSentPackageRequestAlready = $this->TripModel->checkTravellerPackageRequestSentAlready($traveller_trip_id, $seeker_package['seeker_trip']);
				
				$seeker_trip = $seeker_package['seeker_trip'];
                if ($travellerSentPackageRequestAlready['ID'] == '') {
                    $SeekerPackageRequestExist = $this->TripModel->checkSeekerPackageRequestExist($traveller_trip_id, $seeker_package_request_id);
                    if ($SeekerPackageRequestExist['ID'] == '') {
                        $trip_seeker_package_request = array(
                            'trip' => $traveller_trip_id,
                            'package_request' => $seeker_package_request_id,
                            'service_id' => $seeker_package_service_id,
                            'card_id' => $cardId,
                            'traveller_package_amount' => $traveller_package_amount,
                            'approved' => 0,
                            'modified' => date('Y-m-d H:i:s')
                        );
                        $this->TripModel->addTripSeekerPackageRequest($trip_seeker_package_request);
                    } else {
                        if ($service_request_exist)
                            $service_request_exist = $service_request_exist . ',';
                        $service_request_exist = $service_request_exist . ' package';
                    }
                } else {
                    if ($traveller_service_request_exist)
                        $traveller_service_request_exist = $traveller_service_request_exist . ',';
                    $traveller_service_request_exist = $traveller_service_request_exist . 'package';
                }
            }
            if ($seeker_project_request_id) {
                $seeker_project = $this->TripModel->getSeekerProjectByID($seeker_project_request_id);
                $travellerSentProjectRequestAlready = $this->TripModel->checkTravellerProjectRequestSentAlready($traveller_trip_id, $seeker_project['seeker_trip']);
				
				$seeker_trip = $seeker_project['seeker_trip'];
                if ($travellerSentProjectRequestAlready['ID'] == '') {
                    $SeekerProjectRequestExist = $this->TripModel->checkSeekerProjectRequestExist($traveller_trip_id, $seeker_project_request_id);
                    if ($SeekerProjectRequestExist['ID'] == '') {
                        $trip_seeker_project_request = array(
                            'trip' => $traveller_trip_id,
                            'project_request' => $seeker_project_request_id,
                            'service_id' => $seeker_project_service_id,
                            'card_id' => $cardId,
                            'approved' => 0,
                            'modified' => date('Y-m-d H:i:s')
                        );
                        $this->TripModel->addTripSeekerProjectRequest($trip_seeker_project_request);
                    } else {
                        if ($service_request_exist)
                            $service_request_exist = $service_request_exist . ',';
                        $service_request_exist = $service_request_exist . ' project';
                    }
                } else {
                    if ($traveller_service_request_exist)
                        $traveller_service_request_exist = $traveller_service_request_exist . ',';
                    $traveller_service_request_exist = $traveller_service_request_exist . 'project';
                }
            }
            if ($traveller_service_request_exist)
                echo json_encode(array('msg'=>" This Traveler has already sent $traveller_service_request_exist service request to your trip, please check Menu -> Traveler Requests page for more details.<br><br>"));
            else {
                if ($service_request_exist)
                    echo json_encode(array('msg'=>" You have already sent $service_request_exist service request to this traveler"));
                else
                    $s = $this->TripModel->checkSeekerTripDetailsViaID($traveller_trip_id);
                //print_r($_POST);die;
                $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
                $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
                $note_reciever_id = $s['user'];
                $note_sender_id = $this->sessionObj->userid;
                $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
                $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];

                $uD = $this->CommonMethodsModel->getUser($note_sender_id);
				$nname = '';
                if (strtolower($uD['gender']) == 'male') {
                    $nname = 'Mr. ' . $uD['first_name'];
                } elseif (strtolower($uD['gender']) == 'female') {
                    $nname = 'Ms./Mrs. ' . $uD['first_name'];
                }

                if ($_POST['seeker_people_request_id'] != '') {
					$tos = 'People';
                    $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for people companionship, " . $nname . " (Seeker) has sent you people service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours.";
                    $msg = "For the trip from " . $note_origin . " to " . $note_destination . "," . $nname . "  has sent you people service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours..";
                } else if ($_POST['seeker_package_request_id'] != '') {
					$tos = 'Package';
                    $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for package delivery, " . $nname . " (Seeker) has sent you package service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours.";
                    $msg = "For the trip from " . $note_origin . " to " . $note_destination . "," . $nname . "  has sent you package service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours.";
                } else if ($_POST['seeker_project_request_id'] != '') {
					$tos = 'Project';
                    $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for product procurement, " . $nname . " (Seeker) has sent you product service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours.";
                    $msg = "For the trip from " . $note_origin . " to " . $note_destination . "," . $nname . "  has sent you product service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours";
                }

                $hugeData = array('recevier_id' => $note_reciever_id,
                    'sender_id' => $note_sender_id,
                    'message' => $message_for_user,
                    'create_date' => date("Y-m-d H:i:s"),
                    'modified_date' => date("Y-m-d H:i:s"),
                    'active' => 1);
                $this->TripModel->addToSeekerEnquiry($hugeData);
                $hugedata1 = array('user_id' => $note_reciever_id,
                    'notification_type' => 'seeker_request',
                    'notification_subject' => 'Request Send',
                    'notification_message' => $msg,
					'app_redirect' => '/traveller/tripcontenders/'.$this->encrypt($traveller_trip_id).'?action=toyou&service='.$tos,
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
                echo json_encode(array('msg'=>"We have authorized this transaction, and your service request has been sent to traveler successfully.",'redirect'=>'/seeker/tripcontenders/'.$this->encrypt($seeker_trip).'?action=toyou&service='.$tos));
            }
        } else {
            echo json_encode(array('msg'=>" You can't send request to your own trip."));
        }
        exit;
    }

    /*** Start This is the section for approve Traveller's Request for seeker's Trip***/
    public function approveTravellerPeopleRequestAction()
    {   //print_r($_POST);die;
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        extract($_POST);
		
		$userInfo = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
		
        if (isset($cardtype) && $cardtype == 'existing_card') {
            $cardId = $existing_card;
			$customerInfo['cus_id'] = $userInfo['stripe_id'];
        } else {
			$sourceInfo = json_decode($sourceInfo,true);
			if(isset($userInfo['stripe_id']) && $userInfo['stripe_id'] != ''){
				$customerInfo = $this->TripPlugin->attachStripeSource($sourceInfo['source']['id'],$userInfo['stripe_id']);
			} else {
				$customerInfo = $this->TripPlugin->createStripeCustomer($sourceInfo);
				$this->ProfileModel->updateprofile(array('stripe_id'=>$customerInfo['cus_id']),$this->sessionObj->userid);
			}
			
			if($customerInfo['err_msg'] != ''){
				echo json_encode(array('msg_status'=>'failure','msg'=>"We couldn't authorize this transaction due to
which your service request hasn't been sent to traveler. Reason: ".$customerInfo['err_msg']));exit;
			}
			
            $arr_peopleabs_detail = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'holder_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_name']),
                'holder_last_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_last_name']),
                'card_billing_address' => $this->CommonMethodsModel->cleanQuery(isset($_POST['card_billing_address']) ? $_POST['card_billing_address'] : ''),
                'card_billing_country' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_country']),
                'card_billing_zipcode' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_zipcode']),
				'invisibile' => isset($_POST['save_card']) && $_POST['save_card'] == 1?0:1,
				'stripe_src_id' => $this->CommonMethodsModel->cleanQuery($customerInfo['src_id']),
                'modified' => date('Y-m-d H:i:s')
            );
            $cardId = $this->TripModel->Addcarddetails($arr_peopleabs_detail);
        }
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        $traveller_people_request = $this->TripModel->getTravellerPeopleRequestUser($requestid);
        $seeker_trip_user = $this->TripModel->getSeekerTripUser($tripid);
        $peopleservice = $this->TripModel->getSeekerTripPeople($tripid);
        /*echo $status;die;*/
        if ($status == 2) {

            $su = $this->TripModel->checkSeekerTripDetailsViaID($tripid);

            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($su['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($su['destination_location']);
            $note_reciever_id = $su['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];


            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }


            $hugeData = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_declined',
                'notification_subject' => 'Request Declined',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has declined your people service request. Please send request to other potential travelers.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugeData);
            $this->TripModel->approveTravellerPeopleRequest($tripid, $requestid, $status);
            $msg_status = 'success';
            $msg = $traveller_people_request['first_name'] . " " . $traveller_people_request['last_name'] . ' has disapproved your people service request.';
            echo json_encode(array(
                'msg_status' => $msg_status,
                'msg' => $msg
            ));
            exit;
        }
        if ($status == 1) {
            $primary_location = '';
            $user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false, $seeker_trip_user['ID'], $primary_location);
            $primary_card = 'yes';
            $user_card = $this->CommonMethodsModel->getUserCardDetails($cardId);
            $country = $this->CommonMethodsModel->getCountry($user_location['country']);
            $arrDates = explode('/', $user_card['card_valid']);
            $param = array(
                'amount' => $peopleservice['people_service_fee'],
                /*'cc_type' => $user_card['card_type'],*/
                'pay_trip_id' => $tripid,
                'pay_to_trip_id' => $traveller_people_request['tripId'],
                'pay_user_type' => 'seeker',
                'pay_service' => 'people',
                'pay_user' => $this->sessionObj->userid,
                'pay_to_user' => $traveller_people_request['ID'],
                /*'pay_card_type' => $user_card['payment_card_type'],*/
                'pay_service_id' => $traveller_people_request['serviceId'],
                'pay_type' => 'Hold',
                'pay_trip_table' => 'seeker_trips',
                'pay_request_id' => $requestid,
                /*'cc_number' => $this->decrypt($user_card['card_no']),
                'expirty_month' => $arrDates[0],
                'expirty_year' => $arrDates[1],
                'security_code' => $user_card['card_cvv'],*/
                'first_name' => $user_card['holder_name'],
                'last_name' => $user_card['holder_last_name'],
                'email_address' => $seeker_trip_user['email_address'],
                'address' => $user_location['street_address_1'],
                'city' => $user_location['city'],
                'phone_no' => $traveller_people_request['phone'],
                'state' => $user_location['state'],
                'zip' => $user_location['zip_code'],
                'country_code' => $country['code'],
				'stripe_src_id'=> $user_card['stripe_src_id'],
				'stripe_customer_id'=>$customerInfo['cus_id']
            );
            $response = array();
            $response = $this->TripPlugin->doPayPalPayment($param);
            // print_r($response);die;
            //$response['ACK']  = 'Success';
            //$response['pay_id']  = '123';

            if (isset($response['ACK']) && $response['ACK'] == 'Success') {
                $this->TripModel->approveTravellerPeopleRequest($tripid, $requestid, $status);
                $msg_status = 'success';
                $pay_id = $response['pay_id'];
                $msg = $traveller_people_request['first_name'] . " " . $traveller_people_request['last_name'] . ' has accepted your people service request. Please contact him/her for further coordination.';

                $s = $this->TripModel->checkSeekerTripDetailsViaID($tripid);

                //print_r($s);die;
                $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
                $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
                $note_reciever_id = $s['user'];
                $note_sender_id = $this->sessionObj->userid;
                $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
                $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
                $tos = $_POST['requestService'];

                $uD = $this->CommonMethodsModel->getUser($note_sender_id);
                if (strtolower($uD['gender']) == 'male') {
                    $nname = 'Mr. ' . $uD['first_name'];
                    $him_her = 'him';
                } elseif (strtolower($uD['gender']) == 'female') {
                    $nname = 'Ms./Mrs. ' . $uD['first_name'];
                    $him_her = 'her';
                }

                if ($tos == 'people') {
                    $message_for_user = "For the people companionship service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
                } else if ($tos == 'package') {
                    $message_for_user = "For the package delivery service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
                } else {
                    $message_for_user = "For the product procurement service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
                }

                $hugeData = array('user_id' => $note_reciever_id,
                    'message' => $message_for_user,
                    'msg_type' => 'm',
                    'create_date' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToUserMessages($hugeData);
                $hugedata1 = array('user_id' => $note_reciever_id,
                    'notification_type' => 'request_accept',
                    'notification_subject' => 'Request Accept',
                    'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted people service request. Please contact him/her for further coordination.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
                //here
            } else {
                $pay_id = 0;
                $msg_status = 'failed';
                $msg = $traveller_people_request['first_name'] . " " . $traveller_people_request['last_name'] . ' has not been approved, due to payment authorization failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
            }
            echo json_encode(array(
                'msg_status' => $msg_status,
                'msg' => $msg,
                'pay_id' => $this->encrypt($pay_id)
            ));
            exit;
        } else {
            $this->TripModel->approveTravellerPeopleRequest($tripid, $requestid, $status);
            if ($status == '1') {
                $msg_status = 'success';
                $msg = $traveller_people_request['first_name'] . " " . $traveller_people_request['last_name'] . ' has been approved for your people service request';
                print_r($msg);
                die;
                echo json_encode(array(
                    'msg_status' => $msg_status,
                    'msg' => $msg
                ));
            } else {
                $msg_status = 'failure';
                $msg = $traveller_people_request['first_name'] . " " . $traveller_people_request['last_name'] . ' has disapproved your people service request.';
                echo json_encode(array(
                    'msg_status' => $msg_status,
                    'msg' => $msg
                ));
            }

            exit;
        }
        exit;
    }

    public function approveTravellerPackageRequestAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        extract($_POST);
		
		$userInfo = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
		
        if (isset($cardtype) && $cardtype == 'existing_card') {
            $cardId = $existing_card;
			$customerInfo['cus_id'] = $userInfo['stripe_id'];
        } else {
			
			$sourceInfo = json_decode($sourceInfo,true);		
			if(isset($userInfo['stripe_id']) && $userInfo['stripe_id'] != ''){
				$customerInfo = $this->TripPlugin->attachStripeSource($sourceInfo['source']['id'],$userInfo['stripe_id']);
			} else {
				$customerInfo = $this->TripPlugin->createStripeCustomer($sourceInfo);
				$this->ProfileModel->updateprofile(array('stripe_id'=>$customerInfo['cus_id']),$this->sessionObj->userid);
			}
			
			if($customerInfo['err_msg'] != ''){
				echo json_encode(array('msg_status'=>'failure','msg'=>"We couldn't authorize this transaction due to
which your service request hasn't been sent to traveler. Reason: ".$customerInfo['err_msg']));exit;
			}
            $arr_peopleabs_detail = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'holder_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_name']),
                'holder_last_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_last_name']),
                'card_billing_address' => $this->CommonMethodsModel->cleanQuery(isset($_POST['card_billing_address']) ? $_POST['card_billing_address'] : ''),
                'card_billing_country' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_country']),
                'card_billing_zipcode' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_zipcode']),
				'invisibile' => isset($_POST['save_card']) && $_POST['save_card'] == 1?0:1,
				'stripe_src_id' => $this->CommonMethodsModel->cleanQuery($customerInfo['src_id']),
                'modified' => date('Y-m-d H:i:s')
            );
            $cardId = $this->TripModel->Addcarddetails($arr_peopleabs_detail);
        }
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        $seeker_trip_user = $this->TripModel->getSeekerTripUser($tripid);
        $trip_package_request = $this->TripModel->getTravellerPackageRequestUser($requestid);
        $packageservice = $this->TripModel->getSeekerTripPackage($tripid);
        if ($status == 1) {
            $primary_location = '';
            $user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false, $seeker_trip_user['ID'], $primary_location);
            $primary_card = 'yes';
            $user_card = $this->CommonMethodsModel->getUserCardDetails($cardId);
            $country = $this->CommonMethodsModel->getCountry($user_location['country']);
            $arrDates = explode('/', $user_card['card_valid']);
            $param = array(
                'amount' => $packageservice['package_service_fee'],
                /*'cc_type' => $user_card['card_type'],*/
                'pay_trip_id' => $tripid,
                'pay_to_trip_id' => $trip_package_request['tripId'],
                'pay_user_type' => 'seeker',
                'pay_service' => 'package',
                'pay_type' => 'Hold',
                'pay_trip_table' => 'seeker_trips',
                'pay_user' => $this->sessionObj->userid,
                'pay_to_user' => $trip_package_request['ID'],
                /*'pay_card_type' => $user_card['payment_card_type'],*/
                'pay_service_id' => $trip_package_request['serviceId'],
                'pay_request_id' => $requestid,
                /*'cc_number' => $this->decrypt($user_card['card_no']),
                'expirty_month' => $arrDates[0],
                'expirty_year' => $arrDates[1],
                'security_code' => $user_card['card_cvv'],*/
                'first_name' => $user_card['holder_name'],
                'last_name' => $user_card['holder_last_name'],
                'email_address' => $seeker_trip_user['email_address'],
                'address' => $user_location['street_address_1'],
                'city' => $user_location['city'],
                'phone_no' => $trip_package_request['phone'],
                'state' => $user_location['state'],
                'zip' => $user_location['zip_code'],
                'country_code' => $country['code'],
				'stripe_src_id'=> $user_card['stripe_src_id'],
				'stripe_customer_id'=>$customerInfo['cus_id']
            );
            $response = array();
            $response = $this->TripPlugin->doPayPalPayment($param);
            // print_r($response);die;
            if (isset($response['ACK']) && $response['ACK'] == 'Success') {
                $this->TripModel->approveTravellerPackageRequest($tripid, $requestid, $status);
                $msg_status = 'success';
                $pay_id = $response['pay_id'];
                $msg = $trip_package_request['first_name'] . " " . $trip_package_request['last_name'] . ' has been approved for your package service request';
                $s = $this->TripModel->checkSeekerTripDetailsViaID($tripid);
                //print_r($s);die;
                $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
                $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
                $note_reciever_id = $s['user'];
                $note_sender_id = $this->sessionObj->userid;
                $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
                $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
                $tos = $_POST['requestService'];

                $uD = $this->CommonMethodsModel->getUser($note_sender_id);
                if (strtolower($uD['gender']) == 'male') {
                    $nname = 'Mr. ' . $uD['first_name'];
                    $him_her = 'him';
                } elseif (strtolower($uD['gender']) == 'female') {
                    $nname = 'Ms./Mrs. ' . $uD['first_name'];
                    $him_her = 'her';
                }

                if ($tos == 'people') {
                    $message_for_user = "For the people companionship service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
                } else if ($tos == 'package') {
                    $message_for_user = "For the package delivery service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
                } else {
                    $message_for_user = "For the product procurement service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
                }

                $hugeData = array
                (
                    'user_id' => $note_reciever_id,
                    'message' => $message_for_user,
                    'msg_type' => 'm',
                    'create_date' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToUserMessages($hugeData);

                $hugedata1 = array
                (
                    'user_id' => $note_reciever_id,
                    'notification_type' => 'request_accept',
                    'notification_subject' => 'Request Accept',
                    'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted package service request.Please contact him/her for further coordination.",
                    'notification_added' => date("Y-m-d H:i:s")
                );

                $this->TripModel->addToNotifications($hugedata1);
                //here
            } else {
                $pay_id = 0;
                $msg_status = 'failed';
                $msg = $trip_package_request['first_name'] . " " . $trip_package_request['last_name'] . ' has not been approved, due to payment authorization failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
            }
            echo json_encode(array(
                'msg_status' => $msg_status,
                'msg' => $msg,
                'pay_id' => $this->encrypt($pay_id)
            ));
            exit;
        } else {
            $su = $this->TripModel->checkSeekerTripDetailsViaID($tripid);

            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($su['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($su['destination_location']);
            $note_reciever_id = $su['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];


            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
			$nname = '';
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }


            $hugeData = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_declined',
                'notification_subject' => 'Request Declined',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has declined package service request. Please contact him/her for further coordination.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugeData);
            $this->TripModel->approveTravellerPackageRequest($tripid, $requestid, $status);
            $msg = $trip_package_request['first_name'] . " " . $trip_package_request['last_name'] . ' has disapproved your package service request.';
            echo json_encode(array(
                'msg_status' => 'success',
                'msg' => $msg
            ));
            exit;
        }
        exit;
    }

    public function approveTravellerProjectRequestAction()
    {
        //print_r($_POST);die;
        $this->TripPlugin = $this->plugin('TripPlugin');
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        extract($_POST);
		
		$userInfo = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
		
        if (isset($cardtype) && $cardtype == 'existing_card') {
            $cardId = $existing_card;
			$customerInfo['cus_id'] = $userInfo['stripe_id'];
        } else {
			$sourceInfo = json_decode($sourceInfo,true);		
			if(isset($userInfo['stripe_id']) && $userInfo['stripe_id'] != ''){
				$customerInfo = $this->TripPlugin->attachStripeSource($sourceInfo['source']['id'],$userInfo['stripe_id']);
			} else {
				$customerInfo = $this->TripPlugin->createStripeCustomer($sourceInfo);
				$this->ProfileModel->updateprofile(array('stripe_id'=>$customerInfo['cus_id']),$this->sessionObj->userid);
			}
			
			if($customerInfo['err_msg'] != ''){
				echo json_encode(array('msg_status'=>'failure','msg'=>"We couldn't authorize this transaction due to
which your service request hasn't been sent to traveler. Reason: ".$customerInfo['err_msg']));exit;
			}

            $arr_peopleabs_detail = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'holder_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_name']),
                'holder_last_name' => $this->CommonMethodsModel->cleanQuery($_POST['holder_last_name']),
                'card_billing_address' => $this->CommonMethodsModel->cleanQuery(isset($_POST['card_billing_address']) ? $_POST['card_billing_address'] : ''),
                'card_billing_country' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_country']),
                'card_billing_zipcode' => $this->CommonMethodsModel->cleanQuery($_POST['card_billing_zipcode']),
				'invisibile' => isset($_POST['save_card']) && $_POST['save_card'] == 1?0:1,
				'stripe_src_id' => $this->CommonMethodsModel->cleanQuery($customerInfo['src_id']),
                'modified' => date('Y-m-d H:i:s')
            );
            $cardId = $this->TripModel->Addcarddetails($arr_peopleabs_detail);
        }
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        /*/*$trip_project_request = $this->TripModel->getApprovedTripProjectRequestUser();*/
        $trip_project_request = $this->TripModel->getTravellerProjectRequestUser($requestid);
        $seeker_trip_user = $this->TripModel->getSeekerTripUser($tripid);
        $projectservice = $this->TripModel->getSeekerTripProject($tripid);
        if ($status == 1) {
            $primary_location = '';
            $user_location = $this->CommonMethodsModel->getUserLocationByAddrId(false, $seeker_trip_user['ID'], $primary_location);
            $primary_card = 'yes';
            $user_card = $this->CommonMethodsModel->getUserCardDetails($cardId);
            $country = $this->CommonMethodsModel->getCountry($user_location['country']);
            $arrDates = explode('/', $user_card['card_valid']);
            $param = array(
                'amount' => $projectservice['project_service_fee'],
                /*'cc_type' => $user_card['card_type'],*/
                'pay_trip_id' => $tripid,
                'pay_to_trip_id' => $trip_project_request['tripId'],
                'pay_user_type' => 'seeker',
                'pay_service' => 'project',
                'pay_request_id' => $requestid,
                'pay_user' => $this->sessionObj->userid,
                'pay_to_user' => $trip_project_request['ID'],
                /*'pay_card_type' => $user_card['payment_card_type'],*/
                'pay_service_id' => $trip_project_request['serviceId'],
                'pay_type' => 'Hold',
                'pay_trip_table' => 'seeker_trips',
                /*'cc_number' => $this->decrypt($user_card['card_no']),
                'expirty_month' => $arrDates[0],
                'expirty_year' => $arrDates[1],
                'security_code' => $user_card['card_cvv'],*/
                'first_name' => $user_card['holder_name'],
                'last_name' => $user_card['holder_last_name'],
                'email_address' => $seeker_trip_user['email_address'],
                'address' => $user_location['street_address_1'],
                'city' => $user_location['city'],
                'phone_no' => $trip_project_request['phone'],
                'state' => $user_location['state'],
                'zip' => $user_location['zip_code'],
                'country_code' => $user_card['card_billing_country'],
				'stripe_src_id'=> $user_card['stripe_src_id'],
				'stripe_customer_id'=>$customerInfo['cus_id']
            );

            $response = array();
            $response = $this->TripPlugin->doPayPalPayment($param);
            if (isset($response['ACK']) && $response['ACK'] == 'Success') {
                $this->TripModel->approveTravellerProjectRequest($tripid, $requestid, $status);
                $msg = $trip_project_request['first_name'] . " " . $trip_project_request['last_name'] . ' has been approved for your product service request';
                $msg_status = 'success';
                $pay_id = $response['pay_id'];
                $s = $this->TripModel->checkSeekerTripDetailsViaID($tripid);
                //print_r($s);die;
                $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
                $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
                $note_reciever_id = $s['user'];
                $note_sender_id = $this->sessionObj->userid;
                $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
                $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
                $tos = $_POST['requestService'];

                $uD = $this->CommonMethodsModel->getUser($note_sender_id);
                if (strtolower($uD['gender']) == 'male') {
                    $nname = 'Mr. ' . $uD['first_name'];
                    $him_her = 'him';
                } elseif (strtolower($uD['gender']) == 'female') {
                    $nname = 'Ms./Mrs. ' . $uD['first_name'];
                    $him_her = 'her';
                }

                if ($tos == 'people') {
                    $message_for_user = "For the people companionship service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
                } else if ($tos == 'package') {
                    $message_for_user = "For the package delivery service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
                } else {
                    $message_for_user = "For the product procurement service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
                }

                $hugeData = array('user_id' => $note_reciever_id,
                    'message' => $message_for_user,
                    'msg_type' => 'm',
                    'create_date' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToUserMessages($hugeData);
                $hugedata1 = array('user_id' => $note_reciever_id,
                    'notification_type' => 'request_accept',
                    'notification_subject' => 'Request Accept',
                    'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted product service request. Please contact him/her for further coordination.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
                //here
            } else {
                $msg = $trip_project_request['first_name'] . " " . $trip_project_request['last_name'] . ' has not been approved, due to payment authorization failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
                $pay_id = 0;
                $msg_status = 'failed';
            }
            echo json_encode(array(
                'msg_status' => $msg_status,
                'msg' => $msg,
                'pay_id' => $this->encrypt($pay_id)
            ));
            exit;
        } else {
            $su = $this->TripModel->checkSeekerTripDetailsViaID($tripid);

            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($su['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($su['destination_location']);
            $note_reciever_id = $su['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];


            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }


            $hugeData = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_declined',
                'notification_subject' => 'Request Declined',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has declined your product service request. Please send request to other potential travelers.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugeData);
            $this->TripModel->approveTravellerProjectRequest($tripid, $requestid, $status);
            $msg = $trip_project_request['first_name'] . " " . $trip_project_request['last_name'] . ' has disapproved your product service request.';
            echo json_encode(array(
                'msg_status' => 'success',
                'msg' => $msg
            ));
            exit;
        }
        exit;
    }

    /*** End This is the section for approve Traveller's Request for seeker's Trip***/
    public function travelerrequestAction()
    {
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $viewArray['countries'] = $this->CommonMethodsModel->getCountries();
        $viewArray['upcoming_trips'] = $this->TripPlugin->getTrips('request_list', $this->sessionObj->userrole, $this->sessionObj->userid);
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => 'Dashboard',
                'title' => 'Dashboard',
                'active' => false,
                'redirect' => '/dashboard'
            ),
            array(
                'label' => 'Traveller Request',
                'title' => 'Traveller Request',
                'active' => true,
                'redirect' => false
            )
        );
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    public function getSeekerPeopleServiceNeedAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');

        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
		$tTripId = $this->CommonMethodsModel->cleanQuery($_POST['t_tripid']);
        $viewArray['trip_seeker'] = $this->TripModel->getSeekerTripPeople($tripid);
		$sTrip = $this->TripModel->getTrip($tripid);
		if($sTrip['travel_plan_reservation_type'] == 2){
			$tTrip = $this->TripModel->getTrip($tTripId);
			if($tTrip['noofstops'] > 0){
				$stopsPrice = $this->TripModel->getNoofstopsPrice($noofstops);
				$directPrice = $this->TripModel->getNoofstopsPrice('direct');
				$addedStopPrice = $stopsPrice['stop_price'] - $directPrice['stop_price'];
				$viewArray['trip_seeker']['people_service_fee'] = $viewArray['trip_seeker']['people_service_fee'] + $addedStopPrice;
			}
		}

        $viewArray['trip_seeker_people_passengers'] = $this->TripModel->getSeekerTripPeoplePassengers($viewArray['trip_seeker']['ID']);
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)->setTerminal(true);
        return $viewModel;
    }

    public function getSeekerPackageServiceNeedAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        /*$this->_helper->layout->disableLayout();*/
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $viewArray['trip_seeker'] = $this->TripModel->getSeekerTripPackage($tripid);
        /*print_r($this->view->trip_seeker); exit*/
        $viewArray['trip_seeker_package_packages'] = $this->TripModel->getSeekerPackagesPakages($viewArray['trip_seeker']['ID']);
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)->setTerminal(true);
        return $viewModel;
    }

    public function getSeekerProjectServiceNeedAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        /*$this->_helper->layout->disableLayout();*/
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $tripDetails = $this->TripPlugin->getTripDetails($tripid, 'seeker', false, 'Product');
        $viewArray['trip_seeker'] = $this->TripModel->getSeekerTripProject($tripid);
        $viewArray['trip'] = $tripDetails;
        $viewArray['trip_seeker_project_tasks'] = $this->TripModel->getSeekerTripProjectTasks($viewArray['trip_seeker']['ID']);
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)->setTerminal(true);
        return $viewModel;
    }

    public function listingandconfirmAction()
    {
        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }
        $viewArray = array();
        $this->TripPlugin = $this->plugin('TripPlugin');
        $tripId = $this->decrypt($this->params('trip_id'));
        $service = $this->params('service');
        $tripDetails = $this->TripPlugin->getTripDetails($tripId, $this->sessionObj->userrole, true, $service);

        if($tripDetails['total_cost_currency']!="") 
        {
        
        $currency_symbol = $this->CommonMethodsModel->getCurrencySymbol($tripDetails['total_cost_currency']);
        }else {
             $currency_symbol = $this->CommonMethodsModel->getCurrencySymbol($tripDetails['total_discount_currency']);
        }

        $totalCount = $approved = $declined = $expired = $awaiting = 0;
        $totalCountBy = $approvedBy = $declinedBy = $expiredBy = $awaitingBy = 0;
        $trip_people_requests_stats = $trip_package_requests_stats = $trip_project_requests_stats = '';
        $trip_accepted_con = $this->TripModel->getAcceptedRecipts($this->sessionObj->userid, $this->sessionObj->userrole, $tripId);
		
		$currentStatus = $this->TripModel->getTripCurrentStatus($tripId,$this->sessionObj->userrole);
		
        if ($service == 'People') {
            if ($this->sessionObj->userrole == 'traveller') {
                $trip_people_requests_stats = $this->TripModel->getSeekerPeopleRequestStats($tripId);
            } else {
                $trip_people_requests_stats = $this->TripModel->getTravellerPeopleRequestStats($tripId);
            }
            $totalCount = $trip_people_requests_stats['totalCount'];
            $awaiting = $trip_people_requests_stats['awaiting'];
            $approved = $trip_people_requests_stats['approved'];
            $declined = $trip_people_requests_stats['declined'];
            $expired = $trip_people_requests_stats['expired'];
            if ($this->sessionObj->userrole == 'traveller') {
                $trip_people_requests_by_stats = $this->TripModel->getSeekerPeopleRequestsBySeekerStats($tripDetails['trip_people']['ID']);
            } else {
                $trip_people_requests_by_stats = $this->TripModel->getTravellerPeopleRequestsByTravellerStats($tripDetails['trip_people']['ID'], $this->sessionObj->userid);
            }
            $totalCountBy = $trip_people_requests_by_stats['totalCount'];
            $awaitingBy = $trip_people_requests_by_stats['awaiting'];
            $approvedBy = $trip_people_requests_by_stats['approved'];
            $declinedBy = $trip_people_requests_by_stats['declined'];
            $expiredBy = $trip_people_requests_by_stats['expired'];
        }
        if ($service == 'Package') {
            if ($this->sessionObj->userrole == 'traveller') {
                $trip_package_requests_stats = $this->TripModel->getSeekerPackageRequestStats($tripId);
            } else {
                $trip_package_requests_stats = $this->TripModel->getTravellerPackageRequestStats($tripId);
            }
            $totalCount = $trip_package_requests_stats['totalCount'];
            $awaiting = $trip_package_requests_stats['awaiting'];
            $approved = $trip_package_requests_stats['approved'];
            $declined = $trip_package_requests_stats['declined'];
            $expired = $trip_package_requests_stats['expired'];
            if ($this->sessionObj->userrole == 'traveller') {
                $trip_package_requests_by_stats = $this->TripModel->getSeekerPackageRequestsBySeekerStats($tripDetails['trip_package']['ID']);
            } else {
                $trip_package_requests_by_stats = $this->TripModel->getTravellerPackageRequestsByTravellerStats($tripDetails['trip_package']['ID']);
            }
            $totalCountBy = $trip_package_requests_by_stats['totalCount'];
            $awaitingBy = $trip_package_requests_by_stats['awaiting'];
            $approvedBy = $trip_package_requests_by_stats['approved'];
            $declinedBy = $trip_package_requests_by_stats['declined'];
            $expiredBy = $trip_package_requests_by_stats['expired'];
        }
        if ($service == 'Product') {
            if ($this->sessionObj->userrole == 'traveller') {
                $trip_project_requests_stats = $this->TripModel->getSeekerProjectRequestStats($tripId);
            } else {
                $trip_project_requests_stats = $this->TripModel->getTravellerProjectRequestStats($tripId);
            }
            $totalCount = $trip_project_requests_stats['totalCount'];
            $awaiting = $trip_project_requests_stats['awaiting'];
            $approved = $trip_project_requests_stats['approved'];
            $declined = $trip_project_requests_stats['declined'];
            $expired = $trip_project_requests_stats['expired'];
            if ($this->sessionObj->userrole == 'traveller') {
                $trip_project_requests_by_stats = $this->TripModel->getSeekerProjectRequestsBySeekerStats($tripDetails['trip_project']['ID']);
            } else {
                $trip_project_requests_by_stats = $this->TripModel->getTravellerProjectRequestsByTravellerStats($tripDetails['trip_project']['ID']);
            }
            $totalCountBy = $trip_project_requests_by_stats['totalCount'];
            $awaitingBy = $trip_project_requests_by_stats['awaiting'];
            $approvedBy = $trip_project_requests_by_stats['approved'];
            $declinedBy = $trip_project_requests_by_stats['declined'];
            $expiredBy = $trip_project_requests_by_stats['expired'];
        }
        $this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
        $redirectUrl = ($this->sessionObj->userrole == 'seeker') ? 'seeker' : 'traveller';
        if ($this->sessionObj->userrole == 'traveller') {
            $labels = 'My Travel Plans';
        } else {
            $labels = 'My Travel Needs';
        }
        $this->layout()->breadCrumbArr = array(
            array(
                'label' => $this->sessionObj->userrole,
                'title' => $this->sessionObj->userrole,
                'active' => false,
                'redirect' => '/' . $redirectUrl
            ),
            array(
                'label' => $labels,//'My Travel Plans',
                'title' => $labels,//'My Travel Plans',
                'active' => false,
                'redirect' => '/' . $redirectUrl
            ),
            array(
                'label' => 'View',
                'title' => 'View',
                'active' => true,
                'redirect' => false
            )
        );
        $viewArray = array(
            'countries' => $this->CommonMethodsModel->getCountries(),
            'currency' => $this->CommonMethodsModel->getCurrencies()
        );
        $viewArray['trip_seeker_projects'] = $this->TripModel->getSeekerTripProject($tripId);
        $viewArray['trip_seeker_packages'] = $this->TripModel->getSeekerTripPackage($tripId);
        $viewArray['userTrustVerification'] = $this->CommonMethodsModel->getUserTrustVerifyTypes($this->sessionObj->userid);
        $viewArray['referencesWritenByYou'] = $this->ProfileModel->getWritenReference($this->sessionObj->userid);
        $viewArray['referencesWritenToYou'] = $this->ProfileModel->getUserReferences($this->sessionObj->userid);
        $viewArray['userReviews'] = $this->ProfileModel->getreviews($this->sessionObj->userid);
        $viewArray['trip'] = $tripDetails;
        $viewArray['currency_symbol'] = $currency_symbol['currency_symbol'];
        $viewArray['totalCount'] = $totalCount;
        $viewArray['awaiting'] = $awaiting;
        $viewArray['approved'] = $approved;
        $viewArray['declined'] = $declined;
        $viewArray['expired'] = $expired;
        $viewArray['totalCountBy'] = $totalCountBy;
        $viewArray['awaitingBy'] = $awaitingBy;
        $viewArray['approvedBy'] = $approvedBy;
        $viewArray['declinedBy'] = $declinedBy;
        $viewArray['expiredBy'] = $expiredBy;
        $viewArray['trip_accepted_con'] = $trip_accepted_con;
        $viewArray['tripIdEnc'] = $this->params('trip_id');
        $viewArray['languages'] = $this->CommonMethodsModel->getLanguages();
        $viewArray['comSessObj'] = $this->sessionObj;
        $viewArray['workCategory'] = $this->workCategory;
		$viewArray['currentStatus'] = $currentStatus;

        //echo '<pre>';print_r($viewArray['trip']);exit();

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray);
        return $viewModel;
    }

    function updateTripAction()
    {
        $tripId = $this->decrypt($_POST['id']);
        $action = isset($_POST['action']) ? $_POST['action'] : '';
        if (isset($action) && !empty($action)) {
            $tripDetails = $this->TripModel->doTripAction($tripId, $this->sessionObj->userrole, $action);
            if ($tripDetails) {
                echo $tripDetails;
                exit;
            }
        }
        exit;
    }

    public function uploadTicketAction()
    {

        $image_path = "tickets";
        $field_name = "file";
        $file_name_prefix = $this->CommonMethodsModel->cleanQuery($_POST['tripIdNumber']) . '_' . time();
        $tripId = $this->CommonMethodsModel->cleanQuery($_POST['tripId']);
        $tripIdNumber = $this->CommonMethodsModel->cleanQuery($_POST['tripIdNumber']);
        //$uploadResult     = $this->CommonMethodsModel->uploadFiles($field_name, $file_name_prefix, $image_path);
		$uploadResult['result'] = 'fail';
		if(isset($_FILES['file'])){
			$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			if (move_uploaded_file($_FILES['file']['tmp_name'], ACTUAL_ROOTPATH . 'uploads/' . $image_path . '/' . $file_name_prefix . '.' . $ext)) {
				$uploadResult['result'] = 'success';
			}
		}
        if ($uploadResult['result'] == 'success') {

            $ticket_file = $file_name_prefix . '.' . $ext;//$uploadResult['fileName'];
            $arr_ticket_detail = array(
                //'ID' => $tripId,
                'ticket_image' => $this->CommonMethodsModel->cleanQuery($ticket_file)
            );
            //$this->TripModel->updateSeekerTrip($arr_ticket_detail, $tripId);
            $this->TripModel->updateSeekerTrip($arr_ticket_detail, $tripIdNumber);
        }
        echo json_encode(array(
            'result' => 'success'
        ));
        exit;
        exit;
    }

    function sendflightstatusAction()
    {
        $FLIGHT_STATUS = array(
            'A' => 'Active',
            'C' => 'Canceled',
            'D' => 'Diverted',
            'DN' => 'Data source needed',
            'L' => 'Landed',
            'NO' => 'Not Operational',
            'R' => 'Redirected',
            'S' => 'Scheduled',
            'U' => 'Unknown'
        );
        $FLIGHT_STATUS_CLASSES = array(
            'A' => '#2ED800',
            'C' => '#FF0000',
            'D' => '#3300FF',
            'DN' => '#9FA045',
            'L' => '#2ED800',
            'NO' => '#9FA045',
            'R' => '#3300FF',
            'S' => '#2ED800',
            'U' => '#FF0000'
        );
        $ID = $this->params('ID');
        $flightId = $this->decrypt($ID);
        $flightId = 81;
        $flightRow = $this->TripModel->getFlightRow($flightId);
        if (!empty($flightRow)) {
            $postData = $_POST;
            $arrivalDateTime = strtotime($flightRow['arrival_date']);
            $year = date('Y', $arrivalDateTime);
            $month = date('m', $arrivalDateTime);
            $day = date('d', $arrivalDateTime);
            $departure = $flightRow['departure'];
            $arrival = $flightRow['arrival'];
            $trip = $this->TripModel->getTrip($flightRow['trip'], $flightRow['user_role']);
            $trip_user = $this->CommonMethodsModel->getUser($trip['user']);
            $jsonText = $postData;
            $obj = json_decode($jsonText);
            $flightStatuses = $obj->alert->flightStatus;
            if (isset($flightStatuses) && !empty($flightStatuses)) {
                if (($flightStatuses->departureAirportFsCode == $departure) && ($flightStatuses->arrivalAirportFsCode == $arrival)) {
                    $operationalTimes = $flightStatuses->operationalTimes;
                    $arrInsData['flight_status'] = $FLIGHT_STATUS[$flightStatuses->status];
                    $arrInsData['flight_status_color'] = $FLIGHT_STATUS_CLASSES[$flightStatuses->status];
                    $arrInsData['departure_scheduled_date'] = isset($operationalTimes->scheduledGateDeparture->dateUtc) ? $operationalTimes->scheduledGateDeparture->dateUtc : '';
                    $arrInsData['departure_acutal_date'] = isset($operationalTimes->actualGateDeparture->dateUtc) ? $operationalTimes->actualGateDeparture->dateUtc : '';
                    $arrInsData['departure_terminal'] = isset($flightStatuses->airportResources->departureTerminal) ? $flightStatuses->airportResources->departureTerminal : '';
                    $arrInsData['departure_delay'] = isset($flightStatuses->delays->departureGateDelayMinutes) ? $flightStatuses->delays->departureGateDelayMinutes : '';
                    $arrInsData['arrival_scheduled_date'] = isset($operationalTimes->scheduledGateArrival->dateUtc) ? $operationalTimes->scheduledGateArrival->dateUtc : '';
                    $arrInsData['arrival_actual_date'] = isset($operationalTimes->actualGateArrival->dateUtc) ? $operationalTimes->actualGateArrival->dateUtc : '';
                    $arrInsData['arrival_terminal'] = isset($flightStatuses->airportResources->arrivalTerminal) ? $flightStatuses->airportResources->arrivalTerminal : '';
                    $arrInsData['arrival_delay'] = isset($flightStatuses->delays->arrivalGateDelayMinutes) ? $flightStatuses->delays->arrivalGateDelayMinutes : '';
                    $arrInsData['first_name'] = $trip_user['first_name'];
                    $arrInsData['email_address'] = $trip_user['email_address'];
                    $arrInsData['airline_name'] = $flightRow['airline_name'];
                    $arrInsData['airline_number'] = $flightRow['airline_number'];
                    $arrInsData['destination_location'] = $flightRow['destination_location'];
                    $arrInsData['orgin_locaion'] = $flightRow['orgin_locaion'];

                    try
                    {
                        $this->MailModel->sendFlightStatusMailtoUser($arrInsData);
                    }
                    catch (\Exception $e)
                    {
                        error_log($e);
                    }
                }
            }
        }
        exit;
    }

    function trackflightpoistionAction()
    {
        if (is_numeric($_POST['viewid'])) {
            $arrInsData['triggerpopup'] = 0;
            $flightRow = $this->TripModel->getFlightRow($_POST['viewid']);
        } else {
            $arrInsData['triggerpopup'] = 1;
            $postData = explode('@@', $_POST['viewid']);
            $flightRow['arrival_date'] = $postData[5];
            $flightRow['carrier'] = $postData[0];
            $flightRow['number'] = $postData[1];
            $flightRow['departure'] = $postData[2];
            $flightRow['arrival'] = $postData[3];
        }

        if (!empty($flightRow)) {
            $apiUrl = 'https://api.flightstats.com/flex';
            $filght_status_app_id = $this->generalvar['filght_status_app_id'];
            $filght_status_app_key = $this->generalvar['filght_status_app_key'];
            $arrivalDateTime = strtotime($flightRow['arrival_date']);
            $year = date('Y', $arrivalDateTime);
            $month = date('m', $arrivalDateTime);
            $day = date('d', $arrivalDateTime);
            $departure = $flightRow['departure'];
            $arrival = $flightRow['arrival'];
            //$tractcontent            = file_get_contents($apiUrl . "/flightstatus/rest/v2/json/flight/tracks/" . $flightRow['carrier'] . "/" . $flightRow['number'] . "/dep/" . $year . "/" . $month . "/" . $day . "?appId=" . $filght_status_app_id . "&appKey=" . $filght_status_app_key . "&utc=false&includeFlightPlan=false&maxPositions=1");
            $tractcontent = file_get_contents($apiUrl . "/flightstatus/rest/v2/json/flight/tracks/" . $flightRow['carrier'] . "/" . $flightRow['number'] . "/dep/" . $year . "/" . $month . "/" . $day . "?appId=70d5bc60&appKey=4c1e772560c4e1c05cff4438e062db9c&utc=false&includeFlightPlan=false&maxPositions=1");
            $trackobj = json_decode($tractcontent);
            $arrInsData['latitude'] = isset($flightRow['depature_latitude']) ? $flightRow['depature_latitude'] : '';
            $arrInsData['longitude'] = isset($flightRow['depature_longitude']) ? $flightRow['depature_longitude'] : '';
            $flightTracks = $trackobj->flightTracks;
            if (isset($flightTracks) && !empty($flightTracks)) {

                foreach ($flightTracks as $fTracks) {
                    if (($fTracks->departureAirportFsCode == $departure) && ($fTracks->arrivalAirportFsCode == $arrival)) {
                        if (isset($fTracks->positions[0])) {
                            $fTracksPosition = $fTracks->positions[0];
                            // $arrInsData['latitude']  = isset($fTracksPosition->lat) ? $fTracksPosition->lat : '';
                            // $arrInsData['longitude'] = isset($fTracksPosition->lon) ? $fTracksPosition->lon : '';
                            $arrInsData['latitude'] = isset($fTracksPosition->lat) ? $fTracksPosition->lat : $newData['latitude'];
                            $arrInsData['longitude'] = isset($fTracksPosition->lon) ? $fTracksPosition->lon : $newData['longitude'];
                        }
                    }
                }
            } else {
                $newData = $this->TripModel->getLatLongOfDeparture($flightRow['departure']);
                $arrInsData['latitude'] = $newData[0]['latitude'];
                $arrInsData['longitude'] = $newData[0]['longitude'];
            }

        }
        echo json_encode($arrInsData);
        exit;
    }

    function checkflightStatusAction()
    {
        $FLIGHT_STATUS = array(
            'A' => 'Active',
            'C' => 'Canceled',
            'D' => 'Diverted',
            'DN' => 'Data source needed',
            'L' => 'Landed',
            'NO' => 'Not Operational',
            'R' => 'Redirected',
            'S' => 'Scheduled',
            'U' => 'Unknown'
        );
        $FLIGHT_STATUS_CLASSES = array(
            'A' => 'f_active label-success',
            'C' => 'f_canceled label-danger',
            'D' => 'label f_diverted label-primary',
            'DN' => 'label F_data_source_needed label-warning',
            'L' => 'label f_active label-success',
            'NO' => 'label f_not_operational label-warning',
            'R' => 'label f_redirected label-info',
            'S' => 'label f_active label-success',
            'U' => 'label f_unknown label-danger'
        );
        if (isset($_POST) && !empty($_POST)) {
            $filght_status_app_id  = $this->generalvar['filght_status_app_id'];
            $filght_status_app_key = $this->generalvar['filght_status_app_key'];
            //$filght_status_app_id = "a48b6fbf";
            //$filght_status_app_key = "df4b13c21198ff5081a7b88295e8a15b";
            $apiUrl = 'https://api.flightstats.com/flex';
            $postData = explode('@@', $_POST['flightInfo']);
            //print_r($postData);exit;
            @$arrivalDateTime = strtotime($postData[5]);
            @$year = date('Y', $arrivalDateTime);
            @$month = date('m', $arrivalDateTime);
            @$day = date('d', $arrivalDateTime);
            @$carrier = $postData[0];
            @$number = $postData[1];
            @$distance = $postData[8];
            /* $flightRow['carrier'] = '9W';
			$flightRow['number'] = '355';*/
            @$departure = $postData[2];
            @$arrival = $postData[3];
            if (!empty($carrier) && !empty($number)) {
                $content = file_get_contents($apiUrl . "/flightstatus/rest/v2/json/flight/status/" . $carrier . "/" . $number . "/dep/" . $year . "/" . $month . "/" . $day . "?appId=" . $filght_status_app_id . "&appKey=" . $filght_status_app_key . "&utc=false&includeFlightPlan");
                $content = file_get_contents($apiUrl . "/flightstatus/rest/v2/json/flight/status/" . $carrier . "/" . $number . "/dep/" . $year . "/" . $month . "/" . $day . "?appId=70d5bc60&appKey=4c1e772560c4e1c05cff4438e062db9c&utc=false&includeFlightPlan");
                //print_r($content);exit;
                $obj = json_decode($content);
				
				$airportDetails = array();
				if(isset($obj->appendix->airports)){
                	$airportDetails = $obj->appendix->airports;//print_r($content);exit;
				}
                $flightStatuses = $obj->flightStatuses;
                $arrInsData = array();
                $arrInsData = array();

                if (!empty($airportDetails)) {
                    foreach ($airportDetails as $aDetails) {
                        if ($aDetails->iata == $departure) {
                            $arrInsData['orgin_locaion'] = $aDetails->name;
                            $arrInsData['orgin_city'] = $aDetails->city;
                            $arrInsData['orgin_state'] = $aDetails->city;
                            $arrInsData['orgin_country'] = $aDetails->countryName;
                        }
                        if ($aDetails->iata == $arrival) {
                            $arrInsData['destination_location'] = $aDetails->name;
                            $arrInsData['destination_city'] = $aDetails->city;
                            $arrInsData['destination_state'] = $aDetails->city;
                            $arrInsData['destination_country'] = $aDetails->countryName;
                        }
                    }
                }
                if (!empty($airportDetails)) {
                    foreach ($airportDetails as $aDetails) {
                        if ($aDetails->iata == $departure) {
                            $arrInsData['orgin_locaion'] = $aDetails->name;
                            $arrInsData['orgin_city'] = $aDetails->city;
                            $arrInsData['orgin_state'] = $aDetails->city;
                            $arrInsData['orgin_country'] = $aDetails->countryName;
                        }
                        if ($aDetails->iata == $arrival) {
                            $arrInsData['destination_location'] = $aDetails->name;
                            $arrInsData['destination_city'] = $aDetails->city;
                            $arrInsData['destination_state'] = $aDetails->city;
                            $arrInsData['destination_country'] = $aDetails->countryName;
                        }
                    }
                }
                if (isset($flightStatuses) && !empty($flightStatuses)) {
                    foreach ($flightStatuses as $fStatus) {
                        if (($fStatus->departureAirportFsCode == $departure) && ($fStatus->arrivalAirportFsCode == $arrival)) {
                            $operationalTimes = $fStatus->operationalTimes;
                            $arrInsData['flight_status'] = $fStatus->status;
                            $arrInsData['departure_scheduled_date'] = isset($operationalTimes->scheduledGateDeparture->dateUtc) ? $operationalTimes->scheduledGateDeparture->dateUtc : '';
                            $arrInsData['departure_acutal_date'] = isset($operationalTimes->actualGateDeparture->dateUtc) ? $operationalTimes->actualGateDeparture->dateUtc : '';
                            $arrInsData['departure_terminal'] = isset($fStatus->airportResources->departureTerminal) ? $fStatus->airportResources->departureTerminal : '';
                            $arrInsData['departure_delay'] = isset($fStatus->delays->departureGateDelayMinutes) ? $fStatus->delays->departureGateDelayMinutes : '';
                            $arrInsData['arrival_scheduled_date'] = isset($operationalTimes->scheduledGateArrival->dateUtc) ? $operationalTimes->scheduledGateArrival->dateUtc : '';
                            $arrInsData['arrival_actual_date'] = isset($operationalTimes->actualGateArrival->dateUtc) ? $operationalTimes->actualGateArrival->dateUtc : '';
                            $arrInsData['arrival_terminal'] = isset($fStatus->airportResources->arrivalTerminal) ? $fStatus->airportResources->arrivalTerminal : '';
                            $arrInsData['arrival_delay'] = isset($fStatus->delays->arrivalGateDelayMinutes) ? $fStatus->delays->arrivalGateDelayMinutes : '';
                            $arrInsData['departure_extimated_date'] = isset($operationalTimes->estimatedGateDeparture->dateUtc) ? $operationalTimes->estimatedGateDeparture->dateUtc : '';
                            $arrInsData['arrival_extimated_date'] = isset($operationalTimes->estimatedGateArrival->dateUtc) ? $operationalTimes->estimatedGateArrival->dateUtc : '';
                        }
                    }
                }
                $arrivalDateTime = strtotime($postData[4]);
                $year = date('Y', $arrivalDateTime);
                $month = date('m', $arrivalDateTime);
                $day = date('d', $arrivalDateTime);
                //$tractcontent    = file_get_contents($apiUrl . "/flightstatus/rest/v2/json/flight/tracks/" . $carrier . "/" . $number . "/dep/" . $year . "/" . $month . "/" . $day . "?appId=" . $filght_status_app_id . "&appKey=" . $filght_status_app_key . "&utc=false&includeFlightPlan");
                $tractcontent = file_get_contents($apiUrl . "/flightstatus/rest/v2/json/flight/tracks/" . $carrier . "/" . $number . "/dep/" . $year . "/" . $month . "/" . $day . "?appId=70d5bc60&appKey=4c1e772560c4e1c05cff4438e062db9c&utc=false&includeFlightPlan");
                $trackobj = json_decode($tractcontent);
                $flightTracks = $trackobj->flightTracks;
                if (isset($flightTracks) && !empty($flightTracks)) {
                    foreach ($flightTracks as $fTracks) {
                        if (($fTracks->departureAirportFsCode == $departure) && ($fTracks->arrivalAirportFsCode == $arrival)) {
                            if (isset($fTracks->positions[0])) {
                                $fTracksPosition = $fTracks->positions[0];
                                $arrInsData['speed'] = isset($fTracksPosition->speedMph) ? $fTracksPosition->speedMph : '';
                                $arrInsData['altitude'] = isset($fTracksPosition->altitudeFt) ? $fTracksPosition->altitudeFt : '';
                            }
                        }
                    }
                }
            }
        }
        $airline_name = $postData[6];
        $airline_number = $postData[7];
        $flightStatus = isset($arrInsData) ? $arrInsData : '';
        $flight_status = !empty($flightStatus['flight_status']) ? '<span class="' . $FLIGHT_STATUS_CLASSES[$flightStatus['flight_status']] . '">' . ucwords($FLIGHT_STATUS[$flightStatus['flight_status']]) . '</span>' : 'N/A';
        @$orgin_locaion = $flightStatus['orgin_locaion'] . ' - ' . $flightStatus['orgin_city'];
        @$destination_location = $flightStatus['destination_location'] . ' - ' . $flightStatus['destination_city'];
        $airline_number = $airline_name . ' - ' . $airline_number;
        $departure_scheduled_date = !empty($flightStatus['departure_scheduled_date']) ? $flightStatus['departure_scheduled_date'] : 'N/A';
        $arrival_scheduled_date = !empty($flightStatus['arrival_scheduled_date']) ? $flightStatus['arrival_scheduled_date'] : 'N/A';
        $departure_acutal_date = !empty($flightStatus['departure_acutal_date']) ? $flightStatus['departure_acutal_date'] : 'N/A';
        $arrival_acutal_date = !empty($flightStatus['arrival_actual_date']) ? $flightStatus['arrival_actual_date'] : 'N/A';
        $departure_terminal = !empty($flightStatus['departure_terminal']) ? $flightStatus['departure_terminal'] : 'N/A';
        $arrival_terminal = !empty($flightStatus['arrival_terminal']) ? $flightStatus['arrival_terminal'] : 'N/A';
        $departure_extimated_date = !empty($flightStatus['departure_extimated_date']) ? $flightStatus['departure_extimated_date'] : 'N/A';
        $arrival_extimated_date = !empty($flightStatus['arrival_extimated_date']) ? $flightStatus['arrival_extimated_date'] : 'N/A';
        $route = $departure . ' - ' . $arrival;
        $speed = !empty($flightStatus['speed']) ? $flightStatus['speed'] . ' Mph' : 'N/A';
        $altitude = !empty($flightStatus['altitude']) ? $flightStatus['altitude'] : 'N/A';
        if ($orgin_locaion != 'N/A') {
            $route = '<span style="color:#1D00FF">From: </span>' . $orgin_locaion . '(' . $departure . ') - <span style="color:#1D00FF">To: </span>' . $destination_location . '(' . $arrival . ')';
        }
        //print_r($airportDetails);
        $content = '<div class="row"><div class="col-md-12"><div class="form-view-section">    <div class="row">        <div class="form-group col-sm-4">            <label class="control-label">Status</label>            <p>' . $flight_status . '</p>        </div>        <div class="form-group col-sm-4">            <label class="control-label">Origin Airport</label>            <p>' . $orgin_locaion . '</p>        </div>     <div class="form-group col-sm-4">            <label class="control-label">Destination Airport</label>            <p>' . $destination_location . '</p>        </div>    </div>  <div class="row">        <div class="form-group col-sm-4">            <label class="control-label">Flight Name & Number</label>            <p>' . $airline_number . '</p>        </div>        <div class="form-group col-sm-4">            <label class="control-label">Scheduled Departure Date & Time</label>            <p>' . $departure_scheduled_date . '</p>        </div>     <div class="form-group col-sm-4">            <label class="control-label">Scheduled Arrival Date & Time</label>            <p>' . $arrival_scheduled_date . '</p>        </div>    </div>    <div class="row">             <div class="form-group col-sm-4">            <label class="control-label">Actual Departure Date & Time</label>            <p>' . $departure_acutal_date . '</p>        </div><div class="form-group col-sm-4">            <label class="control-label">Actual Arrival Date & Time</label>            <p>' . $arrival_acutal_date . '</p>        </div>    </div>     <div class="row">        <div class="form-group col-sm-4">            <label class="control-label">Estmated Departure Date  </label>            <p>' . $departure_extimated_date . '</p>        </div>        <div class="form-group col-sm-4">            <label class="control-label">Estmated Arrival Date </label>            <p>' . $arrival_extimated_date . '</p>        </div>          </div>    <div class="row">        <div class="form-group col-sm-4">            <label class="control-label">Departure Terminal </label>            <p>' . $departure_terminal . '</p>        </div>        <div class="form-group col-sm-4">            <label class="control-label">Arrival Terminal</label>            <p>' . $arrival_terminal . '</p>        </div>    </div>   <div class="row"> <div class="form-group col-sm-12">            <label class="control-label">Route</label>            <p>' . $route . '</p>        </div>         </div> <div class="row">        <div class="form-group col-sm-4">            <label class="control-label">Speed </label>            <p>' . $speed . '</p>        </div>        <div class="form-group col-sm-4">            <label class="control-label">Altitude</label>            <p>' . $altitude . '</p>        </div>        <div class="form-group col-sm-4">            <label class="control-label">Distance</label>            <p>' . round($distance) . ' Km</p>        </div>    </div>     </div>   <div class="row"> <div class="form-group col-sm-12">                     <p><a id="mapPopUp" href="#flight-position"              class="address_pop_up_add popup-map btn btn-green" data-effect="mfp-zoom-out" onclick="trackFlightPosition(\'' . $_POST['flightInfo'] . '\');"> <i class="fa fa-plus-circle"></i>Track Sky Map</a></p>        </div>         </div></div></div></div>';
        /*     <li> <a id="mapPopUp" href="#flight-position"              class="address_pop_up_add popup-map btn btn-green" data-effect="mfp-zoom-out" onclick="trackFlightPosition('<?php echo $flightStatus['ID']?>');">         <i class="fa fa-plus-circle"></i>Track Sky Map</a></li>*/
        echo $content;
        exit;
    }

    function getplandetailsAction()
    {
        $tripId = $_POST['id'];
        $userrole = $this->sessionObj->userrole;
        $trip = $this->TripModel->getTrip($tripId, $userrole);
        $airport_details = $this->TripModel->getFlightsDetails($trip['ID']);
        $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
        $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
        $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
        $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
        $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
        $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
        $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
        $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
        $origin_location_country_code = isset($origin_location['country_code']) ? $origin_location['country_code'] : '';
        $destination_location_country_code = isset($destination_location['country_code']) ? $destination_location['country_code'] : '';
        $arr_trip = array(
            'ID' => $this->encrypt($trip['ID']),
            'name' => $trip['name'],
            'user' => $trip['user'],
            'origin_location' => $trip['origin_location'],
            'destination_location' => $trip['destination_location'],
            'ticket_image' => $trip['ticket_image'],
            'trip_status' => $trip['trip_status'],
            'active' => $trip['active'],
            'distance' => $trip['distance'],
            'noofstops' => $trip['noofstops'],
            'origin_location_code' => $origin_location_code,
            'destination_location_code' => $destination_location_code,
            'origin_location_city' => $origin_location_city,
            'destination_location_city' => $destination_location_city,
            'origin_location_country' => $origin_location_country,
            'destination_location_country' => $destination_location_country,
            'origin_location_country_code' => $origin_location_country_code,
            'destination_location_country_code' => $destination_location_country_code,
            'airline_name' => $trip['airline_name'],
            'flight_number' => $trip['operating_carrier_name'] . $trip['flight_number'],
            'cabin' => $trip['cabin'],
            'booking_status' => $trip['booking_status'],
            'departure_date' => $trip['departure_date'],
            'arrival_date' => $trip['arrival_date'],
            'airport_details' => $airport_details,
            'modified' => $trip['modified'],
            'tripDetails' => $trip
        );
        $viewArray['trip'] = $arr_trip;
        $viewArray['currency'] = $this->CommonMethodsModel->getCurrency();
        $viewArray['comSessObj'] = $this->sessionObj;
        $result = new ViewModel();
        $result->setTerminal(true);
        $result->setVariables($viewArray);
        return $result;
    }

    public function saveWishlistAction()
    {
        extract($_POST);
        if ($this->sessionObj->userrole == 'traveller') {
            $tableName = 'traveller_wishlist';
        } else {
            $tableName = 'seeker_wishlist';
        }
        if (isset($action) && $action == 'delete') {
            $checkWishList = $this->TripModel->removewishlist($id, $tableName);
            $msg = 'Wishlist has been removed successfully.';
        } elseif (isset($userrole) && !empty($userrole)) {
            $checkWishList = $this->TripModel->checkwishExist($tripid, $tableName, $this->sessionObj->userid);
            if (empty($checkWishList)) {
                $insertData['user_id'] = $this->sessionObj->userid;
                $insertData['trip_id'] = $tripid;
                $insertData['trip_added'] = time();
                $checkWishList = $this->TripModel->AddWishlist($insertData, $tableName);
                $msg = 'This traveller is added to unplanned trip of your traveler\'s wishlist';
                if ($this->sessionObj->userrole == 'seeker') {
                    $sd = $this->TripModel->checkSeekerTripDetailsViaID($tripid);
                } else {
                    $sd = $this->TripModel->checkTripDetailsViaID($tripid);
                }
                $uD = $this->CommonMethodsModel->getUser($sd['user']);
                if (strtolower($uD['gender']) == 'male') {
                    $nname = 'Mr. ' . $uD['first_name'];

                } elseif (strtolower($uD['gender']) == 'female') {
                    $nname = 'Ms./Mrs. ' . $uD['first_name'];

                }


                $hugeData = array('user_id' => $this->sessionObj->userid,
                    'notification_type' => 'wishlist',
                    'notification_subject' => 'wishlist',
                    'notification_message' => "You have following " . $nname . " in Menu -> My Traveler Wish List. Please contact traveler anytime for your travel needs.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugeData);
            } else {
                $msg = 'This traveller is added to unplanned trip of your Menu -> My Traveler Wish List';
            }
        }
        echo json_encode(array(
            'msg_status' => 'success',
            'msg' => $msg
        ));
        exit;
    }

    public function sendreportmailAction()
    {
        extract($_POST);
        $userDetails = $this->sessionObj->user;
        $tripidnumber = $this->TripModel->getSeekerTrip($tripid);
        $reportdata = array(
            'report_username' => $userDetails['first_name'] . " " . $userDetails['last_name'],
            'report_tripid' => $tripidnumber['trip_id_number'],
            'report_message' => $report,
            'report_created' => time()
        );
        $this->CommonMethodsModel->addReport($reportdata);

        try
        {
            $this->MailModel->sendReportListMail($userDetails['first_name'], $userDetails['last_name'], $tripidnumber['trip_id_number'], $report);
        }
        catch (\Exception $e)
        {
            error_log($e);
        }

        echo 'Your report has been sent successfully.';
        exit;
    }

    public function sendmessagemailAction()
    {
        extract($_POST);
        $userDetails = $this->sessionObj->user;
        $tripidnumber = $this->TripModel->getSeekerTrip($tripid);
		$travellertripId = $this->decrypt($travellerTripId);
        $tripuserdetails = $this->CommonMethodsModel->getUser($tripidnumber['user']);
        $tripuserfullname = $tripuserdetails['first_name'] . " " . $tripuserdetails['last_name'];
        $fullname = $userDetails['first_name'] . " " . $userDetails['last_name'];
        $messagedata = array(
            'message_from' => $fullname,
            'message_to' => $tripuserfullname,
            'message_tripid' => $tripidnumber['trip_id_number'],
			'seeker_trip_id'=> $tripid,
            'message_subject' => $subject,
            'message_content' => $message,
            'message_created' => time()
        );
        $this->CommonMethodsModel->addMessage($messagedata);
		
		$hugeData = array
		(
			'recevier_id' => $tripidnumber['user'],
			'sender_id' => $userDetails['ID'],
			'seeker_trip_id'=> $tripid,
			'traveller_trip_id'=> $travellertripId,
			'message' => $message,
			'create_date' => date("Y-m-d H:i:s"),
			'modified_date' => date("Y-m-d H:i:s"),
			'active' => 1
		);
		$this->TripModel->addToSeekerEnquiry($hugeData);
		
		$hugedata1 = array
		(
			'user_id' => $tripidnumber['user'],
			'notification_type' => 'seeker_inquires',
			'notification_subject' => 'Seeker Inquired',
			'notification_message' => 'Seeker service Inquired. Message: '.$message,
			'app_redirect' => '/traveller/detail/'.$travellertripId.'?service='.$service.'&tID='.$travellertripId.'&sID='.$tripid.'&from=inquired',
			'notification_added' => date("Y-m-d H:i:s")
		);
		$this->TripModel->addToNotifications($hugedata1);

        try
        {
            $this->MailModel->sendMemberMail($fullname, $tripuserfullname, $tripuserdetails['email_address'], $tripidnumber['trip_id_number'], $subject, $message, $userDetails['email_address'], 'TRAVELLER');
        }
        catch (\Exception $e)
        {
            error_log($e);
        }

        echo 'Your message sent successfully.';
        exit;
    }

    public function getparticulertripfeeAction()
    {
        if (isset($_POST)) {
            $id = $_POST['id'];
            $fee = $this->CommonMethodsModel->getParticulerTripFee($id, $this->sessionObj->userrole);
            echo $fee['people_service_fee'];
            exit;
        }
    }

    public function changeTripStatusAction()
    {
        // print_r($_POST);exit();
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $service = $this->CommonMethodsModel->cleanQuery($_POST['service']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        $this->TripModel->changeTripStatusBySeeker($requestid, $status, $service);
        echo 1;
        exit();
    }

    public function paypalpeopleSuccessPaymentAction()
    {
        //print_r($_POST);die;
        //echo "in paypal succes payment action seeker controller";exit;
        $this->TripPlugin = $this->plugin('TripPlugin');
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        $traveller_people_request = $this->TripModel->getTravellerPeopleRequestUser($requestid);
        $seeker_trip_user = $this->TripModel->getSeekerTripUser($tripid);
        $peopleservice = $this->TripModel->getSeekerTripPeople($tripid);
        $transactionid = $this->CommonMethodsModel->cleanQuery($_POST['paymentID']);
        $payerid = $this->CommonMethodsModel->cleanQuery($_POST['payerID']);
        $param = array(
            'pay_user' => $this->sessionObj->userid,
            'pay_to_user' => $traveller_people_request['ID'],
            'pay_trip_table' => 'seeker_trips',
            'pay_trip_id' => $tripid,
            'pay_to_trip_id' => $traveller_people_request['tripId'],
            'pay_user_type' => 'seeker',
            'pay_service' => 'people',
            'pay_service_id' => $traveller_people_request['serviceId'],
            'pay_request_id' => $requestid,
            'pay_card_type' => 'PayPal',
            'pay_type' => 'Hold',
            'pay_amount' => $peopleservice['people_service_fee'],
            'pay_currency' => 'USD',
            'pay_trans_id' => $transactionid,
            'pay_cor_id' => $payerid,
            'pay_status' => $status,
            'pay_added' => time()

        );
        $pay_id = $this->TripModel->updatePaymants($param);
        //echo $pay_id;die;
        if ($pay_id != 0) {
            $this->TripModel->approveTravellerPeopleRequest($tripid, $requestid, $status);
            $msg_status = 'success';
            $pay_id = $pay_id;
            $msg = $trip_people_request['first_name'] . " " . $trip_people_request['last_name'] . ' has been approved for your people service request';
            $s = $this->TripModel->checkSeekerTripDetailsViaID($tripid);
            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
            $tos = $_POST['requestService'];

            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }

            if ($tos == 'people') {
                $message_for_user = "For the people companionship service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else if ($tos == 'package') {
                $message_for_user = "For the package delivery service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else {
                $message_for_user = "For the product procurement service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
            }

            $hugeData = array('user_id' => $note_reciever_id,
                'message' => $message_for_user,
                'msg_type' => 'm',
                'create_date' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToUserMessages($hugeData);
            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_accept',
                'notification_subject' => 'Request Accept',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted people service request. Please contact him/her for further coordination.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
            //here
        } else {
            $msg_status = 'failed';
            $msg = $trip_people_request['first_name'] . " " . $trip_people_request['last_name'] . ' has not been approved, due to payment authorization failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
        }
        echo json_encode(array(
            'msg_status' => $msg_status,
            'msg' => $msg,
            'pay_id' => $this->encrypt($pay_id)
        ));
        exit;


    }

    public function paypalpackageSuccessPaymentAction()
    {
        //print_r($_POST);die;
        //echo "in paypal succes payment action seeker controller";exit;
        $this->TripPlugin = $this->plugin('TripPlugin');
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        $traveller_package_request = $this->TripModel->getTravellerPackageRequestUser($requestid);
        $seeker_trip_user = $this->TripModel->getSeekerTripUser($tripid);
        $packageservice = $this->TripModel->getSeekerTripPackage($tripid);
        $transactionid = $this->CommonMethodsModel->cleanQuery($_POST['paymentID']);
        $payerid = $this->CommonMethodsModel->cleanQuery($_POST['payerID']);
        $param = array(
            'pay_user' => $this->sessionObj->userid,
            'pay_to_user' => $traveller_package_request['ID'],
            'pay_trip_table' => 'seeker_trips',
            'pay_trip_id' => $tripid,
            'pay_to_trip_id' => $traveller_package_request['tripId'],
            'pay_user_type' => 'seeker',
            'pay_service' => 'package',
            'pay_service_id' => $traveller_package_request['serviceId'],
            'pay_request_id' => $requestid,
            'pay_card_type' => 'PayPal',
            'pay_type' => 'Hold',
            'pay_amount' => $packageservice['package_service_fee'],
            'pay_currency' => 'USD',
            'pay_trans_id' => $transactionid,
            'pay_cor_id' => $payerid,
            'pay_status' => $status,
            'pay_added' => time()

        );
        $pay_id = $this->TripModel->updatePaymants($param);

        if ($pay_id != 0) {
            $this->TripModel->approveTravellerPackageRequest($tripid, $requestid, $status);
            $msg_status = 'success';
            $pay_id = $pay_id;
            $msg = $trip_package_request['first_name'] . " " . $trip_package_request['last_name'] . ' has been approved for your package service request';
            $s = $this->TripModel->checkSeekerTripDetailsViaID($tripid);
            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
            $tos = $_POST['requestService'];

            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }

            if ($tos == 'people') {
                $message_for_user = "For the people companionship service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else if ($tos == 'package') {
                $message_for_user = "For the package delivery service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else {
                $message_for_user = "For the product procurement service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
            }

            $hugeData = array('user_id' => $note_reciever_id,
                'message' => $message_for_user,
                'msg_type' => 'm',
                'create_date' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToUserMessages($hugeData);
            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_accept',
                'notification_subject' => 'Request Accept',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted package service request. Please contact him/her for further coordination.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
        } else {
            $msg_status = 'failed';
            $msg = $trip_package_request['first_name'] . " " . $trip_package_request['last_name'] . ' has not been approved, due to payment authorization failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
        }
        echo json_encode(array(
            'msg_status' => $msg_status,
            'msg' => $msg,
            'pay_id' => $this->encrypt($pay_id)
        ));
        exit;


    }

    public function paypalprojectSuccessPaymentAction()
    {
        //print_r($_POST);die;
        //echo "in paypal succes payment action seeker controller";exit;
        $this->TripPlugin = $this->plugin('TripPlugin');
        $tripid = $this->CommonMethodsModel->cleanQuery($this->decrypt($_POST['tripid']));
        $requestid = $this->CommonMethodsModel->cleanQuery($_POST['requestid']);
        $status = $this->CommonMethodsModel->cleanQuery($_POST['status']);
        $traveller_project_request = $this->TripModel->getTravellerProjectRequestUser($requestid);
        $seeker_trip_user = $this->TripModel->getSeekerTripUser($tripid);
        $projectservice = $this->TripModel->getSeekerTripProject($tripid);
        $transactionid = $this->CommonMethodsModel->cleanQuery($_POST['paymentID']);
        $payerid = $this->CommonMethodsModel->cleanQuery($_POST['payerID']);
        $param = array(
            'pay_user' => $this->sessionObj->userid,
            'pay_to_user' => $traveller_project_request['ID'],
            'pay_trip_table' => 'seeker_trips',
            'pay_trip_id' => $tripid,
            'pay_to_trip_id' => $traveller_project_request['tripId'],
            'pay_user_type' => 'seeker',
            'pay_service' => 'project',
            'pay_service_id' => $traveller_project_request['serviceId'],
            'pay_request_id' => $requestid,
            'pay_card_type' => 'PayPal',
            'pay_type' => 'Hold',
            'pay_amount' => $projectservice['project_service_fee'],
            'pay_currency' => 'USD',
            'pay_trans_id' => $transactionid,
            'pay_cor_id' => $payerid,
            'pay_status' => $status,
            'pay_added' => time()

        );

        $pay_id = $this->TripModel->updatePaymants($param);

        if ($pay_id != 0) {
            $this->TripModel->approveTravellerProjectRequest($tripid, $requestid, $status);
            $msg_status = 'success';
            $pay_id = $pay_id;
            $msg = $trip_project_request['first_name'] . " " . $trip_project_request['last_name'] . ' has been approved for your product service request';
            $s = $this->TripModel->checkSeekerTripDetailsViaID($tripid);
            //print_r($s);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
            $tos = $_POST['requestService'];

            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
                $him_her = 'him';
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
                $him_her = 'her';
            }

            if ($tos == 'people') {
                $message_for_user = "For the people companionship service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else if ($tos == 'package') {
                $message_for_user = "For the package delivery service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
            } else {
                $message_for_user = "For the product procurement service request you sent to " . $nname . " (Seeker) has accepted the request. Please contact " . $him_her . " for further coordination.";
            }

            $hugeData = array('user_id' => $note_reciever_id,
                'message' => $message_for_user,
                'msg_type' => 'm',
                'create_date' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToUserMessages($hugeData);
            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'request_accept',
                'notification_subject' => 'Request Accept',
                'notification_message' => " For the " . $note_origin . " to " . $note_destination . ", " . $nname . " has accepted product service request. Please contact him/her for further coordination.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
        } else {
            $msg_status = 'failed';
            $msg = $trip_project_request['first_name'] . " " . $trip_project_request['last_name'] . ' has not been approved, due to payment authorization failure. Please verify your payment method and try again.<br/> ' . $response["L_LONGMESSAGE0"];
        }
        echo json_encode(array(
            'msg_status' => $msg_status,
            'msg' => $msg,
            'pay_id' => $this->encrypt($pay_id)
        ));
        exit;


    }

    public function addSeekerTripRequestByPaypalAction()
    {
        // print_r($_POST);die;
        //echo "in paypal succes payment action seeker controller";exit;
        $people = $this->CommonMethodsModel->cleanQuery($_POST['seeker_people_request_id']);
        $package = $this->CommonMethodsModel->cleanQuery($_POST['seeker_package_request_id']);
        $project = $this->CommonMethodsModel->cleanQuery($_POST['seeker_project_request_id']);
        if ($people != '') {
            $transactionid = $this->CommonMethodsModel->cleanQuery($_POST['pay_tran_id']);
            $payerid = $this->CommonMethodsModel->cleanQuery($_POST['payerID']);
            $tripid = $this->CommonMethodsModel->cleanQuery($_POST['traveller_trip_id']);
            $service = $this->TripModel->getTravellerPeople($tripid);
            $requestid = $this->CommonMethodsModel->cleanQuery($_POST['seeker_people_request_id']);
            $serviceid = $this->CommonMethodsModel->cleanQuery($_POST['seeker_people_service_id']);
            //$traveller_package_request = $this->TripModel->getTravellerPackageRequestUser($requestid);
            $seeker_trip = $this->TripModel->getTrip($tripid);
            $traveller_people = $this->TripModel->getSeekerPeopleByID($requestid);


            $tr_param = array(
                'pay_user' => $this->sessionObj->userid,
                'pay_to_user' => $seeker_trip['user'],
                'pay_trip_table' => 'trips',
                'pay_trip_id' => $tripid,
                'pay_to_trip_id' => $traveller_people['seeker_trip'],
                'pay_user_type' => 'seeker',
                'pay_service' => 'people',
                'pay_service_id' => $serviceid,
                'pay_request_id' => $requestid,
                'pay_card_type' => 'PayPal',
                'pay_type' => 'Hold',
                'pay_amount' => $traveller_people['people_service_fee'],
                'pay_currency' => 'USD',
                'pay_trans_id' => $transactionid,
                'pay_cor_id' => $payerid,
                'pay_status' => '1',
                'pay_added' => time()
            );
            $tr_response = $this->TripModel->updatePaymants($tr_param);
            $trip_seeker_people_request = array(
                'trip' => $tripid,
                'people_request' => $requestid,
                'service_id' => $serviceid,
                'card_id' => 0,
                'approved' => 0,
                'modified' => date('Y-m-d H:i:s')
            );
            $this->TripModel->addTripSeekerPeopleRequest($trip_seeker_people_request);
            $s = $this->TripModel->checkSeekerTripDetailsViaID($tripid);
            //print_r($_POST);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
            $tos = $_POST['type_of_service'];

            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
            }


            $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for people companionship, " . $nname . " (Seeker) has sent you people service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours.";

            $hugeData = array('recevier_id' => $note_reciever_id,
                'sender_id' => $note_sender_id,
                'message' => $message_for_user,
                'create_date' => date("Y-m-d H:i:s"),
                'modified_date' => date("Y-m-d H:i:s"),
                'active' => 1);
            $this->TripModel->addToSeekerEnquiry($hugeData);
            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'seeker_request',
                'notification_subject' => 'Request Send',
                'notification_message' => "For the trip from " . $note_origin . " to " . $note_destination . ", " . $nname . "  has sent you people service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
            echo "Your request has been sent successfully";
            exit;
        } else if ($package != '') {
            $transactionid = $this->CommonMethodsModel->cleanQuery($_POST['pay_tran_id']);
            $payerid = $this->CommonMethodsModel->cleanQuery($_POST['payerID']);
            $tripid = $this->CommonMethodsModel->cleanQuery($_POST['traveller_trip_id']);
            $service = $this->TripModel->getTravellerPackage($tripid);
            $requestid = $this->CommonMethodsModel->cleanQuery($_POST['seeker_package_request_id']);
            $serviceid = $this->CommonMethodsModel->cleanQuery($_POST['seeker_package_service_id']);
            //$traveller_package_request = $this->TripModel->getTravellerPackageRequestUser($requestid);
            $seeker_trip = $this->TripModel->getTrip($tripid);
            $traveller_package = $this->TripModel->getSeekerPackageByID($requestid);


            $tr_param = array(
                'pay_user' => $this->sessionObj->userid,
                'pay_to_user' => $seeker_trip['user'],
                'pay_trip_table' => 'trips',
                'pay_trip_id' => $tripid,
                'pay_to_trip_id' => $traveller_package['seeker_trip'],
                'pay_user_type' => 'seeker',
                'pay_service' => 'package',
                'pay_service_id' => $serviceid,
                'pay_request_id' => $requestid,
                'pay_card_type' => 'PayPal',
                'pay_type' => 'Hold',
                'pay_amount' => $traveller_package['package_service_fee'],
                'pay_currency' => 'USD',
                'pay_trans_id' => $transactionid,
                'pay_cor_id' => $payerid,
                'pay_status' => '1',
                'pay_added' => time()
            );
            $tr_response = $this->TripModel->updatePaymants($tr_param);
            $trip_seeker_package_request = array(
                'trip' => $tripid,
                'package_request' => $requestid,
                'traveller_package_amount' => $traveller_package['package_service_fee'],
                'service_id' => $serviceid,
                'card_id' => 0,
                'approved' => 0,
                'modified' => date('Y-m-d H:i:s')
            );
            $this->TripModel->addTripSeekerPackageRequest($trip_seeker_package_request);
            $s = $this->TripModel->checkSeekerTripDetailsViaID($tripid);
            //print_r($_POST);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
            $tos = $_POST['type_of_service'];

            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
            }


            $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for package delivery, " . $nname . " (Seeker) has sent you package service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours.";
            $hugeData = array('recevier_id' => $note_reciever_id,
                'sender_id' => $note_sender_id,
                'message' => $message_for_user,
                'create_date' => date("Y-m-d H:i:s"),
                'modified_date' => date("Y-m-d H:i:s"),
                'active' => 1);
            $this->TripModel->addToSeekerEnquiry($hugeData);
            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'seeker_request',
                'notification_subject' => 'Request Send',
                'notification_message' => "For the trip from " . $note_origin . " to " . $note_destination . ", " . $nname . "  has sent you package service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
            echo "Your request has been sent successfully";
            exit;
        } else if ($project != '') {
            $transactionid = $this->CommonMethodsModel->cleanQuery($_POST['pay_tran_id']);
            $payerid = $this->CommonMethodsModel->cleanQuery($_POST['payerID']);
            $tripid = $this->CommonMethodsModel->cleanQuery($_POST['traveller_trip_id']);
            $service = $this->TripModel->getTravellerProject($tripid);
            $requestid = $this->CommonMethodsModel->cleanQuery($_POST['seeker_project_request_id']);
            $serviceid = $this->CommonMethodsModel->cleanQuery($_POST['seeker_project_service_id']);
            //$traveller_package_request = $this->TripModel->getTravellerPackageRequestUser($requestid);
            $seeker_trip = $this->TripModel->getTrip($tripid);
            $traveller_project = $this->TripModel->getSeekerProjectByID($requestid);


            $tr_param = array(
                'pay_user' => $this->sessionObj->userid,
                'pay_to_user' => $seeker_trip['user'],
                'pay_trip_table' => 'trips',
                'pay_trip_id' => $tripid,
                'pay_to_trip_id' => $traveller_project['seeker_trip'],
                'pay_user_type' => 'seeker',
                'pay_service' => 'project',
                'pay_service_id' => $serviceid,
                'pay_request_id' => $requestid,
                'pay_card_type' => 'PayPal',
                'pay_type' => 'Hold',
                'pay_amount' => $traveller_project['project_service_fee'],
                'pay_currency' => 'USD',
                'pay_trans_id' => $transactionid,
                'pay_cor_id' => $payerid,
                'pay_status' => '1',
                'pay_added' => time()
            );
            $tr_response = $this->TripModel->updatePaymants($tr_param);
            $trip_seeker_project_request = array(
                'trip' => $tripid,
                'project_request' => $requestid,
                'service_id' => $serviceid,
                'card_id' => 0,
                'approved' => 0,
                'modified' => date('Y-m-d H:i:s')
            );
            $this->TripModel->addTripSeekerProjectRequest($trip_seeker_project_request);
            $s = $this->TripModel->checkSeekerTripDetailsViaID($tripid);
            //print_r($_POST);die;
            $note_origin_all = $this->TripModel->getLocationDetails($s['origin_location']);
            $note_destination_all = $this->TripModel->getLocationDetails($s['destination_location']);
            $note_reciever_id = $s['user'];
            $note_sender_id = $this->sessionObj->userid;
            $note_origin = '(' . $note_origin_all['city_code'] . ') ' . $note_origin_all['city'] . ', ' . $note_origin_all['country'];
            $note_destination = '(' . $note_destination_all['city_code'] . ') ' . $note_destination_all['city'] . ', ' . $note_destination_all['country'];
            $tos = $_POST['type_of_service'];

            $uD = $this->CommonMethodsModel->getUser($note_sender_id);
            if (strtolower($uD['gender']) == 'male') {
                $nname = 'Mr. ' . $uD['first_name'];
            } elseif (strtolower($uD['gender']) == 'female') {
                $nname = 'Ms./Mrs. ' . $uD['first_name'];
            }


            $message_for_user = "For the listing you created from " . $note_origin . " to " . $note_destination . " for product procurement, " . $nname . " (Seeker) has sent you product service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours.";
            $hugeData = array('recevier_id' => $note_reciever_id,
                'sender_id' => $note_sender_id,
                'message' => $message_for_user,
                'create_date' => date("Y-m-d H:i:s"),
                'modified_date' => date("Y-m-d H:i:s"),
                'active' => 1);
            $this->TripModel->addToSeekerEnquiry($hugeData);
            $hugedata1 = array('user_id' => $note_reciever_id,
                'notification_type' => 'seeker_request',
                'notification_subject' => 'Request Send',
                'notification_message' => "For the trip from " . $note_origin . " to " . $note_destination . ", " . $nname . "  has sent you product service request. Please check Menu -> Seeker Requests page and respond to the request within 24 hours.",
                'notification_added' => date("Y-m-d H:i:s")
            );
            $this->TripModel->addToNotifications($hugedata1);
            echo "Your request has been sent successfully";
            exit;
        }

    }
    
     public function checkcouponAction()
       { 

        $coupon_details = $this->CommonMethodsModel->getCoupon($_REQUEST['coupon_code']);
        
        if ($coupon_details) {
            echo $coupon_details['offer_percentage'];
            exit();
        } else {
            echo 0;
            exit();
        }
    }

    public function checkRewardsAction()
    {

        $reward_details = $this->CommonMethodsModel->getRewards($_REQUEST['user_id']);

        if ($reward_details) {
            echo $reward_details['balance'];
            exit();
        } else {
            echo 0;
            exit();
        }
    }

}