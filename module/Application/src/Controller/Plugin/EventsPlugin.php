<?php

class Zend_Controller_Action_Helper_Events extends Zend_Controller_Action_Helper_Abstract{

public function init() {
$this->view = $this->getActionController()->view;
$this->comfunc_mod = new Application_Model_DbTable_Commonfunctions();
$this->authNamespace = new Container('maestro_Auth');
$this->comSessObj = new Container('objsess');
$this->adminid = $this->authNamespace->adminid;

$this->controller = $this->getRequest()->getControllerName();
$this->dirpath = str_replace('index.php','theme/',$_SERVER['SCRIPT_FILENAME']);

$config_params = Zend_Registry::get('config');
$this->generalvar = $config_params['general'];
$this->events_mod = new Application_Model_DbTable_Events();
}

protected function _redirect($url, array $options = array()) {
$redirector = $this->_actionController->getHelper('Redirector');
$redirector->gotoUrl($url);
}

public function getEvents($arr_filters = array(),$all_events='no'){
$site = $this->generalvar['visitaltonsiteid'];        
$site_category = $this->generalvar['event']['category']['sitecatid'];
$event_module_type = $this->generalvar['event']['event']['typeid'];
$all_events = 'yes';  // Bcos, events having instance of date, so we need to filter paging manually by here
$arr_events = array();
$arr_every_occur = array();
//echo '<pre>'; print_r($arr_filters); exit;
$events = $this->events_mod->getEvents($site,$event_module_type,'',$start=0,$limit=0,'b.start_date','ASC',$site_category,$arr_filters,$all_events);
$current_time = date('Y-m-d H:i:s');
if(count($events)>0){
foreach($events as $event){
$more_date = $this->events_mod->getMoreDate($event['ID']);
$image = $this->comfunc_mod->getPrimaryImage($event['ID'],$event_module_type);
if($image['image'])
$image = $this->comfunc_mod->splitImages($image['image']);
            
            /*
            $categories = $this->events_mod->getEventCategories($site_category,$event_module_type,$event['ID']);
            $arr_categories = '';
            foreach($categories as $category){
                $arr_categories[] = array('ID' => $category['ID'],'name' => $category['name']);
            }

            $todays_events = $this->events_mod->getEventsByDateDuration('today',$event_module_type,$event['ID']);
            if(count($todays_events)>0)
            $todays_event = 1;
            else
            $todays_event = 0;


            $weekend_events = $this->events_mod->getEventsByDateDuration('weekend',$event_module_type,$event['ID']);
            if(count($weekend_events)>0)
            $weekend_event = 1;
            else
            $weekend_event = 0;
            */

$nextsevenday_events = $this->events_mod->getEventsByDateDuration('nextsevenday',$event_module_type,$event['ID']);
if(count($nextsevenday_events)>0)
$nextsevenday_events = 1;
else
$nextsevenday_events = 0;

if($event['instance']==1){
$every_occurence = $this->events_mod->getEveryOccurence($event['ID'],$event['instance'],$arr_filters);   
if(count($every_occurence)>0){
foreach($every_occurence as $every_occur) {
if($every_occur['end_date']>$current_time) {        
$arr_every_occur[] = array(
'ID' => $every_occur['ID'],
'title' => $every_occur['title'],
'start_date' => $every_occur['start_date'],
'end_date' => $every_occur['end_date'],
'month' => $this->comfunc_mod->timeZoneAdjust('M',$every_occur['start_date']),
'date' => $this->comfunc_mod->timeZoneAdjust('d',$every_occur['start_date']),
'year' => $this->comfunc_mod->timeZoneAdjust('Y',$every_occur['start_date']),
'start_time' => $this->comfunc_mod->timeZoneAdjust('ga',$every_occur['start_date']),
'end_time' => $this->comfunc_mod->timeZoneAdjust('ga T',$every_occur['start_date']),
'more_date' => $more_date['more_date'],        
'venue_name' => $event['venue_name'],
'street_address_1' => $every_occur['street_address_1'],
'street_address_2' => $every_occur['street_address_2'],
'city' => $every_occur['city'],
'city_name' => $every_occur['city_name'],
'state_code' => $every_occur['state_code'],
'zip_code' => $every_occur['zip_code'],
'image' => $image,
/*
'categories'        => $arr_categories,
'todays_event'      => $todays_event,
'weekend_event'     => $weekend_event,
'nextsevenday_event'=> $nextsevenday_events,
* 
*/
'imagepath' => $this->generalvar['event']['event']['images'],
'moduledatesid'=>$every_occur['moduledatesid'], 
'ticket_website' => $event['ticket_website']
);
}
}
}
} else {
if($event['end_date']>$current_time){        
$every_occurence = $this->events_mod->getEveryOccurence($event['ID'],$event['instance']);
$arr_events[] = array(
'ID' => $event['ID'],
'title'  => $event['title'],
'start_date' => $event['start_date'],
'end_date' => $event['end_date'],
'month' => $this->comfunc_mod->timeZoneAdjust('M',$event['start_date']),
'date' => $this->comfunc_mod->timeZoneAdjust('d',$event['start_date']),
'year' => $this->comfunc_mod->timeZoneAdjust('Y',$event['start_date']),
'start_time' => $this->comfunc_mod->timeZoneAdjust('ga',$event['start_date']),
'end_time' => $this->comfunc_mod->timeZoneAdjust('ga T',$event['start_date']),
'more_date' => $more_date['more_date'],    
'venue_name' => $event['venue_name'],
'street_address_1' => $event['street_address_1'],
'street_address_2' => $event['street_address_2'],
'city' => $event['city'],
'city_name' => $event['city_name'],
'state_code' => $event['state_code'],
'zip_code' => $event['zip_code'],
'image'  => $image,
/*
'categories'        => $arr_categories,
'todays_event'      => $todays_event,
'weekend_event'     => $weekend_event,
'nextsevenday_event'=> $nextsevenday_events,
* 
*/
'imagepath' => $this->generalvar['event']['event']['images'],
'moduledatesid'=>$every_occurence[0]['moduledatesid'],
'ticket_website' => $event['ticket_website']
);
}
}
}
}

$merge_events = @array_merge($arr_events,$arr_every_occur);
if($arr_filters['filter_sort_by']=="event_name_Z_A") {
$filter = 'title'; $order = 'DESC';
} else if($arr_filters['filter_sort_by']=="event_name_A_Z") {
$filter = 'title';  $order = 'ASC';
} else {
$filter = 'start_date';  $order = 'ASC';
}
$merge_events = $this->comfunc_mod->aasort($merge_events,$filter,$order);
return $merge_events;
}

public function getHomeEvents($arr_filters = array(),$all_events='no'){
$site = $this->generalvar['visitaltonsiteid'];        
$site_category = $this->generalvar['event']['category']['sitecatid'];
$event_module_type = $this->generalvar['event']['event']['typeid'];
$all_events = 'yes';  // Bcos, events having instance of date, so we need to filter paging manually by here
$arr_events = array();
$arr_every_occur = array();
//echo '<pre>'; print_r($arr_filters); exit;
$events = $this->events_mod->getHomeEvents($site,$event_module_type,'',$start=0,$limit=0,'b.start_date','ASC',$site_category,$arr_filters,$all_events);
$current_time = date('Y-m-d H:i:s');
if(count($events)>0){
foreach($events as $event){
$more_date = $this->events_mod->getMoreDate($event['ID']);
$image = $this->comfunc_mod->getPrimaryImage($event['ID'],$event_module_type);
if($image['image'])
$image = $this->comfunc_mod->splitImages($image['image']);
$nextsevenday_events = $this->events_mod->getEventsByDateDuration('nextsevenday',$event_module_type,$event['ID']);
if(count($nextsevenday_events)>0)
$nextsevenday_events = 1;
else
$nextsevenday_events = 0;

if($event['instance']==1){
$every_occurence = $this->events_mod->getEveryOccurence($event['ID'],$event['instance'],$arr_filters);   
if(count($every_occurence)>0){
foreach($every_occurence as $every_occur) {
if($every_occur['end_date']>$current_time) {        
$arr_every_occur[] = array(
'ID' => $every_occur['ID'],
'title' => $every_occur['title'],
'start_date' => $every_occur['start_date'],
'end_date' => $every_occur['end_date'],
'month' => $this->comfunc_mod->timeZoneAdjust('M',$every_occur['start_date']),
'date' => $this->comfunc_mod->timeZoneAdjust('d',$every_occur['start_date']),
'year' => $this->comfunc_mod->timeZoneAdjust('Y',$every_occur['start_date']),
'start_time' => $this->comfunc_mod->timeZoneAdjust('ga',$every_occur['start_date']),
'end_time' => $this->comfunc_mod->timeZoneAdjust('ga T',$every_occur['start_date']),
'more_date' => $more_date['more_date'],        
'venue_name' => $this->comfunc_mod->trimLength($event['venue_name'],30),
'street_address_1' => $every_occur['street_address_1'],
'street_address_2' => $every_occur['street_address_2'],
'city' => $every_occur['city'],
'city_name' => $every_occur['city_name'],
'state_code' => $every_occur['state_code'],
'zip_code' => $every_occur['zip_code'],
'image' => $image,
/*
'categories'        => $arr_categories,
'todays_event'      => $todays_event,
'weekend_event'     => $weekend_event,
'nextsevenday_event'=> $nextsevenday_events,
* 
*/
'imagepath' => $this->generalvar['event']['event']['images'],
'moduledatesid'=>$every_occur['moduledatesid'], 
'ticket_website' => $event['ticket_website']
);
}
}
}
} else {
if($event['end_date']>$current_time){        
$every_occurence = $this->events_mod->getEveryOccurence($event['ID'],$event['instance']);
$arr_events[] = array(
'ID' => $event['ID'],
'title'  => $event['title'],
'start_date' => $event['start_date'],
'end_date' => $event['end_date'],
'month' => $this->comfunc_mod->timeZoneAdjust('M',$event['start_date']),
'date' => $this->comfunc_mod->timeZoneAdjust('d',$event['start_date']),
'year' => $this->comfunc_mod->timeZoneAdjust('Y',$event['start_date']),
'start_time' => $this->comfunc_mod->timeZoneAdjust('ga',$event['start_date']),
'end_time' => $this->comfunc_mod->timeZoneAdjust('ga T',$event['start_date']),
'more_date' => $more_date['more_date'],    
'venue_name' => $this->comfunc_mod->trimLength($event['venue_name'],30),
'street_address_1' => $event['street_address_1'],
'street_address_2' => $event['street_address_2'],
'city' => $event['city'],
'city_name' => $event['city_name'],
'state_code' => $event['state_code'],
'zip_code' => $event['zip_code'],
'image'  => $image,
/*
'categories'        => $arr_categories,
'todays_event'      => $todays_event,
'weekend_event'     => $weekend_event,
'nextsevenday_event'=> $nextsevenday_events,
* 
*/
'imagepath' => $this->generalvar['event']['event']['images'],
'moduledatesid'=>$every_occurence[0]['moduledatesid'],
'ticket_website' => $event['ticket_website']
);
}
}
}
}

$merge_events = @array_merge($arr_events,$arr_every_occur);
if($arr_filters['filter_sort_by']=="event_name_Z_A") {
$filter = 'title'; $order = 'DESC';
} else if($arr_filters['filter_sort_by']=="event_name_A_Z") {
$filter = 'title';  $order = 'ASC';
} else {
$filter = 'start_date';  $order = 'ASC';
}
$merge_events = $this->comfunc_mod->aasort($merge_events,$filter,$order);
return $merge_events;
}

}

?>