<?php
namespace Application\Controller\Plugin\Factory;

//related controller
use Application\Controller\Plugin\GoogleplusPlugin;
 
//models to load
use Application\Model\CommonMethodsModel;
use Application\Model\MailModel;

//configuration classes
use Interop\Container\ContainerInterface;
use Zend\Mvc\Controller\Plugin\PluginInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class GoogleplusPluginFactory implements PluginInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        echo 'hrete';exit;
        $dbAdapter = $container->get('db_adapter');
        $controllerData['configs'] = $container->get('config');
        $controllerData['models'] = array(
            array('name' => 'CommonMethodsModel', 'obj' => new CommonMethodsModel($dbAdapter)),
            array('name' => 'MailModel', 'obj' => new MailModel($dbAdapter,$controllerData['configs']))
        );
        $controllerData['configs'] = $container->get('config');
        return new GoogleplusPlugin($controllerData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        return $this($container, $requestedName, []);
    }
}