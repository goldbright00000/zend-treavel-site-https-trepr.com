<?php
namespace Application\Controller\Plugin\Factory;

use Application\Controller\Plugin\TripPlugin;

/*models to load*/
use Application\Model\CommonMethodsModel;
use Application\Model\TripModel;
use Application\Model\MailModel;

/*configuration classes*/
use Interop\Container\ContainerInterface;
/*use Zend\Mvc\Controller\Plugin\PluginInterface;*/
use Zend\ServiceManager\ServiceLocatorInterface;

use Zend\ServiceManager\Factory\FactoryInterface;

class TripPluginFactory implements FactoryInterface {
    
    var $CommonMethodsModel;
    var $TripModel;
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
        $pluginData['configs'] = $container->get('config');
        $pluginData['models'] = array(
            array('name' => 'CommonMethodsModel', 'obj' => new CommonMethodsModel($dbAdapter, $pluginData['configs'])),
            array('name' => 'TripModel', 'obj' => new TripModel($dbAdapter, $pluginData['configs']))
        );
        $pluginData['configs'] = $container->get('config');
        return new TripPlugin($pluginData);
    }
    
       
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null){
        return $this($container, $requestedName, []);
    }
    
    /* Trip Plugin Functions Start*/
    
    public function addTripOrginLocation() {
        extract($_POST);
        $contact_address_type = $this->CommonMethodsModel->cleanQuery($_POST['contact_address_type']);
        if ($contact_address_type == 'existing_address')
            $user_location = $this->CommonMethodsModel->cleanQuery($_POST['user_location']);
        else{
            $trip_origin_street_address = $this->CommonMethodsModel->cleanQuery($street_address_1);
            $arr_trip_contact = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'name' => $this->CommonMethodsModel->cleanQuery($contact_name),
                'street_address_1' => $this->CommonMethodsModel->cleanQuery($route),
                'street_address_2' => $this->CommonMethodsModel->cleanQuery($sublocality),
                'city' => $this->CommonMethodsModel->cleanQuery($locality),
                'state' => $this->CommonMethodsModel->cleanQuery($administrative_area_level_1),
                'zip_code' => $this->CommonMethodsModel->cleanQuery($postal_code),
                'country' => $this->CommonMethodsModel->cleanQuery($country),
                /*
                'phone' => $this->CommonMethodsModel->cleanQuery($phone),
                'fax' => $this->CommonMethodsModel->cleanQuery($fax),
                 * 
                 */
                'latitude' => $this->CommonMethodsModel->cleanQuery($lat),
                'longitude' => $this->CommonMethodsModel->cleanQuery($lng),
                'modified' => date('Y-m-d H:i:s')
            );

            $user_location = $this->TripModel->addTripContactAddress($arr_trip_contact);
        }
        return $user_location;
    }
    /* Trip Plugin Functions End */
}