<?php
namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

//require_once(ACTUAL_ROOTPATH.'traity/jwt/src/JWT.php');
//use \Firebase\JWT\JWT;

class CommonPlugin extends AbstractPlugin
{

   
    public function encrypt($string) {
        $output = false;

        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';

        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);

        //do the encyption given text/string/number

        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);

        return $output;

    }

    public function decrypt($string) {

        $output = false;
        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        // hash
        $key = hash('sha256', $secret_key);

        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning

        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        //decrypt the given text/string/number
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);

        return $output;

    }

public function initFacebook(){

    require_once(ACTUAL_ROOTPATH."library/social-login/facebook/facebook.php");
    $facebook = new Facebook(array(
      'appId'  => $this->generalvar['facebook_appId'],
      'secret' => $this->generalvar['facebook_app_secret'],
       'cookie' => true,
        'allowSignedRequest' => true,
        'oauth' => true
    ));
    $this->facebook = $facebook;
    $this->view->facebook = $facebook;

    //$fbuser = $facebook->getUser();

   // echo ACTUAL_ROOTPATH."library/social-login/facebook/facebook.php";
  /*
    if($fbuser){
    $user_profile = $facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
   // print_r($user_profile); exit;
    $this->comSessObj->facebook_user = $user_profile;
    $this->view->facebook_user = $user_profile;
  }*/
    return $facebook;
}

public function checkFacebookAppUser(){

    $fbuser = $this->facebook->getUser();
    if($fbuser){
    $user_profile = $this->facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');
    print_r($user_profile); exit;
    $this->comSessObj->facebook_user = $user_profile;
    $this->view->facebook_user = $user_profile;
    }
    echo $this->view->fbuser;
    exit;
    $user_id = str_replace('https://www.facebook.com/', '', $facebook_url);
    //$user_id = 'veera.bharathi.54';
    $code = $_REQUEST["code"];
    //$my_url = 'http://'.$_SERVER['SERVER_NAME'].'/requestfacebook';
    $my_url = 'http://biztrove.com/requestfacebook';
    if($code==''){
        $dialog_url= "https://www.facebook.com/dialog/oauth?"
          . "client_id=" . $this->generalvar['facebook_appId']
          . "&redirect_uri=" . urlencode($my_url);
        header("location:".$dialog_url);
        /*
        require_once(ACTUAL_ROOTPATH."library/social_network/fb/libs/facebook.php");
        // Create our Application instance (replace this with your appId and secret).

        $facebook = new Facebook(array(
          'appId'  => $this->generalvar['facebook_appId'],
          'secret' => $this->generalvar['facebook_secret'],
          'cookie'  => true
        ));


        $user = $facebook->getUser();
        //print_r($user); exit;
        //$me = null;

        if($user){
            try{

            }catch (FacebookApiException $e){
                echo $e->getMessage();
            }

        }

        $loginUrl = $facebook->getLoginUrl();
        header("location:".$loginUrl);
        */
        /*
        if($user){
            $logoutUrl = $facebook->getLogoutUrl();
            echo "<a href=$logoutUrl>logout</a>";
            return 'yes';
        }else{
            $loginUrl = $facebook->getLoginUrl();
            header("location:".$loginUrl);
            //return $loginUrl;
            //echo "<a href=$loginUrl>login</a>";
        }
         */
    }else{
        $token_url="https://graph.facebook.com/oauth/access_token?client_id="
             . $this->generalvar['facebook_appId'] . "&redirect_uri=" . urlencode($my_url)
             . "&client_secret=" . $this->generalvar['facebook_secret']
             . "&code=" . $code;

           //$response = file_get_contents($token_url);
           $response = $this->curl_get_file_contents($token_url);
           //echo $response;
           $params = null;
           parse_str($response, $params);
           $access_token = $params['access_token'];

           $user_profile = $this->getFacebookUserProfile($access_token);
           //echo "<pre>"; print_r($user_profile); exit;
           $facebook_user_id = $user_profile->id;
           if($this->comSessObj->app_request_type=='facebook_login'){
              $this->comSessObj->app_request_type = '';
              $social_type = 'facebook';
              $user = $this->myaccount_mod->getUserBySocialId($social_type,$facebook_user_id);
              echo "==>>>".$user['user_id'];
              if($user['user_id']!=''){
                  $this->comSessObj->user_id = $user['user_id'];
                  $this->comSessObj->user = $user;
                  $arr_user = array(
                             'facebook_user_id' => $facebook_user_id,
                             'facebook_authrization_code' => $code,
                             'facebook_access_token' => $access_token
                             );
                    $this->myaccount_mod->updateuser($arr_user,$this->comSessObj->user_id);
                    $this->comSessObj->user = $this->myaccount_mod->getuser($this->comSessObj->user_id);
                    $this->comSessObj->social_login_with = 'facebook';
                    $this->comSessObj->social_profile = $user_profile;
              }else{
                   $randpassword=mt_rand();
                  $arr_user = array(
                            'first_name' => $user_profile->first_name,
                            'last_name' => $user_profile->last_name,
                           /* 'facebook_user_id' => $user_profile->id,
                            'facebook_authrization_code' => $code,
                            'facebook_access_token' => $access_token,*/
                       'email_address' => $user_profile->email,
                      'password' => $randpassword,
                      'active'=>'1',
                      'created'=>date("Y-m-d H:i:s")
                            );

                   // $userid = $this->myaccount_mod->adduser($arr_user,$this->comSessObj->user_id);
                   $userid = $this->myaccount_mod->adduser($arr_user);
                   $ltype="facebook";
                   $this->sendmail = $this->_helper->Mails->sendsignupMail($user_profile->first_name,$user_profile->last_name,$user_profile->email,$randpassword,$ltype,$userid);
                    $this->comSessObj->user_id = $userid;
                    $this->comSessObj->user = $this->myaccount_mod->getuser($this->comSessObj->user_id);
                    $this->comSessObj->social_login_with = 'facebook';
                    $this->comSessObj->social_profile = $user_profile;
              }
           }else{

                //facebook_login
                //echo "<pre>";print_r($user_profile); exit;
                $arr_app_data = array(
                  'facebook_user_id' => $user_profile->id,
                  'facebook_authrization_code' => $code,
                  'facebook_access_token' => $access_token
                  );
               if($this->comSessObj->user_id){
                    $this->myaccount_mod->updateuser($arr_app_data,$this->comSessObj->user_id);
                    $this->comSessObj->user = $this->myaccount_mod->getuser($this->comSessObj->user_id);
               }else{
                   $this->comSessObj->facebook_app_data = $arr_app_data;
               }
           }

           echo "Thanks for installation of our app on facebook. if you like to reload the search result, please <a href='javascript:void(0);' onclick='window.opener.location.reload(true);self.close();'  >click here</a>";
    }
}


public function FacebookAppUser()
{
    $fbuser = $this->facebook->getUser();
   if($fbuser){
    $user_profile = $this->facebook->api('/me?fields=id,first_name,last_name,email,gender,locale,picture');


    $checkUserEmailExists = $this->comfunc_mod->checkUserEmailExists($user_profile['email']);

        if (count($checkUserEmailExists) == 0){

             $randpassword=mt_rand();
                  $arr_user = array(
                            'first_name' => $user_profile['first_name'],
                            'last_name' => $user_profile['last_name'],
                           /* 'facebook_user_id' => $user_profile->id,
                            'facebook_authrization_code' => $code,
                            'facebook_access_token' => $access_token,*/
                             "image" => $user_profile['picture']['data']['url'],
                             'email_address' => $user_profile['email'],
                             'password' => md5($randpassword),
                             'active'=>'1',
                             'created'=> date("Y-m-d H:i:s"),
                             'social_type'=> "facebook",
                             'social_id'=> $user_profile['id']
                             );
                // print_r($arr_user);
                   // $userid = $this->myaccount_mod->adduser($arr_user,$this->comSessObj->user_id);
                  //$userid = $this->myaccount_mod->adduser($arr_user);
                  $ltype="facebook";
                  // $this->sendmail = $this->_helper->Mails->sendsignupMail($user_profile->first_name,$user_profile->last_name,$user_profile->email,$randpassword,$ltype,$userid);
                  //$this->comSessObj->user_id = $userid;
                  //  $this->comSessObj->user = $this->myaccount_mod->getuser($this->comSessObj->user_id);
                  $this->comSessObj->social_login_signup = '1';
                  $this->comSessObj->social_login_with = 'facebook';
                  $this->comSessObj->social_profile = $user_profile;
        }else{
                  $this->comSessObj->user_id = $checkUserEmailExists['ID'];
                  $this->comSessObj->userexist = $this->myaccount_mod->getUser($this->comSessObj->user_id);
                  $this->comSessObj->login_try_email_exist = "1";
                  $this->comSessObj->social_login_with = 'facebook';
                  $this->comSessObj->social_profile = $user_profile;
        }
   }
}



public function refershToken($userid){
    $user = $this->comfunc_mod->getUser($userid);
    $code  = $user['facebook_authrization_code'];
    $my_url = 'http://'.$_SERVER['SERVER_NAME'].'/beta/requestfacebook';
    $token_url="https://graph.facebook.com/oauth/access_token?client_id="
        . $this->generalvar['facebook_appId'] . "&redirect_uri=" . urlencode($my_url)
        . "&client_secret=" . $this->generalvar['facebook_secret']
        . "&code=" . $code;
    //$response = file_get_contents($token_url);
    $response = $this->curl_get_file_contents($token_url);
    //echo $response;
    $params = null;
    parse_str($response, $params);
    $access_token = $params['access_token'];
    if($access_token){
    $arr_user = array(
        'facebook_access_token' => $access_token
    );
    $this->myaccount_mod->updateuser($arr_user,$userid);
    }
}

public function getFacebookFriendsOfFriends($facebook_friends){
    //echo "<pre>";print_r($facebook_friends);
    $friends_of_friends = array();
    if(count($facebook_friends)>0){
        foreach($facebook_friends as $facebook_friend){
            $user = $this->myaccount_mod->getUser($facebook_friend['user_id']);
            if($user['facebook_access_token']){
            $graph_url = "https://graph.facebook.com/me/friends?"
            . "access_token=" . $user['facebook_access_token'];
            $response = $this->curl_get_file_contents($graph_url);
            $decoded_response = json_decode($response);
            //echo "<pre>"; print_r($decoded_response); exit;
                $friends_of_friends = array_merge($friends_of_friends,$decoded_response);
            }
        }
    }
    return $friends_of_friends;

}

public function getFacebookFriends($facebook_url=''){
    $user_id = str_replace('https://www.facebook.com/', '', $facebook_url);
    $graph_url = "https://graph.facebook.com/me/friends?"
    . "access_token=" . $this->user['facebook_access_token'];
    $response = $this->curl_get_file_contents($graph_url);
    $decoded_response = json_decode($response);
    //echo "<pre>"; print_r($decoded_response); exit;
    return $decoded_response->data;
    //Check for errors
    if ($decoded_response->error) {
        // check to see if this is an oAuth error:
      if ($decoded_response->error->type== "OAuthException") {
        // Retrieving a valid access token.
        $dialog_url= "https://www.facebook.com/dialog/oauth?"
          . "client_id=" . $app_id
          . "&redirect_uri=" . urlencode($my_url);
        echo("<script> top.location.href='" . $dialog_url
        . "'</script>");
      }
      else {
        echo "other error has happened";
      }
    }
    else {
    // success
        return $decoded_response->name;
      //echo("success" . $decoded_response->name);
      //echo($access_token);
    }
    /*
    //$user_id = 'veera.bharathi.54';
    require_once(ACTUAL_ROOTPATH."library/social_network/fb/libs/facebook.php");
    // Create our Application instance (replace this with your appId and secret).

    $facebook = new Facebook(array(
      'appId'  => $this->generalvar['facebook_appId'],
      'secret' => $this->generalvar['facebook_secret'],
      'cookie'  => true
    ));
        $access_token = $this->user['facebook_access_token'];
    $params = array ('access_token' => $access_token);
    //$user_profile = $facebook->api('/'.$user_id);
    $user_friends = $facebook->api('/'.$user_id.'/friends');
    //$user_friends = $facebook->api('/'.$user_id.'/friends','GET',$params);
    //$user_friends = $facebook->api('/'.$user_id.'/friends?access_token='.$access_token);
    return $user_friends['data'];
    //echo "<pre>";print_r($user_friends); exit;
    */
}

public function getFacebookUserProfile($user_access_token,$userid=''){
    if($user_access_token){
        $graph_url = "https://graph.facebook.com/me?fields=email&"
        . "access_token=" . $user_access_token;
        $response = $this->curl_get_file_contents($graph_url);
        $decoded_response = json_decode($response);
        //echo "<pre>"; print_r($decoded_response);
        if($decoded_response->error){
            //$error = $decoded_response->error;
            //if($error->code == 190){
               //echo 'refreshToken : '.$userid;
               $this->refershToken($userid);
               $user = $this->myaccount_mod->getUser($userid);
                $graph_url = "https://graph.facebook.com/me?fields=email&"
                . "access_token=" . $user_access_token;
                $response = $this->curl_get_file_contents($graph_url);
                $decoded_response = json_decode($response);
                return $decoded_response;
            //}
        }else{
            return $decoded_response;
        }
    }else{
        $this->refershToken($userid);
        $user = $this->comfunc_mod->getUser($userid);
            $graph_url = "https://graph.facebook.com/me?fields=email&"
            . "access_token=" . $user_access_token;
            $response = $this->curl_get_file_contents($graph_url);
            $decoded_response = json_decode($response);
            return $decoded_response;

    }
}

public function getFacebookUser($facebook_url){
    $user_id = str_replace('https://www.facebook.com/', '', $facebook_url);
    $graph_url = "https://graph.facebook.com/$user_id?"
    . "access_token=" . $this->user['facebook_access_token'];
    $response = $this->curl_get_file_contents($graph_url);
    $decoded_response = json_decode($response);
    //echo "<pre>"; print_r($decoded_response); exit;
    return $decoded_response;
}

public function getFacebookFriendsComparison($user_lists,$facebook_friends){
    if(count($user_lists)>0){
        foreach($user_lists as $user_list){
            $facebook_user = $this->getFacebookUser($user_list['facebook_url']);
            if(count($facebook_friends)>0){
                foreach($facebook_friends as $facebook_friend){
                    if($facebook_friend->id==$facebook_user->id){
                        //$facebook_positive_rating = $facebook_positive_rating + 1;
                        $arr_facebook_friends[] = array('id' => $facebook_friend->id,'name' => $facebook_friend->name,'user_id' => $user_list['user_id']);
                    }
                }
            }
        }
    }
    return $arr_facebook_friends;
}

public function curl_get_file_contents($URL) {
    $c = curl_init();
    curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($c, CURLOPT_URL, $URL);
    $contents = curl_exec($c);
    $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
    curl_close($c);
    if ($contents) return $contents;
    else return FALSE;
  }

public  function calculateAge( $dob, $tdate ){
        $age = 0;
		if($dob == '0000-00-00'){
			return '';
		}
		$dob = is_numeric($dob)?$dob:strtotime($dob);
        while( $tdate > $dob = strtotime('+1 year', $dob))
        {
                ++$age;
        }
        return $age;
}
  public function datediffcal($from,$to)
{
    $days = floor((strtotime($from) - strtotime($to))/(60*60*24));
return $days;
}

    /*function widget_signature($app_key, $app_secret, $current_user_id,$options = array()) {
        $payload = array_merge(array(
            'time' => time(),
            'current_user_id' => $current_user_id), $options);

        $signature = array(
            'key' => $app_key,
            'payload' => JWT::encode($payload, $app_secret));

        return JWT::encode($signature, '');
    }*/

 
}



?>
