<?php
namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Mvc\Controller\Plugin\PluginInterface;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Stripe;

class TripPlugin extends AbstractPlugin
{
    var $sessionObj;
    var $generalvar;
    var $TripModel;
    var $CommonMethodsModel;
	var $stripeObj;
	var $stripeConfig;

    public function __construct($data = false)
    {
        /*Loading models from factory * Call the model object using it's class name         */
        if ($data['models'] && is_array($data['models'])) {
            foreach ($data['models'] as $model) {
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->authNamespace = new Container('maestro_Auth');
        $this->sessionObj = new Container('comSessObj');
        $this->generalvar = $data['configs']['siteConfigs'];
		$this->stripeConfig = $data['configs']['stripe_keys'];
        $this->adminid = $this->authNamespace->adminid;
		Stripe\Stripe::setApiKey($this->stripeConfig['api_key']);
    }

    /*public function init() {        $this->view = $this->getActionController()->view;        //$this->CommonMethodsModel = new Application_Model_DbTable_Commonfunctions();        $this->TripModel = new Application_Model_DbTable_Trip();        $this->authNamespace = new Container('maestro_Auth');        $this->sessionObj = new Container('comSessObj');        $this->adminid = $this->authNamespace->adminid;        $this->controller = $this->getRequest()->getControllerName();        $this->dirpath = str_replace('index.php','theme/',$_SERVER['SCRIPT_FILENAME']);        //$config_params = Zend_Registry::get('config');        //$this->generalvar = $config_params['general'];        $this->common_helper = Zend_Controller_Action_HelperBroker::getStaticHelper('Common');    }*/
    public function initCommonFunction()
    {
        $this->view->generalvar = $this->generalvar;
    }

    public function getTrips($status, $userrole = '', $userid = '', $filterRequestTrips = false, $wishlistedTrips = false)
    {
        if ($userrole == '')
            $userrole = $this->sessionObj->userrole;
		
		if($wishlistedTrips){
			$trips = $this->TripModel->getWishlistTrips('', $userrole, $this->sessionObj->userid);
			$userrole = $userrole == 'seeker'?'traveller':'seeker';
		} else {
        	$trips = $this->TripModel->getTrips($status, $userrole, $this->sessionObj->userid);
		}
        // print_r($trips);die;
        $arr_trips = array();

        if ($trips) {
            foreach ($trips as $trip) {
                if ($userrole == 'seeker') {
                    $trip_people = $this->TripModel->getSeekerPeople($trip['ID']);
                    $trip_package = $this->TripModel->getSeekerPackage($trip['ID']);
                    $trip_project = $this->TripModel->getSeekerProject($trip['ID']);
                    // print_r($trip_project);
                    /*** These are requests are sent by Traveller for Seeker's TRIP ****/
                    $trip_people_request = $this->TripModel->getTravellerPeopleRequest($trip['ID'])?count($this->TripModel->getTravellerPeopleRequest($trip['ID'])):0;
                    $trip_package_request = $this->TripModel->getTravellerPackageRequest($trip['ID'])?count($this->TripModel->getTravellerPackageRequest($trip['ID'])):0;
                    $trip_project_request = $this->TripModel->getTravellerProjectRequest($trip['ID'])?count($this->TripModel->getTravellerProjectRequest($trip['ID'])):0;
                    /*** These are requests are sent by Traveller for Seeker's TRIP ****/
					
					$tripServiceType = (is_array($trip_people) && count($trip_people) > 0)?'people':'';
					$tripServiceType = (is_array($trip_package) && count($trip_package) > 0)?'package':$tripServiceType;
					$tripServiceType = (is_array($trip_project) && count($trip_project) > 0)?'project':$tripServiceType;
				
					$requestSent = 0;
					$requestStatus = array();
					if($tripServiceType == 'people'){
						$requestStatus = $this->TripModel->getSeekersRequestStatus($trip_people['ID'],'people',0);
					} else if($tripServiceType == 'package'){
						$requestStatus = $this->TripModel->getSeekersRequestStatus($trip_package['ID'],'package',0);
					} else if($tripServiceType == 'project'){
						$requestStatus = $this->TripModel->getSeekersRequestStatus($trip_project['ID'],'project',0);
					}
					if(count($requestStatus) > 0 && isset($requestStatus['status_cnt']) && $requestStatus['status_cnt'] > 0){
						$requestSent = $requestStatus['status_cnt'];
					}


                } elseif ($userrole == 'traveller') {
                    $trip_people = $this->TripModel->getTravellerPeople($trip['ID']);
                    $trip_package = $this->TripModel->getTravellerPackage($trip['ID']);
                    $trip_project = $this->TripModel->getTravellerProject($trip['ID']);
                    /*** These are requests are sent by Seeker for traveller's TRIP ****/
                    $trip_people_request = $this->TripModel->getSeekerPeopleRequest($trip['ID'])?count($this->TripModel->getSeekerPeopleRequest($trip['ID'])):0;
                    $trip_package_request = $this->TripModel->getSeekerPackageRequest($trip['ID'])?count($this->TripModel->getSeekerPackageRequest($trip['ID'])):0;
                    $trip_project_request = $this->TripModel->getSeekerProjectRequest($trip['ID'])?count($this->TripModel->getSeekerProjectRequest($trip['ID'])):0;
                    /*** These are requests are sent by Seeker for traveller's TRIP ****/
					
					$tripServiceType = (is_array($trip_people) && count($trip_people) > 0)?'people':'';
					$tripServiceType = (is_array($trip_package) && count($trip_package) > 0)?'package':$tripServiceType;
					$tripServiceType = (is_array($trip_project) && count($trip_project) > 0)?'project':$tripServiceType;
				
					$requestSent = 0;
					$requestStatus = array();
					if($tripServiceType == 'people'){
						$requestStatus = $this->TripModel->getTravellersRequestStatus($trip_people['ID'],'people',0);
					} else if($tripServiceType == 'package'){
						$requestStatus = $this->TripModel->getTravellersRequestStatus($trip_package['ID'],'package',0);
					} else if($tripServiceType == 'project'){
						$requestStatus = $this->TripModel->getTravellersRequestStatus($trip_project['ID'],'project',0);
					}
					if(count($requestStatus) > 0 && isset($requestStatus['status_cnt']) && $requestStatus['status_cnt'] > 0){
						$requestSent = $requestStatus['status_cnt'];
					}
                }
                $trip_people = isset($trip_people) ? $trip_people : false;
                $trip_package = isset($trip_package) ? $trip_package : false;
                $trip_project = isset($trip_project) ? $trip_project : false;
                $trip_people_request = isset($trip_people_request) ? $trip_people_request : false;
                $trip_package_request = isset($trip_package_request) ? $trip_package_request : false;
                $trip_project_request = isset($trip_project_request) ? $trip_project_request : false;
				
				if(!$trip_project_request && !$trip_package_request && !$trip_people_request && $filterRequestTrips){
					continue;
				}
				
				$tripStatusTxt = $this->TripModel->getTripCurrentStatus($trip['ID'],$userrole);
				
                $service = '';
                if ($trip_people['ID']) {
                    $service = 'People';
					
                    $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
                    $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
                    $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                    $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                    $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                    $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                    $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                    $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                    $airport_details = $this->TripModel->getLastFlightsRowDetails($trip['ID'], $destination_location_code);
                    $arrivalDate = (isset($airport_details['arrival_date']) && !empty($airport_details['arrival_date'])) ? $airport_details['arrival_date'] : '';
                    $project_start_date = (isset($trip['project_start_date']) && !empty($trip['project_start_date'])) ? $trip['project_start_date'] : 0;
                    $project_end_date = (isset($trip['project_end_date']) && !empty($trip['project_end_date'])) ? $trip['project_end_date'] : 0;
                    $trip_type = (isset($trip['travel_plan_reservation_type']) && !empty($trip['travel_plan_reservation_type'])) ? $trip['travel_plan_reservation_type'] : 0;
                    $arr_trips[] = array(
                        'ID' => $this->encrypt($trip['ID']),
						'normal_id' => $trip['ID'],
                        'name' => $trip['name'],
                        'origin_location' => $trip['origin_location'],
                        'destination_location' => $trip['destination_location'],
                        'ticket_image' => $trip['ticket_image'],
                        'travel_plan_reservation_type' => $trip_type,
                        'project_start_date' => $project_start_date,
                        'project_end_date' => $project_end_date,
                        'origin_location_code' => $origin_location_code,
                        'destination_location_code' => $destination_location_code,
                        'origin_location_city' => $origin_location_city,
                        'destination_location_city' => $destination_location_city,
                        'origin_location_country' => $origin_location_country,
                        'destination_location_country' => $destination_location_country,
                        'departure_date' => $trip['departure_date'],
                        'arrival_date' => $trip['arrival_date'],//$arrivalDate,
                        'service' => $service,
                        'people_request_count' => $trip_people_request,
                        'package_request_count' => $trip_package_request,
                        'project_request_count' => $trip_project_request,
						'total_request_count' => $trip_people_request+$trip_package_request+$trip_project_request,
						'total_request_sent_count'=> $requestSent,
                        'trip_people' => $trip_people,
                        'trip_package' => $trip_package,
                        'trip_status' => $trip['trip_status'],
						'trip_status_txt' => $tripStatusTxt,
                        'trip_project' => $trip_project,
                        'modified' => $trip['modified'],
                    );


                }
                if ($trip_package['ID']) {
                    /*//$service .= ',';    */
                    $service = 'Package';
					
                    $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
                    $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
                    $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                    $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                    $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                    $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                    $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                    $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                    $airport_details = $this->TripModel->getLastFlightsRowDetails($trip['ID'], $destination_location_code);
                    $arrivalDate = (isset($airport_details['arrival_date']) && !empty($airport_details['arrival_date'])) ? $airport_details['arrival_date'] : '';
                    $project_start_date = (isset($trip['project_start_date']) && !empty($trip['project_start_date'])) ? $trip['project_start_date'] : 0;
                    $project_end_date = (isset($trip['project_end_date']) && !empty($trip['project_end_date'])) ? $trip['project_end_date'] : 0;
                    $trip_type = (isset($trip['travel_plan_reservation_type']) && !empty($trip['travel_plan_reservation_type'])) ? $trip['travel_plan_reservation_type'] : 0;
                    $arr_trips[] = array(
                        'ID' => $this->encrypt($trip['ID']),
						'normal_id' => $trip['ID'],
                        'name' => $trip['name'],
                        'origin_location' => $trip['origin_location'],
                        'destination_location' => $trip['destination_location'],
                        'ticket_image' => $trip['ticket_image'],
                        'travel_plan_reservation_type' => $trip_type,
                        'project_start_date' => $project_start_date,
                        'project_end_date' => $project_end_date,
                        'origin_location_code' => $origin_location_code,
                        'destination_location_code' => $destination_location_code,
                        'origin_location_city' => $origin_location_city,
                        'destination_location_city' => $destination_location_city,
                        'origin_location_country' => $origin_location_country,
                        'destination_location_country' => $destination_location_country,
                        'departure_date' => $trip['departure_date'],
                        'arrival_date' => $trip['arrival_date'],//$arrivalDate,
                        'service' => $service,
                        'people_request_count' => $trip_people_request,
                        'package_request_count' => $trip_package_request,
                        'project_request_count' => $trip_project_request,
						'total_request_count' => $trip_people_request+$trip_package_request+$trip_project_request,
						'total_request_sent_count'=> $requestSent,
                        'trip_people' => $trip_people,
                        'trip_package' => $trip_package,
                        'trip_status' => $trip['trip_status'],
						'trip_status_txt' => $tripStatusTxt,
                        'trip_project' => $trip_project,
                        'modified' => $trip['modified']
                    );
                }
                if ($trip_project['ID']) {
                    /*// $service .= ',';  */
                    $service = 'Product';

                    $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
                    $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
                    $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                    $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                    $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                    $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                    $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                    $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                    $airport_details = $this->TripModel->getLastFlightsRowDetails($trip['ID'], $destination_location_code);
                    $arrivalDate = (isset($airport_details['arrival_date']) && !empty($airport_details['arrival_date'])) ? $airport_details['arrival_date'] : '';
                    $project_start_date = (isset($trip['project_start_date']) && !empty($trip['project_start_date'])) ? $trip['project_start_date'] : 0;
                    $project_end_date = (isset($trip['project_end_date']) && !empty($trip['project_end_date'])) ? $trip['project_end_date'] : 0;
                    $trip_type = (isset($trip['travel_plan_reservation_type']) && !empty($trip['travel_plan_reservation_type'])) ? $trip['travel_plan_reservation_type'] : 0;
                    $arr_trips[] = array(
                        'ID' => $this->encrypt($trip['ID']),
						'normal_id' => $trip['ID'],
                        'name' => $trip['name'],
                        'origin_location' => $trip['origin_location'],
                        'destination_location' => $trip['destination_location'],
                        'ticket_image' => $trip['ticket_image'],
                        'travel_plan_reservation_type' => $trip_type,
                        'project_start_date' => $project_start_date,
                        'project_end_date' => $project_end_date,
                        'origin_location_code' => $origin_location_code,
                        'destination_location_code' => $destination_location_code,
                        'origin_location_city' => $origin_location_city,
                        'destination_location_city' => $destination_location_city,
                        'origin_location_country' => $origin_location_country,
                        'destination_location_country' => $destination_location_country,
                        'departure_date' => $trip['departure_date'],
                        'arrival_date' => $trip['arrival_date'],//$arrivalDate,
                        'service' => $service,
                        'people_request_count' => $trip_people_request,
                        'package_request_count' => $trip_package_request,
                        'project_request_count' => $trip_project_request,
						'total_request_count' => $trip_people_request+$trip_package_request+$trip_project_request,
						'total_request_sent_count'=> $requestSent,
                        'trip_people' => $trip_people,
                        'trip_package' => $trip_package,
                        'trip_status' => $trip['trip_status'],
						'trip_status_txt' => $tripStatusTxt,
                        'trip_project' => $trip_project,
                        'modified' => $trip['modified']
                    );
                }
            }


            return $arr_trips;
        }
    }

    public function encrypt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        /*pls set your unique hashing key*/
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        /* hash*/
        $key = hash('sha256', $secret_key);
        /* iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning*/
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        /*do the encyption given text/string/number*/
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }

    public function getWishlistTrips($status, $userrole = '')
    {
        return true;
    }

    public function getServiceExpensesTrips($userrole = '')
    {
        return true;
    }

    public function getTripDetails($tripId, $userrole = '', $checkFlightStatus = true, $service = false)
    {
        if ($userrole == '')
            $userrole = $this->sessionObj->userrole;
        $arr_trip = false;
        $trip = $this->TripModel->getTrip($tripId, $userrole);

        //echo '<pre>'; print_r($trip);exit();

        if ($trip) {
            if ($userrole == 'seeker') {
                $trip_people = $this->TripModel->getSeekerPeople($trip['ID']);
                $trip_package = $this->TripModel->getSeekerPackage($trip['ID']);
                $trip_project = $this->TripModel->getSeekerProject($trip['ID']);
                /*** These are requests are sent by Traveller for Seeker's TRIP ****/
                $trip_people_request = count($this->TripModel->getTravellerPeopleRequest($trip['ID']));
                $trip_package_request = count($this->TripModel->getTravellerPackageRequest($trip['ID']));
                $trip_project_request = count($this->TripModel->getTravellerProjectRequest($trip['ID']));
                /*** These are requests are sent by Traveller for Seeker's TRIP ****/
            } elseif ($userrole == 'traveller') {
                $trip_people = $this->TripModel->getTravellerPeople($trip['ID']);
                $trip_package = $this->TripModel->getTravellerPackage($trip['ID']);
                $trip_project = $this->TripModel->getTravellerProject($trip['ID']);
                if (!empty($trip_project)) {
                    if (!empty($trip_project['task'])) {
                        $trip_project_task = $this->CommonMethodsModel->gerProjectByCat($trip_project['task']);
                        $trip_project_task_sub = $this->CommonMethodsModel->getSubCatById($trip_project['task_subcat']);
                        $trip_project['catname'] = $trip_project_task['catname'];
                        $trip_project['subcatname'] = $trip_project_task_sub['subcatname'];
                    }
                    if (!empty($trip_project['product_category'])) {
                        $trip_project_task = $this->CommonMethodsModel->gerProjectByCatIds($trip_project['product_category']);
						$trip_project_task_type = $this->CommonMethodsModel->getSubCatByIds($trip_project['product_type']);
						$catname = array();
						foreach($trip_project_task as $tp){
							$catname[] = $tp['catname'];
						}
						$catname = implode(',',$catname);
						
						$subcatname = array();
						foreach($trip_project_task_type as $tpt){
							$subcatname[] = $tpt['subcatname'];
						}
						$subcatname = implode(',',$subcatname);
						
                        $trip_project['product_catname'] = $catname;
                        $trip_project['product_subcatname'] = $subcatname;
                    }
                }
                $trip_project_task = $this->CommonMethodsModel->getSubCatById($trip_project['task']);
                /*** These are requests are sent by Seeker for traveller's TRIP ****/
                $trip_people_request = count($this->TripModel->getSeekerPeopleRequest($trip['ID']));
                $trip_package_request = count($this->TripModel->getSeekerPackageRequest($trip['ID']));
                $trip_project_request = count($this->TripModel->getSeekerProjectRequest($trip['ID']));
                /*** These are requests are sent by Seeker for traveller's TRIP ****/
            }

            $airport_details = $this->TripModel->getFlightsDetails($trip['ID'],$userrole);
            if ($checkFlightStatus) {
                if (isset($airport_details) && !empty($airport_details)) {
                    $filght_status_app_id = $this->generalvar['filght_status_app_id'];
                    $filght_status_app_key = $this->generalvar['filght_status_app_key'];
                    $apiUrl = 'https://api.flightstats.com/flex';
                    foreach ($airport_details as $flightRow) {
                        $arrivalDateTime = strtotime($flightRow['arrival_date']);
                        $year = date('Y', $arrivalDateTime);
                        $month = date('m', $arrivalDateTime);
                        $day = date('d', $arrivalDateTime);
                        /* $flightRow['carrier'] = '9W';                    $flightRow['number'] = '355';*/
                        $departure = $flightRow['departure'];
                        $arrival = $flightRow['arrival'];
                        if (!empty($flightRow['carrier']) && !empty($flightRow['number'])) {
                            $content = file_get_contents($apiUrl . "/flightstatus/rest/v2/json/flight/status/" . $flightRow['carrier'] . "/" . $flightRow['number'] . "/dep/" . $year . "/" . $month . "/" . $day . "?appId=" . $filght_status_app_id . "&appKey=" . $filght_status_app_key . "&utc=false&includeFlightPlan");
                            $obj = json_decode($content);
                            $airportDetails = isset($obj->appendix->airports) ? $obj->appendix->airports : array();
                            $flightStatuses = $obj->flightStatuses;
                            $arrInsData = array();
                            if (!empty($airportDetails)) {
                                foreach ($airportDetails as $aDetails) {
                                    if ($aDetails->iata == $departure) {
                                        $arrInsData['orgin_locaion'] = $aDetails->name;
                                        $arrInsData['orgin_city'] = $aDetails->city;
                                        $arrInsData['orgin_state'] = $aDetails->city;
                                        $arrInsData['orgin_country'] = $aDetails->countryName;
                                    }
                                    if ($aDetails->iata == $arrival) {
                                        $arrInsData['destination_location'] = $aDetails->name;
                                        $arrInsData['destination_city'] = $aDetails->city;
                                        $arrInsData['destination_state'] = $aDetails->city;
                                        $arrInsData['destination_country'] = $aDetails->countryName;
                                    }
                                }
                            }
                            if (isset($flightStatuses) && !empty($flightStatuses)) {
                                foreach ($flightStatuses as $fStatus) {
                                    if (($fStatus->departureAirportFsCode == $departure) && ($fStatus->arrivalAirportFsCode == $arrival)) {
                                        $operationalTimes = $fStatus->operationalTimes;
                                        $arrInsData['flight_status'] = $fStatus->status;
                                        $arrInsData['departure_scheduled_date'] = isset($operationalTimes->scheduledGateDeparture->dateUtc) ? $operationalTimes->scheduledGateDeparture->dateUtc : '';
                                        $arrInsData['departure_acutal_date'] = isset($operationalTimes->actualGateDeparture->dateUtc) ? $operationalTimes->actualGateDeparture->dateUtc : '';
                                        $arrInsData['departure_terminal'] = isset($fStatus->airportResources->departureTerminal) ? $fStatus->airportResources->departureTerminal : '';
                                        $arrInsData['departure_delay'] = isset($fStatus->delays->departureGateDelayMinutes) ? $fStatus->delays->departureGateDelayMinutes : '';
                                        $arrInsData['arrival_scheduled_date'] = isset($operationalTimes->scheduledGateArrival->dateUtc) ? $operationalTimes->scheduledGateArrival->dateUtc : '';
                                        $arrInsData['arrival_actual_date'] = isset($operationalTimes->actualGateArrival->dateUtc) ? $operationalTimes->actualGateArrival->dateUtc : '';
                                        $arrInsData['arrival_terminal'] = isset($fStatus->airportResources->arrivalTerminal) ? $fStatus->airportResources->arrivalTerminal : '';
                                        $arrInsData['arrival_delay'] = isset($fStatus->delays->arrivalGateDelayMinutes) ? $fStatus->delays->arrivalGateDelayMinutes : '';
                                        $arrInsData['departure_extimated_date'] = isset($operationalTimes->estimatedGateDeparture->dateUtc) ? $operationalTimes->estimatedGateDeparture->dateUtc : '';
                                        $arrInsData['arrival_extimated_date'] = isset($operationalTimes->estimatedGateArrival->dateUtc) ? $operationalTimes->estimatedGateArrival->dateUtc : '';
                                    }
                                }
                            }
                            $tractcontent = file_get_contents($apiUrl . "/flightstatus/rest/v2/json/flight/tracks/" . $flightRow['carrier'] . "/" . $flightRow['number'] . "/dep/" . $year . "/" . $month . "/" . $day . "?appId=" . $filght_status_app_id . "&appKey=" . $filght_status_app_key . "&utc=false&includeFlightPlan");
                            $trackobj = json_decode($tractcontent);

                            $flightTracks = $trackobj->flightTracks;
                            if (isset($flightTracks) && !empty($flightTracks)) {
                                foreach ($flightTracks as $fTracks) {
                                    if (($fTracks->departureAirportFsCode == $departure) && ($fTracks->arrivalAirportFsCode == $arrival)) {
                                        if (isset($fTracks->positions[0])) {
                                            $fTracksPosition = $fTracks->positions[0];
                                            $arrInsData['speed'] = isset($fTracksPosition->speedMph) ? $fTracksPosition->speedMph : '';
                                            $arrInsData['altitude'] = isset($fTracksPosition->altitudeFt) ? $fTracksPosition->altitudeFt : '';
                                        }
                                    }
                                }
                            }
                            if (!empty($arrInsData)) {
                                $clienturl = str_replace('public/', '', $this->generalvar['mail_url']);
                                $trip_en_id = $this->encrypt($flightRow['ID']);
                                $callbackUrl = $clienturl . 'sendflightstatus/' . $trip_en_id;
                                $this->TripModel->updateTripFlights($arrInsData, $flightRow['ID']);
                                $contentAlert = file_get_contents($apiUrl . "/alerts/rest/v1/json/create/" . $flightRow['carrier'] . "/" . $flightRow['number'] . "/from/" . $departure . "/departing/" . $year . "/" . $month . "/" . $day . "?appId=" . $filght_status_app_id . "&appKey=" . $filght_status_app_key . "&type=JSON&deliverTo=" . $callbackUrl);
                            }
                        }
                    }
                }
            }
            $airport_details = $this->TripModel->getFlightsDetails($trip['ID'],$userrole);
            $trip_people = isset($trip_people) ? $trip_people : false;
            $trip_package = isset($trip_package) ? $trip_package : false;
            $trip_project = isset($trip_project) ? $trip_project : false;
            $trip_project_task = isset($trip_project_task) ? $trip_project_task : false;
            $airport_details = isset($airport_details) ? $airport_details : false;
            $trip_people_request = isset($trip_people_request) ? $trip_people_request : '';
            $trip_package_request = isset($trip_package_request) ? $trip_package_request : '';
            $trip_project_request = isset($trip_project_request) ? $trip_project_request : '';
            $trip_seeker_project_details = isset($trip_seeker_project_details) ? $trip_seeker_project_details : '';
            if (isset($trip_people) && $trip_people['ID'] && $service == 'People') {
                $service = 'People';
                $trip_people_passangers = $this->TripModel->getSeekerPeoplePassangers($trip_people['ID']);
                if ($trip_people_passangers) {
                    foreach ($trip_people_passangers as $key => $passenger) {
                        $pass_att = $this->TripModel->getSeekerTripPeoplePassengersAttachment($passenger['ID']);
                        if ($pass_att) {
                            $trip_people_passangers[$key]['attachments'] = $pass_att;
                        }
                    }
                }

       
                $trip_user = $this->CommonMethodsModel->getUser($trip['user']);
                
                $trip_origin_address = $this->CommonMethodsModel->getUserLocationByAddrId($trip['user_location']);
                $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
                $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
                $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $airportDetails = $this->TripModel->getLastFlightsRowDetails($trip['ID'], $destination_location_code);
                $arrivalDate = (isset($airportDetails['arrival_date']) && !empty($airportDetails['arrival_date'])) ? $airportDetails['arrival_date'] : '';
                $project_start_date = (isset($trip['project_start_date']) && !empty($trip['project_start_date'])) ? $trip['project_start_date'] : 0;
                $project_end_date = (isset($trip['project_end_date']) && !empty($trip['project_end_date'])) ? $trip['project_end_date'] : 0;
                $date_flexibility = (isset($trip['date_flexibility']) && !empty($trip['date_flexibility'])) ? $trip['date_flexibility'] : 0;
                $travel_plan_reservation_type = (isset($trip['travel_plan_reservation_type']) && !empty($trip['travel_plan_reservation_type'])) ? $trip['travel_plan_reservation_type'] : '';
                $arr_trip = array(
                    'ID' => $this->encrypt($trip['ID']),
					'trip_id_number' => $trip['trip_id_number'],
                    'name' => $trip['name'],
                    'user' => $trip['user'],
                    'origin_location' => $trip['origin_location'],
                    'origin_location_airname' => $this->TripModel->getAirPort($trip['origin_location']),
                    'destination_location' => $trip['destination_location'],
                    'destination_location_airname' => $this->TripModel->getAirPort($trip['destination_location']),
                    'ticket_image' => $trip['ticket_image'],
                    'trip_status' => $trip['trip_status'],
                    'active' => $trip['active'],
                    'origin_location_code' => $origin_location_code,
                    'destination_location_code' => $destination_location_code,
                    'origin_location_city' => $origin_location_city,
                    'destination_location_city' => $destination_location_city,
                    'origin_location_country' => $origin_location_country,
                    'destination_location_country' => $destination_location_country,
                    'airline_name' => $trip['airline_name'],
                    'flight_number' => $trip['operating_carrier_name'] . $trip['flight_number'],
                    'cabin' => $trip['cabin'],
                    'booking_status' => $trip['booking_status'],
                    'departure_date' => $trip['departure_date'],
                    'arrival_date' => $arrivalDate !='' ? $arrivalDate : $trip['arrival_date'],//$arrivalDate,
                    'date_flexibility' => $date_flexibility,
                    'travel_plan_reservation_type' => $travel_plan_reservation_type,
                    'project_start_date' => $project_start_date,
                    'project_end_date' => $project_end_date,
                    'service' => $service,
                    'people_request_count' => $trip_people_request,
                    'trip_people' => $trip_people,
                    'trip_project_task' => $trip_project_task,
                    'trip_origin_address' => $trip_origin_address,
                    'trip_user' => $trip_user,
                    'airport_details' => $airport_details,
                    'trip_people_passangers' => $trip_people_passangers,
                    'modified' => $trip['modified'],
                    'travel_agency_name' => $trip['travel_agency_name'],
                    'travel_agency_url' => $trip['travel_agency_url'],
                    'travel_agency_confirmation' => $trip['travel_agency_confirmation'],
                    'travel_agency_contact_name' => $trip['travel_agency_contact_name'],
                    'travel_agency_phone' => $trip['travel_agency_phone'],
                    'agency_email' => $trip['agency_email'],
                    'supplier_name' => $trip['supplier_name'],
                    'supplier_url' => $trip['supplier_url'],
                    'supplier_contact' => $trip['supplier_contact'],
                    'supplier_phone' => $trip['supplier_phone'],
                    'booking_site_name' => $trip['booking_site_name'],
                    'booking_site_url' => $trip['booking_site_url'],
                    'booking_site_phone' => $trip['booking_site_phone'],
                    'booking_site_email' => $trip['booking_site_email'],
                    'booking_date' => $trip['booking_date'],
                    'booking_reference' => $trip['booking_reference'],
                    'reservation_is_purchased' => $trip['reservation_is_purchased'],
					'booking_cost' => $trip['booking_cost'],
					'booking_cost_currency' => $trip['booking_cost_currency'],
                    'total_cost' => $trip['total_cost'],
                    'total_cost_currency' => $trip['total_cost_currency'],
                    'comments_restrictions' => $trip['comments_restrictions'],
                    'supplier_email' => $trip['supplier_email'],
                    'discount_code' => $trip['discount_code'],
                    'total_discount_cost' => $trip['total_discount_cost'],
                    'total_discount_currency' => $trip['total_discount_currency'],
                    'noofstops' => $trip['noofstops']
                );

            }
            $trip_people_passangers = isset($trip_people_passangers) ? $trip_people_passangers : false;
            if (isset($trip_package) && $trip_package['ID'] && $service == 'Package') {
                /*//$service = ',';    */
                $service = 'Package';
                $trip_packages_packages = $this->TripModel->getSeekerPackagesPakages($trip_package['ID']);
                if ($trip_seeker_project_details) {
                    foreach ($trip_seeker_project_details as $key => $project) {
                        $pro_att = $this->TripModel->getSeekerTripProjectTaskAttachment($project['ID']);
                        if ($pro_att) {
                            $trip_packages_packages[$key]['attachments'] = $pro_att;
                        }
                    }
                }
                $trip_user = $this->CommonMethodsModel->getUser($trip['user']);
                $trip_origin_address = $this->CommonMethodsModel->getUserLocationByAddrId($trip['user_location']);
                $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
                $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
                $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $airportDetails = $this->TripModel->getLastFlightsRowDetails($trip['ID'], $destination_location_code);
                $arrivalDate = (isset($airportDetails['arrival_date']) && !empty($airportDetails['arrival_date'])) ? $airportDetails['arrival_date'] : '';
                $project_start_date = (isset($trip['project_start_date']) && !empty($trip['project_start_date'])) ? $trip['project_start_date'] : 0;
                $project_end_date = (isset($trip['project_end_date']) && !empty($trip['project_end_date'])) ? $trip['project_end_date'] : 0;
                $date_flexibility = (isset($trip['date_flexibility']) && !empty($trip['date_flexibility'])) ? $trip['date_flexibility'] : 0;
                $travel_plan_reservation_type = (isset($trip['travel_plan_reservation_type']) && !empty($trip['travel_plan_reservation_type'])) ? $trip['travel_plan_reservation_type'] : '';
                $arr_trip = array(
                    'ID' => $this->encrypt($trip['ID']),
					'trip_id_number' => $trip['trip_id_number'],
                    'name' => $trip['name'],
                    'user' => $trip['user'],
                    'origin_location' => $trip['origin_location'],
                    'origin_location_airname' => $this->TripModel->getAirPort($trip['origin_location']),
                    'destination_location' => $trip['destination_location'],
                    'destination_location_airname' => $this->TripModel->getAirPort($trip['destination_location']),
                    'ticket_image' => $trip['ticket_image'],
                    'trip_status' => $trip['trip_status'],
                    'active' => $trip['active'],
                    'origin_location_code' => $origin_location_code,
                    'destination_location_code' => $destination_location_code,
                    'origin_location_city' => $origin_location_city,
                    'destination_location_city' => $destination_location_city,
                    'origin_location_country' => $origin_location_country,
                    'destination_location_country' => $destination_location_country,
                    'airline_name' => $trip['airline_name'],
                    'flight_number' => $trip['operating_carrier_name'] . $trip['flight_number'],
                    'cabin' => $trip['cabin'],
                    'booking_status' => $trip['booking_status'],
                    'departure_date' => $trip['departure_date'],
                    'arrival_date' => $arrivalDate !='' ? $arrivalDate : $trip['arrival_date'],//$arrivalDate,
                    'date_flexibility' => $date_flexibility,
                    'travel_plan_reservation_type' => $travel_plan_reservation_type,
                    'project_start_date' => $project_start_date,
                    'project_end_date' => $project_end_date,
                    'service' => $service,
                    'package_request_count' => $trip_package_request,
                    'trip_package' => $trip_package,
                    'trip_project_task' => $trip_project_task,
                    'trip_origin_address' => $trip_origin_address,
                    'trip_user' => $trip_user,
                    'airport_details' => $airport_details,
                    'trip_people_passangers' => $trip_people_passangers,
                    'trip_packages_packages' => $trip_packages_packages,
                    'modified' => $trip['modified'],
                    'travel_agency_name' => $trip['travel_agency_name'],
                    'travel_agency_url' => $trip['travel_agency_url'],
                    'travel_agency_confirmation' => $trip['travel_agency_confirmation'],
                    'travel_agency_contact_name' => $trip['travel_agency_contact_name'],
                    'travel_agency_phone' => $trip['travel_agency_phone'],
                    'agency_email' => $trip['agency_email'],
                    'supplier_name' => $trip['supplier_name'],
                    'supplier_url' => $trip['supplier_url'],
                    'supplier_contact' => $trip['supplier_contact'],
                    'supplier_phone' => $trip['supplier_phone'],
                    'booking_site_name' => $trip['booking_site_name'],
                    'booking_site_url' => $trip['booking_site_url'],
                    'booking_site_phone' => $trip['booking_site_phone'],
                    'booking_site_email' => $trip['booking_site_email'],
                    'booking_date' => $trip['booking_date'],
                    'booking_reference' => $trip['booking_reference'],
					'booking_cost' => $trip['booking_cost'],
					'booking_cost_currency' => $trip['booking_cost_currency'],
                    'reservation_is_purchased' => $trip['reservation_is_purchased'],
                    'total_cost' => $trip['total_cost'],
                    'total_cost_currency' => $trip['total_cost_currency'],
                    'comments_restrictions' => $trip['comments_restrictions'],
                    'supplier_email' => $trip['supplier_email'],
                    'discount_code' => $trip['discount_code'],
                    'total_discount_cost' => $trip['total_discount_cost'],
                    'total_discount_currency' => $trip['total_discount_currency'],
                    'noofstops' => $trip['noofstops']
                );
            }
            $trip_packages_packages = isset($trip_packages_packages) ? $trip_packages_packages : false;
            if (isset($trip_project) && $trip_project['ID'] && ($service == 'Product' || $service == 'Project')) {
                /*//$service .= ',';*/
                $service = 'Product';
                $trip_projects_tasks = $this->TripModel->getSeekerTripProjectTasks($trip_project['ID']);
                if ($trip_projects_tasks) {
                    foreach ($trip_projects_tasks as $key => $project) {
                        $pro_att = $this->TripModel->getSeekerTripProjectTaskAttachment($project['project_task_id']);
                        if ($pro_att) {
                            $trip_projects_tasks[$key]['attachments'] = $pro_att;
                        }
                    }
                }
                $trip_projects_tasks = isset($trip_projects_tasks) ? $trip_projects_tasks : false;
                $trip_user = $this->CommonMethodsModel->getUser($trip['user']);
                $trip_origin_address = $this->CommonMethodsModel->getUserLocationByAddrId($trip['user_location']);
                $origin_location = $this->TripModel->getAirPort($trip['origin_location']);
                $destination_location = $this->TripModel->getAirPort($trip['destination_location']);
                $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $airportDetails = $this->TripModel->getLastFlightsRowDetails($trip['ID'], $destination_location_code);
                $arrivalDate = (isset($airportDetails['arrival_date']) && !empty($airportDetails['arrival_date'])) ? $airportDetails['arrival_date'] : '';
                $project_start_date = (isset($trip['project_start_date']) && !empty($trip['project_start_date'])) ? $trip['project_start_date'] : 0;
                $project_end_date = (isset($trip['project_end_date']) && !empty($trip['project_end_date'])) ? $trip['project_end_date'] : 0;
                $date_flexibility = (isset($trip['date_flexibility']) && !empty($trip['date_flexibility'])) ? $trip['date_flexibility'] : 0;
                $travel_plan_reservation_type = (isset($trip['travel_plan_reservation_type']) && !empty($trip['travel_plan_reservation_type'])) ? $trip['travel_plan_reservation_type'] : '';
                $arr_trip = array(
                    'ID' => $this->encrypt($trip['ID']),
					'trip_id_number' => $trip['trip_id_number'],
                    'name' => $trip['name'],
                    'user' => $trip['user'],
                    'origin_location' => $trip['origin_location'],
                    'origin_location_airname' => $this->TripModel->getAirPort($trip['origin_location']),
                    'destination_location' => $trip['destination_location'],
                    'destination_location_airname' => $this->TripModel->getAirPort($trip['destination_location']),
                    'ticket_image' => $trip['ticket_image'],
                    'trip_status' => $trip['trip_status'],
                    'active' => $trip['active'],
                    'origin_location_code' => $origin_location_code,
                    'destination_location_code' => $destination_location_code,
                    'origin_location_city' => $origin_location_city,
                    'destination_location_city' => $destination_location_city,
                    'origin_location_country' => $origin_location_country,
                    'destination_location_country' => $destination_location_country,
                    'airline_name' => $trip['airline_name'],
                    'flight_number' => $trip['operating_carrier_name'] . $trip['flight_number'],
                    'cabin' => $trip['cabin'],
                    'booking_status' => $trip['booking_status'],
                    'departure_date' => $trip['departure_date'],
                    'arrival_date' => $arrivalDate !='' ? $arrivalDate : $trip['arrival_date'],//$arrivalDate,
                    'date_flexibility' => $date_flexibility,
                    'travel_plan_reservation_type' => $travel_plan_reservation_type,
                    'project_start_date' => $project_start_date,
                    'project_end_date' => $project_end_date,
                    'service' => $service,
                    'project_request_count' => $trip_project_request,
                    'trip_project' => $trip_project,
                    'trip_project_task' => $trip_project_task,
                    'trip_origin_address' => $trip_origin_address,
                    'trip_user' => $trip_user,
                    'airport_details' => $airport_details,
                    'trip_projects_tasks' => $trip_projects_tasks,
                    'modified' => $trip['modified'],
                    'travel_agency_name' => $trip['travel_agency_name'],
                    'travel_agency_url' => $trip['travel_agency_url'],
                    'travel_agency_confirmation' => $trip['travel_agency_confirmation'],
                    'travel_agency_contact_name' => $trip['travel_agency_contact_name'],
                    'travel_agency_phone' => $trip['travel_agency_phone'],
                    'agency_email' => $trip['agency_email'],
                    'supplier_name' => $trip['supplier_name'],
                    'supplier_url' => $trip['supplier_url'],
                    'supplier_contact' => $trip['supplier_contact'],
                    'supplier_phone' => $trip['supplier_phone'],
                    'booking_site_name' => $trip['booking_site_name'],
                    'booking_site_url' => $trip['booking_site_url'],
                    'booking_site_phone' => $trip['booking_site_phone'],
                    'booking_site_email' => $trip['booking_site_email'],
                    'booking_date' => $trip['booking_date'],
                    'booking_reference' => $trip['booking_reference'],
					'booking_cost' => $trip['booking_cost'],
					'booking_cost_currency' => $trip['booking_cost_currency'],
                    'reservation_is_purchased' => $trip['reservation_is_purchased'],
                    'total_cost' => $trip['total_cost'],
                    'total_cost_currency' => $trip['total_cost_currency'],
                    'comments_restrictions' => $trip['comments_restrictions'],
                    'supplier_email' => $trip['supplier_email'],
                    'discount_code' => $trip['discount_code'],
                    'total_discount_cost' => $trip['total_discount_cost'],
                    'total_discount_currency' => $trip['total_discount_currency'],
                    'noofstops' => $trip['noofstops']
                );
            }
        }

        //echo '<pre>'; print_r($arr_trip);exit();
        return $arr_trip;
    }

    public function addSeekerPeopleService($new_seeker_trip_id)
    {
        extract($_POST);


        $seekerTripId = $new_seeker_trip_id;
        if (isset($seeker_trip_id) && $seeker_trip_id != '')
            $seekerTripId = $seeker_trip_id;
        
        $pac_coiunt = (isset($selected_passengers) && $selected_passengers != '10') ? $selected_passengers : $passengers_count_more;

        // if (!isset($passengers_count))

        /**/

        //  $pac_coiunt = 1;
        //  $pac_coiunt        = (isset($trip_edit_id) && !empty($trip_edit_id)) ? $passengers_count_more : $pac_coiunt;
        /* if(!isset($_POST['seeker_trip_id'])){            $seeker_trip_id = $new_seeker_trip_id;        }        $trip_passangers = (isset($passengers_count) && $passengers_count != '10')?$passengers_count:$passengers_count_more;        if(!isset($passengers_count))$trip_passangers=1; */
        /* $fees = $this->calculatePeopleServiceFee($new_seeker_trip_id,'people','seeker',$pac_coiunt);*/

        $male_member = 0;
        $female_member = 0;
        $member_above_60 = 0;
        $member_below_6 = 0;

        $user_details = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
        
        if ($user_details['gender'] == 'male' || $user_details['gender'] == 'Male') {
            $male_member = 1;
        }
        if ($user_details['gender'] == 'female' || $user_details['gender'] == 'Female') {
            $female_member = 1;
        }
        if ($user_details['age'] >= 60) {
            $member_above_60 = 1;
        }
        if ($user_details['age'] <= 6) {
            $member_below_6 = 1;
        }

       
        for ($ii = 1; $ii <= $pac_coiunt; $ii++) {
           
            if (isset($_POST['passenger_gender_'.$ii]) && $_POST['passenger_gender_'.$ii] == 'm') {
                $male_member++;
            }
            if (isset($_POST['passenger_gender_'.$ii]) && $_POST['passenger_gender_'.$ii] == 'f') {
                $female_member++;
            }
            if (isset($_POST['passenger_age_'.$ii]) && $_POST['passenger_age_'.$ii] <= 6) {
                $member_below_6++;
            }
            if (isset($_POST['passenger_age_'.$ii]) && $_POST['passenger_age_'.$ii] >= 60) {
                $member_above_60++;
            }
        }
         
        $arr_seeker_people = array(
            'seeker_trip' => $seekerTripId,
            'title' => '',
            'passengers' => $pac_coiunt,
            'contact_name' => $contact_name,
            'people_service_fee' => $people_service_fee,
            'one_of_the_passenger' => (isset($one_of_the_passenger) ? $one_of_the_passenger : '0'),
            'contact_number' => $contact_number,
            'email_address' => $email_address,
            'male_count' => $male_member,
            'female_count' => $female_member,
            'above_60' => $member_above_60,
            'below_6' => $member_below_6,
            'comment' => $comments
        );
        if (isset($seekerTripId) && !empty($seekerTripId))
            $this->TripModel->deleteSeekerPeopleById($seekerTripId);
        $seeker_people_request_id = $this->TripModel->addSeekerPeople($arr_seeker_people);
        for ($i = 1; $i <= $pac_coiunt; $i++) {
            $arr_seeker_people_passengers = array(
                'people_request' => $seeker_people_request_id,
                'first_name' => isset($_POST['passenger_first_name_'.$i]) ? $_POST['passenger_first_name_'.$i] : '',
                'last_name' => isset($_POST['passenger_last_name_'.$i]) ? $_POST['passenger_last_name_'.$i] : '',
                'gender' => isset($_POST['passenger_gender_'.$i]) ? $_POST['passenger_gender_'.$i] : '',
                'age' => isset($_POST['passenger_age_'.$i]) ? $_POST['passenger_age_'.$i] : '',
                'modified' => date('Y-m-d H:i:s')
            );
            //print_r($passengers_count);die;

            $passId = $this->TripModel->addSeekerPeoplePassengers($arr_seeker_people_passengers);
            if (isset($_POST['seekerPeopleDocImages_'.$i][0])) {
                foreach ($_POST['seekerPeopleDocImages_'.$i] as $fileName) {
                    $arr_attachment_detail = array(
                        'seeker_people_passengers_id' => $this->CommonMethodsModel->cleanQuery($passId),
                        'seeker_attachment_name' => $this->CommonMethodsModel->cleanQuery($fileName),
                        'seeker_attachment_added' => time()
                    );
                    $this->TripModel->addPassengerAttachment($arr_attachment_detail);
                }
            }
        }

       // print_r($pac_coiunt);print_r($male_member);print_r($female_member);die();
        return $seeker_people_request_id;
    }

    public function addSeekerPackageService($new_seeker_trip_id)
    {
        extract($_POST);
        $seekerTripId = $new_seeker_trip_id;
        if (isset($seeker_trip_id) && $seeker_trip_id != '')
            $seekerTripId = $seeker_trip_id;
        $pac_coiunt = (isset($packages_count) && $packages_count != '10') ? $packages_count : $packages_count_more;
        if (!isset($packages_count))
            $pac_coiunt = 1;
        $arr_seeker_package = array(
            'seeker_trip' => $seekerTripId,
            'title' => '',
            'packages' => $pac_coiunt,
            'total_weight' => $total_weight,
            'total_worth' => $total_worth,
            'package_service_fee' => $package_service_fee,
            'total_worth_currency' => $total_cost_currency_ajax,
            'contact_name' => $contact_name,
            'contact_number' => $contact_number,
            'email_address' => $contact_email_address,
            'comment' => $comments
        );
        if (isset($seekerTripId) && !empty($seekerTripId))
            $this->TripModel->deleteSeekerPackagesById($seekerTripId);
        $seeker_package_request_id = $this->TripModel->addSeekerPackage($arr_seeker_package);
        $tot_item_price = array();
        for ($i = 1; $i <= $pac_coiunt; $i++) {
            if (isset($_POST['negligible_weight_' . $i]) && $_POST['negligible_weight_' . $i])
                $negligible_weight = $_POST['negligible_weight_' . $i];
            else
                $negligible_weight = '';
            if (isset($_POST['negligible_size_' . $i]) && $_POST['negligible_size_' . $i])
                $negligible_size = $_POST['negligible_size_' . $i];
            else
                $negligible_size = '';
            if (isset($_FILES['package_photo_' . $i]['tmp_name']) && $_FILES['package_photo_' . $i]['tmp_name'] != '') {
                $fileUpRes = $this->CommonMethodsModel->uploadFiles('package_photo_' . $i, 'seeker_package_' . rand(0,30).time(), 'seeker_people_attachment');
                if ($fileUpRes['result'] == 'success') {
                    $imageName = $fileUpRes['fileName'];
                } else {
                    $imageName = NULL;
                }
            } else if (isset($_POST['package_photo_edited_' . $i]) && !empty($_POST['package_photo_edited_' . $i])) {
                $imageName = $_POST['package_photo_edited_' . $i];
			}
            $arr_seeker_package_packages = array(
                'package_request' => $seeker_package_request_id,
                'item_category' => $_POST['category_' . $i],
                'item_sub_category' => $_POST['item_category_' . $i],
                'item_description' => $_POST['item_description_' . $i],
                'color' => $_POST['color_' . $i],
                'condition' => $_POST['condition_' . $i],
                /* 'web_site_link'      => $_POST['web_site_link_'.$i],*/
                'photo' => isset($imageName) ? $imageName : '',
                'item_worth' => $_POST['item_worth_' . $i],
                'item_worth_currency' => $_POST['total_cost_currency_ajax'],
                'weight' => isset($_POST['item_weight_' . $i]) ? $_POST['item_weight_' . $i] : '',
                'negligible_weight' => $negligible_weight,
                'length' => $_POST['item_length_' . $i],
                'width' => isset($_POST['item_width_' . $i]) ? $_POST['item_width_' . $i] : '',
                'height' => isset($_POST['item_height_' . $i]) ? $_POST['item_height_' . $i] : '',
                'item_unit' => isset($_POST['item_unit_' . $i]) ? $_POST['item_unit_' . $i] : '',
                'negligible_size' => $negligible_size,
                'item_quantity' => isset($_POST['item_quantity_' . $i]) ? $_POST['item_quantity_' . $i] : '',
                'modified' => date('Y-m-d H:i:s')
            );
            $this->TripModel->addSeekerPackagePackages($arr_seeker_package_packages);
        }
        return $seeker_package_request_id;
    }

    public function addSeekerProjectService($new_seeker_trip_id)
    {
        extract($_POST);
        $ttip_id = 0;
        if (!isset($_POST['seeker_trip_id']))
            $seeker_trip_id = $new_seeker_trip_id;
        if ($work_category == '1') {
            /*$productCounts = (isset($products_count) && $products_count != '10')?$products_count:$products_count_more;        if(!isset($products_count))$productCounts=$products_count;*/
            $productCounts = $selected_project_products;
            /*echo $productCounts;exit;*/
            $arr_seeker_project = array(
                'seeker_trip' => $seeker_trip_id,
                'title' => '',
                'tasks' => $productCounts,
                'project_service_fee' => $project_service_fee,
                'total_weight' => $total_weight,
                'total_worth' => $total_worth,
                'total_worth_currency' => $total_worth_currency,
                'work_category' => $work_category,
                'contact_name' => $contact_name,
                'contact_number' => $contact_number,
                'email_address' => $email_address,
                'comment' => $comments
            );
            if (isset($seeker_trip_id) && !empty($seeker_trip_id))
                $this->TripModel->deleteSeekerProjectById($seeker_trip_id);
            $seeker_project_request_id = $this->TripModel->addSeekerProject($arr_seeker_project);
            for ($i = 1; $i <= $productCounts; $i++) {
                $arr_seeker_project_tasks = array(
                    'project_request' => $seeker_project_request_id,
                    'category' => (isset($_POST['product_category_' . $i]) ? $_POST['product_category_' . $i] : ''),
                    'description' => (isset($_POST['product_description_' . $i]) ? $_POST['product_description_' . $i] : ''),
                    'additional_requirements_category' => (isset($_POST['product_additional_requirements_category_' . $i]) ? $_POST['product_additional_requirements_category_' . $i] : ''),
                    'product_sku' => (isset($_POST['product_product_sku_' . $i]) ? $_POST['product_product_sku_' . $i] : ''),
                    'web_site_link' => (isset($_POST['product_web_site_link_' . $i]) ? $_POST['product_web_site_link_' . $i] : ''),
                    'price' => (isset($_POST['product_price_' . $i]) ? $_POST['product_price_' . $i] : ''),
                    'price_currency' => (isset($_POST['product_price_currency_' . $i]) ? $_POST['product_price_currency_' . $i] : ''),
                    'item_weight' => (isset($_POST['item_weight_' . $i]) ? $_POST['item_weight_' . $i] : ''),
                    'item_length' => (isset($_POST['item_length_' . $i]) ? $_POST['item_length_' . $i] : ''),
                    'item_width' => (isset($_POST['item_width_' . $i]) ? $_POST['item_width_' . $i] : ''),
                    'item_height' => (isset($_POST['item_height_' . $i]) ? $_POST['item_height_' . $i] : ''),
                    'item_unit' => (isset($_POST['item_unit_' . $i]) ? $_POST['item_unit_' . $i] : ''),
                    'negligible_weight' => (isset($_POST['negligible_weight_' . $i]) ? $_POST['negligible_weight_' . $i] : ''),
                    'negligible_size' => (isset($_POST['negligible_size_' . $i]) ? $_POST['negligible_size_' . $i] : ''),
                    'color' => (isset($_POST['product_color_' . $i]) ? $_POST['product_color_' . $i] : ''),
                    'task_condition' => (isset($_POST['product_condition_' . $i]) ? $_POST['product_condition_' . $i] : ''),
                    'quantity' => (isset($_POST['item_quantity_' . $i]) ? $_POST['item_quantity_' . $i] : ''),
					'purchase_type' => (isset($_POST['purchase_type_' . $i]) ? $_POST['purchase_type_' . $i] : ''),
					'store_name' => (isset($_POST['store_name_' . $i]) ? $_POST['store_name_' . $i] : ''),
					'store_address' => (isset($_POST['store_address_' . $i]) ? $_POST['store_address_' . $i] : ''),
                    'modified' => time()
                );
                $proId = $this->TripModel->addSeekerProjectTasks($arr_seeker_project_tasks);
                if (isset($_POST['seekerPeopleDocImages_' . $i][0])) {
                    foreach ($_POST['seekerPeopleDocImages_' . $i] as $fileName) {
                        $arr_attachment_detail = array(
                            'seeker_project_tasks_id' => $this->CommonMethodsModel->cleanQuery($proId),
                            'task_photo_name' => $this->CommonMethodsModel->cleanQuery($fileName),
                            'task_photo_added' => time()
                        );
                        $this->TripModel->addProjectAttachment($arr_attachment_detail);
                    }
                }

            }

        } else {
            /*$projectCounts = (isset($projects_count) && $projects_count != '10')?$projects_count:$projects_count_more;        //if(!isset($projects_count))$projectCounts=$projects_count;*/
            $projectCounts = $selected_project_tasks;
            $arr_seeker_project = array(
                'seeker_trip' => $seeker_trip_id,
                'title' => '',
                'tasks' => $projectCounts,
                'project_service_fee' => $project_service_fee,
                'work_category' => $work_category,
                'contact_name' => $contact_name,
                'contact_number' => $contact_number,
                'email_address' => $email_address,
                'comment' => $comments
            );
            $this->TripModel->deleteSeekerProjectById($seeker_trip_id);
            $seeker_project_request_id = $this->TripModel->addSeekerProject($arr_seeker_project);
            for ($i = 1; $i <= $projectCounts; $i++) {
                $arr_seeker_project_tasks = array(
                    'project_request' => $seeker_project_request_id,
                    'category' => $_POST['task_category_' . $i],
                    'description' => $_POST['task_description_' . $i],
                    'additional_requirements_category' => $_POST['additional_requirements_category_' . $i],
                    'service_date' => strtotime($_POST['service_date_' . $i]),
                    'date_flexibility' => $_POST['date_flexibility_' . $i],
                    'start_date' => date('Y-m-d h:i:s', strtotime($_POST['service_start_date_' . $i])),
                    'end_date' => date('Y-m-d h:i:s', strtotime($_POST['service_end_date_' . $i])),
                    'project_duration' => $_POST['task_duration_' . $i],
                    'requires_local_travel' => (isset($_POST['requires_local_travel_' . $i]) ? $_POST['requires_local_travel_' . $i] : ''),
                    'payout_type' => (isset($_POST['payout_type_' . $i]) ? $_POST['payout_type_' . $i] : ''),
                    'cost_willing_to_pay' => $_POST['cost_willing_to_pay_' . $i],
                    'currency_of_payment' => $_POST['currency_of_payment_' . $i],
                    'modified' => time()
                );
                $this->TripModel->addSeekerProjectTasks($arr_seeker_project_tasks);


            }

        }
        return $seeker_project_request_id;
    }

    public function addTripOrginLocation()
    {  
      //print_r($_REQUEST);die;
        extract($_POST);
                 
        if($this->sessionObj->offsetGet('userid')){
            
            $users_id = $this->sessionObj->offsetGet('userid');
        }
        else
        {
            $users_id =$user_id;
        }
        
        $contact_address_type = $this->CommonMethodsModel->cleanQuery(isset($_POST['contact_address_type']) ? $_POST['contact_address_type'] : '');
        if (isset($contact_address_type) && $contact_address_type == 'existing_address')
            $user_location = $this->CommonMethodsModel->cleanQuery($_POST['existing_user_location']);
        else {
            $trip_origin_street_address = $this->CommonMethodsModel->cleanQuery($route);
            $arr_trip_contact = array(
                'user' => $users_id,
                'name' => "",
                'street_address_1' => $this->CommonMethodsModel->cleanQuery($route),
                'street_address_2' => $this->CommonMethodsModel->cleanQuery($sublocality),
                'city' => $this->CommonMethodsModel->cleanQuery($locality),
                'state' => $this->CommonMethodsModel->cleanQuery($administrative_area_level_1),
                'zip_code' => $this->CommonMethodsModel->cleanQuery($postal_code),
                'country' => $this->CommonMethodsModel->cleanQuery($country_short),
                'latitude' => $this->CommonMethodsModel->cleanQuery($lat),
                'longitude' => $this->CommonMethodsModel->cleanQuery($lng),
                'modified' => date('Y-m-d H:i:s'),
				'tripId' => 0
            );
			if(isset($tripId) && $tripId != ''){
				$arr_trip_contact['tripId'] = $tripId;
			}
			
              $user_location = $this->TripModel->addTripContactAddress($arr_trip_contact);
			
			
        }
        // print_r($user_location);
        return $user_location;
    }

    public function addTripDetail($user_location)
    {
        
		if ($this->sessionObj->offsetGet('userrole') == 'seeker') {
			
			if(isset($_POST['type_of_service']) && $_POST['type_of_service'] == 'package'){
				$serviceFee = $this->TripModel->getSeekerPackageServiceFee($_POST);
			}
			if(isset($_POST['type_of_service']) && $_POST['type_of_service'] == 'people'){
				$_POST['people_count'] = (isset($_POST['selected_passengers']) && $_POST['selected_passengers'] != '10') ? $_POST['selected_passengers'] : $_POST['passengers_count_more'];
				$serviceFee = $this->TripModel->getSeekerPeopleServiceFee($_POST);
			}
			if(isset($_POST['type_of_service']) && $_POST['type_of_service'] == 'project'){
				$serviceFee = $this->TripModel->getSeekerProjectServiceFee($_POST);
			}
			$totalServiceFee = $serviceFee;
			$convertedPrice  = $this->TripModel->getServiceFeeConverstion($totalServiceFee);
			$_POST['package_service_fee'] 			= $totalServiceFee;
			$_POST['total_cost_ajax'] 				= $convertedPrice['convertedPrice'];
			$_POST['currency_conversion_rate_ajax'] = $convertedPrice['conversionRate'];
			$_POST['total_cost_currency_ajax'] 		= $this->sessionObj->offsetGet('currency');

            //print_r($_POST);exit();

		} else if ($this->sessionObj->offsetGet('userrole') == 'traveller') {
			
			$_POST['people_count'] = 1;
			$serviceFee = $this->TripModel->getSeekerPeopleServiceFee($_POST);
				
			$totalServiceFee = $serviceFee;
			$convertedPrice  = $this->TripModel->getServiceFeeConverstion($totalServiceFee);
			$_POST['package_service_fee'] 			= $totalServiceFee;
			$_POST['total_cost_ajax'] 				= $convertedPrice['convertedTravellerPrice'];
			$_POST['currency_conversion_rate_ajax'] = $convertedPrice['conversionRate'];
			$_POST['total_cost_currency_ajax'] 		= $this->sessionObj->offsetGet('currency');
				
		}

		extract($_POST);

        if (isset($travel_plan_type) && $travel_plan_type == 'existing_plan')
        {
            $ID = $this->CommonMethodsModel->cleanQuery(($travel_plan));

			$existingTripInfo = $this->TripModel->getTrip($ID,$this->sessionObj->userrole);
			unset($existingTripInfo['ID']);

			$ID = $this->TripModel->addTrip($existingTripInfo, $this->sessionObj->userrole);
			
            $arr_trip_detail = array(
                'travel_agency_name' => $this->CommonMethodsModel->cleanQuery(isset($travel_agency_name_ajax) ? $travel_agency_name_ajax : ''),
                'travel_agency_url' => $this->CommonMethodsModel->cleanQuery(isset($travel_agency_url_ajax) ? $travel_agency_url_ajax : ''),
                'travel_agency_confirmation' => $this->CommonMethodsModel->cleanQuery(isset($travel_agency_confirmation_ajax) ? $travel_agency_confirmation_ajax : ''),
                'travel_agency_contact_name' => $this->CommonMethodsModel->cleanQuery(isset($travel_agency_contact_name_ajax) ? $travel_agency_contact_name_ajax : ''),
                'travel_agency_phone' => $this->CommonMethodsModel->cleanQuery(isset($travel_agency_phone_ajax) ? $travel_agency_phone_ajax : ''),
                'agency_email' => $this->CommonMethodsModel->cleanQuery(isset($agency_email_ajax) ? $agency_email_ajax : ''),
                'booking_site_name' => $this->CommonMethodsModel->cleanQuery(isset($booking_site_name_ajax) ? $booking_site_name_ajax : ''),
                'booking_site_url' => $this->CommonMethodsModel->cleanQuery(isset($booking_site_url_ajax) ? $booking_site_url_ajax : ''),
                'booking_site_phone' => $this->CommonMethodsModel->cleanQuery(isset($booking_site_phone_ajax) ? $booking_site_phone_ajax : ''),
                'booking_site_email' => $this->CommonMethodsModel->cleanQuery(isset($booking_site_email_ajax) ? $booking_site_email_ajax : ''),
                'booking_date' => strtotime($this->CommonMethodsModel->cleanQuery(isset($booking_date_ajax) ? $booking_date_ajax : '')),
                'booking_reference' => $this->CommonMethodsModel->cleanQuery(isset($booking_reference_ajax) ? $booking_reference_ajax : ''),
				'booking_cost'=> $this->CommonMethodsModel->cleanQuery(isset($total_cost) ? $total_cost : ''),
				'booking_cost_currency'=> $this->CommonMethodsModel->cleanQuery(isset($total_cost_currency) ? $total_cost_currency : ''),
                'total_cost' => $this->CommonMethodsModel->cleanQuery(isset($total_cost_ajax) ? $total_cost_ajax : ''),
                'conversion_rate' => $this->CommonMethodsModel->cleanQuery(isset($currency_conversion_rate_ajax) ? $currency_conversion_rate_ajax : ''),
                'total_cost_currency' => $this->CommonMethodsModel->cleanQuery(isset($total_cost_currency_ajax) ? $total_cost_currency_ajax : ''),
                'discount_code' => $this->CommonMethodsModel->cleanQuery(isset($discount_code) ? $discount_code : ''),
                'total_discount_cost' => $this->CommonMethodsModel->cleanQuery(isset($total_discount_cost_ajax) ? $total_discount_cost_ajax : ''),
                'total_discount_currency' => $this->CommonMethodsModel->cleanQuery(isset($total_discount_currency_ajax) ? $total_discount_currency_ajax : ''),
				'ticket_image'=>'',
                'comments_restrictions' => $this->CommonMethodsModel->cleanQuery(isset($comments_restrictions_ajax) ? $comments_restrictions_ajax : ''),
				'reservation_is_purchased'=> $this->CommonMethodsModel->cleanQuery(isset($reservation_is_purchased) ? $reservation_is_purchased : '')
            );
			$tripIdNumber = ($this->sessionObj->userrole == 'seeker') ? 'S' : 'T';
			$tripIdNumber .= $this->sessionObj->userid.'TRIP'.date('Ymdhi');
			$arr_trip_detail['trip_id_number'] = isset($tripIdNumber) ? $this->CommonMethodsModel->cleanQuery($tripIdNumber) : 0;
			$arr_trip_detail['created'] = date('Y-m-d H:i:s');
			$arr_trip_detail['modified'] = date('Y-m-d H:i:s');
			
            if ($this->sessionObj->offsetGet('userrole') == 'seeker') {
                $this->TripModel->updateSeekerTripById($arr_trip_detail, $ID);
            } else {
                $this->TripModel->updateTripById($arr_trip_detail, $ID);
            }
        }
        else
        {
            $departure_date = (isset($departure_date)) ? $this->CommonMethodsModel->cleanQuery($departure_date) : 0;
            $departure_time = (isset($departure_time)) ? $this->CommonMethodsModel->cleanQuery($departure_time) : '23:59:59';
            $arrival_date = (isset($arrival_date)) ? $this->CommonMethodsModel->cleanQuery($arrival_date) : 0;
            $arrival_time = isset($arrival_date) ? $this->CommonMethodsModel->cleanQuery($arrival_date) : '23:59:59';
            $arrival_date_time = date('Y-m-d H:i:s', strtotime($arrival_date . ' ' . $arrival_time));
            $departure_date_time = date('Y-m-d H:i:s', strtotime($departure_date . ' ' . $departure_time));
            $date_flexibility = (isset($date_flexibility)) ? $this->CommonMethodsModel->cleanQuery($date_flexibility) : 0;
            if (isset($contact_address_type) && $contact_address_type == 'existing_address'){
                 $user_location = $existing_user_location;
            
            }
            if (isset($travel_plan_reservation_type) && $travel_plan_reservation_type == 2) {
                $origin_location_id = $unplanned_origin_location_id;
                $destination_location_id = $unplanned_destination_location_id;
                $departure_date_time = date('Y-m-d H:i:s', strtotime($unplanned_departure_date));
            } else {
                $travel_plan_reservation_type = 0;
            }
            if (!isset($distance))
                $distance = 0;
            if (!isset($service_fee))
                $service_fee = 0;

            if ($this->sessionObj->offsetGet('userrole') == 'seeker')
            {
                if (isset($selectedFlightData) && $selectedFlightData != "") {
                    $data_as_string = (string)$selectedFlightData;
                    $new_aaray = json_decode($data_as_string, true);
                    $departure_date_new = $new_aaray[0]['departure_date'];
                    $arrival_date_new = $new_aaray[count($new_aaray) - 1]['arrival_date'];
                }
                else {
                    $departure_date_new = $departure_date_time;
                    $arrival_date_new = $arrival_date_time;
                }
                $arr_trip_detail = array
                (
                    'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->user['ID']),
                    'user_location' => $this->CommonMethodsModel->cleanQuery($user_location),
                    'travel_plan_reservation_type' => $this->CommonMethodsModel->cleanQuery(isset($_POST['travel_plan_reservation_type']) ? $_POST['travel_plan_reservation_type'] : ''),
                    'date_flexibility' => $this->CommonMethodsModel->cleanQuery($date_flexibility),
                    //'distance' => $this->CommonMethodsModel->cleanQuery($distance),
                    'service_fee' => $this->CommonMethodsModel->cleanQuery($service_fee),
                    'departure_date' => $this->CommonMethodsModel->cleanQuery($departure_date_new),
                    'arrival_date' => $this->CommonMethodsModel->cleanQuery($arrival_date_new),
                    'booking_status' => $this->CommonMethodsModel->cleanQuery(isset($booking_status) ? $booking_status : ''),
                    'cabin' => $this->CommonMethodsModel->cleanQuery(isset($cabin) ? $cabin : ''),
                    'seat' => $this->CommonMethodsModel->cleanQuery(isset($seat) ? $seat : ''),
                    'project_start_date' => $this->CommonMethodsModel->cleanQuery(isset($project_start_date) ? date('Y-m-d H:i:s', strtotime($project_start_date)) : 0),
                    'project_end_date' => $this->CommonMethodsModel->cleanQuery(isset($project_end_date) ? date('Y-m-d H:i:s', strtotime($project_end_date)) : 0),
                    'ticket_option' => $this->CommonMethodsModel->cleanQuery(isset($ticketOption) ? $ticketOption : NULL),
                    /*optional fields */
                    /* 'ticket_number' => $this->CommonMethodsModel->cleanQuery($ticket_number),*/
                    'travel_agency_name' => $this->CommonMethodsModel->cleanQuery(isset($travel_agency_name) ? $travel_agency_name : ''),
                    'travel_agency_url' => $this->CommonMethodsModel->cleanQuery(isset($travel_agency_url) ? $travel_agency_url : ''),
                    'travel_agency_confirmation' => $this->CommonMethodsModel->cleanQuery(isset($travel_agency_confirmation) ? $travel_agency_confirmation : ''),
                    'travel_agency_contact_name' => $this->CommonMethodsModel->cleanQuery(isset($travel_agency_contact_name) ? $travel_agency_contact_name : ''),
                    'travel_agency_phone' => $this->CommonMethodsModel->cleanQuery(isset($travel_agency_phone) ? $travel_agency_phone : ''),
                    'agency_email' => $this->CommonMethodsModel->cleanQuery(isset($agency_email) ? $agency_email : ''),
                    /* 'supplier_name' => $this->CommonMethodsModel->cleanQuery(isset($supplier_name)?$supplier_name:''),                    'supplier_url' => $this->CommonMethodsModel->cleanQuery(isset($supplier_url)?$supplier_url:''),                    'supplier_contact' => $this->CommonMethodsModel->cleanQuery(isset($supplier_contact)?$supplier_contact:''),                    'supplier_phone' => $this->CommonMethodsModel->cleanQuery(isset($supplier_phone)?$supplier_phone:''),                    'supplier_email' => $this->CommonMethodsModel->cleanQuery(isset($supplier_email)?$supplier_email:''),*/
                    'booking_site_name' => $this->CommonMethodsModel->cleanQuery(isset($booking_site_name) ? $booking_site_name : ''),
                    'booking_site_url' => $this->CommonMethodsModel->cleanQuery(isset($booking_site_url) ? $booking_site_url : ''),
                    'booking_site_phone' => $this->CommonMethodsModel->cleanQuery(isset($booking_site_phone) ? $booking_site_phone : ''),
                    'booking_site_email' => $this->CommonMethodsModel->cleanQuery(isset($booking_site_email) ? $booking_site_email : ''),
                    'booking_date' => strtotime($this->CommonMethodsModel->cleanQuery(isset($booking_date) ? $booking_date : '')),
                    'booking_reference' => $this->CommonMethodsModel->cleanQuery(isset($booking_reference) ? $booking_reference : ''),
					'booking_cost'=> $this->CommonMethodsModel->cleanQuery(isset($total_cost) ? $total_cost : ''),
					'booking_cost_currency'=> $this->CommonMethodsModel->cleanQuery(isset($total_cost_currency) ? $total_cost_currency : ''),
                    'total_cost' => $this->CommonMethodsModel->cleanQuery(isset($total_cost_ajax) ? $total_cost_ajax : ''),
                    'conversion_rate' => $this->CommonMethodsModel->cleanQuery(isset($currency_conversion_rate_ajax) ? $currency_conversion_rate_ajax : ''),
                    'total_cost_currency' => $this->CommonMethodsModel->cleanQuery(isset($total_cost_currency_ajax) ? $total_cost_currency_ajax : ''),
                    'discount_code' => $this->CommonMethodsModel->cleanQuery(isset($discount_code) ? $discount_code : ''),
                    'total_discount_cost' => $this->CommonMethodsModel->cleanQuery(isset($total_discount_cost) ? $total_discount_cost : ''),
                    'total_discount_currency' => $this->CommonMethodsModel->cleanQuery(isset($total_discount_currency) ? $total_discount_currency : ''),
                    'trip_route' => $this->CommonMethodsModel->cleanQuery(isset($trip_route) ? $trip_route : ''),
                    'comments_restrictions' => $this->CommonMethodsModel->cleanQuery(isset($comments_restrictions) ? $comments_restrictions : ''),
					'reservation_is_purchased'=> $this->CommonMethodsModel->cleanQuery(isset($reservation_is_purchased) ? $reservation_is_purchased : ''),
					'ticket_image'=>'',
                    /*optional fields */
                    'active' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                );
                if (isset($_POST['travel_plan_reservation_type']) && $_POST['travel_plan_reservation_type'] == '2')
                {
                    $o_location_id = (isset($unplanned_origin_location_id) && $unplanned_origin_location_id != '') ? $unplanned_origin_location_id : $origin_location_id;
                    $d_location_id = (isset($unplanned_destination_location_id) && $unplanned_destination_location_id != '') ? $unplanned_destination_location_id : $destination_location_id;
                    $arr_trip_detail['origin_location'] = $this->CommonMethodsModel->cleanQuery($o_location_id);
                    $arr_trip_detail['destination_location'] = $this->CommonMethodsModel->cleanQuery($d_location_id);
                    $arr_trip_detail['distance'] = $this->CommonMethodsModel->cleanQuery($distance_unplan);
                }
                else
                {
                    $arr_trip_detail['distance'] = $this->CommonMethodsModel->cleanQuery($distance);
                    $arr_trip_detail['origin_location'] = $this->CommonMethodsModel->cleanQuery($origin_location_id);
                    $arr_trip_detail['destination_location'] = $this->CommonMethodsModel->cleanQuery($destination_location_id);
                }
            }
            else
            { 
                if (isset($selectedFlightData) && $selectedFlightData != '')
                {
                    $data_as_string = (string)$selectedFlightData;
                    $new_aaray = json_decode($data_as_string, true);
                    $departure_date_new = $new_aaray[0]['departure_date'];
                    $arrival_date_new = $new_aaray[count($new_aaray) - 1]['arrival_date'];
                }
                else
                {
                    $departure_date_new = $departure_date_time;
                    $arrival_date_new = $arrival_date_time;
                }
                $arr_trip_detail = array
                (
                    'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->user['ID']),
                    'user_location' => $this->CommonMethodsModel->cleanQuery($user_location),
                    'distance' => $this->CommonMethodsModel->cleanQuery($distance),
                    'service_fee' => $this->CommonMethodsModel->cleanQuery($service_fee),
                    'origin_location' => $this->CommonMethodsModel->cleanQuery($origin_location_id),
                    'destination_location' => $this->CommonMethodsModel->cleanQuery($destination_location_id),
                    'departure_date' => $this->CommonMethodsModel->cleanQuery($departure_date_new),
                    'arrival_date' => $this->CommonMethodsModel->cleanQuery($arrival_date_new),
                    'booking_status' => $this->CommonMethodsModel->cleanQuery($booking_status),
                    'cabin' => $this->CommonMethodsModel->cleanQuery($cabin),
                    'ticket_option' => $this->CommonMethodsModel->cleanQuery(isset($ticketOption) ? $ticketOption : NULL),
                    'travel_agency_name' => $this->CommonMethodsModel->cleanQuery($travel_agency_name),
                    'travel_agency_url' => $this->CommonMethodsModel->cleanQuery($travel_agency_url),
                    'travel_agency_confirmation' => $this->CommonMethodsModel->cleanQuery($travel_agency_confirmation),
                    'travel_agency_contact_name' => $this->CommonMethodsModel->cleanQuery($travel_agency_contact_name),
                    'travel_agency_phone' => $this->CommonMethodsModel->cleanQuery($travel_agency_phone),
                    'agency_email' => $this->CommonMethodsModel->cleanQuery($agency_email),
                    'booking_site_name' => $this->CommonMethodsModel->cleanQuery($booking_site_name),
                    'booking_site_url' => $this->CommonMethodsModel->cleanQuery($booking_site_url),
                    'booking_site_phone' => $this->CommonMethodsModel->cleanQuery($booking_site_phone),
                    'booking_site_email' => $this->CommonMethodsModel->cleanQuery($booking_site_email),
                    'booking_date' => strtotime($this->CommonMethodsModel->cleanQuery($booking_date)),
                    'booking_reference' => $this->CommonMethodsModel->cleanQuery($booking_reference),
					'booking_cost'=> $this->CommonMethodsModel->cleanQuery(isset($total_cost) ? $total_cost : ''),
					'booking_cost_currency'=> $this->CommonMethodsModel->cleanQuery(isset($total_cost_currency) ? $total_cost_currency : ''),
                    'total_cost' => $this->CommonMethodsModel->cleanQuery(isset($total_cost_ajax) ? $total_cost_ajax : ''),
                   	'conversion_rate' => $this->CommonMethodsModel->cleanQuery(isset($currency_conversion_rate_ajax) ? $currency_conversion_rate_ajax : ''),
                    'total_cost_currency' => $this->CommonMethodsModel->cleanQuery(isset($total_cost_currency_ajax) ? $total_cost_currency_ajax : ''),
                    'discount_code' => $this->CommonMethodsModel->cleanQuery(isset($discount_code) ? $discount_code : ''),
                    'total_discount_cost' => $this->CommonMethodsModel->cleanQuery(isset($total_discount_cost) ? $total_discount_cost : ''),
                    'total_discount_currency' => $this->CommonMethodsModel->cleanQuery(isset($total_discount_currency) ? $total_discount_currency : ''),
                    'trip_route' => $this->CommonMethodsModel->cleanQuery(isset($trip_route) ? $trip_route : ''),
                    'comments_restrictions' => $this->CommonMethodsModel->cleanQuery($comments_restrictions),
					'reservation_is_purchased'=> $this->CommonMethodsModel->cleanQuery(isset($reservation_is_purchased) ? $reservation_is_purchased : ''),
					'ticket_image'=>'',
                    'active' => 1,
                    'created' => date('Y-m-d H:i:s'),
                    'modified' => date('Y-m-d H:i:s')
                );
            }
            if (isset($typeOfFlightSelect) && $typeOfFlightSelect == 'auto')
            {
                $arr_trip_detail['noofstops'] = $this->CommonMethodsModel->cleanQuery(isset($nstop) ? $nstop : 0);
            }
            else if (isset($typeOfFlightSelect) && $typeOfFlightSelect == 'manual')
            {
                $arr_trip_detail['noofstops'] = $this->CommonMethodsModel->cleanQuery($nstop_manual);
            }
            $arr_trip_detail['trip_id_number'] = isset($tripIdNumber) ? $this->CommonMethodsModel->cleanQuery($tripIdNumber) : 0;
            $editTripId = isset($trip_edit_id) ? $trip_edit_id : 0;

            if ($editTripId) {
                if ($this->sessionObj->offsetGet('userrole') == 'seeker') {
                    $this->TripModel->updateSeekerTripById($arr_trip_detail, $editTripId);
                } else {
                    $this->TripModel->updateTripById($arr_trip_detail, $editTripId);
                }
                $ID = $editTripId;
            } else {
                $ID = $this->TripModel->addTrip($arr_trip_detail, $this->sessionObj->userrole);
            }

			$_POST['people_service_fee'] = $serviceFee;
			$_POST['package_service_fee'] = $serviceFee;
			$_POST['project_service_fee'] = $serviceFee;

            $flights = $this->sessionObj->flights;
            if (!empty($selectedFlightData) && $selectedFlightData != false) {
                $this->TripModel->deleteTripFlightDetails($ID);
                foreach (json_decode($selectedFlightData) as $sFlight) {
                    $arr_flight = array(
                        'user_role' => $this->sessionObj->userrole,
                        'trip' => $ID,
                        'airline_name' => $this->CommonMethodsModel->cleanQuery($sFlight->airline_name),
                        'airline_number' => $this->CommonMethodsModel->cleanQuery($sFlight->airline_number),
                        'carrier' => $this->CommonMethodsModel->cleanQuery($sFlight->carrier),
                        'number' => $this->CommonMethodsModel->cleanQuery($sFlight->number),
                        'departure' => $this->CommonMethodsModel->cleanQuery($sFlight->departure),
                        'arrival' => $this->CommonMethodsModel->cleanQuery($sFlight->arrival),
                        'departure_date' => $this->CommonMethodsModel->cleanQuery($sFlight->departure_date),
                        'arrival_date' => $this->CommonMethodsModel->cleanQuery($sFlight->arrival_date),
                        'depature_latitude' => $this->CommonMethodsModel->cleanQuery($sFlight->depature_latitude),
                        'depature_longitude' => $this->CommonMethodsModel->cleanQuery($sFlight->depature_longitude),
                        'arrival_latitude' => $this->CommonMethodsModel->cleanQuery($sFlight->arrival_latitude),
                        'arrival_longitude' => $this->CommonMethodsModel->cleanQuery($sFlight->arrival_longitude),
                        'distance' => $this->CommonMethodsModel->cleanQuery($sFlight->distance),
                        'modified' => date('Y-m-d H:i:s')
                    );
                    $this->TripModel->addTripFlight($arr_flight);
                }
            }
        }
        return $ID;
    }

    public function uploadTicket()
    {
        $image_path = "tickets";
        $field_name = 'ticket_image';
        $file_name_prefix = $tripIdNumber;
        $configArray = array(
            array(
                'image_x' => 150,
                'sub_dir_name' => 'medium'
            ),
            array(
                'image_x' => 100,
                'sub_dir_name' => 'small'
            )
        );
        $imgResult = $this->CommonMethodsModel->uploadUserDP($field_name, $file_name_prefix, $image_path, $configArray);
        if ($imgResult['result'] == 'success') {
            $idverfy_image = $imgResult['fileName'];
            $arr_photo_detail = array(
                'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->offsetGet('userid')),
                'user_photo_name' => $this->CommonMethodsModel->cleanQuery($idverfy_image),
                'user_photo_added' => time()
            );
            $this->ProfileModel->addUserPhoto($arr_photo_detail, $this->sessionObj->offsetGet('userid'));
            $this->sessionObj->offsetSet('successMsg', 'Your uploaded photo has been added successfully.');
        }
    }

    public function addTravellerPeopleDetail($trip_ID)
    {
        extract($_POST);
        $trip_people_female = $trip_people_male = 0;
        $trip_people = (isset($trip_people_count) && $trip_people_count != '11') ? $trip_people_count : $trip_people_count_more;
        if (isset($trip_people_male_count)) {
            $trip_people_male = (isset($trip_people_male_count) && $trip_people_male_count != '11') ? $trip_people_male_count : $trip_people_male_count_more;
        }
        if (isset($trip_people_female_count)) {
            $trip_people_female = (isset($trip_people_female_count) && $trip_people_female_count != '11') ? $trip_people_female_count : $trip_people_female_count_more;
        }
        //$fees = $this->calculatePeopleServiceFee($trip_ID,'people','traveller',$trip_people);
        $arr_people_detail = array(
            'trip' => $this->CommonMethodsModel->cleanQuery($trip_ID),
            'persons_travelling' => $this->CommonMethodsModel->cleanQuery($trip_people),
            'male_count' => $this->CommonMethodsModel->cleanQuery($trip_people_male),
            'people_service_fee' => $this->CommonMethodsModel->cleanQuery($people_service_fee),
            'female_count' => $this->CommonMethodsModel->cleanQuery($trip_people_female),
            'age_above_60_years' => $this->CommonMethodsModel->cleanQuery(isset($trip_people_age_bove_60) ? $trip_people_age_bove_60 : 'No'),
            'age_below_3_years' => $this->CommonMethodsModel->cleanQuery(isset($trip_people_age_below_5) ? $trip_people_age_below_5 : 'No'),
            'comment' => $this->CommonMethodsModel->cleanQuery($trip_comments),
            'modified' => date('Y-m-d H:i:s')
        );
        $this->TripModel->deleteTripPeopleDetail($trip_ID);
        $ID = $this->TripModel->addTripPeopleDetail($arr_people_detail);
        return $ID;
    }

    public function addTravellerPackageDetail($trip_ID)
    {
        extract($_POST);
        $packages = (isset($packages_count) && $packages_count != '11') ? $packages_count : $packages_count_more;
      /*  if (!$weight_carrieds) {
            $weight_carrieds = $weight_carried;
        }*/
        //$fees = $this->calculatePackageServiceFee($trip_ID,'package','traveller',$weight_accommodate);

        $arr_package_detail = array(
            'trip' => $this->CommonMethodsModel->cleanQuery($trip_ID),
            'packages_count' => $this->CommonMethodsModel->cleanQuery($packages),
            'airline_allowed_weight' => $this->CommonMethodsModel->cleanQuery($airline_allowed_weight),
            //'weight_carried' => $this->CommonMethodsModel->cleanQuery($weight_carried),
            'weight_carried' => $this->CommonMethodsModel->cleanQuery($weight_carried),
            'weight_accommodate' => $this->CommonMethodsModel->cleanQuery($weight_accommodate),
			'weight_carried_unit' => $this->CommonMethodsModel->cleanQuery($weight_carried_unit),
            'weight_accommodate_unit' => $this->CommonMethodsModel->cleanQuery($weight_accommodate_unit),
            'package_service_fee' => $this->CommonMethodsModel->cleanQuery($package_service_fee),
            'not_wish_to_carry' => $this->CommonMethodsModel->cleanQuery($not_wish_to_carry),
            'comment' => $this->CommonMethodsModel->cleanQuery($comment),
            'item_worth' => $this->CommonMethodsModel->cleanQuery($item_worth),
            'item_worth_currency' => $this->CommonMethodsModel->cleanQuery(isset($item_worth_currency) ? $item_worth_currency : ''),
            'modified' => date('Y-m-d H:i:s')
        );
        $this->TripModel->deleteTripPackageDetail($trip_ID);
        $ID = $this->TripModel->addTripTravellerPackage($arr_package_detail);
        return $ID;
    }

    public function addTravellerProjectDetail($trip_ID)
    {
        extract($_POST);
        if (!isset($anydate))
            $anydate = '';
        $project_start_date_time = '';
        if (isset($project_start_date) && !empty($project_start_date)) {
            $project_start_date_time = date('Y-m-d H:i:s', strtotime($project_start_date));
        }
        $project_end_date_time = '';
        if (isset($project_end_date) && !empty($project_end_date)) {
            $project_end_date_time = date('Y-m-d H:i:s', strtotime($project_end_date));
        }
        //$fees = $this->calculatePackageServiceFee($trip_ID,'project','traveller',1);

        $arr_project_detail = array(
            'trip' => $this->CommonMethodsModel->cleanQuery($trip_ID),
            'work_category' => isset($work_category)?$this->CommonMethodsModel->cleanQuery(implode(',', $work_category)):'',
            'product_type' => $this->CommonMethodsModel->cleanQuery(implode(',', $product_type)),
            'task' => $this->CommonMethodsModel->cleanQuery($task),
            'product_category' => $this->CommonMethodsModel->cleanQuery(implode(',', $product_category)),
            'task_subcat' => $this->CommonMethodsModel->cleanQuery($task_subcat),
            'item_worth' => $this->CommonMethodsModel->cleanQuery($item_worth),
            'item_worth_currency' => $this->CommonMethodsModel->cleanQuery($item_worth_currency),
			'item_weight' => $this->CommonMethodsModel->cleanQuery($item_weight),
			'item_weight_unit' => $this->CommonMethodsModel->cleanQuery($item_weight_unit),
            'project_service_fee' => $this->CommonMethodsModel->cleanQuery($project_service_fee),
            'dates_available' => $this->CommonMethodsModel->cleanQuery(isset($anydate) ? $anydate : ''),
            'start_date' => $this->CommonMethodsModel->cleanQuery($project_start_date_time),
            'end_date' => $this->CommonMethodsModel->cleanQuery($project_end_date_time),
            'flexible_for_local' => $this->CommonMethodsModel->cleanQuery(isset($flexible_for_local) ? $flexible_for_local : ''),
            'comment' => $this->CommonMethodsModel->cleanQuery($comment),
            'modified' => date('Y-m-d H:i:s')
        );
        $this->TripModel->deleteTripProjectDetails($trip_ID);
        $ID = $this->TripModel->addTripTravellerProject($arr_project_detail);
        return $ID;
    }

    public function decrypt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        /*pls set your unique hashing key*/
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        /* hash*/
        $key = hash('sha256', $secret_key);
        /* iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning*/
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        /*decrypt the given text/string/number*/
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    public function doPayPalPayment_bk($param)
    {
        $paypal_api_username = $this->generalvar['paypal_api_username'];
        $paypal_api_password = $this->generalvar['paypal_api_password'];
        $paypal_api_signature = $this->generalvar['paypal_api_signature'];
        $payment_type = 'Sale';
        $cc_start_date_month = isset($_POST['cc_start_date_month']) ? $_POST['cc_start_date_month'] : '';
        $cc_start_date_year = isset($_POST['cc_start_date_year']) ? $_POST['cc_start_date_year'] : '';
        $request = 'METHOD=DoDirectPayment';
        $request .= '&VERSION=65.1';
        $request .= '&USER=' . $paypal_api_username;
        $request .= '&PWD=' . $paypal_api_password;
        $request .= '&SIGNATURE=' . $paypal_api_signature;
        $request .= '&CUSTREF=' . (int)rand();
        $request .= '&PAYMENTACTION=' . $payment_type;
        $request .= '&AMT=' . $param['amount'];
        $request .= '&CREDITCARDTYPE=' . $param['cc_type'];
        $request .= '&ACCT=' . urlencode(str_replace(' ', '', $param['cc_number']));
        $request .= '&CARDSTART=' . urlencode($cc_start_date_month . $cc_start_date_year);
        $request .= '&EXPDATE=122020';
        /* $request .= '&EXPDATE=' . urlencode($param['expirty_date']);*/
        $request .= '&CVV2=' . urlencode($param['security_code']);
        if (isset($_POST['cc_type']) && ($_POST['cc_type'] == 'SWITCH' || $_POST['cc_type'] == 'SOLO')) {
            $request .= '&CARDISSUE=' . urlencode($_POST['cc_issue']);
        }
        $request .= '&FIRSTNAME=' . urlencode($param['first_name']);
        $request .= '&LASTNAME=' . urlencode($param['last_name']);
        $request .= '&EMAIL=' . urlencode($param['email_address']);
        $request .= '&PHONENUM=' . urlencode($param['phone_no']);
        $request .= '&IPADDRESS=' . urlencode($_SERVER['REMOTE_ADDR']);
        $request .= '&STREET=' . urlencode($param['address']);
        $request .= '&CITY=' . urlencode($param['city']);
        $request .= '&STATE=' . urlencode($param['state']);
        $request .= '&ZIP=' . urlencode($param['zip']);
        $request .= '&COUNTRYCODE=' . urlencode($param['country_code']);
        $request .= '&CURRENCYCODE=' . urlencode('USD');
        /* $request .= '&SHIPTONAME=' . urlencode($order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname']);        $request .= '&SHIPTOSTREET=' . urlencode($order_info['shipping_address_1']);        $request .= '&SHIPTOCITY=' . urlencode($order_info['shipping_city']);        $request .= '&SHIPTOSTATE=' . urlencode(($order_info['shipping_iso_code_2'] != 'US') ? $order_info['shipping_zone'] : $order_info['shipping_zone_code']);        $request .= '&SHIPTOCOUNTRYCODE=' . urlencode($order_info['shipping_iso_code_2']);        $request .= '&SHIPTOZIP=' . urlencode($order_info['shipping_postcode']);         */
        /* $curl = curl_init('https://api-3t.paypal.com/nvp'); // This is for live account        $curl = curl_init('https://api-3t.sandbox.paypal.com/nvp'); // This is for sandbox account         */
        $curl = curl_init('https://api-3t.sandbox.paypal.com/nvp');
        curl_setopt($curl, CURLOPT_PORT, 443);
        curl_setopt($curl, CURLOPT_HEADER, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($curl, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $request);
        $response = curl_exec($curl);
        curl_close($curl);
        parse_str($response, $response_info);
        $json = array();
        return $response_info;
    }

    function doPayPalPayment($param)
    {
		return $this->doStripePayment($param);
		
        $paypal_api_username = $this->generalvar['paypal_api_username'];
        $paypal_api_password = $this->generalvar['paypal_api_password'];
        $paypal_api_signature = $this->generalvar['paypal_api_signature'];
        $methodName = 'doDirectPayment';
        $API_Endpoint = 'https://api-3t.sandbox.paypal.com/nvp';
        $version = '65.2';
        $API_UserName = $paypal_api_username;
        $API_Password = $paypal_api_password;
        $API_Signature = $paypal_api_signature;
        $subject = '';
        global $nvp_Header, $AUTH_token, $AUTH_signature, $AUTH_timestamp;
        $nvpHeaderStr = "";
        $paymentType = urlencode('Authorization');
        $firstName = urlencode($param['first_name']);
        $lastName = urlencode($param['last_name']);
        $creditCardNumber = urlencode((str_replace(' ', '', $param['cc_number'])));
        $cvvNumber = urlencode($param['security_code']);
        $creditCardType = urlencode($param['cc_type']);
        $expMonth = str_pad(trim($param['expirty_month']), 2, '0', STR_PAD_LEFT);
        $expYear = urlencode(trim($param['expirty_year']));
        $address = urlencode($param['address']);
        $city = urlencode($param['city']);
        $state = urlencode($param['state']);
        $country = $param['country_code'];
        $zip = urlencode($param['zip']);
        $amount = urlencode($param['amount']);
        $currencyCode = "USD";
        $methodName = 'doDirectPayment';
        $nvpHeaderStr = "&PWD=" . urlencode($API_Password) . "&USER=" . urlencode($API_UserName) . "&SIGNATURE=" . urlencode($API_Signature);
        $nvpstr = "&PAYMENTACTION=$paymentType&AMT=$amount&CREDITCARDTYPE=$creditCardType&ACCT=$creditCardNumber&EXPDATE=" . $expMonth . $expYear . "&CVV2=$cvvNumber&FIRSTNAME=$firstName&LASTNAME=$lastName&STREET=$address&CITY=$city&STATE=$state" . "&ZIP=$zip&CURRENCYCODE=$currencyCode";
        $nvpstr = $nvpHeaderStr . $nvpstr;
        if (strlen(str_replace('VERSION=', '', strtoupper($nvpstr))) == strlen($nvpstr)) {
            $nvpstr = "&VERSION=" . urlencode($version) . $nvpstr;
        }
        $nvpreq = "METHOD=" . urlencode($methodName) . $nvpstr;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $nvpreq);
        $response = curl_exec($ch);
        $nvpResArray = $this->deformatNVP($response);
        $nvpReqArray = $this->deformatNVP($nvpreq);
        if (curl_errno($ch)) {
        } else {
            curl_close($ch);
        }
        $nvpResArray['pay_id'] = 0;
        if (isset($nvpResArray['ACK']) && ($nvpResArray['ACK'] == 'Success' || $nvpResArray['ACK'] == 'SuccessWithWarning')) {
            $paymentArray['pay_user'] = $param['pay_user'];
            $paymentArray['pay_to_trip_id'] = $param['pay_to_trip_id'];
            $paymentArray['pay_trip_id'] = $param['pay_trip_id'];
            $paymentArray['pay_service_id'] = $param['pay_service_id'];
            $paymentArray['pay_user_type'] = $param['pay_user_type'];
            $paymentArray['pay_card_type'] = $param['pay_card_type'];
            $paymentArray['pay_type'] = $param['pay_type'];
            $paymentArray['pay_trip_table'] = $param['pay_trip_table'];
            $paymentArray['pay_request_id'] = $param['pay_request_id'];
            $paymentArray['pay_to_user'] = $param['pay_to_user'];
            $paymentArray['pay_amount'] = $nvpResArray['AMT'];
            $paymentArray['pay_trans_id'] = $nvpResArray['TRANSACTIONID'];
            $paymentArray['pay_cor_id'] = $nvpResArray['CORRELATIONID'];
            $paymentArray['pay_currency'] = 'USD';
            $paymentArray['pay_added'] = time();
            $paymentArray['pay_service'] = $param['pay_service'];
            $pay_id = $this->TripModel->updatePaymants($paymentArray);
            $nvpResArray['pay_id'] = $pay_id;
        }
        return $nvpResArray;
    }
	
	function doStripePayment($param){

		$error = '';
		$nvpResArray['pay_id'] = 0;
		$nvpResArray['ACK'] = 'Failed';
		try {
			$description = $param['pay_user'].'('.$param['pay_trip_id'].') pay to '.$param['pay_to_user'].'('.$param['pay_to_trip_id'].') amount '.$param['amount'];
			$charge = \Stripe\Charge::create(array(
				"amount" => $param['amount']*100,
				"currency" => "GBP",
				"source" => $param['stripe_src_id'],
				"customer" => $param['stripe_customer_id'],
				"description" => $description,
				'metadata' => ['pay_trip_id' => $param['pay_trip_id'],'pay_to_trip_id' => $param['pay_to_trip_id']],
				'capture' => false)
			);
			$payStatus = true;
			$msg = 'Payment successfully completed';
			
			$paymentArray['pay_user'] = $param['pay_user'];
            $paymentArray['pay_to_trip_id'] = $param['pay_to_trip_id'];
            $paymentArray['pay_trip_id'] 	= $param['pay_trip_id'];
            $paymentArray['pay_service_id'] = $param['pay_service_id'];
            $paymentArray['pay_user_type'] 	= $param['pay_user_type'];
            $paymentArray['pay_type'] 		= $param['pay_type'];
            $paymentArray['pay_trip_table'] = $param['pay_trip_table'];
            $paymentArray['pay_request_id'] = $param['pay_request_id'];
            $paymentArray['pay_to_user'] 	= $param['pay_to_user'];
            $paymentArray['pay_amount'] 	= $param['amount'];
            $paymentArray['pay_trans_id'] 	= $charge['id'];
            $paymentArray['pay_cor_id'] 	= '';
            $paymentArray['pay_currency'] 	= 'USD';
            $paymentArray['pay_added'] 		= time();
            $paymentArray['pay_service'] 	= $param['pay_service'];
            $pay_id = $this->TripModel->updatePaymants($paymentArray);
			$nvpResArray['ACK'] = 'Success';
            $nvpResArray['pay_id'] = $pay_id;
			
		} catch(Stripe_CardError $e) {
			$nvpResArray['L_LONGMESSAGE0'] = $e->getMessage();
		} catch (Stripe_InvalidRequestError $e) {
			$nvpResArray['L_LONGMESSAGE0'] = $e->getMessage();
		} catch (Stripe_AuthenticationError $e) {
			$nvpResArray['L_LONGMESSAGE0'] = $e->getMessage();
		} catch (Stripe_ApiConnectionError $e) {
			$nvpResArray['L_LONGMESSAGE0'] = $e->getMessage();
		} catch (Stripe_Error $e) {
			$nvpResArray['L_LONGMESSAGE0'] = $e->getMessage();
		} catch (Exception $e) {
			$nvpResArray['L_LONGMESSAGE0'] = $e->getMessage();
		}
		return $nvpResArray;
	}
	
	function retriveStripeSource($srcId){
		$errMessage = '';$paymentSrc = array();
		try {
			$paymentSrc = Stripe\Source::retrieve($srcId);
		} catch (Exception $e) {
			$errMessage = $e->getMessage();
		}
		return $paymentSrc;
	}
	
	function attachStripeSource($resourceId,$cusId){
		$errMessage = '';
		try {
			$customer = Stripe\Customer::retrieve($cusId);
			$customer->sources->create(["source" => $resourceId]);
		} catch (Exception $e) {
			$errMessage = $e->getMessage();
		}
		return array('cus_id'=>$cusId,'err_msg'=>$errMessage,'src_id'=>$resourceId);
	}
	
	function createStripeCustomer($param){
		$errMessage = $cusId = '';
		try {
			$customer = Stripe\Customer::create([
			  "email" => $param['source']['owner']['email'],
			  "source" => $param['source']['id'],
			]);
			$cusId = $customer->id;
			$resourceId = $param['source']['id'];
		} catch (Exception $e) {
			$errMessage = $e->getMessage();
		}
		return array('cus_id'=>$cusId,'err_msg'=>$errMessage,'src_id'=>$resourceId);
	}

    function deformatNVP($nvpstr)
    {
        $intial = 0;
        $nvpArray = array();
        while (strlen($nvpstr)) {
            $keypos = strpos($nvpstr, '=');
            $valuepos = strpos($nvpstr, '&') ? strpos($nvpstr, '&') : strlen($nvpstr);
            $keyval = substr($nvpstr, $intial, $keypos);
            $valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);
            $nvpArray[urldecode($keyval)] = urldecode($valval);
            $nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
        }
        return $nvpArray;
    }

    function formAutorization($auth_token, $auth_signature, $auth_timestamp)
    {
        $authString = "token=" . $auth_token . ",signature=" . $auth_signature . ",timestamp=" . $auth_timestamp;
        return $authString;
    }

    function calculatePeopleServiceFee($tripId, $type_of_service, $userrole, $passengers_count)
    {
        $data = $this->TripModel->getTrip($tripId, $userrole);
        extract($data);
        $ree = explode('-', $trip_route);
        $dist = array();
        for ($x = 0; $x <= count($ree) - 1; $x++) {
            $y = $x + 1;
            $departureAirport = $this->CommonMethodsModel->getAirPortByAirportCode($ree[$x]);
            $arrivalAirport = $this->CommonMethodsModel->getAirPortByAirportCode($ree[$y]);
            $distancez = $this->CommonMethodsModel->distance($departureAirport['latitude'], $departureAirport['longitude'], $arrivalAirport['latitude'], $arrivalAirport['longitude']);
            $dist[] = $distancez;
        }
        $dist1 = array();
        for ($x1 = 0; $x1 <= count($dist) - 1; $x1++) {
            $y1 = $x1 + 1;
            $distancezz = $this->TripModel->getDistancePrice(round($dist[$x1]), $type_of_service);
            $dist1[] = $distancezz['distance_price'];
        }
        $departureAirport = $this->CommonMethodsModel->getAirPortByAirportCode(current($ree));
        $arrivalAirport = $this->CommonMethodsModel->getAirPortByAirportCode(end($ree));
        $distances = $this->TripModel->getCountryPrice($departureAirport['country_code'], $arrivalAirport['country_code'], $type_of_service);
        $service_fees = (end($dist1)) + ($distances['country_price']) + ($noofstops * 15) * ($passengers_count);
        return $service_fees;
    }

    function calculatePackageServiceFee($tripId, $type_of_service, $userrole, $weight)
    {
        $data = $this->TripModel->getTrip($tripId, $userrole);
        extract($data);
        $ree = explode('-', $trip_route);
        $dist = array();
        for ($x = 0; $x <= count($ree) - 1; $x++) {
            $y = $x + 1;
            $departureAirport = $this->CommonMethodsModel->getAirPortByAirportCode($ree[$x]);
            $arrivalAirport = $this->CommonMethodsModel->getAirPortByAirportCode($ree[$y]);
            $distancez = $this->CommonMethodsModel->distance($departureAirport['latitude'], $departureAirport['longitude'], $arrivalAirport['latitude'], $arrivalAirport['longitude']);
            $dist[] = $distancez;
        }
        $dist1 = array();
        $dist2 = array();
        for ($x1 = 0; $x1 <= count($dist) - 1; $x1++) {
            $y1 = $x1 + 1;
            $distancezz = $this->TripModel->getDistancePrice(round($dist[$x1]), $type_of_service);
            $weight_price = $this->TripModel->getWeightPrice(round($dist[$x1]), $type_of_service);
            $dist1[] = $distancezz['distance_price'];
            $dist2[] = $weight_price['weight_carried_price'];
        }
        $departureAirport = $this->CommonMethodsModel->getAirPortByAirportCode(current($ree));
        $arrivalAirport = $this->CommonMethodsModel->getAirPortByAirportCode(end($ree));
        $distances = $this->TripModel->getCountryPrice($departureAirport['country_code'], $arrivalAirport['country_code'], $type_of_service);
        $service_fees = (end($dist1)) + ($distances['country_price']) + (end($dist2)) * ($weight);
        //echo end($dist1).', '.$distances['country_price'].', '.end($dist2).', '.$service_fees;die;
        return $service_fees;
    }

    function calculateProjectServiceFee($tripId, $type_of_service, $userrole, $passengers_count)
    {
        $data = $this->TripModel->getTrip($tripId, $userrole);
        extract($data);
        $ree = explode('-', $trip_route);
        $dist = array();
        for ($x = 0; $x <= count($ree) - 1; $x++) {
            $y = $x + 1;
            $departureAirport = $this->CommonMethodsModel->getAirPortByAirportCode($ree[$x]);
            $arrivalAirport = $this->CommonMethodsModel->getAirPortByAirportCode($ree[$y]);
            $distancez = $this->CommonMethodsModel->distance($departureAirport['latitude'], $departureAirport['longitude'], $arrivalAirport['latitude'], $arrivalAirport['longitude']);
            $dist[] = $distancez;
        }
        $dist1 = array();
        for ($x1 = 0; $x1 <= count($dist) - 1; $x1++) {
            $y1 = $x1 + 1;
            $distancezz = $this->TripModel->getDistancePrice(round($dist[$x1]), $type_of_service);
            $dist1[] = $distancezz['distance_price'];
        }
        $departureAirport = $this->CommonMethodsModel->getAirPortByAirportCode(current($ree));
        $arrivalAirport = $this->CommonMethodsModel->getAirPortByAirportCode(end($ree));
        $distances = $this->TripModel->getCountryPrice($departureAirport['country_code'], $arrivalAirport['country_code'], $type_of_service);
        $service_fees = (end($dist1)) + ($distances['country_price']) + ($noofstops * 15) * ($passengers_count);
        return $service_fees;
    }

    protected function _redirect($url, array $options = array())
    {
        $redirector = $this->_actionController->getHelper('Redirector');
        $redirector->gotoUrl($url);
    }
}

?>