<?php


class Zend_Controller_Action_Helper_Linkedin extends Zend_Controller_Action_Helper_Abstract{
// Change these
    
public function init() {
$this->comsessobj = new Container('comsessobj');
$config_params = Zend_Registry::get('config');
$this->generalvar = $config_params['general'];
$this->general_mod = new Application_Model_DbTable_General();
$this->myaccount_mod = new Application_Model_DbTable_Myaccount();
define('API_KEY',$this->generalvar['linkedin_appId']);
define('API_SECRET',$this->generalvar['linkedin_secret']);
define('REDIRECT_URI', 'http://' . $_SERVER['SERVER_NAME'] . $_SERVER['SCRIPT_NAME']);
//define('SCOPE',        'r_basicprofile r_emailaddress rw_nus r_network'                        );
define('SCOPE',        'r_basicprofile r_emailaddress'                        );
$this->user = $this->comsessobj->user;

$this->common_helper = Zend_Controller_Action_HelperBroker::getStaticHelper('Common');
}
 
// You'll probably use a database
public function checkLinkedinAppUser() {
 $this->comsessobj->linkedin = '';
 //$this->comsessobj->access_token = '';
//session_name('linkedin');
//session_start();
// OAuth 2 Control Flow
if (isset($_GET['error'])) {
        // LinkedIn returned an error
    print $_GET['error'] . ': ' . $_GET['error_description'];
    exit;
} elseif (isset($_GET['code'])) {
     
    //$this->getAuthorizationCode();
    // User authorized your application
    if ($this->comsessobj->state == $_GET['state']) {
        // Get token so you can make API calls
        $this->getAccessToken();
    } else {
        // CSRF attack? Or did you mix up your states?
        exit;
    }
} else { 
    if ((empty($this->comsessobj->expires_at)) || (time() > $this->comsessobj->expires_at)) {
        // Token has expired, clear the state
        //$_SESSION = array();
    }
    if ($this->user['linkedin_access_token']=='') {
        //return 'get_authorization';
        // Start authorization process
        $this->getAuthorizationCode();
    }
}

//$friends = $connections->values;
//return $friends;

    
    
}
  
public function getLinkedInUserByProfileUrl($url) {
$user = $this->fetch('GET', '/v1/people/url='.$url.':(id,first-name,last-name)',$this->user['linkedin_access_token']);
return $user;
}

public function getLinkedInUser($id) {
$user = $this->fetch('GET', '/v1/people/id='.$id,$this->user['linkedin_access_token']);
return $user;
}

public function getLinkedinUserProfile($user) {
$user_profile = $this->fetch('GET', '/v1/people/~:(id,headline,first-name,last-name,picture-url,public-profile-url)',$user['linkedin_access_token']);
//echo "<pre>";print_r($user_profile);
if($user_profile->id==''){
$this->refreshAccessToken($user);
$user_profile = $this->fetch('GET', '/v1/people/~:(id,headline,first-name,last-name,picture-url,public-profile-url)',$user['linkedin_access_token']);
}
return $user_profile;
}

public function getLinkedInFriendsOfFriends($linkedin_friends) {
$friends_of_friends = array();
if(count($linkedin_friends)>0){
foreach($linkedin_friends as $linkedin_friend){    
$user = $this->myaccount_mod->getUser($linkedin_friend['user_id']);
if($user['linkedin_access_token']){
$connections = $this->fetch('GET', '/v1/people/~/connections:(id,headline,first-name,last-name,picture-url,public-profile-url)',$user['linkedin_access_token']);
//echo "<pre>";print_r($connections); exit;
$friends = $connections->values;
$friends_of_friends = array_merge($friends_of_friends,$friends);
}
}
}
return $friends_of_friends;
}

public function getLinkedInFriends() {
$connections = $this->fetch('GET', '/v1/people/~/connections:(id,headline,first-name,last-name,picture-url,public-profile-url)',$this->user['linkedin_access_token']);
//echo "<pre>";print_r($connections); exit;
$friends = $connections->values;
return $friends;
/*
if(time()>=$this->user['linkedin_expires_at']){
    $this->getAuthorizationCode();
}else{
return $friends;
}
 * 
 */
/*
echo "<pre>";print_r($friends);
foreach($friends as $friend){
    echo $friend->firstName.'<br>';
    $user = $this->fetch('GET', '/v1/people/id='.$friend->id);
    print_r($user);
    echo '<br>';
    echo '<br>';
}
 * */
 //echo "<pre>";print_r($connections);
//session_destroy();
//exit;
}
 
public function getAuthorizationCode() {
    $params = array('response_type' => 'code',
                    'client_id' => API_KEY,
                    'scope' => SCOPE,
                    'state' => uniqid('', true), // unique long string
                    'redirect_uri' => str_replace('index.php', 'requestlinkedin', REDIRECT_URI),
                    //'redirect_uri' => REDIRECT_URI,
              );
 
    //print_r($params); exit;
    // Authentication request
    
    $url = 'https://www.linkedin.com/uas/oauth2/authorization?' . http_build_query($params);
     
    // Needed to identify request when it returns to us
    $this->comsessobj->state = $params['state'];
 
    // Redirect user to authenticate
    //echo "hai:".$url; exit;
    header("Location: $url");
    exit;
}
     
public function getAccessToken() {
    if($_GET['code'])
        $auth_code = $_GET['code'];
    else{
        $auth_code = $this->user['linkedin_authrization_code'];
    }
    $code = $auth_code;
    $params = array('grant_type' => 'authorization_code',
                    'client_id' => API_KEY,
                    'client_secret' => API_SECRET,
                    'code' => $auth_code,
                    'redirect_uri' => str_replace('index.php', 'requestlinkedin', REDIRECT_URI),
                    //'redirect_uri' => REDIRECT_URI,
                    );
     
    // Access Token request
    
    $url = 'https://www.linkedin.com/uas/oauth2/accessToken?' . http_build_query($params);
     
    // Tell streams to make a POST request
    $context = stream_context_create(
                    array('http' => 
                        array('method' => 'GET',
                        )
                    )
                );
 
    // Retrieve access token information
    //$response = file_get_contents($url, false, $context);
    $response = $this->common_helper->curl_get_file_contents($url);
    
 
    // Native PHP object, please
    $token = json_decode($response);
    // Store access token and expiration time
    $this->comsessobj->access_token = $token->access_token; // guard this! 
    $access_token = $this->comsessobj->access_token;
    $this->comsessobj->expires_in   = $token->expires_in; // relative time (in seconds)
    $this->comsessobj->expires_at   = time() + $this->comsessobj->expires_in; // absolute time
    
    $user_profile = $this->getLinkedinUserProfile($token->access_token);
    //echo "<pre>"; print_r($user_profile); exit;
    $linkedin_user_id = $user_profile->id;
    if($this->comsessobj->app_request_type=='linkedin_login'){
       $this->comsessobj->app_request_type = ''; 
       $social_type = 'linkedin';
       $user = $this->myaccount_mod->getUserBySocialId($social_type,$linkedin_user_id);
       if($user['user_id']!=''){
           $this->comsessobj->user_id = $user['user_id'];
           $this->comsessobj->user = $user;
           $arr_user = array(
                      'linkedin_authrization_code' => $code,
                      'linkedin_access_token' => $access_token
                      );
             $this->myaccount_mod->updateuser($arr_user,$this->comsessobj->user_id);
             $this->comsessobj->user = $this->myaccount_mod->getuser($this->comsessobj->user_id);
             $this->comsessobj->social_login_with = 'linkedin';
             $this->comsessobj->social_profile = $user_profile;
       }else{
           $arr_user = array(
                     'firstname' => $user_profile->first_name,
                     'lastname' => $user_profile->last_name,
                     'linkedin_user_id' => $user_profile->id,
                     'linkedin_authrization_code' => $code,
                     'linkedin_access_token' => $access_token
                     );
             $userid = $this->myaccount_mod->adduser($arr_user,$this->comsessobj->user_id);
             $this->comsessobj->user_id = $userid;
             $this->comsessobj->user = $this->myaccount_mod->getuser($this->comsessobj->user_id);
             $this->comsessobj->social_login_with = 'linkedin';
             $this->comsessobj->social_profile = $user_profile;
       }
    }else{
         //facebook_login
         //echo "<pre>";print_r($user_profile); exit;
         $arr_app_data = array(
                          'linkedin_user_id' => $user_profile->id,
                          'linkedin_authrization_code' => $code,
                          'linkedin_access_token' => $access_token
                          );
         if($this->comsessobj->user_id){
         $this->myaccount_mod->updateuser($arr_app_data,$this->comsessobj->user_id);
         $this->comsessobj->user = $this->myaccount_mod->getuser($this->comsessobj->user_id);
         }else{
             $this->comsessobj->linkedin_app_data = $arr_app_data;
         }
    }    
    
    $arr_user_linkedin = array(
                                'linkedin_authrization_code'    => $auth_code,
                                'linkedin_access_token'    => $token->access_token,
                                'linkedin_expires_in'    => $token->expires_in,
                                'linkedin_expires_at'    => $this->comsessobj->expires_at
                                );
    $this->myaccount_mod->updateuser($arr_user_linkedin,$this->comsessobj->user_id);
    $this->comsessobj->user = $this->myaccount_mod->getuser($this->comsessobj->user_id);
    return $token; 
    //return true;
}
 
     
public function refreshAccessToken($user) {
    $auth_code = $user['linkedin_authrization_code'];
    $code = $auth_code;
    $params = array('grant_type' => 'authorization_code',
                    'client_id' => API_KEY,
                    'client_secret' => API_SECRET,
                    'code' => $auth_code,
                    'redirect_uri' => str_replace('index.php', 'requestlinkedin', REDIRECT_URI),
                    //'redirect_uri' => REDIRECT_URI,
                    );
    
    // Access Token request
    
    $url = 'https://www.linkedin.com/uas/oauth2/accessToken?' . http_build_query($params);
     
    // Tell streams to make a POST request
    $context = stream_context_create(
                    array('http' => 
                        array('method' => 'GET',
                        )
                    )
                );
    $response = $this->common_helper->curl_get_file_contents($url);
    
    $token = json_decode($response);
    $access_token = $token->access_token;
    $arr_app_data = array(
                     'linkedin_access_token' => $access_token
                     );
    $this->myaccount_mod->updateuser($arr_app_data,$user['user_id']);
}
 


public function fetch($method, $resource,$access_token='', $body = '') {
    $params = array('oauth2_access_token' => $access_token,
                    'format' => 'json',
              );
     
    // Need to use HTTPS
    $url = 'https://api.linkedin.com' . $resource . '?' . http_build_query($params);
    // Tell streams to make a (GET, POST, PUT, or DELETE) request
    $context = stream_context_create(
                    array('http' => 
                        array('method' => $method,
                        )
                    )
                );
 
 
    // Hocus Pocus
    //echo $url; 
    //$response = file_get_contents($url, false, $context);
    //echo $url;
    $response = $this->common_helper->curl_get_file_contents($url);
    // Native PHP object, please
    return json_decode($response);
}

}

?>