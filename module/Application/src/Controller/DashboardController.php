<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Application\Model\CommonMethodsModel;
use Zend\Mvc\MvcEvent;
use Application\Model\Currencyrate;
use Zend\ServiceManager\ServiceManager; 

class DashboardController extends AbstractActionController  {
    
    var $sessionObj;
    var $CommonPlugin;
    var $GoogleplusPlugin;
    protected $serviceLocator;
    var $siteConfigs;
    var $view;
    
    public function __construct($data=false){
          if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
         if($this->sessionObj->offsetGet('userid')){
            $user = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
            $this->view['user'] = $user;
        } else {
              
        }
    }
	public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        if(isset($this->sessionObj->userid) && $this->sessionObj->userid != ''){
			$isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			if(!$isUserLoggedIn){
				$this->redirect()->toRoute('logout');
			}
		}
		return parent::onDispatch($e);
    }
	 

	public function indexAction() {
               if(!($this->sessionObj->userid) || $this->sessionObj->userid == ''){
            $this->redirect()->toRoute('home');
        }
		if($this->sessionObj->approved_id == 0){
			$this->sessionObj->userrole = 'seeker';
			if(empty($this->sessionObj->switch_userrole)) 
				$this->sessionObj->switch_userrole = 'seeker';
		}
		
            $viewArray = array();
             $viewArray = array(
                'comSessObj'    =>  $this->sessionObj,
                'countries' => $this->CommonMethodsModel->getCountries(),
				'currency' => $this->CommonMethodsModel->getCurrencies()
            );
			$viewArray['idCounts'] = $this->DashboardModel->getIdentityCount($this->sessionObj->offsetGet('userid'));
            $viewArray['user'] = $this->view['user'];
            $viewArray['alrt'] = $this->DashboardModel->getalert($this->sessionObj->userid);
            $viewArray['noft'] = $this->DashboardModel->getnoft($this->sessionObj->userid);
            $viewArray['dashmsg'] = $this->DashboardModel->getdashmsg($this->sessionObj->userid);
            $viewArray['trval'] = $this->DashboardModel->gettrval($this->sessionObj->userid);
            $viewArray['seeker'] = $this->DashboardModel->getseeker($this->sessionObj->userid);
            $viewArray['disputes'] = $this->DashboardModel->getdisputes($this->sessionObj->userid);
			$viewArray['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
			$viewArray['notification'] = $this->CommonMethodsModel->getNotifications($this->sessionObj->userid);
			$viewArray['user']['userVerfication'] = $this->CommonMethodsModel->getIdentityCount($this->sessionObj->userid);
			$viewArray['profiledetails'] = $this->getprofiledetails($this->sessionObj->userid,$viewArray['user']);
			

			$viewModel = new ViewModel();
            $viewModel->setVariables($viewArray)
                ->setTerminal(true);
           return $viewModel;
	}

	public function viewtravelenquiryAction() {
	 if(!($this->sessionObj->userid) || $this->sessionObj->userid == ''){
            $this->redirect()->toRoute('home');
        }
            $viewArray = array(
                'countries' => $this->CommonMethodsModel->getCountries(),
            );
		$VT_ID = $_GET['slug'];
		/*$this->view->trvallst = $this->DashboardModel->viewgettrval($this->sessionObj->userid,$VT_ID);
                return new ViewModel();*/
			$viewArray['trvallst'] = $this->DashboardModel->viewgettrval($this->sessionObj->userid,$VT_ID);
			$viewArray['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
			$viewArray['prof']  = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			$this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
			$this->layout()->breadCrumbArr      = array(
            array(
                'label' => 'Dashboard',
                'title' => 'Dashboard',
                'active' => false,
                'redirect' => '/dashboard'
            ),
            array(
                'label' => 'Notifications',
                'title' => 'Notifications',
                'active' => false,
                'redirect' => false //'/myaccount'
            ),
			array(
                'label' => 'Traveller Inquiries Details',
                'title' => 'Traveller Inquiries Details',
                'active' => true,
                'redirect' => false //'/myaccount'
            )
			);
            $viewModel = new ViewModel();
            $viewModel->setVariables($viewArray);
            return $viewModel;
	}
	
	public function viewseekerenquiryAction() {
		 
		$VT_ID = $this->_request->getParam('ID');
		$this->view->seekerlst = $this->DashboardModel->getseekerlst($this->sessionObj->userid,$VT_ID);
	}
	
	public function viewdisputesAction() {
		 
		$VT_ID = $this->_request->getParam('ID');
		$this->view->disputeslst = $this->DashboardModel->getdisputeslst($this->sessionObj->userid,$VT_ID);
		$this->view->disputeslstdt = $this->DashboardModel->getdisputeslstdt($this->sessionObj->userid,$VT_ID);
		if($_POST){
			$arr_disputes_details = array(
				'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
				'admin_id' => '0',
				'dispautoid' => $this->CommonMethodsModel->cleanQuery($VT_ID),
				'msg' => $this->CommonMethodsModel->cleanQuery($_POST['msg']),
				'datetime' => date('Y-m-d H:i:s')
			);
			$this->DashboardModel->adddisputesDetaildt($arr_disputes_details);
			$this->_redirect("/".$this->controller."/viewdisputes/".$VT_ID);
		}else{
		 $this->view->user_locations = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
		 $this->view->countries = $this->CommonMethodsModel->getCountries();
		  $this->_redirect("/".$this->controller);
		}
	}
	
	public function adddisputesAction() { 
            
         
		if($_POST){   
			$arr_disputes_details = array(
				'user_id' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
				'trip_id' => $this->CommonMethodsModel->cleanQuery($_POST['trip_id']),
                                'issue_type' => $this->CommonMethodsModel->cleanQuery($_POST['trip_id']),
				'issue' => $this->CommonMethodsModel->cleanQuery($_POST['issue']),
                                'comment' => $this->CommonMethodsModel->cleanQuery($_POST['comment']),
				'datetime' => date('Y-m-d H:i:s'),
                             
			);
                        
			$id=$this->DashboardModel->adddisputesDetail($arr_disputes_details);
                        if($id){
                                        
               
                               echo 1;
                                exit;
                        } else {
                            echo '0';
                             exit();
                        }
                        
		}
	}

	public function addtripAction() {
		if($_POST){
			$departure_date = $this->CommonMethodsModel->cleanQuery($_POST['departure_date']);
			$departure_time = $this->CommonMethodsModel->cleanQuery($_POST['departure_time']);
			$arrival_date = $this->CommonMethodsModel->cleanQuery($_POST['arrival_date']);
			$arrival_time = $this->CommonMethodsModel->cleanQuery($_POST['arrival_time']);
			$contact_address_type = $this->CommonMethodsModel->cleanQuery($_POST['contact_address_type']);
			$departure_date_time = date('Y-m-d H:i:s', strtotime($departure_date.' '.$departure_time));
			$arrival_date_time = date('Y-m-d H:i:s', strtotime($arrival_date.' '.$arrival_time));
			/* ticket image upload */
			$image_path = $this->generalvar['trips']['trips']['tickets'];
			$field_name = 'ticket_image';
			$file_name_prefix = $this->generalvar['trips']['trips']['filename'];
			$ticket_image = $this->CommonMethodsModel->uploadFile($field_name,$file_name_prefix,$image_path);
			/* ticket image upload */
			if($contact_address_type=='existing_address')
				$user_location = $this->CommonMethodsModel->cleanQuery($_POST['user_location']);
			elseif($contact_address_type=='new_address'){
				$arr_trip_contact = array(
					'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
					'name' => $this->CommonMethodsModel->cleanQuery($_POST['contact_name']),
					'street_address_1' => $this->CommonMethodsModel->cleanQuery($_POST['street_address_1']),
					'street_address_2' => $this->CommonMethodsModel->cleanQuery($_POST['street_address_2']),
					'city' => $this->CommonMethodsModel->cleanQuery($_POST['city']),
					'state' => $this->CommonMethodsModel->cleanQuery($_POST['state']),
					'zip_code' => $this->CommonMethodsModel->cleanQuery($_POST['zip_code']),
					'country' => $this->CommonMethodsModel->cleanQuery($_POST['country']),
					'phone' => $this->CommonMethodsModel->cleanQuery($_POST['phone']),
					'fax' => $this->CommonMethodsModel->cleanQuery($_POST['fax']),
					'latitude' => $this->CommonMethodsModel->cleanQuery($_POST['latitude']),
					'longitude' => $this->CommonMethodsModel->cleanQuery($_POST['longitude']),
					'modified' => date('Y-m-d H:i:s')
				);
				$user_location = $this->trip_mod->addTripContactAddress($arr_trip_contact);            
			}    
			
			$arr_trip_detail = array(
				'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
				'user_location' => $this->CommonMethodsModel->cleanQuery($user_location),
				'name' => $this->CommonMethodsModel->cleanQuery($_POST['trip_title']),
				'origin_location' => $this->CommonMethodsModel->cleanQuery($_POST['origin_location']),
				'destination_location' => $this->CommonMethodsModel->cleanQuery($_POST['destination_location']),
				'departure_date' => $this->CommonMethodsModel->cleanQuery($departure_date_time),
				'arrival_date' => $this->CommonMethodsModel->cleanQuery($arrival_date_time),
				'airline_name' => $this->CommonMethodsModel->cleanQuery($_POST['airline_name']),
				'flight_number' => $this->CommonMethodsModel->cleanQuery($_POST['flight_number']),
				'booking_status' => $this->CommonMethodsModel->cleanQuery($_POST['booking_status']),
				'cabin' => $this->CommonMethodsModel->cleanQuery($_POST['cabin']),
				'ticket_image' => $this->CommonMethodsModel->cleanQuery($ticket_image),
				'active' =>  1,
				'created' =>  date('Y-m-d H:i:s'),
				'modified' =>  date('Y-m-d H:i:s')
			);
			$ID = $this->trip_mod->addTrip($arr_trip_detail);
			
			$people_detail = $this->CommonMethodsModel->cleanQuery($_POST['people_detail']);
			if($people_detail==1){
				$arr_people_detail = array(
					'trip'                  => $this->CommonMethodsModel->cleanQuery($ID),
					'persons_travelling'    => $this->CommonMethodsModel->cleanQuery($_POST['persons_travelling']),
					'male_count'            => $this->CommonMethodsModel->cleanQuery($_POST['male_count']),
					'female_count'          => $this->CommonMethodsModel->cleanQuery($_POST['female_count']),
					'age_above_60_years'    => $this->CommonMethodsModel->cleanQuery($_POST['age_above_60_years']),
					'age_below_3_years'     => $this->CommonMethodsModel->cleanQuery($_POST['age_below_3_years']),
					'comment' => $this->CommonMethodsModel->cleanQuery($_POST['people_detail_comment']),
					'modified' => date('Y-m-d H:i:s')
				);
				$this->trip_mod->addTripPeopleDetail($arr_people_detail);
			}
			
			$package_detail = $this->CommonMethodsModel->cleanQuery($_POST['package_detail']);
			if($package_detail==1){
				$arr_package_detail = array(
					'trip'                  => $this->CommonMethodsModel->cleanQuery($ID),
					'packages_count' => $this->CommonMethodsModel->cleanQuery($_POST['packages_count']),
					'airline_allowed_weight' => $this->CommonMethodsModel->cleanQuery($_POST['airline_allowed_weight']),
					'weight_carried' => $this->CommonMethodsModel->cleanQuery($_POST['weight_carried']),
					'weight_accommodate' => $this->CommonMethodsModel->cleanQuery($_POST['weight_accommodate']),
					'not_wish_to_carry' => $this->CommonMethodsModel->cleanQuery($_POST['not_wish_to_carry']),
					'comment' => $this->CommonMethodsModel->cleanQuery($_POST['package_comment']),
					'modified' => date('Y-m-d H:i:s')
				);
				$this->trip_mod->addTripPackageDetail($arr_package_detail);
			}
			$this->_redirect("/".$this->controller);
			
		}else{
			$this->view->user_locations = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
			$this->view->countries = $this->CommonMethodsModel->getCountries();
		}
	}
	
	public function edittripAction() {
		$ID = $this->_helper->Common->decrypt($this->_request->getParam('ID'));
		$trip = $this->trip_mod->getTrip($ID);
		$this->view->trip = $trip;
		if($_POST){
			$departure_date = $this->CommonMethodsModel->cleanQuery($_POST['departure_date']);
			$departure_time = $this->CommonMethodsModel->cleanQuery($_POST['departure_time']);
			$arrival_date = $this->CommonMethodsModel->cleanQuery($_POST['arrival_date']);
			$arrival_time = $this->CommonMethodsModel->cleanQuery($_POST['arrival_time']);
			$contact_address_type = $this->CommonMethodsModel->cleanQuery($_POST['contact_address_type']);
			$departure_date_time = date('Y-m-d H:i:s', strtotime($departure_date.' '.$departure_time));
			$arrival_date_time = date('Y-m-d H:i:s', strtotime($arrival_date.' '.$arrival_time));
			if(isset($_FILES[$field_name])){
				$image_path = $this->generalvar['trips']['trips']['images'];
				$field_name = 'ticket_image';
				$file_name_prefix = $this->generalvar['trips']['trips']['filename'];
				$ticket_image = $this->CommonMethodsModel->uploadFile($field_name,$file_name_prefix,$image_path);
			}else{
				$ticket_image = $trip['ticket_image'];
			}
			if($contact_address_type=='existing_address')
				$user_location = $this->CommonMethodsModel->cleanQuery($_POST['user_location']);
			elseif($contact_address_type=='new_address'){
				$arr_trip_contact = array(
					'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),        
					'name' => $this->CommonMethodsModel->cleanQuery($_POST['contact_name']),
					'street_address_1' => $this->CommonMethodsModel->cleanQuery($_POST['street_address_1']),
					'street_address_2' => $this->CommonMethodsModel->cleanQuery($_POST['street_address_2']),
					'city' => $this->CommonMethodsModel->cleanQuery($_POST['city']),
					'state' => $this->CommonMethodsModel->cleanQuery($_POST['state']),
					'zip_code' => $this->CommonMethodsModel->cleanQuery($_POST['zip_code']),
					'country' => $this->CommonMethodsModel->cleanQuery($_POST['country']),
					'phone' => $this->CommonMethodsModel->cleanQuery($_POST['phone']),
					'fax' => $this->CommonMethodsModel->cleanQuery($_POST['fax']),
					'latitude' => $this->CommonMethodsModel->cleanQuery($_POST['latitude']),
					'longitude' => $this->CommonMethodsModel->cleanQuery($_POST['longitude']),
					'modified' => date('Y-m-d H:i:s')
				);
				$user_location = $this->trip_mod->addTripContactAddress($arr_trip_contact);            
			}    
			$arr_trip_detail = array(
				'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
				'user_location' => $this->CommonMethodsModel->cleanQuery($user_location),
				'name' => $this->CommonMethodsModel->cleanQuery($_POST['trip_title']),
				'origin_location' => $this->CommonMethodsModel->cleanQuery($_POST['origin_location']),
				'destination_location' => $this->CommonMethodsModel->cleanQuery($_POST['destination_location']),
				 /*//'departure_date' => $this->CommonMethodsModel->cleanQuery($departure_date_time),
				//'arrival_date' => $this->CommonMethodsModel->cleanQuery($arrival_date_time),*/
				'airline_name' => $this->CommonMethodsModel->cleanQuery($_POST['airline_name']),
				'flight_number' => $this->CommonMethodsModel->cleanQuery($_POST['flight_number']),
				'booking_status' => $this->CommonMethodsModel->cleanQuery($_POST['booking_status']),
				'cabin' => $this->CommonMethodsModel->cleanQuery($_POST['cabin']),
				'ticket_image' => $this->CommonMethodsModel->cleanQuery($ticket_image),
				'active' =>  1,
				'created' =>  date('Y-m-d H:i:s'),
				'modified' =>  date('Y-m-d H:i:s')
			);
			$this->trip_mod->updateTrip($arr_trip_detail,$ID);
			$people_detail = $this->CommonMethodsModel->cleanQuery($_POST['people_detail']);
			if($people_detail==1){
				$people_detail = $this->trip_mod->getPeopleDetailByTripID($ID);
				$arr_people_detail = array(
					'trip'                  => $this->CommonMethodsModel->cleanQuery($ID),
					'persons_travelling'    => $this->CommonMethodsModel->cleanQuery($_POST['persons_travelling']),
					'male_count'            => $this->CommonMethodsModel->cleanQuery($_POST['male_count']),
					'female_count'          => $this->CommonMethodsModel->cleanQuery($_POST['female_count']),
					'age_above_60_years'    => $this->CommonMethodsModel->cleanQuery($_POST['age_above_60_years']),
					'age_below_3_years'     => $this->CommonMethodsModel->cleanQuery($_POST['age_below_3_years']),
					'comment' => $this->CommonMethodsModel->cleanQuery($_POST['people_detail_comment']),
					'modified' => date('Y-m-d H:i:s')
				);
				if($people_detail['ID']){
					$this->trip_mod->updateTripPeopleDetail($arr_people_detail,$ID);
				}else{
					$this->trip_mod->addTripPeopleDetail($arr_people_detail);
				}
			}else{
				$this->trip_mod->deleteTripPeopleDetail($ID);
			}
			$package_detail = $this->CommonMethodsModel->cleanQuery($_POST['package_detail']);
			if($package_detail==1){
				$package_detail = $this->trip_mod->getPackageDetailByTripID($ID);
				$arr_package_detail = array(
					'trip'                  => $this->CommonMethodsModel->cleanQuery($ID),
					'packages_count' => $this->CommonMethodsModel->cleanQuery($_POST['packages_count']),
					'airline_allowed_weight' => $this->CommonMethodsModel->cleanQuery($_POST['airline_allowed_weight']),
					'weight_carried' => $this->CommonMethodsModel->cleanQuery($_POST['weight_carried']),
					'weight_accommodate' => $this->CommonMethodsModel->cleanQuery($_POST['weight_accommodate']),
					'not_wish_to_carry' => $this->CommonMethodsModel->cleanQuery($_POST['not_wish_to_carry']),
					'comment' => $this->CommonMethodsModel->cleanQuery($_POST['package_comment']),
					'modified' => date('Y-m-d H:i:s')
				);
				if($package_detail['ID']){
					$this->trip_mod->updateTripPackageDetail($arr_package_detail);
				}else{
					$this->trip_mod->addTripPackageDetail($arr_package_detail);
				}
			}else{
				$this->trip_mod->deleteTripPackageDetail($ID);
			}
			$this->_redirect("/".$this->controller);
		}else{
			$this->view->people_detail = $this->trip_mod->getPeopleDetailByTripID($ID);
			$this->view->package_detail = $this->trip_mod->getPackageDetailByTripID($ID);
			$this->view->user_locations = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
			$this->view->countries = $this->CommonMethodsModel->getCountries();
		}
	}
	
	public function deletetripAction() {
		$ID = $this->_helper->Common->decrypt($this->_request->getParam('ID'));
		$trip = $this->trip_mod->getTrip($ID);
		$this->view->trip = $trip;
		$this->view->ID = $ID;
		if($_POST){
			$ID = $this->CommonMethodsModel->cleanQuery($_POST['ID']);
			$this->trip_mod->deleteTrip($ID);
			$this->trip_mod->deleteTripPeopleDetail($ID);
			$this->trip_mod->deleteTripPackageDetail($ID);
			$this->_redirect("/".$this->controller);
		}else{
			
		}
	}
	
	public function notificationsAction() {	
            $this->TripPlugin = $this->plugin('TripPlugin');
            if(!($this->sessionObj->userid) || $this->sessionObj->userid == ''){
                $this->redirect()->toRoute('home');
            }
             $viewArray = array();
             $viewArray = array(
                'comSessObj'    =>  $this->sessionObj,
                'countries' => $this->CommonMethodsModel->getCountries(),
            );
			$viewArray['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
			$viewArray['prof']  = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			$viewArray['alrt'] = $this->DashboardModel->getalert($this->sessionObj->userid);
            $viewArray['noft'] = $this->DashboardModel->getnoft($this->sessionObj->userid);
            $viewArray['dashmsg'] = $this->DashboardModel->getdashmsg($this->sessionObj->userid);
            $viewArray['trval'] = $this->DashboardModel->gettrval($this->sessionObj->userid);
            $viewArray['seeker'] = $this->DashboardModel->getseeker($this->sessionObj->userid);
            
            
           // echo $this->sessionObj->userid;die;
            $viewArray['disputes'] = $this->DashboardModel->getdisputes($this->sessionObj->userid);  
            
			$viewArray['supertrav'] = $this->DashboardModel->getsupertrav($this->sessionObj->userid);
			$viewArray['notification'] = $this->CommonMethodsModel->getNotifications($this->sessionObj->userid);
			$viewArray['getTravellerEnquiries'] = $this->DashboardModel->getTravellerEnquiries($this->sessionObj->userid);
                        $viewArray['travel_plans'] = $this->TripModel->getTravelTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid, false);
                         $viewArray['user_role'] =$this->sessionObj->userrole;
			$this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
			$this->layout()->breadCrumbArr      = array(
            array(
                'label' => 'Dashboard',
                'title' => 'Dashboard',
                'active' => false,
                'redirect' => '/dashboard'
            ),
            array(
                'label' => 'Notifications',
                'title' => 'Notifications',
                'active' => true,
                'redirect' => false //'/myaccount'
            )
			);
            $viewModel = new ViewModel();
            $viewModel->setVariables($viewArray);
            return $viewModel;
	}
        
  	public function disputelistAction() {	
            $this->TripPlugin = $this->plugin('TripPlugin');
            if(!($this->sessionObj->userid) || $this->sessionObj->userid == ''){
                $this->redirect()->toRoute('home');
            }
            
            $seeker_people = array(
                                        "I have issues with the trip lsiting created",
                                        "I have problems with the traveller who served my travel service",
                                        "My travel service requested is not initated/incomplete",
                                        "I have problems with sending or receiving request to/from travellers",
                                        "I have questions on cancellations/refund",
                                        "General/Other clarifications"

                                    );
            $seeker_package=array(
                                        "I have issues with the trip lsiting created",
                                        "I have problems with the traveller who served my travel service",
                                        "My travel service requested is not initated/incomplete",
                                        "I need to report missing/damaged items in my package",
                                        "I have problems with sending or receiving request to/from travellers",
                                        "I have questions on cancellations/refund",
                                        "General/Other clarifications",

                                    );
            
            
             $viewArray = array();
             $viewArray = array(
                'comSessObj'    =>  $this->sessionObj,
                'countries' => $this->CommonMethodsModel->getCountries(),
            );
			$viewArray['reviews']  = $this->CommonMethodsModel->getreviews($this->sessionObj->userid);
			$viewArray['prof']  = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
			$viewArray['alrt'] = $this->DashboardModel->getalert($this->sessionObj->userid);
            $viewArray['noft'] = $this->DashboardModel->getnoft($this->sessionObj->userid);
            $viewArray['dashmsg'] = $this->DashboardModel->getdashmsg($this->sessionObj->userid);
            $viewArray['trval'] = $this->DashboardModel->gettrval($this->sessionObj->userid);
            $viewArray['seeker'] = $this->DashboardModel->getseeker($this->sessionObj->userid);
            
            
           // echo $this->sessionObj->userid;die;
            $viewArray['disputes'] = $this->DashboardModel->getdisputes($this->sessionObj->userid);  
            
			$viewArray['supertrav'] = $this->DashboardModel->getsupertrav($this->sessionObj->userid);
			$viewArray['notification'] = $this->CommonMethodsModel->getNotifications($this->sessionObj->userid);
			$viewArray['getTravellerEnquiries'] = $this->DashboardModel->getTravellerEnquiries($this->sessionObj->userid);
                        $viewArray['travel_plans'] = $this->TripModel->getTravelTrips('upcoming', $this->sessionObj->userrole, $this->sessionObj->userid, false);

			$this->layout()->CommonMethodsModel = $this->CommonMethodsModel;
			$this->layout()->breadCrumbArr      = array(
            array(
                'label' => 'Dashboard',
                'title' => 'Dashboard',
                'active' => false,
                'redirect' => '/dashboard'
            ),
            array(
                'label' => 'Notifications',
                'title' => 'Notifications',
                'active' => true,
                'redirect' => false //'/myaccount'
            )
			);
            $viewModel = new ViewModel();
            $viewModel->setVariables($viewArray);
            return $viewModel;
	}

	public function getprofiledetails($userid,$user){
		$profile = [];
	
		$profile['completeurl'] = '';
		$profile['score'] = 0;
		foreach ($user as $key => $value) {

			switch($key){

				case "first_name":
				case "last_name":
				case "gender":
				case "dob":
				case "email_address":
				case "phone":
				case "language":
				case "location":
				case "image":
					if(!empty($value))
						$profile['score'] += 5;
					if(empty($value) && empty($profile['completeurl']))
						$profile['completeurl'] = 'myprofile?profAction=edit&Personal info';
				break;
				case "facebook_id":
				case "google_id":
				case "twitter_id":
				case "linkedin_id":
					if(!empty($value))
						$profile['score'] += 5;
					if(empty($value) && empty($profile['completeurl']))
						$profile['completeurl'] = 'myprofile?profAction=edit&My circles';
				break;
				case "emergency_full_name":
				case "emergency_phone":
				case "emergency_email":
				case "emergency_relationship":
					if(!empty($value))
						$profile['score'] += 1.5;
					if(empty($value) && empty($profile['completeurl']))
						$profile['completeurl'] = 'myprofile?profAction=edit&Personal emergency';
				break;
				case "userVerfication":
					if(!empty($value) && $value > 0)
						$profile['score'] += 20;
					if(empty($value) && empty($profile['completeurl']))
						$profile['completeurl'] = 'myprofile?profAction=edit&Trust and verification';
				break;
				case "paymentMethod":
					if(!empty($value) && $value > 0)
						$profile['score'] += 5;
					if(empty($value) && empty($profile['completeurl']))
						$profile['completeurl'] = 'myprofile?profAction=edit&payment method';
				break;

			}

			//echo $key."==".$value."<br>";
		}
//echo "<pre>";
//print_r($user);		
//die();
		return $profile;
	}
}