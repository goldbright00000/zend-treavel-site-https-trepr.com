<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\Session\Container;
use Zend\Mvc\MvcEvent;

class TripsController extends AbstractActionController
{

    var $TripModel;
    var $siteConfigs;
    var $basePath;

    public function __construct($data = false)
    {
        /* Loading models from factory
         * Call the model object using it's class name
         */
        if ($data['models'] && is_array($data['models'])) {
            foreach ($data['models'] as $model) {
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->basePath = $this->siteConfigs['baseUrl'];
    }

    public function onDispatch(\Zend\Mvc\MvcEvent $e)
    {
        if (isset($this->sessionObj->userid) && $this->sessionObj->userid != '') {
            $isUserLoggedIn = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
            if (!$isUserLoggedIn) {
                $this->redirect()->toRoute('logout');
            }
        }
        return parent::onDispatch($e);
    }

    public function init()
    {
        $this->CommonMethodsModel = new Application_Model_DbTable_Commonfunctions();
        $this->TripModel = new Application_Model_DbTable_Trip();
        $config_params = Zend_Registry::get('config');
        $this->generalvar = $config_params['general'];
        $this->view->generalvar = $this->generalvar;
        $this->sessionObj = new Zend_Session_Namespace('comSessObj');
        $this->controller = $this->getRequest()->getControllerName();
        $this->view->controller = $this->controller;
        $this->action = $this->getRequest()->getActionName();
        $this->view->action = $this->action;

        $this->view->clienturl = $this->generalvar[clienturl];
        $this->view->clientname = $this->generalvar[clientname];
        $this->currentsiteid = $this->generalvar['champaignsiteid'];
        $this->pagemodulename = 'pages';
        $this->pagemodule_config = $this->generalvar[$this->pagemodulename];
        $this->view->pagemodule_config = $this->pagemodule_config;

        /*
          if ($this->sessionObj->userid) {
          $user = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
          $this->view->user = $user;
          } else {
          $this->sessionObj->triggerlogin = 'yes';
          $url = $this->generalvar['nosslurl'];
          $this->_redirect($url . "/");
          }
         * 
         */

        $this->_helper->Common->initCommonFunction();
    }

    public function indexAction()
    {
        /* $this->_helper->layout->disableLayout(); */
        $this->view->upcoming_trips = $this->_helper->Trip->getTrips('upcoming');
        $this->view->completed_trips = $this->_helper->Trip->getTrips('completed');
        $this->view->cancelled_trips = $this->_helper->Trip->getTrips('cancelled');
    }

    public function addtripAction()
    {
        //print_r($_POST);die;
        if ($_POST) {
            $departure_date = $this->CommonMethodsModel->cleanQuery($_POST['departure_date']);
            $departure_time = $this->CommonMethodsModel->cleanQuery($_POST['departure_time']);
            $arrival_date = $this->CommonMethodsModel->cleanQuery($_POST['arrival_date']);
            $arrival_time = $this->CommonMethodsModel->cleanQuery($_POST['arrival_time']);
            $contact_address_type = $this->CommonMethodsModel->cleanQuery($_POST['contact_address_type']);
            $departure_date_time = date('Y-m-d H:i:s', strtotime($departure_date . ' ' . $departure_time));
            $arrival_date_time = date('Y-m-d H:i:s', strtotime($arrival_date . ' ' . $arrival_time));

            /* ticket image upload */
            $image_path = $this->generalvar['trips']['trips']['tickets'];
            $field_name = 'ticket_image';
            $file_name_prefix = $this->generalvar['trips']['trips']['filename'];
            $ticket_image = $this->CommonMethodsModel->uploadFile($field_name, $file_name_prefix, $image_path);

            /* ticket image upload */

            if ($contact_address_type == 'existing_address')
                $user_location = $this->CommonMethodsModel->cleanQuery($_POST['user_location']);
            elseif ($contact_address_type == 'new_address') {
                $arr_trip_contact = array(
                    'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                    'name' => $this->CommonMethodsModel->cleanQuery($_POST['contact_name']),
                    'street_address_1' => $this->CommonMethodsModel->cleanQuery($_POST['street_address_1']),
                    'street_address_2' => $this->CommonMethodsModel->cleanQuery($_POST['street_address_2']),
                    'city' => $this->CommonMethodsModel->cleanQuery($_POST['city']),
                    'state' => $this->CommonMethodsModel->cleanQuery($_POST['state']),
                    'zip_code' => $this->CommonMethodsModel->cleanQuery($_POST['zip_code']),
                    'country' => $this->CommonMethodsModel->cleanQuery($_POST['country']),
                    'phone' => $this->CommonMethodsModel->cleanQuery($_POST['phone']),
                    'fax' => $this->CommonMethodsModel->cleanQuery($_POST['fax']),
                    'latitude' => $this->CommonMethodsModel->cleanQuery($_POST['latitude']),
                    'longitude' => $this->CommonMethodsModel->cleanQuery($_POST['longitude']),
                    'modified' => date('Y-m-d H:i:s')
                );

                $user_location = $this->TripModel->addTripContactAddress($arr_trip_contact);
            }

            $arr_trip_detail = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'user_location' => $this->CommonMethodsModel->cleanQuery($user_location),
                'name' => $this->CommonMethodsModel->cleanQuery($_POST['trip_title']),
                'origin_location' => $this->CommonMethodsModel->cleanQuery($_POST['origin_location']),
                'destination_location' => $this->CommonMethodsModel->cleanQuery($_POST['destination_location']),
                'departure_date' => $this->CommonMethodsModel->cleanQuery($departure_date_time),
                'arrival_date' => $this->CommonMethodsModel->cleanQuery($arrival_date_time),
                'airline_name' => $this->CommonMethodsModel->cleanQuery($_POST['airline_name']),
                'flight_number' => $this->CommonMethodsModel->cleanQuery($_POST['flight_number']),
                'booking_status' => $this->CommonMethodsModel->cleanQuery($_POST['booking_status']),
                'cabin' => $this->CommonMethodsModel->cleanQuery($_POST['cabin']),
                'ticket_image' => $this->CommonMethodsModel->cleanQuery($ticket_image),
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );
            $ID = $this->TripModel->addTrip($arr_trip_detail);

            $people_detail = $this->CommonMethodsModel->cleanQuery($_POST['people_detail']);

            if ($people_detail == 1) {

                $arr_people_detail = array(
                    'trip' => $this->CommonMethodsModel->cleanQuery($ID),
                    'persons_travelling' => $this->CommonMethodsModel->cleanQuery($_POST['persons_travelling']),
                    'male_count' => $this->CommonMethodsModel->cleanQuery($_POST['male_count']),
                    'female_count' => $this->CommonMethodsModel->cleanQuery($_POST['female_count']),
                    'age_above_60_years' => $this->CommonMethodsModel->cleanQuery($_POST['age_above_60_years']),
                    'age_below_3_years' => $this->CommonMethodsModel->cleanQuery($_POST['age_below_3_years']),
                    'comment' => $this->CommonMethodsModel->cleanQuery($_POST['people_detail_comment']),
                    'modified' => date('Y-m-d H:i:s')
                );

                $this->TripModel->addTripPeopleDetail($arr_people_detail);

            }

            $package_detail = $this->CommonMethodsModel->cleanQuery($_POST['package_detail']);

            if ($package_detail == 1) {

                $arr_package_detail = array(
                    'trip' => $this->CommonMethodsModel->cleanQuery($ID),
                    'packages_count' => $this->CommonMethodsModel->cleanQuery($_POST['packages_count']),
                    'airline_allowed_weight' => $this->CommonMethodsModel->cleanQuery($_POST['airline_allowed_weight']),
                    'weight_carried' => $this->CommonMethodsModel->cleanQuery($_POST['weight_carried']),
                    'weight_accommodate' => $this->CommonMethodsModel->cleanQuery($_POST['weight_accommodate']),
                    'not_wish_to_carry' => $this->CommonMethodsModel->cleanQuery($_POST['not_wish_to_carry']),
                    'comment' => $this->CommonMethodsModel->cleanQuery($_POST['package_comment']),
                    'modified' => date('Y-m-d H:i:s')
                );

                $this->TripModel->addTripPackageDetail($arr_package_detail);

            }

            $this->_redirect("/" . $this->controller);
        } else {
            $this->view->user_locations = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
            $this->view->countries = $this->CommonMethodsModel->getCountries();
        }
    }

    public function addtripreviewAction()
    {

        $ID = $this->params('ID');

        if (!($this->sessionObj->userid) || $this->sessionObj->userid == '') {
            $this->redirect()->toRoute('home');
        }

        if (isset($_POST['tripreview']) && $_POST['tripreview']) {
            $revw = $this->CommonMethodsModel->cleanQuery($_POST['tripreview']);
            $trip_review = array(
                'trip_id' => $ID,
                'message' => $revw,
                'rating' => $this->CommonMethodsModel->cleanQuery($_POST['rating']),
                'recevier_id' => 0,
                'create_by' => $this->sessionObj->userid,
                'sender_id' => $this->sessionObj->userid
            );
            $this->TripModel->addTripreview($trip_review, $ID);
            $this->view->triprewadd = "Travel service review has been submited successfully.";
        }

        $this->view->countries = $this->CommonMethodsModel->getCountries();
        $this->view->currency = $this->CommonMethodsModel->getCurrencies();
        $this->view->triprew = $this->TripModel->getTripreview($ID, $this->sessionObj->userid);
    }

    public function inserttripreviewAction()
    {
        if (isset($_POST['tripreview']) && $_POST['tripreview']) {
            $this->TripPlugin = $this->plugin('TripPlugin');
            $tripId = $this->CommonMethodsModel->cleanQuery($_POST['tripId']);
            $seekerTripId = $this->CommonMethodsModel->cleanQuery($_POST['seekerTripId']);
            $revw = $this->CommonMethodsModel->cleanQuery($_POST['tripreview']);
            $trip_review = array(
                'trip_id' => $tripId,
                'seeker_trip_id' => $seekerTripId,
                'message' => $revw,
                'rating' => $this->CommonMethodsModel->cleanQuery($_POST['rating']['rating']),
                'behavior' => $this->CommonMethodsModel->cleanQuery($_POST['rating']['behavior']),
                'promptness' => $this->CommonMethodsModel->cleanQuery($_POST['rating']['promptness']),
                'communication' => $this->CommonMethodsModel->cleanQuery($_POST['rating']['communication']),
                'friendliness' => $this->CommonMethodsModel->cleanQuery($_POST['rating']['friendliness']),
                'flexibility' => $this->CommonMethodsModel->cleanQuery($_POST['rating']['flexibility']),
                'cooperation' => $this->CommonMethodsModel->cleanQuery($_POST['rating']['cooperation']),
                'support' => $this->CommonMethodsModel->cleanQuery($_POST['rating']['support']),
                'status' => $this->CommonMethodsModel->cleanQuery($_POST['tripStatus']),
                'recevier_id' => $this->CommonMethodsModel->cleanQuery($_POST['receiverId']),
                'create_by' => $this->sessionObj->userid,
                'sender_id' => $this->sessionObj->userid
            );
            $this->TripModel->addTripreview($trip_review, $tripId);

            if ($this->sessionObj->userrole == 'traveller') {
                $this->TripModel->updateSeekerTripArrivalDateFromTravelerTrip($seekerTripId);
                $this->TripModel->updateTripStatus($_POST['tripStatus'], $tripId, 'traveller');

                $hugedata1 = array('user_id' => $this->CommonMethodsModel->cleanQuery($_POST['receiverId']),
                    'notification_type' => 'service_completed',
                    'notification_subject' => 'Service Completed',
                    'notification_message' => "Your travel service is marked as completed. Please review and respond to the request within 24 hours.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array(
                    'user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has marked travel service as completed. Please take a decision at the earliest",
                    'create_date' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToAdminNotifications($hugedata);
            }
            if ($this->sessionObj->userrole == 'seeker') {
                $this->TripModel->updateTripStatus($_POST['tripStatus'], $seekerTripId, 'seeker');

                $hugedata1 = array('user_id' => $this->CommonMethodsModel->cleanQuery($_POST['receiverId']),
                    'notification_type' => 'service_completed',
                    'notification_subject' => 'Service Completed',
                    'notification_message' => "Your service trip marked as completed.",
                    'notification_added' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToNotifications($hugedata1);
                $uD = $this->CommonMethodsModel->getUser($this->sessionObj->userid);
                $username = $uD['first_name'];
                $hugedata = array(
                    'user_id' => $this->sessionObj->userid,
                    'message' => "" . $username . " has marked travel service as completed. Please take a decision at the earliest",
                    'create_date' => date("Y-m-d H:i:s")
                );
                $this->TripModel->addToAdminNotifications($hugedata);
            }
        }
        echo 'Completed';
        die;
    }

    public function edittripAction()
    {

        $ID = $this->_helper->Common->decrypt($this->_request->getParam('ID'));
        $trip = $this->TripModel->getTrip($ID);
        $this->view->trip = $trip;
        if ($_POST) {
            $departure_date = $this->CommonMethodsModel->cleanQuery($_POST['departure_date']);
            $departure_time = $this->CommonMethodsModel->cleanQuery($_POST['departure_time']);
            $arrival_date = $this->CommonMethodsModel->cleanQuery($_POST['arrival_date']);
            $arrival_time = $this->CommonMethodsModel->cleanQuery($_POST['arrival_time']);
            $contact_address_type = $this->CommonMethodsModel->cleanQuery($_POST['contact_address_type']);

            $departure_date_time = date('Y-m-d H:i:s', strtotime($departure_date . ' ' . $departure_time));
            $arrival_date_time = date('Y-m-d H:i:s', strtotime($arrival_date . ' ' . $arrival_time));
            if (isset($_FILES[$field_name])) {
                $image_path = $this->generalvar['trips']['trips']['images'];
                $field_name = 'ticket_image';
                $file_name_prefix = $this->generalvar['trips']['trips']['filename'];
                $ticket_image = $this->CommonMethodsModel->uploadFile($field_name, $file_name_prefix, $image_path);
            } else {
                $ticket_image = $trip['ticket_image'];
            }

            if ($contact_address_type == 'existing_address')
                $user_location = $this->CommonMethodsModel->cleanQuery($_POST['user_location']);

            elseif ($contact_address_type == 'new_address') {

                $arr_trip_contact = array(
                    'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                    'name' => $this->CommonMethodsModel->cleanQuery($_POST['contact_name']),
                    'street_address_1' => $this->CommonMethodsModel->cleanQuery($_POST['street_address_1']),
                    'street_address_2' => $this->CommonMethodsModel->cleanQuery($_POST['street_address_2']),
                    'city' => $this->CommonMethodsModel->cleanQuery($_POST['city']),
                    'state' => $this->CommonMethodsModel->cleanQuery($_POST['state']),
                    'zip_code' => $this->CommonMethodsModel->cleanQuery($_POST['zip_code']),
                    'country' => $this->CommonMethodsModel->cleanQuery($_POST['country']),
                    'phone' => $this->CommonMethodsModel->cleanQuery($_POST['phone']),
                    'fax' => $this->CommonMethodsModel->cleanQuery($_POST['fax']),
                    'latitude' => $this->CommonMethodsModel->cleanQuery($_POST['latitude']),
                    'longitude' => $this->CommonMethodsModel->cleanQuery($_POST['longitude']),
                    'modified' => date('Y-m-d H:i:s')
                );

                $user_location = $this->TripModel->addTripContactAddress($arr_trip_contact);
            }

            $arr_trip_detail = array(
                'user' => $this->CommonMethodsModel->cleanQuery($this->sessionObj->userid),
                'user_location' => $this->CommonMethodsModel->cleanQuery($user_location),
                'name' => $this->CommonMethodsModel->cleanQuery($_POST['trip_title']),
                'origin_location' => $this->CommonMethodsModel->cleanQuery($_POST['origin_location']),
                'destination_location' => $this->CommonMethodsModel->cleanQuery($_POST['destination_location']),
                /* 'departure_date' => $this->CommonMethodsModel->cleanQuery($departure_date_time),
                  //'arrival_date' => $this->CommonMethodsModel->cleanQuery($arrival_date_time), */
                'airline_name' => $this->CommonMethodsModel->cleanQuery($_POST['airline_name']),
                'flight_number' => $this->CommonMethodsModel->cleanQuery($_POST['flight_number']),
                'booking_status' => $this->CommonMethodsModel->cleanQuery($_POST['booking_status']),
                'cabin' => $this->CommonMethodsModel->cleanQuery($_POST['cabin']),
                'ticket_image' => $this->CommonMethodsModel->cleanQuery($ticket_image),
                'active' => 1,
                'created' => date('Y-m-d H:i:s'),
                'modified' => date('Y-m-d H:i:s')
            );

            $this->TripModel->updateTrip($arr_trip_detail, $ID);

            $people_detail = $this->CommonMethodsModel->cleanQuery($_POST['people_detail']);
            if ($people_detail == 1) {
                $people_detail = $this->TripModel->getPeopleDetailByTripID($ID);
                $arr_people_detail = array(
                    'trip' => $this->CommonMethodsModel->cleanQuery($ID),
                    'persons_travelling' => $this->CommonMethodsModel->cleanQuery($_POST['persons_travelling']),
                    'male_count' => $this->CommonMethodsModel->cleanQuery($_POST['male_count']),
                    'female_count' => $this->CommonMethodsModel->cleanQuery($_POST['female_count']),
                    'age_above_60_years' => $this->CommonMethodsModel->cleanQuery($_POST['age_above_60_years']),
                    'age_below_3_years' => $this->CommonMethodsModel->cleanQuery($_POST['age_below_3_years']),
                    'comment' => $this->CommonMethodsModel->cleanQuery($_POST['people_detail_comment']),
                    'modified' => date('Y-m-d H:i:s')
                );

                if ($people_detail['ID']) {
                    $this->TripModel->updateTripPeopleDetail($arr_people_detail, $ID);
                } else {
                    $this->TripModel->addTripPeopleDetail($arr_people_detail);
                }
            } else {
                $this->TripModel->deleteTripPeopleDetail($ID);
            }

            $package_detail = $this->CommonMethodsModel->cleanQuery($_POST['package_detail']);
            if ($package_detail == 1) {
                $package_detail = $this->TripModel->getPackageDetailByTripID($ID);
                $arr_package_detail = array(
                    'trip' => $this->CommonMethodsModel->cleanQuery($ID),
                    'packages_count' => $this->CommonMethodsModel->cleanQuery($_POST['packages_count']),
                    'airline_allowed_weight' => $this->CommonMethodsModel->cleanQuery($_POST['airline_allowed_weight']),
                    'weight_carried' => $this->CommonMethodsModel->cleanQuery($_POST['weight_carried']),
                    'weight_accommodate' => $this->CommonMethodsModel->cleanQuery($_POST['weight_accommodate']),
                    'not_wish_to_carry' => $this->CommonMethodsModel->cleanQuery($_POST['not_wish_to_carry']),
                    'comment' => $this->CommonMethodsModel->cleanQuery($_POST['package_comment']),
                    'modified' => date('Y-m-d H:i:s')
                );

                if ($package_detail['ID']) {
                    $this->TripModel->updateTripPackageDetail($arr_package_detail);
                } else {
                    $this->TripModel->addTripPackageDetail($arr_package_detail);
                }
            } else {
                $this->TripModel->deleteTripPackageDetail($ID);
            }

            $this->_redirect("/" . $this->controller);
        } else {
            $this->view->people_detail = $this->TripModel->getPeopleDetailByTripID($ID);
            $this->view->package_detail = $this->TripModel->getPackageDetailByTripID($ID);
            $this->view->user_locations = $this->CommonMethodsModel->getUserLocations($this->sessionObj->userid);
            $this->view->countries = $this->CommonMethodsModel->getCountries();
        }
    }

    public function deletetripAction()
    {
        $ID = $this->_helper->Common->decrypt($this->_request->getParam('ID'));
        $trip = $this->TripModel->getTrip($ID);
        $this->view->trip = $trip;
        $this->view->ID = $ID;
        if ($_POST) {
            $ID = $this->CommonMethodsModel->cleanQuery($_POST['ID']);
            $this->TripModel->deleteTrip($ID);
            $this->TripModel->deleteTripPeopleDetail($ID);
            $this->TripModel->deleteTripPackageDetail($ID);
            $this->_redirect("/" . $this->controller);
        } else {

        }
    }

    public function getpassengersfeeAction()
    {
        $serviceFee = $_POST['service_fee'];
        $conversionRate = $this->CommonMethodsModel->convertCurrency(1, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);
        $peopleServiceFee = $conversionRate * $serviceFee;
        $data['convertedPrice'] = number_format(ceil($peopleServiceFee));
        $travellerPrice = ceil((float)($peopleServiceFee - ($peopleServiceFee * 20 / 100)));
        $data['convertedTravellerPrice'] = number_format($travellerPrice);
        $data['conversionRate'] = number_format($conversionRate,2);
        echo json_encode($data);
        exit;
    }

    public function getpackagesfeeAction()
    {
        $packageserviceFee = 10;
        $packageserviceFee = $this->CommonMethodsModel->convertCurrency($packageserviceFee, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);
        $data['convertedPrice'] = number_format(ceil($packageserviceFee));
        echo json_encode($data);

        exit;
    }

    public function getprojectsfeeAction()
    {
        $packageserviceFee = 30;
        $packageserviceFee = $this->CommonMethodsModel->convertCurrency($packageserviceFee, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);
        $data['convertedPrice'] = number_format($packageserviceFee, 2);
        echo json_encode($data);
        exit;
    }

    public function searchflightsAction()
    {

    }

    public function getDistanceAction()
    {
        extract($_POST);
        $departureAirport = $this->CommonMethodsModel->getAirPort($orginCode);
        $arrivalAirport = $this->CommonMethodsModel->getAirPort($desCode);
        $distance = $this->CommonMethodsModel->distance($departureAirport['latitude'], $departureAirport['longitude'], $arrivalAirport['latitude'], $arrivalAirport['longitude']);

        echo $distance;
        exit;
    }

    public function searchflightviewAction()
    {
        $this->_helper->layout->disableLayout();
        /*  $this->_helper->viewRenderer->setNoRender(true);
          // $this->view->rest=$this->_helper->Common->curl_get_file_contents("http://partners.api.skyscanner.net/apiservices/browseroutes/v1.0/UK/GBP/en-GB/EDI/LHR/2016-04-15/2016-04-22?apiKey=ra846927894945552494219565854033"); */
    }

    function onewayAction()
    {
        $api_token = '4bc8339331bef86019688e0bf0244e22';
        $origin = $_POST['origin'];
        $destination = $_POST['destination'];
        $sign_string = '';
        /* $departure_date = explode('/',$_POST['departure_date']); */
        $departure_date = !empty($_POST['departure_date']) ? date('Y-m-d', strtotime($_POST['departure_date'])) : date('Y-m-d', time());
        /* $segments[] = array("date" => "2016-05-20","destination"=>"DEL","origin"=>"MAA"); */
        $segments[] = array("date" => $departure_date, "destination" => $destination, "origin" => $origin);
        $ipAddress = $_SERVER['REMOTE_ADDR'];
        if (array_key_exists('HTTP_X_FORWARDED_FOR', $_SERVER)) {
            $ipAddress = array_pop(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']));
        }
        $data = array(
            "marker" => "52726",
            /* "host" => $_SERVER['HTTP_HOST'], */
            "host" => $_SERVER['HTTP_HOST'],
            "user_ip" => $ipAddress,
            "locale" => "en",
            "trip_class" => "Y",
            "passengers" => array("adults" => 1, "children" => 0, "infants" => 0),
            "segments" => $segments
        );
        ksort($data);

        foreach ($data as $k => $v) {
            if (is_array($v)) {
                foreach ($v as $kk => $vv) {
                    if (is_array($vv)) {
                        foreach ($vv as $kk => $vvv) {
                            if ($sign_string)
                                $sign_string .= ':';
                            $sign_string .= $vvv;
                        }
                    } else {
                        if ($sign_string)
                            $sign_string .= ':';
                        $sign_string .= $vv;
                    }
                }
            } else {
                if ($sign_string)
                    $sign_string .= ':';
                $sign_string .= $v;
            }
        }
        $sign_string = $api_token . ':' . $sign_string;
        /* echo 'Signature string: '.$sign_string.'<br />';
          //echo "<br>"; */

        $signature = md5($sign_string);
        /* echo 'Signature: '.$signature.'<br />'; */

        $arr_data = array(
            "signature" => $signature,
            "marker" => "52726",
            "host" => $_SERVER['HTTP_HOST'],
            /* "user_ip" => "127.0.0.1", */
            "user_ip" => $ipAddress,
            "locale" => "en",
            "trip_class" => "Y",
            "passengers" => array("adults" => 1, "children" => 0, "infants" => 0),
            "segments" => $segments
        );

        $data_string = json_encode($arr_data);
        /* echo $data_string; */

        $c = curl_init();
        curl_setopt($c, CURLOPT_URL, 'https://api.travelpayouts.com/v1/flight_search');
		//curl_setopt($c, CURLOPT_URL, 'http://ad.vu/videos/test.php');
        curl_setopt($c, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
		curl_setopt($c, CURLOPT_USERAGENT, "Mozilla/5.0 (Windows; U; Windows NT 6.0; en-US; rv:1.9.2) Gecko/20100115 Firefox/3.6 (.NET CLR 3.5.30729)");
        curl_setopt($c, CURLOPT_POST, 1);
        curl_setopt($c, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
		//curl_setopt($c, CURLOPT_TIMEOUT, 10);
        $result = curl_exec($c);
/*if (curl_errno($c) > 0) {
	echo "cURL Error ($curl_errno): ".curl_error($c)." \n";exit;
}*/
        curl_close($c);
		//echo "<pre>";print_r($result); exit;
        $result = json_decode($result, true);

        // echo "<pre>";print_r($result); exit;


        /* $ch = curl_init('http://api.travelpayouts.com/v1/flight_search');
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json',
          'Content-Length: ' . strlen($data_string),
          "Accept:
          "Host: api.travelpayouts.com"
          )
          );

          $result = curl_exec($ch);

          $result = json_decode($result); */

        $flights = $this->getFlightSearchResults($result['search_id']);
        $this->sessionObj->flights = $flights;
        // echo '<pre>'; print_r($flights);exit;
        /* if( count($flights) == 0 ){
          $resArr = json_encode(array('result'=>'failure'));
          echo $resArr;exit;
          } */
        $arr_stops = array();
        if (count($flights) > 0) {
            foreach ($flights as $allflight) {
                if (isset($allflight['proposals']) && count($allflight['proposals']) > 0) {
                    foreach ($allflight['proposals'] as $proposal) {
                        $arr_stops[$proposal['max_stops']] = $proposal['max_stops'];
                    }
                }
            }
        }

        if (is_array($arr_stops))
            asort($arr_stops);

        /*  //echo "<pre>";print_r($arr_stops); exit;
          //echo json_encode($arr_stops); exit; */
        $optString = '';
        if (count($arr_stops) > 0) {
            $optString .= '<option value=""> - select stops - </option>';
            foreach ($arr_stops as $key => $arr_stop) {
                $val = ($key == 0) ? 'Direct' : $key;
                $optString .= '<option value="' . $key . '">' . $val . '</option>';
            }
            $resArr = json_encode(array('result' => 'success', 'optionData' => $optString));
            /* $resArr = json_encode(array('result'=>'failure')); */
        } else {
            $resArr = json_encode(array('result' => 'failure'));
        }
        echo $resArr;
        exit;
    }

    function getFlightSearchResults($search_id)
    {

        sleep(15);
        $s = curl_init();
        curl_setopt($s, CURLOPT_URL, 'http://api.travelpayouts.com/v1/flight_search_results?uuid=' . $search_id);
        curl_setopt($s, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($s);
        curl_close($s);
        $result = json_decode($result, true);

        /* $ch = curl_init('http://api.travelpayouts.com/v1/flight_search_results?uuid='.$search_id);
          curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
          curl_setopt($ch, CURLOPT_HTTPHEADER, array(
          'Content-Type: application/json')
          );
          $result = curl_exec($ch);
          $result = json_decode($result); */
        if ($result[0]['search_id']) {
            return $result;
        } else {
            $this->getFlightSearchResults($search_id);
        }
    }

    public function getflightsAction()
    {
        $flights = $this->sessionObj->flights;

        $stops = $_REQUEST['stops'];
        echo '<div class="table-responsive"><table id="getflightstauspopup" class="table table-striped table-bordered">';
        if (count($flights) > 0) {
            /* echo '<div class="col-sm-1">Flight</div>';
              // echo '<div class="col-sm-3">Airline</div>';
              // echo '<div class="col-sm-2">Dept</div>';
              // echo '<div class="col-sm-2">Arr</div>';
              // echo '<div class="col-sm-2">Dept Dt</div>';
              // echo '<div class="col-sm-2">Arr Dt</div>';
              // echo '<div class="col-sm-12" style="clear:both"></div>'; */

            echo '<thead><tr>';
            echo '<th>Flight</th>';
            echo '<th>Airline Name</th>';
            //echo '<th>Airline Number</th>';
            echo '<th>Dept</th>';
            echo '<th>Arr</th>';
            echo '<th>Dept Dt</th>';
            echo '<th>Arr Dt</th>';
            echo '<th></th>';
            echo '</tr></thead>';

            $arrRoutes = array();
            foreach ($flights as $key => $allflight) {
                if (isset($allflight['proposals']) && count($allflight['proposals']) > 0) {

                    foreach ($allflight['proposals'] as $flightkey => $proposal) {
                        if ($proposal['max_stops'] == $stops) {
                            if (count($proposal['segment'][0]['flight']) > 0) {
                                /*
                                  foreach($proposal->segment[0]->flight as $key => $flight){
                                  $flight_count = $key+1;
                                  echo '<div class="col-sm-1">'.$flight_count.'</div>';
                                  $carrier = $flight->operating_carrier;
                                  //echo '<div class="col-sm-3">'.$flights[$flightkey]->airlines->$carrier->name.'-'.$flight->operating_carrier.$flight->number.'</div>';
                                  echo '<div class="col-sm-3">'.$allflight->airlines->$carrier->name.'-'.$flight->operating_carrier.$flight->number.'</div>';
                                  echo '<div class="col-sm-2">'.$flight->departure.'</div>';
                                  echo '<div class="col-sm-2">'.$flight->arrival.'</div>';
                                  echo '<div class="col-sm-2">'.date('d/m/Y H:i',$flight->departure_timestamp).'</div>';
                                  echo '<div class="col-sm-2">'.date('d/m/Y H:i',$flight->arrival_timestamp).'</div>';
                                  echo '<div class="col-sm-12" style="clear:both"></div>';
                                  }
                                 */
                                $str_flight_counts = '';
                                $str_flight_carrier = '';
                                $str_flight_departure = '';
                                $str_flight_arrival = '';
                                $str_flight_departure_timestamp = '';
                                $str_flight_arrival_timestamp = '';
                                $str_flight_carrier_name = '';
                                $str_flight_carrier_number = '';
                                $arrFlightRoute = array();
                                foreach ($proposal['segment'][0]['flight'] as $flight_count_key => $flight) {

                                    $flight_count = $flight_count_key + 1;
                                    $str_flight_counts .= $flight_count . '<br><br>';
                                    $carrier = $flight['operating_carrier'];
                                    $str_flight_carrier .= $allflight['airlines'][$carrier]['name'] . '-' . $flight['operating_carrier'] . $flight['number'] . '<br><br>';
                                    /*$str_flight_carrier_name .='<img src="http://pics.avs.io/50/50/'.$flight['operating_carrier'].'.png"> ' . $allflight['airlines'][$carrier]['name'] . '<br>';*/
                                    $str_flight_carrier_name .= '<div><span style="width: 100%;float: left;font-size:16px;">' . $allflight['airlines'][$carrier]['name'] . '</span><span style="width: 100%;color: #ababab;font-size:14px;font-weight:bold;">' . $flight['operating_carrier'] . '-' . $flight['number'] . '</span></div>';
                                    $str_flight_carrier_number .= $flight['operating_carrier'] . $flight['number'] . '<br><br>';
                                    $str_flight_departure .= $flight['departure'] . '<br><br>';
                                    $str_flight_arrival .= $flight['arrival'] . '<br><br>';
                                    $arrFlightRoute[$flight['departure']] = $flight['departure'];
                                    $arrFlightRoute[$flight['arrival']] = $flight['arrival'];
                                    date_default_timezone_set($allflight['airports'][$flight['departure']]['time_zone']);
                                    $str_flight_departure_timestamp .= date('d/m/Y H:i', $flight['departure_timestamp']) . '<br>' . $allflight['airports'][$flight['departure']]['time_zone'] . '<br>';
                                    date_default_timezone_set($allflight['airports'][$flight['arrival']]['time_zone']);
                                    $str_flight_arrival_timestamp .= date('d/m/Y H:i', $flight['arrival_timestamp']) . '<br>' . $allflight['airports'][$flight['arrival']]['time_zone'] . '<br>';
                                }
								
								$acOr = $acDe = '';
								
                                if (!empty($arrFlightRoute)) {
									$acOr = current($arrFlightRoute);
									$acDe = end($arrFlightRoute);
								
                                    $strFlighRoute = implode('-', $arrFlightRoute);
                                    $arrRoutes[$strFlighRoute] = $strFlighRoute;
                                }

                                echo '<tr>';
                                echo '<td>' . $str_flight_counts . '</td>';
                                echo '<td>' . $str_flight_carrier_name . '</td>';
                                //echo '<td>' . $str_flight_carrier_number . '</td>';
                                echo '<td>' . $str_flight_departure . '</td>';
                                echo '<td>' . $str_flight_arrival . '</td>';
                                echo '<td>' . $str_flight_departure_timestamp . '</td>';
                                echo '<td>' . $str_flight_arrival_timestamp . '</td>';
                                echo '<td style="vertical-align: middle;"><input data-origin="'.$acOr.'" data-destination="'.$acDe.'" type="button" value="select" onclick="selectFlight(' . $key . ',' . $flightkey . ', this)"></td>';
                                echo '</tr>';
                            }
                        }
                    }
                }
            }
        }
        echo '</table></div>';

        exit;
    }

    public function getCardDetailsAction()
    {
        $this->TripPlugin = $this->plugin('TripPlugin');
        $cardtype = $this->CommonMethodsModel->cleanQuery($_POST['cardtype']);
        $user_cards_details = $this->TripModel->getcarddetails($this->sessionObj->userid, false);
        $contentString = '';
        if ($user_cards_details) {
            foreach ($user_cards_details as $val) {
				$stripeCardInfo = $this->TripPlugin->retriveStripeSource($val['stripe_src_id']);
				if(isset($stripeCardInfo->card) && $val['stripe_src_id'] != ''){
					$val["card_no"] = 'xxxx xxxx xxxx '.$stripeCardInfo->card->last4;
					$val["card_valid"] = $stripeCardInfo->card->exp_month.' / '.$stripeCardInfo->card->exp_year;
					$val['card_type'] = strtolower($stripeCardInfo->card->brand);
					$val['card_billing_zipcode'] = $stripeCardInfo->owner->address->postal_code;
					$val['card_billing_country'] = $stripeCardInfo->owner->address->country;
				} else {
					continue;
				}
				
                //$cardNo = $this->TripPlugin->decrypt($val["card_no"]);
				$cardNo = $val["card_no"];
                $contentString .= ' <div class="form-group col-md-6 card-thumbWrap selected_card_details" data-id="' . $val['ID'] . '" data-card="' . $cardNo . '"  data-fname="' . $val['holder_name'] . '"  data-lname="' . $val['holder_last_name'] . '"  data-valid="' . $val['card_valid'] . '"  data-type="' . $val['card_type'] . '"  data-zip="' . $val['card_billing_zipcode'] . '" data-country="' . $val['card_billing_country'] . '">
                             <div class="card-thumb card-thumb-new1"> 
                                 <input value="' . $val['ID'] . '" class="existing_card" name="existing_card" id="existing_card_' . $val['ID'] . '" type="checkbox">
                                 <div class="card-thumb-num">' . $cardNo . '<img style="width:30px; margin-left:10px;" src="/public/img/payment/' . $val['card_type'] . '.png"></div>
                                 <div class="card-thumb-valid-new"><span>valid thru</span> ' . $val['card_valid'] . '</div>
                                     
                                    <div class="card-thumb-label">cardholder name</div>
                                   <div class="card-thumb-name">' . $val['holder_name'] . ' ' . $val['holder_last_name'] . '  ';
                $contentString .= ' </div>
                                 </div>
                              </div>';
            }
        }

        echo $contentString;
        exit;
    }

    public function getSearchflightsAction()
    {
        $flights = $this->sessionObj->flights;

        $stops = $_REQUEST['stops'];

        if (count($flights) > 0) {


            $arrRoutes = array();
            foreach ($flights as $key => $allflight) {

                if (isset($allflight['proposals']) && count($allflight['proposals']) > 0) {

                    foreach ($allflight['proposals'] as $flightkey => $proposal) {
                        if ($proposal['max_stops'] == $stops) {
                            if (count($proposal['segment'][0]['flight']) > 0) {

                                $str_flight_counts = '';
                                $str_flight_carrier = '';
                                $str_flight_departure = '';
                                $str_flight_arrival = '';
                                $str_flight_departure_timestamp = '';
                                $str_flight_arrival_timestamp = '';
                                $str_flight_carrier_name = '';
                                $str_flight_carrier_number = '';
                                $arrFlightRoute = array();
                                foreach ($proposal['segment'][0]['flight'] as $flight_count_key => $flight) {
                                    $flight_count = $flight_count_key + 1;
                                    $str_flight_counts .= $flight_count . '<br>';
                                    $carrier = $flight['operating_carrier'];
                                    $str_flight_carrier .= $allflight['airlines'][$carrier]['name'] . '-' . $flight['operating_carrier'] . $flight['number'] . '<br>';
                                    $str_flight_carrier_name .= $allflight['airlines'][$carrier]['name'] . '<br>';
                                    $str_flight_carrier_number .= $flight['operating_carrier'] . $flight['number'] . '<br>';
                                    $str_flight_departure .= $flight['departure'] . '<br>';
                                    $str_flight_arrival .= $flight['arrival'] . '<br>';
                                    $arrFlightRoute[$flight['departure']] = $flight['departure'];
                                    $arrFlightRoute[$flight['arrival']] = $flight['arrival'];
                                    $str_flight_departure_timestamp .= date('d/m/Y H:i', $flight['departure_timestamp']) . '<br>';
                                    $str_flight_arrival_timestamp .= date('d/m/Y H:i', $flight['arrival_timestamp']) . '<br>';
                                }

                                if (!empty($arrFlightRoute)) {
                                    $strFlighRoute = implode('-', $arrFlightRoute);
                                    $arrRoutes[$strFlighRoute] = $strFlighRoute;
                                }
                            }
                        }
                    }
                }
            }
        }
        $strOptions = ' <option selected="selected" value="all">Select route</option>';
        if (!empty($arrRoutes)) {
            foreach ($arrRoutes as $keyF => $valF) {
                $strOptions .= '<option value="' . $keyF . '">' . $keyF . '</option>';
            }
        } else {
            $strOptions .= '<option value="">No route found.</option>';
        }
        echo $strOptions;
        exit;
    }

    public function getSelectedFlightAction()
    {

        $flights = $this->sessionObj->flights;
        $flight_key = $_REQUEST['flight_key'];
        $proposal_key = $_REQUEST['proposal_key'];
        $service_type = $_REQUEST['service_type'];
        $flightSelected = '<table  class="table table-striped table-bordered">';
        if ($_REQUEST['flightSelectionType'] == 'auto') {
            if (count($flights) > 0) {
                $flightSelected .= '<thead><tr>';
                $flightSelected .= '<th>Flight</th>';
                $flightSelected .= '<th>Airline</th>';
                $flightSelected .= '<th>Dept</th>';
                $flightSelected .= '<th>Arr</th>';
                $flightSelected .= '<th>Dept Dt</th>';
                $flightSelected .= '<th>Arr Dt</th>';
                $flightSelected .= '<th>&nbsp;</th>';
                $flightSelected .= '</tr></thead>';
                $allflight = $flights[$flight_key];
                $proposal = $allflight['proposals'][$proposal_key];
                //$carryOn = $allflight['airlines']['9W']["baggage"]["carryOn"];

                if (count($proposal['segment'][0]['flight']) > 0) {
                    /* foreach($proposal->segment[0]->flight as $key => $flight){
                      $flight_count = $key+1;
                      echo '<div class="col-sm-1">'.$flight_count.'</div>';
                      $carrier = $flight->operating_carrier;
                      //echo '<div class="col-sm-3">'.$flights[$flightkey]->airlines->$carrier->name.'-'.$flight->operating_carrier.$flight->number.'</div>';
                      echo '<div class="col-sm-3">'.$allflight->airlines->$carrier->name.'-'.$flight->operating_carrier.$flight->number.'</div>';
                      echo '<div class="col-sm-2">'.$flight->departure.'</div>';
                      echo '<div class="col-sm-2">'.$flight->arrival.'</div>';
                      echo '<div class="col-sm-2">'.date('d/m/Y H:i',$flight->departure_timestamp).'</div>';
                      echo '<div class="col-sm-2">'.date('d/m/Y H:i',$flight->arrival_timestamp).'</div>';
                      echo '<div class="col-sm-12" style="clear:both"></div>';
                      }
                     */
                    $str_flight_counts = '';
                    $str_flight_carrier = '';
                    $str_flight_departure = '';
                    $str_flight_arrival = '';
                    $str_flight_departure_timestamp = '';
                    $str_flight_arrival_timestamp = '';
                    $distance = 0;
                    $departure_time = '';
                    foreach ($proposal['segment'][0]['flight'] as $key => $flight) {
                        /* $flight_count = $key+1;
                          $str_flight_counts = $flight_count;
                          $carrier = $flight->operating_carrier;
                          $str_flight_carrier = $allflight->airlines->$carrier->name.'-'.$flight->operating_carrier.$flight->number.'<br>';
                          $str_flight_carrier_name = $allflight->airlines->$carrier->name;
                          $str_flight_operating_carrier_name = $flight->operating_carrier;
                          $str_flignt_number = $flight->number;
                          $str_flight_departure = $flight->departure;
                          $str_flight_arrival = $flight->arrival.'<br>';
                          $str_flight_departure_timestamp = date('d/m/Y H:i',$flight->departure_timestamp);
                          $str_flight_arrival_timestamp = date('d/m/Y H:i',$flight->arrival_timestamp); */
                        $carryOn = '';//$allflight['airlines'][$flight['operating_carrier']]["baggage"]["carryOn"];
                        //echo date('h:i A', strtotime($flight['departure_time'])); die;
                        $this->sessionObj->carryOn = $carryOn;
                        $flight_count = $key + 1;
                        $str_flight_counts = $flight_count;
                        $carrier = $flight['operating_carrier'];
                        //$str_flight_carrier = $allflight['airlines'][$carrier]['name'] . '-' . $flight['operating_carrier'] . $flight['number'] . '<br>';
                        $str_flight_carrier = '<div style="float:left;"><img src="https://pics.avs.io/60/60/' . $flight['operating_carrier'] . '.png"> &nbsp;' . $allflight['airlines'][$carrier]['name'] . '<span style="float:left;width: 49%;text-align: right;margin-top: -10px;font-size: 13px;font-weight:bold;color: #ababab;">' . $flight['operating_carrier'] . '-' . $flight['number'] . '</span></div><br>';
                        $str_flight_carrier_name = $allflight['airlines'][$carrier]['name'];
                        $str_flight_carrier_number = $flight['operating_carrier'] . $flight['number'];
                        $str_flight_departure = $flight['departure'];
                        $str_flight_arrival = $flight['arrival'];
                        date_default_timezone_set($allflight['airports'][$flight['departure']]['time_zone']);
                        $str_flight_departure_timestamp = date('d/m/Y H:i', $flight['departure_timestamp']) . '<br>' . $allflight['airports'][$flight['departure']]['time_zone'];;
                        date_default_timezone_set($allflight['airports'][$flight['arrival']]['time_zone']);
                        $str_flight_arrival_timestamp = date('d/m/Y H:i', $flight['arrival_timestamp']) . '<br>' . $allflight['airports'][$flight['arrival']]['time_zone'];
                        $departure_airport = $this->TripModel->getAirportByCode($flight['departure']);
                        $arrival_airport = $this->TripModel->getAirportByCode($flight['arrival']);
                        $perdistance = $this->CommonMethodsModel->distance($departure_airport['latitude'], $departure_airport['longitude'], $arrival_airport['latitude'], $arrival_airport['longitude']);
                        $distance += $perdistance;
                        $str_dep_lat = $departure_airport['latitude'];
                        $str_dep_lng = $departure_airport['longitude'];
                        $str_arr_lat = $arrival_airport['latitude'];
                        $str_arr_lng = $arrival_airport['longitude'];
                        $flightDetails[] = array(
                            'airline_name' => $allflight['airlines'][$carrier]['name'],
                            'airline_number' => $flight['operating_carrier'] . $flight['number'],
                            'carrier' => $flight['operating_carrier'],
                            'number' => $flight['number'],
                            'departure' => $flight['departure'],

                            'arrival' => $flight['arrival'],
                            'departure_date' => date('Y-m-d H:i:s', $flight['departure_timestamp']),
                            'arrival_date' => date('Y-m-d H:i:s', $flight['arrival_timestamp']),
                            'depature_latitude' => $departure_airport['latitude'],
                            'depature_longitude' => $departure_airport['longitude'],
                            'arrival_latitude' => $arrival_airport['latitude'],
                            'arrival_longitude' => $arrival_airport['longitude'],
                            'distance' => round($perdistance)
                        );
                        $departure_time = date('h:i A', strtotime($flight['departure_time']));
                        $arrDepature[$str_flight_departure] = $str_flight_departure;
                        $arrDepature[$str_flight_arrival] = $str_flight_arrival;
                        $fligtData = $flight['operating_carrier'] . '@@' . $flight['number'] . '@@' . $flight['departure'] . '@@' . $flight['arrival'] . '@@' . date('Y-m-d H:i:s', $flight['departure_timestamp']) . '@@' . date('Y-m-d H:i:s', $flight['arrival_timestamp']) . '@@' . $allflight['airlines'][$carrier]['name'] . '@@' . $flight['operating_carrier'] . $flight['number'] . '@@' . $perdistance;
                        $flightSelected .= '<tr>';
                        $flightSelected .= '<td>' . $str_flight_counts . '</td>';
                        $flightSelected .= '<td>' . $str_flight_carrier . '</td>';
                        $flightSelected .= '<td>' . $str_flight_departure . '</td>';
                        $flightSelected .= '<td>' . $str_flight_arrival . '</td>';
                        $flightSelected .= '<td>' . $str_flight_departure_timestamp . '</td>';
                        $flightSelected .= '<td>' . $str_flight_arrival_timestamp . '</td>';
                        $flightSelected .= '<td>'
                            . '<a id="mapPopUp" href="#check-flight-status" class="popup-map btn-sm btn-primary" '
                            . 'data-effect="mfp-zoom-out" onclick="checkFightStatus(\'' . $fligtData . '\');">'
                            . 'Check Status</a></td>';
                        $flightSelected .= '</tr>';
                    }
                }
            }
        } else if ($_REQUEST['flightSelectionType'] == 'manual') {
            if (!empty($_REQUEST['manual_airline_name'])) {
                $manual_airline_name = $_REQUEST['manual_airline_name'];
                $manual_airline_number = $_REQUEST['manual_airline_number'];
                $manual_airline_carrier = $_REQUEST['manual_airline_carrier'];
                $manual_departure_date = $_REQUEST['manual_departure_date'];
                $manual_departure_time = $_REQUEST['manual_departure_time'];
                $manual_arrival_date = $_REQUEST['manual_arrival_date'];
                $manual_arrival_time = $_REQUEST['manual_arrival_time'];

                $distance = 0;
                $flightSelected .= '<thead><tr>';
                $flightSelected .= '<th>Flight</th>';
                $flightSelected .= '<th>Airline</th>';
                $flightSelected .= '<th>Dept</th>';
                $flightSelected .= '<th>Arr</th>';
                $flightSelected .= '<th>Dept Dt</th>';
                $flightSelected .= '<th>Arr Dt</th>';
                $flightSelected .= '<th></th>';
                $flightSelected .= '</tr></thead>';
                foreach ($_REQUEST['manual_airline_name'] as $key => $flight) {

                    $departureAirport = $this->CommonMethodsModel->getAirPort($_REQUEST['manual_origin_location_' . ($key + 1) . '_id']);
                    $arrivalAirport = $this->CommonMethodsModel->getAirPort($_REQUEST['manual_destination_location_' . ($key + 1) . '_id']);
                    $departureTime = strtotime($manual_departure_date[$key] . ' ' . $manual_departure_time[$key]);
                    $departureDateTime = date('Y-m-d H:i:s', $departureTime);
                    $arrivalTime = strtotime($manual_arrival_date[$key] . ' ' . $manual_arrival_time[$key]);
                    $arrivalDateTime = date('Y-m-d H:i:s', $arrivalTime);
                    $perdistance = $this->CommonMethodsModel->distance($departureAirport['latitude'], $departureAirport['longitude'], $arrivalAirport['latitude'], $arrivalAirport['longitude']);
                    $flightDetails[] = array(//airline_name_1_org_name
                        'airline_name' => isset($_REQUEST['airline_name_' . ($key + 1) . '_org_name']) ? $_REQUEST['airline_name_' . ($key + 1) . '_org_name'] : $manual_airline_name[$key],
                        'airline_number' => $manual_airline_carrier[$key] . $manual_airline_number[$key],
                        'carrier' => $manual_airline_carrier[$key],
                        'number' => $manual_airline_number[$key],
                        'departure' => $_REQUEST['manual_origin_location_' . ($key + 1) . '_code'],
                        'arrival' => $_REQUEST['manual_destination_location_' . ($key + 1) . '_code'],
                        'departure_date' => $departureDateTime,
                        'arrival_date' => $arrivalDateTime,
                        'depature_latitude' => $departureAirport['latitude'],
                        'depature_longitude' => $departureAirport['longitude'],
                        'arrival_latitude' => $arrivalAirport['latitude'],
                        'arrival_longitude' => $arrivalAirport['longitude'],
                        'distance' => round($perdistance),
                    );
                    $distance += $perdistance;
                    $str_flight_departure = $_REQUEST['manual_origin_location_' . ($key + 1) . '_code'];
                    $str_flight_arrival = $_REQUEST['manual_destination_location_' . ($key + 1) . '_code'];
                    $arrDepature[$str_flight_departure] = $str_flight_departure;
                    $arrDepature[$str_flight_arrival] = $str_flight_arrival;

                    $airline_name = isset($_REQUEST['airline_name_' . ($key + 1) . '_org_name']) ? $_REQUEST['airline_name_' . ($key + 1) . '_org_name'] : $manual_airline_name[$key];
                    $airline_number = $manual_airline_carrier[$key] . $manual_airline_number[$key];
                    $fligtData = $manual_airline_carrier[$key] . '@@' . $manual_airline_number[$key] . '@@' . $_REQUEST['manual_origin_location_' . ($key + 1) . '_code'] . '@@' . $_REQUEST['manual_destination_location_' . ($key + 1) . '_code'] . '@@' . $departureDateTime . '@@' . $arrivalDateTime . '@@' . $airline_name . '@@' . $airline_number;

                    $flightSelected .= '<tr>';
                    $flightSelected .= '<td>' . ($key + 1) . '</td>';
                    $flightSelected .= '<td>' . $airline_name . '-' . $airline_number . '</td>';
                    $flightSelected .= '<td>' . $_REQUEST['manual_origin_location_' . ($key + 1) . '_code'] . '</td>';
                    $flightSelected .= '<td>' . $_REQUEST['manual_destination_location_' . ($key + 1) . '_code'] . '</td>';
                    $flightSelected .= '<td>' . $departureDateTime . '</td>';
                    $flightSelected .= '<td>' . $arrivalDateTime . '</td>';
                    $flightSelected .= '<td>'
                        . '<a id="mapPopUp" href="#check-flight-status" class="popup-map btn-sm btn-primary" '
                        . 'data-effect="mfp-zoom-out" onclick="checkFightStatus(\'' . $fligtData . '\');">'
                        . 'Check Status</a></td>';
                    $flightSelected .= '</tr>';
                }
            }
        }
        $flightSelected .= '</table>';
        /* echo '@SPLIT@'.$distance; */
        $service_code = 1;
        if ($service_type == 'people')
            $service_code = 1;
        elseif ($service_type == 'package')
            $service_code = 2;
        elseif ($service_type == 'project')
            $service_code = 3;
        $service_fee = $this->TripModel->getTariffServices($service_code);
        $tripRoute = '';
        if (isset($arrDepature) && !empty($arrDepature))
            $tripRoute = implode('-', $arrDepature);
        @$resArr = array(
            'flightTableHtml' => $flightSelected,
            'flightDetails' => $flightDetails,
            'service_fee' => round($service_fee['price'] * $distance),
            'distance' => $distance,
            'trip_route' => $tripRoute,
            'departure_time' => $departure_time
        );
        echo json_encode($resArr);
        exit;
    }

    public function getprojectcategoryAction()
    {
        $id = $_REQUEST['id'];
		$retString = '';
        $tasks_categories = $this->TripModel->getProjectTasksCategorytype();
        if (is_array($tasks_categories) && count($tasks_categories) > 0) {
            $retString = '<option value="">-- Select --</option>';
            foreach ($tasks_categories as $tasks_category) {
                $retString .= '<option value="' . $tasks_category['ID'] . '">' . $tasks_category['catname'] . '</option>';
            }
        }
        echo $retString;
        exit;
    }

    public function getprojectsubcategoryAction()
    {
        $id = $_REQUEST['id'];
        $tasks_categories = $this->TripModel->getProjectTasksCategorylisttype($id);
        if (count($tasks_categories) > 0) {
            $retString = '<option value="">-- Select --</option>';
            foreach ($tasks_categories as $tasks_category) {
                $retString .= '<option value="' . $tasks_category['ID'] . '">' . $tasks_category['subcatname'] . '</option>';
            }
        }
        echo $retString;
        exit;
    }

    public function getpackagesubcategoryAction()
    {
        $id = $_REQUEST['id'];
        $tasks_categories = $this->TripModel->getPackageTasksCategorylisttype($id);
        if (count($tasks_categories) > 0) {
            $retString = '<option value="">-- Select --</option>';
            foreach ($tasks_categories as $tasks_category) {
                $retString .= '<option value="' . $tasks_category['ID'] . '">' . $tasks_category['subcatname'] . '</option>';
            }
        }
        echo $retString;
        exit;
    }

    public function getfeebycountryanddistanceAction()
    {
        if (isset($_POST)) {
            //print_r($_POST);
            $departureAirport = $this->CommonMethodsModel->getAirPort($_POST['originCode']);
            $arrivalAirport = $this->CommonMethodsModel->getAirPort($_POST['destinationCode']);
            $distance = $this->CommonMethodsModel->distance($departureAirport['latitude'], $departureAirport['longitude'], $arrivalAirport['latitude'], $arrivalAirport['longitude']);
            $distancePrice = $this->TripModel->getDistancePrice(round($distance), $_POST['type_of_service']);
            $countryPrice = $this->TripModel->getCountryPrice($_POST['originCon'], $_POST['destinationCon'], $_POST['type_of_service']);
            $service_fees = (float)($distancePrice['distance_price'] + $countryPrice['country_price']);
			echo $service_fees;
            exit;
            /*if ($_POST['type_of_service'] == 'package' && $this->sessionObj->userrole == 'traveller') {
                echo (float)($service_fees * 0.5);
                exit;
            } else {
                echo $service_fees;
                exit;
            }*/
        }
    }

    public function getcalculatedpeoplepriceAction()
    {
        if (isset($_POST))
        {
			echo $serviceFee = $this->TripModel->getSeekerPeopleServiceFee($_POST);
			exit;
            /*$distance = $_POST['distance'];
            $trip_route = $_POST['trip_route'];
            $type_of_service = $_POST['type_of_service'];
            $people_count = $_POST['people_count'];
            $noofstops = $_POST['noofstops'];
            $distancePrice = $this->TripModel->getDistancePrice(round($distance), $type_of_service);

            $ree = explode('-', $trip_route);

            $departureAirport = $this->CommonMethodsModel->getAirPortByAirportCode(current($ree));
            $arrivalAirport = $this->CommonMethodsModel->getAirPortByAirportCode(end($ree));
            $countryPrice = $this->TripModel->getCountryPrice($departureAirport['country_code'], $arrivalAirport['country_code'], $type_of_service);
			
			$service_fees = (float)($distancePrice['distance_price'] + $countryPrice['country_price']);

            if (($type_of_service == 'package' || $type_of_service == 'project') && $this->sessionObj->userrole == 'traveller') {
				if($type_of_service == 'project'){
					$itemCategoryPricePrice = $this->TripModel->getItemPricePrice(100, 'project');
					$service_fees = $service_fees+$itemCategoryPricePrice['price_item_price'];
				}
                echo $service_fees;
                exit;
            }
            else {
				$noofstops = $noofstops== 0?'direct':$noofstops;
				$stopsPrice = $this->TripModel->getNoofstopsPrice($noofstops);
            	$service_fees = (float)($service_fees + $stopsPrice['stop_price']);
                echo $service_fees * $people_count;
                exit;
            }*/
        }
    }
	
    public function getcalculatedpackagepriceAction()
    {
        if (isset($_POST))
        {
            $distance = $_POST['distance'];
            $trip_route = $_POST['trip_route'];
            $type_of_service = 'package';
            $distancePrice = $this->TripModel->getDistancePrice(round($distance), $type_of_service);

            $ree = explode('-', $trip_route);

            $departureAirport = $this->CommonMethodsModel->getAirPortByAirportCode(current($ree));
            $arrivalAirport = $this->CommonMethodsModel->getAirPortByAirportCode(end($ree));
            $countryPrice   = $this->TripModel->getCountryPrice($departureAirport['country_code'], $arrivalAirport['country_code'], $type_of_service);

            $package_total_fee = array();

            for ($i = 1; $i <= $_POST['packages_count']; $i++)
            {
                $tot_prc = $this->TripModel->getItemCategoryPrice($_POST['category_' . $i] . $_POST['item_category_' . $i], 'package');

                if(!empty($_POST['item_weight_' . $i])){
                    $item_weight = $_POST['item_weight_' . $i];
                }
                else{
                    $item_weight = '0.5';
                }

                $tot_weight_prc = $this->TripModel->getWeightPrice($item_weight, 'package');

                $money = $this->CommonMethodsModel->convertCurrency($_POST['item_worth_' . $i], $_POST['total_cost_currency_ajax'], $this->CommonMethodsModel->getSiteCurrency());
                $itemPricePrice = $this->TripModel->getItemPricePrice($money, 'package');

                $package_total_fee[] = (float)($countryPrice['country_price'] + $distancePrice['distance_price'] + ($tot_prc['tot_item_price'] * $tot_weight_prc['weight_carried_price']) + $itemPricePrice['price_item_price']);
            }
            $packageTotalFee = array_sum($package_total_fee);

            echo $packageTotalFee;
            exit;
        }

    }

    public function getcalculatedprojectpriceAction()
    {
        if (isset($_POST['work_category']) && $_POST['work_category'] == 1)
        {
			$distance = $_POST['distance'];
			$type_of_service = 'project';
			$distancePrice = $this->TripModel->getDistancePrice(round($distance), $type_of_service);

			$countryPrice   = $this->TripModel->getCountryPrice($_POST['origin_location_country_code'], $_POST['destination_location_country_code'], $type_of_service);
		
            $tot_item_price = array();
			
			//$countryPrice['country_price'] = $distancePrice['distance_price'] = 0;
			
			$project_fees = array();
            for ($i = 1; $i <= $_POST['selected_project_products']; $i++) {
                
				$tot_prc = $this->TripModel->getProductCategoryPrice($_POST['product_category_' . $i] . $_POST['product_additional_requirements_category_' . $i], 'project');
                $tot_item_price[] = $tot_prc['tot_item_price'];
				
				$itemWeightPrice = $this->TripModel->getWeightPrice($_POST['item_weight_' . $i], 'project');
				
				$money = $this->CommonMethodsModel->convertCurrency($_POST['product_price_' . $i], $_POST['product_price_currency_' . $i], $this->CommonMethodsModel->getSiteCurrency());
            	$itemCategoryPricePrice = $this->TripModel->getItemPricePrice($money, 'project');
				
				$project_fees[] = (float)($countryPrice['country_price'] + $distancePrice['distance_price'] + ($tot_prc['tot_item_price'] * $itemWeightPrice['weight_carried_price']) + $itemCategoryPricePrice['price_item_price'] + $money + (($money/100)*5));
            }
			$projectTotalFee = array_sum($project_fees);
			
            echo $projectTotalFee;
            exit;
        }
        else
        {
            $tot_item_price = array();
            for ($i = 1; $i <= $_POST['selected_project_tasks']; $i++) {
                $tot_prc = $this->TripModel->getTaskCategoryPrice($_POST['task_category_' . $i] . $_POST['additional_requirements_category_' . $i], 'project');
                $tot_item_price[] = $tot_prc['tot_item_price'];
            }
            $itemCategoryPrice = array_sum($tot_item_price);
            echo $itemCategoryPrice;
            exit;
        }
    }

}
