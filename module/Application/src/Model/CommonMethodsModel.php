<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\DriverInterface;
use Zend\Session\Container;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\ResultSet\ResultSet;
use Interop\Container\ContainerInterface;


require_once(ACTUAL_ROOTPATH."Upload/src/Upload.php");

class CommonMethodsModel extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'core_country';
    protected $adapter;
    protected $sql;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->sql = new Sql($this->adapter);
    }

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function aasort(&$array, $key, $order = 'ASC')
    {
        $sorter = array();
        $ret = array();
        if (is_array($array))
            reset($array);
        if (count($array) > 0) {
            foreach ($array as $ii => $va) {
                $sorter[$ii] = $va[$key];
            }
        }
        if ($order == 'ASC')
            asort($sorter);
        elseif ($order == 'DESC')
            arsort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
        return $array;
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function trimLength($s, $maxlength)
    {
        $words = explode(' ', $s);
        $split = '';
        foreach ($words as $word) {
            if (strlen($split) + strlen($word) < $maxlength)
                $split .= $word . ' ';
            else
                break;
        }
        if (strlen($s) > $maxlength) {
            $split = trim($split) . " ...";
        }
        return $split;
    }

    public function splitImages($img)
    {
        $img = list($img_name, $img_type) = explode(".", $img);
        $xs = $img_name . "_xs." . strtolower($img_type);
        $th = $img_name . "_th." . strtolower($img_type);
        $logo = $img_name . "_logo." . strtolower($img_type);
        $reg = $img_name . "." . strtolower($img_type);
        $lrg = $img_name . "_lg." . strtolower($img_type);
        $as = $img_name . "_as." . strtolower($img_type);
        $og = $img_name . "_og." . strtolower($img_type);
        $arr_imgs = array(
            "small" => $xs,
            "thumbnail" => $th,
            "standard" => $reg,
            "large" => $lrg,
            "actual" => $as,
            "logo" => $logo,
            "original" => $og
        );
        return $arr_imgs;
    }

    public function timeZoneAdjust($format, $dat)
    {
        $dat = strtotime($dat);
        if (date('T', $dat) == 'PST') {
            $dateadjusted = date($format, $dat + 3600);
        } else {
            $dateadjusted = date($format, $dat);
        }
        return $dateadjusted;
    }

    public function cleanTags($txt)
    {
        $specialstuff = array(
            "<br />",
            "<br>",
            "<ul>",
            "<li>",
            "</li>",
            "</ul>",
            "<ol>",
            "</ol>"
        );
        $txt = str_replace($specialstuff, " ", stripslashes($txt));
        return $txt;
    }

    public function in_array_r($needle, $haystack)
    {
        foreach ($haystack as $item) {
            if ($item === $needle || (is_array($item) && $this->in_array_r($needle, $item))) {
                return true;
            }
        }
        return false;
    }

    public function getCountries()
    {
        // $sql1 = "SET NAMES utf8";
        // $statement1 = $this->adapter->query($sql1);
        // $res1 = $statement1->execute();
        $sql = "SELECT * FROM core_country";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->getResource()->fetchAll();
     
    }

    public function getCurrency()
    {
        $sql = "SELECT DISTINCT currency_code FROM core_country WHERE payment_allow=1";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->getResource()->fetchAll();
    }
    
     public function getCurrencySymbol($currency_code)
    {
        $sql1 = "SET NAMES utf8";
        $statement1 = $this->adapter->query($sql1);
        $res1 = $statement1->execute();
   
         
        $sql = "SELECT DISTINCT currency_symbol FROM core_country WHERE currency_code= '".$currency_code."'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->current();
    }

    public function getCountry($countryid)
    {
        $sql1 = "SET NAMES utf8";
        $statement1 = $this->adapter->query($sql1);
        $res1 = $statement1->execute();
        $countrysql = "SELECT * FROM core_country WHERE ID='" . $countryid . "'";
        $statement = $this->adapter->query($countrysql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
        /*$result = $this->_db->fetchRow($countrysql);        if ($result) {            return $result;        }*/
    }

    public function getCountryByCode($countrycode)
    {
        $countrysql = "SELECT code,phone_code,name FROM core_country WHERE code='" . $countrycode . "'";
        $statement = $this->adapter->query($countrysql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
        /*$result = $this->_db->fetchRow($countrysql);        if ($result) {            return $result;        }*/
    }

    public function getCurrenciesCode($countryCode)
    {
        $sql = "SELECT * FROM core_currency_rate WHERE currency = '" . $countryCode . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getCurrencies()
    {
        $sql = "SELECT * FROM core_currency_rate";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function getCountryCurrency()
    {
        $sql = "SELECT * FROM core_currency_rate ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function checkuseremailexistsornot($email)
    {
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
             return $res->current();
        } else
            return false;
    }

    public function checkuserexists($email, $password)
    {
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "' AND password='" . $password . "' AND active='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function checkusersocialexists($email)
    {
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'  AND active='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getUser($userid)
    {
        $sql = "SELECT * FROM users_user WHERE ID = '" . $userid . "' AND active='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getUserID($userid)
    {
        $sql = "SELECT * FROM users_user WHERE email_address = '" . $userid . "' AND active='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getUserByEmailId($email)
    {
        $sql = "SELECT * FROM users_user WHERE email_address = '" . $email . "' AND active='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getUserTrustVerifyDetails($user_id)
    {
        $sql = "SELECT * FROM `user_identity` AS `UI`, `user_identity_type` AS `UIT` WHERE user_id ='" . $user_id . "' and UI.ID = UIT.identity_type_id";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getTrustVerifyTypes()
    {
        $sql = "SELECT * FROM `user_identity_type` WHERE identity_type_deleted = 0";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getUserTrustVerifyTypes($user_id)
    {
        $sql = "SELECT * FROM user_identity_type AS UIT LEFT JOIN user_identity AS UI ON UIT.identity_type_id=UI.identity_type AND UI.user_id=" . $user_id;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getCountryByName($name, $type)
    {
        if ($type == 'country' && is_numeric($name)) {
            $sql = "SELECT * FROM `core_country` WHERE ID='" . $name . "'";
        } elseif ($type == 'country' && !is_numeric($name)) {
            $sql = "SELECT * FROM `core_country` WHERE name='" . $name . "'";
        } else if ($type == 'countryCode' && !is_numeric($name)) {
            $sql = "SELECT * FROM `core_country` WHERE code='" . $name . "'";
        } else {
            $sql = "SELECT * FROM `core_country` WHERE currency_code='" . $name . "'";
        }
        $sql1 = "SET NAMES utf8";
        $statement1 = $this->adapter->query($sql1);
        $res1 = $statement1->execute();
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getUserByEmail($email)
    {
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getUserByKey($userKey)
    {
        $sql = "SELECT * FROM users_user WHERE user_key='" . $userKey . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function checkUserEmailExists($email, $resType = 'list', $ID = false)
    {
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        if ($ID)
            $sql .= ' AND ID != ' . $ID;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function addUsersUser($user)
    {
        $emailExists = false;
        $email_address = $user['email_address'];
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email_address . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $emailExists = true;
        }
        if ($emailExists === false) {
            $insert = $this->sql->insert('users_user');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
            return $userid;
        } else {
            return 'emailExist';
        }
    }

    public function checkEmailUser($user, $userid = false)
    {
        $emailExists = false;
        $email_address = $user['email_address'];
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email_address . "'";
        if ($userid)
            $sql .= ' AND ID != ' . (int)$userid;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $emailExists = true;
        }
        if ($emailExists === false) {
            return false;
        } else {
            return true;
        }
    }

    public function changeUserPassword($ID, $newpwd)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set(array(
            "reset_password" => "1"
        ));
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
    }

    public function updateUserPassword($ID, $newpwd)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set(array(
            "password" => md5($newpwd),
            "reset_password" => "0"
        ));
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
    }

    public function getAirportsBySearch($q)
    {
        $sql = "SELECT * FROM core_airports WHERE (code LIKE '%" . $q . "%' OR city LIKE '%" . $q . "%' OR country LIKE '%" . $q . "%' OR name LIKE '%" . $q . "%') ORDER BY code ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function getAirlinesBySearch($q)
    {
        $sql = "SELECT * FROM airlines WHERE airline_carrier LIKE '" . $q . "%' OR airline_name LIKE '" . $q . "%'  ORDER BY airline_name ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function LoadUsersAddressBook($user)
    {
        $sql = "SELECT a.*, b.name AS country FROM users_user_locations AS a, core_country AS b WHERE a.user='" . $user . "' AND b.code = a.country AND a.deleted=0";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function uploadFile($field_name, $file_name_prefix, $image_path)
    {
        if (isset($_FILES[$field_name])) {
            $dirpath = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']);
            $token = md5(uniqid(rand(), 1));
            $un = substr($token, 0, 5);
            $ext = explode('.', $_FILES[$field_name]['name']);
            $file_ext = strtolower(end($ext));
            $file_name = $file_name_prefix . "_" . $un . "." . $file_ext;
            @move_uploaded_file($_FILES[$field_name]['tmp_name'], $dirpath . $image_path . $file_name);
            return $file_name;
        }
    }

    public function uploadProfileImage($field_name, $file_name, $path)
    {
        if (isset($_FILES[$field_name])) {
            $filename = $_FILES[$field_name]['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $file_name = $file_name;
            $handle = new \upload($_FILES[$field_name]);
            if ($handle->uploaded) {
                $handle->file_new_name_body = $file_name;
                $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/');
            }
            return $file_name;
        }
    }

    public function uploadFileToServer($fileName, $file_new_name_body, $path)
    {
        $uploadFileName = false;
        $objUpload = new \Upload($_FILES[$fileName]);
        if ($objUpload->uploaded) {
            $orgPicPath = $dir_pics = ACTUAL_ROOTPATH . '/uploads/' . $path . '/';
            $objUpload->file_new_name_body = $file_new_name_body;
            $objUpload->file_safe_name = true;
            $objUpload->Process($dir_pics);
            if ($objUpload->processed) {
                $dir_pics = $orgPicPath . '/thumb/';
                $objUpload->file_new_name_body = $file_new_name_body;
                $objUpload->file_safe_name = true;
                $objUpload->image_resize = true;
                /*//$objUpload->image_ratio_fill      = true;*/
                $objUpload->image_y = 143;
                $objUpload->image_x = 143;
                $objUpload->Process($dir_pics);
                $dir_pics = $orgPicPath . '/medium/';
                $objUpload->file_new_name_body = $file_new_name_body;
                $objUpload->file_safe_name = true;
                $objUpload->image_resize = true;
                /*  //$objUpload->image_ratio_fill      = true;*/
                $objUpload->image_y = 173;
                $objUpload->image_x = 181;
                $objUpload->Process($dir_pics);
                $dir_pics = $orgPicPath . '/large/';
                $objUpload->file_new_name_body = $file_new_name_body;
                $objUpload->file_safe_name = true;
                $objUpload->image_resize = true;
                /* //$objUpload->image_ratio_fill      = true;*/
                $objUpload->image_y = 325;
                $objUpload->image_x = 500;
                $objUpload->Process($dir_pics);
                $uploadFileName = $objUpload->file_dst_name;
            } else {
                $uploadFileName = false;
            }
        }
        if ($uploadFileName == false) {
            $result = array(
                'success' => false
            );
            $result['error'] = $objUpload->error;
        } else {
            $result = array(
                'success' => true
            );
            $result['uploadName'] = $uploadFileName;
        }
        return $result;
    }

    public function uploadUserDP($field_name, $file_name_prefix, $path, $configArray)
    {
        if (isset($_FILES[$field_name])) {
            $handle = new upload($_FILES[$field_name]);
            if ($handle->uploaded) {
                $file_name = $file_name_prefix . '_' . time();
                $handle->process($this->basePath() . '/public/uploads/' . $path . '/');
                $handle->file_new_name_body = $file_name;
                $handle->image_resize = true;
                if ($configArray && !empty($configArray)) {
                    foreach ($configArray as $config) {
                        $handle->image_x = $config['image_x'];
                        $handle->image_ratio_y = true;
                        $handle->process($this->basePath() . '/public/uploads/' . $path . '/' . $config['sub_dir_name'] . '/');
                    }
                    if ($handle->processed) {
                        $handle->clean();
                        return true;
                    } else {
                        return false;
                    }
                }
            }
            return $file_name;
        }
    }

    public function uploadImage($field_name, $file_name, $path, $configArray)
    {
        if (isset($_FILES[$field_name])) {
            $filename = $_FILES[$field_name]['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $file_name = $file_name;
            $handle = new \upload($_FILES[$field_name]);
            if ($handle->uploaded) {
                $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/');
                $handle->file_new_name_body = $file_name;
                $handle->image_resize = true;
                if ($configArray && !empty($configArray)) {
                    foreach ($configArray as $config) {
                        $handle->file_new_name_body = $file_name;
                        $handle->image_resize = true;
                        $handle->image_x = $config['image_x'];
                        $handle->image_ratio_y = true;
                        $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/' . $config['sub_dir_name'] . '/');
                    }
                    if ($handle->processed) {
                        $handle->clean();
                        $retArr = array(
                            'result' => 'success',
                            'fileName' => $file_name . '.' . $ext
                        );
                    } else {
                        $retArr = array(
                            'result' => 'failed',
                            'error' => $handle->error
                        );
                    }
                }
            } else {
                $retArr = array(
                    'result' => 'failed',
                    'error' => $handle->error
                );
            }
            return $retArr;
        }
    }

    public function uploadFiles($field_name, $file_name, $path)
    {
        if (isset($_FILES[$field_name])) {
            $filename = $_FILES[$field_name]['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $file_name = $file_name;
            $handle = new \upload($_FILES[$field_name]);
            if ($handle->uploaded) {
                $handle->file_new_name_body = $file_name;
                $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/');
                if ($handle->processed) {
                    $handle->clean();
                    $retArr = array(
                        'result' => 'success',
                        'fileName' => $file_name . '.' . $ext
                    );
                } else {
                    $retArr = array(
                        'result' => 'failed',
                        'error' => $handle->error
                    );
                }
            } else {
                $retArr = array(
                    'result' => 'failed',
                    'error' => $handle->error
                );
            }
            return $retArr;
        }
    }

    public function Listusercard($user)
    {
        $sql = "SELECT * FROM `users_user_card` WHERE user='" . $user . "' AND invisibile = 0";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function deleteUsersAddressBook($id, $user)
    {
        $sql = "DELETE FROM users_user_locations WHERE user='" . $user . "' AND `ID`='" . $id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
    }

    public function viewUsersAddressBook($id, $user)
    {
        $sql = "SELECT * FROM users_user_locations WHERE user='" . $user . "' AND `ID`='" . $id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function updteUsersAddressBook($user, $userid, $id)
    {
        $update = $this->sql->update();
        $update->table('users_user_locations');
        $update->set($user);
        $update->where(array(
            'ID' => $id,
            'user' => $userid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return true;
    }

    public function getSiteConfigs($module = 'GeneralSitewide')
    {
        $sql = "SELECT * FROM `site_configs` WHERE configModule='" . $module . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function addToNotification($notifyData)
    {
        $insert = $this->sql->insert('notifications');
        $insert->values($notifyData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        return $userid;
    }

    public function getTempFriendsRows($sessionId, $identifier)
    {
        $sql = "SELECT * FROM user_friends_list_temp WHERE user_session_id='" . $sessionId . "' AND user_id='" . $identifier . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    function checkInReferredEmails($userId, $email)
    {
        $sql = "SELECT * FROM user_invites WHERE user_id=" . (int)$userId . " AND `invited_email`='" . $email . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function updateSignedUpStatus($arr_sign_detail)
    {
        $update = $this->sql->update();
        $update->table('user_invites');
        $update->set($arr_sign_detail);
        $update->where(array(
            'invited_email' => $arr_sign_detail['invited_email'],
            'user_id' => $arr_sign_detail['user_id']
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return $results;
    }

    public function getUserLocations($userid, $primary = 'no', $except_deleted_id = false)
    {
        $filter_primary = ($primary == 'yes') ? " AND a.set_default = '1' " : "";
        $sql = "SELECT a.*,b.name as country_name FROM users_user_locations as a,core_country as b WHERE b.code=a.country AND a.user = '" . $userid . "'" . $filter_primary;
        if ($except_deleted_id) {
            $sql .= ' AND (a.deleted=0 OR a.ID=' . $except_deleted_id . ')';
        } else {
            $sql .= ' AND a.deleted=0';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
            /*return $res->current();*/
        } else
            return false;
    }

    public function getUserLocationByAddrId($id = false, $userId = false, $primary = false)
    {
        $sql = "SELECT a.*,b.name as country_name FROM users_user_locations as a,core_country as b WHERE b.code=a.country ";
        if ($id) {
            $sql .= ($id) ? " AND a.ID = $id " : "";
        }
        if ($userId) {
            $sql .= ($userId) ? " AND a.user = $userId " : "";
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;            
    }

    public function getLanguages()
    {
        $sql = "SELECT * FROM language where active='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function getLanguagesName($id = false)
    {
        $sql = "SELECT lang FROM language where `ID`=" . (int)$id . " AND active='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current()['lang'];
        } else
            return false;
    }

    public function Listprojectcategory($ids = '')
    {
		$where = '';
		if($ids != ''){
			$where = ' AND ID IN ('.$ids.') ';
		}
		
        $sql = "SELECT * FROM `project_category` WHERE  status='1'".$where;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function Listprojectsubcat($catid)
    {
        $sql = "SELECT * FROM `project_sub_category` WHERE `catid` IN (" . $catid . ") AND `status`='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function Listproductsubcat($catid)
    {
        $sql = "SELECT * FROM `project_sub_category` WHERE `catid`='" . $catid . "' AND `status`='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function gerProjectByCat($catid)
    {
        $sql = "SELECT * FROM `project_category` WHERE `ID`='" . $catid . "' AND `status`='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
	
	public function gerProjectByCatIds($catid)
    {
        $sql = "SELECT * FROM `project_category` WHERE `ID`IN (" . $catid . ") AND `status`='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return array();
    }

    public function getSubCatById($catid)
    {
        $sql = "SELECT S.*,P.catname FROM `project_sub_category` AS S JOIN project_category AS P ON (S.catid = P.ID) WHERE S.`ID`='" . $catid . "' AND S.`status`='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
	
	public function getSubCatByIds($catid)
    {
        $sql = "SELECT S.*,P.catname FROM `project_sub_category` AS S JOIN project_category AS P ON (S.catid = P.ID) WHERE S.`ID` IN (" . $catid . ") AND S.`status`='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return array();
    }

    public function getAirPort($ID)
    {
        $sql = "SELECT * FROM core_airports WHERE ID ='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getAirPortByAirportCode($ID)
    {
        $sql = "SELECT * FROM core_airports WHERE code ='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function mytriphistorycnt($userid)
    {
        $sql = "SELECT COUNT(*) AS count FROM trips WHERE user='" . $userid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getUserCard($userid, $primary = 'no', $ID = false)
    {
        $filter_primary = "";
        if ($primary == 'yes')
            $filter_primary .= " AND `primary` = '1' ";
        if ($ID)
            $filter_primary .= " AND `ID` = " . (int)$ID;
        $sql = "SELECT * FROM user_billing WHERE user = '" . $userid . "' $filter_primary";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getUserCardDetails($ID = false)
    {
        $filter_primary = "";
        if ($ID)
            $filter_primary .= " AND `ID` = " . (int)$ID;
        $sql = "SELECT * FROM users_user_card WHERE 1 $filter_primary";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function doPayPalPayment($param)
    {
        return true;
    }

    public function decrypt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBF";
        /*pls set your unique hashing key*/
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        /* hash*/
        $key = hash('sha256', $secret_key);
        /* iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning*/
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        /*decrypt the given text/string/number*/
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }

    public function encrypt($string)
    {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        /*pls set your unique hashing key*/
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        /* hash*/
        $key = hash('sha256', $secret_key);
        /* iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning*/
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        /*do the encyption given text/string/number*/
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }

    function runQuery($sql, $queryType = '')
    {
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($queryType == 'insert')
            return $this->adapter->getDriver()->getLastGeneratedValue();
    }

    public function getstaticPageRow($page_id)
    {
        $sql = "SELECT * FROM content_pages WHERE page_Id=" . $page_id . "";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    function getCustomsRow($resType = 'list')
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('customs_excise_duty')->where(array(
            'custom_Deleted' => 0,
            'custom_Disabled' => 0
        ));
        if ($resType == 'paginator') {
            return new Paginator(new DbSelect($selectObj, $this->adapter, $this->resultSetPrototype));
        } else if ($resType == 'list') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            } else
                return false;
        }
    }

    function getSiteCurrency()
    {
        $def_cury = "GBP";
        return $def_cury;
    }

    function getUserCurrency($currency)
    {
        $def_cury = "GBP";
        if ($currency) {
            $def_cury = $currency;
        }
        return $def_cury;
    }

    function getUserCurrencySymbol($currency)
    {
        $def_cury_sym = "&#xa3;";
        if ($currency) {
            $def_cury_sym = $currency;
        }
        return $def_cury_sym;
    }

    function convertCurrency($amount, $from, $to)
    {
        if (!empty($from) && !empty($to) && $from != $to && $amount > 0) {
            //$url = "https://finance.google.com/finance/converter?a=$amount&from=$from&to=$to";
            //$url = "http://apilayer.net/api/convert?access_key=dd8df8fbd30c5e1fbe069448db63cc69&from=$from&to=$to&amount=$amount&format=1";
            $url = "https://free.currencyconverterapi.com/api/v5/convert?q=".$from."_".$to."&compact=ultra&apiKey=".CURRENCY_CONVERTER_API;
            $data = json_decode(file_get_contents($url));
            $keyName = $from."_".$to;
            $converted = $amount * $data->$keyName;
			$converted = $converted > 0?$converted:0;
            return round($converted, 2);
        } else {
			$amount = $amount > 0?$amount:0;
            return round($amount, 2);
        }
    }

    public function addReport($reportData)
    {
        $insert = $this->sql->insert('report_list');
        $insert->values($reportData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        return $userid;
    }

    public function addMessage($messageData)
    {
        $insert = $this->sql->insert('message_list');
        $insert->values($messageData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        return $userid;
    }

    public function updteUserProfileData($dara, $userid)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set($dara);
        $update->where(array(
            'ID' => $userid,
        ));
        //echo $update;die;
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return true;
    }

    public function addOTPCodes($dara, $userid)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set($dara);
        $update->where(array(
            'ID' => $userid,
        ));
        // echo $update;die;
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return true;
    }

    public function verifyOTPCode($dara, $userid)
    {
        //
        $id = base64_decode($userid);
        $sql = "SELECT otp,phone FROM users_user_phone_otp WHERE otp='" . $dara['otp'] . "' AND phone='" . $dara['phone'] . "'";
    
        $setts = array("phone" => $dara['number'],"phone_verify" => '1','co_code' => $dara['co_code']);
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            //echo $id; die;

            $update1 = $this->sql->update();
            $update1->table('users_user');
            $update1->set($setts);
            $update1->where(array(
                'ID' => $id
            ));
            $statement1 = $this->sql->prepareStatementForSqlObject($update1);
            $results1 = $statement1->execute();
            return true;
        } else {
            return false;
        }

    }

    public function addOTPCodesSignUp($dara)
    {
        $sql = "SELECT * FROM users_user_phone_otp WHERE phone='" . $dara['phone'] . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $data = array(
                "otp" => $dara['otp'],
                'otp_added_on' => date('Y-m-d H:i:s')
            );
            $update = $this->sql->update();
            $update->table('users_user_phone_otp');
            $update->set($data);
            $update->where(array(
                'phone' => $dara['phone']
            ));
            // echo $update;die;
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();

            return true;
        } else {
            $insert = $this->sql->insert('users_user_phone_otp');
            $insert->values($dara);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
            return true;
        }
    }

    public function updatelocationtimebrowser($location, $browser, $userid)
    {
        $data = array(
            "login_location" => $this->cleanQuery($location),
            "login_browser" => $this->cleanQuery($browser),
            "login_time" => $this->cleanQuery(date("Y-m-d H:i:s"))
        );
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set($data);
        $update->where(array(
            'ID' => $userid,
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return true;
    }

    public function cleanQuery($string)
    {
        if (get_magic_quotes_gpc()) {
            $string = stripslashes($string);
        }
        if (phpversion() >= '4.3.0') {
        } else {
            $string = mysql_escape_string($string);
        }
        return $string;
    }

    public function makeAddressPrimary($dara, $userid)
    {
        $sett = array("set_default" => 0);
        $update = $this->sql->update();
        $update->table('users_user_locations');
        $update->set($sett);
        $update->where(array(
            'user' => $userid,
        ));
        //echo $update;die;
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();//return true;
        if ($results) {
            $setts = array("set_default" => 1);
            $update1 = $this->sql->update();
            $update1->table('users_user_locations');
            $update1->set($setts);
            $update1->where(array(
                'user' => $userid,
                'ID' => $dara['ID']
            ));
            $statement1 = $this->sql->prepareStatementForSqlObject($update1);
            $results1 = $statement1->execute();
            return true;
        } else {
            return false;
        }
    }

    public function getLoginNotifications($catid)
    {
        $sql = "SELECT login_location,login_browser,login_time FROM `users_user` WHERE `ID`='" . $catid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return array("data" => $res->getResource()->fetchAll());
        } else
            return false;
    }

    public function getNotifications($user_id)
    {
        $sql = "SELECT * FROM `notifications`
                  WHERE `user_id`='" . $user_id . "'
                  AND notification_type != 'verify_document'
                  ORDER BY `notification_id` DESC";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function getLatestNotifications($user_id)
    {
        /* $sql = "SELECT * FROM `notifications`
                  WHERE `user_id`='" . $user_id . "'
                  AND notification_type != 'verify_document'
                  AND notification_read = 0
                  ORDER BY `notification_id` DESC";*/
				  
			   $sql = "SELECT * FROM `notifications`
                  WHERE `user_id`='" . $user_id . "'
                  AND notification_read = 0
                  ORDER BY `notification_id` DESC";
				  
				  
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function closeNotification($notification_id)
    {
        $sql = "UPDATE notifications SET notification_read = 1 WHERE `notification_id`='" . $notification_id . "'";
        $statement = $this->adapter->query($sql);
        $statement->execute();
        return true;
    }

    public function abcdefg($data, $userid)
    {

        $sql = "UPDATE users_user SET show_phone='" . $data['show_phone'] . "',show_email='" . $data['show_email'] . "',show_address='" . $data['show_address'] . "' WHERE ID=" . $userid;
        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return true;
    }

    public function checkExistingMobile($number, $code, $user_id)
    {
        /*$sql = "SELECT * FROM `users_user` WHERE ID ='" . $user_id . "' AND phone='" . $number . "' AND co_code='" . $code . "'";*/
         $sql = "SELECT * FROM `users_user` WHERE ID ='" . $user_id . "' AND phone='" . $number . "' AND phone_verify='1'";



        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updatemobilenum($dara, $userid)
    {
        $setts = array("phone" => $dara['phone']);
        $update1 = $this->sql->update();
        $update1->table('users_user');
        $update1->set($setts);
        $update1->where(array(
            'ID' => $userid
        ));
        $statement1 = $this->sql->prepareStatementForSqlObject($update1);
        $results1 = $statement1->execute();
        return true;

    }

    public function getQuestion($text, $type)
    {
        if ($type == 'text') {
            $sql = "SELECT qa_question,qa_id FROM `users_question_answer` WHERE qa_question LIKE '%" . $text . "%' LIMIT 5";
        } else {
            $sql = "SELECT qa_question,qa_id FROM `users_question_answer` WHERE qa_category_id=" . $text;
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return array("data" => $res->getResource()->fetchAll());
        } else {
            return false;
        }
    }

    public function getAnswer($text)
    {
        $sql = "SELECT qa_answer,qa_id FROM `users_question_answer` WHERE qa_id= '" . $text . "'";
        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return array("data" => $res->getResource()->fetchAll());
        } else {
            return false;
        }
    }

    public function verifyemailtosignup($id)
    {
        $setts = array("active" => 1);
        $update1 = $this->sql->update();
        $update1->table('users_user');
        $update1->set($setts);
        $update1->where(array(
            'ID' => $id
        ));
        $statement1 = $this->sql->prepareStatementForSqlObject($update1);
        $results1 = $statement1->execute();
        return true;
    }

    public function completesignup($ID)
    {
        $IDD = base64_decode($ID);
        $setts = array("active" => 1, "phone_verify" => 1);
        $update1 = $this->sql->update();
        $update1->table('users_user');
        $update1->set($setts);
        $update1->where(array(
            'ID' => $IDD
        ));
        $statement1 = $this->sql->prepareStatementForSqlObject($update1);
        $results1 = $statement1->execute();
        return true;
    }

    public function getQACategories()
    {
        $sql = "SELECT * FROM `users_question_answer_categories` WHERE uqa_parent_id=0";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getQASubCategories($id)
    {
        $sql = "SELECT * FROM `users_question_answer_categories` WHERE uqa_parent_id=" . $id;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return array("data" => $res->getResource()->fetchAll());
        } else {
            return false;
        }
    }

    public function checkuserprofilestatus($email)
    {
        $sql = "SELECT * FROM `users_user` WHERE email_address='" . $email . "' AND active='1'";
        //echo $sql; exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function updateReferancesOfNewUser($email, $id)
    {
        $setts = array("requested_to_user" => $id);
        $update1 = $this->sql->update();
        $update1->table('user_reference_requests');
        $update1->set($setts);
        $update1->where(array(
            'requested_to_email' => $email
        ));
        $statement1 = $this->sql->prepareStatementForSqlObject($update1);
        $results1 = $statement1->execute();
        return true;
    }

    public function updateUserUniqueID($code, $id)
    {
        $setts = array("uniqueID" => $code);
        $update1 = $this->sql->update();
        $update1->table('users_user');
        $update1->set($setts);
        $update1->where(array(
            'ID' => $id
        ));
        $statement1 = $this->sql->prepareStatementForSqlObject($update1);
        $results1 = $statement1->execute();
        return true;
    }

    public function completetrip()
    {
        $sett = array("trip_status" => 1);
        $update = $this->sql->update();
        $update->table('seeker_trips');
        $update->set($sett);
        $update->where(array(
            'arrival_date' => $this->cleanQuery(date("Y-m-d H:i:s"))
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        //return true;
        $setts = array("trip_status" => 1);
        $update1 = $this->sql->update();
        $update1->table('trips');
        $update1->set($setts);
        $update1->where(array(
            'arrival_date' => $this->cleanQuery(date("Y-m-d H:i:s"))
        ));
        $statement1 = $this->sql->prepareStatementForSqlObject($update1);
        $results1 = $statement1->execute();
        return true;
    }

    public function getreviews($ID)
    {
        $sql = "SELECT * FROM `users_reviews` WHERE  recevier_id='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    function addEditNotifications($user, $id = '', $action = 'add')
    {

        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('notifications');
            $update->set($user);
            $update->where(array('notification_id' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $insert = $this->sql->insert('notifications');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        return $userid;
    }

    public function getIdentityCount($user_id)
    {
  
        $sql = "SELECT * FROM user_identity WHERE user_id = '" . (int)$user_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function setApprovalIdentities($ID, $status)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set(array(
            "approved_id" => $status
        ));
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
    }

    public function getParticulerTripFee($id, $userrole)
    {
        if ($userrole == 'traveller') {
            $sql = "SELECT * FROM traveller_people WHERE trip = '" . (int)$id . "'";
        } else {
            $sql = "SELECT * FROM seeker_people WHERE seeker_trip = '" . (int)$id . "'";
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else {
            return false;
        }
    }

    public function time_zone()
    {
        return array('Kwajalein' => '(GMT-12:00) International Date Line West',
            'Pacific/Midway' => '(GMT-11:00) Midway Island',
            'Pacific/Samoa' => '(GMT-11:00) Samoa',
            'Pacific/Honolulu' => '(GMT-10:00) Hawaii',
            'America/Anchorage' => '(GMT-09:00) Alaska',
            'America/Los_Angeles' => '(GMT-08:00) Pacific Time (US &amp; Canada)',
            'America/Tijuana' => '(GMT-08:00) Tijuana, Baja California',
            'America/Denver' => '(GMT-07:00) Mountain Time (US &amp; Canada)',
            'America/Chihuahua' => '(GMT-07:00) Chihuahua',
            'America/Mazatlan' => '(GMT-07:00) Mazatlan',
            'America/Phoenix' => '(GMT-07:00) Arizona',
            'America/Regina' => '(GMT-06:00) Saskatchewan',
            'America/Tegucigalpa' => '(GMT-06:00) Central America',
            'America/Chicago' => '(GMT-06:00) Central Time (US &amp; Canada)',
            'America/Mexico_City' => '(GMT-06:00) Mexico City',
            'America/Monterrey' => '(GMT-06:00) Monterrey',
            'America/New_York' => '(GMT-05:00) Eastern Time (US &amp; Canada)',
            'America/Bogota' => '(GMT-05:00) Bogota',
            'America/Lima' => '(GMT-05:00) Lima',
            'America/Rio_Branco' => '(GMT-05:00) Rio Branco',
            'America/Indiana/Indianapolis' => '(GMT-05:00) Indiana (East)',
            'America/Caracas' => '(GMT-04:30) Caracas',
            'America/Halifax' => '(GMT-04:00) Atlantic Time (Canada)',
            'America/Manaus' => '(GMT-04:00) Manaus',
            'America/Santiago' => '(GMT-04:00) Santiago',
            'America/La_Paz' => '(GMT-04:00) La Paz',
            'America/St_Johns' => '(GMT-03:30) Newfoundland',
            'America/Argentina/Buenos_Aires' => '(GMT-03:00) Georgetown',
            'America/Sao_Paulo' => '(GMT-03:00) Brasilia',
            'America/Godthab' => '(GMT-03:00) Greenland',
            'America/Montevideo' => '(GMT-03:00) Montevideo',
            'Atlantic/South_Georgia' => '(GMT-02:00) Mid-Atlantic',
            'Atlantic/Azores' => '(GMT-01:00) Azores',
            'Atlantic/Cape_Verde' => '(GMT-01:00) Cape Verde Is.',
            'Europe/Dublin' => '(GMT) Dublin',
            'Europe/Lisbon' => '(GMT) Lisbon',
            'Europe/London' => '(GMT) London',
            'Africa/Monrovia' => '(GMT) Monrovia',
            'Atlantic/Reykjavik' => '(GMT) Reykjavik',
            'Africa/Casablanca' => '(GMT) Casablanca',
            'Europe/Belgrade' => '(GMT+01:00) Belgrade',
            'Europe/Bratislava' => '(GMT+01:00) Bratislava',
            'Europe/Budapest' => '(GMT+01:00) Budapest',
            'Europe/Ljubljana' => '(GMT+01:00) Ljubljana',
            'Europe/Prague' => '(GMT+01:00) Prague',
            'Europe/Sarajevo' => '(GMT+01:00) Sarajevo',
            'Europe/Skopje' => '(GMT+01:00) Skopje',
            'Europe/Warsaw' => '(GMT+01:00) Warsaw',
            'Europe/Zagreb' => '(GMT+01:00) Zagreb',
            'Europe/Brussels' => '(GMT+01:00) Brussels',
            'Europe/Copenhagen' => '(GMT+01:00) Copenhagen',
            'Europe/Madrid' => '(GMT+01:00) Madrid',
            'Europe/Paris' => '(GMT+01:00) Paris',
            'Africa/Algiers' => '(GMT+01:00) West Central Africa',
            'Europe/Amsterdam' => '(GMT+01:00) Amsterdam',
            'Europe/Berlin' => '(GMT+01:00) Berlin',
            'Europe/Rome' => '(GMT+01:00) Rome',
            'Europe/Stockholm' => '(GMT+01:00) Stockholm',
            'Europe/Vienna' => '(GMT+01:00) Vienna',
            'Europe/Minsk' => '(GMT+02:00) Minsk',
            'Africa/Cairo' => '(GMT+02:00) Cairo',
            'Europe/Helsinki' => '(GMT+02:00) Helsinki',
            'Europe/Riga' => '(GMT+02:00) Riga',
            'Europe/Sofia' => '(GMT+02:00) Sofia',
            'Europe/Tallinn' => '(GMT+02:00) Tallinn',
            'Europe/Vilnius' => '(GMT+02:00) Vilnius',
            'Europe/Athens' => '(GMT+02:00) Athens',
            'Europe/Bucharest' => '(GMT+02:00) Bucharest',
            'Europe/Istanbul' => '(GMT+02:00) Istanbul',
            'Asia/Jerusalem' => '(GMT+02:00) Jerusalem',
            'Asia/Amman' => '(GMT+02:00) Amman',
            'Asia/Beirut' => '(GMT+02:00) Beirut',
            'Africa/Windhoek' => '(GMT+02:00) Windhoek',
            'Africa/Harare' => '(GMT+02:00) Harare',
            'Asia/Kuwait' => '(GMT+03:00) Kuwait',
            'Asia/Riyadh' => '(GMT+03:00) Riyadh',
            'Asia/Baghdad' => '(GMT+03:00) Baghdad',
            'Africa/Nairobi' => '(GMT+03:00) Nairobi',
            'Asia/Tbilisi' => '(GMT+03:00) Tbilisi',
            'Europe/Moscow' => '(GMT+03:00) Moscow',
            'Europe/Volgograd' => '(GMT+03:00) Volgograd',
            'Asia/Tehran' => '(GMT+03:30) Tehran',
            'Asia/Muscat' => '(GMT+04:00) Muscat',
            'Asia/Baku' => '(GMT+04:00) Baku',
            'Asia/Yerevan' => '(GMT+04:00) Yerevan',
            'Asia/Yekaterinburg' => '(GMT+05:00) Ekaterinburg',
            'Asia/Karachi' => '(GMT+05:00) Karachi',
            'Asia/Tashkent' => '(GMT+05:00) Tashkent',
            'Asia/Kolkata' => '(GMT+05:30) Calcutta',
            'Asia/Calcutta' => '(GMT+05:30) Calcutta',
            'Asia/Colombo' => '(GMT+05:30) Sri Jayawardenepura',
            'Asia/Katmandu' => '(GMT+05:45) Kathmandu',
            'Asia/Dhaka' => '(GMT+06:00) Dhaka',
            'Asia/Almaty' => '(GMT+06:00) Almaty',
            'Asia/Novosibirsk' => '(GMT+06:00) Novosibirsk',
            'Asia/Rangoon' => '(GMT+06:30) Yangon (Rangoon)',
            'Asia/Krasnoyarsk' => '(GMT+07:00) Krasnoyarsk',
            'Asia/Bangkok' => '(GMT+07:00) Bangkok',
            'Asia/Jakarta' => '(GMT+07:00) Jakarta',
            'Asia/Brunei' => '(GMT+08:00) Beijing',
            'Asia/Chongqing' => '(GMT+08:00) Chongqing',
            'Asia/Hong_Kong' => '(GMT+08:00) Hong Kong',
            'Asia/Urumqi' => '(GMT+08:00) Urumqi',
            'Asia/Irkutsk' => '(GMT+08:00) Irkutsk',
            'Asia/Ulaanbaatar' => '(GMT+08:00) Ulaan Bataar',
            'Asia/Kuala_Lumpur' => '(GMT+08:00) Kuala Lumpur',
            'Asia/Singapore' => '(GMT+08:00) Singapore',
            'Asia/Taipei' => '(GMT+08:00) Taipei',
            'Australia/Perth' => '(GMT+08:00) Perth',
            'Asia/Seoul' => '(GMT+09:00) Seoul',
            'Asia/Tokyo' => '(GMT+09:00) Tokyo',
            'Asia/Yakutsk' => '(GMT+09:00) Yakutsk',
            'Australia/Darwin' => '(GMT+09:30) Darwin',
            'Australia/Adelaide' => '(GMT+09:30) Adelaide',
            'Australia/Canberra' => '(GMT+10:00) Canberra',
            'Australia/Melbourne' => '(GMT+10:00) Melbourne',
            'Australia/Sydney' => '(GMT+10:00) Sydney',
            'Australia/Brisbane' => '(GMT+10:00) Brisbane',
            'Australia/Hobart' => '(GMT+10:00) Hobart',
            'Asia/Vladivostok' => '(GMT+10:00) Vladivostok',
            'Pacific/Guam' => '(GMT+10:00) Guam',
            'Pacific/Port_Moresby' => '(GMT+10:00) Port Moresby',
            'Asia/Magadan' => '(GMT+11:00) Magadan',
            'Pacific/Fiji' => '(GMT+12:00) Fiji',
            'Asia/Kamchatka' => '(GMT+12:00) Kamchatka',
            'Pacific/Auckland' => '(GMT+12:00) Auckland',
            'Pacific/Tongatapu' => '(GMT+13:00) Nukualofa');
    }
    
    public function getCoupon($coupon_code)
    {
        $sql = "SELECT offer_percentage FROM coupons WHERE coupon_code = '".$coupon_code."' and expires >= CURDATE() and status=1";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return 0;
    }
	
	public function getRewards($user_id)
    {
        $sql = "SELECT balance FROM rewards_wallet WHERE user_id = '".$user_id."'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return 0;
    }

    public function getTestimonials()
    {
        $sql1 = "SET NAMES utf8";
        $statement1 = $this->adapter->query($sql1);
        $res1 = $statement1->execute();
        $sql = "SELECT * FROM testimonials WHERE active = 1 AND deleted = 0";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->getResource()->fetchAll();
    }

    public function getHelpBySearch($q)
    {
        $sql = "SELECT * FROM users_question_answer
                    WHERE (qa_question LIKE '%" . $q . "%' OR qa_answer LIKE '%" . $q . "%' )
                    AND qa_active = 1
                    ORDER BY qa_id ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    
}

?>