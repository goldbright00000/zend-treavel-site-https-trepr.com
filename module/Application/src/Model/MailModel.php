<?php
namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
use Interop\Container\ContainerInterface;
use Zend\Mail;
use Zend\Mail\Message;
use Zend\Mail\Transport\Smtp as SmtpTransport;
use Zend\Mail\Transport\SmtpOptions;

class MailModel extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $adapter;
    protected $siteConfigs;
    protected $generalvar;
    protected $email_log_path = 'public/img/email-logo.jpg';

    public function __construct(Adapter $adapter, $siteConfigs = false)
    {
        $this->adapter = $adapter;
        $this->generalvar = $siteConfigs['siteConfigs'];
        /*site configs*/
        $siteConfs = $this->getSiteConfigs();
        $siteConfRows = array();
        if ($siteConfs) {
            foreach ($siteConfs as $configRow) {
                $siteConfRows[$configRow['configName']] = $configRow['configValue'];
            }
        }
        $this->siteConfigs = $siteConfRows;
    }

    public function getSiteConfigs($module = 'GeneralSitewide')
    {
        $sql = "SELECT * FROM `site_configs` WHERE configModule='" . $module . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function aasort(&$array, $key, $order = 'ASC')
    {
        $sorter = array();
        $ret = array();
        if (is_array($array))
            reset($array);
        if (count($array) > 0) {
            foreach ($array as $ii => $va) {
                $sorter[$ii] = $va[$key];
            }
        }
        if ($order == 'ASC')
            asort($sorter);
        elseif ($order == 'DESC')
            arsort($sorter);
        foreach ($sorter as $ii => $va) {
            $ret[$ii] = $array[$ii];
        }
        $array = $ret;
        return $array;
    }

    function distance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);
        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }

    public function cleanQuery($string)
    {
        if (get_magic_quotes_gpc()) {
            $string = stripslashes($string);
        }
        if (phpversion() >= '4.3.0') {
        } else {
            $string = mysql_escape_string($string);
        }
        return $string;
    }

    public function trimLength($s, $maxlength)
    {
        $words = explode(' ', $s);
        $split = '';
        foreach ($words as $word) {
            if (strlen($split) + strlen($word) < $maxlength)
                $split .= $word . ' ';
            else
                break;
        }
        if (strlen($s) > $maxlength) {
            $split = trim($split) . " ...";
        }
        return $split;
    }

    public function splitImages($img)
    {
        $img = list($img_name, $img_type) = explode(".", $img);
        $xs = $img_name . "_xs." . strtolower($img_type);
        $th = $img_name . "_th." . strtolower($img_type);
        $logo = $img_name . "_logo." . strtolower($img_type);
        $reg = $img_name . "." . strtolower($img_type);
        $lrg = $img_name . "_lg." . strtolower($img_type);
        $as = $img_name . "_as." . strtolower($img_type);
        $og = $img_name . "_og." . strtolower($img_type);
        $arr_imgs = array(
            "small" => $xs,
            "thumbnail" => $th,
            "standard" => $reg,
            "large" => $lrg,
            "actual" => $as,
            "logo" => $logo,
            "original" => $og
        );
        return $arr_imgs;
    }

    public function timeZoneAdjust($format, $dat)
    {
        $dat = strtotime($dat);
        if (date('T', $dat) == 'PST') {
            $dateadjusted = date($format, $dat + 3600);
        } else {
            $dateadjusted = date($format, $dat);
        }
        return $dateadjusted;
    }

    public function cleanTags($txt)
    {
        $specialstuff = array(
            "<br />",
            "<br>",
            "<ul>",
            "<li>",
            "</li>",
            "</ul>",
            "<ol>",
            "</ol>"
        );
        $txt = str_replace($specialstuff, " ", stripslashes($txt));
        return $txt;
    }

    public function in_array_r($needle, $haystack)
    {
        foreach ($haystack as $item) {
            if ($item === $needle || (is_array($item) && $this->in_array_r($needle, $item))) {
                return true;
            }
        }
        return false;
    }

    function sendwelcomeMail($firstname, $lastname, $to, $userid)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl . '';
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(4);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }

    function getMailTemplate($temp_id)
    {
        $sql = "SELECT * FROM email_templates WHERE template_id = " . $temp_id;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    function sendMail($from, $to, $subject, $message1)
    {
        if(empty($from)){
            $from="no-reply@trepr.com";
        }
        /*$to = "admin@localhost";        $from = site_from_email();        $this->siteConfigs['site_name'];*/
        $textVersion = strip_tags($message1);
        $htmlVersion = '<html xmlns="http://www.w3.org/1999/xhtml">';
        $htmlVersion .= $message1;
        $htmlVersion .= '</html>';
        $boundary = uniqid('np');
        $boundary = uniqid('np');
        /*headers - specify your from email address and name here        and specify the boundary for the email*/
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "From: " . $from . " \r\n";
        $headers .= "Content-Type: multipart/alternative;boundary=" . $boundary . "\r\n";
        /*here is the content body*/
        $message = "This is a MIME encoded message.";
        $message .= "\r\n\r\n--" . $boundary . "\r\n";
        $message .= "Content-type: text/plain;charset=utf-8\r\n\r\n";
        /*Plain text body*/
        $message .= $textVersion;
        $message .= "\r\n\r\n--" . $boundary . "\r\n";
        $message .= "Content-type: text/html;charset=utf-8\r\n\r\n";
        /*Html body*/
        $message .= $htmlVersion;
        $message .= "\r\n\r\n--" . $boundary . "--";

        $messages = new Message();
        $messages->addTo($to)
            ->addFrom($from)
            ->setSubject($subject);
        $bodyPart = new \Zend\Mime\Message();

        $bodyMessage = new \Zend\Mime\Part($htmlVersion);
        $bodyMessage->type = 'text/html';

        $bodyPart->setParts(array($bodyMessage));

        $messages->setBody($bodyPart);
        $messages->setEncoding('UTF-8');
        $transport = new SmtpTransport();
       /*$options = new SmtpOptions(['name' => 'trepr.com', 'host' => 'smtp.zoho.com', 'port' => 465, 'connection_class' => 'plain', 'connection_config' => ['username' => 'no-reply@trepr.com', 'password' => 'Trepr#2017', 'ssl' => 'ssl'],]); */
	   /*  $options = new SmtpOptions(['name' => 'trepr.co.uk', 'host' => 'mail.trepr.co.uk', 'port' => 25, 'connection_class' => 'plain', 'connection_config' => ['username' => 'admin@trepr.co.uk', 'password' => 'Myth#17', 'ssl' => ''],]);*/
        
       $options = new SmtpOptions(['name' =>$this->generalvar['smtpname'], 'host' =>$this->generalvar['host'], 'port' => $this->generalvar['port'], 'connection_class' => $this->generalvar['connection_class'], 'connection_config' => ['username' => $this->generalvar['connection_username'], 'password' => $this->generalvar['password'], 'ssl' => 'ssl'],]); 
       $transport->setOptions($options);
        $sent = $transport->send($messages);
        //if (@mail($to, $subject, $message, $headers, '-f ' . $from)) {
        if ($sent) {
            return true;
        } else {
            return false;
        }
    }

    function sendsignupMail($firstname, $lastname, $to, $userid)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl.'mailactive?md=' . $userid;//$clienturl . 'mailactive?md=' . $userid;
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(6);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }

    function sendVerifyIdMailToAdmin($firstname, $lastname, $to, $userid)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl . 'admin';
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(7);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($to, $from, $subject, $newphrase);
        return true;
    }

    function sendVerifyIdMailToUser($firstname, $lastname, $to, $id)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl . 'myprofile/trustVerification';
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(8);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}",
            "{VERIFY_ID}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link,
            $id
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }

    function sendTripRequestMailtoAdmin($firstname, $lastname, $tripIdNumber)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $to = $this->siteConfigs['site_email'];
        $link = $clienturl . 'myprofile/trustVerification';
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(11);
        $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $searchSub = array(
            "{TRIP_ID}"
        );
        $replaceSub = array(
            '#' . $tripIdNumber
        );
        $newSub = str_replace($searchSub, $replaceSub, $subject);
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{FULL_NAME}",
            "{TRIP_ID}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            '#' . $tripIdNumber
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $newSub, $newphrase);
        return true;
    }

    function sendTicketRequestMailtoAdmin($firstname, $lastname, $newsubject, $message)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $to = $this->siteConfigs['site_email'];
        $link = $clienturl . 'myprofile/trustVerification';
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(14);
        $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $searchSub = array(
            "{SUBJECT}"
        );
        $replaceSub = array(
            $newsubject
        );
        $newSub = str_replace($searchSub, $replaceSub, $subject);
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{MESSAGE}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $message
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $newSub, $newphrase);
        return true;
    }

    public function sendsignupMail_bk($firstname, $lastname, $email, $userid)
    {
        $clienturl = 'http://trepr.co.uk';//$this->generalvar['clienturl'];
        $clientname = $this->generalvar['clientname'];
        $subject = "Register details";
        $from = 'info@biztrove.com';
        $logoPath = $clienturl . '/' . $this->email_log_path;
        $mssg = '<html>        <body>        <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">        <tr>        <td align="center"><img src="$logoPath" alt="$clientname" /></td>        </tr>        <tr>        <td valign="top" style="font: 14px/20px Cambria;">Welcome,' . $firstname . ' ' . $lastname . ' <br/><br/>        And thanks for joining our global community! Here\'s a quick guide to help you get started.<br/><br/>        Search <br/><br/>        Find the perfect person to serve you<br/><br/>        Whether you want to transport your loved ones, send package anywhere, or get work done in a remote location, we\'ll help you find the representativewho is right for you.<br/><br/>        Contact <br/><br/>        Message a few Treper<br/><br/>        When you find a person (or two or three) you like, contact them to learn more. Feel free to message as many Trepers as you like.<br/><br/>        Request<br/><br/>        Make your request<br/><br/>        Once you\'re ready, make a request for ‘P’ service. You won\'t be charged until the Treper accepts. And then you\'re worry free!<br/><br/>         That\'s it! Why not give it a try now?         <br /><br />      <b>Please <a href="' . $clienturl . '/mailactive?md=' . $userid . '">Click here</a> to Verify your Email</b><br />        <a href="' . $clienturl . '/search">Search Now</a><br /><br />        Thanks,<br />        The Trepr Team         </td>        </tr>        </table>        </body>        </html>';
        $headers = array(
            'From' => $from,
            'Subject' => $subject
        );
        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= "From: " . $from;
        mail($email, $subject, $mssg, $headers);
    }

    function sendResetPasswordMail($to, $fields, $encd)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl.'resetpasswordsubmit?id=55SADG45&md=' . $encd;
        //$clienturl . '/resetpasswordsubmit?id=55SADG45&md=' . $encd;
        $name = $fields['first_name'] . ' ' . $fields['last_name'];
        $mailTemplates = $this->getMailTemplate(3);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }

    function sendReferralMail($data)
    {
        $from = $this->siteConfigs['site_from_email'];
        $to = $data['REFERRAL_EMAIL'];
        $name = $data['FULL_NAME'];
        $mailTemplates = $this->getMailTemplate(1);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{FULL_NAME}",
            "{SIGNUP_LINK}",
            "{REFERRED_PHOTO_URL}"
        );
        $replace = array(
            $this->generalvar['mail_url'],
            $this->generalvar['mail_url'] . 'img/logo-white.png',
            $data['FULL_NAME'],
            $data['SIGNUP_LINK'],
            $data['REFERRED_PHOTO_URL']
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }

    function sendReferenceRequestMail($data)
    {
        //echo $data['REFERENCE_LINK'];
        $from = $this->siteConfigs['site_from_email'];
        $to = $data['REQUEST_EMAIL'];
        $name = $data['FIRST_NAME'];
        $mailTemplates = $this->getMailTemplate(2);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            // "{FIRST_NAME}",
            "{FULL_NAME}",
            "{REFERENCE_LINK}"
        );
        $replace = array(
            $this->generalvar['mail_url'],
            $this->generalvar['mail_url'] . 'img/logo-white.png',
            $data['FIRST_NAME'],
            $data['REFERENCE_LINK']
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }

    function sendConatctUsMail($data)
    {
        $from = $data['contactemail'];
        $to = $this->siteConfigs['site_email'];
        $mailTemplates = $this->getMailTemplate(17);
        $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{SITE_NAME}",
            "{CONTACT_NAME}",
            "{CONTACT_EMAIL}",
            "{CONTACT_PHONE}",
            "{CONTACT_QUESTION}",
            "{CONTACT_LOCATION}",
            "{CONTACT_DEADLINE}",
            "{MESSAGE}"
        );
        $replace = array(
            $this->generalvar['mail_url'],
            $this->generalvar['mail_url'] . 'img/logo-white.png',
            $this->siteConfigs['site_name'],
            $data['contactname'],
            $data['contactemail'],
            $data['contactphone'],
            $data['contactquestion'],
            $data['contactaddress'],
            $data['contactdeadline'],
            $data['contactmsg']
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }

    function sendFlightStatusMailtoUser($arrInsData)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $to = $arrInsData['email_address'];
        $link = $clienturl . 'myprofile/trustVerification';
        $mailTemplates = $this->getMailTemplate(16);
        $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $searchSub = array(
            "{STATUS}"
        );
        $replaceSub = array(
            $arrInsData['flight_status']
        );
        $newSub = str_replace($searchSub, $replaceSub, $subject);
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{ORGIN}",
            "{DESTINATION}",
            "{AIRLINE}",
            "{AIRNUMBER}",
            "{COLOR}",
            "{STATUS}",
            "{DEPT_SCH_DATE}",
            "{DEPT_ACT_DATE}",
            "{DEPT_TERMINAL}",
            "{DEPT_DELAY}",
            "{ARR_SCH_DATE}",
            "{ARR_ACT_DATE}",
            "{ARR_TERMINAL}",
            "{ARR_DELAY}",
            "{FULL_NAME}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $arrInsData['orgin_locaion'],
            $arrInsData['destination_location'],
            $arrInsData['airline_name'],
            $arrInsData['airline_number'],
            $arrInsData['flight_status_color'],
            $arrInsData['flight_status'],
            $arrInsData['departure_scheduled_date'],
            $arrInsData['departure_acutal_date'],
            $arrInsData['arrival_terminal'],
            $arrInsData['departure_delay'] . ' Minutes',
            $arrInsData['arrival_scheduled_date'],
            $arrInsData['arrival_actual_date'],
            $arrInsData['arrival_terminal'],
            $arrInsData['arrival_delay'] . ' Minutes',
            $arrInsData['first_name']
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $newSub, $newphrase);
        return true;
    }

    function sendReportListMail($firstname, $lastname, $tripIdNumber, $report)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $to = $this->siteConfigs['site_email'];
        $link = $clienturl . 'myprofile/trustVerification';
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(18);
        $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $searchSub = array(
            "{TRIP_ID}"
        );
        $replaceSub = array(
            '#' . $tripIdNumber
        );
        $newSub = str_replace($searchSub, $replaceSub, $subject);
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{FULL_NAME}",
            "{TRIP_ID}",
            "{REPORT}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            '#' . $tripIdNumber,
            $report
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $newSub, $newphrase);
        return true;
    }

    function sendMemberMail($fullname, $membername, $memberemail, $tripIdNumber, $subject, $message, $email, $role)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $to = $memberemail;
        $link = $clienturl . 'myprofile/trustVerification';
        $mailTemplates = $this->getMailTemplate(19);
        $subject = $subject;
        /*$mailTemplates['template_subject'];*/
        $phrase = $mailTemplates['template_content'];
        $searchSub = array(
            "{TRIP_ID}"
        );
        $replaceSub = array(
            '#' . $tripIdNumber
        );
        $newSub = str_replace($searchSub, $replaceSub, $subject);
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{FULL_NAME}",
            "{TRIP_ID}",
            "{MEMBER}",
            "{MESSAGE}",
            "{EMAIL}",
            "{ROLE}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $fullname,
            '#' . $tripIdNumber,
            $membername,
            $message,
            $email,
            $role
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $newSub, $newphrase);
        return true;
    }

    public function sendVerifyUserMailId($firstname, $lastname, $to, $id, $BT)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl.'myprofile/verifyemailtosignup?user=' . $id . '&bt=' . $BT;//$clienturl . 'myprofile/verifyemailtosignup?user='.$id;
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(6);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}",
            "{VERIFY_ID}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link,
            $id
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;

    }
	
	
	function sendBecomeTravellerEmail($firstname, $lastname, $to, $userid)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl . '';
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(4);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }
	
	
	
	function sendPeopleServiceEmail($firstname, $lastname, $to, $userid)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl . '';
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(4);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }
	
	
	function sendPackageServiceEmail($firstname, $lastname, $to, $userid)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl . '';
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(4);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }
	
	
	function sendProductServiceEmail($firstname, $lastname, $to, $userid)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl . '';
        $name = $firstname . ' ' . $lastname;
        $mailTemplates = $this->getMailTemplate(4);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}"
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }
	
	function sendCreateListEmail($firstname,$origin_location,$destination_location,$departure_date,$create_date,$to)
    {
        $clienturl = $this->generalvar['mail_url'];
        $clientname = $this->generalvar['clientname'];
        $from = $this->siteConfigs['site_from_email'];
        $link = $clienturl . '';
        $name = $firstname;
		$origin=$origin_location;
		$destination =$destination_location;
		$date= $departure_date;
		$listed_date=$create_date;
		
		
        $mailTemplates = $this->getMailTemplate(18);
        if (empty($subject))
            $subject = $mailTemplates['template_subject'];
        $phrase = $mailTemplates['template_content'];
        $search = array(
            "{SITE_PATH}",
            "{LOGO_PATH}",
            "{USER_NAME}",
            "{LINK}",
			"{ORIGIN}",
			"{DESTINATION}",
			"{DATE}",
			"LISTED_DATE"
			
        );
        $replace = array(
            $clienturl,
            $clienturl . 'img/logo-white.png',
            $name,
            $link,
			$origin,
		    $destination,
		    $date,
		    $listed_date
        );
        $newphrase = str_replace($search, $replace, $phrase);
        $this->sendMail($from, $to, $subject, $newphrase);
        return true;
    }
	
}

?>