<?php

class Application_Model_DbTable_Trip extends Zend_Db_Table_Abstract {



//public function getTravellers($origin_location,$destination_location,$date,$days=''){

public function getTravellers($filters){

    if($filters['airline_name']!='')

    $filter_airline_name = " b.airline_name='".$filters['airline_name']."' ";



    if($filters['flight_number']!='')

    $filter_flight_number = " b.flight_number='".$filters['flight_number']."' ";



    if($filter_airline_name!='' && $filter_flight_number!='')

    $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";

    elseif($filter_airline_name!='')

    $filter_airline_name_number = " AND $filter_airline_name ";

    elseif($filter_flight_number!='')

    $filter_airline_name_number = " AND $filter_flight_number ";



    if($filters['radius']!=''){

    $filter_radius = " HAVING distance <= ".$filters['radius'];

    $filter_radius_field = " , ( 3959 * acos( cos( radians( ".$filters['current_latitude']." ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( ".$filters['current_longitude']." ) ) + sin( radians( ".$filters['current_latitude']." ) ) * sin( radians( a.latitude ) ) ) ) AS distance ";

    }



    if($filters['days']!=''){

        $start_date = date('Y-m-d h:i:s', strtotime($filters['date'].' - '.$filters['days'].' days'));    

        $end_date = date('Y-m-d 23:59:59', strtotime($filters['date'].' + '.$filters['days'].' days'));    

        $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b,traveller_people as c  WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date>='".$start_date."' AND b.departure_date<='".$end_date."'  $filter_airline_name_number $filter_radius";

    }else{

        $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b,traveller_people as c  WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date='".$filters['date']."' $filter_airline_name_number $filter_radius";

    }



    //echo $sql;  exit;

    $rs = $this->_db->fetchAll($sql);

    return $rs;	    

}



public function getTravellersForPackage($filters){

    if($filters['airline_name']!='')

    $filter_airline_name = " b.airline_name='".$filters['airline_name']."' ";

    if($filters['flight_number']!='')

    $filter_flight_number = " b.flight_number='".$filters['flight_number']."' ";

    if($filter_airline_name!='' && $filter_flight_number!='')

    $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";

    elseif($filter_airline_name!='')

    $filter_airline_name_number = " AND $filter_airline_name ";

    elseif($filter_flight_number!='')

    $filter_airline_name_number = " AND $filter_flight_number ";



    if($filters['radius']!=''){

    $filter_radius = " HAVING distance <= ".$filters['radius'];

    $filter_radius_field = " , ( 3959 * acos( cos( radians( ".$filters['current_latitude']." ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( ".$filters['current_longitude']." ) ) + sin( radians( ".$filters['current_latitude']." ) ) * sin( radians( a.latitude ) ) ) ) AS distance ";

    }



    if($filters['days']!=''){

    $start_date = date('Y-m-d h:i:s', strtotime($filters['date'].' - '.$filters['days'].' days'));    

    $end_date = date('Y-m-d 23:59:59', strtotime($filters['date'].' + '.$filters['days'].' days'));

    $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b, traveller_packages as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.trip=b.ID AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND  b.departure_date>='".$start_date."' AND b.departure_date<='".$end_date."' $filter_airline_name_number $filter_radius";

    }else{     

    $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b, traveller_packages as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.trip=b.ID AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date='".$filters['date']."' $filter_airline_name_number $filter_radius";

    }

    $rs = $this->_db->fetchAll($sql);

    return $rs;	    

}



public function getTravellerProjects($filters){

    if($filters['airline_name']!='')

    $filter_airline_name = " b.airline_name='".$filters['airline_name']."' ";



    if($filters['flight_number']!='')

    $filter_flight_number = " b.flight_number='".$filters['flight_number']."' ";



    if($filter_airline_name!='' && $filter_flight_number!='')

    $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";

    elseif($filter_airline_name!='')

    $filter_airline_name_number = " AND $filter_airline_name ";

    elseif($filter_flight_number!='')

    $filter_airline_name_number = " AND $filter_flight_number ";



    if($filters['radius']!=''){

    $filter_radius = " HAVING distance <= ".$filters['radius'];

    $filter_radius_field = " , ( 3959 * acos( cos( radians( ".$filters['current_latitude']." ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( ".$filters['current_longitude']." ) ) + sin( radians( ".$filters['current_latitude']." ) ) * sin( radians( a.latitude ) ) ) ) AS distance ";

    }



    if($filters['days']!=''){

        $start_date = date('Y-m-d h:i:s', strtotime($filters['date'].' - '.$filters['days'].' days'));    

        $end_date = date('Y-m-d 23:59:59', strtotime($filters['date'].' + '.$filters['days'].' days'));    

        $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b,traveller_projects as c  WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date>='".$start_date."' AND b.departure_date<='".$end_date."'  $filter_airline_name_number $filter_radius";

    }else{

        $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b,traveller_projects as c  WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date='".$filters['date']."' $filter_airline_name_number $filter_radius";

    }



    //echo $sql;  exit;

    $rs = $this->_db->fetchAll($sql);

    return $rs;	    

}



public function getSeekersPeopleForSearch($filters){

    if($filters['airline_name']!='')

    $filter_airline_name = " b.airline_name='".$filters['airline_name']."' ";



    if($filters['flight_number']!='')

    $filter_flight_number = " b.flight_number='".$filters['flight_number']."' ";



    if($filter_airline_name!='' && $filter_flight_number!='')

    $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";

    elseif($filter_airline_name!='')

    $filter_airline_name_number = " AND $filter_airline_name ";

    elseif($filter_flight_number!='')

    $filter_airline_name_number = " AND $filter_flight_number ";



    if($filters['radius']!=''){

    $filter_radius = " HAVING distance <= ".$filters['radius'];

    $filter_radius_field = " , ( 3959 * acos( cos( radians( ".$filters['current_latitude']." ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( ".$filters['current_longitude']." ) ) + sin( radians( ".$filters['current_latitude']." ) ) * sin( radians( a.latitude ) ) ) ) AS distance ";

    }



    if($filters['days']!=''){

        $start_date = date('Y-m-d h:i:s', strtotime($filters['date'].' - '.$filters['days'].' days'));    

        $end_date = date('Y-m-d 23:59:59', strtotime($filters['date'].' + '.$filters['days'].' days'));    

        $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_people as c  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date>='".$start_date."' AND b.departure_date<='".$end_date."'  $filter_airline_name_number $filter_radius";

    }else{

        $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_people as c  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date='".$filters['date']."' $filter_airline_name_number $filter_radius";

    }



    //echo $sql;  exit;

    $rs = $this->_db->fetchAll($sql);

    return $rs;	    

}



public function getSeekersPackageForSearch($filters){

if($filters['airline_name']!='')

$filter_airline_name = " b.airline_name='".$filters['airline_name']."' ";

if($filters['flight_number']!='')

$filter_flight_number = " b.flight_number='".$filters['flight_number']."' ";

if($filter_airline_name!='' && $filter_flight_number!='')

$filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";

elseif($filter_airline_name!='')

$filter_airline_name_number = " AND $filter_airline_name ";

elseif($filter_flight_number!='')

$filter_airline_name_number = " AND $filter_flight_number ";



if($filters['radius']!=''){

$filter_radius = " HAVING distance <= ".$filters['radius'];

$filter_radius_field = " , ( 3959 * acos( cos( radians( ".$filters['current_latitude']." ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( ".$filters['current_longitude']." ) ) + sin( radians( ".$filters['current_latitude']." ) ) * sin( radians( a.latitude ) ) ) ) AS distance ";

}



if($filters['days']!=''){

$start_date = date('Y-m-d h:i:s', strtotime($filters['date'].' - '.$filters['days'].' days'));    

$end_date = date('Y-m-d 23:59:59', strtotime($filters['date'].' + '.$filters['days'].' days'));

$sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, seeker_trips as b, seeker_package as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.seeker_trip=b.ID AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND  b.departure_date>='".$start_date."' AND b.departure_date<='".$end_date."' $filter_airline_name_number $filter_radius";

}else{     

$sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, seeker_trips as b, seeker_package as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.seeker_trip=b.ID AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date='".$filters['date']."' $filter_airline_name_number $filter_radius";

}

$rs = $this->_db->fetchAll($sql);

return $rs;	    

}



public function getSeekersProjectForSearch($filters){

    if($filters['airline_name']!='')

    $filter_airline_name = " b.airline_name='".$filters['airline_name']."' ";



    if($filters['flight_number']!='')

    $filter_flight_number = " b.flight_number='".$filters['flight_number']."' ";



    if($filter_airline_name!='' && $filter_flight_number!='')

    $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";

    elseif($filter_airline_name!='')

    $filter_airline_name_number = " AND $filter_airline_name ";

    elseif($filter_flight_number!='')

    $filter_airline_name_number = " AND $filter_flight_number ";



    if($filters['radius']!=''){

    $filter_radius = " HAVING distance <= ".$filters['radius'];

    $filter_radius_field = " , ( 3959 * acos( cos( radians( ".$filters['current_latitude']." ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( ".$filters['current_longitude']." ) ) + sin( radians( ".$filters['current_latitude']." ) ) * sin( radians( a.latitude ) ) ) ) AS distance ";

    }



    if($filters['days']!=''){

        $start_date = date('Y-m-d h:i:s', strtotime($filters['date'].' - '.$filters['days'].' days'));    

        $end_date = date('Y-m-d 23:59:59', strtotime($filters['date'].' + '.$filters['days'].' days'));    

        $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b,seeker_projects as c  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date>='".$start_date."' AND b.departure_date<='".$end_date."'  $filter_airline_name_number $filter_radius";

    }else{

        $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b,seeker_projects as c  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date='".$filters['date']."' $filter_airline_name_number $filter_radius";

    }



    //echo $sql;  exit;

    $rs = $this->_db->fetchAll($sql);

    return $rs;	    

}





public function getTrips($userid,$userrole,$trip_status=''){





if($trip_status=='upcoming')

$status_filter = " AND b.active=1 AND b.departure_date>='".date('Y-m-d H:i:s')."' ";

elseif($trip_status=='cancelled')

$status_filter = " AND b.trip_status='cancelled' ";

elseif($trip_status=='completed')

$status_filter = " AND b.trip_status!='cancelled' AND b.departure_date<='".date('Y-m-d H:i:s')."' ";



if($userrole=='seeker')

$sql = "SELECT b.* FROM seeker_trips as b WHERE user='".$userid."' $status_filter";

else

$sql = "SELECT b.* FROM trips as b WHERE user='".$userid."' $status_filter";

//echo $sql; exit;

$rs = $this->_db->fetchAll($sql);



return $rs;	    





}



public function getAvailableAirlines(){





//echo $sql = "SELECT b.* FROM trips as b  WHERE b.departure_date>='".date('Y-m-d H:i:s')."'"; exit;





$sql = "SELECT DISTINCT(b.airline_name),b.* FROM trips as b group by b.airline_name";





$rs = $this->_db->fetchAll($sql);





return $rs;	    





}



public function getAirPort($ID){

$sql = "SELECT * FROM core_airports WHERE ID ='".$ID."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function getAirPortByCode($code){

$sql = "SELECT * FROM core_airports WHERE code ='".$code."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function getTripPeople($tripid){

$sql = "SELECT * FROM traveller_people WHERE trip='".$tripid."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function getTripPackage($tripid){





$sql = "SELECT * FROM traveller_packages WHERE trip='".$tripid."'";





$rs = $this->_db->fetchRow($sql);





return $rs;	    





}

public function addTripreview($trip_review,$ID){

        $sql = "SELECT `user` FROM trips WHERE ID='".$ID."'";

$rs = $this->_db->fetchRow($sql);

if($rs['user'])

{

$this->_db->insert('users_reviews',$trip_review);

$user_id = $this->_db->lastInsertId('users_reviews', 'ID');



$this->_db->update('users_reviews',array("recevier_id"=>$rs['user']),"ID='{$user_id}'");



}

}



public function getTripreview($ID,$userid){

        $sql = "SELECT `ID` FROM users_reviews WHERE trip_id='".$ID."' AND sender_id='".$userid."'";

$rs = $this->_db->fetchAll($sql);

return $rs;

}

public function getTripreviewall($userid){

        $sql = "SELECT b.name,b.departure_date,b.arrival_date,a.message,a.rating,c.image,c.first_name,c.last_name FROM users_reviews as a,trips as b,users_user as c WHERE a.trip_id=b.ID  AND  a.recevier_id='".$userid."' AND c.ID=a.sender_id";

$rs = $this->_db->fetchAll($sql);

return $rs;

}



public function getuservaliddetails($editadminidu,$table){

    

   

$sql = "SELECT a.field_name,b.validtype,b.notifalert,b.msg FROM core_table_fieldvalue as a,users_field_valid as b WHERE  b.field_id=a.ID AND b.user_id='".$editadminidu."' and a.table_name='".$table."'  AND a.ID IN(3,5)";

$rs = $this->_db->fetchAll($sql);

$retv=array();

foreach($rs as $val => $key)

{

    $fldn=$key['field_name'];

    $sqlv = "SELECT $fldn FROM users_user WHERE ID='".$editadminidu."'";

$rsv = $this->_db->fetchRow($sqlv);



    

    $retv[$key['field_name']]=array("0"=>$rsv[$fldn],"1"=>$key['validtype']);

}





return $retv;	    

}





public function getTripProject($tripid){

$sql = "SELECT * FROM traveller_projects WHERE trip='".$tripid."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function getTripProjectTask($ID){

$sql = "SELECT * FROM core_tasks_category WHERE ID='".$ID."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function getProjectTasksCategory(){

$sql = "SELECT * FROM core_tasks_category";

$rs = $this->_db->fetchAll($sql);

return $rs;

}



public function getProjectTasksCategorytype(){

$sql = "SELECT * FROM project_category";

$rs = $this->_db->fetchAll($sql);

return $rs;

}



public function getProjectTasksCategorylisttype($id){

	$subquery = "SELECT ID, subcatname from project_sub_category where catid = '".$id."'";

	$res = $this->_db->fetchAll($subquery);

	return $res;

}



public function getSeekerPeopleRequest($tripid){

$sql = "SELECT * FROM seeker_people_request WHERE trip='".$tripid."'";

$rs = $this->_db->fetchAll($sql);

return $rs;	    

}



public function getSeekerPackageRequest($tripid){

$sql = "SELECT * FROM seeker_package_request WHERE trip='".$tripid."'";

$rs = $this->_db->fetchAll($sql);

return $rs;

}



public function getSeekerProjectRequest($tripid){

$sql = "SELECT * FROM seeker_package_request WHERE trip='".$tripid."'";

$rs = $this->_db->fetchAll($sql);

return $rs;

}





public function checkSeekerPeopleRequestExist($tripid,$people_request){

$sql = "SELECT * FROM seeker_people_request WHERE trip='".$tripid."' AND people_request='".$people_request."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function checkSeekerPackageRequestExist($tripid,$package_request){

$sql = "SELECT * FROM seeker_package_request WHERE trip='".$tripid."' AND package_request='".$package_request."'";

$rs = $this->_db->fetchRow($sql);

return $rs;

}



public function checkSeekerProjectRequestExist($tripid,$project_request){

$sql = "SELECT * FROM seeker_project_request WHERE trip='".$tripid."' AND project_request='".$project_request."'";

$rs = $this->_db->fetchRow($sql);

return $rs;

}



public function checkTravellerPeopleRequestExist($tripid,$people_request){

$sql = "SELECT * FROM traveller_people_request WHERE trip='".$tripid."' AND people_request='".$people_request."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function checkTravellerPackageRequestExist($tripid,$package_request){

$sql = "SELECT * FROM traveller_package_request WHERE trip='".$tripid."' AND package_request='".$package_request."'";

$rs = $this->_db->fetchRow($sql);

return $rs;

}



public function checkTravellerProjectRequestExist($tripid,$project_request){

$sql = "SELECT * FROM traveller_project_request WHERE trip='".$tripid."' AND project_request='".$project_request."'";

$rs = $this->_db->fetchRow($sql);

return $rs;

}



public function checkTravellerPeopleRequestSentAlready($traveller_tripid,$seeker_trip_id){

$sql = "SELECT b.* FROM traveller_people as a,traveller_people_request as b WHERE b.trip='".$seeker_trip_id."' AND b.people_request=a.ID AND a.trip='".$traveller_tripid."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function checkTravellerPackageRequestSentAlready($traveller_tripid,$seeker_trip_id){

$sql = "SELECT b.* FROM traveller_packages as a,traveller_package_request as b WHERE b.trip='".$seeker_trip_id."' AND b.package_request=a.ID AND a.trip='".$traveller_tripid."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function checkTravellerProjectRequestSentAlready($traveller_tripid,$seeker_trip_id){

$sql = "SELECT b.* FROM traveller_project as a,traveller_project_request as b WHERE b.trip='".$seeker_trip_id."' AND b.project_request=a.ID AND a.trip='".$traveller_tripid."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function checkSeekerPeopleRequestSentAlready($seeker_trip_id,$traveller_tripid){

$sql = "SELECT b.* FROM seeker_people as a,seeker_people_request as b WHERE b.trip='".$traveller_tripid."' AND b.people_request=a.ID AND a.seeker_trip='".$seeker_trip_id."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function checkSeekerPackageRequestSentAlready($seeker_trip_id,$traveller_tripid){

$sql = "SELECT b.* FROM seeker_package as a,seeker_package_request as b WHERE b.trip='".$traveller_tripid."' AND b.package_request=a.ID AND a.seeker_trip='".$seeker_trip_id."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function checkSeekerProjectRequestSentAlready($seeker_trip_id,$traveller_tripid){

$sql = "SELECT b.* FROM seeker_project as a,seeker_project_request as b WHERE b.trip='".$traveller_tripid."' AND b.project_request=a.ID AND a.seeker_trip='".$seeker_trip_id."'";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function getTravellerPeopleRequest($tripid){

$sql = "SELECT * FROM traveller_people_request WHERE trip='".$tripid."'";

$rs = $this->_db->fetchAll($sql);

return $rs;	    

}



public function getTravellerPackageRequest($tripid){

$sql = "SELECT * FROM traveller_package_request WHERE trip='".$tripid."'";

$rs = $this->_db->fetchAll($sql);

return $rs;

}



public function getTravellerProjectRequest($tripid){

$sql = "SELECT * FROM traveller_package_request WHERE trip='".$tripid."'";

$rs = $this->_db->fetchAll($sql);

return $rs;

}



public function addTripContactAddress($arr_trip_contact){

$this->_db->insert('users_user_locations',$arr_trip_contact);

$ID = $this->_db->lastInsertId('users_user_locations', 'ID');

return $ID;            

}



public function updateTripContactAddress($arr_trip_contact,$id,$userid){

    

$this->_db->update('users_user_locations', $arr_trip_contact, "ID ='$id' AND user='$userid'");

        

}



public function updateTripContactAddresssetdef($userid){

    

$this->_db->update('users_user_locations', array("set_default"=>0), "user='$userid'");

        

}



public function addTrip($trip,$userrole){

if($userrole=='seeker'){

$this->_db->insert('seeker_trips',$trip);

$ID = $this->_db->lastInsertId('seeker_trips', 'ID');

}else{

$this->_db->insert('trips',$trip);

$ID = $this->_db->lastInsertId('trips', 'ID');    

}

return $ID;            

}



public function addTripFlight($trip_flight){

$this->_db->insert('trips_flight',$trip_flight);

}



public function addTripPeopleDetail($people_detail){

$this->_db->insert('traveller_people',$people_detail);

$ID = $this->_db->lastInsertId('traveller_people', 'ID');

return $ID;    

}



public function addTripTravellerPackage($package_detail){

$this->_db->insert('traveller_packages',$package_detail);

$ID = $this->_db->lastInsertId('traveller_packages', 'ID');

return $ID;    

}



public function addTripTravellerProject($project_detail){

$this->_db->insert('traveller_projects',$project_detail);

$ID = $this->_db->lastInsertId('traveller_projects', 'ID');

return $ID;    

}



public function getTrip($ID){

$sql = "SELECT * FROM trips WHERE ID ='".$ID."' ";

$rs = $this->_db->fetchRow($sql);

return $rs;	    

}



public function getPeopleDetailByTripID($trip){





$sql = "SELECT * FROM traveller_people WHERE trip ='".$trip."' ";





$rs = $this->_db->fetchRow($sql);





return $rs;	    





}



public function getPackageDetailByTripID($trip){





$sql = "SELECT * FROM traveller_packages WHERE trip ='".$trip."' ";





$rs = $this->_db->fetchRow($sql);





return $rs;	    





}



public function updateTrip($arr_trip_detail,$ID){





$this->_db->update('trips',$arr_trip_detail,"ID='{$ID}'");





}



public function updateTripPeopleDetail($arr_people_detail,$ID){

$this->_db->update('traveller_people',$arr_people_detail,"ID='{$ID}'");

}



public function updateTripPackageDetail($arr_package_detail,$ID){

$this->_db->update('traveller_packages',$arr_package_detail,"ID='{$ID}'");

}



public function deleteTrip($ID){

$this->_db->query("DELETE FROM trips WHERE ID='".$ID."'");

}



public function deleteTripPeopleDetail($ID){

$this->_db->query("DELETE FROM traveller_people WHERE trip='".$ID."'");    

}



public function deleteTripPackageDetail($ID){

$this->_db->query("DELETE FROM traveller_packages WHERE trip='".$ID."'");        

}



public function getTariffServices($ID){

    $sql = "SELECT * FROM trip_services WHERE ID ='".$ID."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getSeekerPeopleByID($ID){

    $sql = "SELECT * FROM seeker_people WHERE ID ='".$ID."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getSeekerPackageByID($ID){

    $sql = "SELECT * FROM seeker_package WHERE ID ='".$ID."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getSeekerProjectByID($ID){

    $sql = "SELECT * FROM seeker_project WHERE ID ='".$ID."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getTravellerPeopleByID($ID){

    $sql = "SELECT * FROM traveller_people WHERE ID ='".$ID."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getTravellerPackageByID($ID){

    $sql = "SELECT * FROM traveller_packages WHERE ID ='".$ID."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getTravellerProjectByID($ID){

    $sql = "SELECT * FROM traveller_project WHERE ID ='".$ID."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getSeekerPeople($seeker_trip_id){

    $sql = "SELECT * FROM seeker_people WHERE seeker_trip ='".$seeker_trip_id."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getSeekerPackage($seeker_trip_id){

    $sql = "SELECT * FROM seeker_package WHERE seeker_trip ='".$seeker_trip_id."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getSeekerProject($seeker_trip_id){

    $sql = "SELECT * FROM seeker_project WHERE seeker_trip ='".$seeker_trip_id."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function addSeekerTripByTravellerTrip($traveller_trip_id,$seeker_user_id,$seeker_user_location_id){

    $sql = "INSERT INTO seeker_trips (`user`,`user_location`,`name`,`description`,`origin_location`,`destination_location`,`departure_date`,`arrival_date`,`departure_time`,`arrival_time`,`flight_number`,`booking_status`,`cabin`,`ticket_image`,`active`,`origin_latitude`,`origin_longitude`,`created`,`modified`) SELECT $seeker_user_id,$seeker_user_location_id,`name`,`description`,`origin_location`,`destination_location`,`departure_date`,`arrival_date`,`departure_time`,`arrival_time`,`flight_number`,`booking_status`,`cabin`,`ticket_image`,`active`,`origin_latitude`,`origin_longitude`,date_format(UNIX_TIMESTAMP( ),'%Y-%m-%d %H:%i:s'),date_format(UNIX_TIMESTAMP( ),'%Y-%m-%d %H:%i:s') FROM trips WHERE ID='".$traveller_trip_id."'";

    $this->_db->query($sql);

    $ID = $this->_db->lastInsertId('seeker_trips', 'ID');

    return $ID;    

}



public function addTravellerTripBySeekerTrip($seeker_trip_id,$traveller_user_id,$traveller_user_location_id){

    $sql = "INSERT INTO trips (`user`,`user_location`,`name`,`description`,`origin_location`,`destination_location`,`departure_date`,`arrival_date`,`departure_time`,`arrival_time`,`flight_number`,`booking_status`,`cabin`,`ticket_image`,`active`,`origin_latitude`,`origin_longitude`,`created`,`modified`) SELECT $traveller_user_id,$traveller_user_location_id,`name`,`description`,`origin_location`,`destination_location`,`departure_date`,`arrival_date`,`departure_time`,`arrival_time`,`flight_number`,`booking_status`,`cabin`,`ticket_image`,`active`,`origin_latitude`,`origin_longitude`,date_format(UNIX_TIMESTAMP( ),'%Y-%m-%d %H:%i:s'),date_format(UNIX_TIMESTAMP( ),'%Y-%m-%d %H:%i:s') FROM seeker_trips WHERE ID='".$seeker_trip_id."'";

    $this->_db->query($sql);

    $ID = $this->_db->lastInsertId('trips', 'ID');

    return $ID;    

}



public function checkSeekerTripExistByTravellerTrip($traveller_trip_id,$userid){

    $sql = "SELECT * FROM trips WHERE ID='".$traveller_trip_id."'";

    $rs = $this->_db->fetchRow($sql);

    if($rs['ID']){

    $sql = "SELECT * FROM seeker_trips WHERE user='".$userid."' AND origin_location='".$rs['origin_location']."' AND destination_location='".$rs['destination_location']."' AND  departure_date='".$rs['departure_date']."'";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	

}

}



public function checkTravellerTripExistBySeekerTrip($seeker_trip_id,$userid){

    $sql = "SELECT * FROM seeker_trips WHERE ID='".$seeker_trip_id."'";

    $rs = $this->_db->fetchRow($sql);

    if($rs['ID']){

    $sql = "SELECT * FROM trips WHERE user='".$userid."' AND origin_location='".$rs['origin_location']."' AND destination_location='".$rs['destination_location']."' AND  departure_date='".$rs['departure_date']."'";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	

}

}



public function addSeekerPeople($arr_seeker_people_request){

    $this->_db->insert('seeker_people',$arr_seeker_people_request);

    $ID = $this->_db->lastInsertId('seeker_people', 'ID');

    return $ID;            

}



public function addSeekerPackage($arr_seeker_package_request){

    $this->_db->insert('seeker_package',$arr_seeker_package_request);

    $ID = $this->_db->lastInsertId('seeker_package', 'ID');

    return $ID;            

}



public function addSeekerProject($arr_seeker_project_request){

    $this->_db->insert('seeker_project',$arr_seeker_project_request);

    $ID = $this->_db->lastInsertId('seeker_project', 'ID');

    return $ID;            

}



public function addSeekerPeoplePassengers($arr_seeker_people_passengers){

    $this->_db->insert('seeker_people_passengers',$arr_seeker_people_passengers);

}



public function addSeekerPackagePackages($arr_seeker_package_packages){

    $this->_db->insert('seeker_package_packages',$arr_seeker_package_packages);

}



public function addSeekerProjectTasks($arr_seeker_project_tasks){

    $this->_db->insert('seeker_project_tasks',$arr_seeker_project_tasks);

}



public function getUserPrimaryLocation($user_id){

    $sql = "SELECT * FROM users_user_locations WHERE address_type='1' AND user ='".$user_id."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function addTripSeekerPeopleRequest($arr_trip_seeker_people_request){

    $this->_db->insert('seeker_people_request',$arr_trip_seeker_people_request);

}



public function addTripSeekerPackageRequest($arr_trip_seeker_package_request){

    $this->_db->insert('seeker_package_request',$arr_trip_seeker_package_request);

}



public function addTripSeekerProjectRequest($arr_trip_seeker_project_request){

    $this->_db->insert('seeker_project_request',$arr_trip_seeker_project_request);

}



public function addTripTravellerPeopleRequest($arr_trip_traveller_people_request){

    $this->_db->insert('traveller_people_request',$arr_trip_traveller_people_request);

}



public function addTripTravellerPackageRequest($arr_trip_traveller_package_request){

    $this->_db->insert('traveller_package_request',$arr_trip_traveller_package_request);

}



public function addTripTravellerProjectRequest($arr_trip_traveller_project_request){

    $this->_db->insert('traveller_project_request',$arr_trip_traveller_project_request);

}



public function getTripPeopleRequests($tripid){

    $sql = "SELECT a.people_request as requestid,a.approved,d.* FROM seeker_people_request as a, seeker_people as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.people_request AND a.trip ='".$tripid."' AND a.approved='0' ";

    $rs = $this->_db->fetchAll($sql);

    return $rs;	        

}



public function getTripPackageRequests($tripid){

    $sql = "SELECT a.package_request as requestid,a.approved,d.* FROM seeker_package_request as a, seeker_package as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.package_request AND a.trip ='".$tripid."' AND a.approved='0' ";

    $rs = $this->_db->fetchAll($sql);

    return $rs;	        

}



public function getTripProjectRequests($tripid){

    $sql = "SELECT a.project_request as requestid,a.approved,d.* FROM seeker_project_request as a, seeker_project as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.project_request AND a.trip ='".$tripid."' AND a.approved='0' ";

    $rs = $this->_db->fetchAll($sql);

    return $rs;	        

}



public function approvePeopleRequest($tripid,$requestid,$status){

    $this->_db->update('seeker_people_request',array('approved'=> $status),"trip='{$tripid}' AND people_request='{$requestid}'");    

}



public function approvePackageRequest($tripid,$requestid,$status){

    $this->_db->update('seeker_package_request',array('approved'=> $status),"trip='{$tripid}' AND package_request='{$requestid}'");    

}



public function approveProjectRequest($tripid,$requestid,$status){

    $this->_db->update('seeker_project_request',array('approved'=> $status),"trip='{$tripid}' AND project_request='{$requestid}'");

}



public function getApprovedTripPeopleRequestUser(){

    $sql = "SELECT a.ID as requestid,d.* FROM seeker_people_request as a, seeker_people as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.people_request AND a.approved='1' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	           

}



public function getApprovedTripPackageRequestUser(){

    $sql = "SELECT a.ID as requestid,d.* FROM seeker_package_request as a, seeker_package as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.package_request AND a.approved='1' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	           

}



public function getTripPackageRequestUser($requestid){

    $sql = "SELECT a.ID as requestid,d.* FROM seeker_package_request as a, seeker_package as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.package_request AND a.package_request='".$requestid."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	           

}



public function getApprovedTripProjectRequestUser(){

    $sql = "SELECT a.ID as requestid,d.* FROM seeker_project_request as a, seeker_project as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.project_request AND a.approved='1' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	           

}



public function getTripProjectRequestUser($requestid){

    $sql = "SELECT a.ID as requestid,d.* FROM seeker_project_request as a, seeker_project as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.project_request AND a.project_request='".$requestid."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	           

}



public function getSeekerTrip($ID){

    $sql = "SELECT * FROM seeker_trips WHERE ID ='".$ID."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	    

}



public function getSeekerTripPeople($tripid){

    $sql = "SELECT * FROM seeker_people WHERE seeker_trip='".$tripid."'";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	    

}



public function getSeekerTripPeoplePassengers($people_request_id){

    $sql = "SELECT * FROM seeker_people_passengers WHERE people_request='".$people_request_id."'";

    $rs = $this->_db->fetchAll($sql);

    return $rs;	    

}



public function getSeekerTripPackage($tripid){

    $sql = "SELECT * FROM seeker_package WHERE seeker_trip='".$tripid."'";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	    

}



public function getSeekerTripPackagePackages($package_request_id){

    $sql = "SELECT * FROM seeker_package_packages WHERE package_request='".$package_request_id."'";

    $rs = $this->_db->fetchAll($sql);

    return $rs;	    

}



public function getSeekerTripProject($tripid){

    $sql = "SELECT * FROM seeker_project WHERE seeker_trip='".$tripid."'";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	    

}



public function getSeekerTripProjectTasks($project_request_id){

    $sql = "SELECT * FROM seeker_project_tasks WHERE project_request='".$project_request_id."'";

    $rs = $this->_db->fetchAll($sql);

    return $rs;

}



public function getTravellerPeople($traveller_trip_id){

    $sql = "SELECT * FROM traveller_people WHERE trip ='".$traveller_trip_id."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getTravellerPackage($traveller_trip_id){

    $sql = "SELECT * FROM traveller_packages WHERE trip ='".$traveller_trip_id."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getTravellerProject($traveller_trip_id){

    $sql = "SELECT * FROM traveller_projects WHERE trip ='".$traveller_trip_id."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	        

}



public function getTravellerPeopleRequests($traveller_trip_id){

    $sql = "SELECT a.ID as requestid,a.approved,d.* FROM traveller_people_request as a, traveller_people as b,trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip AND b.ID=a.people_request AND a.trip ='".$traveller_trip_id."' AND a.approved='0' ";

    $rs = $this->_db->fetchAll($sql);

    return $rs;	        

}



public function getTravellerPackageRequests($traveller_trip_id){

    $sql = "SELECT a.ID as requestid,a.approved,d.* FROM traveller_package_request as a, traveller_packages as b,trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip AND b.ID=a.package_request AND a.trip ='".$traveller_trip_id."' AND a.approved='0' ";

    $rs = $this->_db->fetchAll($sql);

    return $rs;	        

}



public function getTravellerProjectRequests($traveller_trip_id){

    $sql = "SELECT a.ID as requestid,a.approved,d.* FROM traveller_project_request as a, traveller_projects as b,trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip AND b.ID=a.project_request AND a.trip ='".$traveller_trip_id."' AND a.approved='0' ";

    $rs = $this->_db->fetchAll($sql);

    return $rs;	        

}



public function getSeekerTripUser($tripid){

    $sql = "SELECT b.* FROM seeker_trips as a,users_user as b WHERE  b.ID=a.user AND a.ID='".$tripid."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	           

}



public function getTravellerPeopleRequestUser($requestid){

    $sql = "SELECT d.* FROM traveller_people_request as a,traveller_people as b, trips as c ,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip  AND b.ID=a.people_request AND a.ID='".$requestid."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	               

}



public function getTravellerPackageRequestUser($requestid){

    $sql = "SELECT d.* FROM traveller_project_request as a,traveller_projects as b, trips as c ,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip  AND b.ID=a.package_request AND a.ID='".$requestid."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	               

}



public function getTravellerProjectRequestUser($requestid){

    $sql = "SELECT d.* FROM traveller_projects_request as a,traveller_projects as b, trips as c ,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip  AND b.ID=a.project_request AND a.ID='".$requestid."' ";

    $rs = $this->_db->fetchRow($sql);

    return $rs;	               

}



public function getSeekerTravelPlans(){

    

}



public function travellregdetcnt($userid)

{

     $sql = "SELECT COUNT(*) AS count FROM trips WHERE user='" . $userid . "' AND active='1' AND departure_date>='".date('Y-m-d H:i:s')."'";

     $result = $this->_db->fetchOne($sql);

     return $result;

}



public function travellregdet($userid,$offset, $rec_limit)

{

     $sql = "SELECT * FROM trips WHERE user='" . $userid . "' AND active='1' AND departure_date>='".date('Y-m-d H:i:s')."' LIMIT $offset, $rec_limit";

     $result = $this->_db->fetchAll($sql);

     return $result;

}



public function travellerservicetrip($tripid)

{

    $retvm='';

    $sql = "SELECT ID FROM traveller_people_request WHERE trip='" . $tripid . "' ";

    $result = $this->_db->fetchRow($sql);

    if($result['ID']>0)

    {

       $retvm="People"; 

    }

    $sqld = "SELECT ID FROM traveller_package_request WHERE trip='" . $tripid . "' ";

    $resultd = $this->_db->fetchRow($sqld);

    if($resultd['ID']>0)

    {

        if($retvm=='')

        {

            $retvm="Package";  

        }else{

             $retvm=$retvm.",Package"; 

        }

    }



    $sqldv = "SELECT ID FROM traveller_project_request WHERE trip='" . $tripid . "' ";

    $resultdv = $this->_db->fetchRow($sqldv);

    if($resultdv['ID']>0)

    {

        if($retvm=='')

        {

            $retvm="Product";

        }else{

          $retvm=$retvm.",Product";

        }

    }

return $retvm;

}



public function addAirports($airport)

{

$this->_db->insert('core_airports',$airport);

}





public function getFlightSearchResults($search_id){

    $ch = curl_init('http://api.travelpayouts.com/v1/flight_search_results?uuid='.$search_id);

    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");                                                                     

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      

    curl_setopt($ch, CURLOPT_HTTPHEADER, array(

        'Content-Type: application/json')

    );



    $result = curl_exec($ch);

    $result = json_decode($result);    

    if($result[1]->search_id)

        return $result;

    else

        $this->getFlightSearchResults($search_id);

}





}