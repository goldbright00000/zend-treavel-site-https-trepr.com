<?php
class Application_Model_DbTable_Profile extends Zend_Db_Table_Abstract {

	//public function getTravellers($origin_location,$destination_location,$date,$days=''){
	public function getTravellers($filters){
		if($filters['airline_name']!='')
			$filter_airline_name = " b.airline_name='".$filters['airline_name']."' ";
		if($filters['flight_number']!='')
			$filter_flight_number = " b.flight_number='".$filters['flight_number']."' ";
		if($filter_airline_name!='' && $filter_flight_number!='')
			$filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";
		elseif($filter_airline_name!='')
			$filter_airline_name_number = " AND $filter_airline_name ";
		elseif($filter_flight_number!='')
			$filter_airline_name_number = " AND $filter_flight_number ";
		if($filters['radius']!=''){
			$filter_radius = " HAVING distance <= ".$filters['radius'];
			$filter_radius_field = " , ( 3959 * acos( cos( radians( ".$filters['current_latitude']." ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( ".$filters['current_longitude']." ) ) + sin( radians( ".$filters['current_latitude']." ) ) * sin( radians( a.latitude ) ) ) ) AS distance ";
		}
		if($filters['days']!=''){
			$start_date = date('Y-m-d h:i:s', strtotime($filters['date'].' - '.$filters['days'].' days'));    
			$end_date = date('Y-m-d 23:59:59', strtotime($filters['date'].' + '.$filters['days'].' days'));    
			$sql = "SELECT DISTINCT(a.ID),a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b  WHERE aa.ID=a.user AND a.user=b.user AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date>='".$start_date."' AND b.departure_date<='".$end_date."'  $filter_airline_name_number $filter_radius";
		} else {
			$sql = "SELECT DISTINCT(a.ID),a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b  WHERE aa.ID=a.user AND a.user=b.user AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date='".$filters['date']."' $filter_airline_name_number $filter_radius";
		}
		//echo $sql;  exit;
		$rs = $this->_db->fetchAll($sql);
		return $rs;	    
	}
	
	public function getTravellersForPackage($filters){
		if($filters['airline_name']!='')
			$filter_airline_name = " b.airline_name='".$filters['airline_name']."' ";
		if($filters['flight_number']!='')
			$filter_flight_number = " b.flight_number='".$filters['flight_number']."' ";
		if($filter_airline_name!='' && $filter_flight_number!='')
			$filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";
		elseif($filter_airline_name!='')
			$filter_airline_name_number = " AND $filter_airline_name ";
		elseif($filter_flight_number!='')
			$filter_airline_name_number = " AND $filter_flight_number ";
		if($filters['radius']!=''){
			$filter_radius = " HAVING distance <= ".$filters['radius'];
			$filter_radius_field = " , ( 3959 * acos( cos( radians( ".$filters['current_latitude']." ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( ".$filters['current_longitude']." ) ) + sin( radians( ".$filters['current_latitude']." ) ) * sin( radians( a.latitude ) ) ) ) AS distance ";
		}
		if($filters['days']!=''){
			$start_date = date('Y-m-d h:i:s', strtotime($filters['date'].' - '.$filters['days'].' days'));
			$end_date = date('Y-m-d 23:59:59', strtotime($filters['date'].' + '.$filters['days'].' days'));
			$sql = "SELECT DISTINCT(a.ID),a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b, trips_traveller_packages as c  WHERE aa.ID=a.user AND a.user=b.user AND c.trip=b.ID AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND  b.departure_date>='".$start_date."' AND b.departure_date<='".$end_date."' $filter_airline_name_number $filter_radius";
		} else {
			$sql = "SELECT DISTINCT(a.ID),a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b, trips_traveller_packages as c  WHERE aa.ID=a.user AND a.user=b.user AND c.trip=b.ID AND b.active=1 AND b.origin_location='".$filters['origin']."' AND b.destination_location='".$filters['destination']."' AND b.departure_date='".$filters['date']."' $filter_airline_name_number $filter_radius";
		}
		$rs = $this->_db->fetchAll($sql);
		return $rs;	    
	}

	public function getprofile($ID){
	$sql = "SELECT * FROM users_user WHERE ID='".$ID."'";
	$rs = $this->_db->fetchRow($sql);
	return $rs;
	}
	
	public function getprofileaddr($ID){
		$sql = "SELECT * FROM `users_user_locations` WHERE  address_type='home' and user='".$ID."' order by ID desc";
		$rs = $this->_db->fetchRow($sql);
		return $rs;
	}
	
	public function getprofileaddrmail($ID){
		$sql = "SELECT * FROM `users_user_locations` WHERE address_type='mail' and user='".$ID."'  order by ID desc";
		$rs = $this->_db->fetchRow($sql);
		return $rs;
	}
	
	public function getabtus($ID){
		$sql = "SELECT * FROM `user_identity` WHERE  user_id='".$ID."'";
		$rs = $this->_db->fetchRow($sql);
		return $rs;	    
	}
	
	public function getreviews($ID){
		$sql = "SELECT * FROM `users_reviews` WHERE  recevier_id='".$ID."'";
		$rs = $this->_db->fetchAll($sql);
		return $rs;
	}
	
	public function getprofilenotification($ID){
		$sql = "SELECT b.msg FROM `core_table_fieldvalue` as a,users_field_valid as b WHERE b.field_id=a.ID and b.user_id='".$ID."'  and b.notifalert='3'";
		$rs = $this->_db->fetchAll($sql);
		return $rs;
	}
	
	public function getlang($ID){
		$sql2 = "SELECT * FROM users_user WHERE ID='".$ID."'";
		$rs2 = $this->_db->fetchRow($sql2);
		$sql = "SELECT * FROM language ";
		$rs = $this->_db->fetchAll($sql);
		$retv='';
		foreach($rs as $val => $key){
			$lanid=$val;
			$selv='';
		if($rs2['ID']==$key['ID']){$selv="selected='selected'";}
		$retv.='<option value="'.$key['ID'].'" '.$selv.'>'.$key['lang'].'</option>';
		}
		return $retv;	    
	}
	
	public function updateprofile($arr_people_detail,$user_id){
		$this->_db->update('users_user',$arr_people_detail,"ID='{$user_id}'");
	}
	
	public function updateprofileabus($arr_peopleabs_detail,$user_id){
		$this->_db->update('users_user',$arr_peopleabs_detail,"ID='{$user_id}'");
	}

	public function updateprofileaddhome($arr_address_detail,$user_id){
		$sql = "SELECT * FROM users_user_locations WHERE address_type='home' and ID='".$user_id."'";
		$rs = $this->_db->fetchRow($sql);
		if($rs['user_id']){
			$this->_db->update('users_user_locations',$arr_address_detail,"ID='{$user_id}' and address_type='home' ");
		} else {
			$this->_db->insert('users_user_locations',$arr_address_detail);
		}
	}

	public function updateprofileverfy($arr_peoplevery_detail,$user_id){
		$sql = "SELECT * FROM user_identity WHERE  user_id='".$user_id."'";
		$rs = $this->_db->fetchRow($sql);
		if($rs['user_id']){
			$this->_db->update('user_identity',$arr_peoplevery_detail,"user_id='{$user_id}'");
		} else {
			$this->_db->insert('user_identity',$arr_peoplevery_detail);
		}
	}

	public function updateprofileaddmail($arr_addressmail_detail,$user_id){
		$sql = "SELECT * FROM users_user_locations WHERE  address_type='mail' and ID='".$user_id."'";
		$rs = $this->_db->fetchRow($sql);
		if($rs['user_id']){
			$this->_db->update('users_user_locations',$arr_addressmail_detail," ID='{$user_id}' and address_type='mail' ");
		} else {
			$this->_db->insert('users_user_locations',$arr_addressmail_detail);
		}
	}
}


