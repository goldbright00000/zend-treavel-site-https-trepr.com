<?php 
namespace Application\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\DriverInterface;
use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\ResultSet\ResultSet;
use Interop\Container\ContainerInterface;
use Zend\Session\Container;

class TripModel extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $table = 'trips';
    protected $adapter;
    protected $sql;
	protected $CommonMethodsModel;
	protected $sessionObj;
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->sql     = new Sql($this->adapter);
		$this->CommonMethodsModel = new CommonMethodsModel($this->adapter);
		$this->sessionObj = new Container('comSessObj');
    }
    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
	public function addAddressNew($data){
		$sql="select * from users_user_locations where user=$data[user] AND latitude='$data[latitude]' AND longitude='$data[longitude]'";
		$statement = $this->adapter->query($sql);
        $res       = $statement->execute();
		$response=$res->getResource()->fetchAll();
		
		if($res->count()>0){ 
			// print_r($response);addTripContactAddress
			$value=$response[0]['street_address_1'].", ".$response[0]['street_address_2'].", ".$response[0]['city']." ,".$response[0]['state']." ,".$response[0]['country']." ,".$response[0]['zip_code'];
			$return=Array('id'=>$response[0]['ID'],'value'=>$value);
			
		}
		else{
			
			$insert="INSERT INTO `users_user_locations`(`user`, `name`, `street_address_1`, `street_address_2`, `city`, `state`, `zip_code`, `country`, `latitude`, `longitude`) VALUES ($_POST[user],'','$_POST[street_address_1]','$_POST[street_address_2]','$_POST[city]','$_POST[state]','$_POST[zip_code]','$_POST[country]','$_POST[latitude]','$_POST[longitude]')";
			$statement = $this->adapter->query($insert);
			$res= $statement->execute();
			$sql="select * from users_user_locations where user=$data[user] AND latitude='$data[latitude]' AND longitude='$data[longitude]'";
			$statement = $this->adapter->query($sql);
			$res       = $statement->execute();
			$response=$res->getResource()->fetchAll();
			$value=$response[0]['street_address_1'].", ".$response[0]['street_address_2'].", ".$response[0]['city']." ,".$response[0]['state']." ,".$response[0]['country']." ,".$response[0]['zip_code'];
			$return=Array('id'=>$response[0]['ID'],'value'=>$value);
		}
		// print_r($return);
		return $return;
	}
    public function getTravellers($filters)
    {
		//print_r($filters);die;
		$filter_airline_name = $filter_airline_name = $filter_airline_name_number = $filter_airline_name_number = '';
        if (isset($filters['airline_name']) && $filters['airline_name'] != '')
            $filter_airline_name = " b.airline_name='" . $filters['airline_name'] . "' ";
        if (isset($filters['flight_number']) && $filters['flight_number'] != '')
            $filter_flight_number = " b.flight_number='" . $filters['flight_number'] . "' ";
        if (isset($filter_airline_name) && $filter_airline_name != '' && $filter_flight_number != '')
            $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";
        elseif (isset($filter_airline_name) && $filter_airline_name != '')
            $filter_airline_name_number = " AND $filter_airline_name ";
        elseif (isset($filter_flight_number) && $filter_flight_number != '')
            $filter_airline_name_number = " AND $filter_flight_number ";
        $filter_radius_field = $filter_radius = '';
        if (isset($filters['radius']) && $filters['radius'] != '') {
            //$filter_radius = "HAVING tdistance <= ".$filters['radius'];
            $filter_radius_field = " , ( 3959 * acos( cos( radians( " . $filters['current_latitude'] . " ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( " . $filters['current_longitude'] . " ) ) + sin( radians( " . $filters['current_latitude'] . " ) ) * sin( radians( a.latitude ) ) ) ) AS tdistance ";
        }
        $user = '';
        if (isset($filters['user']) && !empty($filters['user']))
            $user = ' AND b.user != ' . $filters['user'];
        $locationQry = '  AND b.origin_location=' . $filters['origin_id'] . ' AND b.destination_location=' . $filters['destination_id'];
        if (isset($filters['locationType']) && $filters['locationType'] == 1) {
            $locationQry = '  AND b.origin_location=' . $filters['origin_id'];
        } else if (isset($filters['locationType']) && $filters['locationType'] == 2) {
            $locationQry = '  AND b.destination_location=' . $filters['destination_id'];
        }
        if (isset($filters['noofstops']) && $filters['noofstops'] != '' ) {
            
			//$locationQry = '  AND b.noofstops="' . $filters['noofstops']'"';
			 $locationQry = '  AND b.noofstops="' . $filters['noofstops'].'"';
		
        }
        if (isset($_POST['trip_id']) && !empty($_POST['trip_id'])) {
            $locationQry .= ' AND b.ID  != ' . (int) $_POST['trip_id'];
        }
        $additionalTable = '';
        if (isset($filters['routeSearch']) && !empty($filters['routeSearch'])) {
            $additionalTable = ',trips_flight as tf';
            if (isset($filters['origin_route_code']) && !empty($filters['origin_route_code']) && $filters['origin_route_code']!='all') {
                $locationQry .= '  AND b.trip_route = "' . $filters['origin_route_code'] . '"';
            }
        }else{
            if (isset($filters['origin_iata_code']) && $filters['origin_iata_code'] !="") {
           // $locationQry .= '  AND b.trip_route = "'.$filters['origin_iata_code'] .'-'.$filters['destination_iata_code'] .'"';
        }
        }
        if (isset($filters['sortBy']) && !empty($filters['sortBy'])) {
            if ($filters['sortBy'] == 'Rating') {
                $filter_radius .= ' ORDER BY ratingsCount DESC';
            } else if ($filters['sortBy'] == 'Reviews') {
                $filter_radius .= ' ORDER BY ratings DESC';
            } else if ($filters['sortBy'] == 'Distance') { 
                $filter_radius .= ' ORDER BY tdistance ASC';
            } else if ($filters['sortBy'] == 'DistanceDesc') {
                $filter_radius .= ' ORDER BY tdistance DESC';
            }
        }
        //$ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratingsCount ';
		$ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratingsCount ';
       /*if ($filters['days'] != '') {
            $start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'] . ' days'));
            $end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'] . ' days'));
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.persons_travelling ,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b,traveller_people as c $additionalTable  WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry  AND b.departure_date>='" . $start_date . "' AND b.departure_date <='" . $end_date . "' $user   $filter_airline_name_number $filter_radius";
        } else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.persons_travelling ,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b,traveller_people as c $additionalTable WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry  AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "'   $user  $filter_airline_name_number $filter_radius";
        }*/
        if($filters['days'][1]=='d')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'days', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'days', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.persons_travelling ,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b,traveller_people as c $additionalTable  WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry  AND b.departure_date>='" . $start_date . "' AND b.departure_date <='" . $end_date . "' $user   $filter_airline_name_number $filter_radius";
        }
        else if($filters['days'][1]=='w')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.persons_travelling ,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b,traveller_people as c $additionalTable  WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry  AND b.departure_date>='" . $start_date . "' AND b.departure_date <='" . $end_date . "' $user   $filter_airline_name_number $filter_radius";
        }
        else if($filters['days'][1]=='m')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'months', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'months', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.persons_travelling ,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b,traveller_people as c $additionalTable  WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry  AND b.departure_date>='" . $start_date . "' AND b.departure_date <='" . $end_date . "' $user   $filter_airline_name_number $filter_radius";
        }
        else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.persons_travelling ,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b,traveller_people as c $additionalTable WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry  AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "'   $user  $filter_airline_name_number $filter_radius";
        }
       //echo $sql;
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
		//print_r($res->getResource()->fetchAll());
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getTravellersForPackage($filters)
    {
		//print_r($filters);
        $filter_airline_name = $filter_flight_number = $filter_airline_name_number = '';
        if (isset($filters['airline_name']) && $filters['airline_name'] != '')
            $filter_airline_name = " b.airline_name='" . $filters['airline_name'] . "' ";
        if (isset($filters['flight_number']) && $filters['flight_number'] != '')
            $filter_flight_number = " b.flight_number='" . $filters['flight_number'] . "' ";
        if ($filter_airline_name != '' && $filter_flight_number != '')
            $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";
        elseif ($filter_airline_name != '')
            $filter_airline_name_number = " AND $filter_airline_name ";
        elseif ($filter_flight_number != '')
            $filter_airline_name_number = " AND $filter_flight_number ";
        $filter_radius = $filter_radius_field = '';
        if (isset($filters['radius']) && $filters['radius'] != '') {
            /*//  $filter_radius = " HAVING tdistance <= ".$filters['radius'];*/
            $filter_radius_field = " , ( 3959 * acos( cos( radians( " . $filters['current_latitude'] . " ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( " . $filters['current_longitude'] . " ) ) + sin( radians( " . $filters['current_latitude'] . " ) ) * sin( radians( a.latitude ) ) ) ) AS tdistance ";
        }
        $user = '';
        if (isset($filters['user']) && !empty($filters['user']))
            $user = ' AND b.user != ' . $filters['user'];
        $locationQry = '  AND b.origin_location=' . $filters['origin_id'] . ' AND b.destination_location=' . $filters['destination_id'];
        if (isset($filters['locationType']) && $filters['locationType'] == 1) {
            $locationQry = '  AND b.origin_location=' . $filters['origin_id'];
        } else if (isset($filters['locationType']) && $filters['locationType'] == 2) {
            //print_r("ds");exit();
            $locationQry = 'AND b.destination_location = "'.$filters['destination_id'].'"';
        }
        if (isset($_POST['trip_id']) && !empty($_POST['trip_id'])) {
            $locationQry .= ' AND b.ID  != ' . (int) $_POST['trip_id'];
        }
        if (isset($filters['sortBy']) && !empty($filters['sortBy'])) {
            if ($filters['sortBy'] == 'Rating') {
                $filter_radius .= ' ORDER BY ratingsCount DESC';
            } else if ($filters['sortBy'] == 'Reviews') {
                $filter_radius .= ' ORDER BY ratings DESC';
            } else if ($filters['sortBy'] == 'Distance') {
                $filter_radius .= ' ORDER BY tdistance ASC';
            } else if ($filters['sortBy'] == 'DistanceDesc') {
                $filter_radius .= ' ORDER BY tdistance DESC';
            }
        }
        if (isset($filters['package_weight']) && !empty($filters['package_weight'])) {
            if ($filters['package_weight'] == 1)
                $locationQry .= '  AND c.weight_carried >= 1 AND c.weight_carried <=5 ';
            if ($filters['package_weight'] == 5)
                $locationQry .= '  AND c.weight_carried >= 5 AND c.weight_carried <= 10 ';
            if ($filters['package_weight'] == 10)
                $locationQry .= '  AND c.weight_carried >= 10 AND c.weight_carried <= 15 ';
            if ($filters['package_weight'] == 15)
                $locationQry .= '  AND c.weight_carried >= 15 AND c.weight_carried <= 20 ';
            if ($filters['package_weight'] == 20)
                $locationQry .= '  AND c.weight_carried >= 20 AND c.weight_carried <= 30 ';
            if ($filters['package_weight'] == 30)
                $locationQry .= '  AND c.weight_carried >= 30 AND c.weight_carried <= 40 ';
            if ($filters['package_weight'] == 40)
                $locationQry .= '  AND c.weight_carried >= 40 AND c.weight_carried <= 50 ';
            if ($filters['package_weight'] == 50)
                $locationQry .= '  AND c.weight_carried >= 50';
        }
        if (isset($filters['package_worth']) && !empty($filters['package_worth'])) {
            if ($filters['package_worth'] == 99)
                $locationQry .= '  AND c.item_worth < 100';
            if ($filters['package_worth'] == 100)
                $locationQry .= '  AND c.item_worth >= 100 AND c.item_worth <=250 ';
            if ($filters['package_worth'] == 251)
                $locationQry .= '  AND c.item_worth >= 251 AND c.item_worth <=500 ';
            if ($filters['package_worth'] == 501)
                $locationQry .= '  AND c.item_worth >= 501 AND c.item_worth <=1000 ';
            if ($filters['package_worth'] == 1001)
                $locationQry .= '  AND c.item_worth >= 1001 AND c.item_worth <=2000 ';
            if ($filters['package_worth'] == 2001)
                $locationQry .= '  AND c.item_worth >= 2001 AND c.item_worth <=3500 ';
            if ($filters['package_worth'] == 3501)
                $locationQry .= '  AND c.item_worth >= 3501 AND c.item_worth <=5000 ';
            if ($filters['package_worth'] == 5000)
                $locationQry .= '  AND c.item_worth > 5000';
        }
       /* if (isset($filters['noofstops']) && $filters['noofstops'] !="") {
            $locationQry .= '  AND b.noofstops = "' . $filters['noofstops'] . '"';
        }*/
        /*if (isset($filters['origin_route_code']) && $filters['origin_route_code'] !="all") {
            $locationQry .= '  AND b.trip_route = "'.$filters['origin_route_code'] .'"';
        }*/
        if (isset($filters['origin_iata_code']) && $filters['origin_iata_code'] !="") {
            //$locationQry .= '  AND b.trip_route = "'.$filters['origin_iata_code'] .'-'.$filters['destination_iata_code'] .'"';
        }
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratingsCount ';
        /*if (isset($filters['days']) && $filters['days'] != '') {
            $start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'] . ' days'));
            $end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'] . ' days'));
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.item_worth,c.weight_carried,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b, traveller_packages as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.trip=b.ID AND b.active=1 $locationQry AND  b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user $filter_airline_name_number $filter_radius";
        } else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.item_worth,c.weight_carried,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b, traveller_packages as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.trip=b.ID AND b.active=1 $locationQry  AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "' $user $filter_airline_name_number $filter_radius";
        }*/
        if($filters['days'][1]=='d')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'days', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'days', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
           $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.item_worth,c.weight_carried,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b, traveller_packages as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.trip=b.ID AND b.active=1 $locationQry AND  b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user $filter_airline_name_number $filter_radius";
        }
        else if($filters['days'][1]=='w')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.item_worth,c.weight_carried,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b, traveller_packages as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.trip=b.ID AND b.active=1 $locationQry AND  b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user $filter_airline_name_number $filter_radius";
        }
        else if($filters['days'][1]=='m')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'months', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'months', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
           $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.item_worth,c.weight_carried,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b, traveller_packages as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.trip=b.ID AND b.active=1 $locationQry AND  b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user $filter_airline_name_number $filter_radius";
        }
        else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,b.origin_location,b.trip_route,b.destination_location,c.item_worth,c.weight_carried,a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, trips as b, traveller_packages as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.trip=b.ID AND b.active=1 $locationQry  AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "' $user $filter_airline_name_number $filter_radius";
        }
		// echo $sql ; 
		
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getSeekerPeoplePassangers($people_request)
    {
        $sql       = "SELECT * FROM seeker_people_passengers WHERE people_request='" . $people_request . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getSeekerPackagesPakages($people_request)
    {
        $sql       = "SELECT *,P.ID AS catId, PC.ID subCatId FROM seeker_package_packages                             AS SPP                    LEFT JOIN package_category AS P ON (P.ID = SPP.item_category)                    LEFT JOIN package_sub_category AS PC ON (PC.ID = SPP.item_sub_category)                            WHERE package_request='" . $people_request . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getTravellerProjects($filters)
    {

        $filter_airline_name = $filter_flight_number = $filter_airline_name_number = '';
        if (isset($filters['airline_name']) && $filters['airline_name'] != '')
            $filter_airline_name = " b.airline_name='" . $filters['airline_name'] . "' ";
        if (isset($filters['flight_number']) && $filters['flight_number'] != '')
            $filter_flight_number = " b.flight_number='" . $filters['flight_number'] . "' ";
        if ($filter_airline_name != '' && $filter_flight_number != '')
            $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";
        elseif ($filter_airline_name != '')
            $filter_airline_name_number = " AND $filter_airline_name ";
        elseif ($filter_flight_number != '')
            $filter_airline_name_number = " AND $filter_flight_number ";
        $filter_radius = $filter_radius_field = '';
        if (isset($filters['radius']) && $filters['radius'] != '') {
            /*// $filter_radius = " HAVING tdistance <= ".$filters['radius'];*/
            $filter_radius_field = " , ( 3959 * acos( cos( radians( " . $filters['current_latitude'] . " ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( " . $filters['current_longitude'] . " ) ) + sin( radians( " . $filters['current_latitude'] . " ) ) * sin( radians( a.latitude ) ) ) ) AS tdistance ";
        }
        $user = '';
        if (isset($filters['user']) && !empty($filters['user']))
            $user = ' AND b.user != ' . $filters['user'];
        $locationQry = '  AND b.origin_location=' . $filters['origin_id'] . ' AND b.destination_location=' . $filters['destination_id'];
        if (isset($filters['locationType']) && $filters['locationType'] == 1) {
            $locationQry = '  AND b.origin_location=' . $filters['origin_id'];
        } else if (isset($filters['locationType']) && $filters['locationType'] == 2) {
            $locationQry = '  AND b.destination_location=' . $filters['destination_id'];
        }
        if (isset($_POST['trip_id']) && !empty($_POST['trip_id'])) {
            $locationQry .= ' AND b.ID  != ' . (int) $_POST['trip_id'];
        }

       if (isset($filters['product_worth']) && !empty($filters['product_worth'])) {

            if($filters['product_worth'] < 100)
            {
                 $locationQry .= '  AND c.item_worth < 100';
            }
            else if($filters['product_worth'] == 100) 
            {
                 $locationQry .= '  AND c.item_worth BETWEEN 100 AND 250';
            }
            else if($filters['product_worth'] == 251) 
            {
                 $locationQry .= '  AND c.item_worth BETWEEN 251 AND 500';
            }
            else if($filters['product_worth'] == 501) 
            {
                 $locationQry .= '  AND c.item_worth BETWEEN 501 AND 1000';
            }
            else if($filters['product_worth'] == 1001) 
            {
                 $locationQry .= '  AND c.item_worth BETWEEN 1001 AND 2000';
            }
            else if($filters['product_worth'] == 2001) 
            {
                 $locationQry .= '  AND c.item_worth BETWEEN 2001 AND 3500';
            }
            else if($filters['product_worth'] == 3501) 
            {
                 $locationQry .= '  AND c.item_worth BETWEEN 3501 AND 5000';
            }
            else if($filters['product_worth'] == 5000) 
            {
                 $locationQry .= '  AND c.item_worth > 5000';
            }

           
        }

        if (isset($filters['sortBy']) && !empty($filters['sortBy'])) {
            if ($filters['sortBy'] == 'Rating') {
                $filter_radius .= ' ORDER BY ratingsCount DESC';
            } else if ($filters['sortBy'] == 'Reviews') {
                $filter_radius .= ' ORDER BY ratings DESC';
            } else if ($filters['sortBy'] == 'Distance') {
                $filter_radius .= ' ORDER BY tdistance ASC';
            } else if ($filters['sortBy'] == 'DistanceDesc') {
                $filter_radius .= ' ORDER BY tdistance DESC';
            }
        }
        if (isset($filters['catSearch']) && !empty($filters['catSearch'])) {
            $locationQry .= ' AND (c.product_type =  sc.ID OR c.task =  psc.ID)';
            if (isset($filters['task']) && !empty($filters['task'])) {
               // $locationQry .= ' AND (sc.catid = ' . $filters['task'] . ' OR psc.catid = ' . $filters['task'] . ')';
            
                $locationQry .= 'AND FIND_IN_SET("'.$filters['task'].'",c.product_category) ';
            }
            if (isset($filters['projsubcat']) && !empty($filters['projsubcat'])) {
                $locationQry .= ' AND (sc.ID = ' . $filters['projsubcat'] . ' OR psc.ID = ' . $filters['projsubcat'] . ')';
            }
        }
       
	  /* if (isset($filters['noofstops']) && $filters['noofstops'] !="") {
            $locationQry .= '  AND b.noofstops = "' . $filters['noofstops'] . '"';
        }*/
		
        /*if (isset($filters['origin_route_code']) && $filters['origin_route_code'] !="all") {
            $locationQry .= '  AND b.trip_route = "' . $filters['origin_route_code'] . '"';
        }*/
        if (isset($filters['origin_iata_code']) && $filters['origin_iata_code'] !="") {
           // $locationQry .= '  AND b.trip_route = "'.$filters['origin_iata_code'] .'-'.$filters['destination_iata_code'] .'"';
        }
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratingsCount ';
       /* if (isset($filters['days']) && $filters['days'] != '') {
            $start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'] . ' days'));
            $end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'] . ' days'));
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.task,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field  $ratingQry  FROM users_user as aa,users_user_locations as a, trips as b,traveller_projects as c ,package_sub_category sc,package_sub_category psc WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1  $locationQry AND b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user  $filter_airline_name_number $filter_radius";
			
        } else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.task,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field  $ratingQry  FROM users_user as aa,users_user_locations as a, trips as b,traveller_projects as c,package_sub_category sc,package_sub_category psc  WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1  $locationQry  AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "' $user $filter_airline_name_number $filter_radius";
        }*/

        if(isset($filters['days'][1]) && $filters['days'][1]=='d')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'days', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'days', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.task,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field  $ratingQry  FROM users_user as aa,users_user_locations as a, trips as b,traveller_projects as c ,package_sub_category sc,package_sub_category psc WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1  $locationQry AND b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user  $filter_airline_name_number $filter_radius";
        }
        else if(isset($filters['days'][1]) && $filters['days'][1]=='w')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
           $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.task,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field  $ratingQry  FROM users_user as aa,users_user_locations as a, trips as b,traveller_projects as c ,package_sub_category sc,package_sub_category psc WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1  $locationQry AND b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user  $filter_airline_name_number $filter_radius";
        }
        else if(isset($filters['days'][1]) && $filters['days'][1]=='m')
        {//$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'months', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'months', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
           $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.task,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field  $ratingQry  FROM users_user as aa,users_user_locations as a, trips as b,traveller_projects as c ,package_sub_category sc,package_sub_category psc WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1  $locationQry AND b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user  $filter_airline_name_number $filter_radius";
        }
        else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.task,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field  $ratingQry  FROM users_user as aa,users_user_locations as a, trips as b,traveller_projects as c,package_sub_category sc,package_sub_category psc  WHERE c.trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1  $locationQry  AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "' $user $filter_airline_name_number $filter_radius";
        }
		//echo $sql;die;
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getSeekersPeopleForSearch($filters)
    {
        //print_r($filters);
        $filter_airline_name = $filter_flight_number = $filter_airline_name_number = '';
        if (isset($filters['airline_name']) && $filters['airline_name'] != '')
            $filter_airline_name = " b.airline_name='" . $filters['airline_name'] . "' ";
        if (isset($filters['flight_number']) && $filters['flight_number'] != '')
            $filter_flight_number = " b.flight_number='" . $filters['flight_number'] . "' ";
        if ($filter_airline_name != '' && $filter_flight_number != '')
            $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";
        elseif ($filter_airline_name != '')
            $filter_airline_name_number = " AND $filter_airline_name ";
        elseif ($filter_flight_number != '')
            $filter_airline_name_number = " AND $filter_flight_number ";
        $filter_radius = $filter_radius_field = '';
        if (isset($filters['radius']) && $filters['radius'] != '') {
            /*$filter_radius = " HAVING tdistance <= ".$filters['radius'];*/
            $filter_radius_field = " , ( 3959 * acos( cos( radians( " . $filters['current_latitude'] . " ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( " . $filters['current_longitude'] . " ) ) + sin( radians( " . $filters['current_latitude'] . " ) ) * sin( radians( a.latitude ) ) ) ) AS tdistance ";
        }
        $user = '';
        if (isset($filters['user']) && !empty($filters['user']))
            $user = ' AND b.user != ' . $filters['user'];
        $locationQry = '  AND b.origin_location=' . $filters['origin_id'] . ' AND b.destination_location=' . $filters['destination_id'];
        if (isset($filters['locationType']) && $filters['locationType'] == 1) {
            $locationQry = '  AND b.origin_location=' . $filters['origin_id'];
        } else if (isset($filters['locationType']) && $filters['locationType'] == 2) {
            $locationQry = '  AND b.destination_location=' . $filters['destination_id'];
        }
        if (isset($_POST['trip_id']) && !empty($_POST['trip_id'])) {
            $locationQry .= ' AND b.ID  != ' . (int) $_POST['trip_id'];
        }
        if (isset($filters['sortBy']) && !empty($filters['sortBy'])) {
            if ($filters['sortBy'] == 'Rating') {
                $filter_radius .= ' ORDER BY ratingsCount DESC';
            } else if ($filters['sortBy'] == 'Reviews') {
                $filter_radius .= ' ORDER BY ratings DESC';
            } else if ($filters['sortBy'] == 'Distance') {
                $filter_radius .= ' ORDER BY tdistance ASC';
            } else if ($filters['sortBy'] == 'DistanceDesc') {
                $filter_radius .= ' ORDER BY tdistance DESC';
            }
        }
        if (isset($filters['noofpassanger']) && !empty($filters['noofpassanger'])) {
            $locationQry .= '  AND c.passengers = "' . $filters['noofpassanger'] . '"';
        }
        if (isset($filters['ticket_status']) && !empty($filters['ticket_status'])) {
            $locationQry .= '  AND b.travel_plan_reservation_type = "' . $filters['ticket_status'] . '"';
        }
        if (isset($filters['noofstops']) && $filters['noofstops'] !="") {
           // $locationQry .= '  AND b.noofstops = "' . $filters['noofstops'] . '"';
        }
        /*if (isset($filters['origin_route_code']) && $filters['origin_route_code'] !="all") {
            $locationQry .= '  AND b.trip_route = "' . $filters['origin_route_code'] . '"';
        }*/
        if (isset($filters['origin_iata_code']) && $filters['origin_iata_code'] !="all") {
            //$locationQry .= '  AND b.trip_route = "'.$filters['origin_iata_code'].'-'.$filters['destination_iata_code'].'"';
        }
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratingsCount ';
        /*if (isset($filters['days']) && $filters['days'] != '') {
            $start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'] . ' days'));
            $end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'] . ' days'));
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.passengers,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_people as c  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry AND b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user  $filter_airline_name_number $filter_radius";
        } else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.passengers,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_people as c  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "' $user $filter_airline_name_number $filter_radius";
        }*/
        if($filters['days'][1]=='d')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'days', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'days', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
             $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,b.departure_time,c.passengers,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_people as c  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry AND b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user  $filter_airline_name_number $filter_radius";
        }
        else if($filters['days'][1]=='w')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.passengers,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_people as c  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry AND b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user  $filter_airline_name_number $filter_radius";
        }
        else if($filters['days'][1]=='m')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'months', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'months', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
           $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.passengers,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_people as c  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry AND b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user  $filter_airline_name_number $filter_radius";
        }
        else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,b.trip_route,b.origin_location,b.destination_location,c.passengers,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_people as c  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "' $user $filter_airline_name_number $filter_radius";
        }
		//echo $sql ; die;
		
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
   public function getSeekersPackageForSearch($filters)
    {
        $filter_airline_name = $filter_flight_number = $filter_airline_name_number = '';
        if (isset($filters['airline_name']) && $filters['airline_name'] != '')
            $filter_airline_name = " b.airline_name='" . $filters['airline_name'] . "' ";
        if (isset($filters['flight_number']) && $filters['flight_number'] != '')
            $filter_flight_number = " b.flight_number='" . $filters['flight_number'] . "' ";
        if ($filter_airline_name != '' && $filter_flight_number != '')
            $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";
        elseif ($filter_airline_name != '')
            $filter_airline_name_number = " AND $filter_airline_name ";
        elseif ($filter_flight_number != '')
            $filter_airline_name_number = " AND $filter_flight_number ";
        $filter_radius = $filter_radius_field = '';
        if (isset($filters['radius']) && $filters['radius'] != '') {
            /*$filter_radius = " HAVING tdistance <= ".$filters['radius'];*/
            $filter_radius_field = " , ( 3959 * acos( cos( radians( " . $filters['current_latitude'] . " ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( " . $filters['current_longitude'] . " ) ) + sin( radians( " . $filters['current_latitude'] . " ) ) * sin( radians( a.latitude ) ) ) ) AS tdistance ";
        }
        $user = '';
        if (isset($filters['user']) && !empty($filters['user']))
            $user = ' AND b.user != ' . $filters['user'];
        $locationQry = '  AND b.origin_location=' . $filters['origin_id'] . ' AND b.destination_location=' . $filters['destination_id'];
        if (isset($filters['locationType']) && $filters['locationType'] == 1) {
            $locationQry = '  AND b.origin_location=' . $filters['origin_id'];
        } else if (isset($filters['locationType']) && $filters['locationType'] == 2) {
            $locationQry = '  AND b.destination_location=' . $filters['destination_id'];
        }
        if (isset($_POST['trip_id']) && !empty($_POST['trip_id'])) {
            $locationQry .= ' AND b.ID  != ' . (int) $_POST['trip_id'];
        }
        if (isset($filters['sortBy']) && !empty($filters['sortBy'])) {
            if ($filters['sortBy'] == 'Rating') {
                $filter_radius .= ' ORDER BY ratingsCount DESC';
            } else if ($filters['sortBy'] == 'Reviews') {
                $filter_radius .= ' ORDER BY ratings DESC';
            } else if ($filters['sortBy'] == 'Distance') {
                $filter_radius .= ' ORDER BY tdistance ASC';
            } else if ($filters['sortBy'] == 'DistanceDesc') {
                $filter_radius .= ' ORDER BY tdistance DESC';
            }
        }
        if (isset($filters['package_worth']) && !empty($filters['package_worth'])) {

            if($filters['package_worth'] < 100)
            {
                 $locationQry .= '  AND c.total_worth < 100';
            }
            else if($filters['package_worth'] == 100) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 100 AND 250';
            }
            else if($filters['package_worth'] == 251) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 251 AND 500';
            }
            else if($filters['package_worth'] == 501) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 501 AND 1000';
            }
            else if($filters['package_worth'] == 1001) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 1001 AND 2000';
            }
            else if($filters['package_worth'] == 2001) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 2001 AND 3500';
            }
            else if($filters['package_worth'] == 3501) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 3501 AND 5000';
            }
            else if($filters['package_worth'] == 5000) 
            {
                 $locationQry .= '  AND c.total_worth > 5000';
            }

           // echo '<pre>';print_r($filters['package_worth']);exit();
            //$locationQry .= '  AND c.total_worth = "' . $filters['package_worth'] . '"';
        }
        if (isset($filters['package_weight']) && !empty($filters['package_weight'])) {

            if($filters['package_weight'] == 1)
            {
                 $locationQry .= '  AND c.total_weight BETWEEN 1 AND 5';
            }
            else if($filters['package_weight'] == 5) 
            {
                 $locationQry .= '  AND c.total_weight BETWEEN 5 AND 10';
            }
            else if($filters['package_weight'] == 10) 
            {
                 $locationQry .= '  AND c.total_weight BETWEEN 10 AND 15';
            }
            else if($filters['package_weight'] == 15) 
            {
                 $locationQry .= '  AND c.total_weight BETWEEN 15 AND 20';
            }
            else if($filters['package_weight'] == 20) 
            {
                 $locationQry .= '  AND c.total_weight BETWEEN 20 AND 30';
            }
            else if($filters['package_weight'] == 30) 
            {
                 $locationQry .= '  AND c.total_weight BETWEEN 30 AND 40';
            }
            else if($filters['package_weight'] == 40) 
            {
                 $locationQry .= '  AND c.total_weight BETWEEN 40 AND 50';
            }
            else if($filters['package_weight'] == 50) 
            {
                 $locationQry .= '  AND c.total_weight >= 50';
            }
        

            //$locationQry .= '  AND c.total_weight = "' . $filters['package_weight'] . '"';
        }
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratingsCount ';
        /*if (isset($filters['days']) && $filters['days'] != '') {
            $start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'] . ' days'));
            $end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'] . ' days'));
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,c.total_weight,c.total_worth,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b, seeker_package as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.seeker_trip=b.ID AND b.active=1 $locationQry AND  b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user $filter_airline_name_number $filter_radius";
        } else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,c.total_weight,c.total_worth,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b, seeker_package as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.seeker_trip=b.ID AND b.active=1 $locationQry  AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "' $user  $filter_airline_name_number $filter_radius";
        }*/
            if($filters['days'][1]=='d')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'days', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'days', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
           $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,c.total_weight,c.total_worth,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b, seeker_package as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.seeker_trip=b.ID AND b.active=1 $locationQry AND b.project_end_date>='" . $start_date . "' AND b.project_start_date<='" . $end_date . "'$user $filter_airline_name_number $filter_radius";
        }
        else if($filters['days'][1]=='w')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
           $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,c.total_weight,c.total_worth,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b, seeker_package as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.seeker_trip=b.ID AND b.active=1 $locationQry AND   b.project_end_date>='" . $start_date . "' AND b.project_start_date<='" . $end_date . "'$user $filter_airline_name_number $filter_radius";
        }
        else if($filters['days'][1]=='m')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'months', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'months', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
           $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,c.total_weight,c.total_worth,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b, seeker_package as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.seeker_trip=b.ID AND b.active=1 $locationQry AND  b.project_end_date>='" . $start_date . "' AND b.project_start_date<='" . $end_date . "' $user $filter_airline_name_number $filter_radius";
        }
        else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,a.*,aa.first_name,c.total_weight,c.total_worth,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b, seeker_package as c  WHERE aa.ID=b.user AND a.ID=b.user_location AND c.seeker_trip=b.ID AND b.active=1 $locationQry  AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "' $user  $filter_airline_name_number $filter_radius";
        }
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getSeekersProjectForSearch($filters)
    {
        $filter_airline_name = $filter_flight_number = $filter_airline_name_number = '';
        if (isset($filters['airline_name']) && $filters['airline_name'] != '')
            $filter_airline_name = " b.airline_name='" . $filters['airline_name'] . "' ";
        if (isset($filters['flight_number']) && $filters['flight_number'] != '')
            $filter_flight_number = " b.flight_number='" . $filters['flight_number'] . "' ";
        if ($filter_airline_name != '' && $filter_flight_number != '')
            $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";
        elseif ($filter_airline_name != '')
            $filter_airline_name_number = " AND $filter_airline_name ";
        elseif ($filter_flight_number != '')
            $filter_airline_name_number = " AND $filter_flight_number ";
        $filter_radius = $filter_radius_field = '';
        if (isset($filters['radius']) && $filters['radius'] != '') {
            /*$filter_radius = " HAVING tdistance <= ".$filters['radius'];*/
            $filter_radius_field = " , ( 3959 * acos( cos( radians( " . $filters['current_latitude'] . " ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( " . $filters['current_longitude'] . " ) ) + sin( radians( " . $filters['current_latitude'] . " ) ) * sin( radians( a.latitude ) ) ) ) AS tdistance ";
        }
        $user = '';
        if (isset($filters['user']) && !empty($filters['user']))
            $user = ' AND b.user != ' . $filters['user'];
        $locationQry = '  AND b.origin_location=' . $filters['origin_id'] . ' AND b.destination_location=' . $filters['destination_id'];
        if (isset($filters['locationType']) && $filters['locationType'] == 1) {
            $locationQry = '  AND b.origin_location=' . $filters['origin_id'];
        } else if (isset($filters['locationType']) && $filters['locationType'] == 2) {
            $locationQry = '  AND b.destination_location=' . $filters['destination_id'];
        }
        if (isset($_POST['trip_id']) && !empty($_POST['trip_id'])) {
            $locationQry .= ' AND b.ID  != ' . (int) $_POST['trip_id'];
        }
        if (isset($filters['sortBy']) && !empty($filters['sortBy'])) {
            if ($filters['sortBy'] == 'Rating') {
                $filter_radius .= ' ORDER BY ratingsCount DESC';
            } else if ($filters['sortBy'] == 'Reviews') {
                $filter_radius .= ' ORDER BY ratings DESC';
            } else if ($filters['sortBy'] == 'Distance') {
                $filter_radius .= ' ORDER BY tdistance ASC';
            } else if ($filters['sortBy'] == 'DistanceDesc') {
                $filter_radius .= ' ORDER BY tdistance DESC';
            }
        }        
       if (isset($filters['product_worth']) && !empty($filters['product_worth'])) {

            if($filters['product_worth'] < 100)
            {
                 $locationQry .= '  AND c.total_worth < 100';
            }
            else if($filters['product_worth'] == 100) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 100 AND 250';
            }
            else if($filters['product_worth'] == 251) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 251 AND 500';
            }
            else if($filters['product_worth'] == 501) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 501 AND 1000';
            }
            else if($filters['product_worth'] == 1001) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 1001 AND 2000';
            }
            else if($filters['product_worth'] == 2001) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 2001 AND 3500';
            }
            else if($filters['product_worth'] == 3501) 
            {
                 $locationQry .= '  AND c.total_worth BETWEEN 3501 AND 5000';
            }
            else if($filters['product_worth'] == 5000) 
            {
                 $locationQry .= '  AND c.total_worth > 5000';
            }

           
        }
        if (isset($filters['catSearch']) && !empty($filters['catSearch'])) {
            $locationQry .= ' AND (pt.project_request =  c.ID)';
            if (isset($filters['task']) && !empty($filters['task'])) {
                $locationQry .= ' AND (pt.category = ' . $filters['task'] . ')';
            }
            if (isset($filters['projsubcat']) && !empty($filters['projsubcat'])) {
                $locationQry .= ' AND (pt.additional_requirements_category = ' . $filters['projsubcat'] . ')';
            }
        }
        if (isset($_POST['work_category']) && $_POST['work_category'] != 'all') {
            $locationQry .= ' AND (c.work_category = ' . (int) $_POST['work_category'] . ')';
        }
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratingsCount ';
        /*if (isset($filters['days']) && $filters['days'] != '') {
            $start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'] . ' days'));
            $end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'] . ' days'));
            $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,c.work_category,a.*,aa.first_name,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry  FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_project as c,seeker_project_tasks as pt  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry AND b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $user   $filter_airline_name_number $filter_radius";
        } else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,c.work_category,a.*,aa.first_name,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_project as c,seeker_project_tasks as pt  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry  AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "' $user  $filter_airline_name_number $filter_radius";
        }*/
              if($filters['days'][1]=='d')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'days', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'days', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
          $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,c.work_category,a.*,aa.first_name,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry  FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_project as c,seeker_project_tasks as pt  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry  AND b.project_end_date>='" . $start_date . "' AND b.project_start_date<='" . $end_date . "' $user   $filter_airline_name_number $filter_radius";
        }
        else if($filters['days'][1]=='w')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'weeks', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
          $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,c.work_category,a.*,aa.first_name,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry  FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_project as c,seeker_project_tasks as pt  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry AND b.project_end_date>='" . $start_date . "' AND b.project_start_date<='" . $end_date . "' $user   $filter_airline_name_number $filter_radius";
        }
        else if($filters['days'][1]=='m')
        {
            //$start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'][0] );
            $start_date = date('Y-m-d', strtotime('-'.$filters['days'][0].'months', strtotime($filters['date']))). ' 00:00:00';
            $end_date = date('Y-m-d', strtotime('+'.$filters['days'][0].'months', strtotime($filters['date']))). ' 23:59:59';
            //$end_date   = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'][0]);
           $sql        = "SELECT DISTINCT(a.ID),b.ID as tripid,c.work_category,a.*,aa.first_name,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry  FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_project as c,seeker_project_tasks as pt  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry AND  b.project_end_date>='" . $start_date . "' AND b.project_start_date<='" . $end_date . "' $user   $filter_airline_name_number $filter_radius";
        }
        else {
            $sql = "SELECT DISTINCT(a.ID),b.ID as tripid,c.work_category,a.*,aa.first_name,aa.last_name,aa.image,b.trip_route,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%d/%b/%Y') as departure_date,DATE_FORMAT(aa.created,'%M %Y') as member_since $filter_radius_field $ratingQry FROM users_user as aa,users_user_locations as a, seeker_trips as b,seeker_project as c,seeker_project_tasks as pt  WHERE c.seeker_trip=b.ID AND aa.ID=b.user AND a.ID=b.user_location AND b.active=1 $locationQry  AND DATE_FORMAT(b.departure_date,'%m%d%Y') ='" . date("mdY", strtotime($filters['date'])) . "' $user  $filter_airline_name_number $filter_radius";
        }
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    /*public function getTrips($trip_status = '', $userrole, $userid)
    {
        $status_filter = "";
        if ($trip_status == 'request_list'){
            $status_filter = " AND b.active=1 AND ((b.departure_date <=  DATE_ADD(NOW(), INTERVAL 3 HOUR) AND b.departure_date >=  DATE_ADD(NOW(), INTERVAL -3 HOUR)) OR (b.departure_date >= '" . date('Y-m-d H:i:s') . "')) GROUP BY b.ID";
        }
        if ($trip_status == 'current_upcoming'){
            $status_filter = " AND b.active=1 AND (b.departure_date <=  DATE_ADD(NOW(), INTERVAL 3 HOUR) AND b.departure_date >=  DATE_ADD(NOW(), INTERVAL -3 HOUR)) OR (b.departure_date >= '" . date('Y-m-d H:i:s') . "') ";
        }
        if ($trip_status == 'current'){
            $status_filter = " AND b.active=1 AND b.departure_date <=  DATE_ADD(NOW(), INTERVAL 3 HOUR) AND b.departure_date >=  DATE_ADD(NOW(), INTERVAL -3 HOUR) ";
        }
        if ($trip_status == 'upcoming'){
            $status_filter = " AND b.active=1 AND  b.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        }
        elseif ($trip_status == 'cancelled'){
            $status_filter = " AND b.trip_status = 6 ";
        }
        elseif ($trip_status == 'completed'){
            $status_filter = " AND b.trip_status = 7 AND b.departure_date <= '" . date('Y-m-d H:i:s') . "' ";
        }
        if ($userrole == 'seeker' && $trip_status == 'request_list'){
            $sql = "SELECT b.* FROM seeker_trips AS b JOIN traveller_project_request AS c ON b.ID=c.trip UNION SELECT b.* FROM seeker_trips AS b JOIN traveller_people_request AS d ON b.ID=d.trip UNION SELECT b.* FROM seeker_trips AS b JOIN traveller_package_request AS e ON b.ID=e.trip WHERE user='" . $userid . "' $status_filter";
        }
        if ($userrole == 'seeker' && $trip_status != 'request_list'){
        $sql = "SELECT b.* FROM seeker_trips as b WHERE user='" . $userid . "' $status_filter";
    }
        if ($userrole == 'traveller' && $trip_status != 'request_list'){
            $sql = "SELECT b.* FROM trips as b WHERE user='" . $userid . "' $status_filter";
        }
        if ($userrole == 'traveller' && $trip_status == 'request_list'){
            $sql = "SELECT b.* FROM trips AS b JOIN seeker_project_request  WHERE user='" . $userid . "' $status_filter";
        }
        
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }*/
    public function getTrips($trip_status = '', $userrole, $userid)
    {
        $status_filter = "";
        if ($trip_status == 'current_upcoming')
            $status_filter = " AND b.trip_status != 6 AND b.trip_status != 7 AND b.active=1 AND (b.arrival_date <=  DATE_ADD(NOW(), INTERVAL 48 HOUR) AND b.arrival_date >=  DATE_ADD(NOW(), INTERVAL -48 HOUR) OR b.departure_date >= '" . date('Y-m-d H:i:s') . "') AND b.is_expired = 0 ";
        if ($trip_status == 'current')
            $status_filter = " AND b.trip_status != 6 AND b.trip_status != 7 AND b.active=1 AND b.arrival_date <=  DATE_ADD(NOW(), INTERVAL 48 HOUR) AND b.arrival_date >=  DATE_ADD(NOW(), INTERVAL -48 HOUR) ";
        if ($trip_status == 'upcoming')
            $status_filter = " AND b.trip_status != 6 AND b.trip_status != 7 AND b.active=1 AND  b.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
		elseif ($trip_status == 'active')
            $status_filter = " AND b.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
		elseif ($trip_status == 'expired')
            $status_filter = " AND b.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        elseif ($trip_status == 'cancelled')
            $status_filter = " AND b.trip_status = 7 ";
        elseif ($trip_status == 'completed')
            $status_filter = " AND b.trip_status = 6 AND b.arrival_date <= '" . date('Y-m-d H:i:s') . "' ";
        if ($userrole == 'seeker')
            $sql = "SELECT b.* FROM seeker_trips as b WHERE b.user='" . $userid . "' $status_filter";
        else
            $sql = "SELECT b.* FROM trips as b WHERE b.user='" . $userid . "' $status_filter";
        //echo $sql;exit;/* */
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getWishlistTrips($trip_status = '', $userrole, $userid)
    {
        $status_filter = "";
        $status_filter = " AND b.active=1 AND  b.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        $ratingQry     = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE aa.ID = r.recevier_id) AS ratingsCount ';
        if ($userrole == 'seeker')
            $sql = "SELECT b.*,w.trip_added,w.ID as wishlistId,aa.image $ratingQry FROM seeker_wishlist AS w JOIN trips as b ON (w.trip_id = b.ID) JOIN users_user as aa ON (aa.ID = b.user) WHERE w.user_id='" . $userid . "' $status_filter";
        else
            $sql = "SELECT b.*,w.trip_added,w.ID as wishlistId,aa.image $ratingQry FROM traveller_wishlist AS w JOIN  seeker_trips  as b ON (w.trip_id = b.ID) JOIN users_user as aa ON (aa.ID = b.user) WHERE w.user_id='" . $userid . "' $status_filter";
        /*echo $sql;exit; */
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getAvailableAirlines($looking_for)
    {
        $tableName = ($looking_for == 'seeker') ? 'seeker_trips' : 'trips';
        $sql       = ' SELECT DISTINCT(TF.airline_name) AS airlineName,T.*,TF.airline_number FROM ' . $tableName . ' AS T JOIN trips_flight AS TF ON (user_role = "' . $looking_for . '" AND TF.trip = T.ID ) group by TF.airline_name';
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getAirPort($ID)
    {
        $sql       = "SELECT * FROM core_airports WHERE ID ='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getAirPortByCode($code)
    {
        $sql       = "SELECT * FROM core_airports WHERE code ='" . $code . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTripPeople($tripid)
    {
        $sql       = "SELECT * FROM traveller_people WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return null;
    }
    public function getTripPackage($tripid)
    {
        $sql       = "SELECT * FROM traveller_packages WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return null;
    }
    public function addTripreview($trip_review, $ID)
    {
        $sql       = "SELECT `user` FROM trips WHERE ID='" . $ID . "'"; //echo $sql; die;
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            $rs = $res->current();
        } else
            $rs = false;
        if ($rs['user']) {
            $insert  = $this->sql->insert('users_reviews');
            $newData = $trip_review;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid       = $this->adapter->getDriver()->getLastGeneratedValue();
            /*$update       = $this->sql->update();
            $update->table('users_reviews');
            $update->set(array(
                "recevier_id" => $rs['user']
            ));
            $update->where(array(
                'ID' => $userid
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results   = $statement->execute();*/
        }
    }
    public function getTripreview($ID, $userid, $all = false, $userrole = 'traveller')
    {
		if($userrole == 'seeker'){
			$tripField = 'seeker_trip_id';
		} else {
			$tripField = 'trip_id';
		}
        $sql       = "SELECT * FROM users_reviews WHERE trip_id='" . $ID . "' AND sender_id='" . $userid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
			if($all){
				return $res->getResource()->fetchAll();
			} else {
            	return $res->current();
			}
        } else
            return array();
    }
    public function getTripreviewall($userid, $all = false)
    {
        $sql       = "SELECT b.name,b.departure_date,b.arrival_date,a.*,c.image,c.first_name,c.last_name,c.location FROM users_reviews as a,trips as b,users_user as c WHERE a.trip_id=b.ID  AND  a.recevier_id='" . $userid . "' AND c.ID=a.sender_id";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
			if($all){
				return $res->getResource()->fetchAll();
			} else {
            	return $res->current();
			}
        } else
            return false;
    }
    public function getuservaliddetails($editadminidu, $table)
    {
        $sql       = "SELECT a.field_name,b.validtype,b.notifalert,b.msg FROM core_table_fieldvalue as a,users_field_valid as b WHERE  b.field_id=a.ID AND b.user_id='" . $editadminidu . "' and a.table_name='" . $table . "'  AND a.ID IN(3,5)";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            $rs = $res->current();
        } else
            $rs = false;
        $retv = array();
        if ($rs) {
            foreach ($rs as $val => $key) {
                $fldn      = $key['field_name'];
                $sqlv      = "SELECT " . $fldn . " FROM users_user WHERE ID='" . $editadminidu . "'";
                $statement = $this->adapter->query($sql);
                $res       = $statement->execute();
                if ($res->count() > 0) {
                    $rsv = $res->current();
                } else
                    $rsv = false;
                if ($retv) {
                    $retv[$key['field_name']] = array(
                        "0" => $rsv[$fldn],
                        "1" => $key['validtype']
                    );
                }
            }
            return $retv;
        } else {
            return false;
        }
    }
    public function getTripProjectTask($ID)
    {
        $sql       = "SELECT * FROM core_tasks_category WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getProjectTasksCategory()
    {
        $sql       = "SELECT * FROM core_tasks_category";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getProjectTasksCategoryAll()
    {
        $sql       = "SELECT * FROM core_tasks_category";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getProjectTasksCategorytype($id = false)
    {
        $sql = "SELECT * FROM project_category WHERE status='1' and deleted=0";
        if ($id !== false && $id != '') {
            $sql .= ' AND work_category=' . $id;
        }
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getPackageCategorytype($id = false)
    {
        $sql = "SELECT * FROM package_category WHERE status='1' and deleted=0";
        if ($id !== false && $id != '') {
            $sql .= ' AND work_category=' . $id;
        }
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getPackageTasksCategory()
    {
        $sql       = "SELECT * FROM package_category where deleted=0";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getPackageTasksCategorylisttype($id)
    {
        $subquery  = "SELECT ID, subcatname from package_sub_category where catid = '" . $id . "' and deleted=0";
        $statement = $this->adapter->query($subquery);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getProjectTasksCategorylisttype($id)
    {
        $subquery  = "SELECT ID, subcatname from project_sub_category where catid = '" . $id . "' and deleted=0";
        $statement = $this->adapter->query($subquery);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getProjectTasksCategorylisttypeAll()
    {
        $subquery  = "SELECT ID, subcatname from project_sub_category where  deleted=0";
        $statement = $this->adapter->query($subquery);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getPackageTasksCategorylisttypeAll()
    {
        $subquery  = "SELECT ID, subcatname from package_sub_category where  deleted=0";
        $statement = $this->adapter->query($subquery);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
	public function getTripCurrentStatus($tripid,$userrole){
		$trip = $this->getTrip($tripid,$userrole);
		$status = '';
		$requestStatus = array();
		if($userrole == 'seeker'){
			$trip_people	= $this->getSeekerPeople($tripid);
			$trip_package	= $this->getSeekerPackage($tripid);
			$trip_project	= $this->getSeekerProject($tripid);

			$tripServiceType = (is_array($trip_people) && count($trip_people) > 0)?'people':'';
			$tripServiceType = (is_array($trip_package) && count($trip_package) > 0)?'package':$tripServiceType;
			$tripServiceType = (is_array($trip_project) && count($trip_project) > 0)?'project':$tripServiceType;

			if($trip['is_expired'] == '1'){
				$status = 'Trip listing expired';
			} else if($trip['trip_status'] =='0' && $tripServiceType == 'package'){
				$status = 'Awaiting ID Document';
			} else if($trip['trip_status'] =='2' && $tripServiceType == 'package'){
				$status = 'Awaiting ID Approval';
			} elseif($trip['trip_status'] =='6'){
				$status = 'Travel Service Completed';
			} elseif($trip['trip_status'] =='7'){
				$status = 'Cancelled';
			} else{
				$inquired = $this->getTripInquired($tripid,$userrole);
				if(count($inquired) > 0){
					$status = 'Travel Service Inquired';
				}
				if($status == ''){
					$pending = $this->getTravellerTripRequestStatus($tripid,0);
					if(count($pending) > 0 && ($pending['pe_re'] > 0 || $pending['pr_re'] > 0 || $pending['pa_re'] > 0 )){
						$status = 'Service Decision Pending';
					}
				}
				if($status == ''){
					if($tripServiceType == 'people'){
						$requestStatus = $this->getSeekersRequestStatus($trip_people['ID'],'people',0);
					} else if($tripServiceType == 'package'){
						$requestStatus = $this->getSeekersRequestStatus($trip_package['ID'],'package',0);
					} else if($tripServiceType == 'project'){
						$requestStatus = $this->getSeekersRequestStatus($trip_project['ID'],'project',0);
					}
					if(count($requestStatus) > 0 && isset($requestStatus['status_cnt']) && $requestStatus['status_cnt'] > 0){
						$status = 'Service Request Sent';
					}
				}
				if($status == ''){
					if($tripServiceType == 'people'){
						$requestStatus = $this->getSeekersRequestStatus($trip_people['ID'],'people',3);
					} else if($tripServiceType == 'package'){
						$requestStatus = $this->getSeekersRequestStatus($trip_package['ID'],'package',3);
					} else if($tripServiceType == 'project'){
						$requestStatus = $this->getSeekersRequestStatus($trip_project['ID'],'project',3);
					}
					if(count($requestStatus) > 0 && isset($requestStatus['status_cnt']) && $requestStatus['status_cnt'] > 0){
						$status = 'Service Request Expired';
					}
				}
				if($status == ''){
					if($tripServiceType == 'people'){
						$requestStatus = $this->getSeekersRequestStatus($trip_people['ID'],'people',1);
					} else if($tripServiceType == 'package'){
						$requestStatus = $this->getSeekersRequestStatus($trip_package['ID'],'package',1);
					} else if($tripServiceType == 'project'){
						$requestStatus = $this->getSeekersRequestStatus($trip_project['ID'],'project',1);
					}
					if(count($requestStatus) > 0 && isset($requestStatus['status_cnt']) && $requestStatus['status_cnt'] > 0){
						$status = 'Service Request Accepted';
					}
				}
				if($status == ''){
					if($tripServiceType == 'people'){
						$requestStatus = $this->getSeekersRequestStatus($trip_people['ID'],'people',2);
					} else if($tripServiceType == 'package'){
						$requestStatus = $this->getSeekersRequestStatus($trip_package['ID'],'package',2);
					} else if($tripServiceType == 'project'){
						$requestStatus = $this->getSeekersRequestStatus($trip_project['ID'],'project',2);
					}
					if(count($requestStatus) > 0 && isset($requestStatus['status_cnt']) && $requestStatus['status_cnt'] > 0){
						$status = 'Service Request Declined';
					}
				}
				if($status == ''){
					if(date('Y-m-d',strtotime($trip['arrival_date'])) != '1970-01-01' && strtotime($trip['arrival_date']) <= strtotime("now") &&  strtotime($trip['arrival_date']." +48 hours") >= strtotime("now")){
						$status = 'Completion Notification Pending';
					}
				}
				if($status == ''){
					$status = 'Trip Listing Approved';
				}
			}
		} else {
			
			if($trip['is_expired'] == '1'){
				$status = 'Trip listing expired';
			} else if($trip['trip_status'] =='6'){
				$status = 'Travel Service Completed';
			} elseif($trip['trip_status'] =='7'){
				$status = 'Cancelled';
			} elseif($trip['trip_status'] =='8'){
				$status = 'Service Earnings Paid';
			} else{
				$trip_people	= $this->getTravellerPeopleByTrip($tripid);
				$trip_package	= $this->getTravellerPackageByTrip($tripid);
				$trip_project	= $this->getTravellerProjectByTrip($tripid);
	
				$tripServiceType = (is_array($trip_people) && count($trip_people) > 0)?'people':'';
				$tripServiceType = (is_array($trip_package) && count($trip_package) > 0)?'package':$tripServiceType;
				$tripServiceType = (is_array($trip_project) && count($trip_project) > 0)?'project':$tripServiceType;
			
				$inquired = $this->getTripInquired($tripid,$userrole);
				if(count($inquired) > 0){
					$status = 'Travel Service Inquired';
				}
				if($status == ''){
					$pending = $this->getSeekerTripRequestStatus($tripid,0);
					if(count($pending) > 0 && ($pending['pe_re'] > 0 || $pending['pr_re'] > 0 || $pending['pa_re'] > 0 )){
						$status = 'Service Decision Pending';
					}
				}
				if($status == ''){
					if($tripServiceType == 'people'){
						$requestStatus = $this->getTravellersRequestStatus($trip_people['ID'],'people',0);
					} else if($tripServiceType == 'package'){
						$requestStatus = $this->getTravellersRequestStatus($trip_package['ID'],'package',0);
					} else if($tripServiceType == 'project'){
						$requestStatus = $this->getTravellersRequestStatus($trip_project['ID'],'project',0);
					}
					if(count($requestStatus) > 0 && isset($requestStatus['status_cnt']) && $requestStatus['status_cnt'] > 0){
						$status = 'Service Request Sent';
					}
				}
				if($status == ''){
					if($tripServiceType == 'people'){
						$requestStatus = $this->getTravellersRequestStatus($trip_people['ID'],'people',3);
					} else if($tripServiceType == 'package'){
						$requestStatus = $this->getTravellersRequestStatus($trip_package['ID'],'package',3);
					} else if($tripServiceType == 'project'){
						$requestStatus = $this->getTravellersRequestStatus($trip_project['ID'],'project',3);
					}
					if(count($requestStatus) > 0 && isset($requestStatus['status_cnt']) && $requestStatus['status_cnt'] > 0){
						$status = 'Service Request Expired';
					}
				}
				if($status == ''){
					//$accepted = $this->getTravellerTripRequestStatus($tripid,1);
					if($tripServiceType == 'people'){
						$requestStatus = $this->getTravellersRequestStatus($trip_people['ID'],'people',1);
					} else if($tripServiceType == 'package'){
						$requestStatus = $this->getTravellersRequestStatus($trip_package['ID'],'package',1);
					} else if($tripServiceType == 'project'){
						$requestStatus = $this->getTravellersRequestStatus($trip_project['ID'],'project',1);
					}
					if(count($requestStatus) > 0 && isset($requestStatus['status_cnt']) && $requestStatus['status_cnt'] > 0){
						$status = 'Service Request Accepted';
					}
				}
				if($status == ''){
					//$accepted = $this->getTravellerTripRequestStatus($tripid,2);
					if($tripServiceType == 'people'){
						$requestStatus = $this->getTravellersRequestStatus($trip_people['ID'],'people',2);
					} else if($tripServiceType == 'package'){
						$requestStatus = $this->getTravellersRequestStatus($trip_package['ID'],'package',2);
					} else if($tripServiceType == 'project'){
						$requestStatus = $this->getTravellersRequestStatus($trip_project['ID'],'project',2);
					}
					if(count($requestStatus) > 0 && isset($requestStatus['status_cnt']) && $requestStatus['status_cnt'] > 0){
						$status = 'Service Request Declined';
					}
				}
				if($status == ''){
					if(date('Y-m-d',strtotime($trip['arrival_date'])) != '1970-01-01' && strtotime($trip['arrival_date']) <= strtotime("now") &&  strtotime($trip['arrival_date']." +48 hours") >= strtotime("now")){
						$status = 'Completion Notification Pending';
					}
				} 
				if($trip['trip_status'] =='0' && $status == ''){
					$status = 'Awaiting ID Document';
				} 
                if($trip['trip_status'] =='1' && $status == ''){
                    $status = 'Service Request Accepted';
                }
				if($trip['trip_status'] =='2' && $status == ''){
					$status = 'Awaiting ID Approval';
				} 
				if($trip['trip_status'] =='3' && $status == '') {
					$status = 'Awaiting Ticket Approval';
				} if($trip['trip_status'] =='4' && $status == ''){
					$status = 'Awaiting Ticket Document';
				} 
				if($status == ''){
					$status = 'Trip Listing Approved';
				}
			}
		}
		return $status;
	}
	public function inactiveTripInquired($tId,$sId,$userRole){
		$tripTable = $userRole == 'seeker'?'seeker_inquires':'travaller_inquires';

		$update = $this->sql->update();
        $update->table($tripTable);
        $update->set(array('active'=>"0"));
        $update->where(array(
            'traveller_trip_id' => $tId,
            'seeker_trip_id' => $sId
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
	}
	public function getTripInquired($tripId,$userRole){
		
		$tripField = $userRole == 'seeker'?'seeker_trip_id':'traveller_trip_id';
		$tripTable = $userRole == 'seeker'?'seeker_inquires':'travaller_inquires';
		
		$query = 'SELECT * FROM '.$tripTable.' WHERE '.$tripField.' = '.$tripId.' AND active = "1" ';
		$statement = $this->adapter->query($query);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
	}
	public function getSeekersRequestStatus($tripId,$type = 'people',$status = '0'){
		if($type == 'people'){
			$table = 'seeker_people_request';
			$field = 'people_request';
		} else if($type == 'package'){
			$table = 'seeker_package_request';
			$field = 'package_request';
		} else if($type == 'project'){
			$table = 'seeker_project_request';
			$field = 'project_request';
		}
		$query = 'SELECT COUNT(trip) AS status_cnt FROM '.$table.' 
				  WHERE '.$field.' = "'.$tripId.'" AND approved = "'.$status.'" ';
		$statement = $this->adapter->query($query);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
	}
	public function getTravellersRequestStatus($tripId,$type = 'people',$status = '0'){
		if($type == 'people'){
			$table = 'traveller_people_request';
			$field = 'people_request';
		} else if($type == 'package'){
			$table = 'traveller_package_request';
			$field = 'package_request';
		} else if($type == 'project'){
			$table = 'traveller_project_request';
			$field = 'project_request';
		}
		$query = 'SELECT COUNT(trip) AS status_cnt FROM '.$table.' 
				  WHERE '.$field.' = "'.$tripId.'" AND approved = "'.$status.'" ';
		$statement = $this->adapter->query($query);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
	}
	public function getTravellerTripRequestStatus($tripId,$status = 1){
		$query = 'SELECT COUNT(pe.trip) AS pe_re,COUNT(pr.trip) AS pr_re,COUNT(pa.trip) AS pa_re FROM seeker_trips AS tt 
				  LEFT JOIN traveller_people_request AS pe ON pe.trip = tt.ID AND pe.approved = '.$status.'
				  LEFT JOIN traveller_project_request AS pr ON pr.trip = tt.ID AND pr.approved = '.$status.' 
				  LEFT JOIN traveller_package_request AS pa ON pa.trip = tt.ID  AND pa.approved = '.$status.'
				  WHERE tt.ID = "'.$tripId.'" ';
		$statement = $this->adapter->query($query);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
	}
	public function getSeekerTripRequestStatus($tripId,$status = 1){
		$query = 'SELECT COUNT(pe.trip) AS pe_re,COUNT(pr.trip) AS pr_re,COUNT(pa.trip) AS pa_re FROM trips AS tt 
				  LEFT JOIN seeker_people_request AS pe ON pe.trip = tt.ID AND pe.approved = '.$status.'
				  LEFT JOIN seeker_project_request AS pr ON pr.trip = tt.ID AND pr.approved = '.$status.' 
				  LEFT JOIN seeker_package_request AS pa ON pa.trip = tt.ID  AND pa.approved = '.$status.'
				  WHERE tt.ID = "'.$tripId.'" ';
		$statement = $this->adapter->query($query);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
	}
    public function getSeekerPeopleRequest($tripid)
    {
        $sql       = "SELECT * FROM seeker_people_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getSeekerPackageRequest($tripid)
    {
        $sql       = "SELECT * FROM seeker_package_request WHERE trip='" . $tripid . "'";

        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getSeekerProjectRequest($tripid)
    {
        $sql       = "SELECT * FROM seeker_project_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getSeekerPeopleRequestStats($tripid)
    {
        $sql = "SELECT COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM seeker_people_request WHERE approved = 0 AND trip='" . $tripid . "') AS awaiting,
				(SELECT COUNT(*) FROM seeker_people_request WHERE approved = 1 AND trip='" . $tripid . "') AS approved,
				(SELECT COUNT(*) FROM seeker_people_request WHERE (approved = 2 OR approved = 5) AND trip='" . $tripid . "') AS declined,
				(SELECT COUNT(*) FROM seeker_people_request WHERE (approved = 3 OR approved = 4) AND trip='" . $tripid . "') AS expired
				FROM seeker_people_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPackageRequestStats($tripid)
    {
        $sql = "SELECT COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM seeker_package_request WHERE approved = 0 AND trip='" . $tripid . "') AS awaiting,
				(SELECT COUNT(*) FROM seeker_package_request WHERE approved = 1 AND trip='" . $tripid . "') AS approved,
				(SELECT COUNT(*) FROM seeker_package_request WHERE (approved = 2 OR approved = 5) AND trip='" . $tripid . "') AS declined,
				(SELECT COUNT(*) FROM seeker_package_request WHERE (approved = 3 OR approved = 4) AND trip='" . $tripid . "') AS expired                    
				FROM seeker_package_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerProjectRequestStats($tripid)
    {
        $sql  = "SELECT  COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM seeker_project_request WHERE approved = 0 AND trip='" . $tripid . "') AS awaiting,
				(SELECT COUNT(*) FROM seeker_project_request WHERE approved = 1 AND trip='" . $tripid . "') AS approved,
				(SELECT COUNT(*) FROM seeker_project_request WHERE (approved = 2 OR approved = 5) AND trip='" . $tripid . "') AS declined,                                                
				(SELECT COUNT(*) FROM seeker_project_request WHERE (approved = 3 OR approved = 4) AND trip='" . $tripid . "') AS expired                    
				FROM seeker_project_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPeopleRequestsBySeekerStats($seeker_trip_id)
    {
        $sql = "SELECT  COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM traveller_people_request WHERE approved = 0 AND people_request ='" . $seeker_trip_id . "') AS awaiting,                                  
				(SELECT COUNT(*) FROM traveller_people_request WHERE approved = 1 AND people_request ='" . $seeker_trip_id . "') AS approved,                        
				(SELECT COUNT(*) FROM traveller_people_request WHERE (approved = 2 OR approved = 5) AND people_request ='" . $seeker_trip_id . "') AS declined,
				(SELECT COUNT(*) FROM traveller_people_request WHERE (approved = 3 OR approved = 4) AND people_request ='" . $seeker_trip_id . "') AS expired                    
				FROM traveller_people_request  WHERE people_request ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return null;
    }
    public function getTravellerPeopleRequestsByTravellerStats($seeker_trip_id)
    {
        $sql = "SELECT  COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM seeker_people_request WHERE approved = 0 AND people_request ='" . $seeker_trip_id . "') AS awaiting,                                  
				(SELECT COUNT(*) FROM seeker_people_request WHERE approved = 1 AND people_request ='" . $seeker_trip_id . "') AS approved,                        
				(SELECT COUNT(*) FROM seeker_people_request WHERE (approved = 2 OR approved = 5) AND people_request ='" . $seeker_trip_id . "') AS declined,                                                
				(SELECT COUNT(*) FROM seeker_people_request WHERE (approved = 3 OR approved = 4) AND people_request ='" . $seeker_trip_id . "') AS expired
				FROM seeker_people_request  WHERE people_request ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return null;
    }
    public function getTravellerPackageRequestsByTravellerStats($seeker_trip_id)
    {
        $sql = "SELECT  COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM seeker_package_request WHERE approved = 0 AND package_request ='" . $seeker_trip_id . "') AS awaiting,                                  
				(SELECT COUNT(*) FROM seeker_package_request WHERE approved = 1 AND package_request ='" . $seeker_trip_id . "') AS approved,                        
				(SELECT COUNT(*) FROM seeker_package_request WHERE (approved = 2 OR approved = 5) AND package_request ='" . $seeker_trip_id . "') AS declined,                                                
				(SELECT COUNT(*) FROM seeker_package_request WHERE (approved = 3 OR approved = 4) AND package_request ='" . $seeker_trip_id . "') AS expired
				FROM seeker_package_request  WHERE package_request ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return null;
    }
    public function getSeekerPackageRequestsBySeekerStats($seeker_trip_id)
    {
        $sql = "SELECT  COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM traveller_package_request WHERE approved = 0 AND package_request ='" . $seeker_trip_id . "') AS awaiting,                                  
				(SELECT COUNT(*) FROM traveller_package_request WHERE approved = 1 AND package_request ='" . $seeker_trip_id . "') AS approved,                        
				(SELECT COUNT(*) FROM traveller_package_request WHERE (approved = 2 OR approved = 5) AND package_request ='" . $seeker_trip_id . "') AS declined,                                                
				(SELECT COUNT(*) FROM traveller_package_request WHERE (approved = 3 OR approved = 4) AND package_request ='" . $seeker_trip_id . "') AS expired                    
				FROM traveller_package_request  WHERE package_request ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return null;
    }
    public function getTravellerProjectRequestsByTravellerStats($seeker_trip_id)
    {
        $sql = "SELECT  COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM seeker_project_request WHERE approved = 0 AND project_request ='" . $seeker_trip_id . "') AS awaiting,                                  
				(SELECT COUNT(*) FROM seeker_project_request WHERE approved = 1 AND project_request ='" . $seeker_trip_id . "') AS approved,                        
				(SELECT COUNT(*) FROM seeker_project_request WHERE (approved = 2 OR approved = 5) AND project_request ='" . $seeker_trip_id . "') AS declined,                                                
				(SELECT COUNT(*) FROM seeker_project_request WHERE (approved = 3 OR approved = 4) AND project_request ='" . $seeker_trip_id . "') AS expired
				FROM seeker_project_request  WHERE project_request ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return null;
    }
    public function getSeekerProjectRequestsBySeekerStats($seeker_trip_id)
    {
        $sql = "SELECT  COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM traveller_project_request WHERE approved = 0 AND project_request ='" . $seeker_trip_id . "') AS awaiting,                                  
				(SELECT COUNT(*) FROM traveller_project_request WHERE approved = 1 AND project_request ='" . $seeker_trip_id . "') AS approved,                        
				(SELECT COUNT(*) FROM traveller_project_request WHERE (approved = 2 OR approved = 5) AND project_request ='" . $seeker_trip_id . "') AS declined,                                                
				(SELECT COUNT(*) FROM traveller_project_request WHERE (approved = 3 OR approved = 4) AND project_request ='" . $seeker_trip_id . "') AS expired                    
				FROM traveller_project_request  WHERE project_request ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return null;
    }
    public function checkSeekerPeopleRequestExist($tripid, $people_request)
    {
        $sql       = "SELECT * FROM seeker_people_request WHERE trip='" . $tripid . "' AND people_request='" . $people_request . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkSeekerPackageRequestExist($tripid, $package_request)
    {
        $sql       = "SELECT * FROM seeker_package_request WHERE trip='" . $tripid . "' AND package_request='" . $package_request . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkSeekerProjectRequestExist($tripid, $project_request)
    {
        $sql       = "SELECT * FROM seeker_project_request WHERE trip='" . $tripid . "' AND project_request='" . $project_request . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkTravellerPeopleRequestExist($tripid, $people_request)
    {
        $sql       = "SELECT * FROM traveller_people_request WHERE trip='" . $tripid . "' AND people_request='" . $people_request . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkTravellerPackageRequestExist($tripid, $package_request)
    {
        $sql       = "SELECT * FROM traveller_package_request WHERE trip='" . $tripid . "' AND package_request='" . $package_request . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkTravellerProjectRequestExist($tripid, $project_request)
    {
        $sql       = "SELECT * FROM traveller_project_request WHERE trip='" . $tripid . "' AND project_request='" . $project_request . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkTravellerPeopleRequestSentAlready($traveller_tripid, $seeker_trip_id)
    {
        $sql       = "SELECT b.* FROM traveller_people as a,traveller_people_request as b WHERE b.trip='" . $seeker_trip_id . "' AND b.people_request=a.ID AND a.trip='" . $traveller_tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkTravellerPackageRequestSentAlready($traveller_tripid, $seeker_trip_id)
    {
        $sql       = "SELECT b.* FROM traveller_packages as a,traveller_package_request as b WHERE b.trip='" . $seeker_trip_id . "' AND b.package_request=a.ID AND a.trip='" . $traveller_tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkTravellerProjectRequestSentAlready($traveller_tripid, $seeker_trip_id)
    {
        $sql       = "SELECT b.* FROM traveller_projects as a,traveller_project_request as b WHERE b.trip='" . $seeker_trip_id . "' AND b.project_request=a.ID AND a.trip='" . $traveller_tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkSeekerPeopleRequestSentAlready($seeker_trip_id, $traveller_tripid)
    {
        $sql       = "SELECT b.* FROM seeker_people as a,seeker_people_request as b WHERE b.trip='" . $traveller_tripid . "' AND b.people_request=a.ID AND a.seeker_trip='" . $seeker_trip_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkSeekerPackageRequestSentAlready($seeker_trip_id, $traveller_tripid)
    {
        $sql       = "SELECT b.* FROM seeker_package as a,seeker_package_request as b WHERE b.trip='" . $traveller_tripid . "' AND b.package_request=a.ID AND a.seeker_trip='" . $seeker_trip_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function checkSeekerProjectRequestSentAlready($seeker_trip_id, $traveller_tripid)
    {
        $sql       = "SELECT b.* FROM seeker_project as a,seeker_project_request as b WHERE b.trip='" . $traveller_tripid . "' AND b.project_request=a.ID AND a.seeker_trip='" . $seeker_trip_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPeopleRequest($tripid)
    {   
        $sql       = "SELECT * FROM traveller_people_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getTravellerPackageRequest($tripid)
    {
        $sql       = "SELECT * FROM traveller_package_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getTravellerProjectRequest($tripid)
    {
        
        $sql       = "SELECT * FROM traveller_project_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getTravellerPeopleRequestStats($tripid)
    {
        $sql = "SELECT  COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM traveller_people_request WHERE approved = 0 AND trip='" . $tripid . "') AS awaiting,                                  
				(SELECT COUNT(*) FROM traveller_people_request WHERE approved = 1 AND trip='" . $tripid . "') AS approved,                        
				(SELECT COUNT(*) FROM traveller_people_request WHERE (approved = 2 OR approved = 5) AND trip='" . $tripid . "') AS declined,                                                
				(SELECT COUNT(*) FROM traveller_people_request WHERE (approved = 3 OR approved = 4) AND trip='" . $tripid . "') AS expired 
				FROM traveller_people_request AS TP JOIN trips AS T ON(T.ID = TP.people_request) JOIN users_user AS U ON (U.ID = T.user) WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPackageRequestStats($tripid)
    {
        $sql = "SELECT COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM traveller_package_request WHERE approved = 0 AND trip='" . $tripid . "') AS awaiting,                                  
				(SELECT COUNT(*) FROM traveller_package_request WHERE approved = 1 AND trip='" . $tripid . "') AS approved,                        
				(SELECT COUNT(*) FROM traveller_package_request WHERE (approved = 2 OR approved = 5) AND trip='" . $tripid . "') AS declined,
				(SELECT COUNT(*) FROM traveller_package_request WHERE (approved = 3 OR approved = 4) AND trip='" . $tripid . "') AS expired 
				FROM traveller_package_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerProjectRequestStats($tripid)
    {
        $sql = "SELECT COUNT(*) as totalCount, 
				(SELECT COUNT(*) FROM traveller_project_request WHERE approved = 0 AND trip='" . $tripid . "') AS awaiting,                                  
				(SELECT COUNT(*) FROM traveller_project_request WHERE approved = 1 AND trip='" . $tripid . "') AS approved,                        
				(SELECT COUNT(*) FROM traveller_project_request WHERE (approved = 2 OR approved = 5) AND trip='" . $tripid . "') AS declined,                                                
				(SELECT COUNT(*) FROM traveller_project_request WHERE (approved = 3 OR approved = 4) AND trip='" . $tripid . "') AS expired 
				FROM traveller_project_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function updatePaymants($arr_trip_contact)
    {
        $insert  = $this->sql->insert('user_payments');
        $newData = $arr_trip_contact;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
       
        return $ID;
    }
    
    /* aaddress insert */
    public function addTripContactAddress($arr_trip_contact)
    { 
        $insert  = $this->sql->insert('users_user_locations');
		
        $newData = $arr_trip_contact;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
       
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    public function updateTripContactAddress($arr_trip_contact, $id, $userid)
    {
        $update = $this->sql->update();
        $update->table('users_user_locations');
        $update->set($arr_trip_contact);
        $update->where(array(
            'ID' => $id,
            'user' => $userid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function updateTripFlights($arr_trip_contact, $id)
    {
        $update = $this->sql->update();
        $update->table('trips_flight');
        $update->set($arr_trip_contact);
        $update->where(array(
            'ID' => $id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function updateTripContactAddresssetdef($userid)
    {
        $update = $this->sql->update();
        $update->table('users_user_locations');
        $update->set(array(
            "set_default" => 0
        ));
        $update->where(array(
            'user' => $userid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function addTrip($trip, $userrole)
    {
        //echo '<pre>'; print_r($trip); print_r($userrole); exit;
        if ($userrole == 'seeker') {
            $insert  = $this->sql->insert('seeker_trips');
            $newData = $trip;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        else {
            $insert  = $this->sql->insert('trips');
            $newData = $trip;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        return $ID;
    }
	public function updateTripStatus($status,$id,$userrole){
		$update = $this->sql->update();
		if ($userrole == 'seeker') {
			$update->table('seeker_trips');
		} else {
			$update->table('trips');
		}
        $update->set(array(
            "trip_status" => $status
        ));
        $update->where(array(
            'ID' => $id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
	}
	public function updateSeekerTripArrivalDateFromTravelerTrip($id){
		$update = $this->sql->update();
		$update->table('seeker_trips');
        $update->set(array(
            "arrival_date" => date('Y-m-d H:i:s')
        ));
        $update->where(array(
            'ID' => $id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
	}
    public function addTripFlight($trip_flight)
    {
        $insert  = $this->sql->insert('trips_flight');
        $newData = $trip_flight;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    public function addTripPeopleDetail($people_detail)
    {
        $people_detail['is_completed'] = 1;
        
        $insert  = $this->sql->insert('traveller_people');
        $newData = $people_detail;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();

        $is_completed = [
            'is_completed' => 1
        ];

        $update = $this->sql->update();
        $update->table('trips');
        $update->set($is_completed);
        $update->where(array(
            'ID' => $people_detail['trip']
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $statement->execute();

        return $ID;
    }
    public function addTripTravellerPackage($package_detail)
    {
        $package_detail['is_completed'] = 1;
        $insert  = $this->sql->insert('traveller_packages');
        $newData = $package_detail;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();

        $is_completed = [
            'is_completed' => 1
        ];

        $update = $this->sql->update();
        $update->table('trips');
        $update->set($is_completed);
        $update->where(array(
            'ID' => $package_detail['trip']
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $statement->execute();

        return $ID;
    }
    public function addTripTravellerProject($project_detail)
    {
        $project_detail['is_completed'] = 1;

        $insert  = $this->sql->insert('traveller_projects');
        $newData = $project_detail;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();

        $is_completed = [
            'is_completed' => 1
        ];

        $update = $this->sql->update();
        $update->table('trips');
        $update->set($is_completed);
        $update->where(array(
            'ID' => $project_detail['trip']
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $statement->execute();

        return $ID;
    }
    public function getTrip($ID, $userrole = 'traveller')
    {
        /* traveller trip */
        $sql = "SELECT * FROM trips WHERE ID ='" . $ID . "' ";
        /* seeker trip */
        if ($userrole == 'seeker') {
            $sql = "SELECT * FROM seeker_trips WHERE ID ='" . $ID . "' ";
        }
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getReceiptTravellerTripId($tripID)
    {
        $sql       = "SELECT * FROM trips WHERE ID ='" . $tripID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getPeopleDetailByTripID($trip)
    {
        $sql       = "SELECT * FROM traveller_people WHERE trip ='" . $trip . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getPackageDetailByTripID($trip)
    {
        $sql       = "SELECT * FROM traveller_packages WHERE trip ='" . $trip . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    
    public function updateTrip($arr_trip_detail, $ID)
    {
        $update = $this->sql->update();
        $update->table('trips');
        $update->set($arr_trip_detail);
        $update->where(array(
            'trip_id_number' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
	
	public function updateTripById($arr_trip_detail, $ID)
    {
        $update = $this->sql->update();
        $update->table('trips');
        $update->set($arr_trip_detail);
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    
        public function updateCostTrip($arr_trip_detail, $ID)
    {
        $update = $this->sql->update();
        $update->table('trips');
        $update->set($arr_trip_detail);
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
	
	public function updateSeekerTripById($arr_trip_detail, $ID)
    {
        $update = $this->sql->update();
        $update->table('seeker_trips');
        $update->set($arr_trip_detail);
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
	
    public function updateSeekerTrip($arr_trip_detail, $ID)
    {
        $update = $this->sql->update();
        $update->table('seeker_trips');
        $update->set($arr_trip_detail);
        $update->where(array(
			'trip_id_number' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function updateTripPeopleDetail($arr_people_detail, $ID)
    {
        $update = $this->sql->update();
        $update->table('traveller_people');
        $update->set($arr_people_detail);
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function updateTripPackageDetail($arr_package_detail, $ID)
    {
        $update = $this->sql->update();
        $update->table('traveller_packages');
        $update->set($arr_package_detail);
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function deleteTrip($ID)
    {
        $sql       = "DELETE FROM trips WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
    }
    public function removewishlist($ID, $tablename)
    {
        $sql       = "DELETE FROM $tablename WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
    }
    public function deleteTripFlightDetails($ID)
    {
        $sql       = "DELETE FROM trips_flight WHERE trip='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
    }
    public function deleteTripPeopleDetail($ID)
    {
        $sql       = "DELETE FROM traveller_people WHERE trip='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
    }
    public function deleteTripPackageDetail($ID)
    {
        $sql       = "DELETE FROM traveller_packages WHERE trip='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
    }
    public function getTariffServices($ID)
    {
         $sql       = "SELECT * FROM trip_services WHERE ID ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPeopleByID($ID)
    {
        $sql       = "SELECT * FROM seeker_people WHERE ID ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPackageByID($ID)
    {
        $sql       = "SELECT * FROM seeker_package WHERE ID ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerProjectByID($ID)
    {
        $sql       = "SELECT * FROM seeker_project WHERE ID ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function deleteSeekerPeopleById($ID)
    {
        $seekerPeopleIds = false;
        /* find seeker projects from seeker trips and delete them */
        $sql             = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_people WHERE seeker_trip = " . $ID;
        $statement       = $this->adapter->query($sql);
        $res             = $statement->execute();
        if ($res->count() > 0) {
            $seekerPeopleIds = $res->current();
        }
        if (isset($seekerPeopleIds) && $seekerPeopleIds['Ids'] != '') {
            $sql       = "DELETE FROM seeker_people WHERE ID IN (" . $seekerPeopleIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            /* find seeker project tasks from seeker project and delete them */
            $sql       = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_people_passengers WHERE people_request IN (" . $seekerPeopleIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            if ($res->count() > 0) {
                $seekerPeoplePassengerIds = $res->current();
            }
            /*if (isset($seekerPeoplePassengerIds) && $seekerPeoplePassengerIds['Ids'] != '') {
                $sql       = "DELETE FROM seeker_people_passengers WHERE ID IN (" . $seekerPeoplePassengerIds['Ids'] . ")";
                $statement = $this->adapter->query($sql);
                $res       = $statement->execute();
            }*/
            /* find seeker project tasks photos from seeker project tasks and delete them */
            $sql       = "SELECT GROUP_CONCAT( seeker_attachment_id SEPARATOR  ',' ) AS Ids FROM seeker_people_passengers_attachment WHERE seeker_people_passengers_id IN (" . $seekerPeoplePassengerIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            if ($res->count() > 0) {
                $seekerPeoplePassAttachmentIds = $res->current();
            }
            if (isset($seekerPeoplePassAttachmentIds) && $seekerPeoplePassAttachmentIds['Ids'] != '') {
                $sql       = "DELETE FROM seeker_people_passengers_attachment WHERE seeker_attachment_id IN (" . $seekerPeoplePassAttachmentIds['Ids'] . ")";
                $statement = $this->adapter->query($sql);
                $res       = $statement->execute();
            }
        }
    }
    public function deleteSeekerProjectById($ID)
    {
        $seekerProjectIds = false;
        /* find seeker projects from seeker trips and delete them */
        $sql              = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_project WHERE seeker_trip = " . $ID;
        $statement        = $this->adapter->query($sql);
        $res              = $statement->execute();
        if ($res->count() > 0) {
            $seekerProjectIds = $res->current();
        }
        if (isset($seekerProjectIds) && $seekerProjectIds['Ids'] != '') {
            $sql       = "DELETE FROM seeker_project WHERE ID IN (" . $seekerProjectIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            /* find seeker project tasks from seeker project and delete them */
            $sql       = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_project_tasks WHERE project_request IN (" . $seekerProjectIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            if ($res->count() > 0) {
                $seekerProjectTasksIds = $res->current();
            }
            if (isset($seekerProjectTasksIds) && $seekerProjectTasksIds['Ids'] != '') {
                $sql       = "DELETE FROM seeker_project_tasks WHERE ID IN (" . $seekerProjectTasksIds['Ids'] . ")";
                $statement = $this->adapter->query($sql);
                $res       = $statement->execute();
            }
            /* find seeker project tasks photos from seeker project tasks and delete them */
            $sql       = "SELECT GROUP_CONCAT( task_photo_id SEPARATOR  ',' ) AS Ids FROM seeker_project_task_photos WHERE seeker_project_tasks_id IN (" . $seekerProjectTasksIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            if ($res->count() > 0) {
                $seekerProjectTasksPhotoIds = $res->current();
            }
            if (isset($seekerProjectTasksPhotoIds) && $seekerProjectTasksPhotoIds['Ids'] != '') {
                $sql       = "DELETE FROM seeker_project_task_photos WHERE task_photo_id IN (" . $seekerProjectTasksPhotoIds['Ids'] . ")";
                $statement = $this->adapter->query($sql);
                $res       = $statement->execute();
            }
        }
    }
    public function deleteSeekerPackagesById($ID)
    {
        $seekerPackageIds = false;
        /* find seeker projects from seeker trips and delete them */
        $sql              = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_package WHERE seeker_trip = " . $ID;
        $statement        = $this->adapter->query($sql);
        $res              = $statement->execute();
        if ($res->count() > 0) {
            $seekerPackageIds = $res->current();
        }
        if (isset($seekerPackageIds) && $seekerPackageIds['Ids'] != '') {
            $sql       = "DELETE FROM seeker_package WHERE ID IN (" . $seekerPackageIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            /* find seeker project tasks from seeker project and delete them */
            $sql       = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_package_packages WHERE package_request IN (" . $seekerPackageIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            if ($res->count() > 0) {
                $seekerPackagePackagesIds = $res->current();
            }
            if (isset($seekerPackagePackagesIds) && $seekerPackagePackagesIds['Ids'] != '') {
                $sql       = "DELETE FROM seeker_package_packages WHERE ID IN (" . $seekerPackagePackagesIds['Ids'] . ")";
                $statement = $this->adapter->query($sql);
                $res       = $statement->execute();
            }
        }
    }
    public function getTravellerPeopleByID($ID)
    {
        $sql       = "SELECT * FROM traveller_people WHERE ID ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPeopleByTrip($ID)
    {
        $sql       = "SELECT * FROM traveller_people WHERE trip ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPackageByID($ID)
    {
        $sql       = "SELECT * FROM traveller_packages WHERE ID ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPackageByTrip($ID)
    {
        $sql       = "SELECT * FROM traveller_packages WHERE trip ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function deleteTripProjectDetails($ID)
    {
        $sql       = "DELETE FROM traveller_projects WHERE trip ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
    }
    public function getTravellerProjectByID($ID)
    {
        $sql       = "SELECT * FROM traveller_projects WHERE ID ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerProjectByTrip($ID)
    {
        $sql       = "SELECT * FROM traveller_projects WHERE trip ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPeople($seeker_trip_id)
    {
        $sql       = "SELECT * FROM seeker_people WHERE seeker_trip ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPackage($seeker_trip_id)
    {
        $sql       = "SELECT * FROM seeker_package WHERE seeker_trip ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerProject($seeker_trip_id)
    {
        $sql       = "SELECT * FROM seeker_project WHERE seeker_trip ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function addSeekerTripByTravellerTrip($traveller_trip_id, $seeker_user_id, $seeker_user_location_id)
    {
        $sql     = "INSERT INTO seeker_trips (`user`,`user_location`,`name`,`description`,`origin_location`,`destination_location`,`departure_date`,`arrival_date`,`departure_time`,`arrival_time`,`flight_number`,`booking_status`,`cabin`,`ticket_image`,`active`,`origin_latitude`,`origin_longitude`,`created`,`modified`) SELECT $seeker_user_id,$seeker_user_location_id,`name`,`description`,`origin_location`,`destination_location`,`departure_date`,`arrival_date`,`departure_time`,`arrival_time`,`flight_number`,`booking_status`,`cabin`,`ticket_image`,`active`,`origin_latitude`,`origin_longitude`,date_format(UNIX_TIMESTAMP( ),'%Y-%m-%d %H:%i:s'),date_format(UNIX_TIMESTAMP( ),'%Y-%m-%d %H:%i:s') FROM trips WHERE ID='" . $traveller_trip_id . "'";
        $results = $this->adapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        $ID      = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    public function addTravellerTripBySeekerTrip($seeker_trip_id, $traveller_user_id, $traveller_user_location_id)
    {
        $locationId = (int) $traveller_user_location_id;
        $sql        = "INSERT INTO trips (`user`,`user_location`,`name`,`description`,`origin_location`,`destination_location`,`departure_date`,`arrival_date`,`departure_time`,`arrival_time`,`flight_number`,`booking_status`,`cabin`,`ticket_image`,`active`,`origin_latitude`,`origin_longitude`,`created`,`modified`) SELECT $traveller_user_id,$locationId,`name`,`description`,`origin_location`,`destination_location`,`departure_date`,`arrival_date`,`departure_time`,`arrival_time`,`flight_number`,`booking_status`,`cabin`,`ticket_image`,`active`,`origin_latitude`,`origin_longitude`,date_format(UNIX_TIMESTAMP( ),'%Y-%m-%d %H:%i:s'),date_format(UNIX_TIMESTAMP( ),'%Y-%m-%d %H:%i:s') FROM seeker_trips WHERE ID='" . $seeker_trip_id . "'";
        $results    = $this->adapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        $ID         = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    public function checkSeekerTripExistByTravellerTrip($traveller_trip_id, $userid)
    {
        $sql       = "SELECT * FROM trips WHERE ID='" . $traveller_trip_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            $rs = $res->current();
        } else
            $rs = false;
        if ($rs['ID']) {
            $sql       = "SELECT * FROM seeker_trips WHERE user='" . $userid . "' AND origin_location='" . $rs['origin_location'] . "' AND destination_location='" . $rs['destination_location'] . "' AND  departure_date='" . $rs['departure_date'] . "'";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            if ($res->count() > 0) {
                return $res->current();
            } else
                return false;
        } 
		
		else
            return false;
    }
    public function checkTravellerTripExistBySeekerTrip($seeker_trip_id, $userid)
    {
        $sql       = "SELECT * FROM seeker_trips WHERE ID='" . $seeker_trip_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            $rs = $res->current();
        } else
            $rs = false;
        if ($rs['ID']) {
            $sql       = "SELECT * FROM trips WHERE user='" . $userid . "' AND origin_location='" . $rs['origin_location'] . "' AND destination_location='" . $rs['destination_location'] . "' AND  departure_date='" . $rs['departure_date'] . "'";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            if ($res->count() > 0) {
                return $res->current();
            } else
                return false;
        } else
            return false;
    }
    public function addSeekerPeople($arr_seeker_people_request)
    {
        $insert  = $this->sql->insert('seeker_people');
        $newData = $arr_seeker_people_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    public function addSeekerPackage($arr_seeker_package_request)
    {
        $insert  = $this->sql->insert('seeker_package');
        $newData = $arr_seeker_package_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    public function addSeekerProject($arr_seeker_project_request)
    {
        $insert  = $this->sql->insert('seeker_project');
        $newData = $arr_seeker_project_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    public function addSeekerPeoplePassengers($arr_seeker_people_passengers)
    {
        $insert  = $this->sql->insert('seeker_people_passengers');
        $newData = $arr_seeker_people_passengers;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    public function addSeekerPackagePackages($arr_seeker_package_packages)
    {
        $insert  = $this->sql->insert('seeker_package_packages');
        $newData = $arr_seeker_package_packages;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    public function addSeekerProjectTasks($arr_seeker_project_tasks)
    {
        $insert  = $this->sql->insert('seeker_project_tasks');
        $newData = $arr_seeker_project_tasks;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    public function getUserPrimaryLocation($user_id)
    {
        $sql       = "SELECT * FROM users_user_locations WHERE set_default='1' AND user ='" . $user_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function addTripSeekerPeopleRequest($arr_trip_seeker_people_request)
    {
        $insert  = $this->sql->insert('seeker_people_request');
        $newData = $arr_trip_seeker_people_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();

		$seeker_people = $this->getSeekerPeopleByID($arr_trip_seeker_people_request['people_request']);
		$whislist_data = array(
							'trip_id' => $arr_trip_seeker_people_request['trip'],
							'seeker_trip_id' => $seeker_people['seeker_trip'],
							'request_id' => $ID,
							'request_type' => 'people',
							'is_wishlist' => 1,
							'created_on' => date('Y-m-d H:i:s')
						);
		$this->addTripSeekerRequestWishlist($whislist_data);
		
        return $ID;
    }
    public function addTripSeekerPackageRequest($arr_trip_seeker_package_request)
    {
        $insert  = $this->sql->insert('seeker_package_request');
        $newData = $arr_trip_seeker_package_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
		
		$seeker_package = $this->getSeekerPackageByID($arr_trip_seeker_package_request['package_request']);
		$whislist_data = array(
							'trip_id' => $arr_trip_seeker_package_request['trip'],
							'seeker_trip_id' => $seeker_package['seeker_trip'],
							'request_id' => $ID,
							'request_type' => 'package',
							'is_wishlist' => 1,
							'created_on' => date('Y-m-d H:i:s')
						);
		$this->addTripSeekerRequestWishlist($whislist_data);
		
        return $ID;
    }
    public function addTripSeekerProjectRequest($arr_trip_seeker_project_request)
    {
        $insert  = $this->sql->insert('seeker_project_request');
        $newData = $arr_trip_seeker_project_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
		
		$seeker_project = $this->getSeekerProjectByID($arr_trip_seeker_project_request['project_request']);
		$whislist_data = array(
							'trip_id' => $arr_trip_seeker_project_request['trip'],
							'seeker_trip_id' => $seeker_project['seeker_trip'],
							'request_id' => $ID,
							'request_type' => 'project',
							'is_wishlist' => 1,
							'created_on' => date('Y-m-d H:i:s')
						);
		$this->addTripSeekerRequestWishlist($whislist_data);
		
        return $ID;
    }
    public function addTripTravellerPeopleRequest($arr_trip_traveller_people_request)
    {
        $insert  = $this->sql->insert('traveller_people_request');
        $newData = $arr_trip_traveller_people_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
		
		$traveller_people = $this->getTravellerPeopleByID($arr_trip_traveller_people_request['people_request']);
		$whislist_data = array(
							'trip_id' => $traveller_people['trip'],
							'seeker_trip_id' => $arr_trip_traveller_people_request['trip'],
							'request_id' => $ID,
							'request_type' => 'people',
							'is_wishlist' => 1,
							'created_on' => date('Y-m-d H:i:s')
						);
		$this->addTripTravellerRequestWishlist($whislist_data);
		
        return $ID;
    }
    public function addTripTravellerPackageRequest($arr_trip_traveller_package_request)
    {
        $insert  = $this->sql->insert('traveller_package_request');
        $newData = $arr_trip_traveller_package_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();

		$traveller_package = $this->getTravellerPackageByID($arr_trip_traveller_package_request['package_request']);
		$whislist_data = array(
							'trip_id' => $traveller_package['trip'],
							'seeker_trip_id' => $arr_trip_traveller_package_request['trip'],
							'request_id' => $ID,
							'request_type' => 'package',
							'is_wishlist' => 1,
							'created_on' => date('Y-m-d H:i:s')
						);
		$this->addTripTravellerRequestWishlist($whislist_data);

        return $ID;
    }
    public function addTripTravellerProjectRequest($arr_trip_traveller_project_request)
    {
        $insert  = $this->sql->insert('traveller_project_request');
        $newData = $arr_trip_traveller_project_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
		
		$traveller_project = $this->getTravellerProjectByID($arr_trip_traveller_project_request['project_request']);
		$whislist_data = array(
							'trip_id' => $traveller_project['trip'],
							'seeker_trip_id' => $arr_trip_traveller_project_request['trip'],
							'request_id' => $ID,
							'request_type' => 'project',
							'is_wishlist' => 1,
							'created_on' => date('Y-m-d H:i:s')
						);
		$this->addTripTravellerRequestWishlist($whislist_data);
		
        return $ID;
    }
	public function addTripTravellerRequestWishlist($arr_traveller_wishlist)
    {
        $insert  = $this->sql->insert('traveller_request_wishlist');
        $newData = $arr_traveller_wishlist;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
	public function addTripSeekerRequestWishlist($arr_traveller_wishlist)
    {
        $insert  = $this->sql->insert('seeker_request_wishlist');
        $newData = $arr_traveller_wishlist;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
	public function updateTripSeekerRequestWishlist($tripid,$requestid,$service){
		$update = $this->sql->update();
        $update->table('seeker_request_wishlist');
        $update->set(array(
            'is_wishlist' => 0
        ));
        $update->where(array(
            'trip_id' => $tripid,
            'request_id' => $requestid,
			'request_type'=> $service
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
        if($results)
			return true;
		else
			return false;
	}
	public function updateTripTravellerRequestWishlist($tripid,$requestid,$service){
		$update = $this->sql->update();
        $update->table('traveller_request_wishlist');
        $update->set(array(
            'is_wishlist' => 0
        ));
        $update->where(array(
            'seeker_trip_id' => $tripid,
            'request_id' => $requestid,
			'request_type'=> $service
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
        if($results)
			return true;
		else
			return false;
	}
	public function getTripRequestWishlist($tripid,$userType)
    {
		$tName 		= $userType == 'seeker'?'traveller_request_wishlist':'seeker_request_wishlist';
		$tripFields	= $userType == 'seeker'?'seeker_trip_id':'trip_id';
		
        $sql       	= "SELECT * FROM ".$tName." WHERE ".$tripFields." ='" . $tripid . "' AND is_wishlist = 1 ";
        $statement 	= $this->adapter->query($sql);
        $res       	= $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return array();
    }
    public function getTripPeopleRequests($tripid)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        $sql       = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,d.*,b.*,c.departure_date,c.trip_status $ratingQry FROM seeker_people_request as a, seeker_people as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.people_request AND a.trip ='" . $tripid . "'  ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return null;
    }   
    public function getTripPackageRequests($tripid)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        $sql       = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,d.*,b.*,c.departure_date,c.trip_status $ratingQry  FROM seeker_package_request as a, seeker_package as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.package_request AND a.trip ='" . $tripid . "'  ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return null;
    }
    public function getTripProjectRequests($tripid)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        $sql       = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,d.*,b.*,c.departure_date,c.trip_status $ratingQry FROM seeker_project_request as a, seeker_project as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.project_request AND a.trip ='" . $tripid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            $returnArray = array();
            foreach ($res as $result) {
                $resultArray[] = $result;
            }
            return $resultArray;
        } else
            return null;
    }
	public function updateTravellerTripIdInSeeker($seekertripid, $requestid,$type){
		$data = $this->getTravellerRequestInfo($requestid,$type);
		if($type == 'people'){
			$tripInfo = $this->getTravellerPeopleByID($data['people_request']);
		} else if($type == 'package'){
			$tripInfo = $this->getTravellerPackageByID($data['package_request']);
		} else if($type == 'project'){
			$tripInfo = $this->getTravellerProjectByID($data['project_request']);
		}
		if(isset($tripInfo['trip'])){
			$update = $this->sql->update();
			$update->table('seeker_trips');
			$update->set(array(
				'traveller_trip_id' => $tripInfo['trip']
			));
			$update->where(array(
				'ID' => $seekertripid
			));
			$statement = $this->sql->prepareStatementForSqlObject($update);
			$results   = $statement->execute();
		}
	}
	public function getTravellerRequestInfo($reqId,$type){
		
		if($type == 'people'){
			$table = 'traveller_people_request';
		} else if($type == 'package'){
			$table = 'traveller_package_request';
		} else if($type == 'project'){
			$table = 'traveller_project_request';
		}
		
		$sql       = "SELECT * FROM ".$table." WHERE ID ='" . $reqId . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
	}
	public function updateTravellerTripIdInSeeker_traveller($tripid, $requestid,$type){
		$data = $this->getSeekerRequestInfo($requestid,$type);
		if($type == 'people'){
			$tripInfo = $this->getSeekerPeopleByID($data['people_request']);
		} else if($type == 'package'){
			$tripInfo = $this->getSeekerPackageByID($data['package_request']);
		} else if($type == 'project'){
			$tripInfo = $this->getSeekerProjectByID($data['project_request']);
		}
		if(isset($tripInfo['seeker_trip'])){
			$update = $this->sql->update();
			$update->table('seeker_trips');
			$update->set(array(
				'traveller_trip_id' => $tripid
			));
			$update->where(array(
				'ID' => $tripInfo['seeker_trip']
			));
			$statement = $this->sql->prepareStatementForSqlObject($update);
			$results   = $statement->execute();
		}
	}
	public function getSeekerRequestInfo($reqId,$type){
		if($type == 'people'){
			$table = 'seeker_people_request';
		} else if($type == 'package'){
			$table = 'seeker_package_request';
		} else if($type == 'project'){
			$table = 'seeker_project_request';
		}
		
		$sql       = "SELECT * FROM ".$table." WHERE ID ='" . $reqId . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
	}
    public function approveTravellerPeopleRequest($tripid, $requestid, $status)
    {
        /** Expire other trip request - start **/
		if($status == 1){
			$update = $this->sql->update();
			$update->table('traveller_people_request');
			$update->set(array(
				'approved' => 4
			));
			$update->where(array(
				'trip' => $tripid
			));
			$statement = $this->sql->prepareStatementForSqlObject($update);
			$results   = $statement->execute();
			
			$travellerItem = $this->getSeekerPeople($tripid); // Traveller requests
			if(isset($travellerItem['ID'])){
				$itemId = $travellerItem['ID'];
				
				$update = $this->sql->update();
				$update->table('seeker_people_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'people_request' => $itemId
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
	
			$data = $this->getTravellerRequestInfo($requestid,'people');
			$tripInfo = $this->getTravellerPeopleByID($data['people_request']);
			if(isset($tripInfo['trip'])){
				$update = $this->sql->update();
				$update->table('seeker_people_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'trip' => $tripInfo['trip']
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
		}
		/** Expire other trip request - end **/
		
        $update = $this->sql->update();
        $update->table('traveller_people_request');
        $update->set(array(
            'approved' => $status
        ));
        $update->where(array(
            'trip' => $tripid,
            'ID' => $requestid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();

		$this->updateTripTravellerRequestWishlist($tripid,$requestid,'people');		
		$this->updateTravellerTripIdInSeeker($tripid,$requestid,'people');
		
        if($results)
        	return true;
	    else
        	return false;
    }
    public function approveTravellerPackageRequest($tripid, $requestid, $status)
    {
		/** Expire other trip request - start **/
		if($status == 1){
			$update = $this->sql->update();
			$update->table('traveller_package_request');
			$update->set(array(
				'approved' => 4
			));
			$update->where(array(
				'trip' => $tripid
			));
			$statement = $this->sql->prepareStatementForSqlObject($update);
			$results   = $statement->execute();
			
			$travellerItem = $this->getSeekerPackage($tripid); // Traveller requests
			if(isset($travellerItem['ID'])){
				$itemId = $travellerItem['ID'];
				
				$update = $this->sql->update();
				$update->table('seeker_package_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'package_request' => $itemId
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
			
			$data = $this->getTravellerRequestInfo($requestid,'package');
			$tripInfo = $this->getTravellerPackageByID($data['package_request']);
			if(isset($tripInfo['trip'])){
				$update = $this->sql->update();
				$update->table('seeker_package_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'trip' => $tripInfo['trip']
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
		}
		/** Expire other trip request - end **/
		
        $update = $this->sql->update();
        $update->table('traveller_package_request');
        $update->set(array(
            'approved' => $status
        ));
        $update->where(array(
            'trip' => $tripid,
            'ID' => $requestid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
		
		$this->updateTripTravellerRequestWishlist($tripid,$requestid,'package');
		$this->updateTravellerTripIdInSeeker($tripid,$requestid,'package');
		
		if($results)
        	return true;
	    else
        	return false;
    }
    public function approveTravellerProjectRequest($tripid, $requestid, $status)
    {
		/** Expire other trip request - start **/
		if($status == 1){
			$update = $this->sql->update();
			$update->table('traveller_project_request');
			$update->set(array(
				'approved' => 4
			));
			$update->where(array(
				'trip' => $tripid
			));
			$statement = $this->sql->prepareStatementForSqlObject($update);
			$results   = $statement->execute();
			
			$travellerItem = $this->getSeekerProject($tripid); // Traveller requests
			if(isset($travellerItem['ID'])){
				$itemId = $travellerItem['ID'];
				
				$update = $this->sql->update();
				$update->table('seeker_project_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'project_request' => $itemId
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
			
			$data = $this->getTravellerRequestInfo($requestid,'project');
			$tripInfo = $this->getTravellerProjectByID($data['project_request']);
			if(isset($tripInfo['trip'])){
				$update = $this->sql->update();
				$update->table('seeker_project_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'trip' => $tripInfo['trip']
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
		}
		/** Expire other trip request - end **/
		
        $update = $this->sql->update();
        $update->table('traveller_project_request');
        $update->set(array(
            'approved' => $status
        ));
        $update->where(array(
            'trip' => $tripid,
            'ID' => $requestid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();

        $this->updateTripTravellerRequestWishlist($tripid,$requestid,'project');
		$this->updateTravellerTripIdInSeeker($tripid,$requestid,'project');
		
		if($results)
            return true;
        else
            return false;
    }
    public function approvePeopleRequest($tripid, $requestid, $status)
    {
        /** Expire other trip request - start **/
		if($status == 1){
			$update = $this->sql->update();
			$update->table('seeker_people_request');
			$update->set(array(
				'approved' => 4
			));
			$update->where(array(
				'trip' => $tripid
			));
			$statement = $this->sql->prepareStatementForSqlObject($update);
			$results   = $statement->execute();
			
			$travellerItem = $this->getTripPeople($tripid); // Traveller requests
			if(isset($travellerItem['ID'])){
				$itemId = $travellerItem['ID'];
				
				$update = $this->sql->update();
				$update->table('traveller_people_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'people_request' => $itemId
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
			
			$data = $this->getSeekerRequestInfo($requestid,'people');
			$tripInfo = $this->getSeekerPeopleByID($data['people_request']);
			if(isset($tripInfo['seeker_trip'])){
				$update = $this->sql->update();
				$update->table('traveller_people_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'trip' => $tripInfo['seeker_trip']
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
		}
		/** Expire other trip request - end **/
		
		$update = $this->sql->update();
        $update->table('seeker_people_request');
        $update->set(array(
            'approved' => $status
        ));
        $update->where(array(
            'trip' => $tripid,
            'ID' => $requestid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
		
		$this->updateTripSeekerRequestWishlist($tripid,$requestid,'people');
		$this->updateTravellerTripIdInSeeker_traveller($tripid,$requestid,'people');
		
		if($results)
            return true;
        else
            return false;
    }
    public function approvePackageRequest($tripid, $requestid, $status)
    { 
		/** Expire other trip request - start **/
		if($status == 1){
			$update = $this->sql->update();
			$update->table('seeker_package_request');
			$update->set(array(
				'approved' => 4
			));
			$update->where(array(
				'trip' => $tripid
			));
			$statement = $this->sql->prepareStatementForSqlObject($update);
			$results   = $statement->execute();
			
			$travellerItem = $this->getTripPackage($tripid); // Traveller requests
			if(isset($travellerItem['ID'])){
				$itemId = $travellerItem['ID'];
				
				$update = $this->sql->update();
				$update->table('traveller_package_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'package_request' => $itemId
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
			
			$data = $this->getSeekerRequestInfo($requestid,'package');
			$tripInfo = $this->getSeekerPackageByID($data['package_request']);
			if(isset($tripInfo['seeker_trip'])){
				$update = $this->sql->update();
				$update->table('traveller_package_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'trip' => $tripInfo['seeker_trip']
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
		}
		/** Expire other trip request - end **/
		
		$update = $this->sql->update();
        $update->table('seeker_package_request');
		$update->set(array(
            'approved' => $status
        ));
        $update->where(array(
            'trip' => $tripid,
            'ID' => $requestid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
		
		$this->updateTripSeekerRequestWishlist($tripid,$requestid,'package');
		$this->updateTravellerTripIdInSeeker_traveller($tripid,$requestid,'package');
		
		if($results)
            return true;
        else
            return false;
    }
    public function approveProjectRequest($tripid, $requestid, $status)
    {
		/** Expire other trip request - start **/
		if($status == 1){
			$update = $this->sql->update();
			$update->table('seeker_project_request');
			$update->set(array(
				'approved' => 4
			));
			$update->where(array(
				'trip' => $tripid
			));
			$statement = $this->sql->prepareStatementForSqlObject($update);
			$results   = $statement->execute();
			
			$travellerItem = $this->getTripProject($tripid); // Traveller requests
			if(isset($travellerItem['ID'])){
				$itemId = $travellerItem['ID'];
				
				$update = $this->sql->update();
				$update->table('traveller_project_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'project_request' => $itemId
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
			
			$data = $this->getSeekerRequestInfo($requestid,'project');
			$tripInfo = $this->getSeekerProjectByID($data['project_request']);
			if(isset($tripInfo['seeker_trip'])){
				$update = $this->sql->update();
				$update->table('traveller_people_request');
				$update->set(array(
					'approved' => 4
				));
				$update->where(array(
					'trip' => $tripInfo['seeker_trip']
				));
				$statement = $this->sql->prepareStatementForSqlObject($update);
				$results   = $statement->execute();
			}
		}
		/** Expire other trip request - end **/
		
		$update = $this->sql->update();
        $update->table('seeker_project_request');
        $update->set(array(
            'approved' => $status
        ));
        $update->where(array(
            'trip' => $tripid,
            'ID' => $requestid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
		
		$this->updateTripSeekerRequestWishlist($tripid,$requestid,'project');
		$this->updateTravellerTripIdInSeeker_traveller($tripid,$requestid,'project');
		
		if($results)
            return true;
        else
            return false;
    }
	public function getTravellerRequestWishlist($tripid,$tripServiceType){

		$tripServiceType = strtolower($tripServiceType);

		$requestStatus = array('status_cnt'=>0);
		if($tripServiceType == 'people'){	
			$trip_people	= $this->getTravellerPeopleByTrip($tripid);
			$requestStatus = $this->getTravellersRequestStatus($trip_people['ID'],'people',4);
		} else if($tripServiceType == 'package'){
			$trip_package	= $this->getTravellerPackageByTrip($tripid);
			$requestStatus = $this->getTravellersRequestStatus($trip_package['ID'],'package',4);
		} else if($tripServiceType == 'project'){
			$trip_project	= $this->getTravellerProjectByTrip($tripid);
			$requestStatus = $this->getTravellersRequestStatus($trip_project['ID'],'project',4);
		}
		
		$total_cnt = 0;
		$pending = $this->getSeekerTripRequestStatus($tripid,4);
		if(count($pending) > 0 && ($pending['pe_re'] > 0 || $pending['pr_re'] > 0 || $pending['pa_re'] > 0 )){
			$total_cnt = $pending['pe_re']+$pending['pr_re']+$pending['pa_re'];
		}
		$total_cnt = $total_cnt+$requestStatus['status_cnt'];
		return $total_cnt;
	}
	public function getSeekerRequestWishlist($tripid,$tripServiceType){
		
		$tripServiceType = strtolower($tripServiceType);
		
		$requestStatus = array('status_cnt'=>0);
		if($tripServiceType == 'people'){
			$trip_people	= $this->getSeekerPeople($tripid);
			$requestStatus = $this->getSeekersRequestStatus($trip_people['ID'],'people',4);
		} else if($tripServiceType == 'package'){
			$trip_package	= $this->getSeekerPackage($tripid);
			$requestStatus = $this->getSeekersRequestStatus($trip_package['ID'],'package',4);
		} else if($tripServiceType == 'project'){
			$trip_project	= $this->getSeekerProject($tripid);
			$requestStatus = $this->getSeekersRequestStatus($trip_project['ID'],'project',4);
		}
		
		$total_cnt = 0;
		$pending = $this->getTravellerTripRequestStatus($tripid,0);
		if(count($pending) > 0 && ($pending['pe_re'] > 0 || $pending['pr_re'] > 0 || $pending['pa_re'] > 0 )){
			$total_cnt = $pending['pe_re']+$pending['pr_re']+$pending['pa_re'];
		}
		$total_cnt = $total_cnt+$requestStatus['status_cnt'];
		return $total_cnt;
	}
    public function getApprovedTripPeopleRequestUser($requestid)
    {
        $sql       = "SELECT a.ID as requestid,d.*,a.card_id,a.service_id as serviceId,b.people_service_fee,b.seeker_trip as tripId FROM seeker_people_request as a, seeker_people as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.people_request AND a.ID='" . $requestid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getApprovedTripPackageRequestUser()
    {
        $sql       = "SELECT a.ID as requestid,d.*,a.card_id,b.package_service_fee ,b.seeker_trip as tripId FROM seeker_package_request as a, seeker_package as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.package_request AND a.approved='1' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTripPackageRequestUser($requestid)
    {
        $sql       = "SELECT a.ID as requestid,d.*,a.card_id,a.service_id as serviceId,b.package_service_fee,a.traveller_package_amount ,b.seeker_trip as tripId FROM seeker_package_request as a, seeker_package as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.package_request AND a.ID='" . $requestid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getApprovedTripProjectRequestUser()
    {
        $sql       = "SELECT a.ID as requestid,d.* FROM seeker_project_request as a, seeker_project as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.project_request AND a.approved='1' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTripProjectRequestUser($requestid)
    {
        $sql       = "SELECT a.ID as requestid,d.*,a.card_id ,a.service_id as serviceId,b.project_service_fee,c.ID tripId  FROM seeker_project_request as a, seeker_project as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.ID=a.project_request AND a.ID='" . $requestid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerTrip($ID)
    {
        $sql       = "SELECT * FROM seeker_trips WHERE ID ='" . $ID . "' ";
        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerTripPaymentById($pay_id)
    {
        $sql       = "SELECT * FROM user_payments WHERE pay_id='" . $pay_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerTripPeople($tripid)
    {
        $sql       = "SELECT * FROM seeker_people WHERE seeker_trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerTripPeoplePassengers($people_request_id)
    {
        $sql       = "SELECT * FROM seeker_people_passengers WHERE people_request='" . $people_request_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getSeekerTripPeoplePassengersAttachment($passenger_id)
    {
        $sql       = "SELECT * FROM seeker_people_passengers_attachment WHERE seeker_people_passengers_id='" . $passenger_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getSeekerTripPackage($tripid)
    {
        $sql       = "SELECT * FROM seeker_package WHERE seeker_trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerTripPackagePackages($package_request_id)
    {
        $sql       = "SELECT *  FROM seeker_package_packages                                      WHERE package_request='" . $package_request_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getTripProject($tripid)
    {
        $sql       = "SELECT *,P.ID,PC.catid AS catId,PC.ID AS subCatId, PCT.catid AS product_catId,PCT.ID AS product_subCatId FROM traveller_projects AS P LEFT JOIN project_sub_category AS PC ON (PC.ID = P.task) LEFT JOIN project_sub_category AS PCT ON (PCT.ID = P.product_type) WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return null;
    }
    public function getSeekerTripProject($tripid)
    {
        $sql       = "SELECT * FROM seeker_project WHERE seeker_trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerTripProjectTasks($project_request_id)
    {
        $sql       = "SELECT *, P.ID as catId,PC.ID as subCatId, SPP.ID as project_task_id FROM seeker_project_tasks AS SPP                    LEFT JOIN project_category AS P ON (P.ID = SPP.category)                    LEFT JOIN project_sub_category AS PC ON (PC.ID = SPP.additional_requirements_category)                WHERE  project_request='" . $project_request_id . "'";
      
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getSeekerTripProjectTaskAttachment($project_task_id)
    {
        $sql       = "SELECT * FROM seeker_project_task_photos WHERE seeker_project_tasks_id='" . $project_task_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getTravellerPeople($traveller_trip_id)
    {
        $sql       = "SELECT * FROM traveller_people WHERE trip ='" . $traveller_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPackage($traveller_trip_id)
    {
        $sql       = "SELECT * FROM traveller_packages WHERE trip ='" . $traveller_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerProject($traveller_trip_id)
    {
        $sql       = "SELECT * FROM traveller_projects WHERE trip ='" . $traveller_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPeopleRequests($traveller_trip_id)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        $sql       = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,d.*,b.*,c.ID as traveller_trip_id,c.departure_date $ratingQry FROM traveller_people_request as a, traveller_people as b,trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip AND b.ID=a.people_request AND a.trip ='" . $traveller_trip_id . "' "; 
        //echo $sql;die;
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return null;
    }
    public function getTravellerPeopleRequestsByTraveller($traveller_trip_id)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        //$sql       = "SELECT a.ID as requestid,a.approved,d.*,b.* $ratingQry FROM traveller_people_request as a, traveller_people as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip AND b.ID=a.people_request AND a.people_request ='" . $traveller_trip_id . "' ";
        $sql = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,d.*,b.*,c.departure_date,c.trip_status $ratingQry FROM traveller_people_request as a, seeker_people as b,seeker_trips as c,users_user as d WHERE d.ID=c.user AND c.ID=b.seeker_trip AND b.seeker_trip=a.trip AND a.people_request ='" . $traveller_trip_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return null;
    }
    public function getTravellerPackageRequestsByTraveller($traveller_trip_id)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        //$sql       = "SELECT a.ID as requestid,a.approved,d.*,b.* $ratingQry FROM traveller_package_request as a, traveller_packages as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip AND b.ID=a.package_request AND a.package_request ='" . $traveller_trip_id . "' ";
       $sql ="SELECT a.ID as requestid,a.modified AS requestDate,a.approved,d.*,b.*,spp.*,c.departure_date,c.trip_status $ratingQry FROM traveller_package_request as a, seeker_package as b,seeker_trips as c,users_user as d , seeker_package_packages as spp  WHERE d.ID=c.user AND c.ID=b.seeker_trip AND b.seeker_trip=a.trip AND b.ID=spp.package_request AND a.package_request ='" . $traveller_trip_id . "'";
       // echo $sql;die;
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return null;
    }
    public function getTravellerProjectRequestsByTraveller($traveller_trip_id)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
       // $sql       = "SELECT a.ID as requestid,a.approved,d.*,b.* $ratingQry FROM traveller_project_request as a, traveller_projects as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip AND b.ID=a.project_request AND a.project_request ='" . $traveller_trip_id . "' ";
        $sql       = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,d.*,b.*,c.departure_date,c.trip_status $ratingQry FROM traveller_project_request as a, seeker_project as b,seeker_trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.seeker_trip AND b.seeker_trip=a.trip AND a.project_request ='" . $traveller_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return null;
    }
    public function getSeekerPeopleRequestsBySeeker($traveller_trip_id)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        $sql       = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,a.trip as traveller_trip_id,d.*,b.*,c.departure_date $ratingQry FROM seeker_people_request as a, seeker_people as b,trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=a.trip AND b.ID=a.people_request AND a.people_request ='" . $traveller_trip_id . "' "; 
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return null;
    }
    public function getSeekerProjectRequestsBySeeker($traveller_trip_id)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        $sql       = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,a.trip as traveller_trip_id,d.*,b.*,c.departure_date $ratingQry FROM seeker_project_request as a, seeker_project as b,trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=a.trip AND b.ID=a.project_request AND a.project_request ='" . $traveller_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return null;
    }
    public function getSeekerPackageRequestsBySeeker($traveller_trip_id)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        
		$sql  = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,d.*,b.* $ratingQry,a.trip as traveller_trip_id FROM seeker_package_request as a, seeker_package as b,trips as c,users_user as d WHERE d.ID=c.user AND c.ID=a.trip AND b.ID=a.package_request AND a.package_request ='" . $traveller_trip_id . "' ";
		
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return null;
    }
    public function getTravellerPackageRequests($traveller_trip_id)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        $sql       = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,d.*,b.*,c.ID as traveller_trip_id,c.departure_date $ratingQry FROM traveller_package_request as a, traveller_packages as b,trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip AND b.ID=a.package_request AND a.trip =" . $traveller_trip_id . " ";

        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return null;
        }
    }
    public function getTravellerProjectRequests($traveller_trip_id)
    {
        $ratingQry = ' , (SELECT ROUND(AVG(rating),2) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratings, (SELECT COUNT(*) FROM users_reviews AS r WHERE d.ID = r.recevier_id) AS ratingsCount ';
        $sql       = "SELECT a.ID as requestid,a.modified AS requestDate,a.approved,d.*,b.*,c.ID as traveller_trip_id,c.departure_date $ratingQry FROM traveller_project_request as a, traveller_projects as b,trips as c,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip AND b.ID=a.project_request AND a.trip ='" . $traveller_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return null;
    }
    public function getSeekerTripUser($tripid)
    {
        $sql       = "SELECT b.* FROM seeker_trips as a,users_user as b WHERE  b.ID=a.user AND a.ID='" . $tripid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getFlightsDetails($trip_id,$userrole)
    {
        $sql       = "SELECT * FROM trips_flight WHERE trip='" . $trip_id . "' AND user_role = '".$userrole."' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getLastFlightsRowDetails($trip_id, $arrival)
    {
        $sql       = "SELECT * FROM trips_flight WHERE trip='" . $trip_id . "' AND arrival = '" . $arrival . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getFlightRow($trip_id)
    {
        $sql       = "SELECT * FROM trips_flight WHERE ID='" . $trip_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getFlightSeekerRow($trip_id)
    {
        $sql       = "SELECT * FROM trips_flight WHERE trip='" . $trip_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    public function getTravellerPeopleRequestUser($requestid)
    {
        $sql       = "SELECT d.*,a.card_id,a.service_id AS serviceId,c.ID as tripId FROM traveller_people_request as a,traveller_people as b, trips as c ,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip  AND b.ID=a.people_request AND a.ID='" . $requestid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPackageRequestUser($requestid)
    {
        $sql       = "SELECT d.*,a.card_id ,a.service_id AS serviceId,c.ID as tripId FROM traveller_package_request as a,traveller_packages as b, trips as c ,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip  AND b.ID=a.package_request AND a.ID='" . $requestid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerProjectRequestUser($requestid)
    {
        $sql       = "SELECT d.*,a.service_id AS serviceId,c.ID as tripId FROM traveller_project_request as a,traveller_projects as b, trips as c ,users_user as d WHERE  d.ID=c.user AND c.ID=b.trip  AND b.ID=a.project_request AND a.ID='" . $requestid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerTravelPlans()
    {
    }
    public function travellregdetcnt($userid)
    {
        $sql       = "SELECT COUNT(*) AS count FROM trips WHERE user='" . (int) $userid . "' AND active='1' AND departure_date>='" . date('Y-m-d H:i:s') . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            $result = $res->current();
            return $result['count'];
        } else
            return false;
    }
    public function travellregdet($userid, $offset, $rec_limit)
    {
        $sql       = "SELECT * FROM trips WHERE user='" . (int) $userid . "' AND active='1' AND departure_date>='" . date('Y-m-d H:i:s') . "' LIMIT $offset, $rec_limit";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function travellerservicetrip($tripid)
    {
        $retvm     = '';
        $sql       = "SELECT ID FROM traveller_people_request WHERE trip='" . $tripid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        $result    = $res->current();
        if ($result['ID'] > 0) {
            $retvm = "People";
        }
        $sqld      = "SELECT ID FROM traveller_package_request WHERE trip='" . $tripid . "' ";
        $statement = $this->adapter->query($sqld);
        $res       = $statement->execute();
        $resultd   = $res->current();
        if ($resultd['ID'] > 0) {
            if ($retvm == '') {
                $retvm = "Package";
            } else {
                $retvm = $retvm . ",Package";
            }
        }
        $sqldv     = "SELECT ID FROM traveller_project_request WHERE trip='" . $tripid . "' ";
        $statement = $this->adapter->query($sqldv);
        $res       = $statement->execute();
        $resultdv  = $res->current();
        if ($resultdv['ID'] > 0) {
            if ($retvm == '') {
                $retvm = "Product";
            } else {
                $retvm = $retvm . ",Product";
            }
        }
        return $retvm;
    }
    public function addAirports($airport)
    {
        $insert  = $this->sql->insert('core_airports');
        $newData = core_airports;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $airPortid    = $this->adapter->getDriver()->getLastGeneratedValue();
    }
    public function getFlightSearchResults($search_id)
    {
        $ch = curl_init('http://api.travelpayouts.com/v1/flight_search_results?uuid=' . $search_id);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json'
        ));
        $result = curl_exec($ch);
        $result = json_decode($result);
        if ($result[1]->search_id) {
            return $result;
        } else {
            $this->getFlightSearchResults($search_id);
        }
    }
    /* Trip */
    public function getTravelTrips($status, $userrole = '', $userId, $idEncrypted = true)
    {
        $trips = $this->getTrips($status, $userrole, $userId);
        if ($trips) {
            foreach ($trips as $trip) {
                if ($userrole == 'seeker') {
                    $trip_people          = $this->getSeekerPeople($trip['ID']);
                    $trip_package         = $this->getSeekerPackage($trip['ID']);
                    $trip_project         = $this->getSeekerProject($trip['ID']);
                    /*** These are requests are sent by Traveller for Seeker's TRIP ****/
                    $trip_people_request  = count($this->getTravellerPeopleRequest($trip['ID']));
                    $trip_package_request = count($this->getTravellerPackageRequest($trip['ID']));
                    $trip_project_request = count($this->getTravellerProjectRequest($trip['ID']));
                    /*** These are requests are sent by Traveller for Seeker's TRIP ****/
                } elseif ($userrole == 'traveller') {
                    $trip_people          = $this->getTravellerPeople($trip['ID']);
                    $trip_package         = $this->getTravellerPackage($trip['ID']);
                    $trip_project         = $this->getTravellerProject($trip['ID']);
                    /*** These are requests are sent by Seeker for traveller's TRIP ****/
                    $trip_people_request  = count($this->getSeekerPeopleRequest($trip['ID']));
                    $trip_package_request = count($this->getSeekerPackageRequest($trip['ID']));
                    $trip_project_request = count($this->getSeekerProjectRequest($trip['ID']));
                    /*** These are requests are sent by Seeker for traveller's TRIP ****/
                }
                $service = '';
                if ($trip_people['ID'])
                    $service = 'People';
                if ($trip_package['ID']) {
                    if ($service)
                        $service .= ',';
                    $service .= 'Package';
                }
                if ($trip_project['ID']) {
                    if ($service)
                        $service .= ',';
                    $service .= 'Product';
                }
                $origin_location              = $this->getAirPort($trip['origin_location']);
                $destination_location         = $this->getAirPort($trip['destination_location']);
                $origin_location_code         = isset($origin_location['code']) ? $origin_location['code'] : '';
                $origin_location_city         = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country      = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_code    = isset($destination_location['code']) ? $destination_location['code'] : '';
                $destination_location_city    = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $trip_id                      = ($idEncrypted) ? $this->encrypt($trip['ID']) : $trip['ID'];
                $arr_trips[]                  = array(
                    'ID' => $trip_id,
                    'name' => $trip['name'],
                    'origin_location' => $trip['origin_location'],
                    'destination_location' => $trip['destination_location'],
                    'origin_location_code' => $origin_location_code,
                    'destination_location_code' => $destination_location_code,
                    'origin_location_city' => $origin_location_city,
                    'destination_location_city' => $destination_location_city,
                    'origin_location_country' => $origin_location_country,
                    'destination_location_country' => $destination_location_country,
                    'departure_date' => $trip['departure_date'],
                    'arrival_date' => $trip['arrival_date'],
                    'service' => $service,
                    'people_request_count' => $trip_people_request,
                    'package_request_count' => $trip_package_request,
                    'project_request_count' => $trip_project_request,
                    'trip_people' => $trip_people,
                    'trip_package' => $trip_package,
                    'trip_project' => $trip_project,
                    'modified' => $trip['modified']
                );
            }
            return $arr_trips;
        }
    }
    public function encrypt($string)
    {
        $output         = false;
        $encrypt_method = "AES-256-CBC";
        /*pls set your unique hashing key */
        $secret_key     = 'trepr';
        $secret_iv      = 'trepr321';
        /* hash */
        $key            = hash('sha256', $secret_key);
        /* iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning */
        $iv             = substr(hash('sha256', $secret_iv), 0, 16);
        /*do the encyption given text/string/number */
        $output         = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output         = base64_encode($output);
        return $output;
    }
    function addPassengerAttachment($arr_pass_attachment)
    {
        $insert  = $this->sql->insert('seeker_people_passengers_attachment');
        $newData = $arr_pass_attachment;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    function addProjectAttachment($arr_pro_attachment)
    {
        $insert  = $this->sql->insert('seeker_project_task_photos');
        $newData = $arr_pro_attachment;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }
    function doTripAction($tripId, $userRole, $action)
    {
        $sucessMsg = '';
        $tableName = ($userRole == 'seeker') ? 'seeker_trips' : 'trips';
        if (strtolower($action) == 'snooze') {
            $upQry     = 'UPDATE ' . $tableName . ' SET active = 0 WHERE ID = ' . $tripId;
            $statement = $this->adapter->query($upQry);
            $res       = $statement->execute();
            $sucessMsg = 'Trip has been  snoozed successfully!';
        } else if (strtolower($action) == 'resume') {
            $upQry     = 'UPDATE ' . $tableName . ' SET active = 1 WHERE ID = ' . $tripId;
            $statement = $this->adapter->query($upQry);
            $res       = $statement->execute();
            $sucessMsg = 'Trip has been  resumed successfully!';
        } else if (strtolower($action) == 'delete') {
            $upQry     = 'DELETE FROM  ' . $tableName . '  WHERE ID = ' . $tripId;
            $statement = $this->adapter->query($upQry);
            $res       = $statement->execute();
            $sucessMsg = 'Trip has been  deleted successfully!';
        } else if (strtolower($action) == 'cancel') {
			$upQry     = 'UPDATE ' . $tableName . ' SET trip_status = 7 WHERE ID = ' . $tripId;
            $statement = $this->adapter->query($upQry);
            $res       = $statement->execute();
            $sucessMsg = 'Trip has been cancelled successfully!';
		}
        return $sucessMsg;
    }
    public function checkVerificationDocument($userId)
    {
        $sql       = "SELECT * FROM user_identity WHERE user_id ='" . $userId . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getcarddetails($usrid, $payment_card_type = false)
    {
        $sql = "SELECT * FROM users_user_card WHERE  user='" . $usrid . "' AND invisibile = 0 ";
        if ($payment_card_type)
            $sql .= ' AND payment_card_type = "' . $payment_card_type . '"';
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function checkwishExist($trip_id, $tableName, $usrid)
    {
        $sql       = "SELECT * FROM $tableName WHERE  user_id='" . $usrid . "' AND trip_id='" . $trip_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function Addcarddetails($user)
    {
       $insert = $this->sql->insert('users_user_card');
        $insert->values($user);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $id           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $id;
       //return true;
    }
    public function AddWishlist($user, $tableName)
    {
        $insert = $this->sql->insert($tableName);
        $insert->values($user);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $id           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $id;
    }
    function getCountryPrice($from, $to, $service_type)
    {
        $sql       = "SELECT country_price,country_currency FROM price_settings_country WHERE  country_setting_deleted = 0  AND country_from ='" . $from . "' AND country_to ='" . $to . "' AND service_type = '" . $service_type . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } 
        else
        {  
            $data = [];

            if($service_type == 'people')
            {
                 $data['country_price'] = 24;
                 $data['country_currency'] = 'GBP';
            }
            else
            {
                 $data['country_price'] = 12;
                 $data['country_currency'] = 'GBP';
            }        
            return $data;
        }
    }
    function getNoofstopsPrice($no_of_stops)
    {
        $sql       = "SELECT stop_price,stop_price_currency FROM price_settings_stops WHERE   no_of_stops ='" . $no_of_stops . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    function getDistancePrice($distance, $service_type)
    {
        $sql       = "SELECT distance_price,distance_currency FROM price_settings_distance WHERE   distance_from <= '" . $distance . "' AND distance_to >= '" . $distance . "' AND service_type = '" . $service_type . "' AND distance_setting_deleted = 0";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    function getItemPricePrice($money, $service_type)
    {
        $sql       = "SELECT price_item_price,price_item_currency FROM price_settings_price_item WHERE   price_item_from <= '" . $money . "' AND price_item_to >= '" . $money . "' AND service_type = '" . $service_type . "' AND price_item_setting_deleted = 0";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    function getWeightPrice($weight, $service_type)
    {
        $sql       = "SELECT weight_carried_price,weight_carried_currency FROM price_settings_weight WHERE   weight_carried_from <= '" . number_format($weight,1) . "' AND weight_carried_to >= '" . number_format($weight,1) . "' AND service_type = '" . $service_type . "' AND weight_setting_deleted = 0";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    function getItemCategoryPrice($item_category_id, $service_type)
    {
        $sql       = "SELECT sum(item_price) as tot_item_price FROM price_settings_item_category WHERE  CONCAT(`item_category_id`, '', `item_sub_category_id`) IN (" . $item_category_id . ") AND service_type = '" . $service_type . "'  AND item_category_setting_deleted = 0";
        //echo $sql; 
		$statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    function getProductCategoryPrice($item_category_id, $service_type)
    {
        $sql       = "SELECT sum(item_price) as tot_item_price FROM price_settings_item_product_category WHERE  CONCAT(`item_category_id`, '', `item_sub_category_id`) IN (" . $item_category_id . ") AND service_type = '" . $service_type . "'  AND item_product_category_setting_deleted = 0";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    function getTaskCategoryPrice($item_category_id, $service_type)
    {
        $sql       = "SELECT sum(item_task_price) as tot_item_price FROM price_settings_item_task_category WHERE  CONCAT(`item_task_category_id`, '', `item_task_sub_category_id`) IN (" . $item_category_id . ") AND service_type = '" . $service_type . "'  AND item_task_category_setting_deleted = 0";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    function getPaymentDetails($userrole, $userid)
    {
        if ($userrole == 'seeker')
            $where = ' AND pay_user = ' . (int) $userid;
        else
            $where = ' AND pay_to_user = ' . (int) $userid;
        $sql       = "SELECT * FROM user_payments WHERE pay_user_type = 'seeker'  $where";
        /*echo $sql;exit; */
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    public function getTravellerPaymentPeopleRequest($tripid)
    {
        $sql = "SELECT *,T.ID FROM traveller_people_request AS R                 JOIN traveller_people AS P ON (R.people_request = P.ID)                 JOIN trips AS T ON (T.ID = P.trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPaymentPackageRequest($tripid)
    {
        $sql = "SELECT *,T.ID FROM traveller_package_request AS R                 JOIN  	traveller_packages AS P ON (R.package_request = P.ID)                 JOIN trips AS T ON (T.ID = P.trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPaymentPeopleRequestBySeeker($tripid)
    {
        $sql = "SELECT *,T.ID FROM seeker_people_request AS R                 JOIN traveller_people AS P ON (R.service_id = P.ID)                 JOIN trips AS T ON (T.ID = P.trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPaymentPackageRequestBySeeker($tripid)
    {
        $sql = "SELECT *,T.ID FROM seeker_package_request AS R                 JOIN  	traveller_packages AS P ON (R.service_id = P.ID)                 JOIN trips AS T ON (T.ID = P.trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPaymentProjectRequestBySeeker($tripid)
    {
        $sql = "SELECT *,T.ID FROM seeker_people_request AS R                 JOIN  	traveller_projects AS P ON (R.service_id = P.ID)                 JOIN trips AS T ON (T.ID = P.trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getTravellerPaymentProjectRequest($tripid)
    {
        $sql = "SELECT *,T.ID FROM traveller_project_request AS R                 JOIN  	traveller_projects AS P ON (R.project_request = P.ID)                 JOIN trips AS T ON (T.ID = P.trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPaymentPeopleRequest($tripid)
    {
        $sql = "SELECT *,T.ID FROM traveller_people_request AS R                 JOIN seeker_people AS P ON (R.service_id = P.ID)                 JOIN seeker_trips AS T ON (T.ID = P.seeker_trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPaymentPackageRequest($tripid)
    {
        $sql = "SELECT *,T.ID FROM traveller_package_request AS R                 JOIN seeker_package AS P ON (R.service_id = P.ID)                 JOIN seeker_trips AS T ON (T.ID = P.seeker_trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPaymentprojectRequest($tripid)
    {
        $sql = " SELECT *,T.ID FROM traveller_project_request AS R                 JOIN  seeker_project AS P ON (R.service_id = P.ID)                 JOIN seeker_trips AS T ON (T.ID = P.seeker_trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPaymentPeopleRequestByTraveller($tripid)
    {
        $sql = " SELECT *,T.ID FROM seeker_people_request AS R                 JOIN seeker_people AS P ON (R.people_request = P.ID)                 JOIN seeker_trips AS T ON (T.ID = P.seeker_trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPaymentPackageRequestByTraveller($tripid)
    {
        $sql = "SELECT *,T.ID FROM seeker_package_request AS R                 JOIN seeker_package AS P ON (R.package_request = P.ID)                 JOIN seeker_trips AS T ON (T.ID = P.seeker_trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getSeekerPaymentprojectRequestByTraveller($tripid)
    {
        $sql = "SELECT *,T.ID FROM seeker_project_request AS R                 JOIN  seeker_project AS P ON (R.project_request = P.ID)                 JOIN seeker_trips AS T ON (T.ID = P.seeker_trip) WHERE R.ID='" . $tripid . "'";
        if (isset($_GET['filterby']) && ($_GET['filterby'] == 'active'))
            $sql .= " AND   T.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif (isset($_GET['filterby']) && ($_GET['filterby'] == 'expired'))
            $sql .= " AND  T.departure_date < '" . date('Y-m-d H:i:s') . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    function getAcceptedRecipts($userid, $userrole, $tripid)
    {
        if ($userrole == 'seeker')
            $where = ' AND pay_user = ' . (int) $userid . ' AND pay_trip_id = ' . (int) $tripid;
        else
            $where = ' AND pay_to_user = ' . (int) $userid . ' AND pay_to_trip_id = ' . (int) $tripid;
        $sql = "SELECT * FROM user_payments ";
        if ($userrole == 'seeker') {
            $sql .= ' JOIN users_user AS U ON (U.ID = pay_to_user)';
        } else {
            $sql .= ' JOIN users_user AS U ON (U.ID = pay_user)';
        }
        $sql .= " WHERE pay_user_type = 'seeker'  $where";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
	public function getLatLongOfDeparture($code){
		$sql = "SELECT latitude,longitude FROM core_airports WHERE code='".$code."'";
		
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else{
            return false;
		}
	}
    public function checkIdentityStatus($userid)
    {
        $sql       = "SELECT * FROM user_identity WHERE user_id ='" . $userid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() >= 2) {
            return true;//$res->current();
        } else
            return false;
    }
    public function checkverifiedIdentityStatus($userid)
    {
        $sql       = "SELECT * FROM user_identity WHERE user_id ='" . $userid . "' AND approved=1";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() >= 2) {
            return true;//$res->current();
        } else
            return false;
    }
    public function checkTicketStatus($tripid)
    {
        $sql       = "SELECT * FROM trips WHERE ID ='" . $tripid . "' AND ticket_option !=''";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return true;//$res->current();
        } else
            return false;
    }
    public function setTripStatus($tripid, $status)
    {
        $update = $this->sql->update();
        $update->table('trips');
        $update->set(array(
            'trip_status' => $status
        ));
        $update->where(array(
            //'trip' => $tripid,
            'ID' => $tripid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
	function updatePackageFee($seeker_package_request_id,$package_total_fee){
		$update = $this->sql->update();
        $update->table('seeker_package');
        $update->set(array(
            'package_service_fee' => $package_total_fee
        ));
        $update->where(array(
            'ID' => $seeker_package_request_id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
	}
	function updateProjectFee($seeker_package_request_id,$package_total_fee){
		$update = $this->sql->update();
        $update->table('seeker_project');
        $update->set(array(
            'project_service_fee' => $package_total_fee
        ));
        $update->where(array(
            'ID' => $seeker_package_request_id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
	}
	public function checkTravellerPeopleNeed($seeker_trip_id,$tripid)
    {
        $sql       = "SELECT * FROM traveller_people_request WHERE trip ='" . $seeker_trip_id . "' AND people_request ='" . $tripid . "' ";
		$statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
    }
	public function checkTravellerProjectNeed($seeker_trip_id,$tripid)
    {
        $sql       = "SELECT * FROM traveller_project_request WHERE trip ='" . $seeker_trip_id . "' AND project_request ='" . $tripid . "' ";
		$statement = $this->adapter->query($sql);
        $res       = $statement->execute();
       if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
    }
	public function checkTravellerPackageNeed($seeker_trip_id,$tripid)
    {
        $sql       = "SELECT * FROM traveller_package_request WHERE trip ='" . $seeker_trip_id . "' AND package_request ='" . $tripid . "' ";
		$statement = $this->adapter->query($sql);
        
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
    }
	public function checkSeekerPeopleNeed($traveller_trip_id,$tripid)
    {
        $sql       = "SELECT * FROM seeker_people_request WHERE trip ='" . $traveller_trip_id . "' AND people_request ='" . $tripid . "' ";
		$statement = $this->adapter->query($sql);
       //echo $sql;die;
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
    }
	public function checkSeekerPackageNeed($traveller_trip_id,$tripid)
    {
        $sql       = "SELECT * FROM seeker_package_request WHERE trip ='" . $traveller_trip_id . "' AND package_request ='" . $tripid . "' ";
		$statement = $this->adapter->query($sql);
        //echo $sql;
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
    }
    public function checkSeekerProjectNeed($traveller_trip_id,$tripid)
    {
        $sql       = "SELECT * FROM seeker_project_request WHERE trip ='" . $traveller_trip_id . "' AND project_request ='" . $tripid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return array();
    }
    public function changeTripStatusByTraveller($requestid, $status, $service){
        if($service =='people'){
            $table = 'traveller_people_request';
        }
        if($service =='package'){
            $table = 'traveller_package_request';
        }
        if($service =='project'){
            $table = 'traveller_project_request';
        }
        $update = $this->sql->update();
        $update->table($table);
        $update->set(array('approved' => $status));
        $update->where(array(
            'ID' => $requestid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();

    }
    public function changeTripStatusBySeeker($requestid, $status, $service){
        if($service =='people'){
            $table = 'seeker_people_request';
        }
        if($service =='package'){
            $table = 'seeker_package_request';
        }
        if($service =='project'){
            $table = 'seeker_project_request';
        }
        $update = $this->sql->update();
        $update->table($table);
        $update->set(array('approved' => $status));
        $update->where(array(
            'ID' => $requestid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();

    }
    public function checkTripDetailsViaID($tripid)
    {
        $sql       = "SELECT user,origin_location,destination_location FROM seeker_trips WHERE ID ='" . $tripid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getLocationDetails($ID)
    {
        $sql       = "SELECT * FROM core_airports WHERE ID ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function addToTravellerEnquiry($hugeData){
        $insert  = $this->sql->insert('travaller_inquires');
        $newData = $hugeData;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
       
        return $ID;
    }
    public function checkSeekerTripDetailsViaID($tripid)
    {
        $sql       = "SELECT user,origin_location,destination_location FROM trips WHERE ID ='" . $tripid . "' ";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function addToSeekerEnquiry($hugeData){
        $insert  = $this->sql->insert('seeker_inquires');
        $newData = $hugeData;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
       
        return $ID;
    }
    public function addToUserMessages($hugeData){
        $insert  = $this->sql->insert('users_message');
        $newData = $hugeData;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
       
        return $ID;
    }
    public function addToAdminNotifications($hugeData){
        $insert  = $this->sql->insert('administration_notifications');
        $newData = $hugeData;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
       
        return $ID;
    }
    public function addToNotifications($hugeData){
        $insert  = $this->sql->insert('notifications');
        $newData = $hugeData;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
       
        return $ID;
    }
	public function updateCompletionNotificationSent($type,$tripId){
		$tName = $type == 'seeker'?'seeker_trips':'trips';
		$query = 'UPDATE '.$tName.' SET is_cmp_notification_sent = 1 WHERE ID = "'.$tripId.'" ';
		$statement 	= $this->adapter->query($query);
		$res		= $statement->execute();
	}
	public function getArrivaledTrips($type){
		$tName = $type == 'seeker'?'seeker_trips':'trips';
		
		$sql = "SELECT b.* FROM ".$tName." as b WHERE b.trip_status != 6 AND b.trip_status != 7 AND b.active=1 AND b.arrival_date <=  NOW() AND b.arrival_date != '' AND b.arrival_date != '0000-00-00 00:00:00' AND b.arrival_date != '1970-01-01 01:00:00' AND b.arrival_date != '1970-01-01 00:00:00' AND is_expired = 0 AND is_cmp_notification_sent = 0";
		$statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return array();
		}
	}
	public function getExpiredTrips($type){
		
		$tName = $type == 'seeker'?'seeker_trips':'trips';
		
		$sql = "SELECT b.* FROM ".$tName." as b WHERE b.departure_date < '" . date('Y-m-d H:i:s') . "' AND is_expired = 0 ";
		$statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return array();
	}
	public function setTripExpired($type,$tripId){
		$tName = $type == 'seeker'?'seeker_trips':'trips';
		$query = 'UPDATE '.$tName.' SET is_expired = 1 WHERE ID = "'.$tripId.'" ';
		$statement 	= $this->adapter->query($query);
		$res		= $statement->execute();
	}
	public function getSeekerPeopleServiceFee($values){
		if (isset($values))
        {
            $distance = isset($values['distance']) && $values['distance'] !=''?$values['distance']:$values['distance_unplan'];
            $trip_route = $values['trip_route'];
            $type_of_service = isset($values['type_of_service'])?$values['type_of_service']:'people';
            $people_count = $values['people_count'];
            $noofstops = $values['noofstops'];
            $distancePrice = $this->getDistancePrice(round($distance), $type_of_service);

            $ree = explode('-', $trip_route);

            $departureAirport = $this->CommonMethodsModel->getAirPortByAirportCode(current($ree));
            $arrivalAirport = $this->CommonMethodsModel->getAirPortByAirportCode(end($ree));
            $countryPrice = $this->getCountryPrice($departureAirport['country_code'], $arrivalAirport['country_code'], $type_of_service);
			
			$service_fees = (float)($distancePrice['distance_price'] + $countryPrice['country_price']);

			if(($type_of_service == 'package' || $type_of_service == 'project') && $this->sessionObj->userrole == 'traveller') {
				if($type_of_service == 'project'){
					$itemCategoryPricePrice = $this->getItemPricePrice(100, 'project');
					$service_fees = $service_fees+$itemCategoryPricePrice['price_item_price'];
				}
                return $service_fees;
            } else {
				$noofstops = $noofstops==0 || $noofstops==''?'direct':$noofstops;
				$stopsPrice = $this->getNoofstopsPrice($noofstops);
				$service_fees = (float)($service_fees + $stopsPrice['stop_price']);
				return $service_fees * $people_count;
			}
        }
		return 0;
	}
	public function getSeekerPackageServiceFee($values)
    {
        if (isset($values))
        {
            $distance = $values['distance'];
            $trip_route = $values['trip_route'];
            $type_of_service = 'package';
            $distancePrice = $this->getDistancePrice(round($distance), $type_of_service);

            $ree = explode('-', $trip_route);

            $departureAirport = $this->CommonMethodsModel->getAirPortByAirportCode(current($ree));
            $arrivalAirport = $this->CommonMethodsModel->getAirPortByAirportCode(end($ree));
            $countryPrice   = $this->getCountryPrice($departureAirport['country_code'], $arrivalAirport['country_code'], $type_of_service);

            $package_total_fee = array();

            for ($i = 1; $i <= $values['packages_count']; $i++)
            {
                $tot_prc = $this->getItemCategoryPrice($values['category_' . $i] . $values['item_category_' . $i], 'package');

                if(!empty($values['item_weight_' . $i])){
                    $item_weight = $values['item_weight_' . $i];
                }
                else{
                    $item_weight = '0.5';
                }

                $tot_weight_prc = $this->getWeightPrice($item_weight, 'package');

                $money = $this->CommonMethodsModel->convertCurrency($values['item_worth_' . $i], $values['total_cost_currency_ajax'], $this->CommonMethodsModel->getSiteCurrency());
                $itemPricePrice = $this->getItemPricePrice($money, 'package');

                $package_total_fee[] = (float)($countryPrice['country_price'] + $distancePrice['distance_price'] + ($tot_prc['tot_item_price'] * $tot_weight_prc['weight_carried_price']) + $itemPricePrice['price_item_price']);
            }
            $packageTotalFee = array_sum($package_total_fee);

            return $packageTotalFee;
        }
		return 0;
    }
	public function getSeekerProjectServiceFee($values) {
        if (isset($values['work_category']) && $values['work_category'] == 1)
        {
			$distance = $values['distance'];
			$type_of_service = 'project';
			$distancePrice = $this->getDistancePrice(round($distance), $type_of_service);

			$countryPrice   = $this->getCountryPrice($values['origin_location_country_code'], $values['destination_location_country_code'], $type_of_service);

            $tot_item_price = array();
			
			$project_fees = array();
            for ($i = 1; $i <= $values['selected_project_products']; $i++) {
				$tot_prc = $this->getProductCategoryPrice($values['product_category_' . $i] . $values['product_additional_requirements_category_' . $i], 'project');
                $tot_item_price[] = $tot_prc['tot_item_price'];
				
				$itemWeightPrice = $this->getWeightPrice($values['item_weight_' . $i], 'project');
				$money = $this->CommonMethodsModel->convertCurrency($values['product_price_' . $i], $values['product_price_currency_' . $i], $this->CommonMethodsModel->getSiteCurrency());
            	$itemCategoryPricePrice = $this->getItemPricePrice($money, 'project');
				
				$project_fees[] = (float)($countryPrice['country_price'] + $distancePrice['distance_price'] + ($tot_prc['tot_item_price'] * $itemWeightPrice['weight_carried_price']) + $itemCategoryPricePrice['price_item_price'] + $money + (($money/100)*5));
            }
			$projectTotalFee = array_sum($project_fees);

            return $projectTotalFee;
        }
        else
        {
            $tot_item_price = array();
            for ($i = 1; $i <= $values['selected_project_tasks']; $i++) {
                $tot_prc = $this->getTaskCategoryPrice($values['task_category_' . $i] . $values['additional_requirements_category_' . $i], 'project');
                $tot_item_price[] = $tot_prc['tot_item_price'];
            }
            $itemCategoryPrice = array_sum($tot_item_price);
            return $itemCategoryPrice;
        }
    }
	public function getServiceFeeConverstion($serviceFee){
        $conversionRate = $this->CommonMethodsModel->convertCurrency(1, $this->CommonMethodsModel->getSiteCurrency(), $this->sessionObj->currency);
        $peopleServiceFee = $conversionRate * $serviceFee;
        $data['convertedPrice'] = number_format(ceil($peopleServiceFee));
        $travellerPrice = ceil((float)($peopleServiceFee - ($peopleServiceFee * 20 / 100)));
        $data['convertedTravellerPrice'] = number_format($travellerPrice);
        $data['conversionRate'] = number_format($conversionRate,2);
        return $data;
	}
	public function getExpiredRequests($type){

		if($type == 'seeker_package'){
			$tName = '`seeker_package_request`';
		} else if($type == 'seeker_people'){
			$tName = '`seeker_people_request`';
		} else if($type == 'seeker_project'){
			$tName = '`seeker_project_request`';
		} else if($type == 'traveller_package'){
			$tName = '`traveller_package_request`';
		} else if($type == 'traveller_people'){
			$tName = '`traveller_people_request`';
		} else if($type == 'traveller_project'){
			$tName = '`traveller_project_request`';
		}

		$sql = 'SELECT * FROM '.$tName.' WHERE TIMESTAMPDIFF(HOUR, `modified`, NOW()) > 24 AND `approved` = 0 ';
		$statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return array();
		}
	}
    public function requestExpire(){
		$query = 'UPDATE `seeker_package_request` SET `approved` = 4 WHERE TIMESTAMPDIFF(HOUR, `modified`, NOW()) > 24;
				  UPDATE `seeker_people_request` SET `approved` = 4 WHERE TIMESTAMPDIFF(HOUR, `modified`, NOW()) > 24;
				  UPDATE `seeker_project_request` SET `approved` = 4 WHERE TIMESTAMPDIFF(HOUR, `modified`, NOW()) > 24;
				  UPDATE `traveller_package_request` SET `approved` = 4 WHERE TIMESTAMPDIFF(HOUR, `modified`, NOW()) > 24;
				  UPDATE `traveller_people_request` SET `approved` = 4 WHERE TIMESTAMPDIFF(HOUR, `modified`, NOW()) > 24;
				  UPDATE `traveller_project_request` SET `approved` = 4 WHERE TIMESTAMPDIFF(HOUR, `modified`, NOW()) > 24;';
		$statement 	= $this->adapter->query($query);
		$res		= $statement->execute();
	}
}