<?php namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;

class ProfileModel extends AbstractTableGateway implements AdapterAwareInterface
{

    protected $table = 'trips';
    protected $adapter;
    protected $sql;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->sql = new Sql($this->adapter);
    }

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }

    //public function getTravellers($origin_location,$destination_location,$date,$days=''){
    public function getTravellers($filters)
    {
        if ($filters['airline_name'] != '')
            $filter_airline_name = " b.airline_name='" . $filters['airline_name'] . "' ";
        if ($filters['flight_number'] != '')
            $filter_flight_number = " b.flight_number='" . $filters['flight_number'] . "' ";
        if ($filter_airline_name != '' && $filter_flight_number != '')
            $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";
        elseif ($filter_airline_name != '')
            $filter_airline_name_number = " AND $filter_airline_name ";
        elseif ($filter_flight_number != '')
            $filter_airline_name_number = " AND $filter_flight_number ";
        if ($filters['radius'] != '') {
            $filter_radius = " HAVING distance <= " . $filters['radius'];
            $filter_radius_field = " , ( 3959 * acos( cos( radians( " . $filters['current_latitude'] . " ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( " . $filters['current_longitude'] . " ) ) + sin( radians( " . $filters['current_latitude'] . " ) ) * sin( radians( a.latitude ) ) ) ) AS distance ";
        }
        if ($filters['days'] != '') {
            $start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'] . ' days'));
            $end_date = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'] . ' days'));
            $sql = "SELECT DISTINCT(a.ID),a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b  WHERE aa.ID=a.user AND a.user=b.user AND b.active=1 AND b.origin_location='" . $filters['origin'] . "' AND b.destination_location='" . $filters['destination'] . "' AND b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "'  $filter_airline_name_number $filter_radius";
        } else {
            $sql = "SELECT DISTINCT(a.ID),a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b  WHERE aa.ID=a.user AND a.user=b.user AND b.active=1 AND b.origin_location='" . $filters['origin'] . "' AND b.destination_location='" . $filters['destination'] . "' AND b.departure_date='" . $filters['date'] . "' $filter_airline_name_number $filter_radius";
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getTravellersForPackage($filters)
    {
        if ($filters['airline_name'] != '')
            $filter_airline_name = " b.airline_name='" . $filters['airline_name'] . "' ";
        if ($filters['flight_number'] != '')
            $filter_flight_number = " b.flight_number='" . $filters['flight_number'] . "' ";
        if ($filter_airline_name != '' && $filter_flight_number != '')
            $filter_airline_name_number = " AND ($filter_airline_name OR $filter_flight_number) ";
        elseif ($filter_airline_name != '')
            $filter_airline_name_number = " AND $filter_airline_name ";
        elseif ($filter_flight_number != '')
            $filter_airline_name_number = " AND $filter_flight_number ";
        if ($filters['radius'] != '') {
            $filter_radius = " HAVING distance <= " . $filters['radius'];
            $filter_radius_field = " , ( 3959 * acos( cos( radians( " . $filters['current_latitude'] . " ) ) * cos( radians( a.latitude ) ) * cos( radians( a.longitude ) - radians( " . $filters['current_longitude'] . " ) ) + sin( radians( " . $filters['current_latitude'] . " ) ) * sin( radians( a.latitude ) ) ) ) AS distance ";
        }
        if ($filters['days'] != '') {
            $start_date = date('Y-m-d h:i:s', strtotime($filters['date'] . ' - ' . $filters['days'] . ' days'));
            $end_date = date('Y-m-d 23:59:59', strtotime($filters['date'] . ' + ' . $filters['days'] . ' days'));
            $sql = "SELECT DISTINCT(a.ID),a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b, trips_traveller_packages as c  WHERE aa.ID=a.user AND a.user=b.user AND c.trip=b.ID AND b.active=1 AND b.origin_location='" . $filters['origin'] . "' AND b.destination_location='" . $filters['destination'] . "' AND  b.departure_date>='" . $start_date . "' AND b.departure_date<='" . $end_date . "' $filter_airline_name_number $filter_radius";
        } else {
            $sql = "SELECT DISTINCT(a.ID),a.*,aa.first_name,aa.last_name,aa.image,b.airline_name,b.flight_number,DATE_FORMAT(b.departure_date,'%m/%d/%Y') as departure_date,DATE_FORMAT(aa.created,'%m/%d/%Y') as member_since $filter_radius_field FROM users_user as aa,users_user_locations as a, trips as b, trips_traveller_packages as c  WHERE aa.ID=a.user AND a.user=b.user AND c.trip=b.ID AND b.active=1 AND b.origin_location='" . $filters['origin'] . "' AND b.destination_location='" . $filters['destination'] . "' AND b.departure_date='" . $filters['date'] . "' $filter_airline_name_number $filter_radius";
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getUserProfilePhoto($ID)
    {
        $sql = "SELECT user_photo_name FROM user_photos WHERE user_id='" . $ID . "' and marked_as_profile_photo=1";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        //  var_dump($res);exit;
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getprofileaddr($ID)
    {
        $sql = "SELECT * FROM `users_user_locations` WHERE  address_type='home' and user='" . $ID . "' order by ID desc";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getprofileaddrmail($ID)
    {
        $sql = "SELECT * FROM `users_user_locations` WHERE address_type='mail' and user='" . $ID . "'  order by ID desc";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getabtus($ID)
    {
        $sql = "SELECT * FROM `user_identity` WHERE  user_id='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getreviews($ID)
    {
        $sql = "SELECT * FROM `users_reviews` WHERE  recevier_id='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return array();
    }

    public function getprofilenotification($ID)
    {
        $sql = "SELECT b.msg FROM `core_table_fieldvalue` as a,users_field_valid as b WHERE b.field_id=a.ID and b.user_id='" . $ID . "'  and b.notifalert='3'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getlang($ID)
    {
        $sql2 = "SELECT * FROM users_user WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql2);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $rs2 = $res->current();
        } else $rs2 = false;

        $sql = "SELECT * FROM language ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $rs = $res->getResource()->fetchAll();
        } else $rs = false;
        $retv = '';
        foreach ($rs as $val => $key) {
            $lanid = $val;
            $selv = '';
            if ($rs2['ID'] == $key['ID']) {
                $selv = "selected='selected'";
            }
            $retv .= '<option value="' . $key['ID'] . '" ' . $selv . '>' . $key['lang'] . '</option>';
        }
        return $retv;
    }

    public function updateprofile($arr_people_detail, $user_id)
    {
        //print_r($arr_people_detail);exit;
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set($arr_people_detail);
        $update->where(array('ID' => $user_id));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return $results;
    }

    public function updateprofileabus($arr_peopleabs_detail, $user_id)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set($arr_peopleabs_detail);
        $update->where(array('ID' => $user_id));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
    }

    public function updateprofileaddhome($arr_address_detail, $user_id)
    {
        $sql = "SELECT * FROM users_user_locations WHERE address_type='home' and ID='" . $user_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $rs = $res->current();
        } else $rs = false;
        if ($rs['user_id']) {
            $update = $this->sql->update();
            $update->table('users_user_locations');
            $update->set($arr_address_detail);
            $update->where(array('ID' => $user_id, 'address_type' => 'home'));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            //$this->_db->update('users_user_locations',$arr_address_detail,"ID='{$user_id}' and address_type='home' ");
        } else {
            $insert = $this->sql->insert('users_user_locations');
            $insert->values($arr_address_detail);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
            //$this->_db->insert('users_user_locations',$arr_address_detail);
        }
    }

    public function updateprofileverfy($arr_peoplevery_detail, $user_id)
    {
        $sql = "SELECT * FROM user_identity WHERE  user_id='" . $user_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $rs = $res->current();
        } else {
            $rs = false;
        }
        if ($rs['user_id']) {
            $update = $this->sql->update();
            $update->table('users_user_locations')
                ->set($arr_address_detail)
                ->where(array('ID' => $user_id, 'address_type' => 'home'));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            //$this->_db->update('user_identity',$arr_peoplevery_detail,"user_id='{$user_id}'");
        } else {
            $insert = $this->sql->insert('user_identity');
            $insert->values($arr_peoplevery_detail);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
            //$this->_db->insert('user_identity',$arr_peoplevery_detail);
        }
    }

    public function updateprofileaddmail($arr_addressmail_detail, $user_id)
    {
        $sql = "SELECT * FROM users_user_locations WHERE  address_type='mail' and ID='" . $user_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $rs = $res->current();
        } else {
            $rs = false;
        }
        if ($rs['user_id']) {
            $update = $this->sql->update();
            $update->table('users_user_locations')
                ->set($arr_addressmail_detail)
                ->where(array('ID' => $user_id, 'address_type' => 'mail'));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            //$this->_db->update('users_user_locations',$arr_addressmail_detail," ID='{$user_id}' and address_type='mail' ");
        } else {
            $insert = $this->sql->insert('users_user_locations');
            $insert->values($arr_addressmail_detail);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
            //$this->_db->insert('users_user_locations',$arr_addressmail_detail);
        }
    }

    public function addUserPhoto($arr_user_photo_detail, $user_id)
    {
        if ($user_id) {
            $insert = $this->sql->insert('user_photos');
            $insert->values($arr_user_photo_detail);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userPhotoId = $this->adapter->getDriver()->getLastGeneratedValue();
            return $userPhotoId;
        } else
            return false;
    }

    public function getUserPhotos($user_id, $resType = 'list')
    {
        $sql = "SELECT * FROM user_photos where user_id= " . (int)$user_id;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($resType == 'list') {
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            } else {
                return false;
            }
        } else if ($resType == 'count') {
            return $res->count();
        }
    }
	
	
	public function getUserPhotosMarked($user_id, $resType = 'list')
    {
         $sql = "SELECT * FROM user_photos where  marked_as_profile_photo = '1' and user_id= " . (int)$user_id ;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($resType == 'list') {
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            } else {
                return false;
            }
        } else if ($resType == 'count') {
            return $res->count();
        }
    }


    public function removeUserPhoto($photoId)
    {
        $sql = "DELETE FROM user_photos WHERE user_photo_id=" . (int)$photoId;
        $statement = $this->adapter->query($sql);
        try {
            $affectedRows = $statement->execute()->getAffectedRows();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }
        if (empty($affectedRows)) {
            $affectedRows = false;
        }
        return $affectedRows;
    }

    public function markAsProfilePhoto($photoId, $userId)
    {
        $update = $this->sql->update();
        $update->table('user_photos');
        $update->set(array('marked_as_profile_photo' => 0));
        $update->where(array('user_id' => $userId));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();

        $update = $this->sql->update();
        $update->table('user_photos');
        $update->set(array('marked_as_profile_photo' => 1));
        $update->where(array('user_photo_id' => $photoId));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        //$results    = $statement->execute();
        try {
            $affectedRows = $statement->execute()->getAffectedRows();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }
        if (empty($affectedRows)) {
            $affectedRows = false;
        }
        return $affectedRows;
    }

    public function addUserVideo($arr_user_video_detail, $user_id)
    {
        if ($user_id) {
            $insert = $this->sql->insert('user_videos');
            $insert->values($arr_user_video_detail);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userVideoId = $this->adapter->getDriver()->getLastGeneratedValue();
            return $userVideoId;
        } else
            return false;
    }

    public function getUserVideos($user_id, $resType = 'list')
    {
        $sql = "SELECT * FROM user_videos where user_id= " . (int)$user_id;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($resType == 'list') {
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            } else {
                return false;
            }
        } else if ($resType == 'count') {
            return $res->count();
        }
    }

    public function removeUserVideo($videoId)
    {
        $sql = "DELETE FROM user_videos WHERE user_video_id=" . (int)$videoId;
        $statement = $this->adapter->query($sql);
        try {
            $affectedRows = $statement->execute()->getAffectedRows();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }
        if (empty($affectedRows)) {
            $affectedRows = false;
        }
        return $affectedRows;
    }

    public function getIdentityTypes($user_id)
    {
        $sql = "SELECT * FROM user_identity_type AS T

                LEFT JOIN user_identity AS I ON  (I.identity_type = T.identity_type_id AND I.user_id = '" . (int)$user_id . "')



                WHERE T.identity_type_deleted = 0 GROUP BY T.identity_type_id";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getIdentityCount($userid)
    {
        $sql = "SELECT * FROM user_identity WHERE user_id = '" . (int)$userid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function addInviteMails($data)
    {
        $insertString = array();
        foreach ($data as $row) {
            $insertString[] = '(' . $row['user_id'] . ', "' . $row['invited_email'] . '" ,' . $row['invite_added'] . ')';
        }
        $sql = 'INSERT INTO user_invites (user_id, invited_email, invite_added) VALUES ' . implode(',', $insertString);
        $results = $this->adapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        return $results->getAffectedRows();
    }

    public function getInviteMails($user_id, $data = false)
    {
        $sql = "SELECT * FROM user_invites WHERE user_id = " . $user_id;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getUserInviteDetails($userId)
    {
        $sql =  "SELECT `U`.`ID`, `U`.`first_name`, `U`.`last_name`, `U`.`first_name`, `U`.`email_address`, (select sum(`T`.`trans_amount`) FROM `user_wallet_transactions` AS `T` WHERE `T`.`user_id` = `U`.`user_referrer` AND `T`.`referral_user_id` = `U`.`ID` "
                . "AND `T`.`trans_deleted` = 0) AS `trans_amount` "
                . "FROM `users_user` AS `U` WHERE `U`.`user_referrer`=".$userId;
      /*  $sql = "SELECT U.user_id,U.invited_email as email_address,UU.ID,UU.first_name,UU.last_name FROM `user_invites` AS `U` LEFT JOIN users_user AS UU ON U.signed_user_id=UU.ID WHERE `U`.`user_id`=" . $userId;*/
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function extradata()
    {
        return 1;
    }

    public function getUserInviteProgressStatus($userId)
    {
        /*$sql =  "SELECT `U`.`ID`, `U`.`first_name`, `U`.`last_name`, `U`.`first_name`, `U`.`email_address`, (select sum(`T`.`trans_amount`) FROM `user_wallet_transactions` AS `T` WHERE `T`.`user_id` = `U`.`user_referrer` AND `T`.`referral_user_id` = `U`.`ID` "
                . "AND `T`.`trans_deleted` = 0) AS `trans_amount` "
                . "FROM `users_user` AS `U` WHERE `U`.`user_referrer`=".$userId;*/
        $sql = "SELECT user_id,invited_email as email_address FROM `user_invites` AS `U` WHERE `U`.`user_id`=" . $userId;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getUserInviteDetailsOld($userId)
    {
        $sql = "SELECT `U`.`ID`, `U`.`first_name`, `U`.`last_name`, `U`.`first_name`, `U`.`email_address`, (select sum(`T`.`trans_amount`) FROM `user_wallet_transactions` AS `T` WHERE `T`.`user_id` = `U`.`user_referrer` AND `T`.`referral_user_id` = `U`.`ID` "
            . "AND `T`.`trans_deleted` = 0) AS `trans_amount` "
            . "FROM `users_user` AS `U` WHERE `U`.`user_referrer`=" . $userId;
        /*$sql =  "SELECT `UI`.*,`U`.`ID`, `U`.`first_name`, `U`.`last_name`, `U`.`first_name`, (select sum(`T`.`trans_amount`)
                        FROM `user_wallet_transactions` AS `T`
                        WHERE `T`.`user_id` = `UI`.`user_id` AND `T`.`referral_user_id` = `U`.`ID` "
                . "AND `T`.`trans_deleted` = 0) AS `trans_amount`"
                . "FROM `user_invites` AS `UI` "
                . "LEFT JOIN `users_user` AS `U` "
                . "ON `U`.`ID` = `UI`.`signed_user_id` "
                . "WHERE `UI`.`user_id`=".$userId;*/
        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function addUpdateFriendsList($data = false)
    {
        $sqlVals = array();
        foreach ($data as $row) {
            $sqlVals[] = '(' . (int)$row['user_id']
                . ', "' . $row['oauth_provider_type']
                . '", "' . $row['friend_oauth_id']
                . '", "' . $row['friend_website_url']
                . '", "' . $row['friend_profile_url']
                . '", "' . $row['friend_photo_url']
                . '", "' . $row['friend_photo_location']
                . '", "' . $row['friend_display_name']
                . '", "' . $row['friend_description']
                . '", "' . $row['friend_email']
                . '", ' . (int)time()
                . ')';
        }
        $sql = 'REPLACE INTO user_friends_list ('
            . 'user_id,'
            . 'oauth_provider_type,'
            . 'friend_oauth_id,'
            . 'friend_website_url,'
            . 'friend_profile_url,'
            . 'friend_photo_url,'
            . 'friend_photo_location,'
            . 'friend_display_name,'
            . 'friend_description,'
            . 'friend_email,'
            . 'friend_added'
            . ') VALUES ' . implode(',', $sqlVals);
        //echo $sql;exit;
        $results = $this->adapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        if ($results->getAffectedRows()) {
            return $results->getAffectedRows();
        } else {
            return false;
        }
    }

    public function addUpdateTempFriendsList($data = false)
    {
        $sqlVals = array();
        foreach ($data as $row) {
            $sqlVals[] = '("' . $row['user_id']
                . '", "' . $row['user_session_id']
                . '", "' . $row['oauth_provider_type']
                . '", "' . $row['friend_oauth_id']
                . '", "' . $row['friend_website_url']
                . '", "' . $row['friend_profile_url']
                . '", "' . $row['friend_photo_url']
                . '", "' . $row['friend_photo_location']
                . '", "' . $row['friend_display_name']
                . '", "' . $row['friend_description']
                . '", "' . $row['friend_email']
                . '", ' . (int)time()
                . ')';
        }
        $sql = 'REPLACE INTO user_friends_list_temp ('
            . 'user_id,'
            . 'user_session_id,'
            . 'oauth_provider_type,'
            . 'friend_oauth_id,'
            . 'friend_website_url,'
            . 'friend_profile_url,'
            . 'friend_photo_url,'
            . 'friend_photo_location,'
            . 'friend_display_name,'
            . 'friend_description,'
            . 'friend_email,'
            . 'friend_added'
            . ') VALUES ' . implode(',', $sqlVals);
        $sql = html_entity_decode($sql);
        //echo $sql;exit;
        $results = $this->adapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        if ($results->getAffectedRows()) {
            return $results->getAffectedRows();
        } else {
            return false;
        }
    }

    public function getTempFriendsList($user_id)
    {
        //echo 'identifier: '.$identifier;exit;
        //$sql = "SELECT * FROM `user_friends_list_temp` WHERE `user_session_id` = '" . $session_id . "' AND `user_id`='" . $identifier . "'";
		$sql = "SELECT * FROM `user_friends_list_temp` WHERE  `user_id`='" .$user_id. "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function treperZoneFriends($user_id)
    {
        $myProfile = $this->getProfile($user_id);
        $sql = "SELECT `UF`.*,U.ID, U.image, U.first_name, U.last_name FROM `user_friends_list` as `UF` JOIN `users_user` as `U` ON `UF`.`friend_email` = `U`.`email_address` WHERE `UF`.`user_id`=" . $user_id . " AND `UF`.`friend_email` != '" . $myProfile['email_address'] . "' GROUP BY `U`.`email_address`,`UF`.`friend_oauth_id`";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getprofile($ID)
    {
        $sql = "SELECT * FROM users_user WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $result = $res->current();
            //$photoRow = $this->getUserProfilePhoto($ID);
            //$result['image'] = $photoRow['user_photo_name'];
            return $result;
        } else return false;
    }

    public function removeFriendsList($user_id, $provider)
    {
        $sql = "DELETE FROM user_friends_list WHERE user_id=" . (int)$user_id . " AND oauth_provider_type='" . $provider . "'";
        $statement = $this->adapter->query($sql);
        try {
            $affectedRows = $statement->execute()->getAffectedRows();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }
        if (empty($affectedRows)) {
            $affectedRows = false;
        }
        return $affectedRows;
    }

    public function removeTempFriendsList($session_id, $identifier)
    {
        $sql = "DELETE FROM user_friends_list_temp WHERE user_id='" . $identifier . "' AND user_session_id='" . $session_id . "'";
        $statement = $this->adapter->query($sql);
        try {
            $affectedRows = $statement->execute()->getAffectedRows();
        } catch (\Exception $e) {
            die('Error: ' . $e->getMessage());
        }
        if (empty($affectedRows)) {
            $affectedRows = false;
        }
        return $affectedRows;
    }

    public function sendReferenceRequest($arr_request_detail = array())
    {
        if (!empty($arr_request_detail)) {
            $insert = $this->sql->insert('user_reference_requests');
            $insert->values($arr_request_detail);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $requestId = $this->adapter->getDriver()->getLastGeneratedValue();
            return $requestId;
        } else {
            return false;
        }
    }

    public function updateReferenceRequest($req_id, $arr_request_detail)
    {
        $update = $this->sql->update();
        $update->table('user_reference_request');
        $update->set($arr_request_detail);
        $update->where(array('request_id' => $req_id));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return $results;
    }

    public function sendMultipleReferenceRequest($data = array())
    {
        $insertString = array();
        foreach ($data as $row) {
            $insertString[] = '(' . $row['requested_by_user'] . ', "' . $row['requested_to_email'] . '" ,' . $row['requested_to_user'] . ',' . $row['request_added'] . ')';
        }
        $sql = 'INSERT INTO user_reference_requests (requested_by_user, requested_to_email, requested_to_user, request_added) VALUES ' . implode(',', $insertString);
        $results = $this->adapter->query($sql, Adapter::QUERY_MODE_EXECUTE);
        return $results->getAffectedRows();
    }

    public function checkReferenceRequested($requestedBy, $requestedTo)
    {
        $sql = "SELECT * FROM `user_reference_requests` WHERE requested_by_user = " . $requestedBy . " AND requested_to_user= " . $requestedTo;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getReferenceRequests($user_id)
    {
        $sql = "SELECT `URR`.*,U.ID, U.image, U.first_name, U.last_name FROM `user_reference_requests` as `URR` JOIN `users_user` as `U` ON `URR`.`requested_to_user` = `U`.`ID` WHERE `URR`.`requested_to_user`=" . $user_id;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getReferenceRequestById($req_id)
    {
        $sql = "SELECT `URR`.*,U.ID, U.image, U.first_name, U.last_name FROM `user_reference_requests` as `URR` LEFT JOIN `users_user` as `U` ON `URR`.`requested_to_user` = `U`.`ID` WHERE `URR`.`request_id`=" . $req_id;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else {
            return false;
        }
    }

    public function getReferenceResponseByKey($res_key)
    {
        $sql = "SELECT * FROM `user_reference_response` WHERE `reference_response_key`='" . $res_key . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else {
            return false;
        }
    }

    public function getPendingReferenceRequests($user_id)
    {
        $sql = "SELECT * FROM `user_reference_requests` as `req`"
            . " LEFT JOIN `user_reference_response` as `res` ON `res`.`request_id` = `req`.`request_id`"
            . " JOIN `users_user` as `U` ON `req`.`requested_by_user` = `U`.`ID`"
            . " WHERE `req`.`requested_to_user` = " . $user_id . " AND `res`.`request_id` is null";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function writeReferenceResponse($arr_response_detail)
    {
        $insert = $this->sql->insert('user_reference_response');
        $insert->values($arr_response_detail);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $resId = $this->adapter->getDriver()->getLastGeneratedValue();
        return $resId;
    }

    public function updateReferenceResponse($ref_id, $arr_response_detail)
    {
        $update = $this->sql->update();
        $update->table('user_reference_response');
        $update->set($arr_response_detail);
        $update->where(array('reference_id' => $ref_id));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return $results;
    }

    public function getWritenReference($user_id)
    {
        $sql = "SELECT * FROM `user_reference_response` as `res`"
            . " LEFT JOIN `user_reference_requests` as `req` ON `req`.`request_id` = `res`.`request_id`"
            . " LEFT JOIN `users_user` as `U` ON `req`.`requested_by_user` = `U`.`ID`"
            . " WHERE `req`.`requested_to_user` = " . $user_id . "";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getUserReferences($user_id, $pending = false)
    {
        $sql = "SELECT * FROM `user_reference_response` as `res`"
            . " LEFT JOIN `user_reference_requests` as `req` ON `req`.`request_id` = `res`.`request_id`"
            . " LEFT JOIN `users_user` as `U` ON `req`.`requested_to_user` = `U`.`ID`"
            . " WHERE `req`.`requested_by_user` = " . $user_id . "";
        if ($pending === true) {
            $sql .= " AND `res`.`reference_approved` = 0";
        } else {
            $sql .= " AND `res`.`reference_approved` != 0";
        }
        $sql .= " AND `res`.`reference_deleted` = 0";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function addEditUserVerfications($arrData = array(), $userid)
    {
        $verfiyid = isset($arrData['verifyid']) ? $arrData['verifyid'] : '';
        $verify_details = isset($arrData['verify_details']) ? $arrData['verify_details'] : '';
        $identity_type = $arrData['identity_type'];
        $arrIds = array();
        if (!empty($identity_type)) {
            foreach ($identity_type as $keyId => $valId) {
                $arrInsData = array();
                $arrInsData['user_id'] = $userid;
                $arrInsData['verfiyid'] = $verfiyid;
                $arrInsData['verify_details'] = $verify_details;
                $arrInsData['approved'] = '0';
                $arrInsData['rejected'] = '0';
                $arrInsData['identity_type'] = $valId;
                if (isset($arrData['image'][$valId]) && !empty($arrData['image'][$valId])) {
                    $arrInsData['image'] = $arrData['image'][$valId];
                }
                if (isset($arrData['ID'][$valId]) && !empty($arrData['ID'][$valId])) {
                    $update = $this->sql->update();
                    $update->table('user_identity');
                    $update->set($arrInsData);
                    $update->where(array('ID' => $arrData['ID'][$valId]));
                    $statement = $this->sql->prepareStatementForSqlObject($update);
                    $results = $statement->execute();
                    $arrIds[] = '<input type="hidden" class="identity_type_id" name="identity_type[' . $valId . ']" value="' . $arrData['ID'][$valId] . '" >';
                } else {
                    $insert = $this->sql->insert('user_identity');
                    $insert->values($arrInsData);
                    $selectString = $this->sql->getSqlStringForSqlObject($insert);
                    $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
                    $userIdenTifyId = $this->adapter->getDriver()->getLastGeneratedValue();
                    $arrIds[] = '<input type="hidden" class="identity_type_id" name="identity_type_id[' . $valId . ']" value="' . $userIdenTifyId . '" >';
                }
            }
        }

        if (!empty($arrIds)) {
            return $arrIds;
        }
        return false;

    }

    public function getVerifyDetails($user_id)
    {
        $sql = "SELECT * FROM `user_identity`  WHERE user_id ='" . $user_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getVerifyRow($ID)
    {
        $sql = "SELECT * FROM `user_identity`  WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function updateUserVerifyDetails($arr_verify_detail, $ID)
    {
        // print_r($ID);exit;
        $update = $this->sql->update();
        $update->table('user_identity');
        $update->set($arr_verify_detail);
        $update->where(array('ID' => $ID));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();

        return $results;

    }

    public function addUserProfilePicture($photoid, $userid)
    {
        $sql = "SELECT user_photo_name FROM user_photos WHERE user_photo_id = " . $photoid;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $asd = $res->getResource()->fetchAll();
            $da = array("image" => $asd[0]['user_photo_name']);
            $update = $this->sql->update();
            $update->table('users_user');
            $update->set($da);
            $update->where(array('ID' => $userid));
            $statement1 = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement1->execute();
            return true;
        } else {
            return false;
        }
    }

    public function getDPName($photoId)
    {
        $sql = "SELECT user_photo_name FROM user_photos WHERE user_photo_id = " . $photoId;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }
}