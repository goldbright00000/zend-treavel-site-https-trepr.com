<?php

namespace Application\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;

class DashboardModel extends AbstractTableGateway implements AdapterAwareInterface
{

    protected $adapter;
    protected $sql;

    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->sql = new Sql($this->adapter);
    }

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }

    public function getTravellerEnquiries($ID)
    {
        $sql = "SELECT * FROM traveller_people_request AS spr JOIN seeker_trips AS st ON st.ID=spr.people_request JOIN users_user AS u ON u.ID=st.user WHERE u.ID='" . $ID . "'";
        return $this->getFromSqlString($sql, 'rows');
    }

    private function getFromSqlString($sql, $resType = 'row')
    {
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            if ($resType == 'row')
                return $res->current();
            else if ($resType == 'rows')
                return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerEnquiries($ID)
    {

    }

    public function getalert($ID)
    {
        $sql = "SELECT * FROM `users_message` WHERE  user_id='" . $ID . "' and msg_type='a'";
        return $this->getFromSqlString($sql, 'rows');
    }

    public function getnoft($ID)
    {
        $sql = "SELECT * FROM `users_message` WHERE  user_id='" . $ID . "' and msg_type='n'";
        return $this->getFromSqlString($sql, 'rows');
    }

    public function getdashmsg($ID)
    {
        $sql = "SELECT * FROM `users_message` WHERE  user_id='" . $ID . "' and msg_type='m'";
        return $this->getFromSqlString($sql, 'rows');
    }

    public function gettrval($ID)
    {
        $sql = "SELECT t.*,u.first_name,u.last_name FROM `travaller_inquires` AS t  JOIN users_user AS u ON u.ID=t.sender_id WHERE t.recevier_id='" . $ID . "' ";
        return $this->getFromSqlString($sql, 'rows');
    }

    public function getseeker($ID)
    {
        $sql = "SELECT s.*,u.first_name,u.last_name FROM `seeker_inquires` AS s  JOIN users_user AS u ON u.ID=s.sender_id WHERE s.recevier_id='" . $ID . "' ";
        return $this->getFromSqlString($sql, 'rows');
    }

    public function getdisputes($ID)
    {
        //$sql = "SELECT * FROM `trips_disputes` WHERE  user_id='".$ID."' ";
         $sql = "SELECT t.*,u.first_name,u.last_name FROM `getdisputes` AS t  JOIN users_user AS u ON u.ID=t.user_id WHERE t.user_id='" . $ID . "' ";
      
        return $this->getFromSqlString($sql, 'rows');
    }

    public function viewgettrval($ID, $VT_ID)
    {
        $sql = "SELECT * FROM `travaller_inquires` WHERE  recevier_id='" . $ID . "' and ID='" . $VT_ID . "' ";
        return $this->getFromSqlString($sql, 'row');
    }

    public function getseekerlst($ID, $VT_ID)
    {
        $sql = "SELECT * FROM `seeker_inquires` WHERE  recevier_id='" . $ID . "' and ID='" . $VT_ID . "' ";
        $rs = $this->_db->fetchRow($sql);
        return $rs;
    }

    public function getdisputeslst($ID, $VT_ID)
    {
        $sql = "SELECT * FROM `user_dispute` WHERE  user_id='" . $ID . "' and ID='" . $VT_ID . "' ";
        return $this->getFromSqlString($sql, 'row');
    }

    public function getdisputeslstdt($ID, $VT_ID)
    {
        $sql = "SELECT * FROM `trips_disputes_details` WHERE   dispautoid='" . $VT_ID . "' ";
        return $this->getFromSqlString($sql, 'rows');
    }

    public function getsupertrav($ID)
    {
        //$sql = "SELECT * FROM `users_user` WHERE   dispautoid='".$VT_ID."' ";
        $sql = "SELECT u.ID,u.first_name,u.last_name,u.location,u.image,u.created,u.gender,u.aboutus,u.age,r.rating AS rates FROM `users_user` AS u JOIN users_reviews AS r ON r.recevier_id=u.ID WHERE r.rating >=3 AND r.recevier_id !=$ID GROUP BY u.ID";
        return $this->getFromSqlString($sql, 'rows');
    }

    public function adddisputesDetail($arr_disputes_details)
    { 
       
        $insert  = $this->sql->insert('user_dispute');
        $newData = $arr_disputes_details;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();
       
        return $ID;

    }

    public function adddisputesDetaildt($arr_disputes_details)
    {
        $this->_db->insert('trips_disputes_details', $arr_disputes_details);
        $ID = $this->_db->lastInsertId('trips_disputes_details', 'ID');
        return $ID;
    }

    public function getIdentityCount($user_id)
    {
        $sql = "SELECT * FROM user_identity WHERE user_id = '" . (int)$user_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }
}