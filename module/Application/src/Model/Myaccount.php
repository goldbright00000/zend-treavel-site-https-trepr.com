<?php
namespace Application\Model;
use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Sql;
use Zend\Db\ResultSet\ResultSet;
class Myaccount extends AbstractTableGateway implements AdapterAwareInterface
{
    protected $adapter;
    protected $sql;
    public function __construct(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->sql     = new Sql($this->adapter);
    }
    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->initialize();
    }
    public function updateaccountnotif($arr_address_detail, $user_id)
    {
        $sql       = "SELECT * FROM myaccount_notification WHERE  user_id='" . $user_id . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            $rs = $res->current();
        } else {
            $rs = false;
	}
        if ($rs['user_id']) {
            $update = $this->sql->update();
            $update->table('myaccount_notification');
            $update->set($arr_address_detail);
            $update->where(array(
                'user_id' => $user_id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results   = $statement->execute();
            $userid    = $rs['user_id'];
        } else {
            $insert = $this->sql->insert('myaccount_notification');
            $insert->values($arr_address_detail);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid       = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        return $userid;
    }
    public function updateuser($user_info, $user_id)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set($user_info);
        $update->where(array(
            'ID' => $user_id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function getactnotification($ID)
    {
        $sql       = "SELECT * FROM myaccount_notification WHERE user_id='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function getcancelaccount($ID)
    {
        $sql       = "SELECT * FROM users_user WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function updateuserpass($oldpass, $conpass, $newpass, $ID)
    {
        $retvn = '';
        if ($conpass == $conpass) {
            $sql       = "SELECT * FROM users_user WHERE ID='" . $ID . "'";
            $statement = $this->adapter->query($sql);
            $res       = $statement->execute();
            if ($res->count() > 0) {
                $rs = $res->current();
            } else
                $rs = false;
            if ($rs['password'] == md5($oldpass)) {
                $update = $this->sql->update();
                $update->table('users_user');
                $update->set(array(
                    'password' => md5($conpass)
                ));
                $update->where(array(
                    'ID' => $user_id
                ));
                $statement = $this->sql->prepareStatementForSqlObject($update);
                $results   = $statement->execute();
                return array(
                    'result' => 'success',
                    'msg' => 'Your password has been updated successfully'
                );
                /*$retvn='Password Updated Successfully';*/
            } else {
                return array(
                    'result' => 'failure',
                    'msg' => 'Your entered new password is mismatch with old passowrd'
                );
                /*$retvn='Old Password Mismatch';*/
            }
        } else {
            return array(
                'result' => 'failure',
                'msg' => 'Your new password and confirm password are mismatch'
            );
            /*$retvn='Password Mismatch';*/
        }
    }
    public function updatcanceleaccount($arr_address_detail, $user_id)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set($arr_address_detail);
        $update->where(array(
            'ID' => $user_id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function adduser($user)
    {
        $insert = $this->sql->insert('users_user');
        $insert->values($user);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $id           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $id;
    }
    public function Addcarddetails($user)
    {
        $insert = $this->sql->insert('users_user_card');
        $insert->values($user);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $id           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $id;
    }
    public function edituser($user, $userid)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set($user);
        $update->where(array(
            'user_id' => $user_id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function updatedefaddressval($userid)
    {
        $update = $this->sql->update();
        $update->table('users_user_card');
        $update->set(array(
            "default_card" => 0
        ));
        $update->where(array(
            'ID' => $userid
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function deletecarddetails($userid, $id)
    {
		$update = $this->sql->update();
        $update->table('users_user_card');
        $update->set(array(
            "invisibile" => 1
        ));
        $update->where(array(
            'user' => $userid,
            'id' => $id
        ));
		
		$statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
		
        /*$delete       = $this->sql->delete('users_user_card')->where(array(
            'user' => $userid,
            'id' => $id
        ));
        $deleteString = $this->sql->getSqlStringForSqlObject($delete);
        $results      = $this->adapter->query($deleteString, Adapter::QUERY_MODE_EXECUTE);*/
    }
    public function viewcarddetails($usrid, $ID)
    {
        $sql       = "SELECT * FROM users_user_card WHERE ID='" . $ID . "' AND user='" . $usrid . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    public function updatecarddetails($arr_peopleabs_detail, $userid, $id)
    {
        $update = $this->sql->update();
        $update->table('users_user_card');
        $update->set($arr_peopleabs_detail);
        $update->where(array(
            'user' => $userid,
            'ID' => $id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results   = $statement->execute();
    }
    public function deleteuser($userid)
    {
        $delete       = $this->sql->delete('users_user')->where("user = " . $userid);
        $deleteString = $this->sql->getSqlStringForSqlObject($delete);
        $results      = $this->adapter->query($deleteString, Adapter::QUERY_MODE_EXECUTE);
    }
    public function getUserBySocialId($type, $social_user_id)
    {
        if ($type == 'facebook')
            $condition = " facebook_user_id = '" . $social_user_id . "'";
        elseif ($type == 'linkedin')
            $condition = " linkedin_user_id = '" . $social_user_id . "'";
        elseif ($type == 'googleplus')
            $condition = " googleplus_user_id = '" . $social_user_id . "'";
        $sql       = "SELECT * FROM users_user WHERE $condition";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    /* user_transactions */
    function getUserTransactions($filters = false)
    {
        $select    = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from(array(
            'UT' => 'user_transactions'
        ))->join(array(
            'UP' => 'user_payments'
        ), 'UT.txn_pay_id = UP.pay_id', array(
            '*'
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }
            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach ($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }
            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            } else {
                $selectObj->order(array(
                    'txn_added' => 'DESC'
                ));
            }
        }
        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement    = $this->adapter->query($selectString);
        $res          = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    /* user_transactions */
    /* payout method */
    function getPayoutMethodSettingsByCountry($filters = false)
    {
        $select    = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('payout_method_settings');
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }
            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach ($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }
            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            } else {
                $selectObj->order(array(
                    'country_code' => 'ASC'
                ));
            }
        }
        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement    = $this->adapter->query($selectString);
        $res          = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    function getPayoutMethodFields($filters = false)
    {
        $select    = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('payout_method_fields');
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }
            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach ($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }
            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            } else {
                $selectObj->order(array(
                    'payout_method_setting_id' => 'ASC'
                ));
            }
        }
        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement    = $this->adapter->query($selectString);
        $res          = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
    function addEditUserPayoutMethod($uPayMethodAddress, $uPayMethodid = false)
    {
        if ($uPayMethodid === false || $uPayMethodid == '') {
            $insert = $this->sql->insert('user_payout_address');
            $insert->values($uPayMethodAddress);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $id           = $this->adapter->getDriver()->getLastGeneratedValue();
            return $id;
        } else {
            $update = $this->sql->update();
            $update->table('user_payout_address');
            $update->set($uPayMethodAddress);
            $update->where(array(
                'user_payout_address_id' => $uPayMethodid
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results   = $statement->execute();
            return $uPayMethodid;
        }
    }
    function deleteUserPayoutMethod($payoutMethodAddrId)
    {
        $delete       = $this->sql->delete('user_payout_address')->where(array(
            'user_payout_address_id' => $payoutMethodAddrId
        ));
        $deleteString = $this->sql->getSqlStringForSqlObject($delete);
        $results      = $this->adapter->query($deleteString, Adapter::QUERY_MODE_EXECUTE);
    }
    function deleteUserPayoutMethodFields($payoutMethodAddrId)
    {
        $delete       = $this->sql->delete('user_payout_methods_fields')->where(array(
            'user_payout_address_id' => $payoutMethodAddrId
        ));
        $deleteString = $this->sql->getSqlStringForSqlObject($delete);
        $results      = $this->adapter->query($deleteString, Adapter::QUERY_MODE_EXECUTE);
    }
    function addUserPayoutMethodFields($uPayMethodFields)
    {
        $insert = $this->sql->insert('user_payout_methods_fields');
        $insert->values($uPayMethodFields);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $id           = $this->adapter->getDriver()->getLastGeneratedValue();
        return $id;
    }
    function getUserPayoutMethods($filters = false, $resType = "list")
    {
        $select    = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from(array(
            'UPA' => 'user_payout_address'
        ))->join(array(
            'PMS' => 'payout_method_settings'
        ), 'UPA.payout_method_setting_id = PMS.payout_method_setting_id', array(
            '*'
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }
            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach ($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }
            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            } else {
                $selectObj->order(array(
                    'user_payout_address_id' => 'ASC'
                ));
            }
        }
        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement    = $this->adapter->query($selectString);
        $res          = $statement->execute();
        if ($resType == 'row') {
            if ($res->count() > 0) {
                return $res->current();
            } else
                return false;
        } else if ($resType == 'list') {
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            } else
                return false;
        }
    }
    function getUserPayoutMethodFields($filters = false)
    {
        $select    = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from(array(
            'UPMF' => 'user_payout_methods_fields'
        ))->join(array(
            'PMF' => 'payout_method_fields'
        ), 'UPMF.payout_method_field_id = PMF.payout_method_field_id', array(
            '*'
        ));
        if ($filters) {
            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach ($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }
        }
        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement    = $this->adapter->query($selectString);
        $res          = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }
	public function addremovecardasdefault($id,$userid,$type){
		if($type=='add'){
		$rem = array('default_card'=>'0');
		$ad = array('default_card'=>'1');
		$update = $this->sql->update();
            $update->table('users_user_card');
            $update->set($rem);
            $update->where(array(
                'user' =>$userid
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results   = $statement->execute();
			if($results){
			$update1 = $this->sql->update();
            $update1->table('users_user_card');
            $update1->set($ad);
            $update1->where(array(
				'ID' => $id,
                'user' =>$userid
            ));
            $statement1 = $this->sql->prepareStatementForSqlObject($update1);
            $results1   = $statement1->execute();
            return true;
			}
		}else{
			$ary = array('default_card'=>'0');
			$update = $this->sql->update();
            $update->table('users_user_card');
            $update->set($ary);
            $update->where(array(
                'ID' => $id,
				'user' =>$userid
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results   = $statement->execute();
            return true;
		}
			
	}
	
    /* payout method */
}
?>