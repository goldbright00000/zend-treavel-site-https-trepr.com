<?php
namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container;
use Zend\Mvc\Plugin\FlashMessenger;


class Common extends AbstractHelper {
    var $countries;
    var $addressTypes;
    var $comSessObj;
    
    public function __construct($data=false){
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
            	$modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->countries = $this->CommonMethodsModel->getCountries();
        
        $this->addressTypes = $data['configs']['siteConfigs']['address_type'];
        $this->common_model = $this->CommonMethodsModel;
        $this->comSessObj = $data['comSessObj'];
		
		$this->comSessObj['avg_rating'] = 0;
		if(isset($this->comSessObj['user']) && isset($this->comSessObj['user']['ID']) && $this->comSessObj['user']['ID'] > 0){
			$reviews = $this->CommonMethodsModel->getreviews($this->comSessObj['user']['ID']);
			if($reviews){
				foreach($reviews as $rKey=>$rev){
					$this->comSessObj['avg_rating'] += $reviews[$rKey]['rating'];
				}
				$this->comSessObj['avg_rating'] = number_format($this->comSessObj['avg_rating'] / count($reviews),0);
			}
		}
		
        $this->adminModules = $data['configs']['adminModules'];
        $this->adminMenus = $data['configs']['adminMenus'];
        $siteConfigRows = $this->common_model->getSiteConfigs();
        $siteConfRows = array();
        if($siteConfigRows){
            foreach($siteConfigRows as $configRow){
                $siteConfRows[$configRow['configName']] = $configRow['configValue'];
            }
        }
        $this->siteConfigs = $siteConfRows;
    }
 
    public function splitPhoneFormat($number) {
        $arr_number[] = substr($number, 1,3);
        $arr_number[] = substr($number, 6,3);
        $arr_number[] = substr($number, 10,4);
        return $arr_number;
    }
    
    public function timeZoneAdjust($format,$dat) {
        $dat = strtotime($dat);
        if(date('T',$dat)=='PST'){
            $dateadjusted = date($format,$dat+3600);
        } else {
            $dateadjusted = date($format,$dat);
        }
        return $dateadjusted;
    }
    
public function ThemeUrl($secured_url='http') //$secured_url - if the file is secured., we should use ThemeUrl('https') otherwise default is ThemeUrl() 
{
    //$front = Zend_Controller_Front::getInstance();
    //$this->currentController = $this->getRequest()->getControllerName();
    //if( ($this->currentController=='profile' && !empty($this->comSessObj->userid)) ){
    $this->currentController = 'index';
    if($this->currentController=='profile'){
        $secured_url = 1;
    }else{
        $secured_url = 0;
    }

    $themepath = $this->view->generalvar['themepath'];
    $themename = $this->view->generalvar['themename'];
    $application_run_mode = $this->view->generalvar['application_run_mode'];
    if($secured_url==1)
        $cdn_url_http = $this->view->generalvar['cdn_url_https'];
    elseif($secured_url==0)
        $cdn_url_http = $this->view->generalvar['cdn_url_http'];    

    if($application_run_mode=='development'){
        $themeurl = str_replace('index.php','',$_SERVER['PHP_SELF']).$themepath.'/'.$themename;
        return $themeurl;
    }elseif($application_run_mode=='deployment'){
        $themeurl = $cdn_url_http.$themepath.'/'.$themename;
        return $themeurl;
    }
}
    
    public function FileUrl($secured_url='https') //$secured_url - if the file is secured., we should use FileUrl('https') otherwise default is FileUrl() 
    {
        $themepath = $this->view->generalvar['themepath'];
        $filepath = $this->view->generalvar['filepath'];
        $application_run_mode = $this->view->generalvar['application_run_mode'];
        if($secured_url=='http')
            $cdn_url_http = $this->view->generalvar['cdn_url_http'];
        elseif($secured_url=='https')
            $cdn_url_http = $this->view->generalvar['cdn_url_https'];
        if($application_run_mode=='development'){
            $themeurl = str_replace('index.php','',$_SERVER['PHP_SELF']).$themepath.'/'.$filepath;
            return $themeurl;
        } elseif ($application_run_mode=='deployment'){
            $themeurl = $cdn_url_http.$themepath.'/'.$filepath;
            return $themeurl;
        }
    }
	
	public function getCurrentURL(){
		return $this->view->url();
	}

    public function urlStringConvert($str) {
        $str=trim($str);$existdata = array(" ", "&", ",","'","?","/");$replacedata   = array("-", "and","","","","-");$covertstr = strtolower(str_replace($existdata, $replacedata, $str));$covertstr = urlencode($covertstr);return $covertstr;
    }
    
    public function getPrimaryImage($moduleid,$type){
        $image = $this->CommonMethodsModel->getPrimaryImage($moduleid,$type);
        if($image['image'])
        $image = $this->CommonMethodsModel->splitListingImages($image['image']);
        return $image;
    }

    public function datediffcal($from,$to){
        $days = floor((strtotime($from) - strtotime($to))/(60*60*24));
        return $days;
    }
    function getSiteCurrency(){
        $SiteCurrency =  $this->CommonMethodsModel->getSiteCurrency();
        return $SiteCurrency;
        
    }
    function getUserCurrency(){
         $UserCurrency =  $this->CommonMethodsModel->getUserCurrency($this->comSessObj->currency);
         return $UserCurrency;
        
    }
    function getUserCurrencySymbol(){
         $UserCurrency =  $this->CommonMethodsModel->getUserCurrencySymbol($this->comSessObj->currencycode);
         return $UserCurrency;
        
    }
    function convertCurrency($amount,$from, $to){
         $converedAmount =  $this->CommonMethodsModel->convertCurrency($amount,$from, $to);
         return $converedAmount;
    }
    
    function showFlashMessage($flashMessenger = array()){
        var_dump($flashMessenger);exit;
        if (!empty($flashMessages)){
            $messageString = '<div class="row">';
             foreach ($flashMessages as $msg) {
                 foreach ($msg as $type => $message) {
                     $messageString .= '<div class="col-xs-12 msg_div">'
                                     .'<div class="alert alert-dismissable <?php echo $type; ?> alert-hide">'
                                     . '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">'
                                     . '<span aria-hidden="true">×</span></button><?php echo $message; ?></div>'
                                     . '</div>';
                }
            }
            return $messageString;
        }
    }
    public function getAirPort($ID){
      $arrAirPorts = $this->CommonMethodsModel->getAirPort($ID);	    
      return $arrAirPorts;
    }
}