<?php
/**
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2016 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Router\Http\Regex;
use Application\Route\StaticRoute;
use Zend\Session\Container;

use Application\Factory\IndexFactory;
use Application\Factory\MyaccountFactory;
use Application\Factory\SeekerFactory;
use Application\Factory\TravellerFactory;
use Application\Factory\HelperFactory;
use Application\Factory\DashboardFactory;
use Application\Factory\UserprofileFactory;
use Application\Factory\ProfileFactory;
use Application\Factory\TripsFactory;
use Application\Factory\AjaxFactory;

use Application\Model\CommonMethodsModel;
use Application\Model\Currencyrate;

//use Application\Factory\TripPluginFactory;
use Application\Controller\Plugin\TripPlugin;
use Application\Controller\Plugin\Factory\TripPluginFactory;


return [
    'router' => [
        'routes' => [
            'home' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
			'indexnewdesign' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/indexnewdesign/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'indexnewdesign',
                    ],
                ],
            ],
            'application' => [
                'type'    => Literal::class,
                'options' => [
                    'route' =>  '/application',
                    'defaults'  =>  [
                        '__NAMESPACE__' =>  'Application\Controller',
                        'controller'    =>  'Index',
                        'action'        =>  'index' 
                    ],
                ],
            ],
			'user' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '[/:ID]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'index',
                        'ID'        => 0,
                    ],
                ],
            ],
            'becometraveler' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/becometraveler',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'becometraveler',
                    ],
                ],
            ],
            'becometraveler-signup' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/signupBT',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'signupBT',
                    ],
                ],
            ],
            'becometraveler-param' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/becometraveler[/:steps]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'becometraveler',
                        'steps' => ''
                    ],
                ],
            ],
            'seeker' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            
             'sendflightstatus' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/sendflightstatus[/:ID]',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'sendflightstatus',
                        'ID'        => 0,
                    ],
                ],
            ],
            'trackflightpoistion' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/trackflightpoistion',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'trackflightpoistion',
                    ],
                ],
            ],
            'checkflightstatus' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/checkflightstatus',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'checkflightStatus',
                    ],
                ],
            ],
            'checkdocumentstatus' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/checkdocumentstatus',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'checkdocumentstatus',
                    ],
                ],
            ],
			'checkticketstatus' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveler/checkticketstatus',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'checkticketstatus',
                    ],
                ],
            ],
            'seeker-add-people-service' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/add-people-service[/:ID]',
                    'defaults' => [
                        'controller'=> Controller\SeekerController::class,
                        'action'    => 'addPeopleService',
                        'ID'        => 0,
                    ],
                ],
            ],
            'seeker-add-package-service' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/add-package-service[/:ID]',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'addPackageService',
                        'ID'         => 0,
                    ],
                ],
            ],
            'seeker-add-project-service' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/add-project-service[/:ID]',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'addProjectService',
                        'ID'         => 0,
                    ],
                ],
            ],
            'seeker-details' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/detail[/:ID]',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'detail',
                    ],
                ],
            ],
            'seeker-receipt' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/receipt[/:ID]',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'receipt',
                    ],
                ],
            ],
            'traveller-details' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/detail[/:ID]',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'detail',
                    ],
                ],
            ],
             'traveller-details-param' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/traveller/detail[/:ID]?[:seeker_package_request_id]',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'detail',
                    ]
                ]
            ],
            'traveller-receipt' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/receipt[/:ID]',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'receipt',
                    ],
                ],
            ],
            'seeker-listingandconfirm' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/listingandconfirm[/:trip_id][/:service]',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'listingandconfirm',
                        'trip_id'    => 0,
                    ],
                ],
            ],
             'updatetrip' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/update-trip',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'updateTrip',
                        'trip_id'    => 0,
                    ],
                ],
            ],
            'traveller-listingandconfirm' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/listingandconfirm[/:trip_id][/:service]',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'listingandconfirm',
                        'trip_id'    => 0,
                    ],
                ],
            ],
            
            'seeker-detail' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/detail[/:ID]',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'detail',
                        'ID'    => 0,
                    ],
                ],
            ],
            'traveller' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            
             'add-traveller-people-request' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/add-traveller-people-request',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addTravellerPeopleRequest',
                    ],
                ],
            ],
			
			'add-new-address'=> [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/add-new-address',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addNewAddress',
                    ],
                ],
            ],
             'add-trip-location' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/add-trip-location',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addTripLocation',
                    ],
                ],
            ],
            
            
           'add-traveller-package-request' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/add-traveller-package-request',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addTravellerPackageRequest',
                    ],
                ],
            ],
            
            'add-traveller-project-request' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/add-traveller-project-request',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addTravellerProjectRequest',
                    ],
                ],
            ],
			
			'traveller-cancel-service-request' => [
				'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/cancel-service-request',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'serviceRequestCancel',
                    ],
                ],
			],
            
             'traveller-approve-people-request' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/approve-people-request',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'approvePeopleRequest',
                    ],
                ],
            ],
             'traveller-approve-package-request' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/approve-package-request',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'approvePackageRequest',
                    ],
                ],
            ],
            
             'traveller-approve-project-request' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/approve-project-request',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'approveProjectRequest',
                    ],
                ],
            ],
            
             'get-traveller-package-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/get-traveller-package-service-need',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'getTravellerPackageServiceNeed',
                    ],
                ],
            ],
            
             'get-traveller-project-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/get-traveller-project-service-need',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'getTravellerProjectServiceNeed',
                    ],
                ],
            ],
            
            
            'traveller-seekerrequests' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/seekerrequests',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'seekerrequests',
                    ],
                ],
            ],
             'seeker-tripcontenders' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/tripcontenders[/:ID]',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'tripcontenders',
                        'ID'    => 0,
                    ],
                ],
            ],
             'traveler-tripcontenders' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/tripcontenders[/:ID]',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'tripcontenders',
                        'ID'    => 0,
                    ],
                ],
            ],
			'traveler-requestexpcron' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveler/requestexpcron',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'requestexpcron'
                    ],
                ],
            ],
            'traveller-uploadticket' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/uploadticket',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'uploadTicket',
                        'trip_id'    => 0,
                    ],
                ],
            ],
             'traveller-composeemail' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/composeEmail',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'composeEmail',
                        'trip_id'    => 0,
                    ],
                ],
            ],
            'traveller-add-people-service' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/add-people-service',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addPeopleService',
                    ],
                ],
            ],
             'bt-traveller-add-people-service' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/addBecameTravellerService',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addBecameTravellerService',
                    ],
                ],
            ],
             'traveller-add-people-return-service' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/add-people-service[/:ID][/:TYPE]',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addPeopleService',
                         
                        
                    ],
                ],
            ],
             'traveller-add-people-service' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/add-people-service[/:ID]',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addPeopleService',
                        'ID'    => 0,
                    ],
                ],
            ],
            
            'traveller-add-package-service' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/add-package-service',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addPackageService',
                    ],
                ],
            ],
            'traveller-add-package-return-service' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/add-package-service[/:ID][/:TYPE]',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addPackageService',
                         
                        
                    ],
                ],
            ],
            'traveller-add-traveller-trip-request' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/add-traveller-trip-request',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addTravellerTripRequest',
                    ],
                ],
            ],
             'traveller-add-traveller-trip-request-by-paypal' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/add-traveller-trip-request-by-paypal',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addTravellerTripRequestByPaypal',
                    ],
                ],
            ],
            
             'traveller-add-seeker-trip-request' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/add-seeker-trip-request',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'addSeekerTripRequest',
                    ],
                ],
            ],
            
            'traveller-add-package-service' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/add-package-service[/:ID]',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addPackageService',
                        'ID'    => 0,
                    ],
                ],
            ],
             'traveller-send-report-mail' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/send-report-mail',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'sendreportmail',
                    ],
                ],
            ],
            'seeker-send-report-mail' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/send-report-mail',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'sendreportmail',
                    ],
                ],
            ],
              'seeker-add-seeker-people-trip-request-by-paypal' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/add-seeker-trip-request-by-paypal',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'addSeekerTripRequestByPaypal',
                    ],
                ],
            ],
            'traveller-send-message-mail' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/send-message-mail',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'sendmessagemail',
                    ],
                ],
            ],
            'seeker-send-message-mail' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/send-message-mail',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'sendmessagemail',
                    ],
                ],
            ],
             'traveller-add-project-service' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/add-project-service',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addProjectService',
                    ],
                ],
            ],
            'traveller-add-project-service' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/add-project-service[/:ID]',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addProjectService',
                         'ID'    => 0,
                    ],
                ],
            ],
             'traveller-add-project-return-service' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/add-project-service[/:ID][/:TYPE]',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'addProjectService',
                         
                        
                    ],
                ],
            ],
            'traveller-project-service-on-location' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/project-service-on-location',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'projectserviceonlocation',
                    ],
                ],
            ],
            'traveller-getprojectsubcat' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/getprojectsubcat',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'getprojectsubcat',
                    ],
                ],
            ],
            'traveller-getproductsubcat' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/getproductsubcat',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'getproductsubcat',
                    ],
                ],
            ],
            
            
            'checkuseremail' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/checkuseremail',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'checkuseremail',
                    ],
                ],
            ],
             'user-wishlist' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/wishlist',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'wishlist',
                    ],
                ],
            ],
			 'user-wishlist-request' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/wishlist-request/',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'wishlistrequest',
                    ],
                ],
            ],
            'user-myexpenses' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/myexpenses',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'myexpenses',
                    ],
                ],
            ],
            'user-myearnings' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/myearnings',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'myearnings',
                    ],
                ],
            ],
             'user-mytriphistory' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/mytriphistory',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'mytriphistory',
                    ],
                ],
            ],
            
            
              'checklogin' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/checklogin',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'checklogin',
                    ],
                ],
            ],
             'getpassengersfee' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/getpassengersfee',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'getpassengersfee',
                    ],
                ],
            ],
              'get-traveller-people-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/get-traveller-people-service-need',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'getTravellerPeopleServiceNeed',
                    ],
                ],
            ],
             'check-traveller-people-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/check-traveller-people-service-need',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'checkTravellerPeopleServiceNeed',
                    ],
                ],
            ],
            'get-seeker-people-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/get-seeker-people-service-need',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'getSeekerPeopleServiceNeed',
                    ],
                ],
            ],
             'check-traveller-project-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/check-traveller-project-service-need',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'checkTravellerProjectServiceNeed',
                    ],
                ],
            ],
            'get-seeker-project-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/get-seeker-project-service-need',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'getSeekerProjectServiceNeed',
                    ],
			
                ],
				
            ],
            'get-seeker-package-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/get-seeker-package-service-need',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'getSeekerPackageServiceNeed',
                    ],
                ],
            ],
             'check-traveller-package-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/traveller/check-traveller-package-service-need',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'checkTravellerPackageServiceNeed',
                    ],
                ],
            ],
			
            'get-seeker-package-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/get-seeker-package-service-need',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'getSeekerPackageServiceNeed',
                    ],
			
                ],
            ],
             'check-seeker-people-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/check-seeker-people-service-need',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'checkSeekerPeopleServiceNeed',
                    ],
                ],
            ],
			  'check-seeker-package-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/check-seeker-package-service-need',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'checkSeekerPackageServiceNeed',
                    ],
                ],
            ],
            'check-seeker-project-service-need' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/check-seeker-project-service-need',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'checkSeekerProjectServiceNeed',
                    ],
                ],
            ],
             'resetpasswordsubmit' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/resetpasswordsubmit',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'resetpasswordsubmit',
                    ],
                ],
            ],
            'resetpassword' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/resetpassword',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'resetpassword',
                    ],
                ],
            ],
            'search' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/search',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'search',
                    ],
                ],
            ],
            'seacrh-details-param' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/search?[:trip]',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'detail',
                    ]
                ]
            ],
             'search-airport' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/search-airport',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'searchairport',
                    ],
                ],
            ],
            'search-airlines' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/search-airlines',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'searchairlines',
                    ],
                ],
            ],
             'signin' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/signin',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'signin',
                    ],
                ],
            ],
            'logout' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/logout',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'logout',
                    ],
                ],
            ],
            'dashboard' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/dashboard',
                    'defaults' => [
                        'controller' => Controller\DashboardController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            
            'notifications' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/dashboard/notifications',
                    'defaults' => [
                        'controller' => Controller\DashboardController::class,
                        'action'     => 'notifications',
                    ],
                ],
            ],
            'viewtravelenquiry' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/dashboard/viewtravelenquiry',
                    'defaults' => [
                        'controller' => Controller\DashboardController::class,
                        'action'     => 'viewtravelenquiry',
                         
                        
                    ],
                ],
            ],
			'viewseekerenquiry' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/dashboard/viewseekerenquiry',
                    'defaults' => [
                        'controller' => Controller\DashboardController::class,
                        'action'     => 'viewseekerenquiry',
                         
                        
                    ],
                ],
            ],
            'requestgoogleplus' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/requestgoogleplus',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'requestgoogleplus',
                    ],
                ],
            ],
            'requestgoogleplus' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/requestgoogleplus',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'requestgoogleplus',
                    ],
                ],
            ],
            'requestfacebook' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/requestfacebook',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'requestfacebook',
                    ],
                ],
            ],
            'socialsignup' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/socialsignup',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'socialsignup',
                    ],
                ],
            ],
            'signup' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/signup',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'signup',
                    ],
                ],
            ],
            'seeker-controller' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker[/:action]',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'trips-getplandetails' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/getplandetails',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'getplandetails',
                    ],
                ],
            ],
            'myprofile' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/myprofile[/:action]',
                    'defaults' => [
                        'controller' => Controller\UserprofileController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'user-ajax' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/ajax[/:action]',
                    'defaults' => [
                        'controller' => Controller\AjaxController::class,
                        'action'     => 'getLatLngByAddressId',
                    ],
                ],
            ],
            'ajax-get-peoplefeesrc' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/ajax/getpeoplesfeesrc',
                    'defaults' => [
                        'controller' => Controller\AjaxController::class,
                        'action'     => 'getpeoplesfeesrc',
                    ],
                ],
            ],
             'ajax-get-packagefeesrc' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/ajax/getpackagesfeesrc',
                    'defaults' => [
                        'controller' => Controller\AjaxController::class,
                        'action'     => 'getpackagesfeesrc',
                    ],
                ],
            ],
            'ajax-get-projectfeesrc' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/ajax/getprojectsfeesrc',
                    'defaults' => [
                        'controller' => Controller\AjaxController::class,
                        'action'     => 'getprojectsfeesrc',
                    ],
                ],
            ],
            'trips' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/trips[/:action]',
                    'defaults' => [
                        'controller' => Controller\TripsController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'trips-oneway' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/trips/oneway',
                    'defaults' => [
                        'controller' => Controller\TripsController::class,
                        'action'     => 'oneway',
                    ],
                ],
            ],
            
             'get-payment-details' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/trips/getCardDetails',
                    'defaults' => [
                        'controller' => Controller\TripsController::class,
                        'action'     => 'getCardDetails',
                    ],
                ],
            ],
            
            'user-view' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/user/view[/:user_id]',
                    'defaults' => [
                        'controller' => Controller\UserprofileController::class,
                        'action'     => 'showuser',
                        'user_id'    => 'sfdfsdf'
                    ],
                ],
            ],
            'mailtemplate_design' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/mailtemplate',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'mailtemplate',
                    ],
                ],
            ],
            
             'mailactive' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/mailactive',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'mailactive',
                    ],
                ],
            ],
            'switchUser' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/switchuser[/:userrole]',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'switchuser',
                        'userrole'   => 'seeker'
                    ],
                ],
            ],
            'getcountry' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/getcountry',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'getcountry',
                    ],
                ],
            ],
            'download-file' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/download[/:folder][/:file]',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AjaxController::class,
                        'action'     => 'download',
                    ]
                ]
            ],
            
             'about' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/about',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'about',
                    ],
                ],
            ],
            'userterms' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/userterms',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'userterms',
                    ],
                ],
            ],
            'privacy' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/privacy',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'privacy',
                    ],
                ],
            ],
            'copyright' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/copyright',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'copyright',
                    ],
                ],
            ],
            'securitypolicy' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/securitypolicy',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'securitypolicy',
                    ],
                ],
            ],
            'prohibited' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/prohibited',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'prohibited',
                    ],
                ],
            ],
             'merchants' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/merchants',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'merchants',
                    ],
                ],
            ],
             'support' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/support',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'help',
                    ],
                ],
            ], 
            'weight-and-dimensions' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/weight-and-dimensions',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'weightanddimensions',
                    ],
                ],
            ],
            'service-rate-calculations' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/service-rate-calculations',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'serviceratecalculations',
                    ],
                ],
            ],
          'service-rate-calculations-traveller' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/service-rate-calculation-traveller',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'serviceratecalculationstrav',
                    ],
                ],
            ],
             'customs' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/customs',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'customs',
                    ],
                ],
            ],
            'contact-us' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/contact-us',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'contactus',
                    ],
                ],
            ],
             
            'news-rooms' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/news-rooms',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'newsrooms',
                    ],
                ],
            ],
            
            'get-distance' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/get-distance',
                    'defaults' => [
                        'controller' => Controller\TripsController::class,
                        'action'     => 'getDistance',
                    ],
                ],
            ],
           'my-account' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/myaccount[/:action]',
                    'defaults' => [
                        'controller' => Controller\MyaccountController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'traveller-trip-status-change' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/change-trip-status',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'changeTripStatus',
                    ],
                ],
            ],
            'seeker-trip-status-change' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/change-trip-status',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'changeTripStatus',
                    ],
                ],
            ],
            'seeker-people-paypal-status-success' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/people-paypal-success-payment',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'paypalpeopleSuccessPayment',
                    ],
                ],
            ],
              'seeker-package-paypal-status-success' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/package-paypal-success-payment',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'paypalpackageSuccessPayment',
                    ],
                ],
            ],
              'seeker-project-paypal-status-success' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/seeker/project-paypal-success-payment',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'paypalprojectSuccessPayment',
                    ],
                ],
            ],
             'traveller-people-paypal-status-success' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/people-paypal-success-payment',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'paypalpeopleSuccessPaymentTraveller',
                    ],
                ],
            ],
              'traveller-package-paypal-status-success' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/package-paypal-success-payment',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'paypalpackageSuccessPaymentTraveller',
                    ],
                ],
            ],
              'traveller-project-paypal-status-success' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/traveller/project-paypal-success-payment',
                    'defaults' => [
                        'controller' => Controller\TravellerController::class,
                        'action'     => 'paypalprojectSuccessPaymentTraveller',
                    ],
                ],
            ],
            'user-viewq' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/myprofile/te',
                    'defaults' => [
                        'controller' => Controller\UserprofileController::class,
                        'action'     => 'testingmail'
                        //'user_id'    => 'sfdfsdf'
                    ],
                ],
            ],
            'help-search' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/help-search',
                    'defaults' => [
                        'controller' => Controller\IndexController::class,
                        'action'     => 'helpSearch',
                    ],
                ],
            ],
			
        ],
    ],
    'controllers' => [
        'factories' => [
            'Application\\Controller\\IndexController' => IndexFactory::class,
            'Application\\Controller\\DashboardController' => DashboardFactory::class,
            'Application\\Controller\\UserprofileController' => UserprofileFactory::class,
            'Application\\Controller\\CurrencyrateController' => InvokableFactory::class,
            'Application\\Controller\\ErrorController' => InvokableFactory::class,
            'Application\\Controller\\MyaccountController' => MyaccountFactory::class,
            'Application\\Controller\\ProfileController' => ProfileFactory::class,
            'Application\\Controller\\SeekerController' => SeekerFactory::class,
            'Application\\Controller\\TravellerController' => TravellerFactory::class,
            'Application\\Controller\\TripsController' => TripsFactory::class,
            'Application\\Controller\\AjaxController' => AjaxFactory::class,
        ],
        'invokables' => [
            /*'Application\\Controller\\IndexController' => IndexFactory::class,
            'Application\\Controller\\ProfileController' => ProfileFactory::class*/
        ],
        'aliases' => [
            //'index' => 'Application\Controller\Index',
            //'profile' => 'Application\Controller\Profile'
        ]
    ],
    'controller_plugins' => [
        'aliases' => [
            'TripPlugin' => Controller\Plugin\TripPlugin::class,
        ],
        'factories' => [
            Controller\Plugin\TripPlugin::class => TripPluginFactory::class
        ],
        'invokables' => [
            'SessionPlugin' => 'Application\Controller\Plugin\SessionPlugin',
            'CommonPlugin' => 'Application\Controller\Plugin\CommonPlugin',
            'GoogleplusPlugin' => 'Application\Controller\Plugin\GoogleplusPlugin',
        ],
        
    ],
    'view_manager' => [
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/index/index' => __DIR__ . '/../view/application/index/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
            'helper/switch-user'   => __DIR__ . '/../view/application/helpers/switch-user.phtml'
        ],
        'template_path_stack' => [
            __DIR__ . '/../view',
            'application' => __DIR__ . '/../view/helpers',
        ],
        //'base_path' => '/trepr/site_beta/public'
    ],
    'view_helpers' => [
        'factories' => [
            //View\Helper\Common::class => InvokableFactory::class
            View\Helper\Common::class => HelperFactory::class
        ],
        'aliases' => [
            'common' => View\Helper\Common::class
        ],
    ],
    'service_manager'=>[
        'initializers' => [
            function ($instance, $sm) {
                if ($instance instanceof \Zend\Db\Adapter\AdapterAwareInterface) {
                    $instance->setDbAdapter($sm->get('Zend\Db\Adapter\Adapter'));
                }
            }
        ],
        'invokables' => [
            'Application\\Model\\CommonMethodsModel' => CommonMethodsModel::class,
            'Application\\Model\\Currencyrate' => Currencyrate::class
        ],
        'aliases' => [
            'common_model' => Application\Model\CommonMethodsModel::class
        ],  
        'abstract_factories' => [
           \Zend\Db\Adapter\AdapterAbstractServiceFactory::class,
        ]
    ]
    
];