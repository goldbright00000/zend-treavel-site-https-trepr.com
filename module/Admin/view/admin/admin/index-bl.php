<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

 <?= $this->render('layout/admin-top') ?>
<script type="text/javascript">
 $(document).ready(function(){
	$(document).on('click', "#goto_forgot", function() {
		clrErr();
 		 $('#login_panel').hide();
		 reloadCaptcha('captchaForgotImg');
		 $('#forgot_panel').fadeIn();
 		 return false;	 
 	});
	$(document).on('click', "#goto_login", function() {
		clrErr();
 		 $('#forgot_panel').hide();
		 reloadCaptcha('captchaImg');
		 $('#login_panel').fadeIn();
 		 return false;	 
 	});
	$(document).on('click', ".changeCaptcha", function() {
 		reloadCaptcha($(this).attr('id').replace('id_',''));
		 return false;	 
 	}); 
	 
}); 
function reloadCaptcha(captcha_id){
    $('#'+captcha_id).fadeOut();
    $.ajax({ url :"<?php echo $this->basePath(); ?>admin/ajax/loadCaptchaImg",
        success : function (resData) { 
            $('#'+captcha_id).html(resData);
            $('#'+captcha_id).append('<a style="margin:10px 2px; position:absolute;" href="#" class="changeCaptcha" id="'+captcha_id+'" ><?php echo img('assets/images/admin/reload_captcha.png', TRUE);?> </a>');
            console.log(resData);
            $('#'+captcha_id).fadeIn("slow");
        }
    });	
}
function validateForm(){
	clrErr();
	$errStaus = false; 
   	if(validateFormFields('adminLoginName','Please enter your user name.',''))$errStaus=true;
	if(validateFormFields('adminLoginPass','Please enter your password.',''))$errStaus=true;
	if(validateFormFields('adminLoginCode','Please enter the verify code.',''))$errStaus=true;
	if($errStaus) {
		return false;
	} else {
	return true;		
	}
 		
 }
 function validateForgotForm(){
 	clrErr();
	$errStaus = false; 
   	if(validateFormFields('adminForgotEmail','Please enter your user name.',''))$errStaus=true;
	if(validateFormFields('adminForgotCode','Please enter the verify code.',''))$errStaus=true;
	if($errStaus) {
		return false;
	} else {
	return true;		
	}
}
</script>
</head>

<body style="background:#fff;">
<div id="wrapper wrap_login_page" class="login-main"> 
   <!-- Sidebar -->
   <div class="col-md-4  col-md-6 col-sm-8 col-xs-12 login_user_div">
    <div class="" id="messageRow">
      <?php 
  		$errMsg = validation_errors('* ');
		if(empty($errMsg)){ $errMsg = $this->session->flashdata('errMsg');  }
		$warningMsg = $this->session->flashdata('warningMsg');
		$successMsg = $this->session->flashdata('successMsg');
		if(isset($errMsg) && !empty($errMsg)){ echo showMessage($errMsg,'error'); }  
		if(isset($warningMsg) && !empty($warningMsg)){ echo showMessage($warningMsg,'warning'); }
		if(isset($successMsg) && !empty($successMsg)){ echo showMessage($successMsg,'success'); }
 	?>
    </div>
    <div class="bg-white">
      <div class="col-md-12 text-center login-logo"> <a href="<?php echo $this->basePath(); ?>" target="_blank"><img src="<?php echo $this->basePath(); ?>assets/images/admin/logo.jpg"  alt="<?php echo site_name(); ?>" title="<?php echo site_name(); ?>"></a> </div>
      <div class="panel panel-primary" id="login_panel" >
        <div class="panel-heading" style="margin-bottom:15px;">
          <h3 class="panel-title panel_title_login text-left"><i class="fa fa-lock"></i> Login</h3>
        </div>
        <div class="panel-body">
          <?php
			$actUrl = 'admin/login/setLogin';	 
 		 echo form_open($actUrl,array('name' => 'loginFrm', 'id' => 'loginFrm' ,'onsubmit'=>'return validateForm();'));
		  
			?>
          <div class="form-group input-group" id="div_adminLoginName"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <?php   echo form_input(array( 'name' => 'adminLoginName',  'id' => 'adminLoginName',  'placeholder'   => 'Username', 'class' => 'form-control', ));  ?>
           
          </div> <p class="text-danger err_msg" id="err_adminLoginName"></p>
          <div class="form-group input-group" id="div_adminLoginPass"> <span class="input-group-addon"><i class="fa fa-key"></i></span>
            <?php  echo form_password(array( 'name' => 'adminLoginPass',  'id' => 'adminLoginPass',  'placeholder'   => 'Password', 'class' => 'form-control', )); 	?>
            
          </div> <p class="text-danger err_msg" id="err_adminLoginPass"></p>
          <div class="form-group" >
            <div class="row">
              <div class="col-xs-4 ">
                <div class="cap-img" id="captchaImg" style="width:100% !important" >
                  <?php   $this->session->unset_userdata('S4A_CAPTCHA');
							$captcha = create_captcha(captcha_config());
							$this->session->set_userdata('S4A_CAPTCHA', $captcha['word']);
							echo  $captcha['image']; ?>
                  <a style="margin:10px 2px; position:absolute;" href="#" class="changeCaptcha" id="id_captchaImg" ><?php echo img('assets/images/admin/reload_captcha.png', TRUE);?> </a> </div>
              </div>
              <div class="col-xs-8" id="div_adminLoginCode">
                <?php  echo form_input(array( 'name' => 'adminLoginCode',  'id' => 'adminLoginCode',    'class' => 'form-control',  )); ?> <p class="text-danger err_msg" id="err_adminLoginCode"></p>
              </div>
             </div>
          </div>
           <div class="form-group text-left">
            <?php   echo form_button(array( 'name' => 'loginBtn','value' => 'true', 'id' => 'loginBtn',  'type' => 'submit','content' => 'Login', 'class' => 'btn btn-login btn-padding', )); ?>
            &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" id="goto_forgot">Forgot Password</a> </div>
          <?php echo form_close();?> </div>
      </div>
      <div class="panel panel-primary" id="forgot_panel" style="display:none;">
        <div class="panel-heading" style="margin-bottom:15px;">
          <h3 class="panel-title panel_title_login text-left"><i class="fa fa-key"></i> Forgot Password</h3>
        </div>
        <div class="panel-body">
          <?php
			$actUrl = 'admin/login/forgot';	 
 		 echo form_open($actUrl,array('name' => 'forgotFrm', 'id' => 'forgotFrm' ,'onsubmit'=>'return validateForgotForm();'));
			?>
          <div class="form-group input-group" id="div_adminForgotEmail"> <span class="input-group-addon"><i class="fa fa-user"></i></span>
            <?php   echo form_input(array( 'name' => 'adminForgotEmail',  'id' => 'adminForgotEmail',  'placeholder'   => 'Username', 'class' => 'form-control', ));  ?>
          </div><p class="text-danger err_msg" id="err_adminForgotEmail"></p> 
          <div class="form-group" >
            <div class="row">
              <div class="col-xs-4 ">
                <div class="cap-img" id="captchaForgotImg" style="width:100% !important" >
                  <?php   $this->session->unset_userdata('S4A_CAPTCHA');
							$captcha = create_captcha(captcha_config());
							$this->session->set_userdata('S4A_CAPTCHA', $captcha['word']);
							echo  $captcha['image']; ?>
                  <a style="margin:10px 2px; position:absolute" href="#" class="changeCaptcha" id="id_captchaForgotImg" ><?php echo img('assets/images/admin/reload_captcha.png', TRUE);?> </a> </div>
              </div>
              <div class="col-xs-8 " id="div_adminForgotCode">
                <?php  echo form_input(array( 'name' => 'adminForgotCode',  'id' => 'adminForgotCode',    'class' => 'form-control',   )); ?><p class="text-danger err_msg" id="err_adminForgotCode"></p>
              </div>
            </div>
          </div>
         
          <div class="form-group text-left">
            <?php   echo form_button(array( 'name' => 'loginBtn','value' => 'true', 'id' => 'loginBtn',  'type' => 'submit','content' => 'Submit', 'class' => 'btn btn-login btn-padding', )); ?>
            &nbsp;&nbsp;&nbsp;&nbsp;<a href="#" id="goto_login">Click Here to Login</a> </div>
          <?php echo form_close();?> </div>
      </div>
    </div>
  </div>
</div>
</body>
</html>
