<?php
namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\DriverInterface;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\ServiceManager\ServiceManager;
use Interop\Container\ContainerInterface;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;

class AdminUserModel extends AbstractTableGateway implements AdapterAwareInterface
{

    protected $table = 'core_country';
    protected $adapter;
    protected $sql;
    protected $generalvar;

    public function __construct(Adapter $adapter, $siteConfigs = false)
    {
        $this->adapter = $adapter;
        $this->sql = new Sql($this->adapter);
        $this->generalvar = $siteConfigs['siteConfigs'];
    }

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();

    }


    public function cleanQuery($string)
    {

        if (get_magic_quotes_gpc()) {
            $string = stripslashes($string);
        }

        if (phpversion() >= '4.3.0') {
        } else {
            $string = mysql_escape_string($string);
        }

        return $string;
    }

    public function trimLength($s, $maxlength)
    {
        $words = explode(' ', $s);
        $split = '';
        foreach ($words as $word) {
            if (strlen($split) + strlen($word) < $maxlength)
                $split .= $word . ' ';
            else
                break;
        }

        if (strlen($s) > $maxlength) {
            $split = trim($split) . " ...";
        }

        return $split;
    }

    public function splitImages($img)
    {
        $img = list($img_name, $img_type) = explode(".", $img);
        $xs = $img_name . "_xs." . strtolower($img_type);
        $th = $img_name . "_th." . strtolower($img_type);
        $logo = $img_name . "_logo." . strtolower($img_type);
        $reg = $img_name . "." . strtolower($img_type);
        $lrg = $img_name . "_lg." . strtolower($img_type);
        $as = $img_name . "_as." . strtolower($img_type);
        $og = $img_name . "_og." . strtolower($img_type);
        $arr_imgs = array("small" => $xs, "thumbnail" => $th, "standard" => $reg, "large" => $lrg, "actual" => $as, "logo" => $logo, "original" => $og);
        return $arr_imgs;
    }

    public function timeZoneAdjust($format, $dat)
    {
        $dat = strtotime($dat);
        if (date('T', $dat) == 'PST') {
            $dateadjusted = date($format, $dat + 3600);
        } else {
            $dateadjusted = date($format, $dat);
        }
        return $dateadjusted;
    }

    public function cleanTags($txt)
    {
        $specialstuff = array("<br />", "<br>", "<ul>", "<li>", "</li>", "</ul>", "<ol>", "</ol>");
        $txt = str_replace($specialstuff, " ", stripslashes($txt));
        return $txt;
    }

    public function in_array_r($needle, $haystack)
    {
        foreach ($haystack as $item) {
            if ($item === $needle || (is_array($item) && $this->in_array_r($needle, $item))) {
                return true;
            }
        }
        return false;
    }

    public function getCountries()
    {
        $sql = "SELECT * FROM core_country";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->getResource()->fetchAll();
    }

    public function getCountry($countryid)
    {
        $countrysql = "SELECT * FROM core_country WHERE ID='" . $countryid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;


        /*$result = $this->_db->fetchRow($countrysql);
        if ($result) {
            return $result;
        }*/
    }

    public function getCurrencies()
    {
        $sql = "SELECT * FROM core_currency_rate";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function checkAdmin($login, $pass)
    {


        $pwdhash = md5($pass);


        $sectionsql = "SELECT ID,type,first_name,email_address,active FROM users_user WHERE email_address=" . $login . " AND password='" . $pwdhash . "' AND active='1'";

        $statement = $this->adapter->query($sectionsql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->current();
        } else return false;


    }

    public function getUserRow($ID)
    {
        $sql = "SELECT * FROM users_user WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getUserPhotos($ID)
    {
        $sql = "SELECT * FROM user_photos WHERE user_id='".$ID."'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }


    public function EditBelowPhotos($user_id,$photo_id)
    {
        $sql = "SELECT * FROM user_photos WHERE user_id='".$user_id."' and user_photo_id!='".$photo_id."' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }
    

    public function getUserRowAll()
    {
        $sql = "SELECT * FROM users_user";
        //print_r("SELECT * FROM users_user");exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getUserTripsAll()
    {
        $sql = "SELECT * FROM trips";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function userAccouts($filterData = array(), $ID = false)
    {
        $sql = "SELECT * FROM users_user WHERE 1";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            $sql .= ' AND  active = ' . (int)$staus;
        }
        if ($ID) {
            $sql .= ' AND ID = ' . (int)$ID;
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' ORDER BY ID DESC';
        }
        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function supertravellerlistAccouts($filterData = array(), $ID = false)
    {
        $sql = "SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) as recevier_id, COUNT(ut.ID) AS trips, ur.rating, ur.create_date, u.email_address, ur.message FROM users_user as u JOIN users_reviews as ur ON u.id = ur.recevier_id JOIN trips AS ut ON ut.user=u.ID WHERE rating > 3 GROUP BY recevier_id";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            $sql .= ' AND  active = ' . (int)$staus;
        }
        if ($ID) {
            $sql .= ' AND ID = ' . (int)$ID;
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' ORDER BY ID DESC';
        }
        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function seekerenqlistAccouts($filterData = array(), $ID = false)
    {
        $sql = "SELECT si.*, CONCAT(uu.first_name, ' ', uu.last_name) as recevier_id, uu.email_address AS sender_id, uu.email_address AS senderemail FROM seeker_inquires as si JOIN users_user as uu ON si.recevier_id = uu.id";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            $sql .= ' AND  active = ' . (int)$staus;
        }
        if ($ID) {
            $sql .= ' AND ID = ' . (int)$ID;
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' ORDER BY ID DESC';
        }
        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function seekerlistAccouts($filterData = array(), $ID = false)
    {
        $sql = "SELECT si.*, CONCAT(uu.first_name, ' ', uu.last_name) as receiver_id, uu.email_address AS sender_id FROM seeker_inquires as si JOIN users_user as uu ON ti.recevier_id = uu.id";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            $sql .= ' AND  active = ' . (int)$staus;
        }
        if ($ID) {
            $sql .= ' AND ID = ' . (int)$ID;
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' ORDER BY ID DESC';
        }
        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function travellerlistAccouts($filterData = array(), $ID = false)
    {
        $sql = "SELECT ti.*, CONCAT(uu.first_name, ' ', uu.last_name) as sender_id, uu.email_address AS receiver_id, uu.email_address AS senderemail FROM travaller_inquires as ti JOIN users_user as uu ON ti.sender_id = uu.id";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            $sql .= ' AND  active = ' . (int)$staus;
        }
        if ($ID) {
            $sql .= ' AND ID = ' . (int)$ID;
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' ORDER BY ID DESC';
        }
        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function travellerlistAccoutsById($ID)
    {
        $sql = "SELECT ti.message AS rmessage,ti.create_date AS sent_date, ti.ID AS RTID, CONCAT(uu.first_name, ' ', uu.last_name) as receiver_id, uu.email_address AS remail,uu.ID  AS RID FROM travaller_inquires as ti JOIN users_user as uu ON ti.recevier_id = uu.id  WHERE ti.ID=" . $ID;

        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();//getResource()->fetchAll();
        } else return false;
    }

    public function seekerlistAccoutsById($ID)
    {
        $sql = "SELECT si.message AS rmessage,si.create_date AS sent_date, si.ID AS STID, CONCAT(uu.first_name, ' ', uu.last_name) as sender_id, uu.email_address AS remail,uu.ID  AS RID FROM seeker_inquires as si JOIN users_user as uu ON si.sender_id = uu.id  WHERE si.ID=" . $ID;

        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();//getResource()->fetchAll();
        } else return false;
    }

    public function reviewslistAccoutsById($RID)
    {
        $query = $this->sql->select()
            ->columns(array("rating", "create_date", "message", "RID" => "id"))
            ->from(array('users_reviews' => 'users_reviews'))
            ->join(array('users_user' => 'users_user'), 'users_user.id = users_reviews.recevier_id', array(
                "full_name" => new Expression("CONCAT(users_user.first_name, ' ', users_user.last_name)"), "email_address"
            ), 'left')
            ->join(array('trips' => 'trips'), 'trips.id = users_reviews.trip_id', array('trip_id' => 'trip_id_number'))
            ->where(array('users_reviews.id' => $RID));


        $statement = $this->sql->prepareStatementForSqlObject($query);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();//getResource()->fetchAll();
        } else return false;

    }

    public function reviewDetailsByRId($id)
    {
        $query = $this->sql->select()
            ->columns(array('rating_val'))
            ->from(array('users_review_ans' => 'users_review_ans'))
            ->join(array('users_review_questions' => 'users_review_questions'), 'users_review_questions.id = users_review_ans.id', array('question'))
            ->where(array('users_review_ans.review_id' => $id));
        $statement = $this->sql->prepareStatementForSqlObject($query);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function supertravellerlistAccoutsById($ID)
    {
        $sql = "SELECT u.id, CONCAT(u.first_name, ' ', u.last_name) as recevier_id, COUNT(ut.ID) AS trips, ur.rating, ur.create_date, u.email_address, ur.message FROM users_user as u JOIN users_reviews as ur ON u.id = ur.recevier_id JOIN trips AS ut ON ut.user=u.ID WHERE u.id=" . $ID;

        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();//getResource()->fetchAll();
        } else return false;
    }

    public function travellerlistAccoutsBySenderId($ID)
    {
        $sql = "SELECT ti.message, ti.ID AS STID, CONCAT(uu.first_name, ' ', uu.last_name) as sender_id,uu.email_address,uu.ID FROM travaller_inquires as ti JOIN users_user as uu ON ti.sender_id = uu.id  WHERE ti.ID=" . $ID;

        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();//getResource()->fetchAll();
        } else return false;
    }

    public function seekerlistAccoutsByReceiverId($ID)
    {
        $sql = "SELECT si.message, si.ID AS RTID, CONCAT(uu.first_name, ' ', uu.last_name) as recevier_id,uu.email_address,uu.ID FROM seeker_inquires as si JOIN users_user as uu ON si.recevier_id = uu.id  WHERE si.ID=" . $ID;

        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();//getResource()->fetchAll();
        } else return false;
    }

    public function userNotifications($filterData = array(), $ID = false)
    {
        $sql = "SELECT n.*,u.first_name,u.last_name,u.email_address,u.ID FROM notifications AS n JOIN users_user AS u ON u.ID=n.user_id WHERE 1";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            //$sql .=  ' AND  active = '.(int)$staus;
        }
        if ($ID) {
            $sql .=  ' AND n.user_id = '.(int)$ID;
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' ORDER BY n.notification_id DESC';
        }
        //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getLatestNotifications()
    {
        $sql       = "SELECT * FROM `administration_notifications`
                        WHERE 1 = 1
                        AND `read_unread` = '0'
                        ORDER BY `create_date` DESC";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else
            return false;
    }

    public function userNotificationsById($ID)
    {
        $sql = "SELECT n.*,u.first_name,u.last_name,u.email_address FROM notifications AS n JOIN users_user AS u ON u.ID=n.user_id WHERE notification_id = " . (int)$ID;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();//getResource()->fetchAll();
        } else return false;
    }

    public function adminNotificationsByUserId($id)
    {
        $sql = "SELECT n.*, u.first_name, u.last_name, u.email_address
                    FROM notifications AS n
                    JOIN users_user AS u ON u.ID = n.user_id
                    WHERE
                      (
                        n.notification_type = 'signup' OR
                        n.notification_type = 'become_traveller' OR
                        n.notification_type = 'awaiting_document' OR
                        n.notification_type = 'verify_document' OR
                        n.notification_type = 'verify_document_inprogress' OR
                        n.notification_type = 'verify_document_completed'
                      )
                    AND n.user_id = " . (int)$id ."
                    ORDER BY n.notification_id DESC ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function adminSentNotificationsByUserId($id)
    {
        $sql = "SELECT n.*, u.first_name, u.last_name, u.email_address
                    FROM notifications AS n
                    JOIN users_user AS u ON u.ID = n.user_id
                    WHERE
                      (
                        n.notification_type = 'signup' OR
                        n.notification_type = 'become_traveller' OR
                        n.notification_type = 'awaiting_document'
                      )
                    AND n.user_id = " . (int)$id ."
                    ORDER BY n.notification_id DESC ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function adminTravellerEnquiriesByUserId($id)
    {
        $sql = "SELECT n.*, u.first_name, u.last_name, u.email_address
                    FROM notifications AS n
                    JOIN users_user AS u ON u.ID = n.user_id
                    WHERE
                      (
                        n.notification_type = 'traveller_request'
                      )
                    AND n.user_id = " . (int)$id ."
                    ORDER BY n.notification_id DESC ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function adminSeekerEnquiriesByUserId($id)
    {
        $sql = "SELECT n.*, u.first_name, u.last_name, u.email_address
                    FROM notifications AS n
                    JOIN users_user AS u ON u.ID = n.user_id
                    WHERE
                      (
                        n.notification_type = 'seeker_request'
                      )
                    AND n.user_id = " . (int)$id ."
                    ORDER BY n.notification_id DESC ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function adminDisputesByUserId($id)
    {
        $sql = "SELECT n.*, u.first_name, u.last_name, u.email_address
                    FROM notifications AS n
                    JOIN users_user AS u ON u.ID = n.user_id
                    WHERE
                      (
                        n.notification_type = 'disputes'
                      )
                    AND n.user_id = " . (int)$id ."
                    ORDER BY n.notification_id DESC ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    function addEditNotifications($user, $id = '', $action = 'add')
    {

        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('notifications');
            $update->set($user);
            $update->where(array('notification_id' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $insert = $this->sql->insert('notifications');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        return $userid;
    }

    function addEditTravellerenq($user, $id = '', $action = 'add')
    {

        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('travaller_inquires');
            $update->set($user);
            $update->where(array('ID' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $insert = $this->sql->insert('travaller_inquires');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        return $userid;
    }

    function addEditSeekerenq($user, $id = '', $action = 'add')
    {

        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('seeker_inquires');
            $update->set($user);
            $update->where(array('ID' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $insert = $this->sql->insert('seeker_inquires');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        return $userid;
    }

    function addEditReviewlist($user, $id = '', $action = 'add')
    {

        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('users_reviews');
            $update->set($user);
            $update->where(array('ID' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $insert = $this->sql->insert('users_reviews');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        return $userid;
    }

    function doUserActions($action, $ids, $field = false)
    {
        if ($action == 'enable') {
            $upQry = 'UPDATE users_user SET active = 1 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User account(s) are enabled successfully!';
        } else if ($action == 'disable') {
            $upQry = 'UPDATE users_user SET active = 0 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User account(s) are disabled successfully!';
        } else if ($action == 'delete') {
            $upQry = 'DELETE FROM  users_user  WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User account(s) are deleted successfully!';
        } else if ($action == 'verify') {
            $identityRow = $this->userIdentityRow($ids);
            $verifyArr = unserialize($identityRow['verify_details']);

            $verifyArr[$field] = 1;
            $upQry = "UPDATE user_identity SET verify_details = '" . serialize($verifyArr) . "' WHERE ID = " . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are verified successfully!';
        }
        else if ($action == 'approve')
        {
            $upQry = 'UPDATE user_identity SET approved = 1 WHERE ID = ' . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            if ($res) {
                $sql = "SELECT user_id, identity_type FROM user_identity WHERE ID = " . $ids;
                $statement = $this->adapter->query($sql);
                $res = $statement->execute();
                if ($res->count() > 0) {
                    $ust = $res->current();//getResource()->fetchAll();
                    $sql1 = "SELECT trip_status,ID,ticket_option,user FROM trips WHERE user = " . $ust['user_id'];
                    $statement1 = $this->adapter->query($sql1);
                    $res1 = $statement1->execute();
                    if ($res1->count() > 0) {
                        $tp_list = $res1->getResource()->fetchAll();
                        foreach ($tp_list as $appr) {
                            $sql3 = "SELECT COUNT(user_id) AS total FROM user_identity WHERE approved = 1 AND user_id = " . $appr['user'];
                            $statement3 = $this->adapter->query($sql3);
                            $res3 = $statement3->execute();
                            if ($res3->count() > 0) {
                                $ttl = $res3->current();
                            }
                            if ($appr['ticket_option'] != "" && $ttl['total'] >= 1) {
                                $upQry1 = 'UPDATE trips SET trip_status = 3 WHERE ID = ' . $appr['ID'];
                            } elseif ($appr['ticket_option'] != "" && $ttl['total'] < 1) {
                                $upQry1 = 'UPDATE trips SET trip_status = 2 WHERE ID = ' . $appr['ID'];
                            } else {
                                $upQry1 = 'UPDATE trips SET trip_status = 4 WHERE ID = ' . $appr['ID'];
                            }
                            $statement2 = $this->adapter->query($upQry1);
                            $res2 = $statement2->execute();
                        }
                    }
                } else return false;
            }

            if($ust['identity_type'] == 1){
                $identity_name = 'Passport';
            }
            elseif($ust['identity_type'] == 2){
                $identity_name = 'Driving Licence';
            }
            elseif($ust['identity_type'] == 3){
                $identity_name = 'Government ID';
            }

            $hugedata1 = array
            (
                'user_id'               => $ust['user_id'],
                'notification_type'     => "verify_document_completed",
                'notification_subject'  => 'Document Verification',
                'notification_message'  => "Your " . $identity_name . " document has been verified successfully.",
                'app_redirect'          => '/myprofile/trustVerification',
                'notification_added'    => date("Y-m-d H:i:s")
            );

            $this->addToNotifications($hugedata1);

            $sucessMsg = 'Trust verification document are approved successfully!';
        }
        else if ($action == 'reject') {
            $rejected_reason = $field;
            $upQry = 'UPDATE user_identity SET rejected = 1 , rejected_reason = "' . $rejected_reason . '" WHERE ID = ' . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are rejected successfully!';
        }
        return $sucessMsg;
    }

    function userIdentityRow($ID)
    {
        $sql = "SELECT P.*,UI.*,U.first_name,U.email_address FROM user_identity as P
                JOIN  user_identity_type as UI  ON (UI.identity_type_id = P.identity_type AND UI.identity_type_deleted = 0)
                JOIN  users_user as U  ON (U.ID = P.user_id)
                WHERE  P.ID='" . $ID . "' ";

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;


    }

    function doTravellerlistActions($action, $ids, $field = false)
    {
        if ($action == 'enable') {
            $upQry = 'UPDATE travaller_inquires SET active = 1 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User account(s) are enabled successfully!';
        } else if ($action == 'disable') {
            $upQry = 'UPDATE travaller_inquires SET active = 0 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User account(s) are disabled successfully!';
        } else if ($action == 'delete') {
            $upQry = 'DELETE FROM  travaller_inquires  WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Traveller Enquiry List(s) are deleted successfully!';
        } else if ($action == 'verify') {
            $identityRow = $this->userIdentityRow($ids);
            $verifyArr = unserialize($identityRow['verify_details']);
            $verifyArr[$field] = 1;
            $upQry = "UPDATE user_identity SET verify_details = '" . serialize($verifyArr) . "' WHERE ID = " . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are verified successfully!';
        } else if ($action == 'approve') {
            $upQry = 'UPDATE user_identity SET approved = 1 WHERE ID = ' . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are approved successfully!';
        } else if ($action == 'reject') {
            $rejected_reason = $field;
            $upQry = 'UPDATE user_identity SET rejected = 1 , rejected_reason = "' . $rejected_reason . '" WHERE ID = ' . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are rejected successfully!';
        }
        return $sucessMsg;
    }

    function doSupertravellerlistActions($action, $ids, $field = false)
    {
        if ($action == 'enable') {
            $upQry = 'UPDATE users_reviews SET active = 1 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User account(s) are enabled successfully!';
        } else if ($action == 'disable') {
            $upQry = 'UPDATE users_reviews SET active = 0 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User account(s) are disabled successfully!';
        } else if ($action == 'delete') {
            $upQry = 'DELETE FROM  users_reviews  WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Seeker Enquiry List(s) are deleted successfully!';
        } else if ($action == 'verify') {
            $identityRow = $this->userIdentityRow($ids);
            $verifyArr = unserialize($identityRow['verify_details']);
            $verifyArr[$field] = 1;
            $upQry = "UPDATE users_reviews SET verify_details = '" . serialize($verifyArr) . "' WHERE ID = " . $ids;

            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are verified successfully!';
        } else if ($action == 'approve') {
            $upQry = 'UPDATE users_reviews SET approved = 1 WHERE ID = ' . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are approved successfully!';
        } else if ($action == 'reject') {
            $rejected_reason = $field;
            $upQry = 'UPDATE users_reviews SET rejected = 1 , rejected_reason = "' . $rejected_reason . '" WHERE ID = ' . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are rejected successfully!';
        }
        return $sucessMsg;
    }

    function doSeekerlistActions($action, $ids, $field = false)
    {
        if ($action == 'enable') {
            $upQry = 'UPDATE seeker_inquires SET active = 1 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User account(s) are enabled successfully!';
        } else if ($action == 'disable') {
            $upQry = 'UPDATE seeker_inquires SET active = 0 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User account(s) are disabled successfully!';
        } else if ($action == 'delete') {
            $upQry = 'DELETE FROM  seeker_inquires  WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Seeker Enquiry List(s) are deleted successfully!';
        } else if ($action == 'verify') {
            $identityRow = $this->userIdentityRow($ids);
            $verifyArr = unserialize($identityRow['verify_details']);
            $verifyArr[$field] = 1;
            $upQry = "UPDATE user_identity SET verify_details = '" . serialize($verifyArr) . "' WHERE ID = " . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are verified successfully!';
        } else if ($action == 'approve') {
            $upQry = 'UPDATE user_identity SET approved = 1 WHERE ID = ' . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are approved successfully!';
        } else if ($action == 'reject') {
            $rejected_reason = $field;
            $upQry = 'UPDATE user_identity SET rejected = 1 , rejected_reason = "' . $rejected_reason . '" WHERE ID = ' . $ids;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Trust verification document are rejected successfully!';
        }
        return $sucessMsg;
    }

    public function updateAdminRow($ID)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $updateData['email_address'] = $_POST['email'];
        $updateData['first_name'] = $_POST['name'];
        $updateData['username'] = $_POST['username'];
        if (isset($_POST['changeUserPassword']) && !empty($_POST['changeUserPassword'])) {
            $updateData['password'] = md5($_POST['password']);
        }

        $update->set($updateData);
        $update->where(array('ID' => $ID));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return true;


    }

    public function checkAdminEmailExists($email, $ID = false)
    {

        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        if ($ID != false) $sql .= ' AND ID != ' . $ID;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    function addEditUserAccounts($user, $id = '', $action = 'add')
    {

        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('users_user');
            $update->set($user);
            $update->where(array('ID' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $insert = $this->sql->insert('users_user');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        return $userid;
    }

    public function getUser($userid)
    {

        $sql = "SELECT * FROM users_user WHERE ID = '" . $userid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getCountryByName($name, $type)
    {
        if ($type == 'country' && is_numeric($name)) {
            $sql = "SELECT * FROM `core_country` WHERE ID='" . $name . "'";
        } elseif ($type == 'country' && !is_numeric($name)) {
            $sql = "SELECT * FROM `core_country` WHERE name='" . $name . "'";
        } else {
            $sql = "SELECT * FROM `core_country` WHERE currency_code='" . $name . "'";
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getUserByEmail($email)
    {
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getUserByEmailOrName($key)
    {
        $sql = "SELECT * FROM users_user";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function checkUserEmailExists($email)
    {


        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function addUsersUser($user)
    {

        $emailExists = false;
        $email_address = $user['email_address'];
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email_address . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $emailExists = true;
        }
        if ($emailExists === false) {
            $insert = $this->sql->insert('users_user');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);

            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
            return $userid;
        } else {
            return 'emailExist';
        }
    }

    public function changeUserPassword($ID, $newpwd)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set(array("reset_password" => "1"));
        $update->where(array('ID' => $ID));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();

    }

    public function updateUserPassword($ID, $newpwd)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set(array("password" => md5($newpwd), "reset_password" => "0"));
        $update->where(array('ID' => $ID));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();


    }

    public function getAirportsBySearch($q)
    {
        $sql = "SELECT * FROM core_airports WHERE code LIKE '" . $q . "%' OR city LIKE '" . $q . "%' OR country LIKE '" . $q . "%' OR name LIKE '" . $q . "%' ORDER BY code ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function LoadUsersAddressBook($user)
    {

        $sql = "SELECT * FROM users_user_locations WHERE user='" . $user . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function uploadFile($field_name, $file_name_prefix, $image_path)
    {


        if (isset($_FILES[$field_name])) {


            $dirpath = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']);

            $token = md5(uniqid(rand(), 1));


            $un = substr($token, 0, 5);

            $ext = explode('.', $_FILES[$field_name]['name']);
            $file_ext = strtolower(end($ext));


            $file_name = $file_name_prefix . "_" . $un . "." . $file_ext;


            @move_uploaded_file($_FILES[$field_name]['tmp_name'], $dirpath . $image_path . $file_name);


            return $file_name;
        }
    }

    public function getprofileaddr($ID)
    {
        $sql = "SELECT Loc.*, U.email_address FROM `users_user_locations` as Loc, users_user as U  WHERE Loc.ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getUserAllAddress($userId = 0, $ID = 0)
    {
        if ($ID) {
            $sql = "SELECT Loc.*,U.first_name, U.email_address FROM `users_user_locations` as Loc JOIN users_user as U ON Loc.user = U.ID  WHERE Loc.ID=" . $ID;
        } else if ($userId) {
            $sql = "SELECT Loc.*,U.first_name, U.email_address FROM `users_user_locations` as Loc  JOIN users_user as U ON Loc.user = U.ID WHERE Loc.user = " . $userId . " GROUP BY ID";
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }
    
     public function getAddress($id)
    {
      
      $sql = " select location  from users_user where id='".$id."'";

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }
    
    
    function EditAddress($user, $id)
    {
    
            $update = $this->sql->update();
            $update->table('users_user');
            $update->set($user);
            $update->where(array('ID' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        
       return $userid;
    }
    

    function addEditUserAddress($user, $id = '', $action = 'add')
    {
        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('users_user_locations');
            $update->set($user);
            $update->where(array('ID' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $insert = $this->sql->insert('users_user_locations');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        }


        return $userid;
    }

    public function userAddresses($filterData)
    {
        $sql = "SELECT Loc.*, U.first_name, U.email_address FROM users_user_locations as Loc JOIN users_user as U ON U.ID = Loc.user WHERE 1";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {
            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            $sql .= ' AND  active = ' . (int)$staus;
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];
        } else {
            $sql .= ' ORDER BY Loc.ID DESC';
        }
//echo $sql;die;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function userReviewslist($filterData = array(), $RID = false)
    {
        $sql = "SELECT u.*, CONCAT(u.first_name, ' ', u.last_name) as full_name, ur.rating, ur.create_date,ut.trip_id_number as trip_id, u.email_address as sender_id, ur.message, u.id as recevier_id,ur.ID as RID, ur.active, ur.create_date FROM users_user as u JOIN users_reviews as ur ON u.id = ur.recevier_id JOIN trips AS ut ON ut.id=ur.trip_id  WHERE rating > 3  GROUP BY recevier_id";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {
            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            $sql .= ' AND  active = ' . (int)$staus;
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];
        } else {
            $sql .= ' ORDER BY ID DESC';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    function doUserAddressActions($action, $ids)
    {
        if ($action == 'enable') {
            $upQry = 'UPDATE users_user_locations SET active = 1 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User address(s) are enabled successfully!';
        } else if ($action == 'disable') {
            $upQry = 'UPDATE users_user_locations SET active = 0 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User address(s) are disabled successfully!';
        } else if ($action == 'delete') {
            $upQry = 'DELETE FROM  users_user_locations  WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User address(s) are deleted successfully!';
        }
        return $sucessMsg;
    }

    public function userPhotos($filterData = array())
    {
        $sql = "SELECT P.*, U.first_name,U.last_name,U.email_address,U.ID FROM user_photos as P "
            . " JOIN  users_user as U  ON (U.ID = P.user_id) WHERE  1 ";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {
            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['userId']) && !empty($filterData['userId'])) $sql .= ' AND P.user_id = ' . $filterData['userId'];

        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];
        } else {
            $sql .= ' ORDER BY P.user_photo_id DESC';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function userPhotoRow($ID)
    {
        /* $sql = "SELECT P.*, U.first_name, U.email_address FROM user_photos as P "
            . " JOIN  users_user as U  ON (U.ID = P.user_id) WHERE  P.user_photo_id='" . $ID . "' ";*/
 $sql = "SELECT *  FROM user_photos where user_photo_id='" . $ID . "' ";

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    function addEditUserPhotos($user, $id = '', $action = 'add')
    {
        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('user_photos');
            $update->set($user);
            $update->where(array('user_photo_id' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $insert = $this->sql->insert('user_photos');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        if (isset($user['marked_as_profile_photo']) && $user['marked_as_profile_photo'] == 1) {
            $update = $this->sql->update();
            $update->table('users_user');
            $update->set(array('image' => $user['user_photo_name']));
            $update->where(array('ID' => $user['user_id']));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
        }
        return $userid;
    }


    function addEditTrustDocument($user,$id)
    {    
        
            $update = $this->sql->update();
            $update->table('user_identity');
            $update->set(array('image' => $user['image']));
            $update->where(array('ID' => $id));
            
            $statement = $this->sql->prepareStatementForSqlObject($update);
           
            
            $results = $statement->execute();

            $userid = $id;
      
        return $userid;
    }


    function UpdatePhotosStatus($action,$id)
    {
        if ($action == 'enable') {
            $update = $this->sql->update();
            $update->table('user_photos');
            $update->set(array('active'=>'1'));
            $update->where(array('user_photo_id' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $update = $this->sql->update();
            $update->table('user_photos');
            $update->set(array('active'=>'0'));
            $update->where(array('user_photo_id' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        }
        
        return $userid;
    }



    function doUserPhotosActions($action, $ids)
    {
        if ($action == 'delete') {
            $upQry = 'DELETE FROM  user_photos  WHERE 1';
            $upQry .= ' AND user_photo_id IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User photo(s) are deleted successfully!';
        }
        return $sucessMsg;
    }

    public function userVideos($filterData = array())
    {
        $sql = "SELECT P.*, U.first_name, U.email_address FROM user_videos as P "
            . " JOIN  users_user as U  ON (U.ID = P.user_id) WHERE  1 ";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {
            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['userId']) && !empty($filterData['userId'])) $sql .= ' AND P.user_id = ' . $filterData['userId'];

        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];
        } else {
            $sql .= ' ORDER BY P.user_video_id DESC';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function userVideoRow($ID)
    {
        $sql = "SELECT P.*, U.first_name, U.email_address FROM user_videos as P "
            . " JOIN  users_user as U  ON (U.ID = P.user_id) WHERE  P.user_video_id='" . $ID . "' ";

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    function addEditUserVideos($user, $id = '', $action = 'add')
    {
        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('user_videos');
            $update->set($user);
            $update->where(array('user_video_id' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $videoid = $id;
        } else {
            $insert = $this->sql->insert('user_videos');
            $insert->values($user);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $videoid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $videoid;
    }

    function doUserVideosActions($action, $ids)
    {
        if ($action == 'delete') {
            $upQry = 'DELETE FROM  user_videos  WHERE 1';
            $upQry .= ' AND user_video_id IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User video(s) are deleted successfully!';
        }
        return $sucessMsg;
    }

    function getUserReferencesByRow($filterData)
    {
        $sql = "    SELECT P.*,UI.*,U.first_name FROM user_reference_requests as  P
                    JOIN  users_user as U  ON (U.ID = P.requested_by_user)
                    JOIN  user_reference_response  as  UI   ON (UI.request_id = P.request_id AND UI.reference_deleted = 0) 
                    WHERE  P.request_deleted = 0 ";
        if (isset($filterData['reference_id']) && !empty($filterData['reference_id'])) $sql .= ' AND  UI.reference_id = ' . $filterData['reference_id'];

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    function getUserReferences($filterData)
    {
        $sql = "    SELECT P.*,UI.*,U.first_name FROM user_reference_requests as  P
                    JOIN  users_user as U  ON (U.ID = P.requested_by_user)
                    JOIN  user_reference_response  as  UI   ON (UI.request_id = P.request_id AND UI.reference_deleted = 0) 
                    WHERE  P.request_deleted = 0 ";
        if (isset($filterData['userId']) && !empty($filterData['userId'])) $sql .= ' AND  P.requested_by_user = ' . $filterData['userId'];
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {
            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];
        } else {
            $sql .= ' ORDER BY P.request_id DESC';
        } //echo $sql;exit;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    function updateReference($user, $id)
    {
        $update = $this->sql->update();
        $update->table('user_reference_response');
        $update->set($user);
        $update->where(array('reference_id' => $id));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return $results;
    }

    function doUserReferenceActions($action, $ids)
    {
        if ($action == 'delete') {
            $upQry = 'DELETE FROM  user_reference_response  WHERE 1';
            $upQry .= ' AND user_reference_id IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User reference(s) are deleted successfully!';
        }
        return $sucessMsg;
    }

    function doReviewlistActions($action, $ids)
    {
        if ($action == 'delete') {
            $upQry = 'DELETE FROM  users_reviews  WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            // echo $upQry; die;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User review(s) are deleted successfully!';
        }
        return $sucessMsg;
    }

    function doUserReferralsActions($action, $ids)
    {
        if ($action == 'delete') {
            $upQry = 'DELETE FROM  user_invites  WHERE 1';
            $upQry .= ' AND invite_id IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'User referral(s) are deleted successfully!';
        }
        return $sucessMsg;
    }

    function getUserAllReferences($filterData)
    {
        $sql = "    SELECT P.*,UI.* FROM user_reference_requests as  P
                    JOIN  users_user as U  ON (U.ID = P.requested_by_user)
                    LEFT JOIN  user_reference_response  as  UI   ON (UI.request_id = P.request_id AND UI.reference_deleted = 0) 
                    WHERE  P.request_deleted = 0 ";
        if ($filterData['userId']) $sql .= ' AND  P.requested_by_user = ' . $filterData['userId'];
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];
        } else {
            $sql .= ' ORDER BY P.request_id DESC';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getUserVerification($filterData)
    {
        $sql = "SELECT P.*,UI.identity_name,U.first_name, U.email_address, U.active
                    FROM user_identity as P
                    JOIN  users_user as U  ON (U.ID = P.user_id)
                    JOIN  user_identity_type as UI  ON (UI.identity_type_id = P.identity_type AND UI.identity_type_deleted = 0) WHERE  1 "; /*  (P.verfiyid != '' AND  P.verfiyid != 0)*/

        if ($filterData['userId']) $sql .= ' AND P.user_id = ' . $filterData['userId'];
        if (isset($filterData['approved'])) $sql .= ' AND P.approved = ' . $filterData['approved'];
        else if (isset($filterData['rejected'])) $sql .= ' AND P.rejected != 0 ';
        else $sql .= ' AND P.rejected = 0 AND P.approved = 0 ';
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];
        } else {
            $sql .= ' ORDER BY P.ID DESC';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function setUserDefaultAddress($userId, $ID)
    {
        $upQry = 'UPDATE users_user_locations SET set_default = 0 WHERE user=' . $userId;
        $statement = $this->adapter->query($upQry);
        $res = $statement->execute();

        $upQry = 'UPDATE users_user_locations SET set_default = 1 WHERE ID=' . $ID;
        $statement = $this->adapter->query($upQry);
        $res = $statement->execute();
    }

    public function getUserTrustVerification($filterData)
    {
         $sql = "SELECT U.*, U.image as userImage,Loc.*, UIT.identity_name
                FROM user_identity as Loc
                JOIN users_user as U ON U.ID = Loc.user_id
                JOIN user_identity_type as UIT ON UIT.identity_type_id = Loc.identity_type
                WHERE 1";

        if (isset($filterData['user_id']) && !empty($filterData['user_id']))
            $sql .= ' AND Loc.user_id = ' . $filterData['user_id'];
        if (isset($filterData['ID']) && !empty($filterData['ID']))
            $sql .= ' AND Loc.ID = ' . $filterData['ID'];
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {
            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            $sql .= ' AND  active = ' . (int)$staus;
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];
        } else {
            $sql .= ' ORDER BY Loc.ID DESC';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            if (isset($filterData['ID']) && !empty($filterData['ID'])) return $res->current();
            else return $res->getResource()->fetchAll();
        } else return false;
    }


    public function EditBelowTrustVerification($id,$user_id)
    {
          $sql = "SELECT Loc.*, UIT.identity_name
                FROM user_identity as Loc
               
                JOIN user_identity_type as UIT ON UIT.identity_type_id = Loc.identity_type
                WHERE Loc.ID!=".$id. " and Loc.user_id=".$user_id;

        if (isset($filterData['user_id']) && !empty($filterData['user_id']))
            $sql .= ' AND Loc.user_id = ' . $filterData['user_id'];
        if (isset($filterData['ID']) && !empty($filterData['ID']))
            $sql .= ' AND Loc.ID = ' . $filterData['ID'];
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {
            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            $sql .= ' AND  active = ' . (int)$staus;
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];
        } else {
            $sql .= ' ORDER BY Loc.ID DESC';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            if (isset($filterData['ID']) && !empty($filterData['ID'])) return $res->current();
            else return $res->getResource()->fetchAll();
        } else return false;
    }


    public function treperZoneFriends($user_id)
    {
        $myProfile = $this->getProfile($user_id);
        $sql = "SELECT `UF`.*,U.ID, U.image, U.first_name, U.last_name FROM `user_friends_list` as `UF` JOIN `users_user` as `U` ON `UF`.`friend_email` = `U`.`email_address` WHERE `UF`.`user_id`=" . $user_id . " AND `UF`.`friend_email` != '" . $myProfile['email_address'] . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    public function getprofile($ID)
    {
        $sql = "SELECT * FROM users_user WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $result = $res->current();
            //$photoRow = $this->getUserProfilePhoto($ID);
            //$result['image'] = $photoRow['user_photo_name'];
            return $result;
        } else return false;
    }

    public function getreviews($ID)
    {
        $sql = "SELECT * FROM `users_reviews` WHERE  recevier_id='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getUserInviteDetails($userId, $filterData = array())
    {
        $query = $this->sql->select()
            ->columns(array(new \Zend\Db\Sql\Expression("sum(trans_amount)")), false)
            ->from(array('user_wallet_transactions' => 'user_wallet_transactions'))
            ->where(
                array(
                    'user_id = users_user.id'
                )
            );
        $query = str_replace('"', '', $query->getSqlString());
        $subquery = new \Zend\Db\Sql\Expression("({$query})");

        $query = $this->sql->select()
            ->columns(array("invite_id", "user_id", "invite_added", "updated", 'trans_amount' => $subquery))
            ->from(array('user_invites' => 'user_invites'))
            ->join(array('users_user' => 'users_user'), 'users_user.id = user_invites.user_id', array(
                "user_name" => new Expression("CONCAT(users_user.first_name, ' ', users_user.last_name)"), "email_address"
            ), 'left')
            ->group("users_user.id");
        $statement = $this->sql->prepareStatementForSqlObject($query);

        //$this->printSql($query);

        $res =  $statement->execute();
        
        $r = $res->getResource()->fetchAll();



        /*echo '<pre>';
        print_r($r);
        echo '</pre>';
        exit;*/


        $sql =  "SELECT I.user_id,I.invite_id,COUNT(I.user_id) AS 'total_referral' ,`U`.email_address, concat(`U`.`first_name`, ' ', `U`.`last_name`) as username,
		
			
                        (
                            SELECT SUM(`T`.`trans_amount`)
                            FROM `user_wallet_transactions` AS `T`
                            WHERE `T`.`user_id` = `U`.`user_referrer`
                            AND `T`.`referral_user_id` = `U`.`ID`
                            AND `T`.`trans_deleted` = 0
                        )
                        AS `trans_amount`
						
						
						
                    FROM user_invites as I
                    INNER JOIN  `users_user` AS `U` ON (I.user_id = U.ID)  WHERE 1 = 1 group by I.user_id ";

        if(!empty($userId))
           $sql .= "  AND `I`.`user_id`=".$userId;

        if(isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy']))
        {
           $sql .=  ' AND  '.$filterData['searchBy'].' LIKE "%'.$filterData['searchKey'].'%"';
        }

        if(isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder']))
        {
           $sql .= ' ORDER BY '.$filterData['sortBy'] .' '.$filterData['sortOrder'];
        }
        else{
           $sql .= ' ORDER BY I.invite_id DESC';
        }

        //echo $sql; exit;

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }
	
	
	
	public function getInviteDetails($userId, $filterData = array())
    {
        $query = $this->sql->select()
            ->columns(array(new \Zend\Db\Sql\Expression("sum(trans_amount)")), false)
            ->from(array('user_wallet_transactions' => 'user_wallet_transactions'))
            ->where(
                array(
                    'user_id = users_user.id'
                )
            );
        $query = str_replace('"', '', $query->getSqlString());
        $subquery = new \Zend\Db\Sql\Expression("({$query})");

        $query = $this->sql->select()
            ->columns(array("invite_id", "user_id", "invite_added", "updated", 'trans_amount' => $subquery))
            ->from(array('user_invites' => 'user_invites'))
            ->join(array('users_user' => 'users_user'), 'users_user.id = user_invites.user_id', array(
                "user_name" => new Expression("CONCAT(users_user.first_name, ' ', users_user.last_name)"), "email_address"
            ), 'left')
            ->group("users_user.id");
        $statement = $this->sql->prepareStatementForSqlObject($query);

        //$this->printSql($query);

        $res =  $statement->execute();
        
        $r = $res->getResource()->fetchAll();



        /*echo '<pre>';
        print_r($r);
        echo '</pre>';
        exit;*/

 $sql =  "SELECT `U`.`ID`, I.user_id, I.invite_id, concat(`U`.`first_name`, ' ', `U`.`last_name`) as username, `I`.`invited_email`,
                        (
                            SELECT SUM(`T`.`trans_amount`)
                            FROM `user_wallet_transactions` AS `T`
                            WHERE `T`.`user_id` = `U`.`user_referrer`
                            AND `T`.`referral_user_id` = `U`.`ID`
                            AND `T`.`trans_deleted` = 0
                        )
                        AS `trans_amount`
                    FROM user_invites as I
                    LEFT JOIN  `users_user` AS `U` ON (I.signed_user_id = U.ID)  WHERE 1 = 1";


        if(!empty($userId))
           $sql .= "  AND `I`.`user_id`=".$userId;

        if(isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy']))
        {
           $sql .=  ' AND  '.$filterData['searchBy'].' LIKE "%'.$filterData['searchKey'].'%"';
        }

        if(isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder']))
        {
           $sql .= ' ORDER BY '.$filterData['sortBy'] .' '.$filterData['sortOrder'];
        }
        else{
           $sql .= ' ORDER BY I.invite_id DESC';
        }

        //echo $sql; exit;

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else {
            return false;
        }
    }

    /*public function updateStatuses($tablename,$col,$col_val,$cond_col,$cond_col_val){
        $sql = "UPDATE $tablename SET $col=$col_val WHERE $cond_col=$cond_col_val";
        echo $sql; die;
        $statement = $this->adapter->query($sql);
        $res =  $statement->execute();
    }*/

    public function printSql($query)
    {
        echo $this->sql->getSqlstringForSqlObject($query);
        exit;
    }

    public function addToNotifications($hugeData){
        $insert  = $this->sql->insert('notifications');
        $newData = $hugeData;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results      = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID           = $this->adapter->getDriver()->getLastGeneratedValue();

        return $ID;
    }

    /* add edit coupons */


    function addEditCoupons($coupon, $id = '',$action = 'add')
    {

        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('coupons');
            $update->set($coupon);
            $update->where(array('id' => $id));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $insert = $this->sql->insert('coupons');
            $newData = $coupon;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        return $userid;
    }

    /* List coupons */

    public function Couponlist()
    {
        $sql = "SELECT * from coupons";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    /* Check coupon exists*/


 public function checkCouponExists($coupon)
    {

        $sql = "SELECT * FROM coupons WHERE coupon_code='" . $coupon . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    /* get coupons */

     public function getCouponRow($ID)
    {
        $sql = "SELECT * FROM coupons WHERE id='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
    /* delete and status stage */
    
    function doCouponActions($action, $ids, $field = false)
    {  
        if ($action == '1') {
            $upQry = 'UPDATE coupons SET status = 1  WHERE 1';
            $upQry .= ' AND id IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Coupon(s) are enabled successfully!';
        } else if ($action == '2') {
            $upQry = 'UPDATE coupons SET status = 0  WHERE 1';
            $upQry .= ' AND id IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Coupon(s) are disabled successfully!';
        } else if ($action == 'delete') {
            
            $upQry = 'DELETE FROM  coupons  WHERE 1';
            $upQry .= ' AND id IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Coupon(s) are deleted successfully!';
        } 
        return $sucessMsg;
    }

    function identityApprove ($id){
   
         $upQry = 'UPDATE user_identity SET approved = 1 , rejected = 0 WHERE ID = '.$id;
   
    $statement = $this->adapter->query($upQry);
    $res = $statement->execute();
    
    }

    function identityDisApprove ($id,$reason){

       $upQry = "UPDATE user_identity SET rejected ='1',approved = 0 , rejected_reason = '".$reason."' WHERE ID =".$id;
        $statement = $this->adapter->query($upQry);
        $res = $statement->execute();
    
    }

    public function loginAsUser($user_key, $email)
    {
        $sql = "SELECT * FROM users_user WHERE email_address ='" . $email . "' AND user_key ='" . $user_key . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

}