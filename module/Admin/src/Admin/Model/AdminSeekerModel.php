<?php
namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\DriverInterface;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\ResultSet\ResultSet;
use Zend\ServiceManager\ServiceManager;
use Interop\Container\ContainerInterface;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;

class AdminSeekerModel extends AbstractTableGateway implements AdapterAwareInterface
{

    protected $table = 'core_country';
    protected $adapter;
    protected $sql;
    protected $generalvar;

    public function __construct(Adapter $adapter, $siteConfigs = false)
    {
        $this->adapter = $adapter;
        $this->sql = new Sql($this->adapter);
        $this->generalvar = $siteConfigs['siteConfigs'];
    }

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();

    }


    public function cleanQuery($string)
    {

        if (get_magic_quotes_gpc()) {
            $string = stripslashes($string);
        }

        if (phpversion() >= '4.3.0') {
        } else {
            $string = mysql_escape_string($string);
        }

        return $string;
    }

    public function trimLength($s, $maxlength)
    {
        $words = explode(' ', $s);
        $split = '';
        foreach ($words as $word) {
            if (strlen($split) + strlen($word) < $maxlength)
                $split .= $word . ' ';
            else
                break;
        }

        if (strlen($s) > $maxlength) {
            $split = trim($split) . " ...";
        }

        return $split;
    }

    public function splitImages($img)
    {
        $img = list($img_name, $img_type) = explode(".", $img);
        $xs = $img_name . "_xs." . strtolower($img_type);
        $th = $img_name . "_th." . strtolower($img_type);
        $logo = $img_name . "_logo." . strtolower($img_type);
        $reg = $img_name . "." . strtolower($img_type);
        $lrg = $img_name . "_lg." . strtolower($img_type);
        $as = $img_name . "_as." . strtolower($img_type);
        $og = $img_name . "_og." . strtolower($img_type);
        $arr_imgs = array("small" => $xs, "thumbnail" => $th, "standard" => $reg, "large" => $lrg, "actual" => $as, "logo" => $logo, "original" => $og);
        return $arr_imgs;
    }

    public function timeZoneAdjust($format, $dat)
    {
        $dat = strtotime($dat);
        if (date('T', $dat) == 'PST') {
            $dateadjusted = date($format, $dat + 3600);
        } else {
            $dateadjusted = date($format, $dat);
        }
        return $dateadjusted;
    }

    public function cleanTags($txt)
    {
        $specialstuff = array("<br />", "<br>", "<ul>", "<li>", "</li>", "</ul>", "<ol>", "</ol>");
        $txt = str_replace($specialstuff, " ", stripslashes($txt));
        return $txt;
    }

    public function in_array_r($needle, $haystack)
    {
        foreach ($haystack as $item) {
            if ($item === $needle || (is_array($item) && $this->in_array_r($needle, $item))) {
                return true;
            }
        }
        return false;
    }

    public function getCountries()
    {
        $sql = "SELECT * FROM core_country";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->getResource()->fetchAll();
    }

    public function getCountry($countryid)
    {
        $countrysql = "SELECT * FROM core_country WHERE ID='" . $countryid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;


        /*$result = $this->_db->fetchRow($countrysql);
        if ($result) {
            return $result;
        }*/
    }

    public function getCurrencies()
    {
        $sql = "SELECT * FROM core_currency_rate";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }


    public function getUserRow($ID)
    {
        $sql = "SELECT * FROM users_user WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
	
	public function getAllTravelerRequests(){
		$sql = "SELECT r.ID,r.trip,r.approved,'Package' as service,r.modified,i.trip as traveler_trip
					FROM traveller_package_request AS r 
				INNER JOIN traveller_packages AS i ON i.ID = r.package_request
		UNION 
				SELECT r.ID,r.trip,r.approved,'People' as service,r.modified,i.trip as traveler_trip
					FROM traveller_people_request AS r
				INNER JOIN traveller_people AS i ON i.ID = r.people_request
		UNION 
				SELECT r.ID,r.trip,r.approved,'Product' as service,r.modified,i.trip as traveler_trip
					FROM traveller_project_request AS r
				INNER JOIN traveller_projects AS i ON i.ID = r.project_request
		";
		
		if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' ORDER BY ID DESC';
        }
		
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return array();
	}

	public function getAllSeekerRequests($filterData){
		$sql = "SELECT r.ID,r.trip,r.approved,'Package' as service,r.modified,i.seeker_trip 
					FROM seeker_package_request AS r 
				INNER JOIN seeker_package AS i ON i.ID = r.package_request
		UNION 
				SELECT r.ID,r.trip,r.approved,'People' as service,r.modified,i.seeker_trip
					FROM seeker_people_request AS r
				INNER JOIN seeker_people AS i ON i.ID = r.people_request
		UNION 
				SELECT r.ID,r.trip,r.approved,'Product' as service,r.modified,i.seeker_trip
					FROM seeker_project_request AS r
				INNER JOIN seeker_project AS i ON i.ID = r.project_request
		";
		
		if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' ORDER BY ID DESC';
        }
		
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return array();
	}
	
    public function getTrips($filterData)
    {

        $status_filter = "";
        if ($filterData['status'] == 'upcoming')
            $status_filter = " AND b.active=1 AND b.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif ($filterData['status'] == 'cancelled')
            $status_filter = " AND b.trip_status='cancelled' ";
        elseif ($filterData['status'] == 'completed')
            $status_filter = " AND b.trip_status != 'cancelled' AND b.departure_date <= '" . date('Y-m-d H:i:s') . "' ";
        $joinQry = ' LEFT JOIN users_user_locations AS L ON (L.ID = b.user_location)'
            . '  LEFT JOIN trips_flight AS T ON (T.trip = b.ID) ';

        if ($filterData['usertype'] == 'seeker')
            $sql = "SELECT b.*,U.first_name,U.last_name,U.email_address FROM seeker_trips as b JOIN users_user AS U ON(U.ID = b.user) $joinQry WHERE U.deleted = 0  ";
        else
            $sql = "SELECT b.*,U.first_name,U.last_name,U.email_address FROM trips as b JOIN users_user AS U ON(U.ID = b.user) $joinQry WHERE U.deleted = 0  ";
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchStatus'] != 'all') {
            $staus = 0;
            if ($filterData['searchStatus'] == 'Approved') $staus = 1;
            if ($filterData['searchStatus'] == 'Rejected') $staus = 2;
            $sql .= ' AND  trip_status = ' . (int)$staus;
        }
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' GROUP BY ID ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' GROUP BY ID ORDER BY ID DESC';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getTravellerTrips($filterData)
    {

        $status_filter = "";
        if ($filterData['status'] == 'upcoming')
            $status_filter = " AND b.active=1 AND b.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif ($filterData['status'] == 'cancelled')
            $status_filter = " AND b.trip_status='cancelled' ";
        elseif ($filterData['status'] == 'completed')
            $status_filter = " AND b.trip_status != 'cancelled' AND b.departure_date <= '" . date('Y-m-d H:i:s') . "' ";
        $joinQry = ' LEFT JOIN users_user_locations AS L ON (L.ID = b.user_location)'
            . '  LEFT JOIN trips_flight AS T ON (T.trip = b.ID) ';

        if ($filterData['usertype'] == 'seeker')
            $sql = "SELECT b.*,U.first_name,U.email_address FROM seeker_trips as b JOIN users_user AS U ON(U.ID = b.user) $joinQry WHERE U.deleted = 0  ";
        else
            $sql = "SELECT b.*,U.first_name,U.email_address FROM trips as b JOIN users_user AS U ON(U.ID = b.user) $joinQry WHERE U.deleted = 0 AND b.is_completed = 1 ";
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchStatus'] != 'all') {
            $staus = 0;
            if ($filterData['searchStatus'] == 'Approved') $staus = 1;
            if ($filterData['searchStatus'] == 'Rejected') $staus = 2;
            $sql .= ' AND  trip_status = ' . (int)$staus;
        }
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' GROUP BY ID  ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' GROUP BY ID ORDER BY ID DESC ';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }
	
	public function getTravellerPendingPaymentTrips($filterData)
    {

        $status_filter = "";
        if ($filterData['status'] == 'upcoming')
            $status_filter = " AND b.active=1 AND b.departure_date >= '" . date('Y-m-d H:i:s') . "' ";
        elseif ($filterData['status'] == 'cancelled')
            $status_filter = " AND b.trip_status='7' ";
        elseif ($filterData['status'] == 'completed')
            $status_filter = " AND b.trip_status = '6' AND b.arrival_date <= '" . date('Y-m-d H:i:s') . "' ";
        $joinQry = ' LEFT JOIN users_user_locations AS L ON (L.ID = b.user_location)'
            . '  LEFT JOIN trips_flight AS T ON (T.trip = b.ID) ';

        if ($filterData['usertype'] == 'seeker')
            $sql = "SELECT b.*,U.first_name,U.email_address FROM seeker_trips as b JOIN users_user AS U ON(U.ID = b.user) $joinQry WHERE U.deleted = 0 ".$status_filter;
        else
            $sql = "SELECT b.*,U.first_name,U.email_address FROM trips as b JOIN users_user AS U ON(U.ID = b.user) $joinQry WHERE U.deleted = 0 ".$status_filter;
        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchStatus'] != 'all') {
            $staus = 0;
            if ($filterData['searchStatus'] == 'Approved') $staus = 1;
            if ($filterData['searchStatus'] == 'Rejected') $staus = 2;
            $sql .= ' AND  trip_status = ' . (int)$staus;
        }
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' GROUP BY ID  ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        } else {
            $sql .= ' GROUP BY ID ORDER BY ID DESC ';
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }
	
	public function getSeekerTripByTravellerTripId($ID,$status = ''){
		$where = '';
		if($status == 'completed'){
			$where .= ' AND trip_status = "6" ';
		}
		$sql = "SELECT * FROM seeker_trips WHERE traveller_trip_id ='" . $ID . "' ".$where;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return array();
	}
	
	public function travellerMarkPaymentPaid($ID){
		$update = $this->sql->update();
        $update->table('trips');
        $update->set(array('trip_status'=>8));
        $update->where(array('ID' => $ID));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
	}

    public function getTripRow($ID, $usertype)
    {

        $status_filter = "";
        if ($usertype == 'seeker')
            $sql = "SELECT b.*,U.first_name,U.last_name,U.email_address,U.gender FROM seeker_trips as b JOIN users_user AS U ON(U.ID = b.user) WHERE U.deleted = 0  ";
        else
            $sql = "SELECT b.*,U.first_name,U.last_name,U.email_address,U.gender FROM trips as b JOIN users_user AS U ON(U.ID = b.user) WHERE U.deleted = 0  ";
        if (isset($ID)) {
            $sql .= ' AND b.ID =' . (int)$ID;
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;

    }
    
    
     public function getAddressRow($ID)
    {

        $status_filter = "";
       /* if ($usertype == 'seeker')
            $sql = "SELECT b.*,U.first_name,U.last_name,U.email_address,U.gender FROM seeker_trips as b JOIN users_user AS U ON(U.ID = b.user) WHERE U.deleted = 0  ";
        else
            $sql = "SELECT b.*,U.first_name,U.last_name,U.email_address,U.gender FROM trips as b JOIN users_user AS U ON(U.ID = b.user) WHERE U.deleted = 0  ";
        if (isset($ID)) {
            $sql .= ' AND b.ID =' . (int)$ID;
        }*/
           $sql = "SELECT * from users_user_locations where tripId=".$ID;

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;

    }

    public function getAirPort($ID)
    {
        $sql = "SELECT * FROM core_airports WHERE ID ='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getUserLocationById($ID)
    {
        $sql = "SELECT * FROM users_user_locations WHERE ID ='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getSeekerPeoplePassangers($people_request)
    {
        $sql = "SELECT * FROM seeker_people_passengers WHERE people_request='" . $people_request . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerPeoplePassangerAttachments($passengerId)
    {
        $sql = "SELECT * FROM seeker_people_passengers_attachment WHERE seeker_people_passengers_id='" . $passengerId . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getFlightsDetails($trip_id)
    {
        $sql = "SELECT * FROM trips_flight WHERE trip='" . $trip_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerPeopleRequest($tripid)
    {
        $sql = "SELECT * FROM seeker_people_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }
	
	public function getSeekerRequestsByid($id,$type)
    {
		$type = strtolower($type);
		if($type == 'people'){
			$tblName = 'seeker_people_request';
		} else if($type == 'package'){
			$tblName = 'seeker_package_request';
		} else if($type == 'project'){
			$tblName = 'seeker_project_request';
		}
        $sql = "SELECT * FROM ".$tblName." WHERE ID='" . $id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return array();
    }
	
	public function getTravelerRequestsByid($id,$type)
    {
		$type = strtolower($type);
		if($type == 'people'){
			$tblName = 'traveller_people_request';
		} else if($type == 'package'){
			$tblName = 'traveller_package_request';
		} else if($type == 'project'){
			$tblName = 'traveller_project_request';
		}
        $sql = "SELECT * FROM ".$tblName." WHERE ID='" . $id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return array();
    }
	
	public function getSeekerRequests($id,$type)
    {
		$type = strtolower($type);
		if($type == 'people'){
			$tblName = 'seeker_people_request';
			$fieldName = 'people_request';
		} else if($type == 'package'){
			$tblName = 'seeker_package_request';
			$fieldName = 'package_request';
		} else if($type == 'project'){
			$tblName = 'seeker_project_request';
			$fieldName = 'project_request';
		}
        $sql = "SELECT * FROM ".$tblName." WHERE ".$fieldName."='" . $id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return array();
    }
	
	public function getTravelerRequests($id,$type)
    {
		$type = strtolower($type);
		if($type == 'people'){
			$tblName = 'traveller_people_request';
			$fieldName = 'people_request';
		} else if($type == 'package'){
			$tblName = 'traveller_package_request';
			$fieldName = 'package_request';
		} else if($type == 'project'){
			$tblName = 'traveller_project_request';
			$fieldName = 'project_request';
		}
        $sql = "SELECT * FROM ".$tblName." WHERE ".$fieldName."='" . $id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return array();
    }
	
	public function getSeekerRequestsByTraveler($tripid,$type)
    {
		if($type == 'people'){
			$tblName = 'traveller_people_request';
		} else if($type == 'package'){
			$tblName = 'traveller_package_request';
		} else if($type == 'project'){
			$tblName = 'traveller_project_request';
		}
        $sql = "SELECT * FROM ".$tblName." WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return array();
    }
	
	public function getTravelerRequestsBySeeker($tripid,$type)
    {
		if($type == 'people'){
			$tblName = 'seeker_people_request';
		} else if($type == 'package'){
			$tblName = 'seeker_package_request';
		} else if($type == 'project'){
			$tblName = 'seeker_project_request';
		}
        $sql = "SELECT * FROM ".$tblName." WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return array();
    }
	
	public function getTrvellerServiceByRequest($id,$type)
    {
		if($type == 'people'){
			$tblName = 'traveller_people';
		} else if($type == 'package'){
			$tblName = 'traveller_packages';
		} else if($type == 'project'){
			$tblName = 'traveller_projects';
		}
        $sql = "SELECT * FROM ".$tblName." WHERE ID='" . $id. "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return array();
    }
	
	public function getSeekerServiceByRequest($id,$type)
    {
		if($type == 'people'){
			$tblName = 'seeker_people';
		} else if($type == 'package'){
			$tblName = 'seeker_package';
		} else if($type == 'project'){
			$tblName = 'seeker_project';
		}
        $sql = "SELECT * FROM ".$tblName." WHERE ID='" . $id. "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return array();
    }

    public function getSeekerPackageRequest($tripid)
    {
        $sql = "SELECT * FROM seeker_package_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerProjectRequest($tripid)
    {
        $sql = "SELECT * FROM seeker_package_request WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerPeople($seeker_trip_id)
    {
        $sql = "SELECT * FROM seeker_people WHERE seeker_trip ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getSeekerPackage($seeker_trip_id)
    {
        $sql = "SELECT * FROM seeker_package WHERE seeker_trip ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getSeekerPackagePackages($package_id)
    {
        $sql = "SELECT * FROM seeker_package_packages AS SPP"
            . " LEFT JOIN package_category AS PC ON SPP.item_category = PC.ID"
            . " LEFT JOIN package_sub_category AS PSC ON SPP.item_sub_category = PSC.ID"
            . " WHERE package_request='" . $package_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }
	
	public function getSeekerProjectTasks($project_id)
    {
        $sql = "SELECT * FROM seeker_project_tasks AS SPP"
            . " LEFT JOIN package_category AS PC ON SPP.category = PC.ID"
            . " LEFT JOIN package_sub_category AS PSC ON SPP.additional_requirements_category = PSC.ID"
            . " WHERE project_request='" . $project_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerProject($seeker_trip_id)
    {
        $sql = "SELECT * FROM seeker_project WHERE seeker_trip ='" . $seeker_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getTravellerPeople($traveller_trip_id)
    {
        $sql = "SELECT * FROM traveller_people WHERE trip ='" . $traveller_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }


    public function getTravellerPackage($traveller_trip_id)
    {
        $sql = "SELECT * FROM traveller_packages WHERE trip ='" . $traveller_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getTravellerProject($traveller_trip_id)
    {
        $sql = "SELECT * FROM traveller_projects WHERE trip ='" . $traveller_trip_id . "' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    function doTripActions($action, $ids, $field = false)
    {
        $sql = "SELECT user FROM trips WHERE ID = " . $ids;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        $ust = $res->current();

        if ($action == 'approve') {
            $upQry = 'UPDATE trips SET trip_status = 1 WHERE ID = ' . $ids;
            $statement = $this->adapter->query($upQry);
            $statement->execute();

            $hugedata1 = array
            (
                'user_id' => $ust['user'],
                'notification_type' => "ticket_approved",
                'notification_subject' => 'Ticket Verification',
                'notification_message' => "Your ticket has been approved.",
                'notification_added' => date("Y-m-d H:i:s")
            );

            $this->addToNotifications($hugedata1);

            $sucessMsg = 'Ticket has been approved successfully!';
        } else if ($action == 'reject') {
            $rejected_reason = $field;
            $upQry = 'UPDATE trips SET trip_status = 2 , rejected_reason = "' . $rejected_reason . '" WHERE ID = ' . $ids;
            $statement = $this->adapter->query($upQry);
            $statement->execute();

            $hugedata1 = array
            (
                'user_id' => $ust['user'],
                'notification_type' => "ticket_rejected",
                'notification_subject' => 'Ticket Verification',
                'notification_message' => "Your ticket has been rejected.",
                'notification_added' => date("Y-m-d H:i:s")
            );

            $this->addToNotifications($hugedata1);

            $sucessMsg = 'Ticket has been rejected successfully!';

        }
        return $sucessMsg;
    }

    public function addToNotifications($hugeData)
    {
        $insert = $this->sql->insert('notifications');
        $newData = $hugeData;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();

        return $ID;
    }

    public function checkAdminEmailExists($email, $ID = false)
    {


        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        if ($ID != false) $sql .= ' AND ID != ' . $ID;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getUser($userid)
    {

        $sql = "SELECT * FROM users_user WHERE ID = '" . $userid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getCountryByName($name, $type)
    {
        if ($type == 'country' && is_numeric($name)) {
            $sql = "SELECT * FROM `core_country` WHERE ID='" . $name . "'";
        } elseif ($type == 'country' && !is_numeric($name)) {
            $sql = "SELECT * FROM `core_country` WHERE name='" . $name . "'";
        } else {
            $sql = "SELECT * FROM `core_country` WHERE currency_code='" . $name . "'";
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getUserByEmail($email)
    {
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function checkUserEmailExists($email)
    {


        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getTravellerPeopleList($filterData)
    {
        $sql = "SELECT *,T.ID,TP.Id as people_service_id FROM traveller_people AS TP JOIN trips AS T ON (T.ID = TP.trip) JOIN users_user AS U ON (U.ID = T.user)";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getTravellerPackagesList($filterData)
    {
        $sql = "SELECT *,T.ID,TP.Id as package_service_id FROM traveller_packages AS TP JOIN trips AS T ON (T.ID = TP.trip) JOIN users_user AS U ON (U.ID = T.user)";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getTravellerProjectsList($filterData)
    {
        $sql = "SELECT *,T.ID,TP.Id as project_service_id FROM traveller_projects AS TP JOIN trips AS T ON (T.ID = TP.trip) JOIN users_user AS U ON (U.ID = T.user)";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getTravellerWishList($filterData = array(), $user_id = false)
    {
        $sql = "SELECT u.first_name,u.last_name,u.email_address,t.trip_id_number FROM users_user AS u JOIN traveller_wishlist AS s ON s.user_id = u.ID JOIN seeker_trips AS t ON t.ID = s.trip_id";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerPeopleList($filterData)
    {
        $sql = "SELECT *,T.ID,TP.ID as people_service_id FROM seeker_people AS TP JOIN seeker_trips AS T ON (T.ID = TP.seeker_trip) JOIN users_user AS U ON (U.ID = T.user)";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerPackagesList($filterData)
    {
        $sql = "SELECT *,T.ID,TP.ID as package_service_id FROM seeker_package AS TP JOIN seeker_trips AS T ON (T.ID = TP.seeker_trip) JOIN users_user AS U ON (U.ID = T.user)";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerProjectList($filterData)
    {
        $sql = "SELECT *,T.ID,TP.ID as project_service_id FROM  seeker_project AS TP JOIN seeker_trips AS T ON (T.ID = TP.seeker_trip) JOIN users_user AS U ON (U.ID = T.user)";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerWishList($filterData = array(), $user_id = false)
    {
        $sql = "SELECT u.first_name,u.last_name,u.email_address,t.trip_id_number,s.ID FROM users_user AS u JOIN seeker_wishlist AS s ON s.user_id = u.ID JOIN trips AS t ON t.ID = s.trip_id";
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {

            $sql .= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }
        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql .= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];

        }
        //echo $sql;die;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getUserLocations($userid, $primary = 'no', $except_deleted_id = false)
    {
        $filter_primary = ($primary == 'yes') ? " AND a.set_default = '1' " : "";
        $sql = "SELECT a.*,b.name as country_name FROM users_user_locations as a,core_country as b WHERE b.code=a.country AND a.user = '" . $userid . "'" . $filter_primary;
        if ($except_deleted_id) {
            $sql .= ' AND (a.deleted=0 OR a.ID=' . $except_deleted_id . ')';
        } else {
            $sql .= ' AND a.deleted=0';
        }

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
            /*return $res->current();*/
        } else return false;
    }

    public function getSeekerTripPeople($tripid)
    {
        $sql = "SELECT * FROM seeker_people WHERE seeker_trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getSeekerTripPeoplePassengers($people_request_id)
    {
        $sql = "SELECT * FROM seeker_people_passengers WHERE people_request='" . $people_request_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerTripPeoplePassengersAttachment($passenger_id)
    {
        $sql = "SELECT * FROM seeker_people_passengers_attachment WHERE seeker_people_passengers_id='" . $passenger_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getPackageTasksCategory()
    {
        $sql = "SELECT * FROM package_category where deleted=0";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerTripPackage($tripid)
    {
        $sql = "SELECT * FROM seeker_package WHERE seeker_trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getSeekerTripPackagePackages($package_request_id)
    {
        $sql = "SELECT *  FROM seeker_package_packages

                 WHERE package_request='" . $package_request_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getProjectTasksCategorylisttype($id)
    {
        $subquery = "SELECT ID, subcatname from project_sub_category where catid = '" . $id . "' and deleted=0";
        $statement = $this->adapter->query($subquery);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function addTripContactAddress($arr_trip_contact)
    {
        $insert = $this->sql->insert('users_user_locations');
        $newData = $arr_trip_contact;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function updateSeekerTrip($arr_trip_detail, $ID)
    {
        $update = $this->sql->update();
        $update->table('seeker_trips');
        $update->set($arr_trip_detail);
        $update->where(array('ID' => $ID));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
    }

    public function updateTrip($arr_trip_detail, $ID)
    {
        $update = $this->sql->update();
        $update->table('trips');
        $update->set($arr_trip_detail);
        $update->where(array('ID' => $ID));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
    }
    
     public function updateTripAddress($update_arr_trips, $ID)
    {
        $update = $this->sql->update();
        $update->table('users_user_locations');
        $update->set($update_arr_trips);
        $update->where(array('tripId' => $ID));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
    }

    public function addTrip($trip, $userrole)
    {
        if ($userrole == 'seeker') {
            $insert = $this->sql->insert('seeker_trips');
            $newData = $trip;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        } else {
            $insert = $this->sql->insert('trips');
            $newData = $trip;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        }
        return $ID;
    }

    public function addTripFlight($trip_flight)
    {
        $insert = $this->sql->insert('trips_flight');
        $newData = $trip_flight;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function deleteTripFlightDetails($ID)
    {
        $sql = "DELETE FROM trips_flight WHERE trip='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
    }

    public function deleteSeekerPeopleById($ID)
    {
        $seekerPeopleIds = false;
        /* find seeker projects from seeker trips and delete them */
        $sql = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_people WHERE seeker_trip = " . $ID;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            $seekerPeopleIds = $res->current();
        }

        if (isset($seekerPeopleIds) && $seekerPeopleIds['Ids'] != '') {
            $sql = "DELETE FROM seeker_people WHERE ID IN (" . $seekerPeopleIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res = $statement->execute();
            /* find seeker project tasks from seeker project and delete them */
            $sql = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_people_passengers WHERE people_request IN (" . $seekerPeopleIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res = $statement->execute();
            if ($res->count() > 0) {
                $seekerPeoplePassengerIds = $res->current();
            }
            if (isset($seekerPeoplePassengerIds) && $seekerPeoplePassengerIds['Ids'] != '') {
                $sql = "DELETE FROM seeker_people_passengers WHERE ID IN (" . $seekerPeoplePassengerIds['Ids'] . ")";
                $statement = $this->adapter->query($sql);
                $res = $statement->execute();
            }

            /* find seeker project tasks photos from seeker project tasks and delete them */
            $sql = "SELECT GROUP_CONCAT( seeker_attachment_id SEPARATOR  ',' ) AS Ids FROM seeker_people_passengers_attachment WHERE seeker_people_passengers_id IN (" . $seekerPeoplePassengerIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res = $statement->execute();
            if ($res->count() > 0) {
                $seekerPeoplePassAttachmentIds = $res->current();
            }
            if (isset($seekerPeoplePassAttachmentIds) && $seekerPeoplePassAttachmentIds['Ids'] != '') {
                $sql = "DELETE FROM seeker_people_passengers_attachment WHERE seeker_attachment_id IN (" . $seekerPeoplePassAttachmentIds['Ids'] . ")";
                $statement = $this->adapter->query($sql);
                $res = $statement->execute();
            }
        }
    }

    public function deleteSeekerProjectById($ID)
    {
        $seekerProjectIds = false;
        /* find seeker projects from seeker trips and delete them */
        $sql = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_project WHERE seeker_trip = " . $ID;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            $seekerProjectIds = $res->current();
        }

        if (isset($seekerProjectIds) && $seekerProjectIds['Ids'] != '') {
            $sql = "DELETE FROM seeker_project WHERE ID IN (" . $seekerProjectIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res = $statement->execute();
            /* find seeker project tasks from seeker project and delete them */
            $sql = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_project_tasks WHERE project_request IN (" . $seekerProjectIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res = $statement->execute();
            if ($res->count() > 0) {
                $seekerProjectTasksIds = $res->current();
            }
            if (isset($seekerProjectTasksIds) && $seekerProjectTasksIds['Ids'] != '') {
                $sql = "DELETE FROM seeker_project_tasks WHERE ID IN (" . $seekerProjectTasksIds['Ids'] . ")";
                $statement = $this->adapter->query($sql);
                $res = $statement->execute();
            }

            /* find seeker project tasks photos from seeker project tasks and delete them */
            $sql = "SELECT GROUP_CONCAT( task_photo_id SEPARATOR  ',' ) AS Ids FROM seeker_project_task_photos WHERE seeker_project_tasks_id IN (" . $seekerProjectTasksIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res = $statement->execute();
            if ($res->count() > 0) {
                $seekerProjectTasksPhotoIds = $res->current();
            }
            if (isset($seekerProjectTasksPhotoIds) && $seekerProjectTasksPhotoIds['Ids'] != '') {
                $sql = "DELETE FROM seeker_project_task_photos WHERE task_photo_id IN (" . $seekerProjectTasksPhotoIds['Ids'] . ")";
                $statement = $this->adapter->query($sql);
                $res = $statement->execute();
            }
        }
    }

    public function deleteSeekerPackagesById($ID)
    {
        $seekerPackageIds = false;
        /* find seeker projects from seeker trips and delete them */
        $sql = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_package WHERE seeker_trip = " . $ID;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            $seekerPackageIds = $res->current();
        }

        if (isset($seekerPackageIds) && $seekerPackageIds['Ids'] != '') {
            $sql = "DELETE FROM seeker_package WHERE ID IN (" . $seekerPackageIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res = $statement->execute();
            /* find seeker project tasks from seeker project and delete them */
            $sql = "SELECT GROUP_CONCAT( ID SEPARATOR  ',' ) AS Ids FROM seeker_package_packages WHERE package_request IN (" . $seekerPackageIds['Ids'] . ")";
            $statement = $this->adapter->query($sql);
            $res = $statement->execute();
            if ($res->count() > 0) {
                $seekerPackagePackagesIds = $res->current();
            }
            if (isset($seekerPackagePackagesIds) && $seekerPackagePackagesIds['Ids'] != '') {
                $sql = "DELETE FROM seeker_package_packages WHERE ID IN (" . $seekerPackagePackagesIds['Ids'] . ")";
                $statement = $this->adapter->query($sql);
                $res = $statement->execute();
            }
        }
    }

    public function addSeekerPeople($arr_seeker_people_request)
    {
        $insert = $this->sql->insert('seeker_people');
        $newData = $arr_seeker_people_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function addSeekerPackage($arr_seeker_package_request)
    {
        $insert = $this->sql->insert('seeker_package');
        $newData = $arr_seeker_package_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function addSeekerProject($arr_seeker_project_request)
    {
        $insert = $this->sql->insert('seeker_project');
        $newData = $arr_seeker_project_request;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function addSeekerPeoplePassengers($arr_seeker_people_passengers)
    {
        $insert = $this->sql->insert('seeker_people_passengers');
        $newData = $arr_seeker_people_passengers;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function addSeekerPackagePackages($arr_seeker_package_packages)
    {
        $insert = $this->sql->insert('seeker_package_packages');
        $newData = $arr_seeker_package_packages;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function addSeekerProjectTasks($arr_seeker_project_tasks)
    {
        $insert = $this->sql->insert('seeker_project_tasks');
        $newData = $arr_seeker_project_tasks;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    function addPassengerAttachment($arr_pass_attachment)
    {
        $insert = $this->sql->insert('seeker_people_passengers_attachment');
        $newData = $arr_pass_attachment;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    function addProjectAttachment($arr_pro_attachment)
    {
        $insert = $this->sql->insert('seeker_project_task_photos');
        $newData = $arr_pro_attachment;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function getSeekerTripProject($tripid)
    {
        $sql = "SELECT * FROM seeker_project WHERE seeker_trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getSeekerTripProjectTasks($project_request_id)
    {
        $sql = "SELECT *, P.ID as catId,PC.ID as subCatId, SPP.ID as project_task_id FROM seeker_project_tasks AS SPP
                    LEFT JOIN project_category AS P ON (P.ID = SPP.category)
                    LEFT JOIN project_sub_category AS PC ON (PC.ID = SPP.additional_requirements_category)
                WHERE  project_request='" . $project_request_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getSeekerTripProjectTaskAttachment($project_task_id)
    {
        $sql = "SELECT * FROM seeker_project_task_photos WHERE seeker_project_tasks_id='" . $project_task_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getProjectTasksCategorytype($id = false)
    {
        $sql = "SELECT * FROM project_category WHERE status='1' and deleted=0";
        if ($id !== false && $id != '') {
            $sql .= ' AND work_category=' . $id;
        }
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getTripPeople($tripid)
    {
        $sql = "SELECT * FROM traveller_people WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getTripProject($tripid)
    {
        $sql = "SELECT *,PC.catid AS catId,PC.ID AS subCatId, PCT.catid AS product_catId,PCT.ID AS product_subCatId FROM traveller_projects AS P LEFT JOIN project_sub_category AS PC ON (PC.ID = P.task) LEFT JOIN project_sub_category AS PCT ON (PCT.ID = P.product_type) WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getTripPackage($tripid)
    {
        $sql = "SELECT * FROM traveller_packages WHERE trip='" . $tripid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function addTripPeopleDetail($people_detail)
    {
        $insert = $this->sql->insert('traveller_people');
        $newData = $people_detail;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function addTripTravellerPackage($package_detail)
    {
        $insert = $this->sql->insert('traveller_packages');
        $newData = $package_detail;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function addTripTravellerProject($project_detail)
    {
        $insert = $this->sql->insert('traveller_projects');
        $newData = $project_detail;
        $insert->values($newData);
        $selectString = $this->sql->getSqlStringForSqlObject($insert);
        $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
        $ID = $this->adapter->getDriver()->getLastGeneratedValue();
        return $ID;
    }

    public function deleteTripPeopleDetail($ID)
    {
        $sql = "DELETE FROM traveller_people WHERE trip='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
    }

    public function deleteTripPackageDetail($ID)
    {
        $sql = "DELETE FROM traveller_packages WHERE trip='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
    }

    public function deleteTripProjectDetails($ID)
    {
        $sql = "DELETE FROM traveller_projects WHERE trip ='" . $ID . "' ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
    }

    public function getSeekerWishListByID($user_id)
    {
        $sql = "select CONCAT(u.first_name,' ',u.last_name) as traveller_name, u.email_address as traveller_email, CONCAT(u1.first_name,' ',u1.last_name) as seeker_name, u1.email_address as seeker_email,u1.id as seeker_id from traveller_wishlist as ts left join users_user as u on ts.user_id=u.id left join seeker_wishlist as sw on ts.trip_id=sw.trip_id left join users_user as u1 on sw.user_id=u1.id   WHERE u1.id=" . $user_id;
        $statement = $this->adapter->query($sql);

        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();//getResource()->fetchAll();
        } else return false;
    }

    public function getTravellerWishListByID($user_id)
    {
        $sql = "select CONCAT(u.first_name,' ',u.last_name) as traveller_name, u.email_address as traveller_email, CONCAT(u1.first_name,' ',u1.last_name) as seeker_name, u1.email_address as seeker_email, u.id as user_id from traveller_wishlist as ts left join users_user as u on ts.user_id=u.id left join seeker_wishlist as sw on ts.trip_id=sw.trip_id left join users_user as u1 on sw.user_id=u1.id WHERE u.id=" . $user_id;
        $statement = $this->adapter->query($sql);

        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();//getResource()->fetchAll();
        } else return false;
    }
    
   public function setApprovalIdentities($ID, $status)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set(array(
            "approved_id" => $status
        ));
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
    }
    
    public function getBtrip($user_id)
    {
        $sql = "SELECT * FROM trips where user ='".$user_id."' and  trip_status='1' and trip_id_number LIKE 'B%'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->getResource()->fetchAll();
    }
    
    public function getdisputes()
    {
        $sql = "SELECT * FROM `user_dispute`";
       //  $sql = "SELECT t.*,u.first_name,u.last_name FROM `user_dispute` AS t  JOIN users_user AS u ON u.ID=t.user_id WHERE t.user_id='" . $ID . "' ";
      
       $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        return $res->getResource()->fetchAll();
    }
    public function getdisputesrow($dipute_id)
    {
         $sql = "SELECT * FROM `user_dispute` where id=".$dipute_id;
       //  $sql = "SELECT t.*,u.first_name,u.last_name FROM `user_dispute` AS t  JOIN users_user AS u ON u.ID=t.user_id WHERE t.user_id='" . $ID . "' ";
      
       $statement = $this->adapter->query($sql);
        $res = $statement->execute();

        if ($res->count() > 0) {
            return $res->current();
        } else return false;
      
    }


    function UpdateDisputeStatus($action,$id){
        
        if ($action == 'enable') {
            $upQry = 'UPDATE user_dispute SET status = 1 WHERE id='.$id;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Dispute update successfully!';
        } else if ($action == 'disable') {
            $upQry = 'UPDATE user_dispute SET status = 0 WHERE id='.$id;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Dispute update successfully!';
        } else if ($action == 'delete') {
            $upQry = 'DELETE FROM  user_dispute  WHERE id='.$id;
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Dispute deleted successfully!';
        } 
        return $sucessMsg;
    }

    function UpdateDispute($comment,$id){
        
       
            $upQry = "UPDATE user_dispute SET comment = '$comment'  WHERE id=".$id;   
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Dispute update successfully!';
         
        return $sucessMsg;
    }


    
    
   function doUserActions($action, $ids, $field = false)
    {
        if ($action == 'enable') {
            $upQry = 'UPDATE user_dispute SET status = 1 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Dispute update successfully!';
        } else if ($action == 'disable') {
            $upQry = 'UPDATE user_dispute SET status = 0 WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Dispute update successfully!';
        } else if ($action == 'delete') {
            $upQry = 'DELETE FROM  user_dispute  WHERE 1';
            $upQry .= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Dispute deleted successfully!';
        } 
        return $sucessMsg;
    }
}