<?php
namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\DriverInterface;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\Sql\Expression;
use Zend\Db\ResultSet\ResultSet;
use Zend\ServiceManager\ServiceManager;
use InteropContainer\ContainerInterface;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;
use Zend\SessionContainer;
use Zend\Mvc\Plugin\FlashMessenger;
require_once (ACTUAL_ROOTPATH . "Upload/src/Upload.php");

class AdminModel extends AbstractTableGateway implements AdapterAwareInterface

{
    protected $table = 'core_country';
    protected $adapter;
    protected $sql;
    protected $generalvar;
    protected $sessionObj;
    
    public function __construct(Adapter $adapter, $siteConfigs = false)
    {
        $this->adapter = $adapter;
        $this->sql = new Sql($this->adapter);
        $this->generalvar = $siteConfigs['siteConfigs'];
        $this->sessionObj = isset($_SESSION['comSessObj']) ? $_SESSION['comSessObj'] : false;
    }

    public function setDbAdapter(Adapter $adapter)
    {
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
    }

    public function cleanQuery($string)
    {
        if (get_magic_quotes_gpc()) {
            $string = stripslashes($string);
        }

        if (phpversion() >= '4.3.0') {
        } else {
            $string = mysql_escape_string($string);
        }

        return $string;
    }

    public function trimLength($s, $maxlength)
    {
        $words = explode(' ', $s);
        $split = '';
        foreach($words as $word) {
            if (strlen($split) + strlen($word) < $maxlength) $split.= $word . ' ';
            else break;
        }

        if (strlen($s) > $maxlength) {
            $split = trim($split) . " ...";
        }

        return $split;
    }

    public function splitImages($img)
    {
        $img = list($img_name, $img_type) = explode(".", $img);
        $xs = $img_name . "_xs." . strtolower($img_type);
        $th = $img_name . "_th." . strtolower($img_type);
        $logo = $img_name . "_logo." . strtolower($img_type);
        $reg = $img_name . "." . strtolower($img_type);
        $lrg = $img_name . "_lg." . strtolower($img_type);
        $as = $img_name . "_as." . strtolower($img_type);
        $og = $img_name . "_og." . strtolower($img_type);
        $arr_imgs = array(
            "small" => $xs,
            "thumbnail" => $th,
            "standard" => $reg,
            "large" => $lrg,
            "actual" => $as,
            "logo" => $logo,
            "original" => $og
        );
        return $arr_imgs;
    }

    public function timeZoneAdjust($format, $dat)
    {
        $dat = strtotime($dat);
        if (date('T', $dat) == 'PST') {
            $dateadjusted = date($format, $dat + 3600);
        } else {
            $dateadjusted = date($format, $dat);
        }

        return $dateadjusted;
    }

    public function cleanTags($txt)
    {
        $specialstuff = array(
            "<br />",
            "<br />",
            "<ul>",
            "<li>",
            "</li>",
            "</ul>",
            "<ol>",
            "</ol>"
        );
        $txt = str_replace($specialstuff, " ", stripslashes($txt));
        return $txt;
    }

    public function in_array_r($needle, $haystack)
    {
        foreach($haystack as $item) {
            if ($item === $needle || (is_array($item) && $this->in_array_r($needle, $item))) {
                return true;
            }
        }

        return false;
    }

    public function logAdminActivity($activity = '')
    {
        if (!empty($activity) && ADMIN_LOG == 1) {
            $timestamp = time();
            $logTime = date('H:i:s', $timestamp);
            $ipAddress = $this->input->ip_address();
            $userAgent = $this->input->user_agent();
            $reqProtocol = $this->input->server('SERVER_PROTOCOL');
            $reqMethod = $this->input->server('REQUEST_METHOD');
            $logUrl = $this->uri->uri_string();
            $logContent = "$timestamp\t$activity\t$reqProtocol\t$reqMethod\t$logUrl\t$ipAddress\t$userAgent\r\n";
            $logDir = '../logs/' . $this->login_email . '/';
            if (!is_dir('../logs')) {
                mkdir('../logs');
            }

            if (!is_dir($logDir)) {
                mkdir($logDir);
            }

            $logFileName = $logDir . date('Y-m-d', $timestamp) . '.log';
            $logFileHandler = @fopen($logFileName, 'a+');
            fwrite($logFileHandler, $logContent);
            @fclose($logFileHandler);
        }
    }

    public function authenticateAdmin($module)
    {
        if ($this->sessionObj->ADMIN_ID == '') {
            return array(
                'msg' => array(
                    'alert-info' => "You are not authorized to access! Please login."
                ) ,
                'redirectUrl' => 'admin'
            );
        } else if ($this->sessionObj->ADMIN_ID && $this->sessionObj->ADMIN_ACCESS != 1) {
            $arrMyAccessModules = explode('|', $this->sessionObj->ADMIN_MODULES);
            if (!in_array($module, $arrMyAccessModules)) {
                return array(
                    'msg' => array(
                        'alert-danger' => "You don't have enough privilege to process the request"
                    ) ,
                    'redirectUrl' => 'admin_dashboard'
                );
            }
        } else { 
            return false;
        }
    }

    public function checkAdminPrivilage($privilage)
    {
        $return = false;
        if ($this->sessionObj->ADMIN_ID && $this->sessionObj->ADMIN_ACCESS != 1) {
            if (!preg_match("/$privilage\b/i", $this->sessionObj->ADMIN_PRIVILEGES)) {
                $return = true;
            }
        }

        return $return;
    }

    public function getCountries($filters = false, $resType = 'list')
    {
        $sql = "SELECT * FROM core_country WHERE deleted=0"; /* filter query for search */
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $sql.= " AND " . $filters['searchBy'] . " LIKE '" . $this->cleanQuery($filters['searchKey'] . "%'");
            }

            if (isset($filters['paymentAllowed']) && $filters['paymentAllowed'] != '') {
                $sql.= " AND payment_allow = '" . $filters['paymentAllowed'] . "'";
            }

            if (isset($filters['activeStatus']) && $filters['activeStatus'] != '') {
                $sql.= " AND active = " . $filters['activeStatus'];
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $sql.= " ORDER BY " . $filters['sortBy'] . " " . $filters['sortOrder'];
            }

            if (isset($filters['limit']) && $filters['limit'] != '') {
                $sql.= " LIMIT " . $filters['limit'];
                if (isset($filters['offset']) && $filters['offset'] != '') {
                    $sql.= " OFFSET " . $filters['offset'];
                }
            }
        }

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0)
        if ($resType == 'list') {
            return $res->getResource()->fetchAll();
        } else
        if ($resType == "count") {
            return $res->count();
        } else return false;
    }

    public function getCountry($countryid)
    {
        $sql = "SELECT * FROM core_country WHERE ID='" . $countryid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false; /*$result = $this->_db->fetchRow($countrysql);        if ($result) {            return $result;        }*/
    }

    public function addUpdateCountryRow($updateData, $id = false)
    {
        if ($id === false) { /* Add */
            $insert = $this->sql->insert('core_country');
            $insert->values($updateData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        } else { /* Update */
            $update = $this->sql->update();
            $update->table('core_country');
            $update->set($updateData);
            $update->where(array(
                'ID' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            return true;
        }
    }

    public function getLanguagesList($filters = false, $resType = 'list')
    {
        $sql = "SELECT * FROM language WHERE deleted=0"; /* filter query for search */
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $sql.= " AND " . $filters['searchBy'] . " LIKE '" . $this->cleanQuery($filters['searchKey'] . "%'");
            }

            if (isset($filters['activeStatus']) && $filters['activeStatus'] != '') {
                $sql.= " AND active = " . $filters['activeStatus'];
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $sql.= " ORDER BY " . $filters['sortBy'] . " " . $filters['sortOrder'];
            }

            if (isset($filters['limit']) && $filters['limit'] != '') {
                $sql.= " LIMIT " . $filters['limit'];
                if (isset($filters['offset']) && $filters['offset'] != '') {
                    $sql.= " OFFSET " . $filters['offset'];
                }
            }
        }

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0)
        if ($resType == 'list') {
            return $res->getResource()->fetchAll();
        } else
        if ($resType == "count") {
            return $res->count();
        } else return false;
    }

    public function getLanguage($languageid)
    {
        $sql = "SELECT * FROM language WHERE ID='" . $languageid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function addUpdateLanguageRow($updateData, $id = false)
    {
        if ($id === false) { /* Add */
            $insert = $this->sql->insert('language');
            $insert->values($updateData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        } else { /* Update */
            $update = $this->sql->update();
            $update->table('language');
            $update->set($updateData);
            $update->where(array(
                'ID' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            return true;
        }
    }

    public function getCurrencies($filters = false)
    {
        $sql = "SELECT * FROM core_currency_rate";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getAllCurrencies($filters = false)
    {
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('core_country')->where(array(
            'deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }
        }

        $selectObj->group('currency_code');
        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        //echo $selectString;exit;
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $resultSet = new ResultSet;
            $resultSet->initialize($res);
            foreach($resultSet as $row) {
                $currencyArr[] = $row->currency_code;
            }

            return $currencyArr;
        } else return false;
    }

    public function getProjectTasksCategorytype($id = false, $filters = false)
    {
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('project_category')->where(array(
            'status' => '1'
        ))->where(array(
            'deleted' => '0'
        ));
        if ($id !== false && $id != '') {
            $selectObj->where(array(
                'work_category' => $id
            ));
        }

        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'ID' => 'ASC'
                ));
            }
        }

        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function addUpdateProjectCategoryRow($updateData, $id = false)
    {
        if ($id === false) { /* Add */
            $insert = $this->sql->insert('project_category');
            $insert->values($updateData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        } else { /* Update */
            $update = $this->sql->update();
            $update->table('project_category');
            $update->set($updateData);
            $update->where(array(
                'ID' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            return true;
        }
    }

    public function getProjectCategoryById($id = false)
    {
        $sql = "SELECT * FROM project_category";
        if ($id) $sql.= " WHERE ID=" . $id;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getPackageTasksCategory()
    {
        $sql = "SELECT * FROM package_category";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getProjectProductCategory()
    {
        $sql = "SELECT * FROM project_category WHERE work_category=1";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getPackageTasksCategorylisttype($id = false)
    {
        $subquery = "SELECT ID, subcatname from package_sub_category WHERE deleted=0";
        if ($id) $subquery.= " AND catid = '" . $id . "'";
        $statement = $this->adapter->query($subquery);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getProjectProductCategorylisttype($id = false)
    {
        $subquery = "SELECT ID, subcatname from project_sub_category WHERE deleted = 0";
        if ($id) $subquery.= " AND  catid = '" . $id . "'";
        $statement = $this->adapter->query($subquery);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getPackageSubCategoryList($filters = false)
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*',
            'subcatid' => 'ID'
        ))->from(array(
            'PSC' => 'package_sub_category'
        ))->join(array(
            'PC' => 'package_category'
        ) , 'PSC.catid = PC.ID', array(
            '*'
        ) , 'left')->where(array(
            'PC.deleted' => '0'
        ))->where(array(
            'PSC.deleted' => '0'
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'catname' => 'ASC'
                ));
            }
        }

        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getProjectSubCategoryList($filters = false)
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*',
            'subcatid' => 'ID'
        ))->from(array(
            'PSC' => 'project_sub_category'
        ))->join(array(
            'PC' => 'project_category'
        ) , 'PSC.catid = PC.ID', array(
            '*'
        ) , 'left')->where(array(
            'PC.deleted' => '0'
        ))->where(array(
            'PSC.deleted' => '0'
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            } else {
                $selectObj->order(array(
                    'subcatname' => 'ASC'
                ));
            }
        }

        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else { 
            return false;
        }
    }

    public function getPackageSubCategoryById($id = false)
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*',
            'subcatid' => 'ID'
        ))->from(array(
            'PSC' => 'package_sub_category'
        ))->join(array(
            'PC' => 'package_category'
        ) , 'PSC.catid = PC.ID', array(
            '*'
        ) , 'left')->where(array(
            'PSC.deleted' => 0
        ));
        if ($id && $id != '') {
            $selectObj->where(array(
                'PSC.ID' => $id
            ));
        }

        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    function getProjectSubCategoryById($id = false)
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*',
            'subcatid' => 'ID'
        ))->from(array(
            'PSC' => 'project_sub_category'
        ))->join(array(
            'PC' => 'project_category'
        ) , 'PSC.catid = PC.ID', array(
            '*'
        ) , 'left')->where(array(
            'PSC.deleted' => 0
        ));
        if ($id && $id != '') {
            $selectObj->where(array(
                'PSC.ID' => $id
            ));
        }

        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getProjectTasksCategorylisttype($id)
    {
        $subquery = "SELECT ID, subcatname from project_sub_category where catid = '" . $id . "'";
        $statement = $this->adapter->query($subquery);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function checkAdmin($login, $pass)
    {
        $pwdhash = md5($pass);
        $sectionsql = "SELECT * FROM administration_administrators WHERE (email_address='" . $login . "' OR username='". $login ."') AND password='" . $pwdhash . "' AND active='1'";
        $statement = $this->adapter->query($sectionsql);
        
        $res = $statement->execute();
		
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getAdminRow($ID)
    {
        $sql = "SELECT * FROM administration_administrators WHERE ID='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
	 public function travelRequestcount()
    {
        $sql = "SELECT COUNT(tp.ID) as total_count FROM trips as tp";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
	public function travelRequestapproved()
    {
        $sql = "SELECT COUNT(tp.ID) as total_approve FROM trips as tp WHERE trip_status = 0";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
	public function travelRequestpending()
    {
        $sql = "SELECT COUNT(tp.ID) as total_pending FROM trips as tp WHERE trip_status = 2";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
	public function travelReqcancelled()
    {
        $sql = "SELECT COUNT(tp.ID) as total_cancelled FROM trips as tp WHERE trip_status = 3";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
	public function registeredUsers($filter = array())
    {
		$where = '';
		if(isset($filter['active'])){
			$where .= ' AND active = "'.$filter['active'].'" ';
		}
        $sql = "SELECT COUNT(uu.ID) as register_users FROM users_user as uu WHERE 1 ".$where;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
	public function latestUsers()
    {
        $sql = "SELECT COUNT(uu.ID) as latest_users FROM users_user uu WHERE uu.created >= ( CURDATE() - INTERVAL 30 DAY )";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
	public function usersReviews()
    {
        $sql = "SELECT COUNT(ur.rating) as total_reviews FROM users_reviews ur WHERE 1";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
    public function adminAccouts($filterData = array() , $ID)
    {
        $sql = "SELECT * FROM administration_administrators WHERE type != 1 AND ID != " . (int)$ID;
        if (isset($filterData['searchKey']) && isset($filterData['searchBy']) && !empty($filterData['searchKey']) && !empty($filterData['searchBy'])) {
            $sql.= ' AND  ' . $filterData['searchBy'] . ' LIKE "%' . $filterData['searchKey'] . '%"';
        }

        if (isset($filterData['searchStatus']) && !empty($filterData['searchStatus']) && $filterData['searchBy'] != 'all') {
            $staus = ($filterData['searchStatus'] == 'Disabled') ? '0' : '1';
            $sql.= ' AND  active = ' . (int)$staus;
        }

        if (isset($filterData['sortBy']) && isset($filterData['sortOrder']) && !empty($filterData['sortBy']) && !empty($filterData['sortOrder'])) {
            $sql.= ' ORDER BY ' . $filterData['sortBy'] . ' ' . $filterData['sortOrder'];
        } else {
            $sql.= ' ORDER BY ID DESC';
        }

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    function doAdminActions($action, $ids)
    {
        if ($action == 'enable') {
            $upQry = 'UPDATE administration_administrators SET active = 1 WHERE 1';
            $upQry.= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Admin account(s) are enabled successfully!';
        } else
        if ($action == 'disable') {
            $upQry = 'UPDATE administration_administrators SET active = 0 WHERE 1';
            $upQry.= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Admin account(s) are disabled successfully!';
        } else
        if ($action == 'delete') {
            $upQry = 'DELETE FROM  administration_administrators  WHERE 1';
            $upQry.= ' AND ID IN (' . implode(",", $ids) . ')';
            $statement = $this->adapter->query($upQry);
            $res = $statement->execute();
            $sucessMsg = 'Admin account(s) are deleted successfully!';
        }

        return $sucessMsg;
    }

    public function updateAdminRow($ID)
    {
        $update = $this->sql->update();
        $update->table('administration_administrators');
        $updateData['email_address'] = $_POST['email'];
        $updateData['first_name'] = $_POST['name'];
        $updateData['username'] = $_POST['username'];
        if (isset($_POST['changeUserPassword']) && !empty($_POST['changeUserPassword'])) {
            $updateData['password'] = md5($_POST['password']);
        }

        $update->set($updateData);
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return true;
    }

    public function checkAdminEmailExists($email, $ID = false)
    {
        $sql = "SELECT * FROM administration_administrators WHERE email_address='" . $email . "'";
        if ($ID != false) $sql.= ' AND ID != ' . $ID;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    function addEditAdminAccounts($user, $id = '', $action = 'add')
    {
        if ($action == 'edit') {
            $update = $this->sql->update();
            $update->table('administration_administrators');
            $update->set($user);
            $update->where(array(
                'ID' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $userid = $id;
        } else {
            $insert = $this->sql->insert('administration_administrators');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $userid;
    }

    public function getUser($userid)
    {
        $sql = "SELECT * FROM users_user WHERE ID = '" . $userid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getCountryByName($name, $type)
    {
        if ($type == 'country' && is_numeric($name)) {
            $sql = "SELECT * FROM `core_country` WHERE ID='" . $name . "'";
        } elseif ($type == 'country' && !is_numeric($name)) {
            $sql = "SELECT * FROM `core_country` WHERE name='" . $name . "'";
        } else {
            $sql = "SELECT * FROM `core_country` WHERE currency_code='" . $name . "'";
        }

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getUserByEmail($email)
    {
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function checkUserEmailExists($email)
    {
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function addUsersUser($user)
    {
        $emailExists = false;
        $email_address = $user['email_address'];
        $sql = "SELECT * FROM users_user WHERE email_address='" . $email_address . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $emailExists = true;
        }

        if ($emailExists === false) {
            $insert = $this->sql->insert('users_user');
            $newData = $user;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $userid = $this->adapter->getDriver()->getLastGeneratedValue();
            return $userid;
        } else {
            return 'emailExist';
        }
    }

    public function changeUserPassword($ID, $newpwd)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set(array(
            "reset_password" => "1"
        ));
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
    }

    public function updateUserPassword($ID, $newpwd)
    {
        $update = $this->sql->update();
        $update->table('users_user');
        $update->set(array(
            "password" => md5($newpwd) ,
            "reset_password" => "0"
        ));
        $update->where(array(
            'ID' => $ID
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
    }

    public function getAirportsBySearch($q)
    {
        $sql = "SELECT * FROM core_airports WHERE code LIKE '" . $q . "%' OR city LIKE '" . $q . "%' OR country LIKE '" . $q . "%' OR name LIKE '" . $q . "%' ORDER BY code ";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function LoadUsersAddressBook($user)
    {
        $sql = "SELECT * FROM users_user_locations WHERE user='" . $user . "' and deleted=0";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function uploadFile($field_name, $file_name_prefix, $image_path)
    {
        if (isset($_FILES[$field_name])) {
            $dirpath = str_replace('index.php', '', $_SERVER['SCRIPT_FILENAME']);
            $token = md5(uniqid(rand() , 1));
            $un = substr($token, 0, 5);
            $ext = explode('.', $_FILES[$field_name]['name']);
            $file_ext = strtolower(end($ext));
            $file_name = $file_name_prefix . "_" . $un . "." . $file_ext;
            @move_uploaded_file($_FILES[$field_name]['tmp_name'], $dirpath . $image_path . $file_name);
            return $file_name;
        }
    }

    public function getLanguages()
    {
        $sql = "SELECT * FROM language where active='1'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    public function getMailTemplateRow($template_id)
    {
        $sql = "SELECT * FROM email_templates WHERE template_id='" . $template_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getstaticPageRow($page_id)
    {
        $sql = "SELECT * FROM content_pages WHERE page_Id=" . $page_id . "";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function updateMailtemplate($template_id, $mailData)
    {
        $template_id = $template_id;
        $template_subject = $mailData['template_subject'];
        $template_name = isset($mailData['template_name']) ? $mailData['template_name'] : '';
        $template_content = $mailData['template_content'];
        $selQry = ' REPLACE INTO `email_templates` (                                             template_id,                                            template_name,                                            template_subject,                                            template_content                                         ) VALUES';
        $selQry.= ' (' . (int)$template_id . ',                                        "' . $this->cleanQuery($template_name) . '",                                        "' . $this->cleanQuery($template_subject) . '",                                        "' . addslashes($this->cleanQuery($template_content)) . '" )';
        $statement = $this->adapter->query($selQry);
        $res = $statement->execute();
        return true;
    }

    public function updateStaticpage($page_id, $pageData)
    {
        $page_id = $page_id; /*//$template_subject = $mailData['template_subject'];*/
        $page_name = isset($pageData['page_name']) ? $pageData['page_name'] : '';
        $page_content = $pageData['page_content'];
        $selQry = ' UPDATE `content_pages` SET                                            page_Last_Modified=' . $this->cleanQuery(date(time())) . ',                                            page_Content="' . addslashes($this->cleanQuery($page_content)) . '"                                         WHERE page_Id=' . (int)$page_id . ''; /*//        $selQry.=' ('. (int)$page_id .',//                                        "'. $this->cleanQuery($page_name) .'",//                                        "'. $this->cleanQuery(date(time())) .'",//                                        "'. addslashes($this->cleanQuery($page_content)) .'" )';*/
        $statement = $this->adapter->query($selQry);
        $res = $statement->execute();
        return true;
    } /* Price settings */
    function getStopsPriceSettings()
    {
        $sql = "SELECT * FROM price_settings_stops WHERE 1=1";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    function updateStopsPriceSettings($priceArray, $id)
    {
        $update = $this->sql->update();
        $update->table('price_settings_stops');
        $update->set($priceArray);
        $update->where(array(
            'price_settings_stops_id' => $id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return true;
    }

    function getDistancePriceSettings($filters = false, $resType = "list")
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('price_settings_distance')->where(array(
            'distance_setting_deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['paymentAllowed']) && $filters['paymentAllowed'] != '') {
                $selectObj->where(array(
                    'payment_allow' => $filters['paymentAllowed']
                ));
            }

            if (isset($filters['activeStatus']) && $filters['activeStatus'] != '') {
                $selectObj->where(array(
                    'active' => $filters['activeStatus']
                ));
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['greaterThanOrEqualTo']) && !empty($filters['greaterThanOrEqualTo']) && is_array($filters['greaterThanOrEqualTo'])) {
                foreach($filters['greaterThanOrEqualTo'] as $colName => $colValue) {
                    $selectObj->where->greaterThanOrEqualTo($colName, $colValue);
                }
            }

            if (isset($filters['lessThanOrEqualTo']) && !empty($filters['lessThanOrEqualTo']) && is_array($filters['lessThanOrEqualTo'])) {
                foreach($filters['lessThanOrEqualTo'] as $colName => $colValue) {
                    $selectObj->where->lessThanOrEqualTo($colName, $colValue);
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'distance_from' => 'ASC'
                ));
            }
        }

        if ($resType == 'paginator') {
            return new Paginator(new DbSelect($selectObj, $this->adapter, $this->resultSetPrototype));
        } else
        if ($resType == 'list') { /*echo $this->sql->getSqlStringForSqlObject($selectObj);exit;*/
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            }
            else return false;
        } else
        if ($resType == 'count') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            return $res->count();
        }
    }

    function addEditDistancePriceSettings($priceData, $id = false)
    {
        if ($id !== false) {
            $update = $this->sql->update();
            $update->table('price_settings_distance');
            $update->set($priceData);
            $update->where(array(
                'price_settings_distance_id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $distanceid = $id;
        } else {
            $insert = $this->sql->insert('price_settings_distance');
            $newData = $priceData;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $distanceid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $distanceid;
    }

    function getPriceItemPriceSettings($filters = false, $resType = "list")
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('price_settings_price_item')->where(array(
            'price_item_setting_deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['greaterThanOrEqualTo']) && !empty($filters['greaterThanOrEqualTo']) && is_array($filters['greaterThanOrEqualTo'])) {
                foreach($filters['greaterThanOrEqualTo'] as $colName => $colValue) {
                    $selectObj->where->greaterThanOrEqualTo($colName, $colValue);
                }
            }

            if (isset($filters['lessThanOrEqualTo']) && !empty($filters['lessThanOrEqualTo']) && is_array($filters['lessThanOrEqualTo'])) {
                foreach($filters['lessThanOrEqualTo'] as $colName => $colValue) {
                    $selectObj->where->lessThanOrEqualTo($colName, $colValue);
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'price_item_from' => 'ASC'
                ));
            }
        }

        if ($resType == 'paginator') {
            return new Paginator(new DbSelect($selectObj, $this->adapter, $this->resultSetPrototype));
        } else
        if ($resType == 'list') { /*echo $this->sql->getSqlStringForSqlObject($selectObj);exit;*/
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            }
            else return false;
        } else
        if ($resType == 'count') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            return $res->count();
        }
    }

    function addEditPriceItemPriceSettings($priceData, $id = false)
    {
        if ($id !== false) {
            $update = $this->sql->update();
            $update->table('price_settings_price_item');
            $update->set($priceData);
            $update->where(array(
                'price_settings_price_item_id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $priceItemid = $id;
        } else {
            $insert = $this->sql->insert('price_settings_price_item');
            $newData = $priceData;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $priceItemid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $priceItemid;
    }

    function getCountryPriceSettings($filters = false, $resType = "list")
    {
        $sql = "SELECT PSC.*,CCF.name AS from_country,CCT.name AS to_country FROM price_settings_country AS PSC" . " LEFT JOIN core_country AS CCF ON PSC.country_from = CCF.code" . " LEFT JOIN core_country AS CCT ON PSC.country_to = CCT.code" . " WHERE country_setting_deleted=0";
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $sql.= " AND " . $filters['searchBy'] . " LIKE '" . $this->cleanQuery($filters['searchKey'] . "%'");
            }

            if (isset($filters['paymentAllowed']) && $filters['paymentAllowed'] != '') {
                $sql.= " AND payment_allow = '" . $filters['paymentAllowed'] . "'";
            }

            if (isset($filters['activeStatus']) && $filters['activeStatus'] != '') {
                $sql.= " AND active = " . $filters['activeStatus'];
            }

            if (isset($filters['where']) && $filters['where'] != '') {
                $sql.= " AND " . $filters['where'];
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $sql.= " ORDER BY " . $filters['sortBy'] . " " . $filters['sortOrder'];
            }
            else {
                $sql.= " ORDER BY price_settings_country_id asc";
            }
        }

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($resType == 'list') {
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            }
            else return false;
        } else
        if ($resType == 'count') {
            return $res->count();
        }
    }

    function getCountryPriceSettingsLarge($filters, $resType = 'list')
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from(array(
            'PSC' => 'price_settings_country'
        ))->join(array(
            'CCF' => 'core_country'
        ) , 'PSC.country_from = CCF.code', array(
            'from_country' => 'name'
        ) , 'left')->join(array(
            'CCT' => 'core_country'
        ) , 'PSC.country_to = CCT.code', array(
            'to_country' => 'name'
        ) , 'left')->where(array(
            'country_setting_deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['paymentAllowed']) && $filters['paymentAllowed'] != '') {
                $selectObj->where(array(
                    'payment_allow' => $filters['paymentAllowed']
                ));
            }

            if (isset($filters['activeStatus']) && $filters['activeStatus'] != '') {
                $selectObj->where(array(
                    'active' => $filters['activeStatus']
                ));
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'from_country' => 'ASC'
                ));
            }
        }

        if ($resType == 'paginator') {
            return new Paginator(new DbSelect($selectObj, $this->adapter, $this->resultSetPrototype));
        } else
        if ($resType == 'list') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            }
            else return false;
        } else
        if ($resType == 'count') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            return $res->count();
        }
    }

    function addEditCountryPriceSettings($priceData, $id = false)
    {
        if ($id !== false) {
            $update = $this->sql->update();
            $update->table('price_settings_country');
            $update->set($priceData);
            $update->where(array(
                'price_settings_country_id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $countryid = $id;
        } else {
            $insert = $this->sql->insert('price_settings_country');
            $newData = $priceData;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $countryid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $countryid;
    }

    function getWeightCarriedPriceSettings($filters = false, $resType = 'list')
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('price_settings_weight')->where(array(
            'weight_setting_deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['greaterThanOrEqualTo']) && !empty($filters['greaterThanOrEqualTo']) && is_array($filters['greaterThanOrEqualTo'])) {
                foreach($filters['greaterThanOrEqualTo'] as $colName => $colValue) {
                    $selectObj->where->greaterThanOrEqualTo($colName, $colValue);
                }
            }

            if (isset($filters['lessThanOrEqualTo']) && !empty($filters['lessThanOrEqualTo']) && is_array($filters['lessThanOrEqualTo'])) {
                foreach($filters['lessThanOrEqualTo'] as $colName => $colValue) {
                    $selectObj->where->lessThanOrEqualTo($colName, $colValue);
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'weight_carried_from' => 'ASC'
                ));
            }
        }

        if ($resType == 'paginator') {
            return new Paginator(new DbSelect($selectObj, $this->adapter, $this->resultSetPrototype));
        } else
        if ($resType == 'list') { /*echo $this->sql->getSqlStringForSqlObject($selectObj);exit;*/
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            }
            else return false;
        } else
        if ($resType == 'count') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            return $res->count();
        }
    }

    function addEditWeightCarriedPriceSettings($priceData, $id = false)
    {
        if ($id !== false) {
            $update = $this->sql->update();
            $update->table('price_settings_weight');
            $update->set($priceData);
            $update->where(array(
                'price_settings_weight_id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $distanceid = $id;
        } else {
            $insert = $this->sql->insert('price_settings_weight');
            $newData = $priceData;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $distanceid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $distanceid;
    }

    function getItemCategoryPriceSettings($filters = false)
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from(array(
            'PIC' => 'price_settings_item_category'
        ))->join(array(
            'PC' => 'package_category'
        ) , 'PIC.item_category_id = PC.ID', array(
            'item_category_name' => 'catname'
        ) , 'left')->join(array(
            'PSC' => 'package_sub_category'
        ) , 'PIC.item_sub_category_id = PSC.ID', array(
            'item_sub_category_name' => 'subcatname'
        ) , 'left')->where(array(
            'PC.deleted' => 0
        ))->where(array(
            'PSC.deleted' => 0
        ))->where(array(
            'item_category_setting_deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'price_settings_item_category_id' => 'ASC'
                ));
            }
        }

        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    function addEditItemCategoryPriceSettings($priceData, $id = false)
    {
        if ($id !== false) {
            $update = $this->sql->update();
            $update->table('price_settings_item_category');
            $update->set($priceData);
            $update->where(array(
                'price_settings_item_category_id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $distanceid = $id;
        } else {
            $insert = $this->sql->insert('price_settings_item_category');
            $newData = $priceData;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $distanceid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $distanceid;
    }

    function getItemProductCategoryPriceSettings($filters = false)
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from(array(
            'PIPC' => 'price_settings_item_product_category'
        ))->join(array(
            'PC' => 'project_category'
        ) , 'PIPC.item_category_id = PC.ID', array(
            'item_category_name' => 'catname'
        ) , 'left')->join(array(
            'PSC' => 'project_sub_category'
        ) , 'PIPC.item_sub_category_id = PSC.ID', array(
            'item_sub_category_name' => 'subcatname'
        ) , 'left')->where(array(
            'PC.deleted' => 0
        ))->where(array(
            'PSC.deleted' => 0
        ))->where(array(
            'item_product_category_setting_deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'price_settings_item_product_category_id' => 'ASC'
                ));
            }
        }

        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    function addEditItemProductCategoryPriceSettings($priceData, $id = false)
    {
        if ($id !== false) {
            $update = $this->sql->update();
            $update->table('price_settings_item_product_category');
            $update->set($priceData);
            $update->where(array(
                'price_settings_item_product_category_id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $distanceid = $id;
        } else {
            $insert = $this->sql->insert('price_settings_item_product_category');
            $newData = $priceData;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $distanceid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $distanceid;
    }

    function getItemTaskCategoryPriceSettings($filters = false)
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from(array(
            'PIC' => 'price_settings_item_task_category'
        ))->join(array(
            'PC' => 'project_category'
        ) , 'PIC.item_task_category_id = PC.ID', array(
            'item_task_category_name' => 'catname'
        ) , 'left')->join(array(
            'PSC' => 'project_sub_category'
        ) , 'PIC.item_task_sub_category_id = PSC.ID', array(
            'item_task_sub_category_name' => 'subcatname'
        ) , 'left')->where(array(
            'item_task_category_setting_deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'price_settings_item_task_category_id' => 'ASC'
                ));
            }
        }

        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    function addEditItemTaskCategoryPriceSettings($priceData, $id = false)
    {
        if ($id !== false) {
            $update = $this->sql->update();
            $update->table('price_settings_item_task_category');
            $update->set($priceData);
            $update->where(array(
                'price_settings_item_task_category_id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $distanceid = $id;
        } else {
            $insert = $this->sql->insert('price_settings_item_task_category');
            $newData = $priceData;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $distanceid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $distanceid;
    } /* Price settings */
    function getPackageCategory($filters = false)
    {
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('package_category')->where(array(
            'deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'ID' => 'ASC'
                ));
            }
        }

        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->getResource()->fetchAll();
        } else return false;
    }

    function getPackageCategoryById($id = false)
    {
        $sql = "SELECT * FROM package_category";
        if ($id) $sql.= " WHERE ID=" . $id;
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function addUpdatePackageCategoryRow($updateData, $id = false)
    {
        if ($id === false) { /* Add */
            $insert = $this->sql->insert('package_category');
            $insert->values($updateData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        } else { /* Update */
            $update = $this->sql->update();
            $update->table('package_category');
            $update->set($updateData);
            $update->where(array(
                'ID' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            return $id;
        }
    }

    public function addUpdatePackageSubCategoryRow($updateData, $id = false)
    {
        if ($id === false) { /* Add */
            $insert = $this->sql->insert('package_sub_category');
            $insert->values($updateData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        } else { /* Update */
            $update = $this->sql->update();
            $update->table('package_sub_category');
            $update->set($updateData);
            $update->where(array(
                'ID' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            return $id;
        }
    }

    public function packageSubCategoryDeleteByCategory($id = false)
    { /* Update */
        $update = $this->sql->update();
        $update->table('package_sub_category');
        $update->set(array(
            'deleted' => time()
        ));
        $update->where(array(
            'catid' => $id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return $id;
    }

    public function projectSubCategoryDeleteByCategory($id = false)
    { /* Update */
        $update = $this->sql->update();
        $update->table('project_sub_category');
        $update->set(array(
            'deleted' => time()
        ));
        $update->where(array(
            'catid' => $id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return $id;
    }

    public function addUpdateTaskSubCategoryRow($updateData, $id = false)
    {
        if ($id === false) { /* Add */
            $insert = $this->sql->insert('project_sub_category');
            $insert->values($updateData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        } else { /* Update */
            $update = $this->sql->update();
            $update->table('project_sub_category');
            $update->set($updateData);
            $update->where(array(
                'ID' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            return $id;
        }
    }

    function runQuery($sql)
    {
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
    }

    public function uploadFiles($field_name, $file_name, $path)
    {
        if (isset($_FILES[$field_name])) {
            $filename = $_FILES[$field_name]['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $file_name = $file_name;
            $handle = new upload($_FILES[$field_name]);
			//print_r($handle);exit;
            if ($handle->uploaded) {
                $handle->file_new_name_body = $file_name;
                $handle->file_new_name_ext = $ext;
                $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/');
                if ($handle->processed) {
                    $handle->clean();
                    $retArr = array(
                        'result' => 'success',
                        'fileName' => $file_name . '.' . $ext
                    );
                }
                else {
                    $retArr = array(
                        'result' => 'failed',
                        'error' => $handle->error
                    );
                }
            }
            else {
                $retArr = array(
                    'result' => 'failed',
                    'error' => $handle->error
                );
            }

            return $retArr;
        }
    }

    function getTaskPriceSettings()
    {
        $sql = "SELECT * FROM price_settings_task_price WHERE 1=1";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    function updateTaskPriceSettings($priceArray, $id)
    {
        $update = $this->sql->update();
        $update->table('price_settings_task_price');
        $update->set($priceArray);
        $update->where(array(
            'price_settings_task_price_id' => $id
        ));
        $statement = $this->sql->prepareStatementForSqlObject($update);
        $results = $statement->execute();
        return true;
    } /* Download Action */
    function getCountriesSettingsExport($filters = false)
    {
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('price_settings_country')->where(array(
            'country_setting_deleted' => 0
        ));
        if (isset($filters) && !empty($filters)) {
            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }
        }

        $selectObj->order(array(
            'price_settings_country_id' => 'ASC'
        ));
        $exportPriceArr = array();
        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $resultSet = new ResultSet;
            $resultSet->initialize($res);
            foreach($resultSet as $row) {
                $exportPriceArr[$row->country_from][$row->country_to] = $row->country_price;
            }

            return $exportPriceArr;
        } else return false;
    }

    function getCustomSettingsExport()
    {
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('customs_excise_duty')->where(array(
            'custom_Deleted' => 0,
            'custom_Disabled' => 0
        ));
        $selectObj->order(array(
            'custom_Id' => 'ASC'
        ));
        $exportPriceArr = array();
        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res = $statement->execute();
        if ($res->count() > 0) {
            $resultSet = new ResultSet;
            $resultSet->initialize($res);
            foreach($resultSet as $row) {
                $exportCustomArr[0][0] = $row->custom_Countryname;
                $exportCustomArr[0][1] = $row->custom_Localcurrencys;
                $exportCustomArr[0][2] = $row->custom_Localpounds;
                $exportCustomArr[0][3] = $row->custom_Link;
            }

            return $exportCustomArr;
        } else return false;
    }

    function getCustoms($filters, $resType = 'list')
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('customs_excise_duty')->where(array(
            'custom_Deleted' => 0,
            'custom_Disabled' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['activeStatus']) && $filters['activeStatus'] != '') {
                $selectObj->where(array(
                    'active' => $filters['activeStatus']
                ));
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'custom_CountryName' => 'ASC'
                ));
            }
        }

        if ($resType == 'paginator') {
            return new Paginator(new DbSelect($selectObj, $this->adapter, $this->resultSetPrototype));
        } else
        if ($resType == 'list') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            }
            else return false;
        } else
        if ($resType == 'count') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            return $res->count();
        }
    }

    function getReports($filters, $resType = 'list')
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('report_list')->where(array(
            'report_deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['activeStatus']) && $filters['activeStatus'] != '') {
                $selectObj->where(array(
                    'active' => $filters['activeStatus']
                ));
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'report_username' => 'ASC'
                ));
            }
        }

        if ($resType == 'paginator') {
            return new Paginator(new DbSelect($selectObj, $this->adapter, $this->resultSetPrototype));
        } else
        if ($resType == 'list') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            }
            else return false;
        } else
        if ($resType == 'count') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            return $res->count();
        }
    }

    function getMessage($filters, $resType = 'list')
    {
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array(
            '*'
        ))->from('message_list')->where(array(
            'message_deleted' => 0
        ));
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'] . '%');
            }

            if (isset($filters['activeStatus']) && $filters['activeStatus'] != '') {
                $selectObj->where(array(
                    'active' => $filters['activeStatus']
                ));
            }

            if (isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])) {
                foreach($filters['where'] as $key => $where) {
                    $selectObj->where(array(
                        $key => $where
                    ));
                }
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $selectObj->order(array(
                    $filters['sortBy'] => $filters['sortOrder']
                ));
            }
            else {
                $selectObj->order(array(
                    'message_from' => 'ASC'
                ));
            }
        }

        if ($resType == 'paginator') {
            return new Paginator(new DbSelect($selectObj, $this->adapter, $this->resultSetPrototype));
        } else
        if ($resType == 'list') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            if ($res->count() > 0) {
                return $res->getResource()->fetchAll();
            }
            else return false;
        } else
        if ($resType == 'count') {
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res = $statement->execute();
            return $res->count();
        }
    }

    function addEditCustomsSettings($customData, $id = false)
    {
        if ($id !== false) {
            $update = $this->sql->update();
            $update->table('customs_excise_duty');
            $update->set($customData);
            $update->where(array(
                'custom_Id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $countryid = $id;
        } else {
            $insert = $this->sql->insert('customs_excise_duty');
            $newData = $customData;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $countryid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $countryid;
    }

    function addEditReportSettings($reportData, $id = false)
    {
        if ($id !== false) {
            $update = $this->sql->update();
            $update->table('report_list');
            $update->set($reportData);
            $update->where(array(
                'report_id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $countryid = $id;
        } else {
            $insert = $this->sql->insert('report_list');
            $newData = $reportData;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $countryid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $countryid;
    }

    function addEditMessageSettings($messageData, $id = false)
    {
        if ($id !== false) {
            $update = $this->sql->update();
            $update->table('message_list');
            $update->set($messageData);
            $update->where(array(
                'message_id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            $countryid = $id;
        } else {
            $insert = $this->sql->insert('message_list');
            $newData = $messageData;
            $insert->values($newData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            $countryid = $this->adapter->getDriver()->getLastGeneratedValue();
        }

        return $countryid;
    } /* Download Action */

    function getDistancePrice($distance, $service_type)
    {
        $sql       = "SELECT distance_price,distance_currency FROM price_settings_distance WHERE   distance_from <= '" . $distance . "' AND distance_to >= '" . $distance . "' AND service_type = '" . $service_type . "' AND distance_setting_deleted = 0";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    function getCountryPrice($from, $to, $service_type)
    {
        $sql       = "SELECT country_price,country_currency FROM price_settings_country WHERE  country_setting_deleted = 0  AND country_from ='" . $from . "' AND country_to ='" . $to . "' AND service_type = '" . $service_type . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        }
        else
        {  
            $data = [];

            if($service_type == 'people')
            {
                 $data['country_price'] = 24;
                 $data['country_currency'] = 'GBP';
            }
            else
            {
                 $data['country_price'] = 12;
                 $data['country_currency'] = 'GBP';
            }        
            return $data;
        }
    }

    public function getAirPortByAirportCode($ID)
    {
        $sql = "SELECT * FROM core_airports WHERE code ='" . $ID . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    function getNoofstopsPrice($no_of_stops)
    {
        $sql       = "SELECT stop_price,stop_price_currency FROM price_settings_stops WHERE   no_of_stops ='" . $no_of_stops . "'";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    function getItemPricePrice($money, $service_type)
    {
        $sql       = "SELECT price_item_price,price_item_currency FROM price_settings_price_item WHERE   price_item_from <= '" . $money . "' AND price_item_to >= '" . $money . "' AND service_type = '" . $service_type . "' AND price_item_setting_deleted = 0";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    function getWeightPrice($weight, $service_type)
    {
        $sql       = "SELECT weight_carried_price,weight_carried_currency FROM price_settings_weight WHERE   weight_carried_from <= '" . number_format($weight,1) . "' AND weight_carried_to >= '" . number_format($weight,1) . "' AND service_type = '" . $service_type . "' AND weight_setting_deleted = 0";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }
    function getItemCategoryPrice($item_category_id, $service_type)
    {
        $sql       = "SELECT sum(item_price) as tot_item_price FROM price_settings_item_category WHERE  CONCAT(`item_category_id`, '', `item_sub_category_id`) IN (" . $item_category_id . ") AND service_type = '" . $service_type . "'  AND item_category_setting_deleted = 0";
        $statement = $this->adapter->query($sql);
        $res       = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else
            return false;
    }

    function convertCurrency($amount, $from, $to)
    {
        if (!empty($from) && !empty($to) && $from != $to && $amount > 0) {
            //$url = "https://finance.google.com/finance/converter?a=$amount&from=$from&to=$to";
            //$url = "http://apilayer.net/api/convert?access_key=dd8df8fbd30c5e1fbe069448db63cc69&from=$from&to=$to&amount=$amount&format=1";
            $url = "https://free.currencyconverterapi.com/api/v5/convert?q=".$from."_".$to."&compact=ultra";
            $data = json_decode(file_get_contents($url));
            $keyName = $from."_".$to;
            $converted = $amount * $data->$keyName;
            return round($converted, 2);
        } else {
            return round($amount, 2);
        }
    }

    public function getTestimonial($filters = false, $resType = 'list')
    {
        $sql = "SELECT * FROM testimonials WHERE deleted = 0"; /* filter query for search */
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $sql.= " AND " . $filters['searchBy'] . " LIKE '" . $this->cleanQuery($filters['searchKey'] . "%'");
            }

            if (isset($filters['activeStatus']) && $filters['activeStatus'] != '') {
                $sql.= " AND active = " . $filters['activeStatus'];
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $sql.= " ORDER BY " . $filters['sortBy'] . " " . $filters['sortOrder'];
            }

            if (isset($filters['limit']) && $filters['limit'] != '') {
                $sql.= " LIMIT " . $filters['limit'];
                if (isset($filters['offset']) && $filters['offset'] != '') {
                    $sql.= " OFFSET " . $filters['offset'];
                }
            }
        }

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0)
            if ($resType == 'list') {
                return $res->getResource()->fetchAll();
            } else
                if ($resType == "count") {
                    return $res->count();
                }
                else return false;
    }

    public function addUpdateTestimonial($updateData, $id = false)
    {
        if ($id === false) { /* Add */
            $insert = $this->sql->insert('testimonials');
            $insert->values($updateData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        } else { /* Update */
            $update = $this->sql->update();
            $update->table('testimonials');
            $update->set($updateData);
            $update->where(array(
                'ID' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            return true;
        }
    }

    public function getTestimonialByID($countryid)
    {
        $sql = "SELECT * FROM testimonials WHERE ID='" . $countryid . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }

    public function getHelp($filters = false, $resType = 'list')
    {
        $sql = "SELECT * FROM users_question_answer WHERE 1 = 1 "; /* filter query for search */
        if ($filters) {
            if (isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != '') {
                $sql.= " AND " . $filters['searchBy'] . " LIKE '" . $this->cleanQuery($filters['searchKey'] . "%'");
            }

            if (isset($filters['activeStatus']) && $filters['activeStatus'] != '') {
                $sql.= " AND active = " . $filters['activeStatus'];
            }

            if (isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != '') {
                $sql.= " ORDER BY " . $filters['sortBy'] . " " . $filters['sortOrder'];
            }

            if (isset($filters['limit']) && $filters['limit'] != '') {
                $sql.= " LIMIT " . $filters['limit'];
                if (isset($filters['offset']) && $filters['offset'] != '') {
                    $sql.= " OFFSET " . $filters['offset'];
                }
            }
        }

        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0)
            if ($resType == 'list') {
                return $res->getResource()->fetchAll();
            } else
                if ($resType == "count") {
                    return $res->count();
                }
                else return false;
    }

    public function addUpdateHelp($updateData, $id = false)
    {
        if ($id === false) { /* Add */
            $insert = $this->sql->insert('users_question_answer');
            $insert->values($updateData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        }
        else { /* Update */
            $update = $this->sql->update();
            $update->table('users_question_answer');
            $update->set($updateData);
            $update->where(array(
                'qa_id' => $id
            ));
            $statement = $this->sql->prepareStatementForSqlObject($update);
            $results = $statement->execute();
            return true;
        }
    }

    public function getHelpByID($qa_id)
    {
        $sql = "SELECT * FROM users_question_answer WHERE qa_id ='" . $qa_id . "'";
        $statement = $this->adapter->query($sql);
        $res = $statement->execute();
        if ($res->count() > 0) {
            return $res->current();
        } else return false;
    }
}

?>
