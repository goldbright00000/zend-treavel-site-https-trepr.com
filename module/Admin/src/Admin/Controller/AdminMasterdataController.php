<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager; 
use Admin\Model\AdminUserModel;
use Zend\Mvc\Plugin\FlashMessenger;
use Zend\Mvc\Plugin\Url;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;

class AdminMasterdataController extends AbstractActionController
{
     const ITEM_PER_PAGE = 20;
     public function __construct($data = false){
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->adminModules = $data['configs']['adminModules'];
        $this->adminMenus = $data['configs']['adminMenus'];
        $this->timeZones = $data['configs']['timezones'];
        $this->AdminModel->authenticateAdmin('masterdata');
       if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           // return $this->redirect()->toRoute('admin');
       }
    }
    
    public function indexAction(){
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if($this->params()->fromPost('doAction')){
           $status = $this->AdminUserModel->doUserActions($this->params()->fromPost('doAction'),$this->params()->fromPost('ids'));
             $this->flashMessenger()->addMessage(array('alert-success' => $status));
           return $this->redirect()->toRoute('useraccounts');
        }
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
       
        $data = array();
        $arrAdminAccounts = $this->AdminUserModel->userAccouts( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        $data = !empty($arrAdminAccounts)?$arrAdminAccounts:array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator],['filterData'=>$filterData],['flashMessages' =>  $this->flashMessenger()->getMessages()],['Menu_select'=>'user_accounts']));   
    }
    
   
    public function usermanagerAction(){
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0); 
        
        if(isset($_POST['subAdminBtn'])){
            $checkEmail =  $this->AdminUserModel->checkAdminEmailExists($this->params()->fromPost('email_address'),$this->params()->fromPost('id'));
            if($checkEmail === false){
                $insData['first_name'] = $this->params()->fromPost('first_name');
                $insData['last_name'] = $this->params()->fromPost('last_name');
                $insData['email_address'] = $this->params()->fromPost('email_address');
                if(isset($_POST['password']) && !empty($_POST['password']))
                $insData['password'] = md5($this->params()->fromPost('password'));
                $insData['dob'] = !empty($this->params()->fromPost('dob'))?strtotime($this->params()->fromPost('dob')):'';
                $insData['age'] = $this->params()->fromPost('age');
                if($action == 'add') $insData['active'] = 1;
                $insData['phone'] = $this->params()->fromPost('phone');
                $insData['gender'] = $this->params()->fromPost('gender');
                $insData['language'] = implode(",", $_POST['language']);
                $insData['country'] = $this->params()->fromPost('country');
                $insData['city'] = $this->params()->fromPost('city');
                $insData['school'] = $this->params()->fromPost('school');
                $insData['work'] = $this->params()->fromPost('work');
                $insData['timezone'] = $this->params()->fromPost('timezone');
                $insData['emergency_full_name'] = $this->params()->fromPost('emergency_full_name');
                $insData['emergency_phone'] = $this->params()->fromPost('emergency_phone');
                $insData['emergency_email'] = $this->params()->fromPost('emergency_email');
                $insData['emergency_relationship'] = $this->params()->fromPost('emergency_relationship');
                
                $updateAccount =  $this->AdminUserModel->addEditUserAccounts($insData,$this->params()->fromPost('id'),$this->params()->fromPost('action'));
                $message = ($action == 'add')?'User account created successfully':'User account updated  successfully';
                if( $updateAccount){
                  $this->flashMessenger()->addMessage(array('alert-success' =>$message));
                }else{
                  $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update account. Please try again'));
                }
                $flashMessages =  $this->flashMessenger()->getMessages();
                return $this->redirect()->toRoute('useraccounts'); 
            }else{
                $flashMessages[] =  array('alert-danger' => 'Email address already being used');
            }
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $viewArray = array(
            'adminRow' => $this->AdminUserModel->getUserRow($id),
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'lang' => $this->AdminModel->getLanguages(),
            'countries' => $this->AdminModel->getCountries(),
            'site_time_zones' => $this->timeZones
        );
        
        $viewArray['Menu_select'] = 'user_accounts';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    } 
    
    public function userAddressesAction(){
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if($this->params()->fromPost('doAction')){
           $status = $this->AdminUserModel->doUserAddressActions($this->params()->fromPost('doAction'),$this->params()->fromPost('ids'));
             $this->flashMessenger()->addMessage(array('alert-success' => $status));
           return $this->redirect()->toRoute('admin/users/useraddresses');
        }
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
       
        $data = array();
        $arrAdminAccounts = $this->AdminUserModel->userAddresses( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        //echo '<pre>';print_r($arrAdminAccounts);exit;
        $data = !empty($arrAdminAccounts)?$arrAdminAccounts:array();
        $page = $this->params()->fromQuery('page',1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator],['filterData'=>$filterData],['flashMessages' =>  $this->flashMessenger()->getMessages()],['Menu_select'=>'user_accounts']));   
    }
    
    public function userAddressManagerAction(){
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0); 
        
        if(isset($_POST['subAdminBtn'])){
            $insData['user'] = $this->params()->fromPost('user');
            $insData['name'] = $this->params()->fromPost('name');
            $insData['street_address_1'] = $this->params()->fromPost('street_address_1');
            $insData['street_address_2'] = $this->params()->fromPost('street_address_2');
            $insData['city'] = $this->params()->fromPost('city');
            $insData['state'] = $this->params()->fromPost('state');
            $insData['zip_code'] = $this->params()->fromPost('zip_code');
            $insData['country'] = $this->params()->fromPost('country');
            $insData['set_default'] = $this->params()->fromPost('set_default');
            $insData['latitude'] = $this->params()->fromPost('latitude');
            $insData['longitude'] = $this->params()->fromPost('longitude');
            
            $updateAccount =  $this->AdminUserModel->addEditUserAddress($insData,$this->params()->fromPost('id'),$this->params()->fromPost('action'));
            $message = ($action == 'add')?'User address created successfully':'User address updated  successfully';
            if( $updateAccount){
                if($this->params()->fromPost('set_default') == '1'){
                    $this->AdminUserModel->setUserDefaultAddress($this->params()->fromPost('user'),$id);
                }
                $this->flashMessenger()->addMessage(array('alert-success' =>$message));
            }else{
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update address. Please try again'));
            }
            $flashMessages =  $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('useraddresses'); 
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $viewArray = array(
            'adminRow' => $this->AdminUserModel->getprofileaddr($id),
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'countries' => $this->AdminModel->getCountries(),
        );
        if($action == 'edit'){
            //Pre populate user field
            $uAcc = $this->AdminUserModel->userAccouts(array(),$viewArray['adminRow']['user']);
            if($uAcc){
                foreach($uAcc as $acc){
                    $retArr[] = array(
                        'id' => $acc['ID'],
                        'name' => $acc['email_address']
                    );
                }
                $viewArray['userPrePop'] = json_encode($retArr);
            }
        }
        $viewArray['Menu_select'] = 'user_accounts';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;
    }
    
    public function userReferencesAction(){
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if($this->params()->fromPost('doAction')){
           $status = $this->AdminUserModel->doUserAddressActions($this->params()->fromPost('doAction'),$this->params()->fromPost('ids'));
             $this->flashMessenger()->addMessage(array('alert-success' => $status));
           return $this->redirect()->toRoute('admin/users/useraddresses');
        }
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
       
        $data = array();
        $arrAdminAccounts = $this->AdminUserModel->userAddresses( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        //echo '<pre>';print_r($arrAdminAccounts);exit;
        $data = !empty($arrAdminAccounts)?$arrAdminAccounts:array();
        $page = $this->params()->fromQuery('page',1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator],['filterData'=>$filterData],['flashMessages' =>  $this->flashMessenger()->getMessages()],['Menu_select'=>'user_accounts']));   
    }
    
    public function userReferenceManagerAction(){
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0); 
        
        if(isset($_POST['subAdminBtn'])){
            $insData['name'] = $this->params()->fromPost('name');
            $insData['street_address_1'] = $this->params()->fromPost('street_address_1');
            $insData['street_address_2'] = $this->params()->fromPost('street_address_2');
            $insData['city'] = $this->params()->fromPost('city');
            $insData['state'] = $this->params()->fromPost('state');
            $insData['zip_code'] = implode(",", $_POST['zip_code']);
            $insData['country'] = $this->params()->fromPost('country');
            $insData['set_default'] = $this->params()->fromPost('set_default');
            
            $updateAccount =  $this->AdminUserModel->addEditUserAddress($insData,$this->params()->fromPost('id'),$this->params()->fromPost('action'));
            $message = ($action == 'add')?'User address created successfully':'User address updated  successfully';
            if( $updateAccount){
              $this->flashMessenger()->addMessage(array('alert-success' =>$message));
            }else{
              $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update address. Please try again'));
            }
            $flashMessages =  $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('useraddresses'); 
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $viewArray = array(
            'adminRow' => $this->AdminUserModel->getprofileaddr($id),
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'countries' => $this->AdminModel->getCountries(),
        );
        
        $viewArray['Menu_select'] = 'user_accounts';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;
    }
}
?>