<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager; 
use Admin\Model\AdminUserModel;
use Zend\Mvc\Plugin\FlashMessenger;
use Zend\Mvc\Plugin\Url;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;


class AdminPriceSettingsController extends AbstractActionController
{
     const ITEM_PER_PAGE = 20;
     public function __construct($data = false){
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->adminModules = $data['configs']['adminModules'];
        $this->adminMenus = $data['configs']['adminMenus'];
        $this->timeZones = $data['configs']['timezones'];
        
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
             
        }
    }
    
    public function indexAction(){
        /* Misc Module dashboard page */
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        $this->authenticateModule();
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['flashMessages' =>  $this->flashMessenger()->getMessages()],
            ['Menu_select'=>'price_settings'],
            ['Sub_menu_select' => 'people_service']
        ));
    }
    public function authenticateModule() {
        $resp = $this->AdminModel->authenticateAdmin('price_settings');
        if($resp) { 
            $this->flashMessenger()->addMessage($resp['msg']);
            return $this->redirect()->toRoute($resp['redirectUrl']); 
        }
    }
    
    /* Price settings */
    public function peopleServiceAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
        $filterData['status'] =  $this->params()->fromQuery('status');
        $filterData['searchFrom'] =  $this->params()->fromQuery('searchFrom'); 
        $filterData['searchTo'] =  $this->params()->fromQuery('searchTo');
        
        $flashMessages = false;
        $countryArr = false;$noOfStopsArr=false;$distanecArr=false;$countriesList=false;
        $pageType =  $this->params('type','country');
        
        if($this->getRequest()->isPost()){
            if($this->params()->fromPost('pageType') == 'country'){
                if($this->params()->fromPost('country_from') != '' && $this->params()->fromPost('country_to') != '' && $this->params()->fromPost('country_price') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('INSERT');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'peopleService','type'=>$pageType));     
                    }
                    $updateCountryData = array(
                        'service_type'  =>  'people',
                        'country_from'  =>  $this->params()->fromPost('country_from'),
                        'country_to'  =>  $this->params()->fromPost('country_to'),
                        'country_price'  =>  $this->params()->fromPost('country_price'),
                        'country_currency'  =>  $this->params()->fromPost('country_currency'),
                        'last_modified'  =>  time()
                    );
                    $editId = false;$actionTxtSuc = 'added';$actionTxtFail = 'add';
                    if($this->params()->fromPost('editCountrySettingId') != ''){
                        $editId = $this->params()->fromPost('editCountrySettingId');
                        $actionTxtSuc = 'update';
                        $actionTxtFail = 'updated';
                    }
                    $result = $this->AdminModel->addEditCountryPriceSettings($updateCountryData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting is ".$actionTxtSuc." successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to ".$actionTxtFail." price setting. Please try again later.!"));
                    }
                } else if($this->params()->fromPost('doAction') == 'delete'){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp){
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'peopleService','type'=>$pageType));     
                    }
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('country_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditCountryPriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadPricingSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'peopleService','type'=>$pageType));     
                    }
                    $uploadedFile = $this->params()->fromFiles('uploadPricingSettings');
                    $file_name = 'country_price_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadPricingSettings', $file_name, 'country_price_settings');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvAction('uploads/country_price_settings', $upResult['fileName'], 'GBP', 'people');
                    }
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price setting(s). Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'noofstops'){
                $stopPrices = $this->params()->fromPost('stopPrices');
                $stopPrices_currency = $this->params()->fromPost('stopPrices_currency');
                if(!empty($stopPrices)){
                    foreach($stopPrices as $key=>$price){
                        $updateStopsData = array(
                            'stop_price'  =>  $price,
                            'stop_price_currency'  =>  $stopPrices_currency[$key],
                            'last_modified'  =>  time()
                        );
                        $result = $this->AdminModel->updateStopsPriceSettings($updateStopsData,$key);
                    }
                }
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) was updated successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price setting(s). Please try again later.!"));
                }
            }
            if($this->params()->fromPost('pageType') == 'distance'){
                if($this->params()->fromPost('doAction') == 'delete'){
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('distance_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditDistancePriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadPricingSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'peopleService','type'=>$pageType));     
                    }
                    $uploadedFile = $this->params()->fromFiles('uploadPricingSettings');
                    $file_name = 'distance_price_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadPricingSettings', $file_name, 'distance_price_settings');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvDistance('uploads/distance_price_settings', $upResult['fileName'], 'GBP', 'people');
                    }
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Distance price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update distance price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromPost('distance_from') != '' && $this->params()->fromPost('distance_to') != '' && $this->params()->fromPost('distance_price') != ''){
                    $updateDistanceData = array(
                        'service_type'  => 'people',
                        'distance_from'  =>  $this->params()->fromPost('distance_from'),
                        'distance_to'  =>  $this->params()->fromPost('distance_to'),
                        'distance_price'  =>  $this->params()->fromPost('distance_price'),
                        'distance_currency'  =>  $this->params()->fromPost('distance_currency'),
                        'last_modified'  =>  time()
                    );
                    $editId = ($this->params()->fromPost('editDistanceId') != '')?$this->params()->fromPost('editDistanceId'):false;
                    $result = $this->AdminModel->addEditDistancePriceSettings($updateDistanceData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price settings was update(d) successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price setting(s). Please try again later.!"));
                    }
                }
            }
            return $this->redirect()->toRoute('admin_price_settings', array(
                'controller' => 'admin',
                'action' =>  'people_service',
                'type' => $this->params()->fromPost('pageType')
            ));
        }
        $paginator=false;
        $currecyFilter = array(
            'where' => array(
                'payment_allow' => 1
            )
        );
        $currencyArr = $this->AdminModel->getAllCurrencies($currecyFilter);
        if($pageType == 'country'){
            if($this->params()->fromQuery('searchFrom') != ''){
                $filterData['where']['country_from']=$this->params()->fromQuery('searchFrom');
            }
            if($this->params()->fromQuery('searchTo') != ''){
                $filterData['where']['country_to']=$this->params()->fromQuery('searchTo');
            }
            ini_set("memory_limit","1024M");
            $countriesList = $this->AdminModel->getCountries();
            $filterData['where']['service_type'] = "people";
            $page = $this->params()->fromQuery('page', 1);
            $paginator = $this->AdminModel->getCountryPriceSettingsLarge($filterData,'paginator');
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'noofstops'){
            $noOfStopsArr = $this->AdminModel->getStopsPriceSettings();
        }
        else if($pageType == 'distance'){
            if($this->params()->fromQuery('searchFrom') != ''){
                $filterData['greaterThanOrEqualTo']['distance_from']=$this->params()->fromQuery('searchFrom');
            }
            if($this->params()->fromQuery('searchTo') != ''){
                $filterData['lessThanOrEqualTo']['distance_to']=$this->params()->fromQuery('searchTo');
            }
            $filterData['where']['service_type'] = "people";
            $distanecArr = $this->AdminModel->getDistancePriceSettings($filterData);
            $distanecArr = ($distanecArr)?$distanecArr:array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($distanecArr));
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['flashMessages' =>  $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'price_settings'],
            ['Sub_menu_select' => 'price_settings/people_service'],
            ['paginator' => $paginator],
            ['filterData'=>$filterData],
            ['pageType' => $pageType],
            ['countriesList' => $countriesList],
            ['noOfStopsArr' => $noOfStopsArr],
            ['distanecArr' => $distanecArr],
            ['currencyArr' => $currencyArr]
        ));
    }
    
    public function packageServiceAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
        $filterData['status'] =  $this->params()->fromQuery('status');
        $filterData['searchFrom'] =  $this->params()->fromQuery('searchFrom'); 
        $filterData['searchTo'] =  $this->params()->fromQuery('searchTo');
        
        $flashMessages = false;
        $countryArr = false;$weightCarriedArr=false;$itemCategoryArr=false;$countriesList=false;$packageCategories=false;$distanecArr=false;$priceItemcArr=false;
        $pageType =  $this->params('type','country');
        
        if($this->getRequest()->isPost()){
            if($this->params()->fromPost('pageType') == 'country'){
                if($this->params()->fromPost('country_from') != '' && $this->params()->fromPost('country_to') != '' && $this->params()->fromPost('country_price') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('INSERT');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'packageService','type'=>$pageType));     
                    }
                    $updateCountryData = array(
                        'service_type'  =>  'package',
                        'country_from'  =>  $this->params()->fromPost('country_from'),
                        'country_to'  =>  $this->params()->fromPost('country_to'),
                        'country_price'  =>  $this->params()->fromPost('country_price'),
                        'country_currency'  =>  $this->params()->fromPost('country_currency'),
                        'last_modified'  =>  time()
                    );
                    $editId = false;$actionTxtSuc = 'added';$actionTxtFail = 'add';
                    if($this->params()->fromPost('editCountrySettingId') != ''){
                        $editId = $this->params()->fromPost('editCountrySettingId');
                        $actionTxtSuc = 'update';
                        $actionTxtFail = 'updated';
                    }
                    $result = $this->AdminModel->addEditCountryPriceSettings($updateCountryData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting is ".$actionTxtSuc." successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to ".$actionTxtFail." price setting. Please try again later.!"));
                    }
                } else if($this->params()->fromPost('doAction') == 'delete'){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp){
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'packageService','type'=>$pageType));     
                    }
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('country_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditCountryPriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadPricingSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'packageService','type'=>$pageType));     
                    }
                    $uploadedFile = $this->params()->fromFiles('uploadPricingSettings');
                    $file_name = 'country_price_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadPricingSettings', $file_name, 'country_price_settings');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvAction('uploads/country_price_settings', $upResult['fileName'], 'GBP', 'package');
                    }
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price setting(s). Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'distance'){
                if($this->params()->fromPost('doAction') == 'delete'){
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('distance_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditDistancePriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadPricingSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'packageService','type'=>$pageType));     
                    }
                    $uploadedFile = $this->params()->fromFiles('uploadPricingSettings');
                    $file_name = 'distance_price_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadPricingSettings', $file_name, 'distance_price_settings');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvDistance('uploads/distance_price_settings', $upResult['fileName'], 'GBP', 'package');
                    }
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Distance price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update distance price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromPost('distance_from') != '' && $this->params()->fromPost('distance_to') != '' && $this->params()->fromPost('distance_price') != ''){
                    $updateDistanceData = array(
                        'service_type'  => 'package',
                        'distance_from'  =>  $this->params()->fromPost('distance_from'),
                        'distance_to'  =>  $this->params()->fromPost('distance_to'),
                        'distance_price'  =>  $this->params()->fromPost('distance_price'),
                        'distance_currency'  =>  $this->params()->fromPost('distance_currency'),
                        'last_modified'  =>  time(),
                    );
                    $editId = ($this->params()->fromPost('editDistanceId') != '')?$this->params()->fromPost('editDistanceId'):false;
                    $result = $this->AdminModel->addEditDistancePriceSettings($updateDistanceData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price settings was update(d) successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price setting(s). Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'item_category'){
                if($this->params()->fromPost('doAction') == 'delete'){
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('item_category_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditItemCategoryPriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else {
                    $updateItemCategoryData = array(
                        'service_type'  =>  'package',
                        'item_category_id'  =>  $this->params()->fromPost('item_category_id'),
                        'item_sub_category_id'  =>  $this->params()->fromPost('item_sub_category_id'),
                        'item_price'  =>  $this->params()->fromPost('item_price'),
                        'item_currency'  =>  $this->params()->fromPost('item_currency'),
                        'last_modified'  =>  time()
                    );
                    $editId = ($this->params()->fromPost('editItemCategoryId') != '')?$this->params()->fromPost('editItemCategoryId'):false;
                    $result = $this->AdminModel->addEditItemCategoryPriceSettings($updateItemCategoryData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Item category price settings was update successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update item category price setting. Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'price_item'){
                if($this->params()->fromPost('doAction') == 'delete'){
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('price_item_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditPriceItemPriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadPricingSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'packageService','type'=>$pageType));     
                    }
                    $uploadedFile = $this->params()->fromFiles('uploadPricingSettings');
                    $file_name = 'price_item_price_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadPricingSettings', $file_name, 'price_item_price_settings');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvPriceItem('uploads/price_item_price_settings', $upResult['fileName'], 'GBP', 'package');
                    }
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price Item price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price item price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromPost('price_item_from') != '' && $this->params()->fromPost('price_item_to') != '' && $this->params()->fromPost('price_item_price') != ''){
                    $updatePriceItemData = array(
                        'service_type'  => 'package',
                        'price_item_from'  =>  $this->params()->fromPost('price_item_from'),
                        'price_item_to'  =>  $this->params()->fromPost('price_item_to'),
                        'price_item_price'  =>  $this->params()->fromPost('price_item_price'),
                        'price_item_currency'  =>  $this->params()->fromPost('price_item_currency'),
                        'last_modified'  =>  time(),
                    );
                    $editId = ($this->params()->fromPost('editPriceItemId') != '')?$this->params()->fromPost('editPriceItemId'):false;
                    $result = $this->AdminModel->addEditPriceItemPriceSettings($updatePriceItemData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price settings was update(d) successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price setting(s). Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'weight_carried'){
                if($this->params()->fromPost('doAction') == 'delete'){
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('weight_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditWeightCarriedPriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Weight carried price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete weight carried price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadPricingSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'packageService','type'=>$pageType));     
                    }
                    $uploadedFile = $this->params()->fromFiles('uploadPricingSettings');
                    $file_name = 'weight_carried_price_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadPricingSettings', $file_name, 'weight_carried_price_settings');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvWeightCarried('uploads/weight_carried_price_settings', $upResult['fileName'], 'GBP', 'package');
                    }
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Weight carried price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update weight carried price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromPost('weight_carried_from') != '' && $this->params()->fromPost('weight_carried_to') != '' && $this->params()->fromPost('weight_carried_price') != ''){
                    $updateWeightCarriedData = array(
                        'service_type'  =>  'package',
                        'weight_carried_from'  =>  $this->params()->fromPost('weight_carried_from'),
                        'weight_carried_to'  =>  $this->params()->fromPost('weight_carried_to'),
                        'weight_carried_price'  =>  $this->params()->fromPost('weight_carried_price'),
                        'weight_carried_currency'  =>  $this->params()->fromPost('weight_carried_currency'),
                        'last_modified'  =>  time(),
                    );
                    $editId = ($this->params()->fromPost('editWeightCarriedId') != '')?$this->params()->fromPost('editWeightCarriedId'):false;
                    $result = $this->AdminModel->addEditWeightCarriedPriceSettings($updateWeightCarriedData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Weight carried price settings was update(d) successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update weight carried price setting(s). Please try again later.!"));
                    }
                }
            }
            return $this->redirect()->toRoute('admin_price_settings', array(
                'controller' => 'admin',
                'action' =>  'package_service',
                'type' => $this->params()->fromPost('pageType')
            ));
        }
        $paginator=false;
        $currecyFilter = array(
            'where' => array(
                'payment_allow' => 1
            )
        );
        $currencyArr = $this->AdminModel->getAllCurrencies($currecyFilter);
        if($pageType == 'country'){
            if($this->params()->fromQuery('searchFrom') != ''){
                $filterData['where']['country_from']=$this->params()->fromQuery('searchFrom');
            }
            if($this->params()->fromQuery('searchTo') != ''){
                $filterData['where']['country_to']=$this->params()->fromQuery('searchTo');
            }
            ini_set("memory_limit","1024M");
            $countriesList = $this->AdminModel->getCountries();
            $filterData['where'] = array('service_type'=>"package");
            $page = $this->params()->fromQuery('page', 1);
            $paginator = $this->AdminModel->getCountryPriceSettingsLarge($filterData,'paginator');
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'distance'){
            if($this->params()->fromQuery('searchFrom') != ''){
                $filterData['greaterThanOrEqualTo']['distance_from']=$this->params()->fromQuery('searchFrom');
            }
            if($this->params()->fromQuery('searchTo') != ''){
                $filterData['lessThanOrEqualTo']['distance_to']=$this->params()->fromQuery('searchTo');
            }
            $filterData['where']['service_type'] = "package";
            $distanecArr = $this->AdminModel->getDistancePriceSettings($filterData);
            $distanecArr = ($distanecArr)?$distanecArr:array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($distanecArr));
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'item_category'){
            $packageCategories = $this->AdminModel->getPackageCategory();
            $filterData['where'] = array("service_type"=>"package"); 
            $itemCategoryArrArr = $this->AdminModel->getItemCategoryPriceSettings($filterData);
            $itemCategoryArrArr = ($itemCategoryArrArr)?$itemCategoryArrArr:array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($itemCategoryArrArr));
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'price_item'){
            if($this->params()->fromQuery('searchFrom') != ''){
                $filterData['greaterThanOrEqualTo']['price_item_from']=$this->params()->fromQuery('searchFrom');
            }
            if($this->params()->fromQuery('searchTo') != ''){
                $filterData['lessThanOrEqualTo']['price_item_to']=$this->params()->fromQuery('searchTo');
            }
            $filterData['where']['service_type'] = "package";
            $priceItemcArr = $this->AdminModel->getPriceItemPriceSettings($filterData);
            $priceItemcArr = ($priceItemcArr)?$priceItemcArr:array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($priceItemcArr));
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'weight_carried'){
            if($this->params()->fromQuery('searchFrom') != ''){
                $filterData['greaterThanOrEqualTo']['weight_carried_from']=$this->params()->fromQuery('searchFrom');
            }
            if($this->params()->fromQuery('searchTo') != ''){
                $filterData['lessThanOrEqualTo']['weight_carried_to']=$this->params()->fromQuery('searchTo');
            }
            $filterData['where']['service_type'] = "package";
            $weightCarriedArr = $this->AdminModel->getWeightCarriedPriceSettings($filterData);
            $weightCarriedArr = ($weightCarriedArr)?$weightCarriedArr:array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($weightCarriedArr));
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['flashMessages' =>  $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'price_settings'],
            ['Sub_menu_select' => 'price_settings/package_service'],
            ['paginator' => $paginator],
            ['filterData'=>$filterData],
            ['pageType' => $pageType],
            ['countryArr' => $countryArr],
            ['countriesList' => $countriesList],
            ['weightCarriedArr' => $weightCarriedArr],
            ['packageCategoryArr' => $packageCategories],
            ['currencyArr' => $currencyArr],
            ['distanecArr' => $distanecArr],
            ['priceItemcArr' => $priceItemcArr]
        ));
    }
    
    public function projectServiceAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
        $filterData['status'] =  $this->params()->fromQuery('status');
        
        $flashMessages = false;
        $countryArr = false;$weightCarriedArr=false;$itemCategoryArr=false;$countriesList=false;
        $packageCategories=false;$taskPrice=false;$projectCategories=false;
        $pageType =  $this->params('type','country');
        
        if($this->getRequest()->isPost()){
            if($this->params()->fromPost('pageType') == 'country'){
                if($this->params()->fromPost('country_from') != '' && $this->params()->fromPost('country_to') != '' && $this->params()->fromPost('country_price') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('INSERT');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'projectService','type'=>$pageType));     
                    }
                    $updateCountryData = array(
                        'service_type'  =>  'project',
                        'country_from'  =>  $this->params()->fromPost('country_from'),
                        'country_to'  =>  $this->params()->fromPost('country_to'),
                        'country_price'  =>  $this->params()->fromPost('country_price'),
                        'country_currency'  =>  $this->params()->fromPost('country_currency'),
                        'last_modified'  =>  time()
                    );
                    $editId = false;$actionTxtSuc = 'added';$actionTxtFail = 'add';
                    if($this->params()->fromPost('editCountrySettingId') != ''){
                        $editId = $this->params()->fromPost('editCountrySettingId');
                        $actionTxtSuc = 'update';
                        $actionTxtFail = 'updated';
                    }
                    $result = $this->AdminModel->addEditCountryPriceSettings($updateCountryData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting is ".$actionTxtSuc." successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to ".$actionTxtFail." price setting. Please try again later.!"));
                    }
                } else if($this->params()->fromPost('doAction') == 'delete'){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp){
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'projectService','type'=>$pageType));     
                    }
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('country_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditCountryPriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadPricingSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'projectService','type'=>$pageType));     
                    }
                    $uploadedFile = $this->params()->fromFiles('uploadPricingSettings');
                    $file_name = 'country_price_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadPricingSettings', $file_name, 'country_price_settings');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvAction('uploads/country_price_settings', $upResult['fileName'], 'GBP', 'package');
                    }
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price setting(s). Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'distance'){
                if($this->params()->fromPost('doAction') == 'delete'){
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('distance_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditDistancePriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadPricingSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'projectService','type'=>$pageType));     
                    }
                    $uploadedFile = $this->params()->fromFiles('uploadPricingSettings');
                    $file_name = 'distance_price_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadPricingSettings', $file_name, 'distance_price_settings');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvDistance('uploads/distance_price_settings', $upResult['fileName'], 'GBP', 'project');
                    }
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Distance price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update distance price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromPost('distance_from') != '' && $this->params()->fromPost('distance_to') != '' && $this->params()->fromPost('distance_price') != ''){
                    $updateDistanceData = array(
                        'service_type'  => 'project',
                        'distance_from'  =>  $this->params()->fromPost('distance_from'),
                        'distance_to'  =>  $this->params()->fromPost('distance_to'),
                        'distance_price'  =>  $this->params()->fromPost('distance_price'),
                        'distance_currency'  =>  $this->params()->fromPost('distance_currency'),
                        'last_modified'  =>  time(),
                    );
                    $editId = ($this->params()->fromPost('editDistanceId') != '')?$this->params()->fromPost('editDistanceId'):false;
                    $result = $this->AdminModel->addEditDistancePriceSettings($updateDistanceData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price settings was update(d) successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price setting(s). Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'item_product_category'){
                if($this->params()->fromPost('doAction') == 'delete'){
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('item_product_category_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditItemProductCategoryPriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else {
                    $updateItemCategoryData = array(
                        'service_type'  =>  'project',
                        'item_category_id'  =>  $this->params()->fromPost('item_category_id'),
                        'item_sub_category_id'  =>  $this->params()->fromPost('item_sub_category_id'),
                        'item_price'  =>  $this->params()->fromPost('item_price'),
                        'item_currency'  =>  $this->params()->fromPost('item_currency'),
                        'last_modified'  =>  time()
                    );
                    $editId = ($this->params()->fromPost('editItemCategoryId') != '')?$this->params()->fromPost('editItemCategoryId'):false;
                    $result = $this->AdminModel->addEditItemProductCategoryPriceSettings($updateItemCategoryData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Item category price settings was update successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update item category price setting. Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'item_task_category'){
                if($this->params()->fromPost('doAction') == 'delete'){
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('item_task_category_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditItemTaskCategoryPriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else {
                    $updateItemTaskCategoryData = array(
                        'service_type'  =>  'project',
                        'item_task_category_id'  =>  $this->params()->fromPost('item_task_category_id'),
                        'item_task_sub_category_id'  =>  $this->params()->fromPost('item_task_sub_category_id'),
                        'item_task_price'  =>  $this->params()->fromPost('item_task_price'),
                        'item_task_currency'  =>  $this->params()->fromPost('item_task_currency'),
                        'last_modified'  =>  time()
                    );
                    $editId = ($this->params()->fromPost('editItemTaskCategoryId') != '')?$this->params()->fromPost('editItemTaskCategoryId'):false;
                    $result = $this->AdminModel->addEditItemTaskCategoryPriceSettings($updateItemTaskCategoryData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Task category price settings was update successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update task category price setting. Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'price_item'){
                if($this->params()->fromPost('doAction') == 'delete'){
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('price_item_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditPriceItemPriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadPricingSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'projectService','type'=>$pageType));     
                    }
                    $uploadedFile = $this->params()->fromFiles('uploadPricingSettings');
                    $file_name = 'price_item_price_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadPricingSettings', $file_name, 'price_item_price_settings');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvPriceItem('uploads/price_item_price_settings', $upResult['fileName'], 'GBP', 'project');
                    }
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price Item price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price item price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromPost('price_item_from') != '' && $this->params()->fromPost('price_item_to') != '' && $this->params()->fromPost('price_item_price') != ''){
                    $updatePriceItemData = array(
                        'service_type'  => 'project',
                        'price_item_from'  =>  $this->params()->fromPost('price_item_from'),
                        'price_item_to'  =>  $this->params()->fromPost('price_item_to'),
                        'price_item_price'  =>  $this->params()->fromPost('price_item_price'),
                        'price_item_currency'  =>  $this->params()->fromPost('price_item_currency'),
                        'last_modified'  =>  time(),
                    );
                    $editId = ($this->params()->fromPost('editPriceItemId') != '')?$this->params()->fromPost('editPriceItemId'):false;
                    $result = $this->AdminModel->addEditPriceItemPriceSettings($updatePriceItemData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price settings was update(d) successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price setting(s). Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'weight_carried'){
                if($this->params()->fromPost('doAction') == 'delete'){
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('weight_setting_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditWeightCarriedPriceSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Weight carried price setting(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete weight carried price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadPricingSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_price_settings', array('controller' => 'admin','action' =>'packageService','type'=>$pageType));     
                    }
                    $uploadedFile = $this->params()->fromFiles('uploadPricingSettings');
                    $file_name = 'weight_carried_price_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadPricingSettings', $file_name, 'weight_carried_price_settings');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvWeightCarried('uploads/weight_carried_price_settings', $upResult['fileName'], 'GBP', 'project');
                    }
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Weight carried price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update weight carried price setting(s). Please try again later.!"));
                    }
                } else if($this->params()->fromPost('weight_carried_from') != '' && $this->params()->fromPost('weight_carried_to') != '' && $this->params()->fromPost('weight_carried_price') != ''){
                    $updateWeightCarriedData = array(
                        'service_type'  =>  'project',
                        'weight_carried_from'  =>  $this->params()->fromPost('weight_carried_from'),
                        'weight_carried_to'  =>  $this->params()->fromPost('weight_carried_to'),
                        'weight_carried_price'  =>  $this->params()->fromPost('weight_carried_price'),
                        'weight_carried_currency'  =>  $this->params()->fromPost('weight_carried_currency'),
                        'last_modified'  =>  time(),
                    );
                    $editId = ($this->params()->fromPost('editWeightCarriedId') != '')?$this->params()->fromPost('editWeightCarriedId'):false;
                    $result = $this->AdminModel->addEditWeightCarriedPriceSettings($updateWeightCarriedData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Weight carried price settings was update(d) successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update weight carried price setting(s). Please try again later.!"));
                    }
                }
            }
            if($this->params()->fromPost('pageType') == 'task_price'){
                if($this->params()->fromPost('task_price_fixed') != '' && $this->params()->fromPost('task_price_hourly') != ''){
                    $updateTaskData = array(
                        'service_type'  =>  'project',
                        'task_price_hourly'  => $this->params()->fromPost('task_price_hourly'),
                        'task_price_fixed'  => $this->params()->fromPost('task_price_fixed'),
                        'task_currency_hourly'  => $this->params()->fromPost('task_currency_hourly'),
                        'task_currency_fixed'  => $this->params()->fromPost('task_currency_fixed'),
                        'last_modified'  =>  time()
                    );
                    $result = $this->AdminModel->updateTaskPriceSettings($updateTaskData,$this->params()->fromPost('taskPriceId'));
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Price setting(s) was updated successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update price setting(s). Please try again later.!"));
                    }
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "You have to fill both fixed and hourly prices.!"));
                    return $this->redirect()->toRoute('admin_price_settings', array(
                        'controller' => 'admin',
                        'action' =>  'project_service',
                        'type' => $this->params()->fromPost('pageType')
                    ));
                }
            }
            return $this->redirect()->toRoute('admin_price_settings', array(
                'controller' => 'admin',
                'action' =>  'project_service',
                'type' => $this->params()->fromPost('pageType')
            ));
        }
        $paginator=false;
        $currecyFilter = array(
            'where' => array(
                'payment_allow' => 1
            )
        );
        $currencyArr = $this->AdminModel->getAllCurrencies($currecyFilter);
        if($pageType == 'country'){
            ini_set("memory_limit","1024M");
            $countriesList = $this->AdminModel->getCountries();
            $filterData['where'] = array('service_type'=>"project");
            $page = $this->params()->fromQuery('page', 1);
            $paginator = $this->AdminModel->getCountryPriceSettingsLarge($filterData,'paginator');
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'distance'){
            if($this->params()->fromQuery('searchFrom') != ''){
                $filterData['greaterThanOrEqualTo']['distance_from']=$this->params()->fromQuery('searchFrom');
            }
            if($this->params()->fromQuery('searchTo') != ''){
                $filterData['lessThanOrEqualTo']['distance_to']=$this->params()->fromQuery('searchTo');
            }
            $filterData['where']['service_type'] = "project";
            $distanecArr = $this->AdminModel->getDistancePriceSettings($filterData);
            $distanecArr = ($distanecArr)?$distanecArr:array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($distanecArr));
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'item_product_category'){
            /*$projectCategories = $this->AdminModel->getProjectProductCategory();*/
            $projectCategories = $this->AdminModel->getProjectTasksCategorytype('1');
            $filterData['where'] = array("service_type"=>"project");
            $itemCategoryArrArr = $this->AdminModel->getItemProductCategoryPriceSettings($filterData);
            $itemCategoryArrArr = ($itemCategoryArrArr)?$itemCategoryArrArr:array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($itemCategoryArrArr));
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'item_task_category'){
            $projectCategories = $this->AdminModel->getProjectTasksCategorytype('0');
            $filterData['where'] = array("service_type"=>"project");
            $itemTaskCategoryArr = $this->AdminModel->getItemTaskCategoryPriceSettings($filterData);
            $itemTaskCategoryArr = ($itemTaskCategoryArr)?$itemTaskCategoryArr:array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($itemTaskCategoryArr));
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'price_item'){
            if($this->params()->fromQuery('searchFrom') != ''){
                $filterData['greaterThanOrEqualTo']['price_item_from']=$this->params()->fromQuery('searchFrom');
            }
            if($this->params()->fromQuery('searchTo') != ''){
                $filterData['lessThanOrEqualTo']['price_item_to']=$this->params()->fromQuery('searchTo');
            }
            $filterData['where']['service_type'] = "project";
            $priceItemcArr = $this->AdminModel->getPriceItemPriceSettings($filterData);
            $priceItemcArr = ($priceItemcArr)?$priceItemcArr:array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($priceItemcArr));
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'weight_carried'){
            $filterData['where']['service_type'] = "project";
            $weightCarriedArr = $this->AdminModel->getWeightCarriedPriceSettings($filterData);
            $weightCarriedArr = ($weightCarriedArr)?$weightCarriedArr:array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($weightCarriedArr));
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
        else if($pageType == 'task_price'){
            $taskPrice = $this->AdminModel->getTaskPriceSettings();
        }
        
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['flashMessages' =>  $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'price_settings'],
            ['Sub_menu_select' => 'price_settings/project_service'],
            ['paginator' => $paginator],
            ['filterData'=>$filterData],
            ['pageType' => $pageType],
            ['countryArr' => $countryArr],
            ['countriesList' => $countriesList],
            ['weightCarriedArr' => $weightCarriedArr],
            ['projectCategoryArr' => $projectCategories],
            ['taskPrice' => $taskPrice],
            ['currencyArr' => $currencyArr]
        ));
    }
    /* Price settings */
    
    public function countryPricingAction(){
        $service_type=$this->params('type');$currency="GBP";$rowLimit = 0;
        $domestic = $this->params()->fromQuery('domestic');
        $international = $this->params()->fromQuery('international');
        
        $filterAllData['sortBy']="code";$filterAllData['sortOrder']="asc";
        $allCountries = $this->AdminModel->getCountries($filterAllData);
        $sliceLimit = 21;$sliceOffset = '0';
        $loopCount = ceil(count($allCountries)/$sliceLimit);
        
        for($i=0;$i<$loopCount;$i++){
            $sql = "REPLACE INTO price_settings_country (service_type,country_from,country_to,country_price,country_currency,last_modified)";
            $sqlValues = array();
            if($i > 0){
                $sliceOffset = ($sliceLimit*$i);
            }
            $filterData = array('limit'=>$sliceLimit,'offset'=>$sliceOffset);
            $filterData['sortBy']="code";$filterData['sortOrder']="asc";
            $countriesArray = $this->AdminModel->getCountries($filterData);
            foreach ($countriesArray as $countryFrom){
                foreach ($allCountries as $countryTo){
                    $priceValue = ($countryFrom['code'] == $countryTo['code'])?$domestic:$international;
                    $sqlValues[] = "("
                        . "'".$this->AdminModel->cleanQuery($service_type)."',"
                        . "'".$this->AdminModel->cleanQuery($countryFrom['code'])."',"
                        . "'".$this->AdminModel->cleanQuery($countryTo['code'])."',"
                        . (float)$this->AdminModel->cleanQuery($priceValue).","
                        . "'".$this->AdminModel->cleanQuery($currency)."',"
                        . time()
                        . ")";
                }
            }
            $sql .= " VALUES ";
            $sql .= implode(',',$sqlValues);
            $this->AdminModel->runQuery($sql);
        }
        exit;
    }
    
    public function readCsvAction($path, $fileName, $currency, $service_type){
        $row = 1;$headers = array();$fromCountries = array();
        $sql = "REPLACE INTO price_settings_country (service_type,country_from,country_to,country_price,country_currency,last_modified)";
        $sqlValues = array();
        if (($handle = fopen(ACTUAL_ROOTPATH.$path."/".$fileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                $num = count($data);
                if($row == 1){
                    for ($c=0; $c < $num; $c++) {
                        $toCountries[] =  $data[$c];
                    }
                }
                for ($c=0; $c < $num; $c++) {
                    if($c != 0 && $row != 1){
                        $from = trim(preg_replace('/\s+/', ' ', $data[0]));
                        $to = trim(preg_replace('/\s+/', ' ', $toCountries[$c]));
                        $priceSettings[$from][$to] = $data[$c]; 
                        $sqlValues[] = "("
                            . "'".$this->AdminModel->cleanQuery($service_type)."',"
                            . "'".$this->AdminModel->cleanQuery($from)."',"
                            . "'".$this->AdminModel->cleanQuery($to)."',"
                            . (float)$this->AdminModel->cleanQuery($data[$c]).","
                            . "'".$this->AdminModel->cleanQuery($currency)."',"
                            . time()
                            . ")";
                    }
                }
                $row++;
            }
            fclose($handle);
        }
        if(!empty($sqlValues)){
            $srcArray = $sqlValues;
            $sliceLimit = 5000;
            $totalArraySize = count($srcArray);
            $loopCount = ceil($totalArraySize/$sliceLimit);
            for($i=0;$i<$loopCount;$i++){
                $sql = "REPLACE INTO price_settings_country (service_type,country_from,country_to,country_price,country_currency,last_modified)";
                $startLimit = ($sliceLimit*$i);
                $input = $srcArray;
                $slicedArr = array_slice($input,$startLimit,$sliceLimit);
                $sql .= " VALUES ";
                $sql .= implode(',',$slicedArr);
                $this->AdminModel->runQuery($sql);
            }
        }
        return true; exit;
    }
    
    public function readCsvDistance($path, $fileName, $currency, $service_type){
        $row = 1;$headers = array();$fromCountries = array();
        ini_set('display_errors',1);
        error_reporting(E_ALL);
        $sqlValues = array();
        if (($handle = fopen(ACTUAL_ROOTPATH.$path."/".$fileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                if($row != 1){
                    $sqlValues[] = "("
                        . "'".$this->AdminModel->cleanQuery($service_type)."',"
                        . "'".$this->AdminModel->cleanQuery($data[0])."',"
                        . "'".$this->AdminModel->cleanQuery($data[1])."',"
                        . (float)$this->AdminModel->cleanQuery($data[2]).","
                        . "'".$this->AdminModel->cleanQuery($currency)."',"
                        . time()
                        . ")";
                }
                $row++;
            }
            fclose($handle);
        }
        if(!empty($sqlValues)){
            $srcArray = $sqlValues;
            $sliceLimit = 5000;
            $totalArraySize = count($srcArray);
            $loopCount = ceil($totalArraySize/$sliceLimit);
            for($i=0;$i<$loopCount;$i++){
                $sql = "REPLACE INTO price_settings_distance (service_type,distance_from,distance_to,distance_price,distance_currency,last_modified)";
                $startLimit = ($sliceLimit*$i);
                $input = $srcArray;
                $slicedArr = array_slice($input,$startLimit,$sliceLimit);
                $sql .= " VALUES ";
                $sql .= implode(',',$slicedArr);
                $this->AdminModel->runQuery($sql);
            }
        }
        return true; exit;
    }
    
    public function readCsvPriceItem($path, $fileName, $currency, $service_type){
        $row = 1;$headers = array();$fromCountries = array();
        $sqlValues = array();
        if (($handle = fopen(ACTUAL_ROOTPATH.$path."/".$fileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                if($row != 1){
                    $sqlValues[] = "("
                        . "'".$this->AdminModel->cleanQuery($service_type)."',"
                        . (float)$this->AdminModel->cleanQuery($data[0]).","
                        . (float)$this->AdminModel->cleanQuery($data[1]).","
                        . (float)$this->AdminModel->cleanQuery($data[2]).","
                        . "'".$this->AdminModel->cleanQuery($currency)."',"
                        . time()
                        . ")";
                }
                $row++;
            }
            fclose($handle);
        }
         
        if(!empty($sqlValues)){
            $srcArray = $sqlValues;
            $sliceLimit = 5000;
            $totalArraySize = count($srcArray);
            $loopCount = ceil($totalArraySize/$sliceLimit);
            for($i=0;$i<$loopCount;$i++){
                $sql = "REPLACE INTO price_settings_price_item (service_type,price_item_from,price_item_to,price_item_price,price_item_currency,last_modified)";
                $startLimit = ($sliceLimit*$i);
                $input = $srcArray;
                $slicedArr = array_slice($input,$startLimit,$sliceLimit);
                $sql .= " VALUES ";
                $sql .= implode(',',$slicedArr);
                $this->AdminModel->runQuery($sql);
            }
        }
        return true; exit;
    }
    
    public function readCsvWeightCarried($path, $fileName, $currency, $service_type){
        $row = 1;$headers = array();$fromCountries = array();
        $sqlValues = array();
        if (($handle = fopen(ACTUAL_ROOTPATH.$path."/".$fileName, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
                if($row != 1){
                    $sqlValues[] = "("
                        . "'".$this->AdminModel->cleanQuery($service_type)."',"
                        . (float)$this->AdminModel->cleanQuery($data[0]).","
                        . (float)$this->AdminModel->cleanQuery($data[1]).","
                        . (float)$this->AdminModel->cleanQuery($data[2]).","
                        . "'".$this->AdminModel->cleanQuery($currency)."',"
                        . time()
                        . ")";
                }
                $row++;
            }
            fclose($handle);
        }
        if(!empty($sqlValues)){
            $srcArray = $sqlValues;
            $sliceLimit = 5000;
            $totalArraySize = count($srcArray);
            $loopCount = ceil($totalArraySize/$sliceLimit);
            for($i=0;$i<$loopCount;$i++){
                $sql = "REPLACE INTO price_settings_weight (service_type,weight_carried_from,weight_carried_to,weight_carried_price,weight_carried_currency,last_modified)";
                $startLimit = ($sliceLimit*$i);
                $input = $srcArray;
                $slicedArr = array_slice($input,$startLimit,$sliceLimit);
                $sql .= " VALUES ";
                $sql .= implode(',',$slicedArr);
                $this->AdminModel->runQuery($sql);
            }
        }
        return true; exit;
    }
    
    public function importCountryCurrencyAction(){
        $row = 1;$headers = array();$fromCountries = array();$user_type = 'seeker';$currency='USD';
        $sql = "INSERT INTO core_country_new (code,currency_code,name,region,continents,currency_names)";
        $sqlValues = array();
        if (($handle = fopen(ACTUAL_ROOTPATH."data/countries_and_currencies.csv", "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 1000, ",")) !== FALSE) {
                $num = count($data);
                if($row != 1){
                    $sqlValues[] = "("
                        . "'".mysql_real_escape_string($data[0])."',"
                        . "'".mysql_real_escape_string($data[5])."',"
                        . "'".mysql_real_escape_string($data[1])."',"
                        . "'".mysql_real_escape_string($data[2])."',"
                        . "'".mysql_real_escape_string($data[3])."',"
                        . "'".mysql_real_escape_string($data[4])."'"
                        . ")";
                }
                $row++;
            }
            fclose($handle);
        }
        if(!empty($sqlValues)){
            $sql .= " VALUES ";
            $sql .= implode(',',$sqlValues);
        }
        
        $this->AdminModel->runQuery($sql);
        return true; exit;
    }
}
?>