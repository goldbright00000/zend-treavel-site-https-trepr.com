<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager; 
use Admin\Model\AdminUserModel;
use Zend\Mvc\Plugin\FlashMessenger;
use Zend\Mvc\Plugin\Url;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;

class MasterdataController extends AbstractActionController
{
     const ITEM_PER_PAGE = 20;
     public function __construct($data = false){
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->adminModules = $data['configs']['adminModules'];
        $this->adminMenus = $data['configs']['adminMenus'];
        $this->timeZones = $data['configs']['timezones'];
        
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            /* return $this->redirect()->toRoute('admin'); */
        }
    }
    
    public function indexAction(){
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        $this->authenticateModule();
    }
    public function authenticateModule(){
        $resp = $this->AdminModel->authenticateAdmin('masterdata');
        if($resp) {
            $this->flashMessenger()->addMessage($resp['msg']);
            return $this->redirect()->toRoute($resp['redirectUrl']);
        }
    }
    public function mailtemplateAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $id =  $this->params()->fromQuery('id',1); 
        if(isset($_POST['subAdminBtn'])){
            $priv = 'UPDATE';
            $resp = $this->AdminModel->checkAdminPrivilage($priv);             
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                $flashMessages =  $this->flashMessenger()->getMessages();                 
                                  
            }
            if(!$flashMessages) {
                $template_id = $this->params()->fromPost('template_id');
                
                $insData['template_subject'] = $this->params()->fromPost('template_subject');
                $insData['template_content'] = $this->params()->fromPost('template_content');
                $insData['template_name'] = $this->params()->fromPost('template_name');
                $updateAccount =  $this->AdminModel->updateMailtemplate($template_id,$insData);
                $message = 'Mail template updated successfully';
                if( $updateAccount){
                  $this->flashMessenger()->addMessage(array('alert-success' =>$message));
                }else{
                  $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update template. Please try again'));
                }
                $flashMessages =  $this->flashMessenger()->getMessages();
                return $this->redirect()->toRoute('mailtemplate'); 
            }
        }
        
        $viewArray = array(
            'templateRow' => $this->AdminModel->getMailTemplateRow($id),
            'flashMessages' =>   $flashMessages,
            'templateId' =>   $id,
        );
        
        $viewArray['Menu_select'] = 'masterdata';
        $viewArray['Sub_menu_select'] = 'masterdata/mailtemplate';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    }
      public function staticmanagerAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $id =  $this->params()->fromQuery('id',1); 
        if(isset($_POST['subAdminBtn'])){
            $priv = 'UPDATE';
            $resp = $this->AdminModel->checkAdminPrivilage($priv);             
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                $flashMessages =  $this->flashMessenger()->getMessages();                 
                                  
            }
            if(!$flashMessages) {
                $page_id = $this->params()->fromPost('page_id');
                
                 /*//$insData['template_subject'] = $this->params()->fromPost('template_subject');*/
                $insData['page_content'] = $this->params()->fromPost('page_content');
                $insData['page_name'] = $this->params()->fromPost('page_name');
                $updateAccount =  $this->AdminModel->updateStaticpage($page_id,$insData);
                $message = 'Page content updated successfully';
                if( $updateAccount){
                  $this->flashMessenger()->addMessage(array('alert-success' =>$message));
                }else{
                  $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update template. Please try again'));
                }
                $flashMessages =  $this->flashMessenger()->getMessages();
                return $this->redirect()->toRoute('admin_static_manager'); 
            }
        }
        
        $viewArray = array(
            'templateRow' => $this->AdminModel->getstaticPageRow($id),
            'flashMessages' =>   $flashMessages,
            'templateId' =>   $id,
        );
        
        $viewArray['Menu_select'] = 'masterdata';
        $viewArray['Sub_menu_select'] = 'masterdata/static-manager';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    }
    public function customsAction(){
         $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
        $filterData['status'] =  $this->params()->fromQuery('status');

        
        $flashMessages = false;
        $countryArr = false;$noOfStopsArr=false;$distanecArr=false;$countriesList=false;
        $pageType =  $this->params('type','custom');
        if($this->getRequest()->isPost()){
         
                if($this->params()->fromPost('custom_Countryname') != '' && $this->params()->fromPost('custom_Localcurrencys') != '' && $this->params()->fromPost('custom_Localpounds') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('INSERT');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_customs_excise_duty_list', array('controller' => 'admin','action' =>'customs'));     
                    }
                    $updateCountryData = array(
                        'custom_Countryname'  =>  $this->params()->fromPost('custom_Countryname'),
                        'custom_Localcurrencys'  =>  $this->params()->fromPost('custom_Localcurrencys'),
                        'custom_currency'  =>  $this->params()->fromPost('country_currency'),
                        'custom_Localpounds'  =>  $this->params()->fromPost('custom_Localpounds'),
                        'custom_Link'  =>  $this->params()->fromPost('custom_Link'),
                        'custom_Added'  =>  time()
                    );
                    $editId = false;$actionTxtSuc = 'added';$actionTxtFail = 'add';
                    if($this->params()->fromPost('editCustomId') != ''){
                        $editId = $this->params()->fromPost('editCustomId');
                        $actionTxtSuc = 'update';
                        $actionTxtFail = 'updated';
                    }
                    $result = $this->AdminModel->addEditCustomsSettings($updateCountryData,$editId);
                    if($result){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Custom is ".$actionTxtSuc." successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to ".$actionTxtFail." Custom. Please try again later.!"));
                    }
                } else if($this->params()->fromPost('doAction') == 'delete'){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp){
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_customs_excise_duty_list', array('controller' => 'admin','action' =>'customs'));     
                    }
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('custom_Deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditCustomsSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Custom(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete Custom(s). Please try again later.!"));
                    }
                } else if($this->params()->fromFiles('uploadCustomSettings') != ''){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp) {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_customs_excise_duty_list', array('controller' => 'admin','action' =>'customs'));     
                    }
                    
                    $uploadedFile = $this->params()->fromFiles('uploadCustomSettings');
                    $file_name = 'custom_'.time();
                    $upResult = $this->AdminModel->uploadFiles('uploadCustomSettings', $file_name, 'custom');
                    $result=false;
                    if($upResult['result'] == 'success'){
                        $result = $this->readCsvAction('uploads/custom', $upResult['fileName']);
                    }
                    if($result){
                        if($result=='true')
                        $this->flashMessenger()->addMessage(array('alert-success' => "Custom(s) was updated successfully!"));
                        else
                            $this->flashMessenger()->addMessage(array('alert-danger' => $result));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to update Custom(s). Please try again later.!"));
                    }
                }
          
       
            return $this->redirect()->toRoute('admin_customs_excise_duty_list', array(
                'controller' => 'admin',
                'action' =>  'customs'
            ));
        }
        $paginator=false;
        $currencyArr = $this->AdminModel->getAllCurrencies();
        if($pageType == 'custom'){
            if($this->params()->fromQuery('searchFrom') != ''){
                $filterData['where']['country_from']=$this->params()->fromQuery('searchFrom');
            }
            if($this->params()->fromQuery('searchTo') != ''){
                $filterData['where']['country_to']=$this->params()->fromQuery('searchTo');
            }
            ini_set("memory_limit","1024M");
            $countriesList = $this->AdminModel->getCountries();
            /*$filterData['where']['service_type'] = "";*/
            $page = $this->params()->fromQuery('page', 1);
            $paginator = $this->AdminModel->getCustoms($filterData,'paginator');
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        }
   
        
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['flashMessages' =>  $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'masterdata'],
            ['Sub_menu_select' => 'masterdata/customs_excise_duty_list'],
            ['paginator' => $paginator],
            ['filterData'=>$filterData],
            ['pageType' => $pageType],
            ['countriesList' => $countriesList],
            ['currencyArr' => $currencyArr]
        )); 
    }
     public function reportlistAction(){
         $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
        $filterData['status'] =  $this->params()->fromQuery('status');

        
        $flashMessages = false;
        $countryArr = false;$noOfStopsArr=false;$distanecArr=false;
        $pageType =  $this->params('type','custom');
        if($this->getRequest()->isPost()){
         
               if($this->params()->fromPost('doAction') == 'delete'){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp){
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_report_list', array('controller' => 'admin','action' =>'report_list'));     
                    }
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('report_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditReportSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Report(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete Report(s). Please try again later.!"));
                    }
                } 
          
       
            return $this->redirect()->toRoute('admin_report_list', array(
                'controller' => 'admin',
                'action' =>  'reportlist'
            ));
        }
        $paginator=false;
            ini_set("memory_limit","1024M");
            /*$filterData['where']['service_type'] = "";*/
            $page = $this->params()->fromQuery('page', 1);
            $paginator = $this->AdminModel->getReports($filterData,'paginator');
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        
   
        
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['flashMessages' =>  $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'masterdata'],
            ['Sub_menu_select' => 'masterdata/report_list'],
            ['paginator' => $paginator],
            ['filterData'=>$filterData]
        )); 
    }
     public function messagelistAction(){
         $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
        $filterData['status'] =  $this->params()->fromQuery('status');

        
        $flashMessages = false;
        $countryArr = false;$noOfStopsArr=false;$distanecArr=false;
        if($this->getRequest()->isPost()){
         
               if($this->params()->fromPost('doAction') == 'delete'){
                    $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
                    if($resp){
                        $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                        return  $this->redirect()->toRoute('admin_report_list', array('controller' => 'admin','action' =>'report_list'));     
                    }
                    $deletedResult=false;
                    foreach ($this->params()->fromPost('ids') as $delId){
                        $delUpdate = array('message_deleted'=>time());
                        $deletedResult = $this->AdminModel->addEditMessageSettings($delUpdate, $delId);
                    }
                    if($deletedResult){
                        $this->flashMessenger()->addMessage(array('alert-success' => "Message(s) are deleted successfully!"));
                    } else {
                        $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to delete Message(s). Please try again later.!"));
                    }
                } 
          
       
            return $this->redirect()->toRoute('admin_message_list', array(
                'controller' => 'admin',
                'action' =>  'messagelist'
            ));
        }
        $paginator=false;
            ini_set("memory_limit","1024M");
            /*$filterData['where']['service_type'] = "";*/
            $page = $this->params()->fromQuery('page', 1);
            $paginator = $this->AdminModel->getMessage($filterData,'paginator');
            $paginator->setCurrentPageNumber($page)
                      ->setItemCountPerPage(self::ITEM_PER_PAGE);
        
   
        
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['flashMessages' =>  $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'masterdata'],
            ['Sub_menu_select' => 'masterdata/message_list'],
            ['paginator' => $paginator],
            ['filterData'=>$filterData]
        )); 
    }
    public function readCsvAction($path, $fileName){
        $row = 1;$headers = array();$fromCountries = array();
        $sql = "REPLACE INTO customs_excise_duty (custom_Countryname,custom_Localcurrencys,custom_Localpounds,custom_Link,custom_Added)";
        $sqlValues = array();
        if (($handle = fopen(ACTUAL_ROOTPATH.$path."/".$fileName, "r")) !== FALSE) {$attributematch=false;
            while (($importdata = fgetcsv($handle, 10000, ",")) !== FALSE)
           { 
               if($importdata[0]=='Country' && $importdata[1]=='Limit in Local Currency' && $importdata[2]=='Limit in Pounds' && $importdata[3]=='Link'){$attributematch=true;continue;}
                  $docdata = array(
                      'custom_Countryname' =>$importdata[0],
                      'custom_Localcurrencys'=> (float)preg_replace("/[^0-9\.-]/", "", $importdata[1]),
                      'custom_currency'=>preg_replace("/[^A-Z]+/", "",$importdata[1]),
                      'custom_Localpounds' => (float)preg_replace("/[^0-9\.-]/", "", $importdata[2]),
                      'custom_Link' => $importdata[3],
                      'custom_Added' =>date(time()),
                      );
               if($attributematch){
               $Result = $this->AdminModel->addEditCustomsSettings($docdata);
               }
               else { return 'Attributes Not matched: Required ["Country","Limit in Local Currency","Limit in Pounds","Link"]';}
       
	} 
            fclose($handle);
            return "true";
        }
        return true; exit;
    }
    
    public function countrylistAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_country_list');                      
            }
            if($this->params()->fromPost('doAction') == 'disable'){
                $updateCountryData['active'] = 0;
            }
            if($this->params()->fromPost('doAction') == 'enable'){
                $updateCountryData['active'] = 1;
            }
            if($this->params()->fromPost('doAction') == 'delete'){
                $updateCountryData['deleted'] = time();
            }
            foreach($this->params()->fromPost('ids') as $countryId){
                $this->AdminModel->addUpdateCountryRow($updateCountryData,$countryId);
            }
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['paymentAllowed'] =  $this->params()->fromQuery('paymentAllowed'); 
        $filterData['activeStatus'] =  $this->params()->fromQuery('activeStatus');
        $countries = $this->AdminModel->getCountries( $filterData );
        $data = !empty($countries)?$countries:array();
        
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'masterdata'],
                ['Sub_menu_select' => 'masterdata/country_list'],
                ['params'  => $this->params()]
            ));
    }
    
    public function countrymanagerAction(){  
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';

        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
            $resp = $this->AdminModel->checkAdminPrivilage($priv);               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_country_list');                      
            }
            if($action == 'add' || $action == 'edit'){
                $updateCountryData = array(
                    'code'  =>  $this->params()->fromPost('code'),
                    'currency_code'  =>  $this->params()->fromPost('currency_code'),
                    'name'  =>  $this->params()->fromPost('name'),
                    'payment_allow'  =>  $this->params()->fromPost('payment_allow')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminModel->addUpdateCountryRow($updateCountryData,$rowId);
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Country info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to add country info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin_country_list');
            }
        }
        $countryData = false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $countryData = $this->AdminModel->getCountry($id);
            if(empty($countryData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid country selected!"));
                return $this->redirect()->toRoute('admin_country_list');
            }
        }
       
        $viewArray = array(
            'adminRow' => $countryData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );
        
        $viewArray['Menu_select'] = 'masterdata';
        $viewArray['Sub_menu_select'] = 'masterdata/country_list';
       
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;
    }

    public function languagelistAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_language_list');                      
            }
            if($this->params()->fromPost('doAction') == 'disable'){
                $updatelanguageData['active'] = 0;
            }
            if($this->params()->fromPost('doAction') == 'enable'){
                $updatelanguageData['active'] = 1;
            }
            if($this->params()->fromPost('doAction') == 'delete'){
                $updatelanguageData['deleted'] = time();
            }
            foreach($this->params()->fromPost('ids') as $languageId){
                $this->AdminModel->addUpdateLanguageRow($updatelanguageData,$languageId);
            }
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['paymentAllowed'] =  $this->params()->fromQuery('paymentAllowed'); 
        $filterData['activeStatus'] =  $this->params()->fromQuery('activeStatus');
        $languages = $this->AdminModel->getLanguagesList( $filterData );
        $data = !empty($languages)?$languages:array();
        
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'masterdata'],
                ['Sub_menu_select' => 'masterdata/language_list'],
                ['params'  => $this->params()]
            ));
    }
    
    public function languagemanagerAction(){  
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';

        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
            $resp = $this->AdminModel->checkAdminPrivilage($priv);               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_language_list');                      
            }
            if($action == 'add' || $action == 'edit'){
                $updatelanguageData = array(
                    'lang'  =>  $this->params()->fromPost('lang')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminModel->addUpdateLanguageRow($updatelanguageData,$rowId);
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Language info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to add language info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin_language_list');
            }
        }
        $languageData = false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $languageData = $this->AdminModel->getLanguage($id);
            if(empty($languageData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid language selected!"));
                return $this->redirect()->toRoute('admin_language_list');
            }
        }
       
        $viewArray = array(
            'adminRow' => $languageData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );
        
        $viewArray['Menu_select'] = 'masterdata';
        $viewArray['Sub_menu_select'] = 'masterdata/language_list';
       
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;
    }
    
    public function itemCategoryListAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'item-category-list'
                    ));                      
            }
            if($this->params()->fromPost('doAction') == 'delete'){
                $updateItemCategoryData['deleted'] = time();
            }
            foreach($this->params()->fromPost('ids') as $categoryId){
                $this->AdminModel->addUpdatePackageCategoryRow($updateItemCategoryData,$categoryId);
            }
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy');
        $itemCategories = $this->AdminModel->getPackageCategory( $filterData );
        $data = !empty($itemCategories)?$itemCategories:array();
        
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'masterdata'],
                ['Sub_menu_select' => 'masterdata/item-category-list'],
                ['params'  => $this->params()]
            )); 
    }
    
    public function itemCategoryManagerAction(){  
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';

        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
            $resp = $this->AdminModel->checkAdminPrivilage($priv);               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'item-category-list'
                    ));                    
            }
            if($action == 'add'){
                /* start update category */
                $updateCategoryData = array(
                    'catname'  =>  $this->params()->fromPost('catname'),
                    'status'  =>  '1',
                    'modified_date' => date('Y-m-d H:i:s')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $catIdCreated = $this->AdminModel->addUpdatePackageCategoryRow($updateCategoryData,$rowId);
                /* end update category */
                
                /* start update sub category */
                $subCategories =  $this->params()->fromPost('subCategories');
                if(isset($subCategories['add']) && !empty($subCategories['add'])){
                    $subCatNames = array_values($subCategories['add']);
                    foreach($subCatNames as $key => $subCatNames){
                        $updateSubCategoryData = array(
                            'catid'         =>  $catIdCreated,
                            'subcatname'    =>  $subCatNames,
                            'status'  =>  '1',
                            'modified_date' =>  time()
                        );
                        $result = $this->AdminModel->addUpdatePackageSubCategoryRow($updateSubCategoryData,false);
                    }
                }
                /* end update sub category */
                
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Category info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to add category info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'item-category-list'
                    ));
            }
            
            if($action == 'edit' && $id != '' && $id != 0){
                /* start update category */
                $updateCategoryData = array(
                    'catname'  =>  $this->params()->fromPost('catname'),
                    'status'  =>  '1',
                    'modified_date' => date('Y-m-d H:i:s')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminModel->addUpdatePackageCategoryRow($updateCategoryData,$rowId);
                /* end update category */
                $this->AdminModel->packageSubCategoryDeleteByCategory($rowId);
                /* start update sub category */
                $subCategories =  $this->params()->fromPost('subCategories');
                if(isset($subCategories['update']) && !empty($subCategories['update'])){
                    $subCatNames = array_values($subCategories['update']);
                    $subCatIds = array_keys($subCategories['update']);
                    foreach($subCatIds as $key => $subCat){
                        $updateSubCategoryData = array(
                            'subcatname'    =>  $subCatNames[$key],
                            'status'  =>  '1',
                            'modified_date' =>  time(),
                            'deleted' =>  0
                        );
                        $result = $this->AdminModel->addUpdatePackageSubCategoryRow($updateSubCategoryData,$subCat);
                    }
                }
                if(isset($subCategories['add']) && !empty($subCategories['add'])){
                    $subCatNames = array_values($subCategories['add']);
                    foreach($subCatNames as $key => $subCatNames){
                        $updateSubCategoryData = array(
                            'catid'         =>  $id,
                            'subcatname'    =>  $subCatNames,
                            'status'  =>  '1',
                            'modified_date' =>  time()
                        );
                        $result = $this->AdminModel->addUpdatePackageSubCategoryRow($updateSubCategoryData,false);
                    }
                }
                /* end update sub category */
                
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Category info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to add category info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'item-category-list'
                    ));
            }
        }
        $categoryData = false;$subCategoryData=false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $categoryData = $this->AdminModel->getPackageCategoryById($id);
            $subCategoryData = $this->AdminModel->getPackageTasksCategorylisttype($id);
            if(empty($categoryData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid category selected!"));
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'item-category-list'
                    ));
            }
        }
        
        $viewArray = array(
            'categoryRow' => $categoryData,
            'subCategoryRow' => $subCategoryData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );
        
        $viewArray['Menu_select'] = 'masterdata';
        $viewArray['Sub_menu_select'] = 'masterdata/item-category-list';
       
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    }
    
    public function taskCategoryListAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'task-category-list'
                    ));                      
            }
            if($this->params()->fromPost('doAction') == 'delete'){
                $updateProductCategoryData['deleted'] = time();
            }
            foreach($this->params()->fromPost('ids') as $categoryId){
                $this->AdminModel->addUpdateProjectCategoryRow($updateProductCategoryData,$categoryId);
            }
            
             return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'task-category-list'
                    ));
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $productCategories = $this->AdminModel->getProjectTasksCategorytype('0', $filterData );

        $data = !empty($productCategories)?$productCategories:array();
        
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'masterdata'],
                ['Sub_menu_select' => 'masterdata/task-category-list'],
                ['params'  => $this->params()]
            )); 
    }
    
    public function taskCategoryManagerAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';

        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
             
            $resp = $this->AdminModel->checkAdminPrivilage($priv);               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'task-category-list'
                    ));                    
            }
            if($action == 'add'){
                /* start update category */
                $updateCategoryData = array(
                    'catname'  =>  $this->params()->fromPost('catname'),
                    'status'  =>  '1',
                    'modified_date' => date('Y-m-d H:i:s')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $catIdCreated = $this->AdminModel->addUpdateProjectCategoryRow($updateCategoryData,$rowId);
                /* end update category */
                
                /* start update sub category */
                $subCategories =  $this->params()->fromPost('subCategories');
                if(isset($subCategories['add']) && !empty($subCategories['add'])){
                    $subCatNames = array_values($subCategories['add']);
                    foreach($subCatNames as $key => $subCatNames){
                        $updateSubCategoryData = array(
                            'catid'         =>  $catIdCreated,
                            'subcatname'    =>  $subCatNames,
                            'status'  =>  '1',
                            'modified_date' =>  time()
                        );
                        $result = $this->AdminModel->addUpdateTaskSubCategoryRow($updateSubCategoryData,false);
                    }
                }
                /* end update sub category */
                
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Category info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to add category info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'task-category-list'
                    ));
            }
            
            if($action == 'edit' && $id != '' && $id != 0){
                /* start update category */
                $updateCategoryData = array(
                    'catname'  =>  $this->params()->fromPost('catname'),
                    'status'  =>  '1',
                    'modified_date' => date('Y-m-d H:i:s')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminModel->addUpdateProjectCategoryRow($updateCategoryData,$rowId);
                /* end update category */
                $this->AdminModel->projectSubCategoryDeleteByCategory($rowId);
                /* start update sub category */
                $subCategories =  $this->params()->fromPost('subCategories');
                if(isset($subCategories['update']) && !empty($subCategories['update'])){
                    $subCatNames = array_values($subCategories['update']);
                    $subCatIds = array_keys($subCategories['update']);
                    foreach($subCatIds as $key => $subCat){
                        $updateSubCategoryData = array(
                            'subcatname'    =>  $subCatNames[$key],
                            'status'  =>  '1',
                            'modified_date' =>  time(),
                            'deleted' =>  0
                        );
                        $result = $this->AdminModel->addUpdateTaskSubCategoryRow($updateSubCategoryData,$subCat);
                    }
                }
                if(isset($subCategories['add']) && !empty($subCategories['add'])){
                    $subCatNames = array_values($subCategories['add']);
                    foreach($subCatNames as $key => $subCatNames){
                        $updateSubCategoryData = array(
                            'catid'         =>  $id,
                            'subcatname'    =>  $subCatNames,
                            'status'  =>  '1',
                            'modified_date' =>  time()
                        );
                        $result = $this->AdminModel->addUpdateTaskSubCategoryRow($updateSubCategoryData,false);
                    }
                }
                /* end update sub category */
                
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Category info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to add category info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'task-category-list'
                    ));
            }
        }
        $categoryData = false;$subCategoryData=false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $categoryData = $this->AdminModel->getProjectCategoryById($id);
            $subCatFilter = array('where'=>array('catid'=>$id));
            $subCategoryData = $this->AdminModel->getProjectSubCategoryList($subCatFilter);
            
            if(empty($categoryData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid category selected!"));
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'task-category-list'
                    ));
            }
        }
        
        $viewArray = array(
            'categoryRow' => $categoryData,
            'subCategoryRow' => $subCategoryData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );
        
        $viewArray['Menu_select'] = 'masterdata';
        $viewArray['Sub_menu_select'] = 'masterdata/task-category-list';
       
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    }
    
    public function productCategoryListAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'product-category-list'
                    ));                      
            }
            if($this->params()->fromPost('doAction') == 'delete'){
                $updateProductCategoryData['deleted'] = time();
            }
            foreach($this->params()->fromPost('ids') as $categoryId){
                $this->AdminModel->addUpdateProjectCategoryRow($updateProductCategoryData,$categoryId);
            }
            
            return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'product-category-list'
                    )); 
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $productCategories = $this->AdminModel->getProjectTasksCategorytype('1', $filterData );

        $data = !empty($productCategories)?$productCategories:array();
        
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'masterdata'],
                ['Sub_menu_select' => 'masterdata/product-category-list'],
                ['params'  => $this->params()]
            )); 
    }
    
    public function productCategoryManagerAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';

        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
             
            $resp = $this->AdminModel->checkAdminPrivilage($priv);               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'product-category-list'
                    ));                    
            }
            if($action == 'add'){
                /* start update category */
                $updateCategoryData = array(
                    'catname'  =>  $this->params()->fromPost('catname'),
                    'status'  =>  '1',
                     'work_category'  =>  '1',
                    'modified_date' => date('Y-m-d H:i:s')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $catIdCreated = $this->AdminModel->addUpdateProjectCategoryRow($updateCategoryData,$rowId);
                /* end update category */
                
                /* start update sub category */
                $subCategories =  $this->params()->fromPost('subCategories');
                if(isset($subCategories['add']) && !empty($subCategories['add'])){
                    $subCatNames = array_values($subCategories['add']);
                    foreach($subCatNames as $key => $subCatNames){
                        $updateSubCategoryData = array(
                            'catid'         =>  $catIdCreated,
                            'subcatname'    =>  $subCatNames,
                            'status'  =>  '1',
                            'modified_date' =>  time()
                        );
                        $result = $this->AdminModel->addUpdateTaskSubCategoryRow($updateSubCategoryData,false);
                    }
                }
                /* end update sub category */
                
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Category info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to add category info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'product-category-list'
                    ));
            }
            
            if($action == 'edit' && $id != '' && $id != 0){
                /* start update category */
                $updateCategoryData = array(
                    'catname'  =>  $this->params()->fromPost('catname'),
                    'status'  =>  '1',
                    'modified_date' => date('Y-m-d H:i:s')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminModel->addUpdateProjectCategoryRow($updateCategoryData,$rowId);
                /* end update category */
                $this->AdminModel->projectSubCategoryDeleteByCategory($rowId);
                /* start update sub category */
                $subCategories =  $this->params()->fromPost('subCategories');
                if(isset($subCategories['update']) && !empty($subCategories['update'])){
                    $subCatNames = array_values($subCategories['update']);
                    $subCatIds = array_keys($subCategories['update']);
                    foreach($subCatIds as $key => $subCat){
                        $updateSubCategoryData = array(
                            'subcatname'    =>  $subCatNames[$key],
                            'status'  =>  '1',
                            'modified_date' =>  time(),
                            'deleted' =>  0
                        );
                        $result = $this->AdminModel->addUpdateTaskSubCategoryRow($updateSubCategoryData,$subCat);
                    }
                }
                if(isset($subCategories['add']) && !empty($subCategories['add'])){
                    $subCatNames = array_values($subCategories['add']);
                    foreach($subCatNames as $key => $subCatNames){
                        $updateSubCategoryData = array(
                            'catid'         =>  $id,
                            'subcatname'    =>  $subCatNames,
                            'status'  =>  '1',
                            'modified_date' =>  time()
                        );
                        $result = $this->AdminModel->addUpdateTaskSubCategoryRow($updateSubCategoryData,false);
                    }
                }
                /* end update sub category */
                
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Category info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to add category info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'product-category-list'
                    ));
            }
        }
        $categoryData = false;$subCategoryData=false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $categoryData = $this->AdminModel->getProjectCategoryById($id);
            $subCatFilter = array('where'=>array('catid'=>$id));
            $subCategoryData = $this->AdminModel->getProjectSubCategoryList($subCatFilter);
             
            if(empty($categoryData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid category selected!"));
                return $this->redirect()->toRoute('admin_masterdata',
                    array(
                        'controller'=>  'admin',
                        'action'    =>  'product-category-list'
                    ));
            }
        }
        
        $viewArray = array(
            'categoryRow' => $categoryData,
            'subCategoryRow' => $subCategoryData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );
        
        $viewArray['Menu_select'] = 'masterdata';
        $viewArray['Sub_menu_select'] = 'masterdata/product-category-list';
       
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    }

    public function testimonialAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_testimonial');
            }
            if($this->params()->fromPost('doAction') == 'disable'){
                $updateTestimonialData['active'] = 0;
            }
            if($this->params()->fromPost('doAction') == 'enable'){
                $updateTestimonialData['active'] = 1;
            }
            if($this->params()->fromPost('doAction') == 'delete'){
                $updateTestimonialData['deleted'] = time();
            }
            foreach($this->params()->fromPost('ids') as $countryId){
                $this->AdminModel->addUpdateTestimonial($updateTestimonialData, $countryId);
            }
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy');
        $filterData['activeStatus'] =  $this->params()->fromQuery('activeStatus');
        $countries = $this->AdminModel->getTestimonial( $filterData );
        $data = !empty($countries)?$countries:array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator'    => $paginator],
            ['filterData'   => $filterData],
            ['flashMessages' =>  $this->flashMessenger()->getMessages()],
            ['Menu_select'  => 'masterdata'],
            ['Sub_menu_select' => 'masterdata/testimonial'],
            ['params'       => $this->params()]
        ));
    }

    public function testimonialmanagerAction()
    {
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add');
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';

        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
            $resp = $this->AdminModel->checkAdminPrivilage($priv);
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_testimonial');
            }
            if($action == 'add' || $action == 'edit')
            {
                if ($_FILES['testimonial_Image']['error'] == 0) {
                    $image_path = "testimonials";
                    $field_name = 'testimonial_Image';
                    $file_name_prefix = 'TESTIMONIAL_' . time();
                    $configArray = array(
                        array(
                            'image_x' => 150,
                            'sub_dir_name' => 'medium'
                        ),
                        array(
                            'image_x' => 100,
                            'sub_dir_name' => 'small'
                        )
                    );
                    $imgResult = $this->uploadTestimonialImg($field_name, $file_name_prefix, $image_path, $configArray);
                    if ($imgResult['result'] == 'success')
                    {
                        $testimonial_Image = $imgResult['fileName'];
                    }
                }
                else {
                    $testimonial_Image = $this->params()->fromPost('testimonial_Image_txt');
                }
                $updateTestimonialData = array(
                    'testimonial_Name'  =>  $this->params()->fromPost('testimonial_Name'),
                    'testimonial_Image'  =>  $testimonial_Image,
                    'testimonial_Description'  =>  $this->params()->fromPost('testimonial_Description'),
                    'testimonial_Position'  =>  $this->params()->fromPost('testimonial_Position')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminModel->addUpdateTestimonial($updateTestimonialData, $rowId);
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Testimonial info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to add testimonial info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin_testimonial');
            }
        }
        $countryData = false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $countryData = $this->AdminModel->getTestimonialByID($id);
            if(empty($countryData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid testimonial selected!"));
                return $this->redirect()->toRoute('admin_testimonial');
            }
        }

        $viewArray = array(
            'adminRow' => $countryData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );

        $viewArray['Menu_select'] = 'masterdata';
        $viewArray['Sub_menu_select'] = 'masterdata/testimonial';

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function uploadTestimonialImg($field_name, $file_name, $path, $configArray)
    {
        $this->authenticateModule();
        if (isset($_FILES[$field_name])) {
            $filename = $_FILES[$field_name]['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $file_name = $file_name;
            $handle = new \upload($_FILES[$field_name]);
            if ($handle->uploaded) {
                $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/');
                $handle->file_new_name_body = $file_name;
                $handle->image_resize = true;
                if ($configArray && !empty($configArray)) {
                    foreach ($configArray as $config) {
                        $handle->file_new_name_body = $file_name;
                        $handle->image_resize = true;
                        $handle->image_x = $config['image_x'];
                        $handle->image_ratio_y = true;
                        $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/' . $config['sub_dir_name'] . '/');

                    }
                    if ($handle->processed) {
                        $handle->clean();
                        $retArr = array('result' => 'success', 'fileName' => $file_name . '.' . $ext);
                    } else {
                        $retArr = array('result' => 'failed', 'error' => $handle->error);
                    }
                }
            } else {
                $retArr = array('result' => 'failed', 'error' => $handle->error);
            }
            echo 'Test Process';
            return $retArr;
        }
    }

    public function helpAction()
    {
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_help');
            }
            if($this->params()->fromPost('doAction') == 'disable'){
                $updateHelpData['qa_active'] = 0;
            }
            if($this->params()->fromPost('doAction') == 'enable'){
                $updateHelpData['qa_active'] = 1;
            }
            if($this->params()->fromPost('doAction') == 'delete'){
                $updateHelpData['qa_active'] = 0;
            }
            foreach($this->params()->fromPost('ids') as $countryId){
                $this->AdminModel->addUpdateHelp($updateHelpData, $countryId);
            }
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy');
        $filterData['activeStatus'] =  $this->params()->fromQuery('activeStatus');
        $countries = $this->AdminModel->getHelp($filterData);
        $data = !empty($countries)?$countries:array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator'    => $paginator],
            ['filterData'   => $filterData],
            ['flashMessages' =>  $this->flashMessenger()->getMessages()],
            ['Menu_select'  => 'masterdata'],
            ['Sub_menu_select' => 'masterdata/help'],
            ['params'       => $this->params()]
        ));
    }

    public function helpmanagerAction()
    {
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add');
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';

        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
            $resp = $this->AdminModel->checkAdminPrivilage($priv);
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_help');
            }
            if($action == 'add' || $action == 'edit')
            {
                $updateHelpData = array(
                    'qa_question'  =>  $this->params()->fromPost('qa_question'),
                    'qa_answer'  =>  $this->params()->fromPost('qa_answer')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminModel->addUpdateHelp($updateHelpData, $rowId);
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Help was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to add help, Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin_help');
            }
        }
        $countryData = false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $countryData = $this->AdminModel->getHelpByID($id);
            if(empty($countryData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid help selected!"));
                return $this->redirect()->toRoute('admin_help');
            }
        }

        $viewArray = array(
            'adminRow' => $countryData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );

        $viewArray['Menu_select'] = 'masterdata';
        $viewArray['Sub_menu_select'] = 'masterdata/help';

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }
}
?>