<?php namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager; 
use Admin\Model\AdminUserModel;
use Zend\Mvc\Plugin\FlashMessenger;
use Zend\Mvc\Plugin\Url;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;

class AdminAjaxController extends AbstractActionController
{
     const ITEM_PER_PAGE = 20;
     public function __construct($data = false){
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->adminModules = $data['configs']['adminModules'];
        $this->adminMenus = $data['configs']['adminMenus'];
        $this->timeZones = $data['configs']['timezones'];
    }
    
    function indexAction(){
        echo 'invalid redirect';exit;
    }
    
    function getUsersAction(){
        $filterData['searchBy'] = 'email_address';
        $filterData['searchKey'] = $this->params()->fromPost('query');
        $uId=false;
        if($this->params()->fromQuery('uId')){
            $uId=$this->params()->fromQuery('uId');
        }
        $userAccs = $this->AdminUserModel->userAccouts($filterData,$uId);
        $retArr = array();
        if($userAccs){
            foreach($userAccs as $acc){
                $retArr[] = array(
                    'id' => $acc['ID'],
                    'name' => $acc['email_address']
                );
            }
        }
        echo json_encode($retArr);exit;
    }
    
    function downloadFileAction($fileLocation){
        $filename = $this->params('filelocation');       
        header('Pragma: public');
        header('Expires: 0');
        header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
        header('Cache-Control: private', false);  
        /*header('Content-Type: application/pdf');*/
        header('Content-Disposition: attachment; filename="'. basename($filename) . '";');
        header('Content-Transfer-Encoding: binary');
        header('Content-Length: ' . filesize($filename));
        readfile($filename);
        exit;
    }
    
    public function downloadAction() {  
        $file = ACTUAL_ROOTPATH.'uploads/'.$this->params('folder').'/'.$this->params('file');
        $response = new \Zend\Http\Response\Stream();
        $response->setStream(fopen($file, 'r'));
        $response->setStatusCode(200);
        $response->setStreamName(basename($file));
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Disposition' => 'attachment; filename="' . basename($file) .'"',
            'Content-Type' => 'application/octet-stream',
            'Content-Length' => filesize($file),
            'Expires' => '@0', 
            'Cache-Control' => 'must-revalidate',
            'Pragma' => 'public'
        ));
        $response->setHeaders($headers);
        return $response;exit;
    }
    
    function checkCountryPriceAddedAction(){
        $response = array('result'=>'failed', 'resultArray'=>false);
        if($this->params()->fromPost('country_from') != '' && $this->params()->fromPost('country_to') != ''){
            $whereCondition['where'] = array(
                'country_from'  =>  $this->params()->fromPost('country_from'),
                'country_to'    =>  $this->params()->fromPost('country_to'),
                'service_type'  =>  $this->params()->fromPost('service_type'),
            );
            if($this->params()->fromPost('edit_id') != ''){
                $whereCondition['where']['price_setting_country_id !=']=(int)$this->params()->fromPost('edit_id');    
            }
            $result = $this->AdminModel->getCountryPriceSettings($filterTable);
            if($result){
                $response = array('result'=>'exist', 'resultArray'=>$result);
            }else{
                $response = array('result'=>'not-exist', 'resultArray'=>false);
            }
        }
        echo json_encode($response);exit;
    }
    
    function checkDistancePriceAddedAction(){
        $response = array('result'=>'failed', 'resultArray'=>false);
        if($this->params()->fromPost('distance_from') != '' && $this->params()->fromPost('distance_to') != ''){
            $whereCondition = array(
                "distance_from"  =>  $this->params()->fromPost('distance_from'),
                "distance_to"  =>  $this->params()->fromPost('distance_to'),
                "service_type"      =>  $this->params()->fromPost('service_type')
            );
            if($this->params()->fromPost('edit_id') != ''){
                $whereCondition['price_settings_distance_id != ?'] = (int)$this->params()->fromPost('edit_id');
            }
            $filterTable = array('where'=>$whereCondition);
            $result = $this->AdminModel->getDistancePriceSettings($filterTable);
            if($result){
                $response = array('result'=>'exist', 'resultArray'=>$result);
            }else{
                $response = array('result'=>'not-exist', 'resultArray'=>false);
            }
        }
        echo json_encode($response);exit;
    }
    
    function checkWeightCarriedPriceAddedAction(){
        $response = array('result'=>'failed', 'resultArray'=>false);
        if($this->params()->fromPost('weight_carried_from') != '' && $this->params()->fromPost('weight_carried_to') != ''){
            $whereCondition = array(
                "weight_carried_from"  =>  $this->params()->fromPost('weight_carried_from'),
                "weight_carried_to"  =>  $this->params()->fromPost('weight_carried_to'),
                "service_type"      =>  $this->params()->fromPost('service_type')
            );
            if($this->params()->fromPost('edit_id') != ''){
                $whereCondition['price_settings_weight_id != ?'] = (int)$this->params()->fromPost('edit_id');
            }
            $filterTable = array('where'=>$whereCondition);
            $result = $this->AdminModel->getWeightCarriedPriceSettings($filterTable);
            if($result){
                $response = array('result'=>'exist', 'resultArray'=>$result);
            }else{
                $response = array('result'=>'not-exist', 'resultArray'=>false);
            }
        }
        echo json_encode($response);exit;
    }
    
    function checkItemCategoryPriceAddedAction(){
        $response = array('result'=>'failed', 'resultArray'=>false);
        if($this->params()->fromPost('item_category_id') != ''){
            $whereCondition = array(
                "item_category_id"  =>  $this->params()->fromPost('item_category_id'),
                "item_sub_category_id"  =>  $this->params()->fromPost('item_sub_category_id'),
                "service_type"      =>  $this->params()->fromPost('service_type')
            );
            if($this->params()->fromPost('edit_id') != ''){
                $whereCondition['price_settings_item_category_id != ?'] = (int)$this->params()->fromPost('edit_id');
            }
            $filterTable = array('where'=>$whereCondition);
            $result = $this->AdminModel->getItemCategoryPriceSettings($filterTable);
            if($result){
                $response = array('result'=>'exist', 'resultArray'=>$result);
            }else{
                $response = array('result'=>'not-exist', 'resultArray'=>false);
            }
        }
        echo json_encode($response);exit;
    }
    
    function checkItemTaskCategoryPriceAddedAction(){
        $response = array('result'=>'failed', 'resultArray'=>false);
        if($this->params()->fromPost('item_task_category_id') != ''){
            $whereCondition = array(
                "item_task_category_id"  =>  $this->params()->fromPost('item_task_category_id'),
                "item_task_sub_category_id"  =>  $this->params()->fromPost('item_task_sub_category_id'),
                "service_type"      =>  $this->params()->fromPost('service_type')
            );
            if($this->params()->fromPost('edit_id') != ''){
                $whereCondition['price_settings_item_task_category_id != ?'] = (int)$this->params()->fromPost('edit_id');
            }
            $filterTable = array('where'=>$whereCondition);
            $result = $this->AdminModel->getItemTaskCategoryPriceSettings($filterTable);
            if($result){
                $response = array('result'=>'exist', 'resultArray'=>$result);
            }else{
                $response = array('result'=>'not-exist', 'resultArray'=>false);
            }
        }
        echo json_encode($response);exit;
    }
    
    public function downloadCountryPriceSettingsAction() {
        $service_type = $this->params()->fromQuery('service_type');
        $whereSql = array( 'service_type'  =>  $service_type );
        $filterData = array( 'where' => $whereSql );
        $recordsArr = $this->AdminModel->getCountriesSettingsExport($filterData);
        $priceArray = array();
        $f = fopen('php://memory', 'w+'); 
        $headers = array_keys($recordsArr);
        array_unshift($headers , 'From/To');
        fputcsv($f, $headers, ',');
        if($recordsArr){
            foreach($recordsArr as $key=>$eachRec){
                $colArr = array();
                foreach ($eachRec as $eachCol){
                    $colArr[] = $eachCol;
                }
                array_unshift($colArr , $key);
                fputcsv($f, $colArr, ',');
            }
        }
        array_unshift($priceArray , $headers);
        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$service_type.'_country_price_settings_'.date('YmdHis').'.csv";');
        fpassthru($f);exit;
    }
    
    public function downloadDistancePriceSettingsAction() {
        $service_type = $this->params()->fromQuery('service_type');
        $whereSql = array( 'service_type'  =>  $service_type );
        $filterData = array( 'where' => $whereSql );
        $recordsArr = $this->AdminModel->getDistancePriceSettings($filterData);
        $headers = array('From (KM)', 'To (KM)', 'Value');
        $priceArray = array();
        $f = fopen('php://memory', 'w+'); 
        fputcsv($f, $headers, ',');
        if($recordsArr){
            foreach($recordsArr as $eachRec){
                $colArr = array(
                    $eachRec['distance_from'],
                    $eachRec['distance_to'],
                    $eachRec['distance_price']
                );
                fputcsv($f, $colArr, ',');
            }
        }
        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$service_type.'_distance_price_settings_'.date('YmdHis').'.csv";');
        fpassthru($f);exit;
    }
    
    public function downloadPriceItemPriceSettingsAction() {
        $service_type = $this->params()->fromQuery('service_type');
        $whereSql = array( 'service_type'  =>  $service_type );
        $filterData = array( 'where' => $whereSql );
        $recordsArr = $this->AdminModel->getPriceItemPriceSettings($filterData);
        $headers = array('From (KM)', 'To (KM)', 'Value');
        $priceArray = array();
        $f = fopen('php://memory', 'w+'); 
        fputcsv($f, $headers, ',');
        if($recordsArr){
            foreach($recordsArr as $eachRec){
                $colArr = array(
                    $eachRec['price_item_from'],
                    $eachRec['price_item_to'],
                    $eachRec['price_item_price']
                );
                fputcsv($f, $colArr, ',');
            }
        }
        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$service_type.'_price_item_price_settings_'.date('YmdHis').'.csv";');
        fpassthru($f);exit;
    }
    
    public function downloadWeightCarriedPriceSettingsAction() {
        $service_type = $this->params()->fromQuery('service_type');
        $whereSql = array( 'service_type'  =>  $service_type );
        $filterData = array( 'where' => $whereSql );
        $recordsArr = $this->AdminModel->getWeightCarriedPriceSettings($filterData);
        $headers = array('From (KM)', 'To (KM)', 'Value');
        $priceArray = array();
        $f = fopen('php://memory', 'w+'); 
        fputcsv($f, $headers, ',');
        if($recordsArr){
            foreach($recordsArr as $eachRec){
                $colArr = array(
                    $eachRec['weight_carried_from'],
                    $eachRec['weight_carried_to'],
                    $eachRec['weight_carried_price']
                );
                fputcsv($f, $colArr, ',');
            }
        }
        fseek($f, 0);
        header('Content-Type: application/csv');
        header('Content-Disposition: attachment; filename="'.$service_type.'_weight_carried_price_settings_'.date('YmdHis').'.csv";');
        fpassthru($f);exit;
    }
    
    public function getprojectsubcategoryAction(){
        $id = $_REQUEST['id'];
        $tasks_categories = $this->AdminModel->getProjectTasksCategorylisttype($id);		
        if(count($tasks_categories)>0){
            $retString = '<option value="">-- Select --</option>';
            foreach($tasks_categories as $tasks_category){
               $retString .= '<option value="'.$tasks_category['ID'].'">'.$tasks_category['subcatname'].'</option>';
            }
        }
        echo $retString;
	exit;
    }
    
    public function getpackagesubcategoryAction(){
        $id = $_REQUEST['id'];
        $tasks_categories = $this->AdminModel->getPackageTasksCategorylisttype($id);		
        if(count($tasks_categories)>0){
            $retString = '<option value="">-- Select --</option>';
            foreach($tasks_categories as $tasks_category){
               $retString .= '<option value="'.$tasks_category['ID'].'">'.$tasks_category['subcatname'].'</option>';
            }
        }
        echo $retString;
	exit;
    }
    
    public function getprojectproductsubcategoryAction(){
        $id = $_REQUEST['id'];
        $tasks_categories = $this->AdminModel->getProjectProductCategorylisttype($id);		
        if(count($tasks_categories)>0){
            $retString = '<option value="">-- Select --</option>';
            foreach($tasks_categories as $tasks_category){
               $retString .= '<option value="'.$tasks_category['ID'].'">'.$tasks_category['subcatname'].'</option>';
            }
        }
        echo $retString;
	exit;
    }
    
    public function getPayoutMethodSettingsAction(){
        $countryCode = $this->AdminModel->cleanQuery($this->params()->fromPost('country_code',false));
        if($countryCode && $countryCode == ''){
            $resMessage = array('result'=>'failure', 'message'=>'Please select country');
        }
        else{
            $filterPayoutSetting = array('where'=>array('country_code'=>$countryCode));
            $payoutSettingsData = $this->AdminPaymentModel->getPayoutMethodSettingsByCountry($filterPayoutSetting);
            if(!empty($payoutSettingsData)){
                $fromEncoding = 'ISO-8859-1'; 
                /*array_walk_recursive($payoutSettingsData, function (&$value, $key, $fromEncoding) {
                    if (is_string($value)) {
                        $value = iconv($fromEncoding, 'UTF-8', $value);
                    }
                }, $fromEncoding);*/
                foreach($payoutSettingsData as $keyIndex => $eachSett){
                    foreach($eachSett as $key => $value){
                        if (is_string($value)) {
                            $payoutSettingsData[$keyIndex][$key] = iconv($fromEncoding, 'UTF-8', $value);
                        }
                        $fieldsFilter = array('where'=>array('payout_method_setting_id'=>$eachSett['payout_method_setting_id']));
                        $payoutSettingsData[$keyIndex]['fields'] = $this->AdminPaymentModel->getPayoutMethodFields($fieldsFilter);
                    }
                }
                $resMessage = array('result'=>'success', 'payoutSettingsData'=>$payoutSettingsData);
            }
            else{
                $resMessage = array('result'=>'failure', 'message'=>'No payout methods found');
            }
        }
        echo json_encode((object)$resMessage);exit;
    }
    
    public function getUserPayoutMethodsListAction(){
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Type' => 'application/octet-stream',
        ));
        $userId = $this->AdminModel->cleanQuery($this->params()->fromPost('user_id',false));
        if($userId){
            $filterPayoutMethod = array('where'=>array('user_id'=>$userId));
            $userPayoutMethodsData = $this->AdminPaymentModel->getUserPayoutMethods($filterPayoutMethod);
            $resMessage = array('result'=>'success', 'payoutMethodData'=>$userPayoutMethodsData);
        }
        else{
            $resMessage = array('result'=>'failure', 'message'=>'user id missing!');
        }
        echo json_encode((object)$resMessage);exit;
    }
    
    public function getUserPayoutMethodAction(){
        $headers = new \Zend\Http\Headers();
        $headers->addHeaders(array(
            'Content-Type' => 'application/octet-stream',
        ));
        $payoutAddrId = $this->AdminModel->cleanQuery($this->params()->fromPost('user_payout_address_id',false));
        if($payoutAddrId && $payoutAddrId == ''){
            $resMessage = array('result'=>'failure', 'message'=>'User payout address id is required');
        }
        else{
            $filterPayoutMethod = array('where'=>array('user_payout_address_id'=>$payoutAddrId));
            $userPayoutMethodsData = $this->AdminPaymentModel->getUserPayoutMethods($filterPayoutMethod,'row');
            if(!empty($userPayoutMethodsData)){
                $filterPayoutMethodFileds = array('where'=>array('user_payout_address_id'=>$payoutAddrId));
                $userPayoutMethodsData['fields'] = $this->AdminPaymentModel->getUserPayoutMethodFields($filterPayoutMethodFileds);
                $fromEncoding = 'ISO-8859-1'; 
                array_walk_recursive($userPayoutMethodsData, function (&$value, $key, $fromEncoding) {
                    if (is_string($value)) {
                        $value = iconv($fromEncoding, 'UTF-8', $value);
                    }
                }, $fromEncoding);  
                $resMessage = array('result'=>'success', 'payoutMethodData'=>$userPayoutMethodsData);
            }
            else{
                $resMessage = array('result'=>'failure', 'message'=>'No payout methods found');
            }
        }
        echo json_encode((object)$resMessage);exit;
    }
}
?>