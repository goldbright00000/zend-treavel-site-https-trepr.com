<?php
namespace Admin\Controller;
use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager; 
use Admin\Model\AdminPaymentModel;
use Zend\Mvc\Plugin\FlashMessenger;
use Zend\Mvc\Plugin\Url;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;
class AdminPaymentController extends AbstractActionController
{
    var $AdminCommonPlugin;
     const ITEM_PER_PAGE = 20;
     const ADDR_ITEM_PER_PAGE = 1;
     public function __construct($data = false){
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->generalvar = $data['configs']['siteConfigs'];
        $this->adminModules = $data['configs']['adminModules'];
        $this->adminMenus = $data['configs']['adminMenus'];
        $this->timeZones = $data['configs']['timezones'];
       
       
    }
    
    public function indexAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $data = array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = $this->AdminPaymentModel->getUserPaymentMethods($filterData,'paginator');
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
            ['paginator' => $paginator],
            ['filterData'=>$filterData],
            ['flashMessages' =>  $this->flashMessenger()->getMessages()],
            ['Menu_select'=>'account_settings'],
            ['Sub_menu_select' => 'payments']
        ));   
    }     
   
    public function paymentHistoryAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
        $filterData['status'] =  $this->params()->fromQuery('status'); 
        $filterData['usertype'] =  'seeker';
        $data = array();
        $arrTrips = $this->AdminPaymentModel->getUserPayments( $filterData);
      
        $data = !empty($arrTrips)?$arrTrips:array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'account_settings'],
                ['Sub_menu_select' => 'payments/histrory']
            ));  
    }
	public function payoutPendingAction(){
		$this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
		if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));              
            }
           	$this->AdminSeekerModel->travellerMarkPaymentPaid($this->params()->fromPost('ids'));
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = 'completed';
        $filterData['usertype'] = 'traveller';
        $data = array();

        $trips = $this->AdminSeekerModel->getTravellerPendingPaymentTrips($filterData);

        $arrTrips = array();
        if ($trips) {
            foreach ($trips as $trip)
            {
                $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
                $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);

                //-- origin
                $origin_location_name = isset($origin_location['name'])?$origin_location['name']:'';
                $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';

                //-- destination
                $destination_location_name = isset($destination_location['name'])?$destination_location['name']:'';
                $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';

                //-- To get trip type
                $trip_people = $this->AdminSeekerModel->getTravellerPeople($trip['ID']);
                $trip_package = $this->AdminSeekerModel->getTravellerPackage($trip['ID']);
                $trip_project = $this->AdminSeekerModel->getTravellerProject($trip['ID']);

                $trip_people = isset($trip_people) ? $trip_people : false;
                $trip_package = isset($trip_package) ? $trip_package : false;
                $trip_project = isset($trip_project) ? $trip_project : false;

                $service = '';
                if ($trip_people['ID']) {
                    $service = 'People';
                }

                if ($trip_package['ID']) {
                    $service = 'Package';
                }

                if ($trip_project['ID']) {
                    $service = 'Product';
                }
				
				$seekerTrips = $this->AdminSeekerModel->getSeekerTripByTravellerTripId($trip['ID'],'completed');
				
				$totalCost = 0;
				$toalCostCurrency = '';
				foreach($seekerTrips as $strip){
					$totalCost += $strip['total_cost'];
					$toalCostCurrency = $strip['total_cost_currency'];
				}
                $arrTrips[] = array
                (
                    'trip_id_number' => $trip['trip_id_number'],
                    'ID' => $trip['ID'],
                    'name' => $trip['name'],
                    'trip_status' => $trip['trip_status'],
					'pending_payment' => $totalCost,
					'pending_payment_currency' => $toalCostCurrency,
                    'email_address' => $trip['email_address'],
                    'origin_location' => $trip['origin_location'],
                    'destination_location' => $trip['destination_location'],
                    'origin_location_name'      => $origin_location_name,
                    'destination_location_name' => $destination_location_name,
                    'origin_location_code' => $origin_location_code,
                    'destination_location_code' => $destination_location_code,
                    'origin_location_city' => $origin_location_city,
                    'destination_location_city' => $destination_location_city,
                    'origin_location_country' => $origin_location_country,
                    'destination_location_country' => $destination_location_country,
                    'departure_date' => $trip['departure_date'],
                    'departure_time' => $trip['departure_time'],
                    'arrival_date' => $trip['arrival_date'],
                    'travel_plan_reservation_type' => $trip['travel_plan_reservation_type'],
                    'service_type' => $service,
                    'arrival_time' => $trip['arrival_time'],
                    'active' => $trip['active'],
                    'modified' => $trip['modified']
                );
            }
        }

        $data = !empty($arrTrips) ? $arrTrips : array();
//echo '<pre>';print_r($data);die;
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'payments'],
            ['Sub_menu_select' => 'payments/pending']
        ));
	}
    public function paymentHistoryMangerAction() {
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
        $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
        $filterData['status'] =  $this->params()->fromQuery('status'); 
        $filterData['usertype'] =  'seeker';
        $filterData['id'] =  $this->params()->fromQuery('id',0);
        $filterData['action'] =  $this->params()->fromQuery('action','view');
         
        $paymentHistoryRow = $this->AdminPaymentModel->getUserPayments( $filterData, 'row');
        $page = $this->params()->fromQuery('page', 1);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paymentHistoryRow' => $paymentHistoryRow],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'account_settings'],
                ['Sub_menu_select' => 'payments/histrory']
            ));  
    }
    
    public function methodsAction() {
        $this->authenticateModule();
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));              
            }
            foreach($this->params()->fromPost('ids') as $payMethodId){
                $this->AdminPaymentModel->deletePaymentCardDetails($payMethodId);
            }
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy');
        $page = $this->params()->fromQuery('page', 1);
        $paginator = $this->AdminPaymentModel->getUserPaymentMethods($filterData,'paginator');
        
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'account_settings'],
                ['Sub_menu_select' => 'payments/methods']
            ));  
    }
    
    public function methodsManagerAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        
        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
            $resp = $this->AdminModel->checkAdminPrivilage($priv);               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));
            }
            if($action == 'add' || $action == 'edit'){
                $updatePaymentMethodData = array(
                    'user' => $this->params()->fromPost('user_id'),
                    'holder_name'  =>  $this->params()->fromPost('holder_name'),
                    'card_no' => $this->AdminCommonPlugin->encrypt($this->params()->fromPost('card_no')),
                    'card_type' => $this->params()->fromPost('card_type'),
                    'payment_card_type' => $this->params()->fromPost('payment_card_type'),
                    'issue_date'    => $this->params()->fromPost('issue_date'),
                    'issue_number'    => $this->params()->fromPost('issue_number'),
                    'card_valid' => $this->params()->fromPost('card_valid'),
                    'card_cvv' => $this->params()->fromPost('card_cvv'),
                    'modified' => date('Y-m-d H:i:s')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminPaymentModel->addUpdateUserPaymentMethodRow($updatePaymentMethodData,$rowId);
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Payment method info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to ".$action." payment method info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));
            }
        }
        $paymentMethodData = false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $paymentMethodData = $this->AdminPaymentModel->getUserPaymentMethod($id);
            if(empty($paymentMethodData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid peyment method!"));
                return $this->redirect()->toRoute('admin-payments', array(
                    'controller' => 'admin',
                    'action' =>  'methods'
                ));
            }
        }
        
        $viewArray = array(
            'paymentRow' => $paymentMethodData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );
        if($action == 'edit'){
            $uAcc = $this->AdminUserModel->userAccouts(array(),$viewArray['paymentRow']['user_id']);
            if($uAcc){
                foreach($uAcc as $acc){
                    $retArr[] = array(
                        'id' => $acc['ID'],
                        'name' => $acc['email_address']
                    );
                }
                $viewArray['userPrePop'] = json_encode($retArr);
            }
        }
        
        $viewArray['Menu_select'] = 'account_settings';
        $viewArray['Sub_menu_select'] = 'payments/methods';
       
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    }
    
    public function PayoutMethodsAction() {
        $this->authenticateModule();
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'payout-methods'
                    ));              
            }
            foreach($this->params()->fromPost('ids') as $payMethodId){
                if($payMethodId && $payMethodId != false && $payMethodId != ''){
                    $this->AdminPaymentModel->deleteUserPayoutMethod($payMethodId);
                    $this->AdminPaymentModel->deleteUserPayoutMethodFields($payMethodId);
                    $this->flashMessenger()->addMessage(array('alert-success' => 'Payout method(s) has been deleted successfully.'));
                }
                else{
                    $this->flashMessenger()->addMessage(array('alert-danger' => 'Unable to delete payout method(s). Please try later.'));
                }
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'payout-methods'
                    ));
            }
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy');
        $page = $this->params()->fromQuery('page', 1);
        $paginator = $this->AdminPaymentModel->getUserPayoutMethods($filterData,'paginator');
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'account_settings'],
                ['Sub_menu_select' => 'payments/payout-methods']
            ));  
    }
    
    public function payoutMethodsManagerAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        
        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
            $resp = $this->AdminModel->checkAdminPrivilage($priv);               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'payout-methods'
                    ));
            }
            if($action == 'add' || $action == 'edit'){
                $payoutAddressData = array(
                    'user_id'   => $this->params()->fromPost('user_id'),
                    'payout_method_setting_id' => $this->params()->fromPost('payout_method_type'),
                    'user_payout_country'   => $this->params()->fromPost('payout_country'),
                    'user_payout_address_1'   => $this->params()->fromPost('payout_address1'),
                    'user_payout_address_2'   => $this->params()->fromPost('payout_address2'),
                    'user_payout_city'   => $this->params()->fromPost('payout_city'),
                    'user_payout_state'   => $this->params()->fromPost('payout_state'),
                    'user_payout_zip_code'   => $this->params()->fromPost('payout_zip'),
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $uPayMethodaddrId = $this->AdminPaymentModel->addEditUserPayoutMethod($payoutAddressData,$rowId);
                $payoutFields = $this->params()->fromPost('payout_fields');
                $payoutFieldArray = isset($payoutFields[$this->params()->fromPost('payout_method_type')])?$payoutFields[$this->params()->fromPost('payout_method_type')]:array();
                if($action == 'edit' && $id != false && !empty($id)) {
                    $this->AdminPaymentModel->deleteUserPayoutMethodFields($uPayMethodaddrId);
                }
                $resultSuccess = false;
                if(!empty($payoutFieldArray)){
                    foreach ($payoutFieldArray as $key=>$payoutFields){
                        $payoutFieldData = array(
                            'user_payout_address_id'    =>  $uPayMethodaddrId,
                            'user_id'  =>  $this->params()->fromPost('user_id'),
                            'payout_method_field_id'  =>  $key,
                            'payout_method_field_value'  =>  $payoutFields,
                            'payout_method_modified'  =>  time()
                        );
                        $resultSuccess = $this->AdminPaymentModel->addUserPayoutMethodFields($payoutFieldData);
                    }
                }
                if($resultSuccess){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Payout method info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to ".$action." payout method info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'payout-methods'
                    ));
            }
        }
        $payoutMethodData = false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $payoutMethodData = $this->AdminPaymentModel->getUserPayoutMethod($id);
            if(empty($payoutMethodData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid payout method!"));
                return $this->redirect()->toRoute('admin-payments', array(
                    'controller' => 'admin',
                    'action' =>  'payout-methods'
                ));
            }
        }
        
        $viewArray = array(
            'payoutRow' => $payoutMethodData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones,
            'countries' => $this->AdminModel->getCountries()   
        );
        if($action == 'edit'){
            $uAcc = $this->AdminUserModel->userAccouts(array(),$viewArray['payoutRow']['user_id']);
            if($uAcc){
                foreach($uAcc as $acc){
                    $retArr[] = array(
                        'id' => $acc['ID'],
                        'name' => $acc['email_address']
                    );
                }
                $viewArray['userPrePop'] = json_encode($retArr);
            }
        }
        
        $viewArray['Menu_select'] = 'payments';
        $viewArray['Sub_menu_select'] = 'payments/payout-methods';
       
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    }
    
    public function transactionHistoryAction() {
        $this->authenticateModule();
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy');
        $page = $this->params()->fromQuery('page', 1);
        $paginator = $this->AdminPaymentModel->getUserTransactions($filterData,'paginator');
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'account_settings'],
                ['Sub_menu_select' => 'payments/transaction-history']
            ));  
    }
    
    function acceptPaymentsAction(){
        extract($_POST);
        $filterData['id'] = $id;
        $paymentRow = $this->AdminPaymentModel->getUserPayments( $filterData, 'row');
        $param['pay_id'] = $paymentRow['pay_id'];
        $param['pay_trans_id'] = $paymentRow['pay_trans_id'];
        $param['pay_user'] = $paymentRow['pay_user'];
        $param['pay_amount'] = $paymentRow['pay_amount'];
        if($action == 'approve'){
            $response =  $this->doCapturePayment($param);
            if(isset($response['ACK']) && $response['ACK']=='Success'){ 
                $msg_status = 'success';
                $msg = ' Payment has been accepted.';
            }else{
                $msg_status = 'failed';
                $msg = ' Payment not accepted.<br/> '.$response["L_LONGMESSAGE0"];  
            }
        }else{
            if(!empty($paymentRow)){
                if($paymentRow['pay_status'] == 0){
                    if($refundType == 'Full'){
                        $pay_status = 3;
                         $param['pay_status'] = $pay_status;
                         $response =  $this->doVoidPayment($param);
                          if(isset($response['ACK']) && $response['ACK']=='Success'){ 
                                  $msg_status = 'success'; 
                                 $msg = ' Payment has been returned. ';
                            }else{
                                $msg_status = 'failed';
                                $msg = ' Payment not falied.<br/> '.$response["L_LONGMESSAGE0"];  
                            }
                    }else{
                        $msg_status = 'failed'; 
                        $msg = ' You cannot return partial amount while payment on hold. Please accept the payment and try. ';
                    }
                }else{
                    $txnRow = $this->AdminPaymentModel->getTransactionRow($paymentRow['pay_id'],$paymentRow['pay_user']);
                   
                    if(!empty($txnRow)){
                        $param['refundType'] = $refundType;
                        $travellerEarnings = $paymentRow['pay_amount'];
                        $pay_status = 3;
                        if($refundType == 'Partial'){
                            $adminProfit = round((20 / 100) * $paymentRow['pay_amount']);
                            $travellerEarnings = $paymentRow['pay_amount'] - $adminProfit;
                            $pay_status = 2;
                        }
                            $param['pay_trans_id'] = $txnRow['txn_payal_trans_id'];
                            $param['refundType'] = $refundType;
                            $param['pay_amount'] = $travellerEarnings;
                            $param['pay_status'] = $pay_status;
                            $response =  $this->refundPayment($param);
                            if(isset($response['ACK']) && $response['ACK']=='Success'){ 
                                $msg_status = 'success';
                                $msg = ' Payment has been accepted.';
                            }else{
                                $msg_status = 'failed';
                                $msg = ' Payment not accepted.<br/> '.$response["L_LONGMESSAGE0"];  
                            }
                    }
                }
            }else{
               $msg_status = 'failed'; 
               $msg = ' Invalid payment.';
            }
            
        }
         echo json_encode(array('msg_status' => $msg_status, 'msg' => $msg)); exit; 
    }
    
    
    
    function doCapturePayment($param = false){
        $paypal_api_username = $this->generalvar['paypal_api_username'];
        $paypal_api_password = $this->generalvar['paypal_api_password'];
        $paypal_api_signature = $this->generalvar['paypal_api_signature'];
        $API_Endpoint = 'https://api-3t.sandbox.paypal.com/nvp';
        $version = '65.2';
        $API_UserName = $paypal_api_username;
        $API_Password = $paypal_api_password;
        $API_Signature =$paypal_api_signature;
        $subject = '';
        global $nvp_Header, $AUTH_token, $AUTH_signature, $AUTH_timestamp;
        $nvpHeaderStr = "";
        $methodName = 'DOCapture';
        $authorizationID=urlencode($param['pay_trans_id']);
        $completeCodeType=urlencode('Complete');
        $amount=urlencode($param['pay_amount']);
        $invoiceID=urlencode(rand(0,9999));
        $currency=urlencode('USD');
        $note=urlencode('Trepr Payments');
        $nvpHeaderStr = "&PWD=".urlencode($API_Password)."&USER=".urlencode($API_UserName)."&SIGNATURE=".urlencode($API_Signature);
        $nvpstr="&AUTHORIZATIONID=$authorizationID&COMPLETETYPE=$completeCodeType&AMT=$amount&CURRENCYCODE=$currency&NOTE=$note";
        $nvpstr = $nvpHeaderStr.$nvpstr;

        if(strlen(str_replace('VERSION=','', strtoupper($nvpstr))) == strlen($nvpstr)){
            $nvpstr = "&VERSION=" . urlencode($version) . $nvpstr;
        }
        $nvpreq="METHOD=".urlencode($methodName).$nvpstr;
 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$nvpreq);

        $response = curl_exec($ch);
        $nvpResArray=$this->deformatNVP($response);
        $nvpReqArray=$this->deformatNVP($nvpreq);
 
        if (curl_errno($ch)){} else {
            curl_close($ch);
        }
        $nvpResArray['pay_id'] = 0;
        if($nvpResArray['ACK'] == 'Success'){
            $paymentArray['txn_user'] = $param['pay_user'];
            $paymentArray['txn_pay_id'] = $param['pay_id'];
            $paymentArray['txn_payal_trans_id'] = $nvpResArray['TRANSACTIONID'];
            $paymentArray['txn_receipt_id'] = $nvpResArray['RECEIPTID'];
            $paymentArray['txn_paypal_data'] = serialize($nvpResArray);
            $paymentArray['txn_status'] = $nvpResArray['PAYMENTSTATUS'];
            $paymentArray['txn_amount'] = $nvpResArray['AMT'];
            $paymentArray['txn_curreny'] = $nvpResArray['CURRENCYCODE'];
            $paymentArray['tx_date'] = $nvpResArray['TIMESTAMP'];
            $paymentArray['txn_added'] = time();
            $paymentArray['txn_type'] = 'Debit';
            $updateData['pay_status'] = 1;
            $pay_id =  $this->AdminPaymentModel->updatePaymants($updateData,$param['pay_id']); 
            $pay_id =  $this->AdminPaymentModel->updateTransactions($paymentArray); 
            $nvpResArray['pay_id'] = $pay_id;
        }
        return $nvpResArray;
    }

    function doVoidPayment($param = false){
        $paypal_api_username = $this->generalvar['paypal_api_username'];
        $paypal_api_password = $this->generalvar['paypal_api_password'];
        $paypal_api_signature = $this->generalvar['paypal_api_signature'];
        $API_Endpoint = 'https://api-3t.sandbox.paypal.com/nvp';
        $version = '65.2';
        $API_UserName = $paypal_api_username;
        $API_Password = $paypal_api_password;
        $API_Signature =$paypal_api_signature;
        $subject = '';
        global $nvp_Header, $AUTH_token, $AUTH_signature, $AUTH_timestamp;
        $nvpHeaderStr = "";
        $methodName = 'DOVoid';
        $authorizationID=urlencode($param['pay_trans_id']);
        $note=urlencode('Trepr Refund');
        $nvpHeaderStr = "&PWD=".urlencode($API_Password)."&USER=".urlencode($API_UserName)."&SIGNATURE=".urlencode($API_Signature);
        $nvpstr="&AUTHORIZATIONID=$authorizationID&NOTE=$note"; 
        $nvpstr = $nvpHeaderStr.$nvpstr;

        if(strlen(str_replace('VERSION=','', strtoupper($nvpstr))) == strlen($nvpstr)){
            $nvpstr = "&VERSION=" . urlencode($version) . $nvpstr;
        }
        $nvpreq="METHOD=".urlencode($methodName).$nvpstr;
 
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$nvpreq);
        $response = curl_exec($ch);

        $nvpResArray=$this->deformatNVP($response);
        $nvpReqArray=$this->deformatNVP($nvpreq);
 
        if (curl_errno($ch)){} else {
            curl_close($ch);
        }
        $nvpResArray['pay_id'] = 0;
        if($nvpResArray['ACK'] == 'Success'){
            $paymentArray['txn_user'] = $param['pay_user'];
            $paymentArray['txn_pay_id'] = $param['pay_id'];
            $paymentArray['txn_payal_trans_id'] = $nvpResArray['AUTHORIZATIONID'];
            $paymentArray['txn_paypal_data'] = serialize($nvpResArray);
            $paymentArray['txn_status'] = 'Completed';
            $paymentArray['txn_amount'] = $param['pay_amount'];
            $paymentArray['txn_added'] = time();
            $paymentArray['txn_type'] = 'Credit';
            $updateData['pay_status'] = $param['pay_status'];
            $pay_id =  $this->AdminPaymentModel->updatePaymants($updateData,$param['pay_id']); 
            $pay_id =  $this->AdminPaymentModel->updateTransactions($paymentArray); 
            $nvpResArray['pay_id'] = $pay_id;
        }
        return $nvpResArray;
    }

    function refundPayment($param = false){
        $paypal_api_username = $this->generalvar['paypal_api_username'];
        $paypal_api_password = $this->generalvar['paypal_api_password'];
        $paypal_api_signature = $this->generalvar['paypal_api_signature'];
        $API_Endpoint = 'https://api-3t.sandbox.paypal.com/nvp';
        $version = '65.2';
        $API_UserName = $paypal_api_username;
        $API_Password = $paypal_api_password;
        $API_Signature =$paypal_api_signature;
        $subject = '';
        global $nvp_Header, $AUTH_token, $AUTH_signature, $AUTH_timestamp;
        $nvpHeaderStr = "";
        $methodName = 'RefundTransaction';
        $authorizationID=urlencode($param['pay_trans_id']);
        $completeCodeType=urlencode('Complete');
        $amount=urlencode($param['pay_amount']);
        $invoiceID=urlencode(rand(0,9999));
        $currency=urlencode('USD');
        $note=urlencode('Trepr Payments');
        $transaction_id=urlencode($param['pay_trans_id']);
        $refundType=urlencode($param['refundType']);
        $amount=urlencode($param['pay_amount']-10);
        $currency=urlencode('USD');
        $memo=urlencode('Trepr Payments');
        $nvpHeaderStr = "&PWD=".urlencode($API_Password)."&USER=".urlencode($API_UserName)."&SIGNATURE=".urlencode($API_Signature);

        $nvpstr="&TRANSACTIONID=$transaction_id&REFUNDTYPE=$refundType&CURRENCYCODE=$currency&NOTE=$memo";
        if(strtoupper($refundType)=="PARTIAL") $nvpstr=$nvpstr."&AMT=$amount";
         $nvpstr = $nvpHeaderStr.$nvpstr;

        if(strlen(str_replace('VERSION=','', strtoupper($nvpstr))) == strlen($nvpstr)){
            $nvpstr = "&VERSION=" . urlencode($version) . $nvpstr;
        }
        $nvpreq="METHOD=".urlencode($methodName).$nvpstr;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$API_Endpoint);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch,CURLOPT_POSTFIELDS,$nvpreq);

        $response = curl_exec($ch);
        $nvpResArray=$this->deformatNVP($response);
        $nvpReqArray=$this->deformatNVP($nvpreq);

        if (curl_errno($ch)){} else {
            curl_close($ch);
        }
        $nvpResArray['pay_id'] = 0;
        if($nvpResArray['ACK'] == 'Success'){
            $paymentArray['txn_user'] = $param['pay_user'];
            $paymentArray['txn_pay_id'] = $param['pay_id'];
            $paymentArray['txn_payal_trans_id'] = $nvpResArray['REFUNDTRANSACTIONID'];
            $paymentArray['txn_paypal_data'] = serialize($nvpResArray);
            $paymentArray['txn_status'] = 'Completed';
            $paymentArray['txn_amount'] = $nvpResArray['NETREFUNDAMT'];
            $paymentArray['txn_curreny'] = $nvpResArray['CURRENCYCODE'];
            $paymentArray['tx_date'] = $nvpResArray['TIMESTAMP'];
            $paymentArray['txn_added'] = time();
            $paymentArray['txn_type'] = 'Credit';
            $updateData['pay_status'] = $param['pay_status'];
            $pay_id =  $this->AdminPaymentModel->updatePaymants($updateData,$param['pay_id']); 
            $pay_id =  $this->AdminPaymentModel->updateTransactions($paymentArray); 
            $nvpResArray['pay_id'] = $pay_id;
        }
        return $nvpResArray;
    }

    function deformatNVP ($nvpstr){
        $intial = 0;
        $nvpArray = array();
        while (strlen($nvpstr)) {

            $keypos = strpos($nvpstr, '=');

            $valuepos = strpos($nvpstr, '&') ? strpos($nvpstr, '&') : strlen(
            $nvpstr);

            $keyval = substr($nvpstr, $intial, $keypos);
            $valval = substr($nvpstr, $keypos + 1, $valuepos - $keypos - 1);

            $nvpArray[urldecode($keyval)] = urldecode($valval);
            $nvpstr = substr($nvpstr, $valuepos + 1, strlen($nvpstr));
        }
        return $nvpArray;
    }
    
    function formAutorization ($auth_token, $auth_signature, $auth_timestamp){
        $authString = "token=" . $auth_token . ",signature=" . $auth_signature .
         ",timestamp=" . $auth_timestamp;
        return $authString;
    }
    
    public function authenticateModule() {
        $resp = $this->AdminModel->authenticateAdmin('payments');
        if($resp) { 
            $this->flashMessenger()->addMessage($resp['msg']);
            return $this->redirect()->toRoute($resp['redirectUrl']); 
        }
    }
	
	public function notificationAction() {
        $this->authenticateModule();
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));              
            }
            foreach($this->params()->fromPost('ids') as $payMethodId){
                $this->AdminPaymentModel->deleteNotificationDetails($payMethodId);
            }
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy');
        $page = $this->params()->fromQuery('page', 1);
        $paginator = $this->AdminPaymentModel->getNotification($filterData,'paginator');
        
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'account_settings'],
                ['Sub_menu_select' => 'payments/notification']
            ));  
    }
	
	
	public function notificationManagerAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        
        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
            $resp = $this->AdminModel->checkAdminPrivilage($priv);               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));
            }
            if($action == 'add' || $action == 'edit'){
                $updatePaymentMethodData = array(
                    'user' => $this->params()->fromPost('user_id'),
                    'holder_name'  =>  $this->params()->fromPost('holder_name'),
                    'card_no' => $this->AdminCommonPlugin->encrypt($this->params()->fromPost('card_no')),
                    'card_type' => $this->params()->fromPost('card_type'),
                    'payment_card_type' => $this->params()->fromPost('payment_card_type'),
                    'issue_date'    => $this->params()->fromPost('issue_date'),
                    'issue_number'    => $this->params()->fromPost('issue_number'),
                    'card_valid' => $this->params()->fromPost('card_valid'),
                    'card_cvv' => $this->params()->fromPost('card_cvv'),
                    'modified' => date('Y-m-d H:i:s')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminPaymentModel->addUpdateUserPaymentMethodRow($updatePaymentMethodData,$rowId);
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Payment method info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to ".$action." payment method info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));
            }
        }
        $paymentMethodData = false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $paymentMethodData = $this->AdminPaymentModel->getUserNotification($id);
            if(empty($paymentMethodData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid peyment method!"));
                return $this->redirect()->toRoute('admin-payments', array(
                    'controller' => 'admin',
                    'action' =>  'methods'
                ));
            }
        }
        
        $viewArray = array(
            'paymentRow' => $paymentMethodData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );
        if($action == 'edit'){
            $uAcc = $this->AdminUserModel->userAccouts(array(),$viewArray['paymentRow']['user_id']);
            if($uAcc){
                foreach($uAcc as $acc){
                    $retArr[] = array(
                        'id' => $acc['ID'],
                        'name' => $acc['email_address']
                    );
                }
                $viewArray['userPrePop'] = json_encode($retArr);
            }
        }
        
        $viewArray['Menu_select'] = 'account_settings';
        $viewArray['Sub_menu_select'] = 'payments/notification';
       
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    }
	
	
	
	
	public function privacyAction() {
        $this->authenticateModule();
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));              
            }
            foreach($this->params()->fromPost('ids') as $payMethodId){
                $this->AdminPaymentModel->deleteNotificationDetails($payMethodId);
            }
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy');
        $page = $this->params()->fromQuery('page', 1);
        $paginator = $this->AdminPaymentModel->getNotification($filterData,'paginator');
        
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'account_settings'],
                ['Sub_menu_select' => 'payments/privacy']
            ));  
    }
	
	
	public function privacyManagerAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        
        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
            $resp = $this->AdminModel->checkAdminPrivilage($priv);               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));
            }
            if($action == 'add' || $action == 'edit'){
                $updatePaymentMethodData = array(
                    'user' => $this->params()->fromPost('user_id'),
                    'holder_name'  =>  $this->params()->fromPost('holder_name'),
                    'card_no' => $this->AdminCommonPlugin->encrypt($this->params()->fromPost('card_no')),
                    'card_type' => $this->params()->fromPost('card_type'),
                    'payment_card_type' => $this->params()->fromPost('payment_card_type'),
                    'issue_date'    => $this->params()->fromPost('issue_date'),
                    'issue_number'    => $this->params()->fromPost('issue_number'),
                    'card_valid' => $this->params()->fromPost('card_valid'),
                    'card_cvv' => $this->params()->fromPost('card_cvv'),
                    'modified' => date('Y-m-d H:i:s')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminPaymentModel->addUpdateUserPaymentMethodRow($updatePaymentMethodData,$rowId);
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Payment method info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to ".$action." payment method info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));
            }
        }
        $paymentMethodData = false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $paymentMethodData = $this->AdminPaymentModel->getUserNotification($id);
            if(empty($paymentMethodData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid peyment method!"));
                return $this->redirect()->toRoute('admin-payments', array(
                    'controller' => 'admin',
                    'action' =>  'methods'
                ));
            }
        }
        
        $viewArray = array(
            'paymentRow' => $paymentMethodData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );
        if($action == 'edit'){
            $uAcc = $this->AdminUserModel->userAccouts(array(),$viewArray['paymentRow']['user_id']);
            if($uAcc){
                foreach($uAcc as $acc){
                    $retArr[] = array(
                        'id' => $acc['ID'],
                        'name' => $acc['email_address']
                    );
                }
                $viewArray['userPrePop'] = json_encode($retArr);
            }
        }
        
        $viewArray['Menu_select'] = 'account_settings';
        $viewArray['Sub_menu_select'] = 'payments/privacy';
       
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    }
	
		
	public function cancelaccountAction() {
        $this->authenticateModule();
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if($this->params()->fromPost('doAction') != '' && $this->params()->fromPost('ids') != ''){
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));              
            }
            foreach($this->params()->fromPost('ids') as $payMethodId){
                $this->AdminPaymentModel->deleteNotificationDetails($payMethodId);
            }
        }
        $filterData = array();
        $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
        $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
        $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
        $filterData['searchBy'] =  $this->params()->fromQuery('searchBy');
        $page = $this->params()->fromQuery('page', 1);
        $paginator = $this->AdminPaymentModel->getCancelledaccount($filterData,'paginator');
        

        return new ViewModel(array_merge($this->params()->fromQuery(), 
                ['paginator' => $paginator],
                ['filterData'=>$filterData],
                ['flashMessages' =>  $this->flashMessenger()->getMessages()],
                ['Menu_select'=>'account_settings'],
                ['Sub_menu_select' => 'payments/cancelaccount']
            ));  
    }
	
	
	public function cancelaccountManagerAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0);
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        
        if($this->getRequest()->isPost() && ($priv == 'UPDATE' || $priv == 'INSERT')){
            $resp = $this->AdminModel->checkAdminPrivilage($priv);               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));
            }
            if($action == 'add' || $action == 'edit'){
                $updatePaymentMethodData = array(
                    'user' => $this->params()->fromPost('user_id'),
                    'holder_name'  =>  $this->params()->fromPost('holder_name'),
                    'card_no' => $this->AdminCommonPlugin->encrypt($this->params()->fromPost('card_no')),
                    'card_type' => $this->params()->fromPost('card_type'),
                    'payment_card_type' => $this->params()->fromPost('payment_card_type'),
                    'issue_date'    => $this->params()->fromPost('issue_date'),
                    'issue_number'    => $this->params()->fromPost('issue_number'),
                    'card_valid' => $this->params()->fromPost('card_valid'),
                    'card_cvv' => $this->params()->fromPost('card_cvv'),
                    'modified' => date('Y-m-d H:i:s')
                );
                $rowId = ($action == 'add')?false:$id;
                $result = false;
                $result = $this->AdminPaymentModel->addUpdateUserPaymentMethodRow($updatePaymentMethodData,$rowId);
                if($result){
                    $this->flashMessenger()->addMessage(array('alert-success' => "Payment method info was ".$action."ed successfully!"));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "Unable to ".$action." payment method info. Please try again later.!"));
                }
                return $this->redirect()->toRoute('admin-payments', array(
                        'controller' => 'admin',
                        'action' =>  'methods'
                    ));
            }
        }
        $paymentMethodData = false;
        if(!empty($id) && ($action == 'edit' || $action == 'view')){
            $paymentMethodData = $this->AdminPaymentModel->getCancelaccountDetails($id);
            if(empty($paymentMethodData)){
                $this->flashMessenger()->addMessage(array('alert-danger' => "Invalid peyment method!"));
                return $this->redirect()->toRoute('admin-payments', array(
                    'controller' => 'admin',
                    'action' =>  'cancelaccount'
                ));
            }
        }
        
        $viewArray = array(
            'paymentRow' => $paymentMethodData,
            'flashMessages' =>   $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );
        if($action == 'edit'){
            $uAcc = $this->AdminUserModel->userAccouts(array(),$viewArray['paymentRow']['user_id']);
            if($uAcc){
                foreach($uAcc as $acc){
                    $retArr[] = array(
                        'id' => $acc['ID'],
                        'name' => $acc['email_address']
                    );
                }
                $viewArray['userPrePop'] = json_encode($retArr);
            }
        }
        
        $viewArray['Menu_select'] = 'account_settings';
        $viewArray['Sub_menu_select'] = 'payments/cancelaccount';
       
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    }
	
	
}
?>