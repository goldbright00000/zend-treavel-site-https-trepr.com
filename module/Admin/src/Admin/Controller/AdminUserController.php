<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager;
use Admin\Model\AdminUserModel;
use Zend\Mvc\Plugin\FlashMessenger;
use Zend\Mvc\Plugin\Url;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;

require_once(ACTUAL_ROOTPATH . "Upload/src/Upload.php");
require_once(ACTUAL_ROOTPATH . 'OAuth2/Client.php');
require_once(ACTUAL_ROOTPATH . 'traity/jwt/src/JWT.php');
use \Firebase\JWT\JWT;

class AdminUserController extends AbstractActionController
{
    const ITEM_PER_PAGE = 20;
    const ADDR_ITEM_PER_PAGE = 1;

    public function __construct($data = false)
    {
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if ($data['models'] && is_array($data['models'])) {
            foreach ($data['models'] as $model) {
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->adminModules = $data['configs']['adminModules'];
        $this->adminMenus = $data['configs']['adminMenus'];
        $this->timeZones = $data['configs']['timezones'];
        //$this->AdminModel->authenticateAdmin('user_accounts');
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            // return $this->redirect()->toRoute('admin');
        }

    }

    public function indexAction()
    { 
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }

        $filterData = array();

        if ($this->params()->fromPost('doAction')) { 
            $doAction = $this->params()->fromPost('doAction');
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('useraccounts');
            }
            $status = $this->AdminUserModel->doUserActions($doAction, $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('useraccounts');
        } else if ($this->params()->fromQuery('doAction') && $this->params()->fromQuery('id')) {
            $doAction = $this->params()->fromQuery('doAction');
            $id = $this->params()->fromQuery('id');
            $val = $this->params()->fromQuery('val');
            $uId = $this->params()->fromQuery('uId');
            $listUrl = $this->params()->fromQuery('listUrl');
            $status = $this->AdminUserModel->doUserActions($doAction, $id, $val);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $uId . '#trust_verification',));
        }


        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        //$arrAdminAccounts = $this->AdminUserModel->userAccouts( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        $arrAdminAccounts = $this->AdminUserModel->userAccouts($filterData);
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);


       return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'user_accounts'],
            ['Sub_menu_select' => 'useraccounts']
        ));
    }

    public function authenticateModule()
    {
        $resp = $this->AdminModel->authenticateAdmin('user_accounts');
        if ($resp) {
            $this->flashMessenger()->addMessage($resp['msg']);
            return $this->redirect()->toRoute($resp['redirectUrl']);
        }
    }

    public function travellerenqlistAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $doAction = $this->params()->fromPost('doAction');
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('travellerenqlist');
            }
            $status = $this->AdminUserModel->doTravellerlistActions($doAction, $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('travellerenqlist');
        } else if ($this->params()->fromQuery('doAction') && $this->params()->fromQuery('id')) {
            $doAction = $this->params()->fromQuery('doAction');
            $id = $this->params()->fromQuery('id');
            $val = $this->params()->fromQuery('val');
            $uId = $this->params()->fromQuery('uId');
            $listUrl = $this->params()->fromQuery('listUrl');
            $status = $this->AdminUserModel->doTravellerlistActions($doAction, $id, $val);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $uId . '#trust_verification',));
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        //$arrAdminAccounts = $this->AdminUserModel->userAccouts( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        $arrAdminAccounts = $this->AdminUserModel->travellerlistAccouts($filterData);
        // print_r($arrAdminAccounts);
        // die();
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'user_notifications'],
            ['Sub_menu_select' => 'users/travellerenqlist']
        ));
    }

    public function seekerenqlistAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $doAction = $this->params()->fromPost('doAction');
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('seekerenqlist');
            }
            $status = $this->AdminUserModel->doSeekerlistActions($doAction, $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('seekerenqlist');
        } else if ($this->params()->fromQuery('doAction') && $this->params()->fromQuery('id')) {
            $doAction = $this->params()->fromQuery('doAction');
            $id = $this->params()->fromQuery('id');
            $val = $this->params()->fromQuery('val');
            $uId = $this->params()->fromQuery('uId');
            $listUrl = $this->params()->fromQuery('listUrl');
            $status = $this->AdminUserModel->doSeekerlistActions($doAction, $id, $val);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $uId . '#trust_verification',));
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        //$arrAdminAccounts = $this->AdminUserModel->userAccouts( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        $arrAdminAccounts = $this->AdminUserModel->seekerenqlistAccouts($filterData);
        // print_r($arrAdminAccounts);
        // die();
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'user_notifications'],
            ['Sub_menu_select' => 'users/seekerenqlist']
        ));
    }

    public function supertravellerlistAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $doAction = $this->params()->fromPost('doAction');
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('supertravellerlist');
            }
            $status = $this->AdminUserModel->doSupertravellerlistActions($doAction, $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('supertravellerlist');
        } else if ($this->params()->fromQuery('doAction') && $this->params()->fromQuery('id')) {
            $doAction = $this->params()->fromQuery('doAction');
            $id = $this->params()->fromQuery('id');
            $val = $this->params()->fromQuery('val');
            $uId = $this->params()->fromQuery('uId');
            $listUrl = $this->params()->fromQuery('listUrl');
            $status = $this->AdminUserModel->doSupertravellerlistActions($doAction, $id, $val);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $uId . '#trust_verification',));
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        //$arrAdminAccounts = $this->AdminUserModel->userAccouts( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        $arrAdminAccounts = $this->AdminUserModel->supertravellerlistAccouts($filterData);
        // print_r($arrAdminAccounts);
        // die();
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'user_notifications'],
            ['Sub_menu_select' => 'users/supertravellerlist']
        ));
    }

    public function travellerlistAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        /*print_r($action);die;	*/
        $id = $this->params()->fromQuery('id', 0);
        if ($this->params()->fromPost('rejectBtn')) {
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('travellerenqlist');
            }
            $identity_id = $this->params()->fromPost('identity_id');
            $status = $this->AdminUserModel->doUserActions('reject', $identity_id, $this->params()->fromPost('rejectReason'));
            $identityRow = $this->AdminUserModel->userIdentityRow($identity_id);
            $mailRow['first_name'] = $identityRow['first_name'];
            $mailRow['email_address'] = $identityRow['email_address'];
            $mailRow['identity_name'] = $identityRow['identity_name'];
            $mailRow['rejectReason'] = $this->params()->fromPost('rejectReason');
            $this->AdminMailModel->sendVerificationRejectMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $id . '#tv',));
        }
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('travellerenqlist');
        }
        if (isset($_POST['subAdminBtn'])) {
            $checkEmail = $this->AdminUserModel->checkAdminEmailExists($this->params()->fromPost('email_address'), $this->params()->fromPost('id'));
            if ($checkEmail === false) {
                $insData['first_name'] = $this->params()->fromPost('first_name');
                $insData['last_name'] = $this->params()->fromPost('last_name');
                $insData['email_address'] = $this->params()->fromPost('email_address');
                if (isset($_POST['password']) && !empty($_POST['password']))
                    $insData['password'] = md5($this->params()->fromPost('password'));
                $insData['dob'] = !empty($this->params()->fromPost('dob')) ? strtotime($this->params()->fromPost('dob')) : '';
                $insData['age'] = $this->params()->fromPost('age');
                if ($action == 'add') $insData['active'] = 1;
                $insData['phone'] = $this->params()->fromPost('phone');
                $insData['gender'] = $this->params()->fromPost('gender');
                $insData['language'] = implode(",", $_POST['language']);
                $insData['country'] = $this->params()->fromPost('country');
                $insData['city'] = $this->params()->fromPost('city');
                $insData['school'] = $this->params()->fromPost('school');
                $insData['work'] = $this->params()->fromPost('work');
                $insData['timezone'] = $this->params()->fromPost('timezone');
                $insData['aboutus'] = $this->params()->fromPost('aboutus');
                $insData['emergency_full_name'] = $this->params()->fromPost('emergency_full_name');
                $insData['emergency_phone'] = $this->params()->fromPost('emergency_phone');
                $insData['emergency_email'] = $this->params()->fromPost('emergency_email');
                $insData['emergency_relationship'] = $this->params()->fromPost('emergency_relationship');

                $updateAccount = $this->AdminUserModel->addEditUserAccounts($insData, $this->params()->fromPost('id'), $this->params()->fromPost('action'));
                $message = ($action == 'add') ? 'User account created successfully' : 'User account updated  successfully';
                if ($updateAccount) {
                    $this->flashMessenger()->addMessage(array('alert-success' => $message));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update account. Please try again'));
                }
                $flashMessages = $this->flashMessenger()->getMessages();
                return $this->redirect()->toRoute('travellerenqlist');
            } else {
                $flashMessages[] = array('alert-danger' => 'Email address already being used');
            }
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $viewArray = array(
            'adminRow' => $this->AdminUserModel->getUserRow($id),
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,
            'lang' => $this->AdminModel->getLanguages(),
            'countries' => $this->AdminModel->getCountries(),
            'AdminUserModel' => $this->AdminUserModel,
            'site_time_zones' => $this->timeZones
        );

        if ($action == 'view') {
            $app_key = $this->siteConfigs['traity_api_key'];
            $app_secret = $this->siteConfigs['traity_secret_key'];
            $TRAITY_API = 'https://api.traity.com';
            $CURRENT_USER_ID = $id;
            $api = new \OAuth2\Client($app_key, $app_secret);
            //$result = $api->getAccessToken($TRAITY_API.'/oauth/token',
            //    'client_credentials', array());
            $token = $result['result']['access_token'];
            $api->setAccessToken($token);
            $api->setAccessTokenType(\OAuth2\Client::ACCESS_TOKEN_BEARER);

            $JWT = new \Firebase\JWT\JWT();
            $current_user_id = $id;
            $options = array('verified' => array('phone', 'email'));
            $payload = array_merge(array('time' => time(), 'current_user_id' => $current_user_id), $options);
            $signature = array(
                'key' => $app_key,
                'payload' => $JWT->encode($payload, $app_secret)
            );
            $viewArray['widget_signature'] = $JWT->encode($signature, '');
            $viewArray['userInvites'] = $this->AdminUserModel->getUserInviteDetails($id);
        }

        $viewArray['Menu_select'] = 'user_accounts';
        if ($action == 'add')
            $viewArray['Sub_menu_select'] = 'travellerlist';
        else $viewArray['Sub_menu_select'] = 'travellerenqlist';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function usermanagerAction()
    { 
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        /*print_r($action);die;	*/
        $id = $this->params()->fromQuery('id', 0);
        if ($this->params()->fromPost('rejectBtn')) {
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('useraccounts');
            }
            $identity_id = $this->params()->fromPost('identity_id');
            $status = $this->AdminUserModel->doUserActions('reject', $identity_id, $this->params()->fromPost('rejectReason'));
            $identityRow = $this->AdminUserModel->userIdentityRow($identity_id);
            $mailRow['first_name'] = $identityRow['first_name'];
            $mailRow['email_address'] = $identityRow['email_address'];
            $mailRow['identity_name'] = $identityRow['identity_name'];
            $mailRow['rejectReason'] = $this->params()->fromPost('rejectReason');
            $this->AdminMailModel->sendVerificationRejectMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $id . '#tv',));
        }
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('useraccounts');
        }
        if (isset($_POST['subAdminBtn'])) {  
            $checkEmail = $this->AdminUserModel->checkAdminEmailExists($this->params()->fromPost('email_address'), $this->params()->fromPost('id'));
            if ($checkEmail === false) {
                $insData['first_name'] = $this->params()->fromPost('first_name');
                $insData['last_name'] = $this->params()->fromPost('last_name');
                $insData['email_address'] = $this->params()->fromPost('email_address');
                if (isset($_POST['password']) && !empty($_POST['password']))
                    $insData['password'] = md5($this->params()->fromPost('password'));
               
                    $explode_dob=explode('-',$this->params()->fromPost('dob'));
                    $insData['dob']= $explode_dob[2].'-'.$explode_dob[1].'-'.$explode_dob[0];
               // $insData['dob'] = !empty($this->params()->fromPost('dob')) ? strtotime($this->params()->fromPost('dob')) : '';
                $insData['age'] = $this->params()->fromPost('age');
                if ($action == 'add') $insData['active'] = 1;
                $insData['phone'] = $this->params()->fromPost('phone');
                $insData['gender'] = $this->params()->fromPost('gender');
               if(isset($_REQUEST['language'])){
                    $insData['language'] = implode(",", $_POST['language']);
               }
              /*  $insData['country'] = $this->params()->fromPost('country');
                $insData['city'] = $this->params()->fromPost('city');
                $insData['state'] = $this->params()->fromPost('state');*/

                 $insData['location']=$this->params()->fromPost('city').",".$this->params()->fromPost('state').",".$this->params()->fromPost('country');
              
                $insData['school'] = $this->params()->fromPost('school');
                $insData['work'] = $this->params()->fromPost('work');
                $insData['timezone'] = $this->params()->fromPost('timezone');
                $insData['aboutus'] = $this->params()->fromPost('aboutus');
                $insData['emergency_full_name'] = $this->params()->fromPost('emergency_full_name');
                $insData['emergency_phone'] = $this->params()->fromPost('emergency_phone');
                $insData['emergency_email'] = $this->params()->fromPost('emergency_email');
                $insData['emergency_relationship'] = $this->params()->fromPost('emergency_relationship');

                $updateAccount = $this->AdminUserModel->addEditUserAccounts($insData, $this->params()->fromPost('id'), $this->params()->fromPost('action'));
                $message = ($action == 'add') ? 'User account created successfully' : 'User account updated  successfully';
                if ($updateAccount) {
                    $this->flashMessenger()->addMessage(array('alert-success' => $message));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update account. Please try again'));
                }
                $flashMessages = $this->flashMessenger()->getMessages();
                return $this->redirect()->toRoute('useraccounts');
            } else {
                $flashMessages[] = array('alert-danger' => 'Email address already being used');
            }
        }
        
        //print_r($this->AdminUserModel->getUserRow($id));die;
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $viewArray = array(
            'adminRow' => $this->AdminUserModel->getUserRow($id),
            'adminphotos' => $this->AdminUserModel->getUserPhotos($id),
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,
            'lang' => $this->AdminModel->getLanguages(),
            'countries' => $this->AdminModel->getCountries(),
            'AdminUserModel' => $this->AdminUserModel,
            'site_time_zones' => $this->timeZones
        );

        if ($action == 'view') {
            
            $viewArray['userInvites'] = $this->AdminUserModel->getInviteDetails($id);
            $viewArray['userNotification']          = $this->AdminUserModel->userNotifications($filterData = array(), $id);
            $viewArray['userAdminNotification']     = $this->AdminUserModel->adminNotificationsByUserId($id);
            $viewArray['userSentNotification']      = $this->AdminUserModel->adminSentNotificationsByUserId($id);
            $viewArray['userTravellerEnquiries']    = $this->AdminUserModel->adminTravellerEnquiriesByUserId($id);
            $viewArray['userSeekerEnquiries']       = $this->AdminUserModel->adminSeekerEnquiriesByUserId($id);
            $viewArray['userDisputes']              = $this->AdminUserModel->adminDisputesByUserId($id);
        }

        $viewArray['Menu_select'] = 'user_accounts';
        if ($action == 'add')
            $viewArray['Sub_menu_select'] = 'usermanager';
        else $viewArray['Sub_menu_select'] = 'useraccounts';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);


        return $viewModel;
    }

    public function userphotosAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('userphotos');
            }
            $status = $this->AdminUserModel->doUserPhotosActions($this->params()->fromPost('doAction'), $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('userphotos');
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        $arrAdminAccounts = $this->AdminUserModel->userPhotos($filterData);
        //echo '<pre>';print_r($arrAdminAccounts);exit;
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator], ['filterData' => $filterData], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts'], ['Sub_menu_select' => 'users/userphotos']));
    }


    /* New Code Senthilk  */

    public function userphotomanagerAction()
    { 
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }


       if($_REQUEST['action'] == 'enable' || $_REQUEST['action'] == 'disable') {
                $this->AdminUserModel->UpdatePhotosStatus($_REQUEST['action'],$_REQUEST['id']);
                return $this->redirect()->toRoute('userphotos');
       }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
       
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('userphotos');
        }
        if (isset($_POST['subAdminBtn'])) { 


            
            //$insData['user_id'] = $this->params()->fromPost('user_id');

         //   $insData['user_id'] = $this->params()->fromPost('user_id');


            $insData['marked_as_profile_photo'] = $this->params()->fromPost('marked_as_profile_photo');
            if ($_FILES['user_photo_name']['error'] == 0) { 
                $image_path = "profile_picture";
                $field_name = 'user_photo_name';
                $file_name_prefix = 'USER_DP_' . time();
                $configArray = array(
                    array(
                        'image_x' => 150,
                        'sub_dir_name' => 'medium'
                    ),
                    array(
                        'image_x' => 100,
                        'sub_dir_name' => 'small'
                    )
                );  

             

                $imgResult = $this->uploadUserDP($field_name, $file_name_prefix, $image_path, $configArray);
               

                if ($imgResult['result'] == 'success') {
                    $idverfy_image = $imgResult['fileName'];
                    $insData['user_photo_name'] = $idverfy_image;
                
                    echo $updateAccount = $this->AdminUserModel->addEditUserPhotos($insData, $this->params()->fromPost('id'), $this->params()->fromPost('action'));

                }

            } else {
                $insData['user_photo_name'] = $this->params()->fromPost('user_photo_name_txt');
               
                 $updateAccount = $this->AdminUserModel->addEditUserPhotos($insData, $this->params()->fromPost('id'), $this->params()->fromPost('action'));

               
            }
            $message = ($action == 'add') ? 'User photo created successfully' : 'User photo updated  successfully';
            if ($updateAccount) {
                $this->flashMessenger()->addMessage(array('alert-success' => $message));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update photo. Please try again'));
            }
            
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('userphotos');
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $viewArray = array(
            'adminRow' => $this->AdminUserModel->userPhotoRow($id),
            
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,

        );

    $viewArray['allphotos'] = $this->AdminUserModel->EditBelowPhotos($viewArray['adminRow']['user_id'],$viewArray['adminRow']['user_photo_id']);
      //print_r($viewArray['allphotos']);die;
        


        if ($action == 'edit') { 
            //Pre populate user field
            $uAcc = $this->AdminUserModel->userAccouts(array(), $viewArray['adminRow']['user_id']);
            if ($uAcc) {
                foreach ($uAcc as $acc) {
                    $retArr[] = array(
                        'id' => $acc['ID'],
                        'name' => $acc['email_address']
                    );
                }
                $viewArray['userPrePop'] = json_encode($retArr);
            }
        }
        $viewArray['Menu_select'] = 'user_accounts';
        $viewArray['Sub_menu_select'] = 'users/userphotos';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    /* New Code End   */

    public function uploadUserDP($field_name, $file_name, $path, $configArray)
    { 
        $this->authenticateModule();
        if (isset($_FILES[$field_name])) {
            $filename = $_FILES[$field_name]['name'];
            $ext = pathinfo($filename, PATHINFO_EXTENSION);
            $file_name = $file_name;
            $handle = new \upload($_FILES[$field_name]);
            if ($handle->uploaded) { 
                $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/'); 
                $handle->file_new_name_body = $file_name;
                $handle->image_resize = true;
                if ($configArray && !empty($configArray)) {
                    foreach ($configArray as $config) {
                        $handle->file_new_name_body = $file_name;
                        $handle->image_resize = true;
                        $handle->image_x = $config['image_x'];
                        $handle->image_ratio_y = true;
                        $handle->process(ACTUAL_ROOTPATH . '/uploads/' . $path . '/' . $config['sub_dir_name'] . '/');
                       
                    }
                    if ($handle->processed) { 
                        $handle->clean();
                        $retArr = array('result' => 'success', 'fileName' => $file_name . '.' . $ext);
                    } else {
                        $retArr = array('result' => 'failed', 'error' => $handle->error);
                    }
                }
            } else {
                $retArr = array('result' => 'failed', 'error' => $handle->error);
            }
            return $retArr;
        }
    }

    public function userphotoinfoAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('userphotos');
        }
        if (isset($_POST['subAdminBtn'])) {
            $insData['user_id'] = $this->params()->fromPost('user_id');

            $insData['marked_as_profile_photo'] = $this->params()->fromPost('marked_as_profile_photo');
            if ($_FILES['user_photo_name']['error'] == 0) {
                $image_path = "profile_picture";
                $field_name = 'user_photo_name';
                $file_name_prefix = 'USER_DP_' . time();
                $configArray = array(
                    array(
                        'image_x' => 150,
                        'sub_dir_name' => 'medium'
                    ),
                    array(
                        'image_x' => 100,
                        'sub_dir_name' => 'small'
                    )
                );
                $imgResult = $this->uploadUserDP($field_name, $file_name_prefix, $image_path, $configArray);
                if ($imgResult['result'] == 'success') {
                    $idverfy_image = $imgResult['fileName'];
                    $insData['user_photo_name'] = $idverfy_image;
                }

            } else {
                $insData['user_photo_name'] = $this->params()->fromPost('user_photo_name_txt');
            }
            $updateAccount = $this->AdminUserModel->addEditUserPhotos($insData, $this->params()->fromPost('id'), $this->params()->fromPost('action'));
            $message = ($action == 'add') ? 'User photo created successfully' : 'User photo updated  successfully';
            if ($updateAccount) {
                $this->flashMessenger()->addMessage(array('alert-success' => $message));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update photo. Please try again'));
            }
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('userphotos');
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $viewArray = array(
            'adminRow' => $this->AdminUserModel->userPhotoRow($id),
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,

        );
        if ($action == 'edit') {
            //Pre populate user field
            $uAcc = $this->AdminUserModel->userAccouts(array(), $viewArray['adminRow']['user_id']);
            if ($uAcc) {
                foreach ($uAcc as $acc) {
                    $retArr[] = array(
                        'id' => $acc['ID'],
                        'name' => $acc['email_address']
                    );
                }
                $viewArray['userPrePop'] = json_encode($retArr);
            }
        }
        $viewArray['Menu_select'] = 'user_accounts';
        $viewArray['Sub_menu_select'] = 'users/userphotos';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function uservideosAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('uservideos');
            }
            $status = $this->AdminUserModel->doUserVideosActions($this->params()->fromPost('doAction'), $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('uservideos');
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        $arrAdminAccounts = $this->AdminUserModel->userVideos($filterData);
        //echo '<pre>';print_r($arrAdminAccounts);exit;
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator], ['filterData' => $filterData], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts'], ['Sub_menu_select' => 'users/uservideos']));
    }

    public function uservideomanagerAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('uservideos');
        }
        if (isset($_POST['subAdminBtn'])) {
            $insData['user_id'] = $this->params()->fromPost('user_id');
            $insData['video_parser_type'] = $this->params()->fromPost('parserType');
            $insData['video_url_id'] = $this->params()->fromPost('videoUrlId');
            $insData['user_video_url'] = $this->params()->fromPost('videoUrl');
            $updateAccount = $this->AdminUserModel->addEditUserVideos($insData, $this->params()->fromPost('id'), $this->params()->fromPost('action'));
            $message = ($action == 'add') ? 'User video created successfully' : 'User video updated  successfully';
            if ($updateAccount) {
                $this->flashMessenger()->addMessage(array('alert-success' => $message));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update video. Please try again'));
            }
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('uservideos');
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $viewArray = array(
            'adminRow' => $this->AdminUserModel->userVideoRow($id),
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,

        );
        if ($action == 'edit') {
            //Pre populate user field
            $uAcc = $this->AdminUserModel->userAccouts(array(), $viewArray['adminRow']['user_id']);
            if ($uAcc) {
                foreach ($uAcc as $acc) {
                    $retArr[] = array(
                        'id' => $acc['ID'],
                        'name' => $acc['email_address']
                    );
                }
                $viewArray['userPrePop'] = json_encode($retArr);
            }
        }
        $viewArray['Menu_select'] = 'user_accounts';
        $viewArray['Sub_menu_select'] = 'users/uservideos';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function userTrustVerificationAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $ids = $this->params()->fromPost('ids');
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('usertrustverification');
            }

            $status = $this->AdminUserModel->doUserActions($this->params()->fromPost('doAction'), $ids);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usertrustverification');
        }
        else if ($this->params()->fromPost('rejectBtn'))
        {
            $identity_id = $this->params()->fromPost('identity_id');

            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('usertrustverification');
            }
            $status = $this->AdminUserModel->doUserActions('reject', $identity_id, $this->params()->fromPost('rejectReason'));
            $identityRow = $this->AdminUserModel->userIdentityRow($identity_id);
            $mailRow['first_name'] = $identityRow['first_name'];
            $mailRow['email_address'] = $identityRow['email_address'];
            $mailRow['identity_name'] = $identityRow['identity_name'];
            $mailRow['rejectReason'] = $this->params()->fromPost('rejectReason');
            $this->AdminMailModel->sendVerificationRejectMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usertrustverification');
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        $arrAdminAccounts = $this->AdminUserModel->getUserTrustVerification($filterData);
        // echo '<pre>';print_r($arrAdminAccounts);die;
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator], ['filterData' => $filterData], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts'], ['Sub_menu_select' => 'users/usertrustverification']));
    }

    public function userTrustVerificationManagerAction()
    { //print_r($_REQUEST);die;
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = $addressRow = $arrUsrAdrList = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        $listUri = $this->params()->fromQuery('listUri', 'pending');
        if ($action == 'list')
        {
                if(isset($_REQUEST['rejectBtn'])){
                    

                    $this->AdminUserModel->identityDisApprove($_REQUEST['identity_id'], $_REQUEST['rejectReason']);
                }else {
                    $ids = $this->params()->fromPost('ids');

                     $this->AdminUserModel->identityApprove( $ids);
                }
              
            $listData['user_id'] = $id;
           	$app_key = 'IL8KFOnvyHZwTfYkPJIPg';//$this->siteConfigs['traity_api_key'];
			$app_secret = 'bd6hUNAiy2etdTedE36glqRI2b11SpIk2OA';//$this->siteConfigs['traity_secret_key'];
            $TRAITY_API = 'https://api.traity.com';
            $CURRENT_USER_ID = $id;
            $api = new \OAuth2\Client($app_key, $app_secret);
            //$result = $api->getAccessToken($TRAITY_API.'/oauth/token',
            //'client_credentials', array());
            //$token = $result['result']['access_token'];
            //$api->setAccessToken($token);
            //$api->setAccessTokenType(\OAuth2\Client::ACCESS_TOKEN_BEARER);

            $JWT = new \Firebase\JWT\JWT();
            $current_user_id = $id;
            $options = array('verified' => array('phone', 'email'));
            $payload = array_merge(array('time' => time(), 'current_user_id' => $current_user_id), $options);
            $signature = array(
                'key' => $app_key,
                'payload' => $JWT->encode($payload, $app_secret)
            );
            $arrUsrAdrList = $this->AdminUserModel->getUserTrustVerification($listData);
           // print_r( $arrUsrAdrList );die;
            $data = !empty($arrUsrAdrList) ? $arrUsrAdrList : array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($data));
            $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage(self::ITEM_PER_PAGE);
            return new ViewModel(array_merge($this->params()->fromQuery(), ['listUri' => $listUri], ['paginator' => $paginator], ['AdminUserModel' => $this->AdminUserModel], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts'], ['widget_signature' => $JWT->encode($signature, '')]));
        }


        if (isset($_POST['subAdminBtn'])) { 
           
                       
           
                       if ($_FILES['image']['error'] == 0) { 
                           $image_path = "user_verification";
                           $field_name = 'image';
                           $file_name_prefix = 'USER_DP_' . time();
                           $configArray = array(
                               array(
                                   'image_x' => 150,
                                   'sub_dir_name' => 'medium'
                               ),
                               array(
                                   'image_x' => 100,
                                   'sub_dir_name' => 'small'
                               )
                           ); 
           
                   $imgResult = $this->uploadUserDP($field_name, $file_name_prefix, $image_path, $configArray);
                  
                        
           
                          
           
                           if ($imgResult['result'] == 'success') {
                               $idverfy_image = $imgResult['fileName'];
                                $insData['image'] = $idverfy_image;
                           
                                $updateAccount = $this->AdminUserModel->addEditTrustDocument($insData, $this->params()->fromPost('id'));
      
                           }
           
                       } else {
                           $insData['image'] = $this->params()->fromPost('image');
                          
                            $updateAccount = $this->AdminUserModel->addEditTrustDocument($insData, $this->params()->fromPost('id'));
           
                          
                       }
                       $message = ($action == 'add') ? 'User photo created successfully' : 'Document updated  successfully';
                       if ($updateAccount) {
                           $this->flashMessenger()->addMessage(array('alert-success' => $message));
                       } else {
                           $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update photo. Please try again'));
                       }
                       
                       $flashMessages = $this->flashMessenger()->getMessages();
                       return $this->redirect()->toRoute('usertrustverification');
                   }
           
           

        else if ($action == 'view')
        {
            
            if(isset($_REQUEST['rejectBtn'])){
                    

                $this->AdminUserModel->identityDisApprove($_REQUEST['identity_id'], $_REQUEST['rejectReason']);
            }else {
                $ids = $this->params()->fromPost('ids');

                 $this->AdminUserModel->identityApprove( $ids);
            }
            
            
            $priv = 'VIEW';
            $resp = $this->AdminModel->checkAdminPrivilage($priv);
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('usertrustverification');
            }
            if ($this->params()->fromPost('doAction')) { 
                $ids = $this->params()->fromPost('ids');
                $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
                if ($resp) {
                    $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                    return $this->redirect()->toRoute('usertrustverification');
                }
                $status = $this->AdminUserModel->doUserActions($this->params()->fromPost('doAction'), $ids, $this->params()->fromPost('field'));
                $this->flashMessenger()->addMessage(array('alert-success' => $status));
                return $this->redirect()->toRoute('usertrustWithParam', array('controller' => 'admin', 'action' => 'action=view&id='.$id.'&listUri=' . $listUri));


            }
            $listData['ID'] = $id;
            $arrTrustVerify = $this->AdminUserModel->getUserTrustVerification($listData);
        

               $alltrustdocs=  $this->AdminUserModel->EditBelowTrustVerification($arrTrustVerify['ID'],$arrTrustVerify['user_id']);
           
            $data = !empty($arrTrustVerify) ? $arrTrustVerify : array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($data));
            $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage(self::ADDR_ITEM_PER_PAGE);
            return new ViewModel(array_merge($this->params()->fromQuery(), ['listUri' => $listUri], ['paginator' => $paginator], ['tRow' => $arrTrustVerify],['alltrustdocs' =>  $alltrustdocs], ['id' => $id], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts'], ['Sub_menu_select' => 'users/usertrustverification']));
        }
        else if ($action == 'show')
        {
            
            
            $priv = 'VIEW';
            $resp = $this->AdminModel->checkAdminPrivilage($priv);
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('usertrustverification');
            }
           
            $listData['ID'] = $id;
            $arrTrustVerify = $this->AdminUserModel->getUserTrustVerification($listData);
        

               $alltrustdocs=  $this->AdminUserModel->EditBelowTrustVerification($arrTrustVerify['ID'],$arrTrustVerify['user_id']);
           
            $data = !empty($arrTrustVerify) ? $arrTrustVerify : array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($data));
            $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage(self::ADDR_ITEM_PER_PAGE);
            return new ViewModel(array_merge($this->params()->fromQuery(), ['listUri' => $listUri], ['paginator' => $paginator], ['tRow' => $arrTrustVerify],['alltrustdocs' =>  $alltrustdocs], ['id' => $id], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts'], ['Sub_menu_select' => 'users/usertrustverification']));
        } 
        
        
        else
        {
            $viewArray['Menu_select'] = 'user_accounts';
            $viewArray['Sub_menu_select'] = 'users/usertrustverification';
            $viewModel = new ViewModel();
            $viewModel->setVariables($viewArray)
                ->setTerminal(false);
            return $viewModel;
        }
    }

    public function userAddressesAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $doAction = $this->params()->fromPost('doAction');
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin/users/useraddresses');
            }
            // $status = $this->AdminUserModel->doUserAddressActions($this->params()->fromPost('doAction'),$this->params()->fromPost('ids'));
            // $this->flashMessenger()->addMessage(array('alert-success' => $status));
            // return $this->redirect()->toRoute('admin/users/useraddresses');
            // }
            $status = $this->AdminUserModel->doUserAddressActions($doAction, $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('useraddresses');
        } else if ($this->params()->fromQuery('doAction') && $this->params()->fromQuery('id')) {
            $doAction = $this->params()->fromQuery('doAction');
            $id = $this->params()->fromQuery('id');
            $val = $this->params()->fromQuery('val');
            $uId = $this->params()->fromQuery('uId');
            $listUrl = $this->params()->fromQuery('listUrl');
            $status = $this->AdminUserModel->doUserActions($doAction, $id, $val);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('userAddressesManagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $uId . '#trust_verification',));
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        $arrAdminAccounts = $this->AdminUserModel->userAddresses($filterData);
       
        //echo '<pre>';print_r($arrAdminAccounts);exit;
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator], ['filterData' => $filterData], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts'], ['Sub_menu_select' => 'users/useraddresses']));
    }

    public function userReviewslistAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
     if (isset($_REQUEST['action'])) {
  
        if ( $_REQUEST['action'] == 'disable' || $_REQUEST['action'] == 'enable') {
            if ($_REQUEST['action'] == 'disable') {
                $user = array('active' => 0);
            } else {
                $user = array('active' => 1);
            }

            $as = $this->AdminUserModel->addEditReviewlist($user, $_REQUEST['id'], 'edit');
            if ($as) {
                $this->flashMessenger()->addMessage(array('alert-success' => "User review(s) are enable successfully!."));
                return $this->redirect()->toRoute('userreviewslist');
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => "User review(s) are disabled successfully!"));
                return $this->redirect()->toRoute('userreviewslist');
            }

        }
    }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $doAction = $this->params()->fromPost('doAction');
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('userreviewslist');
            }
            $status = $this->AdminUserModel->doReviewlistActions($doAction, $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('userreviewslist');
        } else if ($this->params()->fromQuery('doAction') && $this->params()->fromQuery('id')) {
            $doAction = $this->params()->fromQuery('doAction');
            $id = $this->params()->fromQuery('id');
            $val = $this->params()->fromQuery('val');
            $uId = $this->params()->fromQuery('uId');
            $listUrl = $this->params()->fromQuery('listUrl');
            $status = $this->AdminUserModel->doReviewlistActions($doAction, $id, $val);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $uId . '#trust_verification',));
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        //$arrAdminAccounts = $this->AdminUserModel->userAccouts( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        $arrAdminAccounts = $this->AdminUserModel->userReviewslist($filterData);
        // print_r($arrAdminAccounts);
        // die();
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['adminRow' => $arrAdminAccounts],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'user_accounts'],
            ['Sub_menu_select' => 'userreviewslist']
        ));
    }

    public function userAddressManagerAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = $addressRow = $arrUsrAdrList = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('useraddresses');
        }
        if (isset($_POST['subAdminBtn'])) {
            $insData['user'] = $this->params()->fromPost('user');
            $insData['name'] = $this->params()->fromPost('name');
            $insData['street_address_1'] = $this->params()->fromPost('street_address_1');
            $insData['street_address_2'] = $this->params()->fromPost('street_address_2');
            $insData['city'] = $this->params()->fromPost('city');
            $insData['state'] = $this->params()->fromPost('state');
            $insData['zip_code'] = $this->params()->fromPost('zip_code');
            $insData['country'] = $this->params()->fromPost('country');
            $insData['set_default'] = $this->params()->fromPost('set_default');
            $insData['latitude'] = $this->params()->fromPost('latitude');
            $insData['longitude'] = $this->params()->fromPost('longitude');

            $updateAccount = $this->AdminUserModel->addEditUserAddress($insData, $this->params()->fromPost('id'), $this->params()->fromPost('action'));
            $message = ($action == 'add') ? 'User address created successfully' : 'User address updated  successfully';
            if ($updateAccount) {
                if ($this->params()->fromPost('set_default') == '1') {
                    $this->AdminUserModel->setUserDefaultAddress($this->params()->fromPost('user'), $id);
                }
                $this->flashMessenger()->addMessage(array('alert-success' => $message));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update address. Please try again'));
            }
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('useraddresses');
        }
         if (isset($_POST['subAddressBtn'])) {
            
             
             $city=$_REQUEST['city'];
             $state=$_REQUEST['state'];
             $country=$_REQUEST['country'];
             
             $location=$city.",".$state.",".$country;
            // print_r($_REQUEST);die;
              $insData['location'] = $location;
              $updateAccount = $this->AdminUserModel->EditAddress($insData, $_REQUEST['id']);
         }
        
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $addressRow = $this->AdminUserModel->getprofileaddr($id);
        $viewArray = array(
            'adminRow' => $addressRow,
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,
            'countries' => $this->AdminModel->getCountries(),
        );

        if ($action == 'edit') {
            //Pre populate user field
            $uAcc = $this->AdminUserModel->userAccouts(array(), $viewArray['adminRow']['user']);
            if ($uAcc) {
                foreach ($uAcc as $acc) {
                    $retArr[] = array(
                        'id' => $acc['ID'],
                        'name' => $acc['email_address']
                    );
                }
                $viewArray['userPrePop'] = json_encode($retArr);
            }
        }
        if ($action == 'list') {
           $arrUsrAdrList = $this->AdminUserModel->getUserAllAddress($id, 0);
             //$arrUsrAdrList = $this->AdminUserModel->getAddress($id);
            $data = !empty($arrUsrAdrList) ? $arrUsrAdrList : array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($data));
            $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage(self::ITEM_PER_PAGE);
            return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts']));
        } else if ($action == 'view') {
            $arrUsrAdrList = $this->AdminUserModel->getUserAllAddress(0, $id);
            $data = !empty($arrUsrAdrList) ? $arrUsrAdrList : array();
            $page = $this->params()->fromQuery('page', 1);
            $paginator = new Paginator(new ArrayAdapter($data));
            $paginator->setCurrentPageNumber($page)
                ->setItemCountPerPage(self::ADDR_ITEM_PER_PAGE);
            //echo '<pre>';print_r($paginator);exit;
            return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator], ['id' => $id], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts'], ['Sub_menu_select' => 'users/useraddresses']));
        } else {
            $viewArray['Menu_select'] = 'user_accounts';
            $viewArray['Sub_menu_select'] = 'users/useraddresses';
            $viewModel = new ViewModel();
            $viewModel->setVariables($viewArray)
                ->setTerminal(false);
            return $viewModel;
        }
    }

    public function userReferencesAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('userreferences');
            }
            $status = $this->AdminUserModel->doUserReferenceActions($this->params()->fromPost('doAction'), $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('userreferences');
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        $arrAdminAccounts = $this->AdminUserModel->getUserReferences($filterData, $this->sessionObj->offsetGet('ADMIN_ID'));
        //echo '<pre>';print_r($arrAdminAccounts);exit;
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator], ['filterData' => $filterData], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts'], ['Sub_menu_select' => 'users/userreferences']));
    }

    public function userreferralsAction()
    { 
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('userreferences');
            }
            $status = $this->AdminUserModel->doUserReferralsActions($this->params()->fromPost('doAction'), $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('userreferrals');
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();

        $arrAdminAccounts = $this->AdminUserModel->getUserInviteDetails(false, $filterData);
        //echo '<pre>';print_r($arrAdminAccounts);exit;
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator], ['filterData' => $filterData], ['flashMessages' => $this->flashMessenger()->getMessages()], ['Menu_select' => 'user_accounts'], ['Sub_menu_select' => 'users/userreferences']));
    }

    public function userReferenceManagerAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('userreferences');
        }
        if (isset($_POST['subAdminBtn'])) {
            $insData['reference_message'] = $this->params()->fromPost('reference_message');
            $updateAccount = $this->AdminUserModel->updateReference($insData, $this->params()->fromPost('id'));
            $message = 'References updated  successfully';
            if ($updateAccount) {
                $this->flashMessenger()->addMessage(array('alert-success' => $message));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update reference. Please try again'));
            }
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('userreferences');
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        if ($action == 'view') {
            $dataArr['userId'] = $id;
            $adminRow = $this->AdminUserModel->getUserReferences($dataArr);
        } else {
            $dataArr['reference_id'] = $id;
            $adminRow = $this->AdminUserModel->getUserReferencesByRow($dataArr);
        }
        $viewArray = array(
            'adminRow' => $adminRow,
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,

        );

        $viewArray['Menu_select'] = 'user_accounts';
        $viewArray['Sub_menu_select'] = 'users/userreferences';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function sendNotificationAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        //echo "<pre>".$action."</pre>";
        $id = $this->params()->fromQuery('id', 0);
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('userreferences');
        }
        if (isset($_POST['subAddBtn']) || isset($_POST['subEditBtn'])) {
            //print_r($_POST);die;
            $insData = array('notification_type' => $_POST['notification_type'],
                'user_id' => $_POST['userID'],
                'notification_message' => $_POST['notification'],
                'notification_added' => date('Y-m-d H:i:s'));
            $addNewNote = $this->AdminUserModel->addEditNotifications($insData, $_POST['id'], $_POST['action']);
            if ($_POST['action'] == 'add') {
                $message = 'Notification added  successfully.';
            } else {
                $message = 'Notification updated  successfully.';
            }
            if ($addNewNote) {
                $this->flashMessenger()->addMessage(array('alert-success' => $message));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update notification. Please try again'));
            }
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toURL('notificationlist');
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;

        if ($action == 'view' || $action == 'edit') {
            $adminRow = $this->AdminUserModel->userNotificationsById($id);
            //print_r($adminRow);die;
        } else {
            $dataArr['reference_id'] = $id;
            $adminRow = $this->AdminUserModel->getUserReferencesByRow($dataArr);
        }
        $viewArray = array(
            'adminRow' => $adminRow,
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,

        );

        $viewArray['Menu_select'] = 'user_notifications';
        $viewArray['Sub_menu_select'] = 'users/sendnotification';
        $viewArray['list'] = $this->AdminUserModel->getUserByEmailOrName($key = false);
        $arrAdminAccounts = $this->AdminUserModel->userNotifications($filterData = array());
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function getLatestNotificationsAction()
    {
        $res = $this->AdminUserModel->getLatestNotifications();
        if (!empty($res)) {
            $res['count'] = count($res);
        }
        echo json_encode($res);
        exit;
    }

    public function notificationlistAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if ($this->params()->fromPost('doAction')) {
            $doAction = $this->params()->fromPost('doAction');
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('useraccounts');
            }
            $status = $this->AdminUserModel->doUserActions($doAction, $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('useraccounts');
        } else if ($this->params()->fromQuery('doAction') && $this->params()->fromQuery('id')) {
            $doAction = $this->params()->fromQuery('doAction');
            $id = $this->params()->fromQuery('id');
            $val = $this->params()->fromQuery('val');
            $uId = $this->params()->fromQuery('uId');
            $listUrl = $this->params()->fromQuery('listUrl');
            $status = $this->AdminUserModel->doUserActions($doAction, $id, $val);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $uId . '#trust_verification',));
        }
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        //$arrAdminAccounts = $this->AdminUserModel->userAccouts( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        $arrAdminAccounts = $this->AdminUserModel->userNotifications($filterData);
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'user_notifications'],
            ['Sub_menu_select' => 'users/notificationlist']
        ));
    }

    public function travellerenqlistmanagerAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        //echo "<pre>".$action."</pre>";
        $id = $this->params()->fromQuery('id', 0);
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('userreferences');
        }
        if (isset($_POST['subAddBtn']) || isset($_POST['subEditBtn'])) {
            //print_r($_POST);die;
            $insData = array(
                'recevier_id' => $_POST['receiver_email'],
                'message' => $_POST['message'],
                'sender_id' => $_POST['sender_email'],
                'create_date' => date('Y-m-d H:i:s'));
            $addNewNote = $this->AdminUserModel->addEditTravellerenq($insData, $_POST['id'], $_POST['action']);
            if ($_POST['action'] == 'add') {
                $message = 'Traveller Enquiries List added  successfully.';
            } else {
                $message = 'Traveller Enquiries List updated  successfully.';
            }
            if ($addNewNote) {
                $this->flashMessenger()->addMessage(array('alert-success' => $message));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update notification. Please try again'));
            }
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toURL('travellerenqlist');
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;

        if ($action == 'view' || $action == 'edit') {
            $adminRow = $this->AdminUserModel->travellerlistAccoutsById($id);
            $adminRowSender = $this->AdminUserModel->travellerlistAccoutsBySenderId($id);
            //print_r($adminRow);die;
        } else {
            $dataArr['reference_id'] = $id;
            $adminRow = $this->AdminUserModel->getUserReferencesByRow($dataArr);
        }
        $viewArray = array(
            'adminRow' => $adminRow,
            'adminRowSender' => $adminRowSender,
            'flashMessages' => $flashMessages,
            'userlist' => $this->AdminUserModel->getUserRowAll(),
            'action' => $action,
            'id' => $id,

        );

        $viewArray['Menu_select'] = 'user_notifications';
        $viewArray['Sub_menu_select'] = 'users/travellerenqlist';
        $viewArray['list'] = $this->AdminUserModel->getUserByEmailOrName($key);
        $arrAdminAccounts = $this->AdminUserModel->userNotifications($filterData);
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function seekerenqlistmanagerAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        //echo "<pre>".$action."</pre>";
        $id = $this->params()->fromQuery('id', 0);
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('userreferences');
        }
        if (isset($_POST['subAddBtn']) || isset($_POST['subEditBtn'])) {
            //print_r($_POST);die;
            $insData = array(
                'recevier_id' => $_POST['receiver_email'],
                'message' => $_POST['message'],
                'sender_id' => $_POST['sender_email'],
                'create_date' => date('Y-m-d H:i:s'));
            $addNewNote = $this->AdminUserModel->addEditSeekerenq($insData, $_POST['id'], $_POST['action']);
            if ($_POST['action'] == 'add') {
                $message = 'Seeker Enquiries List added  successfully.';
            } else {
                $message = 'Seeker Enquiries List updated  successfully.';
            }
            if ($addNewNote) {
                $this->flashMessenger()->addMessage(array('alert-success' => $message));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update notification. Please try again'));
            }
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toURL('seekerenqlist');
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;

        if ($action == 'view' || $action == 'edit') {
            $adminRow = $this->AdminUserModel->seekerlistAccoutsById($id);
            $adminRowSender = $this->AdminUserModel->seekerlistAccoutsByReceiverId($id);
            //print_r($adminRow);die;
        } else {
            $dataArr['reference_id'] = $id;
            $adminRow = $this->AdminUserModel->getUserReferencesByRow($dataArr);
        }
        $viewArray = array(
            'adminRow' => $adminRow,
            'adminRowSender' => $adminRowSender,
            'flashMessages' => $flashMessages,
            'userlist' => $this->AdminUserModel->getUserRowAll(),
            'action' => $action,
            'id' => $id,

        );

        $viewArray['Menu_select'] = 'user_notifications';
        $viewArray['Sub_menu_select'] = 'users/seekerenqlist';
        $viewArray['list'] = $this->AdminUserModel->getUserByEmailOrName($key);
        $arrAdminAccounts = $this->AdminUserModel->userNotifications($filterData);
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function reviewlistmanagerAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }

        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        //echo "<pre>".$action."</pre>";
        $id = $this->params()->fromQuery('id', 0);
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('userreferences');
        }
        if (isset($_POST['subAddBtn']) || isset($_POST['subEditBtn'])) {
            // print_r($_POST);
            // die;
            $insData = array(
                'id' => $_POST['id'],
                'recevier_id' => $_POST['receiver_email'],
                'sender_id' => $_POST['sender_email'],
                'message' => $_POST['message'],
                'rating' => $_POST['rating'],
                'create_by' => $_POST['sender_email'],
                'trip_id' => $_POST['trip_id'],
                'create_date' => date('Y-m-d H:i:s'));
            $addNewNote = $this->AdminUserModel->addEditReviewlist($insData, $_POST['id'], $_POST['action']);
            if ($_POST['action'] == 'add') {
                $message = 'Seeker Enquiries List added  successfully.';
            } else {
                $message = 'Seeker Enquiries List updated  successfully.';
            }
            if ($addNewNote) {
                $this->flashMessenger()->addMessage(array('alert-success' => $message));
            } else {
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update data. Please try again'));
            }
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toURL('userreviewslist');
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;

        if ($action == 'view' || $action == 'edit') {
            $adminRow = $this->AdminUserModel->reviewslistAccoutsById($id);
            $adminRow['reviewdetailsArr'] = $this->AdminUserModel->reviewDetailsByRId($id);
            $adminRowSender = $this->AdminUserModel->seekerlistAccoutsByReceiverId($id);
            //print_r($adminRowSender);die;
        } else {
            $dataArr['RID'] = $id;
            $adminRow = $this->AdminUserModel->getUserReferencesByRow($dataArr);
        }
        $viewArray = array(
            'adminRow' => $adminRow,
            'adminRowSender' => $adminRowSender,
            'flashMessages' => $flashMessages,
            'userlist' => $this->AdminUserModel->getUserRowAll(),
            'triplist' => $this->AdminUserModel->getUserTripsAll(),
            'action' => $action,
            'id' => $id,

        );
        $viewArray['Menu_select'] = 'user_accounts';
        $viewArray['Sub_menu_select'] = 'userreviewslist';
        // $viewArray['list'] = $this->AdminUserModel->getUserByEmailOrName($key);
        // $arrAdminAccounts = $this->AdminUserModel->userNotifications( $filterData);
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

 
    public function couponsAction()
    { 
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        /*print_r($action);die; */
        $id = $this->params()->fromQuery('id', 0);
        if ($this->params()->fromPost('rejectBtn')) {
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('useraccounts');
            }
            $identity_id = $this->params()->fromPost('identity_id');
            $status = $this->AdminUserModel->doUserActions('reject', $identity_id, $this->params()->fromPost('rejectReason'));
            $identityRow = $this->AdminUserModel->userIdentityRow($identity_id);
            $mailRow['first_name'] = $identityRow['first_name'];
            $mailRow['email_address'] = $identityRow['email_address'];
            $mailRow['identity_name'] = $identityRow['identity_name'];
            $mailRow['rejectReason'] = $this->params()->fromPost('rejectReason');
            $this->AdminMailModel->sendVerificationRejectMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $id . '#tv',));
        }
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('useraccounts');
        }
        if (isset($_POST['subAdminBtn'])) {
            $checkCoupon = $this->AdminUserModel->checkCouponExists($this->params()->fromPost('coupon_code'));
            if ($checkCoupon === false) {
                $insData['coupon_code'] = $this->params()->fromPost('coupon_code');
                $insData['offer_percentage'] = $this->params()->fromPost('offer_percentage');
                $insData['status'] = $this->params()->fromPost('status');
                $insData['user_type'] = $this->params()->fromPost('user_type');
                $insData['expires'] = $this->params()->fromPost('expires');
              //  die;
              //  if ($action == 'add') $insData['active'] = 1;
               

                $updateCoupon = $this->AdminUserModel->addEditCoupons($insData, $this->params()->fromPost('id'), $this->params()->fromPost('action'));
                $message = ($action == 'add') ? 'Coupon added successfully' : 'Coupon updated  successfully';
                if ($updateCoupon) {
                    $this->flashMessenger()->addMessage(array('alert-success' => $message));
                } else {
                    $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update coupon. Please try again'));
                }
                $flashMessages = $this->flashMessenger()->getMessages();
                return $this->redirect()->toRoute('couponmanager');
            } else {
                $flashMessages[] = array('alert-danger' => 'Coupon Already Exist');
            }
        }
        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $viewArray = array(
            'adminRow' => $this->AdminUserModel->getCouponRow($id),

            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,
            'lang' => $this->AdminModel->getLanguages(),
            'countries' => $this->AdminModel->getCountries(),
            'AdminUserModel' => $this->AdminUserModel,
            'site_time_zones' => $this->timeZones
        );

        if ($action == 'view') {
            
            $CURRENT_USER_ID = $id;
            $api = new \OAuth2\Client($app_key, $app_secret);
            

            $current_user_id = $id;
         $viewArray['userInvites'] = $this->AdminUserModel->getUserInviteDetails($id);
            $viewArray['userNotification']          = $this->AdminUserModel->userNotifications($filterData = array(), $id);
            $viewArray['userAdminNotification']     = $this->AdminUserModel->adminNotificationsByUserId($id);
            $viewArray['userSentNotification']      = $this->AdminUserModel->adminSentNotificationsByUserId($id);
            $viewArray['userTravellerEnquiries']    = $this->AdminUserModel->adminTravellerEnquiriesByUserId($id);
            $viewArray['userSeekerEnquiries']       = $this->AdminUserModel->adminSeekerEnquiriesByUserId($id);
            $viewArray['userDisputes']              = $this->AdminUserModel->adminDisputesByUserId($id);
        }

        $viewArray['Menu_select'] = 'masterdata';
        if ($action == 'add')
            $viewArray['Sub_menu_select'] = 'coupons';
        else $viewArray['Sub_menu_select'] = 'coupons';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);


        return $viewModel;
    }


     public function couponmanagerAction()
    { 
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }

        $filterData = array();

        if ($this->params()->fromPost('doAction')) { 
            $doAction = $this->params()->fromPost('doAction');
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('couponmanager');
            }
           $status = $this->AdminUserModel->doCouponActions($doAction, $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('couponmanager');
        } else if ($this->params()->fromQuery('doAction') && $this->params()->fromQuery('id')) {
            $doAction = $this->params()->fromQuery('doAction');
            $id = $this->params()->fromQuery('id');
            $val = $this->params()->fromQuery('val');
            $uId = $this->params()->fromQuery('uId');
            $listUrl = $this->params()->fromQuery('listUrl');
            $status = $this->AdminUserModel->doCouponActions($doAction, $id, $val);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            return $this->redirect()->toRoute('usermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $uId . '#trust_verification',));
        }


        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');

        $data = array();
        //$arrAdminAccounts = $this->AdminUserModel->userAccouts( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        		
		$arrCoupons = $this->AdminUserModel->Couponlist($filterData);
		
		$data = !empty($arrCoupons) ? $arrCoupons : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);


       return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'masterdata'],
            ['Sub_menu_select' => 'couponmanager']
        ));
    }

    public function LoginAsUserAction()
    {
        $secure     = explode('|', base64_decode($_REQUEST['secure']));
        $user = $this->AdminUserModel->loginAsUser($secure[0], $secure[1]);

        $this->sessionObj->offsetSet('userid', $user['ID']);
        $this->sessionObj->offsetSet('user', $user);
        $this->sessionObj->offsetSet('need_user_profile', '');

        $this->basePath = $this->getEvent()->getRouter()->assemble(array(), array('name' => 'home', 'force_canonical' => true));

        return $this->redirect()->toUrl($this->basePath .'dashboard');
    }

}

?>