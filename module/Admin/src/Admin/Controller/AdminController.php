<?php namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager; 
use Admin\Model\AdminModel;
use Zend\Mvc\Plugin\FlashMessenger;

class AdminController extends AbstractActionController
{
     public function __construct($data = false){
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
         
    }
    
    public function indexAction(){
        if($this->sessionObj->offsetGet('ADMIN_ID') != ''){
             return $this->redirect()->toRoute('admin_dashboard');
        }
          if(isset($_POST['adminLoginName'])){
            $adminLoginName = $_POST['adminLoginName'];
            $adminLoginPass = $_POST['adminLoginPass'];
            $adminRow =  $this->AdminModel->checkAdmin($adminLoginName,$adminLoginPass);
            if( $adminRow){
                     $this->sessionObj->offsetSet('ADMIN_ID',$adminRow['ID']);
                     $first_name = strtoupper($adminRow['first_name']);                      
                     $this->sessionObj->offsetSet('ADMIN_NAME', $first_name );
                     $this->sessionObj->offsetSet('ADMIN_MODULES', $adminRow['accessModules'] );
                     $this->sessionObj->offsetSet('ADMIN_PRIVILEGES', $adminRow['accessPrivilege'] );
                     $this->sessionObj->offsetSet('ADMIN_ACCESS', $adminRow['adminAccess'] );
                     
                     return $this->redirect()->toRoute('admin_dashboard');
             }else{
                $this->flashMessenger()->addMessage(array('alert-danger' => 'Invalid username or password'));
                return $this->redirect()->toRoute('admin');
            }
        }
         $viewArray = array(
            'flashMessages' =>  $this->flashMessenger()->getMessages(),
        );
        $this->layout()->setVariable('pageType', 'login');
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);

        return $viewModel;  
        //$flashMessage = $this->flashMessenger()->getMessages();
        
    }
    
    public function logoutAction(){
        if ( $this->sessionObj->offsetGet('ADMIN_ID')) {
            $this->sessionObj->offsetSet('ADMIN_ID','');
            $this->sessionObj->offsetSet('ADMIN_NAME','');
 	}
        return $this->redirect()->toRoute('admin');
    }   
}
?>