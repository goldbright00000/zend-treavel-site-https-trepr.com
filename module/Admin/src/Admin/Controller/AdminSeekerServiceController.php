<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager;
use Admin\Model\AdminUserModel;
use Zend\Mvc\Plugin\FlashMessenger;
use Zend\Mvc\Plugin\Url;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;

require_once(ACTUAL_ROOTPATH . "Upload/src/Upload.php");

class AdminSeekerServiceController extends AbstractActionController
{
    const ITEM_PER_PAGE = 20;
    const ADDR_ITEM_PER_PAGE = 1;

    public function __construct($data = false)
    {
        /*Loading models from factory
         * Call the model object using it's class name
         */

        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->adminModules = $data['configs']['adminModules'];
        $this->adminMenus = $data['configs']['adminMenus'];
        $this->timeZones = $data['configs']['timezones'];
        //$this->AdminModel->authenticateAdmin('user_accounts');
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            // return $this->redirect()->toRoute('admin');
        }

    }

    public function indexAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $data = array();

        $arrAdminAccounts = $this->AdminUserModel->userAccouts($filterData);
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_service'],
            ['Sub_menu_select' => 'seekers']
        ));
    }

    public function authenticateModule()
    {
        $resp = $this->AdminModel->authenticateAdmin('admin_seekers');
        if ($resp) {
            $this->flashMessenger()->addMessage($resp['msg']);
            return $this->redirect()->toRoute($resp['redirectUrl']);
        }
    }

    public function seekerPeopleServiceAction()
    {

        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'seeker';
        $data = array();

        $arrTrips = $this->AdminSeekerModel->getSeekerPeopleList($filterData);


        $data = !empty($arrTrips) ? $arrTrips : array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_service'],
            ['Sub_menu_select' => 'seeker/people']
        ));


    }

    public function seekerPackageServiceAction()
    {

        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'seeker';
        $data = array();
        $arrTrips = $this->AdminSeekerModel->getSeekerPackagesList($filterData);
        $data = !empty($arrTrips) ? $arrTrips : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_service'],
            ['Sub_menu_select' => 'seeker/package']
        ));
    }

    public function seekerProjectServiceAction()
    {

        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'seeker';
        $data = array();

        $arrTrips = $this->AdminSeekerModel->getSeekerProjectList($filterData);


        $data = !empty($arrTrips) ? $arrTrips : array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_service'],
            ['Sub_menu_select' => 'seeker/project']
        ));


    }

    public function seekerWishListAction()
    {

        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'seeker';
        $data = array();

        $arrTrips = $this->AdminSeekerModel->getSeekerWishList($filterData);
        // print_r($arrTrips);
        // die;


        $data = !empty($arrTrips) ? $arrTrips : array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);

        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_service'],
            ['Sub_menu_select' => 'seeker/seekerwishlist']
        ));


    }

    public function seekerservicermanagerAction1()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        $usertype = $this->params()->fromQuery('usertype', 'traveller');
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('useraccounts');
        }


        $trip = $this->AdminSeekerModel->getTripRow($id, $usertype);

        $trip_people = $this->AdminSeekerModel->getTravellerPeople($trip['ID']);
        $trip_package = $this->AdminSeekerModel->getTravellerPackage($trip['ID']);
        $trip_project = $this->AdminSeekerModel->getTravellerProject($trip['ID']);
        $trip_location = $this->AdminSeekerModel->getUserLocationById($trip['user_location']);
        $airport_details = $this->AdminSeekerModel->getFlightsDetails($trip['ID']);

        $trip_people = isset($trip_people) ? $trip_people : false;
        $trip_package = isset($trip_package) ? $trip_package : false;
        $trip_project = isset($trip_project) ? $trip_project : false;
        $airport_details = isset($airport_details) ? $airport_details : false;

        $service = '';
        if ($trip_people['ID']) {
            $service = 'People';
            //$trip_people_passangers = $this->AdminSeekerModel->getSeekerPeoplePassangers($trip_people['ID']);
        }
        $trip_people_passangers = isset($trip_people_passangers) ? $trip_people_passangers : false;
        if ($trip_package['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Package';
        }

        if ($trip_project['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Product';
        }
        $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
        $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);

        $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
        $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
        $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
        $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
        $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
        $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';

        $arr_trips = array(
            'trip_id_number' => $trip['trip_id_number'],
            'ID' => $trip['ID'],
            'name' => $trip['name'],
            'rejected_reason' => $trip['rejected_reason'],
            'noofstops' => $trip['noofstops'],
            'ticket_image' => $trip['ticket_image'],
            'trip_status' => $trip['trip_status'],
            'first_name' => $trip['first_name'],
            'email_address' => $trip['email_address'],
            'origin_location' => $trip['origin_location'],
            'destination_location' => $trip['destination_location'],
            'origin_location_code' => $origin_location_code,
            'destination_location_code' => $destination_location_code,
            'origin_location_city' => $origin_location_city,
            'destination_location_city' => $destination_location_city,
            'origin_location_country' => $origin_location_country,
            'destination_location_country' => $destination_location_country,
            'departure_date' => $trip['departure_date'],
            'arrival_date' => $trip['arrival_date'],
            'cabin' => $trip['cabin'],
            'booking_status' => $trip['booking_status'],
            'service' => $service,
            'trip_people' => $trip_people,
            'trip_package' => $trip_package,
            'trip_project' => $trip_project,
            'trip_location' => $trip_location,
            'trip_people_passangers' => $trip_people_passangers,
            'airport_details' => $airport_details,
            'modified' => $trip['modified']
        );

        $viewArray = array(
            'adminRow' => $arr_trips,
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );


        $viewArray['Menu_select'] = 'seeker_service';
        $viewArray['Sub_menu_select'] = '';


        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        //return $viewModel;

        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'seeker';
        $data = array();

        $arrTrips = $this->AdminSeekerModel->getSeekerWishList($filterData);
        // print_r($arrTrips);
        // die;


        $data = !empty($arrTrips) ? $arrTrips : array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);

        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_service'],
            ['Sub_menu_select' => 'seeker/seekerwishlist']
        ));
    }

    public function seekerservicermanagerAction()
    {
        $this->authenticateModule();

        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        $people_service_id = $this->params()->fromQuery('people_service_id', 0);
        $package_service_id = $this->params()->fromQuery('package_service_id', 0);
        $project_service_id = $this->params()->fromQuery('project_service_id', 0);
        $usertype = $this->params()->fromQuery('usertype', 'seeker');
        if ($this->params()->fromPost('rejectBtn')) {
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('useraccounts');
            }
        }
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('useraccounts');
        }

        if ($_POST) {

            $this->AdminCommonPlugin = $this->plugin('AdminCommonPlugin');
            $user_location = $this->AdminCommonPlugin->addTripOrginLocation();
            $trip_ID = $this->AdminCommonPlugin->addTripDetail($user_location);
            if (isset($people_service_id) && !empty($people_service_id)) {

                $reqId = $this->AdminCommonPlugin->addSeekerPeopleService($trip_ID);
                $image_path = "tickets";
                $field_name = "ticket_image";
                $file_name_prefix = $this->AdminModel->cleanQuery($_POST['tripIdNumber']) . '_' . time();
                // $uploadResult = $this->AdminModel->uploadFiles( $field_name,$file_name_prefix,$image_path );

                if ($uploadResult['result'] == 'success') {
                    $ticket_file = $uploadResult['fileName'];
                    $arr_ticket_detail = array(
                        'ID' => $trip_ID,
                        'ticket_image' => $this->AdminModel->cleanQuery($ticket_file),
                    );
                    $this->AdminSeekerModel->updateSeekerTrip($arr_ticket_detail, $trip_ID);
                }

                $this->flashMessenger()->addMessage(array('alert-success' => "People service trip has been updated successfully!"));
                return $this->redirect()->toRoute('admin-seeker-people');

            }

            if (isset($package_service_id) && !empty($package_service_id)) {
                $this->AdminCommonPlugin->addSeekerPackageService($trip_ID);
                $this->flashMessenger()->addMessage(array('alert-success' => "Package service trip has been updated successfully!"));
                return $this->redirect()->toRoute('admin-seeker-package');


            }

            if (isset($project_service_id) && !empty($project_service_id)) {
                $this->AdminCommonPlugin->addSeekerProjectService($trip_ID);
                $this->flashMessenger()->addMessage(array('alert-success' => "Product service trip has been updated successfully!"));
                return $this->redirect()->toRoute('admin-seeker-project');


            }


        }
        $trip = $this->AdminSeekerModel->getTripRow($id, $usertype);

        $trip_people = $this->AdminSeekerModel->getSeekerPeople($trip['ID']);
        $trip_package = $this->AdminSeekerModel->getSeekerPackage($trip['ID']);
        $trip_project = $this->AdminSeekerModel->getSeekerProject($trip['ID']);
        $trip_location = $this->AdminSeekerModel->getUserLocationById($trip['user_location']);
        $airport_details = $this->AdminSeekerModel->getFlightsDetails($trip['ID']);

        $trip_people = isset($trip_people) ? $trip_people : false;
        $trip_package = isset($trip_package) ? $trip_package : false;
        $trip_project = isset($trip_project) ? $trip_project : false;
        $airport_details = isset($airport_details) ? $airport_details : false;

        $service = $type_of_service = '';
        if ($trip_people['ID']) {
            $service = 'People';
            $type_of_service = 'people';
            $trip_people_passangers = $this->AdminSeekerModel->getSeekerPeoplePassangers($trip_people['ID']);
            if ($trip_people_passangers) {
                $passengerAttach = false;
                foreach ($trip_people_passangers as $key => $eachPass) {
                    $trip_people_passangersAttach = $this->AdminSeekerModel->getSeekerPeoplePassangerAttachments($eachPass['ID']);
                    if ($trip_people_passangersAttach) {
                        $trip_people_passangers[$key]['attachmentDetails'] = $trip_people_passangersAttach;
                    }
                }
            }
			
			$requests = $this->AdminSeekerModel->getSeekerRequests($trip_people['ID'],$type_of_service);
			$travellerRequests = $this->AdminSeekerModel->getSeekerRequestsByTraveler($trip['ID'],$type_of_service);
        }
        $trip_people_passangers = isset($trip_people_passangers) ? $trip_people_passangers : false;

        if ($trip_package['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Package';
            $type_of_service = 'package';
            $trip_package_packages = $this->AdminSeekerModel->getSeekerPackagePackages($trip_package['ID']);
			
			$requests = $this->AdminSeekerModel->getSeekerRequests($trip_package['ID'],$type_of_service);
			$travellerRequests = $this->AdminSeekerModel->getSeekerRequestsByTraveler($trip['ID'],$type_of_service);
        }
        $trip_package_packages = isset($trip_package_packages) ? $trip_package_packages : false;

        if ($trip_project['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Product';
            $type_of_service = 'project';
			$trip_project_tasks = $this->AdminSeekerModel->getSeekerProjectTasks($trip_project['ID']);
			
			$requests = $this->AdminSeekerModel->getSeekerRequests($trip_project['ID'],$type_of_service);
			$travellerRequests = $this->AdminSeekerModel->getSeekerRequestsByTraveler($trip['ID'],$type_of_service);
        }
        $trip_project_tasks = isset($trip_project_tasks) ? $trip_project_tasks : false;

        $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
        $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);
        $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
        $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
        $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
        $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
        $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
        $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
        $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
        $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';

        $distancePrice = $countryPrice = $stopsPrice = 0;
        $passengerCount = (!empty($trip_people)) ? $trip_people['passengers'] : 1;

        $departureAirport = $this->AdminModel->getAirPortByAirportCode($origin_location_code);
        $arrivalAirport = $this->AdminModel->getAirPortByAirportCode($destination_location_code);

        $countryPriceList = $this->AdminModel->getCountryPrice($departureAirport['country_code'], $arrivalAirport['country_code'], $type_of_service);

        if (isset($trip['noofstops'])) {
            $noofstops = $trip['noofstops'];
        }
        if (isset($trip['distance'])) {
            $distance = (int) $trip['distance'];
        }
        if (!empty($countryPriceList)) {
            $countryPrice = $countryPriceList['country_price'];
        }
        if (isset($noofstops) && $noofstops != '') {
            $noofstops         = ($noofstops == 0) ? 'direct' : $noofstops;
            $arrNoofStopsPrice = $this->AdminModel->getNoofstopsPrice($noofstops);
            if (!empty($arrNoofStopsPrice)) {
                $stopsPrice = $arrNoofStopsPrice['stop_price'];
            }
        }
        if (!empty($distance)) {
            $arrDistancePrice = $this->AdminModel->getDistancePrice($distance, $type_of_service);
            if (!empty($arrDistancePrice)) {
                $distancePrice = $arrDistancePrice['distance_price'];
            }
        }

        $sub_total = 0;
        if($type_of_service == 'people')
        {
            $service_fees = (float)($distancePrice + $countryPrice + $stopsPrice);
            $sub_total = $service_fees;

            $service_fees = $service_fees * $passengerCount;
        }
        else if($type_of_service == 'package')
        {
            for ($i = 0; $i < count($trip_package_packages); $i++)
            {
                $tot_prc = $this->AdminModel->getItemCategoryPrice($trip_package_packages[$i]['item_category'] . $trip_package_packages[$i]['item_sub_category'], $type_of_service);

                if(!empty($trip_package_packages[$i]['weight'])){
                    $item_weight = $trip_package_packages[$i]['weight'];
                }
                else{
                    $item_weight = '0.5';
                }

                $tot_weight_prc = $this->AdminModel->getWeightPrice($item_weight, $type_of_service);

                $money = $this->AdminModel->convertCurrency($trip_package_packages[$i]['item_worth'], $trip_package_packages[$i]['item_worth_currency'], 'GBP');
                $itemPricePrice = $this->AdminModel->getItemPricePrice($money, $type_of_service);

                $item_sub_total = (float)($countryPrice + $distancePrice + ($tot_prc['tot_item_price'] * $tot_weight_prc['weight_carried_price']) + $itemPricePrice['price_item_price']);

                $trip_package_packages[$i]['item_category_price']   = $tot_prc;
                $trip_package_packages[$i]['item_weight_price']     = $tot_weight_prc;
                $trip_package_packages[$i]['item_price_price']      = $itemPricePrice;
                $trip_package_packages[$i]['item_sub_total']        = $item_sub_total;
                $sub_total = $sub_total + $item_sub_total;
            }

            $service_fees = $trip_package['package_service_fee'];
        }
		else if($type_of_service == 'project')
        {
            for ($i = 0; $i < count($trip_project_tasks); $i++)
            {
                $tot_prc = $this->AdminModel->getItemCategoryPrice($trip_project_tasks[$i]['category'] . $trip_project_tasks[$i]['additional_requirements_category'], $type_of_service);

                if(!empty($trip_project_tasks[$i]['item_weight'])){
                    $item_weight = $trip_project_tasks[$i]['item_weight'];
                }
                else{
                    $item_weight = '0.5';
                }

                $tot_weight_prc = $this->AdminModel->getWeightPrice($item_weight, $type_of_service);

                $money = $this->AdminModel->convertCurrency($trip_project_tasks[$i]['price'], $trip_project_tasks[$i]['price_currency'], 'GBP');
                $itemPricePrice = $this->AdminModel->getItemPricePrice($money, $type_of_service);

                $item_sub_total = (float)($countryPrice + $distancePrice + ($tot_prc['tot_item_price'] * $tot_weight_prc['weight_carried_price']) + $itemPricePrice['price_item_price'] + $money + (($money/100)*5));

                $trip_project_tasks[$i]['item_category_price']   = $tot_prc;
                $trip_project_tasks[$i]['item_weight_price']     = $tot_weight_prc;
                $trip_project_tasks[$i]['item_price_price']      = $itemPricePrice;
                $trip_project_tasks[$i]['item_sub_total']        = $item_sub_total;
                $sub_total = $sub_total + $item_sub_total;
            }

            $service_fees = $trip_package['project_service_fee'];
        }
		
		foreach($requests as $key=>$req){
			$requests[$key]['trip'] = $this->AdminSeekerModel->getTripRow($req['trip'], 'traveller');
			$requests[$key]['service_type'] = $type_of_service;
			if($req['approved'] == '0'){
				$requests[$key]['status'] = 'Pending';
			} else if($req['approved'] == '1'){
				$requests[$key]['status'] = 'Accepted';
			} else if($req['approved'] == '2' || $req['approved'] == '5'){
				$requests[$key]['status'] = 'Declined';
			} else if($req['approved'] == '3'){
				$requests[$key]['status'] = 'Expired';
			} else if($req['approved'] == '4'){
				$requests[$key]['status'] = 'Wishlisted';
			}
		}
		foreach($travellerRequests as $key=>$req){
			if($type_of_service == 'people'){
				$sId = $req['people_request'];
			} else if($type_of_service == 'package'){
				$sId = $req['package_request'];
			} else if($type_of_service == 'project'){
				$sId = $req['project_request'];
			}
			$sInfo = $this->AdminSeekerModel->getTrvellerServiceByRequest($sId,$type_of_service);
			if($req['approved'] == '0'){
				$travellerRequests[$key]['status'] = 'Pending';
			} else if($req['approved'] == '1'){
				$travellerRequests[$key]['status'] = 'Accepted';
			} else if($req['approved'] == '2' || $req['approved'] == '5'){
				$travellerRequests[$key]['status'] = 'Declined';
			} else if($req['approved'] == '3'){
				$travellerRequests[$key]['status'] = 'Expired';
			} else if($req['approved'] == '4'){
				$travellerRequests[$key]['status'] = 'Wishlisted';
			}
			if(count($sInfo)){
				$travellerRequests[$key]['trip'] = $this->AdminSeekerModel->getTripRow($sInfo['trip'], 'traveller');
				$travellerRequests[$key]['service_type'] = $type_of_service;
			}
		}


        $arr_trips = array(
            'trip_id_number' => $trip['trip_id_number'],
            'ID' => $trip['ID'],
            'name' => $trip['name'],
            'noofstops' => $trip['noofstops'],
            'distance' => $trip['distance'],
            'first_name' => $trip['first_name'],
            'email_address' => $trip['email_address'],
            'origin_location' => $trip['origin_location'],
            'destination_location' => $trip['destination_location'],
            'origin_location_code' => $origin_location_code,
            'destination_location_code' => $destination_location_code,
            'origin_location_city' => $origin_location_city,
            'destination_location_city' => $destination_location_city,
            'origin_location_country' => $origin_location_country,
            'destination_location_country' => $destination_location_country,
            'departure_date' => $trip['departure_date'],
            'arrival_date' => $trip['arrival_date'],
            'cabin' => $trip['cabin'],
            'booking_status' => $trip['booking_status'],
            'service' => $service,
            'trip_people' => $trip_people,
            'trip_package' => $trip_package,
            'trip_project' => $trip_project,
            'trip_location' => $trip_location,
            'trip_people_passangers' => $trip_people_passangers,
            'trip_package_packages' => $trip_package_packages,
			'trip_project_tasks' => $trip_project_tasks,
            'airport_details' => $airport_details,
            'countryPrice' => $countryPrice,
            'stopsPrice' => $stopsPrice,
            'distancePrice' => $distancePrice,
            'passengerCount' => $passengerCount,
            'service_fees' => $service_fees,
            'sub_total' => $sub_total,
            'total_cost_currency' => $trip['total_cost_currency'],
            'conversion_rate' => $trip['conversion_rate'],
            'total_cost' => $trip['total_cost'],
            'modified' => $trip['modified'],
			'travellerRequests'=>$travellerRequests,
			'requests'=>$requests
        );

        $viewArray = array(
            'trip' => $trip,
            'adminRow' => $arr_trips,
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,
            'people_service_id' => $people_service_id,
            'package_service_id' => $package_service_id,
            'project_service_id' => $project_service_id,
            'site_time_zones' => $this->timeZones
        );
        if ($action == 'edit') {

            $viewArray['origin_location'] = $origin_location_city . ', ' . $origin_location_country . ', ' . $origin_location_name;
            $viewArray['destination_location'] = $destination_location_city . ', ' . $destination_location_country . ', ' . $destination_location_name;
            $viewArray['origin_location_code'] = isset($origin_location['code']) ? $origin_location['code'] : '';
            $viewArray['destination_location_code'] = isset($destination_location['code']) ? $destination_location['code'] : '';
            $viewArray['selectedFlightData'] = !empty($viewArray['airport_details']) ? htmlspecialchars(json_encode($viewArray['airport_details'])) : '';
            $viewArray['user_locations'] = $this->AdminSeekerModel->getUserLocations($trip['user']);

            if (isset($people_service_id) && !empty($people_service_id)) {
                $trip_seeker_details = $this->AdminSeekerModel->getSeekerTripPeople($trip['ID']);
                $trip_seeker_passenger_details = $this->AdminSeekerModel->getSeekerTripPeoplePassengers($trip_seeker_details['ID']);
                if ($trip_seeker_passenger_details) {
                    foreach ($trip_seeker_passenger_details as $key => $passenger) {
                        $pass_att = $this->AdminSeekerModel->getSeekerTripPeoplePassengersAttachment($passenger['ID']);
                        if ($pass_att) {
                            $trip_seeker_passenger_details[$key]['attachments'] = $pass_att;
                        }
                    }
                }

                $trip_seeker = isset($trip_seeker_details) ? $trip_seeker_details : false;
                $trip_seeker_passenger = isset($trip_seeker_passenger_details) ? $trip_seeker_passenger_details : false;

                $viewArray['trip_seeker'] = $trip_seeker;
                $viewArray['trip_seeker_passenger'] = $trip_seeker_passenger;

            }
            if (isset($package_service_id) && !empty($package_service_id)) {
                $viewArray['project_cats'] = $this->AdminSeekerModel->getPackageTasksCategory();
                $trip_seeker_details = $this->AdminSeekerModel->getSeekerTripPackage($trip['ID']);
                $trip_seeker_packages_details = $this->AdminSeekerModel->getSeekerTripPackagePackages($trip_seeker_details['ID']);
                if ($trip_seeker_packages_details) {
                    foreach ($trip_seeker_packages_details as $key => $package) {
                        $trip_seeker_packages_details[$key]['subCategory'] = $this->AdminSeekerModel->getProjectTasksCategorylisttype($package['item_category']);
                    }
                }

                $viewArray['trip_seeker_packages'] = $trip_seeker_packages_details;
                $viewArray['trip_seeker'] = $trip_seeker_details;
            }
            if (isset($project_service_id) && !empty($project_service_id)) {

                $trip_seeker_details = $this->AdminSeekerModel->getSeekerTripProject($trip['ID']);
                $trip_seeker_project_details = $this->AdminSeekerModel->getSeekerTripProjectTasks($trip_seeker_details['ID']);
                $tripWorkCategory = ($trip_seeker_details['work_category'] == 1) ? '1' : '0';
                $viewArray['project_tasks_categories_type'] = $this->AdminSeekerModel->getProjectTasksCategorytype($tripWorkCategory);
                if ($trip_seeker_project_details) {
                    foreach ($trip_seeker_project_details as $key => $project) {
                        $trip_seeker_project_details[$key]['subCategory'] = $this->AdminSeekerModel->getProjectTasksCategorylisttype($project['category']);
                        $pro_att = $this->AdminSeekerModel->getSeekerTripProjectTaskAttachment($project['project_task_id']);
                        if ($pro_att) {
                            $trip_seeker_project_details[$key]['attachments'] = $pro_att;
                        }
                    }
                }

                $trip_seeker = isset($trip_seeker_details) ? $trip_seeker_details : false;
                $trip_seeker_project = isset($trip_seeker_project_details) ? $trip_seeker_project_details : false;
                $viewArray['trip_seeker'] = $trip_seeker;
                $viewArray['trip_seeker_project'] = $trip_seeker_project;
            }

        }
        $viewArray['Menu_select'] = 'seeker_service';
        $viewArray['Sub_menu_select'] = '';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function travellermanagerAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        $usertype = $this->params()->fromQuery('usertype', 'traveller');
        if ($this->params()->fromPost('doAction')) {

            $ids = $this->params()->fromPost('ids');
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_traveller_service');
            }

            $status = $this->AdminSeekerModel->doTripActions($this->params()->fromPost('doAction'), $ids);
            $identityRow = $this->AdminSeekerModel->getTripRow($id, $usertype);
            $mailRow['first_name'] = $identityRow['first_name'];
            $mailRow['trip_id_number'] = $identityRow['trip_id_number'];
            $mailRow['email_address'] = $identityRow['email_address'];
            $mailRow['rejectReason'] = $this->params()->fromPost('rejectReason');

            $this->AdminMailModel->sendTripApprovedMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('travellermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $id));
        } else if ($this->params()->fromPost('rejectBtn')) {
            $trip_id = $this->params()->fromPost('trip_id');
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('usertrustverification');
            }
            $status = $this->AdminSeekerModel->doTripActions('reject', $trip_id, $this->params()->fromPost('rejectReason'));
            $identityRow = $this->AdminSeekerModel->getTripRow($trip_id, $usertype);
            $mailRow['first_name'] = $identityRow['first_name'];
            $mailRow['trip_id_number'] = $identityRow['trip_id_number'];
            $mailRow['email_address'] = $identityRow['email_address'];
            $mailRow['rejectReason'] = $this->params()->fromPost('rejectReason');

            $this->AdminMailModel->sendTripRejectMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('travellermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $trip_id));


        } else if ($this->params()->fromPost('moreInfoBtn')) {
            $trip_id = $this->params()->fromPost('trip_id');

            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('usertrustverification');
            }
            $mailRow['first_name'] = $this->params()->fromPost('firstname');
            $mailRow['trip_id_number'] = $this->params()->fromPost('trip_id_number');
            $mailRow['email_address'] = $this->params()->fromPost('email');
            $mailRow['moreInfo'] = $this->params()->fromPost('moreInfo');

            $this->AdminMailModel->sendTripRequestMoreInfoMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('travellermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $trip_id));


        }

        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('useraccounts');
        }

        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $trip = $this->AdminSeekerModel->getTripRow($id, $usertype);

        $trip_people = $this->AdminSeekerModel->getTravellerPeople($trip['ID']);
        $trip_package = $this->AdminSeekerModel->getTravellerPackage($trip['ID']);
        $trip_project = $this->AdminSeekerModel->getTravellerProject($trip['ID']);
        $trip_location = $this->AdminSeekerModel->getUserLocationById($trip['user_location']);
        $airport_details = $this->AdminSeekerModel->getFlightsDetails($trip['ID']);
        // print_r($airport_details);exit;
        $trip_people = isset($trip_people) ? $trip_people : false;
        $trip_package = isset($trip_package) ? $trip_package : false;
        $trip_project = isset($trip_project) ? $trip_project : false;
        $airport_details = isset($airport_details) ? $airport_details : false;

        $service = '';
        if ($trip_people['ID']) {
            $service = 'People';
            // $trip_people_passangers = $this->AdminSeekerModel->getSeekerPeoplePassangers($trip_people['ID']);
        }
        $trip_people_passangers = isset($trip_people_passangers) ? $trip_people_passangers : false;
        if ($trip_package['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Package';
        }

        if ($trip_project['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Product';
        }
        $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
        $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);

        $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
        $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
        $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
        $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
        $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
        $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
        $arr_trips = array(
            'trip_id_number' => $trip['trip_id_number'],
            'ID' => $trip['ID'],
            'name' => $trip['name'],
            'rejected_reason' => $trip['rejected_reason'],
            'noofstops' => $trip['noofstops'],
            'ticket_image' => $trip['ticket_image'],
            'trip_status' => $trip['trip_status'],
            'first_name' => $trip['first_name'],
            'email_address' => $trip['email_address'],
            'origin_location' => $trip['origin_location'],
            'destination_location' => $trip['destination_location'],
            'origin_location_code' => $origin_location_code,
            'destination_location_code' => $destination_location_code,
            'origin_location_city' => $origin_location_city,
            'destination_location_city' => $destination_location_city,
            'origin_location_country' => $origin_location_country,
            'destination_location_country' => $destination_location_country,
            'departure_date' => $trip['departure_date'],
            'arrival_date' => $trip['arrival_date'],
            'cabin' => $trip['cabin'],
            'booking_status' => $trip['booking_status'],
            'service' => $service,
            'trip_people' => $trip_people,
            'trip_package' => $trip_package,
            'trip_project' => $trip_project,
            'trip_location' => $trip_location,
            'trip_people_passangers' => $trip_people_passangers,
            'airport_details' => $airport_details,
            'modified' => $trip['modified']
        );
        // print_r($arr_trips);exit;
        $viewArray = array(
            'adminRow' => $arr_trips,
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );

        $viewArray['Menu_select'] = 'seeker_trips';
        $viewArray['Sub_menu_select'] = 'travellermanager';

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function seekerwishlistmanagerAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        //echo "<pre>".$action."</pre>";
        $user_id = $this->params()->fromQuery('id');
        //echo $user_id; die;

        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('useraccounts');
        }


        if ($action == 'view' || $action == 'edit') {
            $adminRow = $this->AdminSeekerModel->getSeekerWishListByID($user_id);

        } else {
            $dataArr['user_id'] = $id;
            $adminRow = $this->AdminSeekerModel->getUserReferencesByRow($dataArr);
        }

        $viewArray = array(
            'adminRows' => $adminRow,
            'flashMessages' => $flashMessages,
            // 'userlist' => $this->AdminSeekerModel->getUserRowAll(),
            'action' => $action,
            'id' => $id,

        );

        /*$viewArray = array(
    'adminRow' => $arr_trips,
    'flashMessages' =>   $flashMessages,
    'action' => $action,
    'id' => $id,
    'site_time_zones' => $this->timeZones
);
*/
        $viewArray['Menu_select'] = 'seeker_service';
        $viewArray['Sub_menu_select'] = 'seeker/seekerwishlist';

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }


}

?>