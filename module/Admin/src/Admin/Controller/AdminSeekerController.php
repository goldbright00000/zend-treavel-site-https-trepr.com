<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager;
use Admin\Model\AdminUserModel;
use Zend\Mvc\Plugin\FlashMessenger;
use Zend\Mvc\Plugin\Url;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;

require_once(ACTUAL_ROOTPATH . "Upload/src/Upload.php");

class AdminSeekerController extends AbstractActionController
{
    const ITEM_PER_PAGE = 20;
    const ADDR_ITEM_PER_PAGE = 1;

    public function __construct($data = false)
    {
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if ($data['models'] && is_array($data['models'])) {
            foreach ($data['models'] as $model) {
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->adminModules = $data['configs']['adminModules'];
        $this->adminMenus = $data['configs']['adminMenus'];
        $this->timeZones = $data['configs']['timezones'];
        //$this->AdminModel->authenticateAdmin('user_accounts');
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            // return $this->redirect()->toRoute('admin');
        }

    }

    public function indexAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $data = array();

        $arrAdminAccounts = $this->AdminUserModel->userAccouts($filterData);
        $data = !empty($arrAdminAccounts) ? $arrAdminAccounts : array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_trips'],
            ['Sub_menu_select' => 'seekers']
        ));
    }

    public function authenticateModule()
    {
        $resp = $this->AdminModel->authenticateAdmin('admin_seekers');
        if ($resp) {
            $this->flashMessenger()->addMessage($resp['msg']);
            return $this->redirect()->toRoute($resp['redirectUrl']);
        }
    }

    public function seekerServiceAction()
    {  

        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'seeker';
        $data = array();

        $trips = $this->AdminSeekerModel->getTrips($filterData);
        //echo '<pre>'; print_r($trips); exit;
        $arrTrips = array();
        if ($trips) {
            foreach ($trips as $trip) {
                $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
                $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);
                $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';

                //-- destination
                $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
                $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';

                //-- To get trip type
                $trip_people = $this->AdminSeekerModel->getSeekerPeople($trip['ID']);
                $trip_package = $this->AdminSeekerModel->getSeekerPackage($trip['ID']);
                $trip_project = $this->AdminSeekerModel->getSeekerProject($trip['ID']);

                $trip_people = isset($trip_people) ? $trip_people : false;
                $trip_package = isset($trip_package) ? $trip_package : false;
                $trip_project = isset($trip_project) ? $trip_project : false;

                $service = '';
                if ($trip_people['ID']) {
                    $service = 'People';
                }

                if ($trip_package['ID']) {
                    $service = 'Package';
                }

                if ($trip_project['ID']) {
                    $service = 'Product';
                }

                $arrTrips[] = array(
                    'trip_id_number' => $trip['trip_id_number'],
                    'ID' => $trip['ID'],
                    'name' => $trip['name'],
                    'first_name' => $trip['first_name'],
                    'email_address' => $trip['email_address'],
                    'origin_location' => $trip['origin_location'],
                    'destination_location' => $trip['destination_location'],
                    'origin_location_code' => $origin_location_code,
                    'destination_location_code' => $destination_location_code,
                    'origin_location_city' => $origin_location_city,
                    'destination_location_city' => $destination_location_city,
                    'origin_location_country' => $origin_location_country,
                    'destination_location_country' => $destination_location_country,
                    'origin_location_name' => $origin_location_name,
                    'destination_location_name' => $destination_location_name,
                    'departure_date' => $trip['departure_date'],
                    'departure_time' => $trip['departure_time'],
                    'arrival_date' => $trip['arrival_date'],
                    'arrival_time' => $trip['arrival_time'],
                    'travel_plan_reservation_type' => $trip['travel_plan_reservation_type'],
                    'service_type' => $service,
                    'active' => $trip['active'],
                    'modified' => $trip['modified']
                );
            }
        }

        $data = !empty($arrTrips) ? $arrTrips : array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_trips'],
            ['Sub_menu_select' => 'seekers']
        ));


    }
	
	public function seekerRequestServiceInfoAction(){
		$this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
		
		$action = $this->params()->fromQuery('action', 'view');
        $id = $this->params()->fromQuery('id', 0);
		$service = $this->params()->fromQuery('type', '');
		
		if($action == 'view' && $id > 0 && $service != ''){
			
			$type_of_service = $service;
			
			$requestInfo = $this->AdminSeekerModel->getSeekerRequestsByid($id,$service);
			$serviceInfo = $this->AdminSeekerModel->getSeekerServiceByRequest($requestInfo[$service.'_request'],$service);
			$requestInfo['service_details'] = $serviceInfo;
			$requestInfo['trip_details'] = $this->AdminSeekerModel->getTripRow($requestInfo['trip'], 'traveller');
			$requestInfo['seeker_trip_details'] = $this->AdminSeekerModel->getTripRow($serviceInfo['seeker_trip'], 'seeker');
			
			$requestInfo['trip_details']['origin_location'] = $this->AdminSeekerModel->getAirPort($requestInfo['trip_details']['origin_location']);
        	$requestInfo['trip_details']['destination_location'] = $this->AdminSeekerModel->getAirPort($requestInfo['trip_details']['destination_location']);
			
			$requestInfo['seeker_trip_details']['origin_location'] = $this->AdminSeekerModel->getAirPort($requestInfo['seeker_trip_details']['origin_location']);
        	$requestInfo['seeker_trip_details']['destination_location'] = $this->AdminSeekerModel->getAirPort($requestInfo['seeker_trip_details']['destination_location']);

			if($requestInfo['approved'] == '0'){
				$requestInfo['status'] = 'Pending';
			} else if($requestInfo['approved'] == '1'){
				$requestInfo['status'] = 'Accepted';
			} else if($requestInfo['approved'] == '2' || $requestInfo['approved'] == '5'){
				$requestInfo['status'] = 'Declined';
			} else if($requestInfo['approved'] == '3'){
				$requestInfo['status'] = 'Expired';
			} else if($requestInfo['approved'] == '4'){
				$requestInfo['status'] = 'Wishlisted';
			}
			
			$sTripId = $requestInfo['seeker_trip_details']['ID'];
			
			$trip_people 	= $this->AdminSeekerModel->getSeekerPeople($sTripId);
			$trip_package 	= $this->AdminSeekerModel->getSeekerPackage($sTripId);
			$trip_project 	= $this->AdminSeekerModel->getSeekerProject($sTripId);
			$airport_details = $this->AdminSeekerModel->getFlightsDetails($sTripId);
	
			$trip_people = isset($trip_people) ? $trip_people : false;
			$trip_package = isset($trip_package) ? $trip_package : false;
			$trip_project = isset($trip_project) ? $trip_project : false;
			$airport_details = isset($airport_details) ? $airport_details : false;
	
			$service = $type_of_service = '';
			if ($trip_people['ID']) {
				$service = 'People';
				$type_of_service = 'people';
				$trip_people_passangers = $this->AdminSeekerModel->getSeekerPeoplePassangers($trip_people['ID']);
				if ($trip_people_passangers) {
					$passengerAttach = false;
					foreach ($trip_people_passangers as $key => $eachPass) {
						$trip_people_passangersAttach = $this->AdminSeekerModel->getSeekerPeoplePassangerAttachments($eachPass['ID']);
						if ($trip_people_passangersAttach) {
							$trip_people_passangers[$key]['attachmentDetails'] = $trip_people_passangersAttach;
						}
					}
				}
				
				$requests = $this->AdminSeekerModel->getSeekerRequests($trip_people['ID'],$type_of_service);
				$travellerRequests = $this->AdminSeekerModel->getSeekerRequestsByTraveler($sTripId,$type_of_service);
			}
			$trip_people_passangers = isset($trip_people_passangers) ? $trip_people_passangers : false;
	
			if ($trip_package['ID']) {
				if ($service)
					$service .= ',';
				$service .= 'Package';
				$type_of_service = 'package';
				$trip_package_packages = $this->AdminSeekerModel->getSeekerPackagePackages($trip_package['ID']);
				
				$requests = $this->AdminSeekerModel->getSeekerRequests($trip_package['ID'],$type_of_service);
				$travellerRequests = $this->AdminSeekerModel->getSeekerRequestsByTraveler($sTripId,$type_of_service);
			}
			$trip_package_packages = isset($trip_package_packages) ? $trip_package_packages : false;
	
			if ($trip_project['ID']) {
				if ($service)
					$service .= ',';
				$service .= 'Product';
				$type_of_service = 'project';
				$trip_project_tasks = $this->AdminSeekerModel->getSeekerProjectTasks($trip_project['ID']);
				
				$requests = $this->AdminSeekerModel->getSeekerRequests($trip_project['ID'],$type_of_service);
				$travellerRequests = $this->AdminSeekerModel->getSeekerRequestsByTraveler($sTripId,$type_of_service);
			}
			$trip_project_tasks = isset($trip_project_tasks) ? $trip_project_tasks : false;
			
			$distancePrice = $countryPrice = $stopsPrice = 0;
			$passengerCount = (!empty($trip_people)) ? $trip_people['passengers'] : 1;

			$countryPriceList = $this->AdminModel->getCountryPrice($requestInfo['seeker_trip_details']['origin_location']['country_code'], $requestInfo['seeker_trip_details']['destination_location']['country_code'], $type_of_service);
	
			if (isset($requestInfo['seeker_trip_details']['noofstops'])) {
				$noofstops = $requestInfo['seeker_trip_details']['noofstops'];
			}
			if (isset($requestInfo['seeker_trip_details']['distance'])) {
				$distance = (int) $requestInfo['seeker_trip_details']['distance'];
			}
			if (!empty($countryPriceList)) {
				$countryPrice = $countryPriceList['country_price'];
			}
			if (isset($noofstops) && $noofstops != '') {
				$noofstops         = ($noofstops == 0) ? 'direct' : $noofstops;
				$arrNoofStopsPrice = $this->AdminModel->getNoofstopsPrice($noofstops);
				if (!empty($arrNoofStopsPrice)) {
					$stopsPrice = $arrNoofStopsPrice['stop_price'];
				}
			}
			if (!empty($distance)) {
				$arrDistancePrice = $this->AdminModel->getDistancePrice($distance, $type_of_service);
				if (!empty($arrDistancePrice)) {
					$distancePrice = $arrDistancePrice['distance_price'];
				}
			}
	
			$sub_total = 0;
			if($type_of_service == 'people')
			{
				$service_fees = (float)($distancePrice + $countryPrice + $stopsPrice);
				$sub_total = $service_fees;
	
				$service_fees = $service_fees * $passengerCount;
			}
			else if($type_of_service == 'package')
			{
				for ($i = 0; $i < count($trip_package_packages); $i++)
				{
					$tot_prc = $this->AdminModel->getItemCategoryPrice($trip_package_packages[$i]['item_category'] . $trip_package_packages[$i]['item_sub_category'], $type_of_service);
	
					if(!empty($trip_package_packages[$i]['weight'])){
						$item_weight = $trip_package_packages[$i]['weight'];
					}
					else{
						$item_weight = '0.5';
					}
	
					$tot_weight_prc = $this->AdminModel->getWeightPrice($item_weight, $type_of_service);
	
					$money = $this->AdminModel->convertCurrency($trip_package_packages[$i]['item_worth'], $trip_package_packages[$i]['item_worth_currency'], 'GBP');
					$itemPricePrice = $this->AdminModel->getItemPricePrice($money, $type_of_service);
	
					$item_sub_total = (float)($countryPrice + $distancePrice + ($tot_prc['tot_item_price'] * $tot_weight_prc['weight_carried_price']) + $itemPricePrice['price_item_price']);
	
					$trip_package_packages[$i]['item_category_price']   = $tot_prc;
					$trip_package_packages[$i]['item_weight_price']     = $tot_weight_prc;
					$trip_package_packages[$i]['item_price_price']      = $itemPricePrice;
					$trip_package_packages[$i]['item_sub_total']        = $item_sub_total;
					$sub_total = $sub_total + $item_sub_total;
				}
	
				$service_fees = $trip_package['package_service_fee'];
			}
			else if($type_of_service == 'project')
			{
				for ($i = 0; $i < count($trip_project_tasks); $i++)
				{
					$tot_prc = $this->AdminModel->getItemCategoryPrice($trip_project_tasks[$i]['category'] . $trip_project_tasks[$i]['additional_requirements_category'], $type_of_service);
	
					if(!empty($trip_project_tasks[$i]['item_weight'])){
						$item_weight = $trip_project_tasks[$i]['item_weight'];
					}
					else{
						$item_weight = '0.5';
					}
	
					$tot_weight_prc = $this->AdminModel->getWeightPrice($item_weight, $type_of_service);
	
					$money = $this->AdminModel->convertCurrency($trip_project_tasks[$i]['price'], $trip_project_tasks[$i]['price_currency'], 'GBP');
					$itemPricePrice = $this->AdminModel->getItemPricePrice($money, $type_of_service);
	
					$item_sub_total = (float)($countryPrice + $distancePrice + ($tot_prc['tot_item_price'] * $tot_weight_prc['weight_carried_price']) + $itemPricePrice['price_item_price'] + $money + (($money/100)*5));
	
					$trip_project_tasks[$i]['item_category_price']   = $tot_prc;
					$trip_project_tasks[$i]['item_weight_price']     = $tot_weight_prc;
					$trip_project_tasks[$i]['item_price_price']      = $itemPricePrice;
					$trip_project_tasks[$i]['item_sub_total']        = $item_sub_total;
					$sub_total = $sub_total + $item_sub_total;
				}
				$service_fees = $trip_package['project_service_fee'];
			}
			
			$viewArray = array(
				'noofstops' => $noofstops,
            	'distance' => $distance,
				'origin_location' => $requestInfo['seeker_trip_details']['origin_location']['ID'],
				'destination_location' => $requestInfo['seeker_trip_details']['destination_location']['ID'],
				'origin_location_code' => $requestInfo['seeker_trip_details']['origin_location']['code'],
				'destination_location_code' => $requestInfo['seeker_trip_details']['destination_location']['code'],
				'origin_location_city' => $requestInfo['seeker_trip_details']['origin_location']['city'],
				'destination_location_city' => $requestInfo['seeker_trip_details']['destination_location']['city'],
				'origin_location_country' => $requestInfo['seeker_trip_details']['origin_location']['country'],
				'destination_location_country' => $requestInfo['seeker_trip_details']['destination_location']['country'],
				'trip_people' => $trip_people,
				'trip_package' => $trip_package,
				'trip_project' => $trip_project,
				'trip_people_passangers' => $trip_people_passangers,
				'trip_package_packages' => $trip_package_packages,
				'trip_project_tasks' => $trip_project_tasks,
				'airport_details' => $airport_details,
				'countryPrice' => $countryPrice,
				'stopsPrice' => $stopsPrice,
				'distancePrice' => $distancePrice,
				'passengerCount' => $passengerCount,
				'service_fees' => $service_fees,
				'sub_total' => $sub_total,
				'total_cost_currency' => $requestInfo['seeker_trip_details']['total_cost_currency'],
				'conversion_rate' => $requestInfo['seeker_trip_details']['conversion_rate'],
				'total_cost' => $requestInfo['seeker_trip_details']['total_cost'],
			);
			
			return new ViewModel(array_merge($this->params()->fromQuery(),
				['flashMessages' => $this->flashMessenger()->getMessages()],
				['Menu_select' => 'seeker_trips'],
				['Sub_menu_select' => 'seekersrequests'],
				['adminRow'=>$viewArray],
				['data'=>$requestInfo]
			));
			
		} else {
			return $this->redirect()->toRoute('admin_seekers_request');
		}
	}
	
	public function seekerRequestServiceAction(){
		
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'seeker';
        $data = array();

        $requests = $this->AdminSeekerModel->getAllSeekerRequests($filterData);
	//print_r($requests);die;
		foreach($requests as $key=>$req){
			$requests[$key]['trip_details'] = $this->AdminSeekerModel->getTripRow($req['trip'], 'traveller');
			$requests[$key]['seeker_trip_details'] = $this->AdminSeekerModel->getTripRow($req['seeker_trip'], 'seeker');
			$requests[$key]['service_type'] = $req['service'];

			if($req['approved'] == '0'){
				$requests[$key]['status'] = 'Pending';
			} else if($req['approved'] == '1'){
				$requests[$key]['status'] = 'Accepted';
			} else if($req['approved'] == '2' || $req['approved'] == '5'){
				$requests[$key]['status'] = 'Declined';
			} else if($req['approved'] == '3'){
				$requests[$key]['status'] = 'Expired';
			} else if($req['approved'] == '4'){
				$requests[$key]['status'] = 'Wishlisted';
			}
		}
	
		$data = !empty($requests) ? $requests : array();
		
		//print_r($data);die;

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_trips'],
            ['Sub_menu_select' => 'seekersrequests']
        ));
	}
	
	public function travelerRequestServiceInfoAction(){
		$this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
		
		$action = $this->params()->fromQuery('action', 'view');
        $id = $this->params()->fromQuery('id', 0);
		$service = $this->params()->fromQuery('type', '');
		
		if($action == 'view' && $id > 0 && $service != ''){
			
			$type_of_service = $service;
			
			$requestInfo = $this->AdminSeekerModel->getTravelerRequestsByid($id,$service);
			$serviceInfo = $this->AdminSeekerModel->getTrvellerServiceByRequest($requestInfo[$service.'_request'],$service);
			$requestInfo['service_details'] = $serviceInfo;
			$requestInfo['trip_details'] = $this->AdminSeekerModel->getTripRow($serviceInfo['trip'], 'traveller');
			$requestInfo['seeker_trip_details'] = $this->AdminSeekerModel->getTripRow($requestInfo['trip'], 'seeker');

			$requestInfo['trip_details']['origin_location'] = $this->AdminSeekerModel->getAirPort($requestInfo['trip_details']['origin_location']);
        	$requestInfo['trip_details']['destination_location'] = $this->AdminSeekerModel->getAirPort($requestInfo['trip_details']['destination_location']);

			$requestInfo['seeker_trip_details']['origin_location'] = $this->AdminSeekerModel->getAirPort($requestInfo['seeker_trip_details']['origin_location']);
        	$requestInfo['seeker_trip_details']['destination_location'] = $this->AdminSeekerModel->getAirPort($requestInfo['seeker_trip_details']['destination_location']);

			if($requestInfo['approved'] == '0'){
				$requestInfo['status'] = 'Pending';
			} else if($requestInfo['approved'] == '1'){
				$requestInfo['status'] = 'Accepted';
			} else if($requestInfo['approved'] == '2' || $requestInfo['approved'] == '5'){
				$requestInfo['status'] = 'Declined';
			} else if($requestInfo['approved'] == '3'){
				$requestInfo['status'] = 'Expired';
			} else if($requestInfo['approved'] == '4'){
				$requestInfo['status'] = 'Wishlisted';
			}
			
			$sTripId = $requestInfo['seeker_trip_details']['ID'];
			
			$trip_people 	= $this->AdminSeekerModel->getSeekerPeople($sTripId);
			$trip_package 	= $this->AdminSeekerModel->getSeekerPackage($sTripId);
			$trip_project 	= $this->AdminSeekerModel->getSeekerProject($sTripId);
			$airport_details = $this->AdminSeekerModel->getFlightsDetails($sTripId);
	
			$trip_people = isset($trip_people) ? $trip_people : false;
			$trip_package = isset($trip_package) ? $trip_package : false;
			$trip_project = isset($trip_project) ? $trip_project : false;
			$airport_details = isset($airport_details) ? $airport_details : false;
	
			$service = $type_of_service = '';
			if ($trip_people['ID']) {
				$service = 'People';
				$type_of_service = 'people';
				$trip_people_passangers = $this->AdminSeekerModel->getSeekerPeoplePassangers($trip_people['ID']);
				if ($trip_people_passangers) {
					$passengerAttach = false;
					foreach ($trip_people_passangers as $key => $eachPass) {
						$trip_people_passangersAttach = $this->AdminSeekerModel->getSeekerPeoplePassangerAttachments($eachPass['ID']);
						if ($trip_people_passangersAttach) {
							$trip_people_passangers[$key]['attachmentDetails'] = $trip_people_passangersAttach;
						}
					}
				}
				
				$requests = $this->AdminSeekerModel->getSeekerRequests($trip_people['ID'],$type_of_service);
				$travellerRequests = $this->AdminSeekerModel->getSeekerRequestsByTraveler($sTripId,$type_of_service);
			}
			$trip_people_passangers = isset($trip_people_passangers) ? $trip_people_passangers : false;
	
			if ($trip_package['ID']) {
				if ($service)
					$service .= ',';
				$service .= 'Package';
				$type_of_service = 'package';
				$trip_package_packages = $this->AdminSeekerModel->getSeekerPackagePackages($trip_package['ID']);
				
				$requests = $this->AdminSeekerModel->getSeekerRequests($trip_package['ID'],$type_of_service);
				$travellerRequests = $this->AdminSeekerModel->getSeekerRequestsByTraveler($sTripId,$type_of_service);
			}
			$trip_package_packages = isset($trip_package_packages) ? $trip_package_packages : false;
	
			if ($trip_project['ID']) {
				if ($service)
					$service .= ',';
				$service .= 'Product';
				$type_of_service = 'project';
				$trip_project_tasks = $this->AdminSeekerModel->getSeekerProjectTasks($trip_project['ID']);
				
				$requests = $this->AdminSeekerModel->getSeekerRequests($trip_project['ID'],$type_of_service);
				$travellerRequests = $this->AdminSeekerModel->getSeekerRequestsByTraveler($sTripId,$type_of_service);
			}
			$trip_project_tasks = isset($trip_project_tasks) ? $trip_project_tasks : false;
			
			$distancePrice = $countryPrice = $stopsPrice = 0;
			$passengerCount = (!empty($trip_people)) ? $trip_people['passengers'] : 1;

			$countryPriceList = $this->AdminModel->getCountryPrice($requestInfo['seeker_trip_details']['origin_location']['country_code'], $requestInfo['seeker_trip_details']['destination_location']['country_code'], $type_of_service);
	
			if (isset($requestInfo['seeker_trip_details']['noofstops'])) {
				$noofstops = $requestInfo['seeker_trip_details']['noofstops'];
			}
			if (isset($requestInfo['seeker_trip_details']['distance'])) {
				$distance = (int) $requestInfo['seeker_trip_details']['distance'];
			}
			if (!empty($countryPriceList)) {
				$countryPrice = $countryPriceList['country_price'];
			}
			if (isset($noofstops) && $noofstops != '') {
				$noofstops         = ($noofstops == 0) ? 'direct' : $noofstops;
				$arrNoofStopsPrice = $this->AdminModel->getNoofstopsPrice($noofstops);
				if (!empty($arrNoofStopsPrice)) {
					$stopsPrice = $arrNoofStopsPrice['stop_price'];
				}
			}
			if (!empty($distance)) {
				$arrDistancePrice = $this->AdminModel->getDistancePrice($distance, $type_of_service);
				if (!empty($arrDistancePrice)) {
					$distancePrice = $arrDistancePrice['distance_price'];
				}
			}
	
			$sub_total = 0;
			if($type_of_service == 'people')
			{
				$service_fees = (float)($distancePrice + $countryPrice + $stopsPrice);
				$sub_total = $service_fees;
	
				$service_fees = $service_fees * $passengerCount;
			}
			else if($type_of_service == 'package')
			{
				for ($i = 0; $i < count($trip_package_packages); $i++)
				{
					$tot_prc = $this->AdminModel->getItemCategoryPrice($trip_package_packages[$i]['item_category'] . $trip_package_packages[$i]['item_sub_category'], $type_of_service);
	
					if(!empty($trip_package_packages[$i]['weight'])){
						$item_weight = $trip_package_packages[$i]['weight'];
					}
					else{
						$item_weight = '0.5';
					}
	
					$tot_weight_prc = $this->AdminModel->getWeightPrice($item_weight, $type_of_service);
	
					$money = $this->AdminModel->convertCurrency($trip_package_packages[$i]['item_worth'], $trip_package_packages[$i]['item_worth_currency'], 'GBP');
					$itemPricePrice = $this->AdminModel->getItemPricePrice($money, $type_of_service);
	
					$item_sub_total = (float)($countryPrice + $distancePrice + ($tot_prc['tot_item_price'] * $tot_weight_prc['weight_carried_price']) + $itemPricePrice['price_item_price']);
	
					$trip_package_packages[$i]['item_category_price']   = $tot_prc;
					$trip_package_packages[$i]['item_weight_price']     = $tot_weight_prc;
					$trip_package_packages[$i]['item_price_price']      = $itemPricePrice;
					$trip_package_packages[$i]['item_sub_total']        = $item_sub_total;
					$sub_total = $sub_total + $item_sub_total;
				}
	
				$service_fees = $trip_package['package_service_fee'];
			}
			else if($type_of_service == 'project')
			{
				for ($i = 0; $i < count($trip_project_tasks); $i++)
				{
					$tot_prc = $this->AdminModel->getItemCategoryPrice($trip_project_tasks[$i]['category'] . $trip_project_tasks[$i]['additional_requirements_category'], $type_of_service);
	
					if(!empty($trip_project_tasks[$i]['item_weight'])){
						$item_weight = $trip_project_tasks[$i]['item_weight'];
					}
					else{
						$item_weight = '0.5';
					}
	
					$tot_weight_prc = $this->AdminModel->getWeightPrice($item_weight, $type_of_service);
	
					$money = $this->AdminModel->convertCurrency($trip_project_tasks[$i]['price'], $trip_project_tasks[$i]['price_currency'], 'GBP');
					$itemPricePrice = $this->AdminModel->getItemPricePrice($money, $type_of_service);
	
					$item_sub_total = (float)($countryPrice + $distancePrice + ($tot_prc['tot_item_price'] * $tot_weight_prc['weight_carried_price']) + $itemPricePrice['price_item_price'] + $money + (($money/100)*5));
	
					$trip_project_tasks[$i]['item_category_price']   = $tot_prc;
					$trip_project_tasks[$i]['item_weight_price']     = $tot_weight_prc;
					$trip_project_tasks[$i]['item_price_price']      = $itemPricePrice;
					$trip_project_tasks[$i]['item_sub_total']        = $item_sub_total;
					$sub_total = $sub_total + $item_sub_total;
				}
				$service_fees = $trip_package['project_service_fee'];
			}
			
			$viewArray = array(
				'noofstops' => $noofstops,
            	'distance' => $distance,
				'origin_location' => $requestInfo['seeker_trip_details']['origin_location']['ID'],
				'destination_location' => $requestInfo['seeker_trip_details']['destination_location']['ID'],
				'origin_location_code' => $requestInfo['seeker_trip_details']['origin_location']['code'],
				'destination_location_code' => $requestInfo['seeker_trip_details']['destination_location']['code'],
				'origin_location_city' => $requestInfo['seeker_trip_details']['origin_location']['city'],
				'destination_location_city' => $requestInfo['seeker_trip_details']['destination_location']['city'],
				'origin_location_country' => $requestInfo['seeker_trip_details']['origin_location']['country'],
				'destination_location_country' => $requestInfo['seeker_trip_details']['destination_location']['country'],
				'trip_people' => $trip_people,
				'trip_package' => $trip_package,
				'trip_project' => $trip_project,
				'trip_people_passangers' => $trip_people_passangers,
				'trip_package_packages' => $trip_package_packages,
				'trip_project_tasks' => $trip_project_tasks,
				'airport_details' => $airport_details,
				'countryPrice' => $countryPrice,
				'stopsPrice' => $stopsPrice,
				'distancePrice' => $distancePrice,
				'passengerCount' => $passengerCount,
				'service_fees' => $service_fees,
				'sub_total' => $sub_total,
				'total_cost_currency' => $requestInfo['seeker_trip_details']['total_cost_currency'],
				'conversion_rate' => $requestInfo['seeker_trip_details']['conversion_rate'],
				'total_cost' => $requestInfo['seeker_trip_details']['total_cost'],
			);
			
			return new ViewModel(array_merge($this->params()->fromQuery(),
				['flashMessages' => $this->flashMessenger()->getMessages()],
				['Menu_select' => 'seeker_trips'],
				['Sub_menu_select' => 'travelersrequests'],
				['adminRow'=>$viewArray],
				['data'=>$requestInfo]
			));
			
		} else {
			return $this->redirect()->toRoute('admin_travelers_request_info');
		}
	}
	
	public function travelerRequestServiceAction(){ 
		
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'traveler';
        $data = array();

        $requests = $this->AdminSeekerModel->getAllTravelerRequests($filterData);
		
		foreach($requests as $key=>$req){
			$requests[$key]['trip_details'] = $this->AdminSeekerModel->getTripRow($req['trip'], 'seeker');
			$requests[$key]['traveler_trip_details'] = $this->AdminSeekerModel->getTripRow($req['traveler_trip'], 'traveller');
			$requests[$key]['service_type'] = $req['service'];

			if($req['approved'] == '0'){
				$requests[$key]['status'] = 'Pending';
			} else if($req['approved'] == '1'){
				$requests[$key]['status'] = 'Accepted';
			} else if($req['approved'] == '2' || $req['approved'] == '5'){
				$requests[$key]['status'] = 'Declined';
			} else if($req['approved'] == '3'){
				$requests[$key]['status'] = 'Expired';
			} else if($req['approved'] == '4'){
				$requests[$key]['status'] = 'Wishlisted';
			}
		}

		$data = !empty($requests) ? $requests : array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_trips'],
            ['Sub_menu_select' => 'travelersrequests']
        ));
	}

    public function seekersticketServiceAction()
    {

        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'seeker';
        $data = array();

        $trips = $this->AdminSeekerModel->getTrips($filterData);
       // print_r($trips);die;
        $arrTrips = array();
        if ($trips) {
            foreach ($trips as $trip) {
                $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
                $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);
                $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
                $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
                $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $arrTrips[] = array(
                    'trip_id_number' => $trip['trip_id_number'],
                    'ID' => $trip['ID'],
                    'name' => $trip['name'],
                    'fullname' => $trip['first_name']." ".$trip['last_name'],
                    'email_address' => $trip['email_address'],
                    'origin_location' => $trip['origin_location'],
                    'destination_location' => $trip['destination_location'],
                    'origin_location_name' => $origin_location_name,
                    'origin_location_code' => $origin_location_code,
                    'destination_location_name' => $destination_location_name,
                    'destination_location_code' => $destination_location_code,
                    'origin_location_city' => $origin_location_city,
                    'destination_location_city' => $destination_location_city,
                    'origin_location_country' => $origin_location_country,
                    'destination_location_country' => $destination_location_country,
                    'departure_date' => $trip['departure_date'],
                    'arrival_date' => $trip['arrival_date'],
                    
                    
                    'ticket_image' => $trip['ticket_image'],
                    'ticket_type' => $trip['ticket_option'],
                    
                    'active' => $trip['active'],
                    'modified' => $trip['modified']
                );
            }
        }

        $data = !empty($arrTrips) ? $arrTrips : array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_trips'],
            ['Sub_menu_select' => 'seekersticket']
        ));


    }

    public function travellerServiceAction()
    { 

        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'traveller';
        $data = array();

        $trips = $this->AdminSeekerModel->getTravellerTrips($filterData);

        $arrTrips = array();
        if ($trips) {
            foreach ($trips as $trip)
            {
                $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
                $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);

                //-- origin
                $origin_location_name = isset($origin_location['name'])?$origin_location['name']:'';
                $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';

                //-- destination
                $destination_location_name = isset($destination_location['name'])?$destination_location['name']:'';
                $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';

                //-- To get trip type
                $trip_people = $this->AdminSeekerModel->getTravellerPeople($trip['ID']);
                $trip_package = $this->AdminSeekerModel->getTravellerPackage($trip['ID']);
                $trip_project = $this->AdminSeekerModel->getTravellerProject($trip['ID']);

                $trip_people = isset($trip_people) ? $trip_people : false;
                $trip_package = isset($trip_package) ? $trip_package : false;
                $trip_project = isset($trip_project) ? $trip_project : false;

                $service = '';
                if ($trip_people['ID']) {
                    $service = 'People';
                }

                if ($trip_package['ID']) {
                    $service = 'Package';
                }

                if ($trip_project['ID']) {
                    $service = 'Product';
                }

                $arrTrips[] = array
                (
                    'trip_id_number' => $trip['trip_id_number'],
                    'ID' => $trip['ID'],
                    'name' => $trip['name'],
                    'trip_status' => $trip['trip_status'],
                    'email_address' => $trip['email_address'],
                    'origin_location' => $trip['origin_location'],
                    'destination_location' => $trip['destination_location'],
                    'origin_location_name'      => $origin_location_name,
                    'destination_location_name' => $destination_location_name,
                    'origin_location_code' => $origin_location_code,
                    'destination_location_code' => $destination_location_code,
                    'origin_location_city' => $origin_location_city,
                    'destination_location_city' => $destination_location_city,
                    'origin_location_country' => $origin_location_country,
                    'destination_location_country' => $destination_location_country,
                    'departure_date' => $trip['departure_date'],
                    'departure_time' => $trip['departure_time'],
                    'arrival_date' => $trip['arrival_date'],
                    'travel_plan_reservation_type' => $trip['travel_plan_reservation_type'],
                    'service_type' => $service,
                    'arrival_time' => $trip['arrival_time'],
                    'active' => $trip['active'],
                    'modified' => $trip['modified']

                );
            }
        }

        $data = !empty($arrTrips) ? $arrTrips : array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_trips'],
            ['Sub_menu_select' => 'travellers']
        ));


    }

    public function travellersticketServiceAction()
    { 

        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'traveller';
        $data = array();

        $trips = $this->AdminSeekerModel->getTrips($filterData);
        $arrTrips = array();
        if ($trips) {
            foreach ($trips as $trip) {
                $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
                $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);
                $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
                $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
                $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
                $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
                $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
                $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
                $arrTrips[] = array(
                    'trip_id_number' => $trip['trip_id_number'],
                    'ID' => $trip['ID'],
                    'name' => $trip['name'],
                    'fullname' => $trip['first_name']." ".$trip['last_name'],
                    'trip_status' => $trip['trip_status'],
                    'email_address' => $trip['email_address'],
                    'origin_location' => $trip['origin_location'],
                    'destination_location' => $trip['destination_location'],
                    'origin_location_code' => $origin_location_code,
                    'destination_location_code' => $destination_location_code,
                    'origin_location_city' => $origin_location_city,
                    'destination_location_city' => $destination_location_city,
                    'origin_location_country' => $origin_location_country,
                    'destination_location_country' => $destination_location_country,
                    'departure_date' => $trip['departure_date'],
                    'arrival_date' => $trip['arrival_date'],
          
                    'ticket_image' => $trip['ticket_image'],
                    'ticket_type' => $trip['ticket_option'],
                    'active' => $trip['active'],
                    'modified' => $trip['modified']
                );
            }
        }

        $data = !empty($arrTrips) ? $arrTrips : array();

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_trips'],
            ['Sub_menu_select' => 'travellersticket']
        ));


    }

    public function seekermanagerAction()
    { 
        
          $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        
          if(isset($_REQUEST['edit_addres']))
            {
                //print_r($_REQUEST);die;
                
                            $update_address_trips = array(

                       'street_address_1'=>$_REQUEST['street_address_1'],
                       'street_address_2'=>$_REQUEST['street_address_2'],
                       'city' =>$_REQUEST['city'],
                       'state' =>$_REQUEST['state'],
                       'zip_code' =>$_REQUEST['zip_code'],
                       'country' =>$_REQUEST['country']


                             );
                            

                $this->AdminSeekerModel->updateTripAddress($update_address_trips,$_REQUEST['id']);
                            
            }
        
        
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        $usertype = $this->params()->fromQuery('usertype', 'seeker');
        if ($this->params()->fromPost('rejectBtn')) {
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('useraccounts');
            }
        }
        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('useraccounts');
        }


        $trip = $this->AdminSeekerModel->getTripRow($id, $usertype);
       // print_r($trip);die;

        $trip_people = $this->AdminSeekerModel->getSeekerPeople($trip['ID']);
        $trip_package = $this->AdminSeekerModel->getSeekerPackage($trip['ID']);
        $trip_project = $this->AdminSeekerModel->getSeekerProject($trip['ID']);
        $trip_location = $this->AdminSeekerModel->getUserLocationById($trip['user_location']);
        $airport_details = $this->AdminSeekerModel->getFlightsDetails($trip['ID']);

        $trip_people = isset($trip_people) ? $trip_people : false;
        $trip_package = isset($trip_package) ? $trip_package : false;
        $trip_project = isset($trip_project) ? $trip_project : false;
        $airport_details = isset($airport_details) ? $airport_details : false;

        $service = '';
        if ($trip_people['ID']) {
            $service = 'People';
            $trip_people_passangers = $this->AdminSeekerModel->getSeekerPeoplePassangers($trip_people['ID']);
            if ($trip_people_passangers) {
                $passengerAttach = false;
                foreach ($trip_people_passangers as $key => $eachPass) {
                    $trip_people_passangersAttach = $this->AdminSeekerModel->getSeekerPeoplePassangerAttachments($eachPass['ID']);
                    if ($trip_people_passangersAttach) {
                        $trip_people_passangers[$key]['attachmentDetails'] = $trip_people_passangersAttach;
                    }
                }
            }
        }
        $trip_people_passangers = isset($trip_people_passangers) ? $trip_people_passangers : false;
        if ($trip_package['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Package';
        }

        if ($trip_project['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Product';
        }
        $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
        $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);

        $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
        $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
        $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
        $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
        $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
        $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
        $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
        $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
        $arr_trips = array(
            'trip_id_number' => $trip['trip_id_number'],
            'ID' => $trip['ID'],
            'name' => $trip['name'],
            'noofstops' => $trip['noofstops'],
            'first_name' => $trip['first_name'],
            'last_name' => $trip['last_name'],
            'gender' => $trip['gender'],
            'email_address' => $trip['email_address'],
            'origin_location' => $trip['origin_location'],
            'destination_location' => $trip['destination_location'],
            'origin_location_code' => $origin_location_code,
            'destination_location_code' => $destination_location_code,
            'origin_location_city' => $origin_location_city,
            'origin_location_name' => $origin_location_name,
            'destination_location_city' => $destination_location_city,
            'destination_location_name' => $destination_location_name,
            'origin_location_country' => $origin_location_country,
            'destination_location_country' => $destination_location_country,
            'departure_date' => $trip['departure_date'],
            'arrival_date' => $trip['arrival_date'],
            'cabin' => $trip['cabin'],
            'booking_status' => $trip['booking_status'],
            'ticket_image' => $trip['ticket_image'],
             'ticket_type' => $trip['ticket_option'],
'travel_plan_reservation_type' => $trip['travel_plan_reservation_type'],
            'ticket_number' => $trip['ticket_number'],
            'service' => $service,
            'trip_people' => $trip_people,
            'trip_package' => $trip_package,
            'trip_project' => $trip_project,
            'trip_location' => $trip_location,
            'trip_people_passangers' => $trip_people_passangers,
            'airport_details' => $airport_details,
            'modified' => $trip['modified']
        );
        // print_r($arr_trips);exit;
        $viewArray = array(
            'adminRow' => $arr_trips,
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );

        $viewArray['Menu_select'] = 'seeker_trips';
        $viewArray['Sub_menu_select'] = 'seekermanager';

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function travellermanagerAction()
    { 

           // print_r($_REQUEST);die;
            if(isset($_REQUEST['subAdminBtn']))
            {
                
                
                 $update_arr_trips = array(
            
           
            'travel_agency_name' =>$_REQUEST['travel_agency_name'],
            'travel_agency_url' => $_REQUEST['travel_agency_url'],
            'travel_agency_confirmation' =>$_REQUEST['travel_agency_confirmation'],
            'travel_agency_contact_name' => $_REQUEST['travel_agency_contact_name'],
            'travel_agency_phone'=> $_REQUEST['travel_agency_phone'],
            'agency_email'=>$_REQUEST['travel_agency_phone'],
            'booking_site_name'=> $_REQUEST['booking_site_name'],
            'booking_site_url'=> $_REQUEST['booking_site_url'],
            'booking_site_email'=> $_REQUEST['booking_site_email'],
           // 'booking_date'=> $_REQUEST['booking_date'],
           // 'booking_referance'=> $trip['booking_referance'],
           // 'reservation_is_purchased'=> $_REQUEST['reservation_is_purchased'],
            'total_cost'=> $_REQUEST['total_cost'],
            'conversion_rate'=> $_REQUEST['conversion_rate'],
            'total_cost_currency'=>$_REQUEST['total_cost_currency'],
            'discount_code'=>$_REQUEST['discount_code'],
            'total_discount_cost'=>$_REQUEST['total_discount_cost'],
            'total_discount_currency'=>$_REQUEST['total_discount_currency'],
            'comments_restrictions'=>$_REQUEST['comments_restrictions'],
            'street_address_1'=>$arr_address['street_address_1'],
            'street_address_2'=>$arr_address['street_address_2'],
            'city' =>$arr_address['city'],
            'state' =>$arr_address['state'],
            'zip_code' =>$arr_address['zip_code'],
            'country' =>$arr_address['country']
            
            
        );
            $this->AdminSeekerModel->updateTrip($update_arr_trips,$_REQUEST['id']);
            }
            
            
            if(isset($_REQUEST['edit_addres']))
            {
                //print_r($_REQUEST);die;
                
                 $update_address_trips = array(
  
            'street_address_1'=>$_REQUEST['street_address_1'],
            'street_address_2'=>$_REQUEST['street_address_2'],
            'city' =>$_REQUEST['city'],
            'state' =>$_REQUEST['state'],
            'zip_code' =>$_REQUEST['zip_code'],
            'country' =>$_REQUEST['country']
            
            
        );
                $this->AdminSeekerModel->updateTripAddress($update_address_trips,$_REQUEST['id']);
            }
            
            
//echo $this->params()->fromPost('doAction');die;
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        $usertype = $this->params()->fromQuery('usertype', 'traveller');
        if ($this->params()->fromPost('doAction')) {

            $ids = $this->params()->fromPost('ids');
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_traveller_service');
            }

            $status = $this->AdminSeekerModel->doTripActions($this->params()->fromPost('doAction'), $ids);
            $identityRow = $this->AdminSeekerModel->getTripRow($id, $usertype);
            $mailRow['first_name'] = $identityRow['first_name'];
            $mailRow['trip_id_number'] = $identityRow['trip_id_number'];
            $mailRow['email_address'] = $identityRow['email_address'];
            $mailRow['rejectReason'] = $this->params()->fromPost('rejectReason');

            $this->AdminMailModel->sendTripApprovedMail($mailRow);
            $this->AdminSeekerModel->setApprovalIdentities($identityRow['user'], 1);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('travellermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $id));
        } else if ($this->params()->fromPost('rejectBtn')) {
            $trip_id = $this->params()->fromPost('trip_id');
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            $identityRow = $this->AdminSeekerModel->getTripRow($id, $usertype);
            $btrip_total=$this->AdminSeekerModel->getBtrip($identityRow['user']);
          
            if(count($btrip_total)== '1')
                
            {
                 $this->AdminSeekerModel->setApprovalIdentities($identityRow['user'], 0);
            }
            
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('usertrustverification');
            }
            $status = $this->AdminSeekerModel->doTripActions('reject', $trip_id, $this->params()->fromPost('rejectReason'));
            
            $btrip_total=$this->AdminSeekerModel->getBtrip($identityRow['user']);
            if(count($btrip_total)== '0')
            
            {
                 $this->AdminSeekerModel->setApprovalIdentities($identityRow['user'], 0);
            }
            $identityRow = $this->AdminSeekerModel->getTripRow($trip_id, $usertype);
            $mailRow['first_name'] = $identityRow['first_name'];
            $mailRow['trip_id_number'] = $identityRow['trip_id_number'];
            $mailRow['email_address'] = $identityRow['email_address'];
            $mailRow['rejectReason'] = $this->params()->fromPost('rejectReason');

            $this->AdminMailModel->sendTripRejectMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('travellermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $trip_id));


        } else if ($this->params()->fromPost('moreInfoBtn')) {
            $trip_id = $this->params()->fromPost('trip_id');

            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('usertrustverification');
            }
            $mailRow['first_name'] = $this->params()->fromPost('firstname');
            $mailRow['trip_id_number'] = $this->params()->fromPost('trip_id_number');
            $mailRow['email_address'] = $this->params()->fromPost('email');
            $mailRow['moreInfo'] = $this->params()->fromPost('moreInfo');

            $this->AdminMailModel->sendTripRequestMoreInfoMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('travellermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $trip_id));


        }

        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('useraccounts');
        }

        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $trip = $this->AdminSeekerModel->getTripRow($id, $usertype);

        $trip_people = $this->AdminSeekerModel->getTravellerPeople($trip['ID']);
        $trip_package = $this->AdminSeekerModel->getTravellerPackage($trip['ID']);
        $trip_project = $this->AdminSeekerModel->getTravellerProject($trip['ID']);
        $trip_location = $this->AdminSeekerModel->getUserLocationById($trip['user_location']);
        $airport_details = $this->AdminSeekerModel->getFlightsDetails($trip['ID']);
        // print_r($airport_details);exit;
        $trip_people = isset($trip_people) ? $trip_people : false;
        $trip_package = isset($trip_package) ? $trip_package : false;
        $trip_project = isset($trip_project) ? $trip_project : false;
        $airport_details = isset($airport_details) ? $airport_details : false;

        $service = '';
        if ($trip_people['ID']) {
            $service = 'People';
            // $trip_people_passangers = $this->AdminSeekerModel->getSeekerPeoplePassangers($trip_people['ID']);
        }
        $trip_people_passangers = isset($trip_people_passangers) ? $trip_people_passangers : false;
        if ($trip_package['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Package';
        }

        if ($trip_project['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Product';
        }
        
        
        
        $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
        $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);

        $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
        $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
        $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
        $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
        $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
        $origin_location_name = isset($origin_location['name']) ? $origin_location['name'] : '';
        $destination_location_name = isset($destination_location['name']) ? $destination_location['name'] : '';
        $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
        
        if(isset($_REQUEST['tab']))
        {
            $tab_active='tickets';
        }
        else
        {
            $tab_active="";
        }
        
        $arr_address= $this->AdminSeekerModel->getAddressRow($trip['ID']);
        
        
       // print_r($arr_address);die;
      
        $arr_trips = array(
            'trip_id_number' => $trip['trip_id_number'],
            'ID' => $trip['ID'],
            'name' => $trip['name'],
            'rejected_reason' => $trip['rejected_reason'],
            'noofstops' => $trip['noofstops'],
            'ticket_image' => $trip['ticket_image'],
            'ticket_type' => $trip['ticket_option'],
            'trip_status' => $trip['trip_status'],
            'first_name' => $trip['first_name'],
            'email_address' => $trip['email_address'],
            'origin_location' => $trip['origin_location'],
            'destination_location' => $trip['destination_location'],
            'origin_location_code' => $origin_location_code,
            'destination_location_code' => $destination_location_code,
            'origin_location_city' => $origin_location_city,
            'origin_location_name' => $origin_location_name,
            'destination_location_city' => $destination_location_city,
            'destination_location_name' => $destination_location_name,
            'origin_location_country' => $origin_location_country,
            'destination_location_country' => $destination_location_country,
            'departure_date' => $trip['departure_date'],
            'arrival_date' => $trip['arrival_date'],
            'cabin' => $trip['cabin'],
            'booking_status' => $trip['booking_status'],
            'service' => $service,
            'trip_people' => $trip_people,
            'trip_package' => $trip_package,
            'trip_project' => $trip_project,
            'trip_location' => $trip_location,
            'trip_people_passangers' => $trip_people_passangers,
            'airport_details' => $airport_details,
            'modified' => $trip['modified'],
            'tab_active' => $tab_active,
            'travel_agency_name' =>$trip['travel_agency_name'],
            'travel_agency_url' => $trip['travel_agency_url'],
            'travel_agency_confirmation' =>$trip['travel_agency_confirmation'],
            'travel_agency_contact_name' => $trip['travel_agency_contact_name'],
            'travel_agency_phone'=> $trip['travel_agency_phone'],
            'agency_email'=>$trip['travel_agency_phone'],
            'booking_site_name'=> $trip['booking_site_name'],
            'booking_site_url'=> $trip['booking_site_phone'],
            'booking_site_email'=> $trip['booking_site_email'],
            'booking_date'=> $trip['booking_date'],
           // 'booking_referance'=> $trip['booking_referance'],
            'reservation_is_purchased'=> $trip['reservation_is_purchased'],
            'total_cost'=> $trip['total_cost'],
            'conversion_rate'=> $trip['conversion_rate'],
            'total_cost_currency'=>$trip['total_cost_currency'],
            'discount_code'=>$trip['discount_code'],
            'total_discount_cost'=>$trip['total_discount_cost'],
            'total_discount_currency'=>$trip['total_discount_currency'],
            'comments_restrictions'=>$trip['comments_restrictions'],
            'street_address_1'=>$arr_address['street_address_1'],
            'street_address_2'=>$arr_address['street_address_2'],
            'city' =>$arr_address['city'],
            'state' =>$arr_address['state'],
            'zip_code' =>$arr_address['zip_code'],
            'country' =>$arr_address['country']
            
            
        );
        // print_r($arr_trips);exit;
        $viewArray = array(
            'adminRow' => $arr_trips,
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );

        $viewArray['Menu_select'] = 'seeker_trips';
        $viewArray['Sub_menu_select'] = 'travellermanager';

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }

    public function travellerticketmanagerAction()
    {
        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action = $this->params()->fromQuery('action', 'add');
        $id = $this->params()->fromQuery('id', 0);
        $usertype = $this->params()->fromQuery('usertype', 'traveller');
        if ($this->params()->fromPost('doAction')) {

            $ids = $this->params()->fromPost('ids');
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('admin_traveller_service');
            }

            $status = $this->AdminSeekerModel->doTripActions($this->params()->fromPost('doAction'), $ids);
            $identityRow = $this->AdminSeekerModel->getTripRow($id, $usertype);
            $mailRow['first_name'] = $identityRow['first_name'];
            $mailRow['trip_id_number'] = $identityRow['trip_id_number'];
            $mailRow['email_address'] = $identityRow['email_address'];
            $mailRow['rejectReason'] = $this->params()->fromPost('rejectReason');
            $this->CommonMethodsModel->setApprovalIdentities($id, 1);
           
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('travellermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $id));
        } else if ($this->params()->fromPost('rejectBtn')) {
            $trip_id = $this->params()->fromPost('trip_id');
            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('usertrustverification');
            }
            $status = $this->AdminSeekerModel->doTripActions('reject', $trip_id, $this->params()->fromPost('rejectReason'));
            $identityRow = $this->AdminSeekerModel->getTripRow($trip_id, $usertype);
            $mailRow['first_name'] = $identityRow['first_name'];
            $mailRow['trip_id_number'] = $identityRow['trip_id_number'];
            $mailRow['email_address'] = $identityRow['email_address'];
            $mailRow['rejectReason'] = $this->params()->fromPost('rejectReason');

            $this->AdminMailModel->sendTripRejectMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('travellermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $trip_id));


        } else if ($this->params()->fromPost('moreInfoBtn')) {
            $trip_id = $this->params()->fromPost('trip_id');

            $resp = $this->AdminModel->checkAdminPrivilage('UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('usertrustverification');
            }
            $mailRow['first_name'] = $this->params()->fromPost('firstname');
            $mailRow['trip_id_number'] = $this->params()->fromPost('trip_id_number');
            $mailRow['email_address'] = $this->params()->fromPost('email');
            $mailRow['moreInfo'] = $this->params()->fromPost('moreInfo');

            $this->AdminMailModel->sendTripRequestMoreInfoMail($mailRow);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
            $flashMessages = $this->flashMessenger()->getMessages();
            return $this->redirect()->toRoute('travellermanagerWithParam', array('controller' => 'admin', 'action' => 'action=view&id=' . $trip_id));


        }

        if ($action == 'add') $priv = 'INSERT';
        else if ($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);
        if ($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('useraccounts');
        }

        //echo '<pre>';var_dump($this->AdminModel->getLanguages());exit;
        $trip = $this->AdminSeekerModel->getTripRow($id, $usertype);

        $trip_people = $this->AdminSeekerModel->getTravellerPeople($trip['ID']);
        $trip_package = $this->AdminSeekerModel->getTravellerPackage($trip['ID']);
        $trip_project = $this->AdminSeekerModel->getTravellerProject($trip['ID']);
        $trip_location = $this->AdminSeekerModel->getUserLocationById($trip['user_location']);
        $airport_details = $this->AdminSeekerModel->getFlightsDetails($trip['ID']);
        // print_r($airport_details);exit;
        $trip_people = isset($trip_people) ? $trip_people : false;
        $trip_package = isset($trip_package) ? $trip_package : false;
        $trip_project = isset($trip_project) ? $trip_project : false;
        $airport_details = isset($airport_details) ? $airport_details : false;

        $service = '';
        if ($trip_people['ID']) {
            $service = 'People';
            // $trip_people_passangers = $this->AdminSeekerModel->getSeekerPeoplePassangers($trip_people['ID']);
        }
        $trip_people_passangers = isset($trip_people_passangers) ? $trip_people_passangers : false;
        if ($trip_package['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Package';
        }

        if ($trip_project['ID']) {
            if ($service)
                $service .= ',';
            $service .= 'Product';
        }
        $origin_location = $this->AdminSeekerModel->getAirPort($trip['origin_location']);
        $destination_location = $this->AdminSeekerModel->getAirPort($trip['destination_location']);

        $origin_location_code = isset($origin_location['code']) ? $origin_location['code'] : '';
        $origin_location_city = isset($origin_location['city']) ? $origin_location['city'] : '';
        $origin_location_country = isset($origin_location['country']) ? $origin_location['country'] : '';
        $destination_location_code = isset($destination_location['code']) ? $destination_location['code'] : '';
        $destination_location_city = isset($destination_location['city']) ? $destination_location['city'] : '';
        $destination_location_country = isset($destination_location['country']) ? $destination_location['country'] : '';
        $arr_trips = array(
            'trip_id_number' => $trip['trip_id_number'],
            'ID' => $trip['ID'],
            'name' => $trip['name'],
            'rejected_reason' => $trip['rejected_reason'],
            'noofstops' => $trip['noofstops'],
            'ticket_image' => $trip['ticket_image'],
            'trip_status' => $trip['trip_status'],
            'first_name' => $trip['first_name'],
            'email_address' => $trip['email_address'],
            'origin_location' => $trip['origin_location'],
            'destination_location' => $trip['destination_location'],
            'origin_location_code' => $origin_location_code,
            'destination_location_code' => $destination_location_code,
            'origin_location_city' => $origin_location_city,
            'destination_location_city' => $destination_location_city,
            'origin_location_country' => $origin_location_country,
            'destination_location_country' => $destination_location_country,
            'departure_date' => $trip['departure_date'],
            'arrival_date' => $trip['arrival_date'],
            'cabin' => $trip['cabin'],
            'booking_status' => $trip['booking_status'],
            'service' => $service,
            'trip_people' => $trip_people,
            'trip_package' => $trip_package,
            'trip_project' => $trip_project,
            'trip_location' => $trip_location,
            'trip_people_passangers' => $trip_people_passangers,
            'airport_details' => $airport_details,
            'modified' => $trip['modified']
        );
        // print_r($arr_trips);exit;
        $viewArray = array(
            'adminRow' => $arr_trips,
            'flashMessages' => $flashMessages,
            'action' => $action,
            'id' => $id,
            'site_time_zones' => $this->timeZones
        );

        $viewArray['Menu_select'] = 'seeker_trips';
        $viewArray['Sub_menu_select'] = 'travellermanager';

        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
            ->setTerminal(false);
        return $viewModel;
    }
    
    
    
    
    public function disputeServiceAction()
    {  

      

        $this->authenticateModule();
        if ($this->sessionObj->offsetGet('ADMIN_ID') == '') {
            return $this->redirect()->toRoute('admin');
        }

        if(isset($_REQUEST['subAdminBtn']))
        { 
            $this->AdminSeekerModel->UpdateDispute($_REQUEST['comment'],$this->params()->fromQuery('id'));
            $this->flashMessenger()->addMessage(array('alert-success' => "Dispute status updated successfully"));

    }
         $action = $this->params()->fromQuery('action');

         if( $action == 'edit') {

           $disputerow = $this->AdminSeekerModel->getdisputesrow($this->params()->fromQuery('id'));

                 
			$disputerow['seeker_trip_details'] = $this->AdminSeekerModel->getTripRow($disputerow['trip_id'], 'seeker');
            $disputerow['traveller_trip_details'] = $this->AdminSeekerModel->getTripRow($disputerow['trip_id'], 'traveller');
                   
			
			/*if($req['approved'] == '0'){
				$requests[$key]['status'] = 'Pending';
			} else if($req['approved'] == '1'){
				$requests[$key]['status'] = 'Accepted';
			} else if($req['approved'] == '2' || $req['approved'] == '5'){
				$requests[$key]['status'] = 'Declined';
			} else if($req['approved'] == '3'){
				$requests[$key]['status'] = 'Expired';
			} else if($req['approved'] == '4'){
				$requests[$key]['status'] = 'Wishlisted';
			}*/

           return new ViewModel(array_merge($this->params()->fromQuery(),
               ['disputerow' => $disputerow],
             
               ['action' => $action],
               ['flashMessages' => $this->flashMessenger()->getMessages()],
               ['Menu_select' => 'seeker_trips'],
               ['Sub_menu_select' => 'dispute']
           ));
         }


         if( $action == 'view') {

            $disputerow = $this->AdminSeekerModel->getdisputesrow($this->params()->fromQuery('id'));
 
                  
             $disputerow['seeker_trip_details'] = $this->AdminSeekerModel->getTripRow($disputerow['trip_id'], 'seeker');
             $disputerow['traveller_trip_details'] = $this->AdminSeekerModel->getTripRow($disputerow['trip_id'], 'traveller');
                    
             
             /*if($req['approved'] == '0'){
                 $requests[$key]['status'] = 'Pending';
             } else if($req['approved'] == '1'){
                 $requests[$key]['status'] = 'Accepted';
             } else if($req['approved'] == '2' || $req['approved'] == '5'){
                 $requests[$key]['status'] = 'Declined';
             } else if($req['approved'] == '3'){
                 $requests[$key]['status'] = 'Expired';
             } else if($req['approved'] == '4'){
                 $requests[$key]['status'] = 'Wishlisted';
             }*/
 
            return new ViewModel(array_merge($this->params()->fromQuery(),
                ['disputerow' => $disputerow],
              
                ['action' => $action],
                ['flashMessages' => $this->flashMessenger()->getMessages()],
                ['Menu_select' => 'seeker_trips'],
                ['Sub_menu_select' => 'dispute']
            ));
          }
 



         if( $action == 'enable') {

            $disputerow = $this->AdminSeekerModel->UpdateDisputeStatus('enable',$this->params()->fromQuery('id'));
 
            $this->flashMessenger()->addMessage(array('alert-success' => "Dispute status updated successfully"));
 
     
          }
          else if( $action == 'disable') {

            $disputerow = $this->AdminSeekerModel->UpdateDisputeStatus('disable',$this->params()->fromQuery('id'));
 
            $this->flashMessenger()->addMessage(array('alert-success' => "Dispute status updated successfully"));
     
          }
    

        $filterData = array();
        $filterData['sortBy'] = $this->params()->fromQuery('sortBy');
        $filterData['sortOrder'] = $this->params()->fromQuery('sortOrder');
        $filterData['searchKey'] = $this->params()->fromQuery('searchKey');
        $filterData['searchBy'] = $this->params()->fromQuery('searchBy');
        $filterData['searchStatus'] = $this->params()->fromQuery('searchStatus');
        $filterData['status'] = $this->params()->fromQuery('status');
        $filterData['usertype'] = 'seeker';
       
        
        if ($this->params()->fromPost('doAction')) { 
            $doAction = $this->params()->fromPost('doAction');
            $resp = $this->AdminModel->checkAdminPrivilage($doAction == 'delete' ? 'DELETE' : 'UPDATE');
            if ($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                

            }
            $status = $this->AdminSeekerModel->doUserActions($doAction, $this->params()->fromPost('ids'));
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
        } else if ($this->params()->fromQuery('doAction') && $this->params()->fromQuery('id')) {
            $doAction = $this->params()->fromQuery('doAction');
            $id = $this->params()->fromQuery('id');
            $val = $this->params()->fromQuery('val');
            $uId = $this->params()->fromQuery('uId');
            $listUrl = $this->params()->fromQuery('listUrl');
            $status = $this->AdminSeekerModel->doUserActions($doAction, $id, $val);
            $this->flashMessenger()->addMessage(array('alert-success' => $status));
        }

        

        $trips = $this->AdminSeekerModel->getdisputes($filterData);
        //echo '<pre>'; print_r($trips); exit;

        $data = array();

        $requests =   $trips = $this->AdminSeekerModel->getdisputes($filterData);
	
		foreach($requests as $key=>$req){
           
			$requests[$key]['seeker_trip_details'] = $this->AdminSeekerModel->getTripRow($req['trip_id'], 'seeker');
                        $requests[$key]['traveller_trip_details'] = $this->AdminSeekerModel->getTripRow($req['trip_id'], 'traveller');
                   
			
			/*if($req['approved'] == '0'){
				$requests[$key]['status'] = 'Pending';
			} else if($req['approved'] == '1'){
				$requests[$key]['status'] = 'Accepted';
			} else if($req['approved'] == '2' || $req['approved'] == '5'){
				$requests[$key]['status'] = 'Declined';
			} else if($req['approved'] == '3'){
				$requests[$key]['status'] = 'Expired';
			} else if($req['approved'] == '4'){
				$requests[$key]['status'] = 'Wishlisted';
			}*/
		}
	
		$data = !empty($requests) ? $requests : array();
		
        
      // print_r($data);
        
        
        
     

        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
            ->setItemCountPerPage(self::ITEM_PER_PAGE);
        return new ViewModel(array_merge($this->params()->fromQuery(),
            ['paginator' => $paginator],
            ['filterData' => $filterData],
            ['action' => $action],
            ['flashMessages' => $this->flashMessenger()->getMessages()],
            ['Menu_select' => 'seeker_trips'],
            ['Sub_menu_select' => 'dispute']
        ));
    }

}

?>