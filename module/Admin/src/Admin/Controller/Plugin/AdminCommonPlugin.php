<?php
namespace Admin\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

//require_once(ACTUAL_ROOTPATH.'traity/jwt/src/JWT.php');
//use \Firebase\JWT\JWT;

class AdminCommonPlugin extends AbstractPlugin
{
    var $AdminModel;
    
    public function __construct($data=false){
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
    }
   
    public function encrypt($string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        //do the encyption given text/string/number
        $output = openssl_encrypt($string, $encrypt_method, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }

    public function decrypt($string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        //pls set your unique hashing key
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        // hash
        $key = hash('sha256', $secret_key);
        // iv - encrypt method AES-256-CBC expects 16 bytes - else you will get a warning
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        //decrypt the given text/string/number
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }


    public function curl_get_file_contents($URL) {
        $c = curl_init();
        curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($c, CURLOPT_URL, $URL);
        $contents = curl_exec($c);
        $err  = curl_getinfo($c,CURLINFO_HTTP_CODE);
        curl_close($c);
        if ($contents) return $contents;
        else return FALSE;
    }

    public  function calculateAge( $dob, $tdate ){
        $age = 0;
        while( $tdate > $dob = strtotime('+1 year', $dob)){
            ++$age;
        }
        return $age;
    }
    
    public function datediffcal($from,$to){
        $days = floor((strtotime($from) - strtotime($to))/(60*60*24));
        return $days;
    }
      public function addTripOrginLocation() {
        extract($_POST);
        $contact_address_type = $this->AdminModel->cleanQuery($_POST['contact_address_type']);
        if ($contact_address_type == 'existing_address')
            $user_location = $this->AdminModel->cleanQuery($_POST['existing_user_location']);
        else{
            $trip_origin_street_address = $this->AdminModel->cleanQuery($route);
            $arr_trip_contact = array(
                'user' => $this->AdminModel->cleanQuery($userid),
                'name' => $this->AdminModel->cleanQuery(isset($contact_name)?$contact_name:''),
                'street_address_1' => $this->AdminModel->cleanQuery($route),
                'street_address_2' => $this->AdminModel->cleanQuery($sublocality),
                'city' => $this->AdminModel->cleanQuery($locality),
                'state' => $this->AdminModel->cleanQuery($administrative_area_level_1),
                'zip_code' => $this->AdminModel->cleanQuery($postal_code),
                'country' => $this->AdminModel->cleanQuery($country_short),
                'latitude' => $this->AdminModel->cleanQuery($lat),
                'longitude' => $this->AdminModel->cleanQuery($lng),
                'modified' => date('Y-m-d H:i:s')
            );
            $user_location = $this->AdminSeekerModel->addTripContactAddress($arr_trip_contact);
        }
        return $user_location;
}

    public function addTripDetail($user_location,$usertype = 'seeker'){
        
        extract($_POST);
        
        if(isset($travel_plan_type) && $travel_plan_type == 'existing_plan'){
            $ID = $this->AdminModel->cleanQuery(($travel_plan));
            $arr_trip_detail = array(
                'travel_agency_name' => $this->AdminModel->cleanQuery(isset($travel_agency_name_ajax)?$travel_agency_name_ajax:''),
                'travel_agency_url' => $this->AdminModel->cleanQuery(isset($travel_agency_url_ajax)?$travel_agency_url_ajax:''),
                'travel_agency_confirmation' => $this->AdminModel->cleanQuery(isset($travel_agency_confirmation_ajax)?$travel_agency_confirmation_ajax:''),
                'travel_agency_contact_name' => $this->AdminModel->cleanQuery(isset($travel_agency_contact_name_ajax)?$travel_agency_contact_name_ajax:''),
                'travel_agency_phone' => $this->AdminModel->cleanQuery(isset($travel_agency_phone_ajax)?$travel_agency_phone_ajax:''),
                'agency_email' => $this->AdminModel->cleanQuery(isset($agency_email_ajax)?$agency_email_ajax:''),
                'booking_site_name' => $this->AdminModel->cleanQuery(isset($booking_site_name_ajax)?$booking_site_name_ajax:''),
                'booking_site_url' => $this->AdminModel->cleanQuery(isset($booking_site_url_ajax)?$booking_site_url_ajax:''),
                'booking_site_phone' => $this->AdminModel->cleanQuery(isset($booking_site_phone_ajax)?$booking_site_phone_ajax:''),
                'booking_site_email' => $this->AdminModel->cleanQuery(isset($booking_site_email_ajax)?$booking_site_email_ajax:''),
                'booking_date' => strtotime($this->AdminModel->cleanQuery(isset($booking_date_ajax)?$booking_date_ajax:'')),
                'booking_reference' => $this->AdminModel->cleanQuery(isset($booking_reference_ajax)?$booking_reference_ajax:''),
                'total_cost' => $this->AdminModel->cleanQuery(isset($total_cost_ajax)?$total_cost_ajax:''),
                'total_cost_currency' => $this->AdminModel->cleanQuery(isset($total_cost_currency_ajax)?$total_cost_currency_ajax:''),
                'discount_code' => $this->AdminModel->cleanQuery(isset($discount_code_ajax)?$discount_code_ajax:''),
                'total_discount_cost' => $this->AdminModel->cleanQuery(isset($total_discount_cost_ajax)?$total_discount_cost_ajax:''),
                'total_discount_currency' => $this->AdminModel->cleanQuery(isset($total_discount_currency_ajax)?$total_discount_currency_ajax:''),
                'comments_restrictions' => $this->AdminModel->cleanQuery(isset($comments_restrictions_ajax)?$comments_restrictions_ajax:'')
                /*optional fields */
            );

            if($usertype=='seeker'){
                $this->AdminSeekerModel->updateSeekerTrip($arr_trip_detail,$ID);
            }else{
                $this->AdminSeekerModel->updateTrip($arr_trip_detail,$ID);
            } 
        }else{
            $departure_date = (isset($departure_date))?$this->AdminModel->cleanQuery($departure_date):0;
            $departure_time = (isset($departure_time))?$this->AdminModel->cleanQuery($departure_time):'23:59:59';
            $arrival_date = (isset($arrival_date))?$this->AdminModel->cleanQuery($arrival_date):0;
            $arrival_time = isset($arrival_date)?$this->AdminModel->cleanQuery($arrival_date):'23:59:59';
            $arrival_date_time = date('Y-m-d H:i:s', strtotime($arrival_date . ' ' . $arrival_time));
            $departure_date_time = date('Y-m-d H:i:s', strtotime($departure_date . ' ' . $departure_time));
            $date_flexibility = (isset($date_flexibility))?$this->AdminModel->cleanQuery($date_flexibility):0;
            if ($contact_address_type == 'existing_address')
            $user_location = $existing_user_location;
            if(isset($travel_plan_reservation_type) && $travel_plan_reservation_type==2){
                $origin_location_id =  $unplanned_origin_location;
                $destination_location_id =  $unplanned_destination_location;
                $departure_date_time = date('Y-m-d H:i:s', strtotime($unplanned_departure_date));
            } else {
                $travel_plan_reservation_type = 0;
            }
           
            if(!isset($distance))
            $distance = 0;
            
            if(!isset($service_fee))
            $service_fee = 0;
         
            if($usertype =='seeker'){
                $arr_trip_detail = array(
                    
                    'user_location' => $this->AdminModel->cleanQuery($user_location),
                    'travel_plan_reservation_type' => $this->AdminModel->cleanQuery(isset($_POST['travel_plan_reservation_type'])?$_POST['travel_plan_reservation_type']:''),
                    'date_flexibility' => $this->AdminModel->cleanQuery($date_flexibility),
                    'distance' => $this->AdminModel->cleanQuery($distance),
                    'service_fee' => $this->AdminModel->cleanQuery($service_fee),
                    'departure_date' => $this->AdminModel->cleanQuery($departure_date_time),
                    'arrival_date' => $this->AdminModel->cleanQuery($arrival_date_time),
                    'booking_status' => $this->AdminModel->cleanQuery(isset($booking_status)?$booking_status:''),
                    'cabin' => $this->AdminModel->cleanQuery(isset($cabin)?$cabin:''),
                    'seat' => $this->AdminModel->cleanQuery(isset($seat)?$seat:''),
                    'project_start_date' => $this->AdminModel->cleanQuery(isset($project_start_date)?date('Y-m-d H:i:s',strtotime($project_start_date)):0),
                    'project_end_date' => $this->AdminModel->cleanQuery(isset($project_end_date)?date('Y-m-d H:i:s',strtotime($project_end_date)):0),
                    'ticket_option' => $this->AdminModel->cleanQuery(isset($ticketOption)?$ticketOption:NULL),
                    /*optional fields */
                    /* 'ticket_number' => $this->AdminModel->cleanQuery($ticket_number),*/
                    'travel_agency_name' => $this->AdminModel->cleanQuery(isset($travel_agency_name)?$travel_agency_name:''),
                    'travel_agency_url' => $this->AdminModel->cleanQuery(isset($travel_agency_url)?$travel_agency_url:''),
                    'travel_agency_confirmation' => $this->AdminModel->cleanQuery(isset($travel_agency_confirmation)?$travel_agency_confirmation:''),
                    'travel_agency_contact_name' => $this->AdminModel->cleanQuery(isset($travel_agency_contact_name)?$travel_agency_contact_name:''),
                    'travel_agency_phone' => $this->AdminModel->cleanQuery(isset($travel_agency_phone)?$travel_agency_phone:''),
                    'agency_email' => $this->AdminModel->cleanQuery(isset($agency_email)?$agency_email:''),
                    /* 'supplier_name' => $this->AdminModel->cleanQuery(isset($supplier_name)?$supplier_name:''),
                    'supplier_url' => $this->AdminModel->cleanQuery(isset($supplier_url)?$supplier_url:''),
                    'supplier_contact' => $this->AdminModel->cleanQuery(isset($supplier_contact)?$supplier_contact:''),
                    'supplier_phone' => $this->AdminModel->cleanQuery(isset($supplier_phone)?$supplier_phone:''),
                    'supplier_email' => $this->AdminModel->cleanQuery(isset($supplier_email)?$supplier_email:''),*/
                    'booking_site_name' => $this->AdminModel->cleanQuery(isset($booking_site_name)?$booking_site_name:''),
                    'booking_site_url' => $this->AdminModel->cleanQuery(isset($booking_site_url)?$booking_site_url:''),
                    'booking_site_phone' => $this->AdminModel->cleanQuery(isset($booking_site_phone)?$booking_site_phone:''),
                    'booking_site_email' => $this->AdminModel->cleanQuery(isset($booking_site_email)?$booking_site_email:''),
                    'booking_date' => strtotime($this->AdminModel->cleanQuery(isset($booking_date)?$booking_date:'')),
                    'booking_reference' => $this->AdminModel->cleanQuery(isset($booking_reference)?$booking_reference:''),
                    'total_cost' => $this->AdminModel->cleanQuery(isset($total_cost)?$total_cost:''),
                    'total_cost_currency' => $this->AdminModel->cleanQuery(isset($total_cost_currency)?$total_cost_currency:''),
                    'discount_code' => $this->AdminModel->cleanQuery(isset($discount_code)?$discount_code:''),
                    'total_discount_cost' => $this->AdminModel->cleanQuery(isset($total_discount_cost)?$total_discount_cost:''),
                    'total_discount_currency' => $this->AdminModel->cleanQuery(isset($total_discount_currency)?$total_discount_currency:''),
                    'comments_restrictions' => $this->AdminModel->cleanQuery(isset($comments_restrictions)?$comments_restrictions:''),
                    /*optional fields */
                    'active' => 1
                   
                );
                if(isset($_POST['travel_plan_reservation_type']) && $_POST['travel_plan_reservation_type'] == '2'){
                     
                    $o_location_id = (isset($unplanned_origin_location_id) && $unplanned_origin_location_id != '')?$unplanned_origin_location_id:$origin_location_id;
                    $d_location_id = (isset($unplanned_destination_location_id) && $unplanned_destination_location_id != '')?$unplanned_destination_location_id:$destination_location_id;
                    $arr_trip_detail['origin_location'] = $this->AdminModel->cleanQuery($o_location_id);
                    $arr_trip_detail['destination_location'] = $this->AdminModel->cleanQuery($d_location_id);
                  
                }
                else{
                    $arr_trip_detail['origin_location'] = $this->AdminModel->cleanQuery($origin_location_id);
                    $arr_trip_detail['destination_location'] = $this->AdminModel->cleanQuery($destination_location_id);
                }
            }else{
                $arr_trip_detail = array(
                    'user_location' => $this->AdminModel->cleanQuery($user_location),
                    'distance' => $this->AdminModel->cleanQuery($distance),
                    'service_fee' => $this->AdminModel->cleanQuery($service_fee),
                    'origin_location' => $this->AdminModel->cleanQuery($origin_location_id),
                    'destination_location' => $this->AdminModel->cleanQuery($destination_location_id),
                    'departure_date' => $this->AdminModel->cleanQuery($departure_date_time),
                    'arrival_date' => $this->AdminModel->cleanQuery($arrival_date_time),
                    'booking_status' => $this->AdminModel->cleanQuery($booking_status),
                    'cabin' => $this->AdminModel->cleanQuery($cabin),
                    /* //'noofstops' => $this->AdminModel->cleanQuery($nstop),
                    'ticket_image' => $this->AdminModel->cleanQuery($ticket_image),*/
                    
                    /*optional fields */
                   /*  'ticket_number' => $this->AdminModel->cleanQuery($ticket_number),*/
                    'travel_agency_name' => $this->AdminModel->cleanQuery($travel_agency_name),
                    'travel_agency_url' => $this->AdminModel->cleanQuery($travel_agency_url),
                    'travel_agency_confirmation' => $this->AdminModel->cleanQuery($travel_agency_confirmation),
                    'travel_agency_contact_name' => $this->AdminModel->cleanQuery($travel_agency_contact_name),
                    'travel_agency_phone' => $this->AdminModel->cleanQuery($travel_agency_phone),
                    'agency_email' => $this->AdminModel->cleanQuery($agency_email),
                     /*'supplier_name' => $this->AdminModel->cleanQuery($supplier_name),
                    'supplier_url' => $this->AdminModel->cleanQuery($supplier_url),
                    'supplier_contact' => $this->AdminModel->cleanQuery($supplier_contact),
                    'supplier_phone' => $this->AdminModel->cleanQuery($supplier_phone),
                    'supplier_email' => $this->AdminModel->cleanQuery($supplier_email),*/
                    'booking_site_name' => $this->AdminModel->cleanQuery($booking_site_name),
                    'booking_site_url' => $this->AdminModel->cleanQuery($booking_site_url),
                    'booking_site_phone' => $this->AdminModel->cleanQuery($booking_site_phone),
                    'booking_site_email' => $this->AdminModel->cleanQuery($booking_site_email),
                    'booking_date' => strtotime($this->AdminModel->cleanQuery($booking_date)),
                    'booking_reference' => $this->AdminModel->cleanQuery($booking_reference),
                    'total_cost' => $this->AdminModel->cleanQuery($total_cost),
                    'total_cost_currency' => $this->AdminModel->cleanQuery(isset($total_cost_currency)?$total_cost_currency:''),
                    'discount_code' => $this->AdminModel->cleanQuery(isset($discount_code)?$discount_code:''),
                    'total_discount_cost' => $this->AdminModel->cleanQuery(isset($total_discount_cost)?$total_discount_cost:''),
                    'total_discount_currency' => $this->AdminModel->cleanQuery(isset($total_discount_currency)?$total_discount_currency:''),
                    'comments_restrictions' => $this->AdminModel->cleanQuery($comments_restrictions),
                    /*optional fields */
                    
                    'active' => 1,
                   
                    'modified' => date('Y-m-d H:i:s')
                );               
            }
             
            if(isset($typeOfFlightSelect) && $typeOfFlightSelect == 'auto'){
                $arr_trip_detail['noofstops'] = $this->AdminModel->cleanQuery(isset($nstop)?$nstop:0);
            } else if(isset($typeOfFlightSelect) && $typeOfFlightSelect == 'manual'){
                $arr_trip_detail['noofstops'] = $this->AdminModel->cleanQuery($nstop_manual);
            }
            $arr_trip_detail['trip_id_number'] = isset($tripIdNumber)?$this->AdminModel->cleanQuery($tripIdNumber):0;
            $editTripId = isset($trip_edit_id)?$trip_edit_id:0;
            
            if($editTripId){
                if($usertype=='seeker'){
                    $this->AdminSeekerModel->updateSeekerTrip($arr_trip_detail,$editTripId);
                }else{
                    $this->AdminSeekerModel->updateTrip($arr_trip_detail,$editTripId);
                } 
                $ID = $editTripId;
            }else{
                $ID = $this->AdminSeekerModel->addTrip($arr_trip_detail,$usertype);
            }
            
            
            
            if(!empty($selectedFlightData) && $selectedFlightData != false){
                $this->AdminSeekerModel->deleteTripFlightDetails($ID);
                foreach(json_decode($selectedFlightData) as $sFlight){
                    $arr_flight = array(
                        'user_role' => $usertype,
                        'trip' => $ID,
                        'airline_name' => $this->AdminModel->cleanQuery($sFlight->airline_name),
                        'airline_number' => $this->AdminModel->cleanQuery($sFlight->airline_number),
                        'carrier' => $this->AdminModel->cleanQuery($sFlight->carrier),
                        'number' => $this->AdminModel->cleanQuery($sFlight->number),
                        'departure' => $this->AdminModel->cleanQuery($sFlight->departure),
                        'arrival' => $this->AdminModel->cleanQuery($sFlight->arrival),
                        'departure_date' => $this->AdminModel->cleanQuery($sFlight->departure_date),
                        'arrival_date' => $this->AdminModel->cleanQuery($sFlight->arrival_date),
                        'depature_latitude' => $this->AdminModel->cleanQuery($sFlight->depature_latitude),
                        'depature_longitude' => $this->AdminModel->cleanQuery($sFlight->depature_longitude),
                        'arrival_latitude' => $this->AdminModel->cleanQuery($sFlight->arrival_latitude),
                        'arrival_longitude' => $this->AdminModel->cleanQuery($sFlight->arrival_longitude),
                        'modified' => date('Y-m-d H:i:s')
                    );
                    $this->AdminSeekerModel->addTripFlight($arr_flight);
                }
            }
            
            
        }
        return $ID;
    }
      public function addSeekerPeopleService($new_seeker_trip_id) {
        extract($_POST);
        $seekerTripId = $new_seeker_trip_id;
        if(isset($seeker_trip_id) && $seeker_trip_id != '')
            $seekerTripId = $seeker_trip_id;
        $pac_coiunt = (isset($passengers_count) && $passengers_count != '10')?$passengers_count:$passengers_count_more;
        if(!isset($passengers_count))$pac_coiunt=1;
        $pac_coiunt = (isset($trip_edit_id) && !empty($trip_edit_id))?$passengers_count_more:$pac_coiunt;
        
        /* if(!isset($_POST['seeker_trip_id'])){
            $seeker_trip_id = $new_seeker_trip_id;
        }
        $trip_passangers = (isset($passengers_count) && $passengers_count != '10')?$passengers_count:$passengers_count_more;
        if(!isset($passengers_count))$trip_passangers=1; */
        $arr_seeker_people = array(
            'seeker_trip'       => $seekerTripId,
            'title'             => '',
            'passengers'        => $pac_coiunt,
            'contact_name'      => $contact_name,
            'one_of_the_passenger' => (isset($one_of_the_passenger)?$one_of_the_passenger:'No'),
            'contact_number'    => $contact_number,
            'email_address'     => $email_address,
            'comment'           => $comments
        );
        if(isset($seekerTripId) && !empty($seekerTripId)) $this->AdminSeekerModel->deleteSeekerPeopleById($seekerTripId);
        $seeker_people_request_id = $this->AdminSeekerModel->addSeekerPeople($arr_seeker_people);

        for($i=1;$i<=$pac_coiunt;$i++){
            $arr_seeker_people_passengers = array(
                'people_request'      => $seeker_people_request_id,
                'first_name'        => isset($_POST['passenger_first_name_'.$i])?$_POST['passenger_first_name_'.$i]:'',
                'last_name'         => isset($_POST['passenger_last_name_'.$i])?$_POST['passenger_last_name_'.$i]:'',
                'gender'            => isset($_POST['passenger_gender_' . $i])?$_POST['passenger_gender_'.$i]:'',
                'age'               =>isset($_POST['passenger_age_' . $i])? $_POST['passenger_age_'.$i]:'',
                'modified'          => date('Y-m-d H:i:s')
            );
            
            $passId = $this->AdminSeekerModel->addSeekerPeoplePassengers($arr_seeker_people_passengers);
            
            if(isset($_POST['seekerPeopleDocImages_'.$i][0])){
                foreach($_POST['seekerPeopleDocImages_'.$i] as $fileName){
                    $arr_attachment_detail = array(
                        'seeker_people_passengers_id'   => $this->AdminModel->cleanQuery($passId),
                        'seeker_attachment_name'	=> $this->AdminModel->cleanQuery($fileName),
                        'seeker_attachment_added' 	=> time()
                    );
                    $this->AdminSeekerModel->addPassengerAttachment($arr_attachment_detail);
                }
            }
        }     
        return $seeker_people_request_id;
    }
    
    public function addSeekerPackageService($new_seeker_trip_id) {
        extract($_POST);
        $seekerTripId = $new_seeker_trip_id;
        if(isset($seeker_trip_id) && $seeker_trip_id != '')
        $seekerTripId = $seeker_trip_id;
        $pac_coiunt = (isset($packages_count) && $packages_count != '10')?$packages_count:$packages_count_more;
        
        if(!isset($packages_count))$pac_coiunt=1;
        $pac_coiunt = (isset($trip_edit_id) && !empty($trip_edit_id))?$packages_count_more:$pac_coiunt;
       
        $arr_seeker_package = array(
            'seeker_trip'       => $seekerTripId,
            'title'             => '',
            'packages'          => $pac_coiunt,
            'total_weight'      => $total_weight,
            'total_worth'       => $total_worth,
            'total_worth_currency'=> $total_worth_currency,
            'contact_name'      => $contact_name,
            'contact_number'    => $contact_number,
            'email_address'     => $contact_email_address,
            'comment'           => $comments
        );
        if(isset($seekerTripId) && !empty($seekerTripId)) $this->AdminSeekerModel->deleteSeekerPackagesById($seekerTripId);
        $seeker_package_request_id = $this->AdminSeekerModel->addSeekerPackage($arr_seeker_package);
        
        for($i=1;$i<=$pac_coiunt;$i++){
            if(isset($_POST['negligible_weight_'.$i]) && $_POST['negligible_weight_'.$i])
                $negligible_weight = $_POST['negligible_weight_'.$i];
            else
                $negligible_weight =  '';   
            if(isset($_POST['negligible_size_'.$i]) && $_POST['negligible_size_'.$i])
                $negligible_size = $_POST['negligible_size_'.$i];
            else
                $negligible_size =  '';
            
            if(isset($_FILES['package_photo_'.$i]['tmp_name'])){ 
                $fileUpRes = $this->AdminModel->uploadFiles('package_photo_'.$i,'seeker_package_'.time(),'seeker_people_attachment');
                if($fileUpRes['result'] == 'success'){
                    $imageName = $fileUpRes['fileName'];
                }
                else{
                    $imageName = NULL;
                }
            }
            if(isset($_POST['package_photo_edited_'.$i]) && !empty($_POST['package_photo_edited_'.$i])){
                $imageName = $_POST['package_photo_edited_'.$i];
            }
                    
            $arr_seeker_package_packages = array(
                'package_request'       => $seeker_package_request_id,
                'item_category'         => $_POST['category_'.$i],
                'item_sub_category'     => $_POST['item_category_'.$i],
                'item_description'      => $_POST['item_description_'.$i],
                'color'                 => $_POST['color_'.$i],
                'condition'             => $_POST['condition_'.$i],
                /* 'web_site_link'      => $_POST['web_site_link_'.$i],*/
                'photo'                 => isset($imageName)?$imageName:'',
                'item_worth'            => $_POST['item_worth_'.$i],
                'item_worth_currency'   => $_POST['item_worth_currency_'.$i],
                'weight'                => isset($_POST['item_weight_'.$i])?$_POST['item_weight_'.$i]:'',
                'negligible_weight'     => $negligible_weight,
                'length'                => $_POST['item_length_'.$i],
                'width'                 => isset($_POST['item_width_'.$i])?$_POST['item_width_'.$i]:'',
                'height'                => isset($_POST['item_height_'.$i])?$_POST['item_height_'.$i]:'',
                'item_unit'             => isset($_POST['item_unit_'.$i])?$_POST['item_unit_'.$i]:'',
                'negligible_size'       => $negligible_size,
                'item_quantity'         => isset($_POST['item_quantity_' . $i])?$_POST['item_quantity_'.$i]:'',
                'modified'              => date('Y-m-d H:i:s')
            );
            $this->AdminSeekerModel->addSeekerPackagePackages($arr_seeker_package_packages);
        }   
      
    return $seeker_package_request_id;
}

    public function addSeekerProjectService($new_seeker_trip_id) {
    extract($_POST); $ttip_id = 0;
    if(!isset($_POST['seeker_trip_id']))
        $seeker_trip_id = $new_seeker_trip_id;
     
    if($work_category == '1'){
        /*$productCounts = (isset($products_count) && $products_count != '10')?$products_count:$products_count_more;
        if(!isset($products_count))$productCounts=$products_count;*/
        $productCounts = $selected_project_products;
        /*echo $productCounts;exit;*/
        $arr_seeker_project = array(
            'seeker_trip'       => $seeker_trip_id,
            'title'             => '',
            'tasks'             => $productCounts,
            'work_category'     => $work_category
        );
       
        if(isset($seeker_trip_id) && !empty($seeker_trip_id)) $this->AdminSeekerModel->deleteSeekerProjectById($seeker_trip_id);
        $seeker_project_request_id = $this->AdminSeekerModel->addSeekerProject($arr_seeker_project);
       
        for($i=1;$i<=$productCounts;$i++){
            $arr_seeker_project_tasks = array(
                'project_request'                   => $seeker_project_request_id,
                'category'                          => (isset($_POST['product_category_'.$i])?$_POST['product_category_'.$i]:''),
                'description'                       => (isset($_POST['product_description_'.$i])?$_POST['product_description_'.$i]:''),
                'additional_requirements_category'  => (isset($_POST['product_additional_requirements_category_'.$i])?$_POST['product_additional_requirements_category_'.$i]:''),
                'product_sku'                       => (isset($_POST['product_product_sku_'.$i])?$_POST['product_product_sku_'.$i]:''),
                'web_site_link'                     => (isset($_POST['product_web_site_link_'.$i])?$_POST['product_web_site_link_'.$i]:''),
                'price'                             => (isset($_POST['product_price_'.$i])?$_POST['product_price_'.$i]:''),
                'price_currency'                    => (isset($_POST['product_price_currency_'.$i])?$_POST['product_price_currency_'.$i]:''),
                'item_weight'                       => (isset($_POST['item_weight_'.$i])?$_POST['item_weight_'.$i]:''),
                'item_length'                       => (isset($_POST['item_length_'.$i])?$_POST['item_length_'.$i]:''),
                'item_width'                        => (isset($_POST['item_width_'.$i])?$_POST['item_width_'.$i]:''),
                'item_height'                       => (isset($_POST['item_height_'.$i])?$_POST['item_height_'.$i]:''),
                'item_unit'                         => (isset($_POST['item_unit_'.$i])?$_POST['item_unit_'.$i]:''),
                'negligible_weight'                 => (isset($_POST['negligible_weight_'.$i])?$_POST['negligible_weight_'.$i]:''),
                'negligible_size'                   => (isset($_POST['negligible_size_'.$i])?$_POST['negligible_size_'.$i]:''),
                'color'                             => (isset($_POST['product_color_'.$i])?$_POST['product_color_'.$i]:''),
                'task_condition'                    => (isset($_POST['product_condition_'.$i])?$_POST['product_condition_'.$i]:''),
                'quantity'                          => (isset($_POST['item_quantity_'.$i])?$_POST['item_quantity_'.$i]:''),
                'modified'                          => time()
            );
            
            $proId = $this->AdminSeekerModel->addSeekerProjectTasks($arr_seeker_project_tasks);
           
            if(isset($_POST['seekerPeopleDocImages_'.$i][0])){
                foreach($_POST['seekerPeopleDocImages_'.$i] as $fileName){
                    $arr_attachment_detail = array(
                        'seeker_project_tasks_id'   => $this->AdminModel->cleanQuery($proId),
                        'task_photo_name'	=> $this->AdminModel->cleanQuery($fileName),
                        'task_photo_added' 	=> time()
                    );
                    $this->AdminSeekerModel->addProjectAttachment($arr_attachment_detail);
                }
            }
        }
    } else {
        /*$projectCounts = (isset($projects_count) && $projects_count != '10')?$projects_count:$projects_count_more;*/
        //if(!isset($projects_count))$projectCounts=$projects_count;
        $projectCounts = $selected_project_tasks;
        $arr_seeker_project = array(
            'seeker_trip'       => $seeker_trip_id,
            'title'             => '',
            'tasks'             => $projectCounts,
            'work_category'     => $work_category,
            'contact_name'      => $contact_name,
            'contact_number'    => $contact_number,
            'email_address'     => $email_address,
            'comment'           => $comments
        );
        $this->AdminSeekerModel->deleteSeekerProjectById($seeker_trip_id);
        $seeker_project_request_id = $this->AdminSeekerModel->addSeekerProject($arr_seeker_project);
        
        for($i=1;$i<=$projectCounts;$i++){
            $arr_seeker_project_tasks = array(
                'project_request'                   => $seeker_project_request_id,
                'category'                          => $_POST['task_category_'.$i],
                'description'                       => $_POST['task_description_'.$i],
                'additional_requirements_category'  => $_POST['additional_requirements_category_'.$i],
                'service_date'                      => strtotime($_POST['service_date_'.$i]),
                'date_flexibility'                  => $_POST['date_flexibility_'.$i],
                'start_date'                        => date('Y-m-d h:i:s', strtotime($_POST['service_start_date_'.$i])),
                'end_date'                          => date('Y-m-d h:i:s', strtotime($_POST['service_end_date_'.$i])),
                'project_duration'                  => $_POST['task_duration_'.$i],
                'requires_local_travel'             => (isset($_POST['requires_local_travel_'.$i])?$_POST['requires_local_travel_'.$i]:''),
                'payout_type'                       => (isset($_POST['payout_type_'.$i])?$_POST['payout_type_'.$i]:''),
                'cost_willing_to_pay'               => $_POST['cost_willing_to_pay_'.$i],
                'currency_of_payment'               => $_POST['currency_of_payment_'.$i],
                'modified'                          => time()
            );
            $this->AdminSeekerModel->addSeekerProjectTasks($arr_seeker_project_tasks);
        }
    }  
    return $seeker_project_request_id;
}
  public function addTravellerPeopleDetail($trip_ID){
        extract($_POST);
        
        $trip_people = (isset($trip_people_count) && $trip_people_count != '11')?$trip_people_count:$trip_people_count_more;
        $trip_people_male = (isset($trip_people_male_count) && $trip_people_male_count != '11')?$trip_people_male_count:$trip_people_male_count_more;
        $trip_people_female = (isset($trip_people_female_count) && $trip_people_female_count != '11')?$trip_people_female_count:$trip_people_female_count_more;
        $arr_people_detail = array(
            'trip' => $this->AdminModel->cleanQuery($trip_ID),
            'persons_travelling' => $this->AdminModel->cleanQuery($trip_people),
            'male_count' => $this->AdminModel->cleanQuery($trip_people_male),
            'female_count' => $this->AdminModel->cleanQuery($trip_people_female),
            'age_above_60_years' => $this->AdminModel->cleanQuery(isset($trip_people_age_bove_60)?$trip_people_age_bove_60:'No'),
            'age_below_3_years' => $this->AdminModel->cleanQuery(isset($trip_people_age_below_5)?$trip_people_age_below_5:'No'),
            'comment' => $this->AdminModel->cleanQuery($trip_comments),
            'modified' => date('Y-m-d H:i:s')
        );
        $this->AdminSeekerModel->deleteTripPeopleDetail($trip_ID); 
        $ID = $this->AdminSeekerModel->addTripPeopleDetail($arr_people_detail);    
        return $ID;
    }

    public function addTravellerPackageDetail($trip_ID){
        extract($_POST);
         $packages = (isset($packages_count) && $packages_count != '11')?$packages_count:$packages_count_more;
        $arr_package_detail = array(
            'trip' => $this->AdminModel->cleanQuery($trip_ID),
            'packages_count' => $this->AdminModel->cleanQuery($packages),
            'airline_allowed_weight' => $this->AdminModel->cleanQuery($airline_allowed_weight),
            'weight_carried' => $this->AdminModel->cleanQuery($weight_carried),
            'weight_accommodate' => $this->AdminModel->cleanQuery($weight_accommodate),
            'not_wish_to_carry' => $this->AdminModel->cleanQuery($not_wish_to_carry),
            'comment' => $this->AdminModel->cleanQuery($comment),
            'item_worth' => $this->AdminModel->cleanQuery($item_worth),
            'item_worth_currency' => $this->AdminModel->cleanQuery($item_worth_currency),
            'modified' => date('Y-m-d H:i:s')
        ); 
        $this->AdminSeekerModel->deleteTripPackageDetail($trip_ID); 
        $ID = $this->AdminSeekerModel->addTripTravellerPackage($arr_package_detail);
        return $ID;
    }

    public function addTravellerProjectDetail($trip_ID){
        extract($_POST);
        if(!isset($anydate))
            $anydate = '';    
        $project_start_date_time = '';
        if(isset($project_start_date) && !empty($project_start_date)){
            $project_start_date_time = date('Y-m-d H:i:s', strtotime($project_start_date));
        } 
        $project_end_date_time = '';
        if(isset($project_end_date) && !empty($project_end_date)){
            $project_end_date_time = date('Y-m-d H:i:s', strtotime($project_end_date));
        } 
       
        $arr_project_detail = array(
            'trip' => $this->AdminModel->cleanQuery($trip_ID),
            'work_category' => $this->AdminModel->cleanQuery(implode(',',$work_category)),
            'product_type' => $this->AdminModel->cleanQuery($product_type),
            'item_worth' => $this->AdminModel->cleanQuery($item_worth),
            'item_worth_currency' => $this->AdminModel->cleanQuery($item_worth_currency),
            'task' => $this->AdminModel->cleanQuery($task),
            'dates_available' => $this->AdminModel->cleanQuery(isset($anydate)?$anydate:''),
            'start_date' => $this->AdminModel->cleanQuery($project_start_date_time),
            'end_date' => $this->AdminModel->cleanQuery($project_end_date_time),
            'flexible_for_local' => $this->AdminModel->cleanQuery(isset($flexible_for_local)?$flexible_for_local:''),
            'comment' => $this->AdminModel->cleanQuery($comment),
            'modified' => date('Y-m-d H:i:s')
        );
     
        $this->AdminSeekerModel->deleteTripProjectDetails($trip_ID); 
        $ID = $this->AdminSeekerModel->addTripTravellerProject($arr_project_detail);    
        return $ID;
    }

}
?>
