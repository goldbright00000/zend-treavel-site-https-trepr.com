<?php namespace Admin\Controller\Plugin\Factory;

use Admin\Controller\Plugin\AdminCommonPlugin;
use Admin\Model\AdminModel;
use Admin\Model\AdminSeekerModel;

use Interop\Container\ContainerInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\ServiceManager\Factory\FactoryInterface;

class AdminCommonPluginFactory implements FactoryInterface {
    
    var $AdminModel;
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
        $pluginData['configs'] = $container->get('config');
        $pluginData['models'] = array(
            array('name' => 'AdminModel', 'obj' => new AdminModel($dbAdapter, $pluginData['configs'])),
            array('name' => 'AdminSeekerModel', 'obj' => new AdminSeekerModel($dbAdapter,$pluginData['configs']))
        );
        $pluginData['configs'] = $container->get('config');
        return new AdminCommonPlugin($pluginData);
    }
       
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null){
        return $this($container, $requestedName, []);
    }
}