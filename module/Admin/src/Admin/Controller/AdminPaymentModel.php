<?php
namespace Admin\Model;

use Zend\Db\TableGateway\AbstractTableGateway;
use Zend\Db\Adapter\AdapterAwareInterface;
use Zend\Db\Adapter\Adapter;
use Zend\Db\Adapter\Driver\DriverInterface;

use Zend\Db\Sql\Sql;
use Zend\Db\Sql\Select;
use Zend\Db\Sql\Insert;
use Zend\Db\ResultSet\ResultSet;
use Zend\ServiceManager\ServiceManager; 
use Interop\Container\ContainerInterface;
use Zend\Db\TableGateway\TableGatewayInterface;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;

class AdminPaymentModel extends AbstractTableGateway implements AdapterAwareInterface {
    
    protected $table = 'core_country';
    protected $adapter;
    protected $sql;
     protected $generalvar;
   
    public function __construct(Adapter $adapter,$siteConfigs = false) {
        $this->adapter = $adapter;
        $this->sql = new Sql($this->adapter);
        $this->generalvar = $siteConfigs['siteConfigs'];
    }
    
    public function setDbAdapter(Adapter $adapter){
        $this->adapter = $adapter;
        $this->resultSetPrototype = new ResultSet(ResultSet::TYPE_ARRAY);
        $this->initialize();
       
    }
    
   
    public function cleanQuery($string) {
        
        if (get_magic_quotes_gpc()) {
            $string = stripslashes($string);
        }

        if (phpversion() >= '4.3.0') {
        } else {
            $string = mysql_escape_string($string);
        }

        return $string;
    }
    
    public function trimLength($s, $maxlength) {
        $words = explode(' ', $s);
        $split = '';
        foreach ($words as $word) {
            if (strlen($split) + strlen($word) < $maxlength)
                $split .= $word . ' ';
            else
                break;
        }

        if (strlen($s) > $maxlength) {
            $split = trim($split) . " ...";
        }

        return $split;
    }
    
    public function splitImages($img) {
        $img = list($img_name, $img_type) = explode(".", $img);
        $xs = $img_name . "_xs." . strtolower($img_type);
        $th = $img_name . "_th." . strtolower($img_type);
        $logo = $img_name . "_logo." . strtolower($img_type);
        $reg = $img_name . "." . strtolower($img_type);
        $lrg = $img_name . "_lg." . strtolower($img_type);
        $as = $img_name . "_as." . strtolower($img_type);
        $og = $img_name . "_og." . strtolower($img_type);
        $arr_imgs = array("small" => $xs, "thumbnail" => $th, "standard" => $reg, "large" => $lrg, "actual" => $as, "logo" => $logo, "original" => $og);
        return $arr_imgs;
    }
    
    public function timeZoneAdjust($format, $dat) {
        $dat = strtotime($dat);
        if (date('T', $dat) == 'PST') {
            $dateadjusted = date($format, $dat + 3600);
        } else {
            $dateadjusted = date($format, $dat);
        }
        return $dateadjusted;
    }
    
    public function cleanTags($txt) {
        $specialstuff = array("<br />", "<br>", "<ul>", "<li>", "</li>", "</ul>", "<ol>", "</ol>");
        $txt = str_replace($specialstuff, " ", stripslashes($txt));
        return $txt;
    }
    
    public function in_array_r($needle, $haystack) {
        foreach ($haystack as $item) {
            if ($item === $needle || (is_array($item) && $this->in_array_r($needle, $item))) {
                return true;
            }
        }
        return false;
    }
    
    function getUserPaymentMethods($filters=false,$resType='list'){
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array('*'))
            ->from(array('UC' => 'users_user_card'))
            ->join(
                array('U' => 'users_user'),
                'UC.user = U.ID',
                array('email_address'),
                'left'
            );
        if($filters){
            if(isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != ''){
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'].'%');
            }
            if(isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])){
                foreach($filters['where'] as $key => $where){
                    $selectObj->where(array($key=>$where));
                }
            }
            if(isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != ''){
                $selectObj->order(array($filters['sortBy']=>$filters['sortOrder']));
            }
            else{
                $selectObj->order(array('UC.ID'=>'ASC'));
            }
        }
        
        if($resType == 'paginator'){
            return new Paginator(
                new DbSelect($selectObj, $this->adapter, $this->resultSetPrototype)
            );
        } else if($resType == 'list'){
            /*echo $this->sql->getSqlStringForSqlObject($selectObj);exit;*/
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res =  $statement->execute();
            if($res->count() > 0){
                return $res->getResource()->fetchAll();
            }
            else return false;
        } else if($resType == 'count'){
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res =  $statement->execute();
            return $res->count();
        }
    }
    
    function getUserPayments($filters=false,$resType='list'){
        
        $select = $this->sql->select();
        $selectObj = $this->sql->select()->columns(array('*'))
            ->from(array('UP' => 'user_payments'))
            ->join(
                array('U' => 'users_user'),
                'UP.pay_user = U.ID',
                array('email_address'),
                'left'
            )->join(
                array('UU' => 'users_user'),
                'UP.pay_to_user = UU.ID',
                array('paytouser'=>'email_address'),
                'left'
            )->join(
                array('ST' => 'seeker_trips'),
                'UP.pay_trip_id = ST.ID',
                array('s_trip_id_number'=>'trip_id_number'),
                'left'
            )->join(
                array('T' => 'trips'),
                'UP.pay_trip_id = T.ID',
                array('trip_id_number'),
                'left'
            );
        if($filters){
            if(isset($filters['searchBy']) && $filters['searchBy'] != '' && isset($filters['searchKey']) && $filters['searchKey'] != ''){
                $selectObj->where->like($filters['searchBy'], $filters['searchKey'].'%');
            }
            if(isset($filters['where']) && !empty($filters['where']) && is_array($filters['where'])){
                foreach($filters['where'] as $key => $where){
                    $selectObj->where(array($key=>$where));
                }
            }
            if(isset($filters['sortBy']) && $filters['sortBy'] != '' && isset($filters['sortOrder']) && $filters['sortOrder'] != ''){
                $selectObj->order(array($filters['sortBy']=>$filters['sortOrder']));
            }
            else{
                $selectObj->order(array('UP.pay_id'=>'DESC'));
            }
        }
        if(isset($filters['id']) && !empty($filters['id'])){
             $selectObj->where(array('UP.pay_id'=>$filters['id']));
        }
        if($resType == 'paginator'){
            return new Paginator(
                new DbSelect($selectObj, $this->adapter, $this->resultSetPrototype)
            );
        } else if($resType == 'list'){
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res =  $statement->execute();
            if($res->count() > 0){
                return $res->getResource()->fetchAll();
            }
            else return false;
        } else if($resType == 'count'){
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res =  $statement->execute();
            return $res->count();
        }else if($resType == 'row'){
            $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
            $statement = $this->adapter->query($selectString);
            $res =  $statement->execute();
            return $res->current();
        }
    }
    
    function getUserPaymentMethod($id){
        $selectObj = $this->sql->select()->columns(array('*'))
            ->from(array('UC' => 'users_user_card'))
            ->join(
                array('U' => 'users_user'),
                'UC.user = U.ID',
                array('email_address','user_id'=>'ID'),
                'left'
            )
            ->where(array('UC.ID'=>$id));
        $selectString = $this->sql->getSqlStringForSqlObject($selectObj);
        $statement = $this->adapter->query($selectString);
        $res =  $statement->execute();
        if($res->count() > 0){
            return $res->current();
        }
        return false;
    }
    
    public function addUpdateUserPaymentMethodRow($updateData,$id=false) {
        if($id === false){
            /* Add */
            $insert = $this->sql->insert('users_user_card');
            $insert->values($updateData);
            $selectString = $this->sql->getSqlStringForSqlObject($insert);
            $results = $this->adapter->query($selectString, Adapter::QUERY_MODE_EXECUTE);
            return $this->adapter->getDriver()->getLastGeneratedValue();
        } else {
            /* Update */
            $update = $this->sql->update();
            $update->table('users_user_card' );
            $update->set(  $updateData );
            $update->where( array( 'ID' => $id ) );
            $statement  =  $this->sql->prepareStatementForSqlObject( $update );
            $results    = $statement->execute();
            return true;
        }
    }
}