<?php
namespace Admin\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;
use Zend\Db\Adapter\Adapter;
use Zend\ServiceManager\ServiceManager; 
use Admin\Model\AdminModel;
use Zend\Mvc\Plugin\FlashMessenger;
use Zend\Mvc\Plugin\Url;
use Zend\Paginator\Paginator;
use Zend\Paginator\Adapter\DbSelect;
use Zend\Paginator\Adapter\ArrayAdapter;
use Application\Model\TripModel;

class AdminDashboardController extends AbstractActionController
{
     const ITEM_PER_PAGE = 20;
     public function __construct($data = false){
        /*Loading models from factory
         * Call the model object using it's class name
         */
        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->sessionObj = new Container('comSessObj');
        $this->siteConfigs = $data['configs']['siteConfigs'];
        $this->adminModules = $data['configs']['adminModules'];
        $this->adminMenus = $data['configs']['adminMenus'];
        
       if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           // return $this->redirect()->toRoute('admin');
       }
    }
    
    public function indexAction(){
        
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $viewArray['Menu_select'] = 'dashboard';
        $viewArray['flashMessages'] = $this->flashMessenger()->getMessages();
		$viewArray['arrAdminAccounts'] = $this->AdminModel->travelRequestcount($this->sessionObj->offsetGet('ADMIN_ID'));
		$viewArray['travelRequestapproved'] = $this->AdminModel->travelRequestapproved($this->sessionObj->offsetGet('ADMIN_ID'));
		$viewArray['travelRequestpending'] = $this->AdminModel->travelRequestpending($this->sessionObj->offsetGet('ADMIN_ID'));
		$viewArray['travelReqcancelled'] = $this->AdminModel->travelReqcancelled($this->sessionObj->offsetGet('ADMIN_ID'));
		$viewArray['latestUsers'] = $this->AdminModel->latestUsers($this->sessionObj->offsetGet('ADMIN_ID'));
		$viewArray['usersReviews'] = $this->AdminModel->usersReviews($this->sessionObj->offsetGet('ADMIN_ID'));
		
		/** USER REPORT **/
		$viewArray['registeredUsers'] = $this->AdminModel->registeredUsers();
		$viewArray['registeredActiveUsers'] = $this->AdminModel->registeredUsers(array('active'=>1));
		$viewArray['registeredInActiveUsers'] = $this->AdminModel->registeredUsers(array('active'=>0));
		
		/** SEEKER TRIPS **/
		/*$seekerTrips = $this->TripModel->getTravelTrips('upcoming','seeker');
		print_r($seekerTrips);die;*/
		
		$viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)->setTerminal(false);
        return $viewModel;
    }
    public function myaccountAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        if(isset($_POST['myAccountBtn'])){
           $checkEmail =  $this->AdminModel->checkAdminEmailExists($_POST['email'],$this->sessionObj->offsetGet('ADMIN_ID'));
           if($checkEmail === false){
                $updateAccount =  $this->AdminModel->updateAdminRow($this->sessionObj->offsetGet('ADMIN_ID'));
                if( $updateAccount){
                     $this->flashMessenger()->addMessage(array('alert-success' => 'Account update successfully'));
                 }else{
                    $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update account. Please try again'));
                 }
           }else{
                 $this->flashMessenger()->addMessage(array('alert-danger' => 'Email address already being used'));
            }
            return $this->redirect()->toRoute('myaccount');
        }
        
         $viewArray = array(
            'adminRow' => $this->AdminModel->getAdminRow($this->sessionObj->offsetGet('ADMIN_ID')),
            'flashMessages' =>  $this->flashMessenger()->getMessages(),
        );
         $viewArray['Menu_select'] = 'admin_accounts';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);

        return $viewModel;
    }
    public function subadminsAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $filterData = array();
        if($this->params()->fromPost('doAction')){
            $resp = $this->AdminModel->checkAdminPrivilage($doAction=='delete'?'DELETE':'UPDATE');               
            if($resp) {
                $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
                return $this->redirect()->toRoute('subadmins');                      
            } 
           $status = $this->AdminModel->doAdminActions($this->params()->fromPost('doAction'),$this->params()->fromPost('ids'));
             $this->flashMessenger()->addMessage(array('alert-success' => $status));
           return $this->redirect()->toRoute('subadmins');
        }
       $filterData['sortBy'] =  $this->params()->fromQuery('sortBy'); 
       $filterData['sortOrder'] =  $this->params()->fromQuery('sortOrder'); 
       $filterData['searchKey'] =  $this->params()->fromQuery('searchKey'); 
       $filterData['searchBy'] =  $this->params()->fromQuery('searchBy'); 
       $filterData['searchStatus'] =  $this->params()->fromQuery('searchStatus'); 
       
        $data = array();
        $arrAdminAccounts = $this->AdminModel->adminAccouts( $filterData,$this->sessionObj->offsetGet('ADMIN_ID'));
        $data = !empty($arrAdminAccounts)?$arrAdminAccounts:array();
        $page = $this->params()->fromQuery('page', 1);
        $paginator = new Paginator(new ArrayAdapter($data));
        $paginator->setCurrentPageNumber($page)
                  ->setItemCountPerPage(self::ITEM_PER_PAGE);
 
        return new ViewModel(array_merge($this->params()->fromQuery(), ['paginator' => $paginator],['filterData'=>$filterData],['flashMessages' =>  $this->flashMessenger()->getMessages()],['Menu_select'=>'admin_accounts']));
      
    } 
    public function subadminmanagerAction(){
        $this->authenticateModule();
        if($this->sessionObj->offsetGet('ADMIN_ID') == ''){
           return $this->redirect()->toRoute('admin');
        }
        $flashMessages = array();
        $action =  $this->params()->fromQuery('action','add'); 
        $id =  $this->params()->fromQuery('id',0); 
        if($action == 'add') $priv = 'INSERT';
        else if($action == 'view') $priv = 'VIEW';
        else  $priv = 'UPDATE';
        $resp = $this->AdminModel->checkAdminPrivilage($priv);               
        if($resp) {
            $this->flashMessenger()->addMessage(array('alert-danger' => "You don't have enough privilege to process the request"));
            return $this->redirect()->toRoute('subadmins');                      
        }
        if(isset($_POST['subAdminBtn'])){
            $checkEmail =  $this->AdminModel->checkAdminEmailExists($_POST['email_address'],$_POST['id']);
           if($checkEmail === false){
               $insData['email_address'] = $_POST['email_address'];
               $insData['username'] = $_POST['username'];
               if(isset($_POST['password']) && !empty($_POST['password']))
               $insData['password'] = md5($_POST['password']);
               $insData['first_name'] = $_POST['first_name'];
               $insData['type'] = $_POST['type'];
               $insData['active'] = 1;
               $insData['accountToken'] = md5($_POST['email_address']);
               $insData['accessModules'] = (isset($_POST['accessModule']) && !empty($_POST['accessModule']))?implode('|',$_POST['accessModule']):'';
               $insData['accessPrivilege'] = (isset($_POST['accessPrivilege']) && !empty($_POST['accessPrivilege']))?implode('|',$_POST['accessPrivilege']):'';
               $updateAccount =  $this->AdminModel->addEditAdminAccounts($insData,$_POST['id'],$_POST['action']);
               $message = ($action == 'add')?'Admin account created successfully':'Admin account updated  successfully';
               if( $updateAccount){
                 $this->flashMessenger()->addMessage(array('alert-success' =>$message));
               }else{
                 $this->flashMessenger()->addMessage(array('alert-danger' => 'Failed to update account. Please try again'));
               }
                 $flashMessages =  $this->flashMessenger()->getMessages();
                 return $this->redirect()->toRoute('subadmins'); 
           }else{
                $flashMessages[] =  array('alert-danger' => 'Email address already being used');
             }
        }
        $viewArray = array(
             'adminRow' => $this->AdminModel->getAdminRow($id),
             'flashMessages' =>   $flashMessages,
             'action' => $action,
             'id' => $id
        );
        $viewArray['Menu_select'] = 'admin_accounts';
        $viewModel = new ViewModel();
        $viewModel->setVariables($viewArray)
                    ->setTerminal(false);
        return $viewModel;  
    } 
     public function authenticateModule() {
        $resp = $this->AdminModel->authenticateAdmin('user_accounts');
        if($resp) { 
            $this->flashMessenger()->addMessage($resp['msg']);
            return $this->redirect()->toRoute($resp['redirectUrl']); 
        }
    }
     
}
 
 ?>