<?php
namespace Admin\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Session\Container;

class AdminCommon extends AbstractHelper {
    
    public function __construct($data=false){
        /*Loading models from factory
         * Call the model object using it's class name
         */  
        if($data['models'] && is_array($data['models'])){
            foreach($data['models'] as $model){
                $modelName = $model['name'];
                $this->$modelName = $model['obj'];
            }
        }
        $this->currentController = 'Index';//$data['currentController'];
        $session = new Container('comSessObj');
         //$this->comSessObj = $data['comSessObj'];
        $this->comSessObj = $session;
    }
 
    public function decrypt($string) {
        $output = false;
        $encrypt_method = "AES-256-CBC";
        $secret_key = 'trepr';
        $secret_iv = 'trepr321';
        $key = hash('sha256', $secret_key);
        $iv = substr(hash('sha256', $secret_iv), 0, 16);
        $output = openssl_decrypt(base64_decode($string), $encrypt_method, $key, 0, $iv);
        return $output;
    }
}

