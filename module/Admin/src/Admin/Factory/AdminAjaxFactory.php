<?php
namespace Admin\Factory;

//related controller
use Admin\Controller\AdminAjaxController;

//models to load
use Admin\Model\AdminModel;
use Admin\Model\AdminUserModel;
use Admin\Model\AdminPaymentModel;
//configuration classes
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class AdminAjaxFactory implements FactoryInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
        
        $controllerData['configs'] = $container->get('config');
        $controllerData['models'] = array(
            array('name' => 'AdminModel', 'obj' => new AdminModel($dbAdapter,$controllerData['configs'])),
            array('name' => 'AdminUserModel', 'obj' => new AdminUserModel($dbAdapter,$controllerData['configs'])),
            array('name' => 'AdminPaymentModel', 'obj' => new AdminPaymentModel($dbAdapter,$controllerData['configs'])),
        );
       
        $controllerData['configs'] = $container->get('config');
        return new AdminAjaxController($controllerData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        
        return $this($container, $requestedName, []);
    }
}