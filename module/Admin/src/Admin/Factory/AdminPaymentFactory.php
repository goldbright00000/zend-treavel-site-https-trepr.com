<?php
namespace Admin\Factory;

//related controller
use Admin\Controller\AdminPaymentController;

//models to load
use Admin\Model\AdminModel;
use Admin\Model\AdminUserModel;
use Admin\Model\AdminMailModel;
use Admin\Model\AdminSeekerModel;
use Admin\Model\AdminPaymentModel;
//configuration classes
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;


class AdminPaymentFactory implements FactoryInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
        
        $controllerData['configs'] = $container->get('config');
        $controllerData['models'] = array(
            array('name' => 'AdminModel', 'obj' => new AdminModel($dbAdapter,$controllerData['configs'])),
            array('name' => 'AdminUserModel', 'obj' => new AdminUserModel($dbAdapter,$controllerData['configs'])),
            array('name' => 'AdminMailModel', 'obj' => new AdminMailModel($dbAdapter,$controllerData['configs'])),
            array('name' => 'AdminSeekerModel', 'obj' => new AdminSeekerModel($dbAdapter,$controllerData['configs'])),
            array('name' => 'AdminPaymentModel', 'obj' => new AdminPaymentModel($dbAdapter,$controllerData['configs']))
        );
       
        $controllerData['configs'] = $container->get('config');
        return new AdminPaymentController($controllerData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        
        return $this($container, $requestedName, []);
    }
}