<?php
namespace Admin\Factory;

//related controller
use Admin\View\Helper\AdminCommon;

//models to load
use Admin\Model\AdminModel;
//configuration classes
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
 use	ZendMvc\Router\RouteMatch;
// use	ZendMvc\Router\RouteMatch::getParams()
class AdminHelperFactory implements FactoryInterface {
   
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        $dbAdapter = $container->get('db_adapter');
         $controllerData['configs'] = $container->get('config');
        $helperData['models'] = array(
             array('name' => 'AdminModel', 'obj' => new AdminModel($dbAdapter,$controllerData['configs'])),
        );
        return new AdminCommon($helperData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        $dbAdapter = $container->get('db_adapter');
         $controllerData['configs'] = $container->get('config');
        $helperData['models'] = array(
            array('name' => 'AdminModel', 'obj' => new AdminModel($dbAdapter,$controllerData['configs'])),
        );
        $currentController = $container->get('application')->getMvcEvent()->getRouteMatch()->getParams('controller');
        $currentController = array_pop(explode('\\',$currentController['controller']));
        $helperData['currentController'] = $currentController;
        //return $this($container, $requestedName, []);
        return new AdminCommon($helperData);
    }
}