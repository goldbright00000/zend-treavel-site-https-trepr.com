<?php namespace Admin\Factory;

//related controller
use Admin\Controller\AdminController;

//models to load
use Admin\Model\AdminModel;

//configuration classes
use Interop\Container\ContainerInterface;
use Zend\ServiceManager\FactoryInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;


class AdminFactory implements FactoryInterface {
    
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null){
        
        $dbAdapter = $container->get('db_adapter');
        $controllerData['configs'] = $container->get('config');
        $sessionObj = new Container('comSessObj');
        $controllerData['models'] = array(
            array('name' => 'AdminModel', 'obj' => new AdminModel($dbAdapter,$controllerData['configs'],$sessionObj)) 
        );
        //$controllerData['configs'] = $container->get('config');
        return new AdminController($controllerData);
    }
    
    public function createService(ServiceLocatorInterface $container, $name = null, $requestedName = null)
    {
        
        return $this($container, $requestedName, []);
    }
}