<?php
 // Filename: /module/Blog/config/module.config.php
namespace Admin;

use Zend\Router\Http\Literal;
use Zend\Router\Http\Segment;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\Router\Http\Regex;
use Admin\Route\StaticRoute;
use Zend\Session\Container;

use Admin\Controller\Factory\AdminFactory;
use Admin\Factory\AdminHelperFactory;
use Admin\View\Helper\AdminCommon;

use Admin\Controller\Plugin\AdminCommonPlugin;
use Admin\Controller\Plugin\Factory\AdminCommonPluginFactory;

 return [      
    'controllers'  => [
        'factories' => [
            'Admin\Controller\AdminController' => 'Admin\Factory\AdminFactory',
            'Admin\Controller\AdminDashboardController' => 'Admin\Factory\AdminDashboardFactory',
            'Admin\Controller\AdminUserController' => 'Admin\Factory\AdminUserFactory',
            'Admin\Controller\AdminSeekerController' => 'Admin\Factory\AdminSeekerFactory',
            'Admin\Controller\AdminTravellerServiceController' => 'Admin\Factory\AdminTravellerServiceFactory',
            'Admin\Controller\AdminSeekerServiceController' => 'Admin\Factory\AdminSeekerServiceFactory',
            'Admin\Controller\AdminAjaxController' => 'Admin\Factory\AdminAjaxFactory',
            'Admin\Controller\MasterdataController' => 'Admin\Factory\AdminMasterdataFactory',  
            'Admin\Controller\AdminPriceSettingsController' => 'Admin\Factory\AdminPriceSettingsFactory', 
            'Admin\Controller\AdminPaymentController' => 'Admin\Factory\AdminPaymentFactory', 
        ]
    ],
    'service_manager'=>[
        'factories' => [
            'Zend\Db\Adapter\Adapter' => 'Zend\Db\Adapter\AdapterServiceFactory',
            'Zend\Mvc\Controller\Plugin\FlashMessenger' => 'Zend\Mvc\Controller\Plugin\FlashMessenger',
        ],
        'initializers' => [
            function ($instance, $sm) {
                if ($instance instanceof \Zend\Db\Adapter\AdapterAwareInterface) {
                    $instance->setDbAdapter($sm->get('Zend\Db\Adapter\Adapter'));
                }
            }
        ],
        'invokables' => [
            'Admin\Model\AdminModel' => 'Admin\Model\AdminModel',
            'Admin\Model\AdminUserModel' => 'Admin\Model\AdminUserModel'
        ],
        'abstract_factories' => [
           \Zend\Db\Adapter\AdapterAbstractServiceFactory::class,
        ]
    ],
    'controller_plugins' => [
        'aliases' => [
            'AdminCommonPlugin' => Controller\Plugin\AdminCommonPlugin::class,
        ],
        'factories' => [
            Controller\Plugin\AdminCommonPlugin::class => AdminCommonPluginFactory::class
        ]
    ],       
    'view_manager' => ['display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'XHTML1_TRANSITIONAL',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => [
            'admin/layout'      => __DIR__ . '/../../Admin/view/layout/admin-layout.phtml',
            'error/404'         => __DIR__ . '/../view/error/404.phtml',
            'error/index'       => __DIR__ . '/../view/error/index.phtml',
            'flash-message'     => __DIR__ . '/../../Admin/view/layout/flash-messages.phtml',
            'download/download-csv'     => __DIR__ . '/../../Admin/view/download/download-csv.phtml',
        ],
        'template_path_stack' => [
            'admin' => __DIR__ . '/../../Admin/view',
        ],
    ],
    'moduleLayouts' => [
        'Admin' => 'admin/layout',
    ],
    'view_helpers' => [
        'factories' => [
            //View\Helper\Common::class => InvokableFactory::class
            View\Helper\AdminCommon::class => AdminHelperFactory::class
        ],
        'aliases' => [
            'AdminCommon' => View\Helper\AdminCommon::class
        ],
    ],
	
	
	/*  New Code Senthilk    */
	
	'checkflightstatus' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/seeker/checkflightstatus',
                    'defaults' => [
                        'controller' => Controller\SeekerController::class,
                        'action'     => 'checkflightStatus',
                    ],
                ],
            ],
	
	
	
	/*  New Code End       */
	
  /*
     'view_manager' => [
         'template_path_stack' => [
             __DIR__ . '/../view',
             ],
        ],*/

     // This lines opens the configuration for the RouteManager
    'router' => [
        'routes' => [
            'admin' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
			'alogout' => [
                'type' => Literal::class,
                    // Configure the route itself
                'options' => [
                    'route'    => '/admin/logout',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminController::class,
                        'action'     => 'logout',
                    ]
                ]
            ],
            'admin_dashboard' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/dashboard',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminDashboardController::class,
                        'action'     => 'index',
                    ]
                ]
            ],
            'myaccount' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/myaccount',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminDashboardController::class,
                        'action'     => 'myaccount',
                    ]
                ]
            ],
            'subadmins' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/subadmins',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminDashboardController::class,
                        'action'     => 'subadmins',
                    ]
                ]
            ],
            'subadminmanager' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/subadminmanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminDashboardController::class,
                        'action'     => 'subadminmanager',
                    ]
                ]
            ],
            'useraccounts' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/useraccounts',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'index',
                    ]
                ]
            ],
            'usermanager' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/usermanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'usermanager',
                    ]
                ]
            ],


            'coupons' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/coupons',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'coupons',
                    ]
                ]
            ],

            'couponmanager' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/couponmanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'couponmanager',
                    ]
                ]
            ],
            'getlatestnotifications' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/getlatestnotifications',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'getlatestnotifications',
                    ]
                ]
            ],
            'usermanagerWithParam' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/usermanager?[:action]',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'usermanager',
                    ]
                ]
            ],
            'users' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users[/:action]',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            'useraddresses' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/useraddresses',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'useraddresses',
                    ],
                ],
            ],
			'userreviewslist' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/userreviewslist',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'userreviewslist',
                    ],
                ],
            ],
            
            'useraddressmanager' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/useraddressmanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'useraddressmanager',
                    ],
                ],
            ],
            'usertrustverification' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/usertrustverification',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'usertrustverification',
                    ],
                ],
            ],
            
            'usertrustverificationmanager' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/usertrustverificationmanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'usertrustverificationmanager',
                    ],
                ],
            ],
             'usertrustWithParam' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/usertrustverificationmanager?[:action]',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'usertrustverificationmanager',
                    ]
                ]
            ], 
            'userphotos' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/userphotos',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'userphotos',
                    ],
                ],
            ],
            'userphotomanager' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/userphotomanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'userphotomanager',
                    ],
                ],
            ],
			
		/*  New Code Senthilk   */
			
			'userphotoinfo' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/userphotoinfo',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'userphotoinfo',
                    ],
                ],
            ],
		
        /* New Code End */	
		
            'uservideos' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/uservideos',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'uservideos',
                    ],
                ],
            ],
            'uservideomanager' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/uservideomanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'uservideomanager',
                    ],
                ],
            ],
            'userreferences' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/userreferences',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'userreferences',
                    ],
                ],
            ],
            'userreferrals' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/userreferrals',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'userreferrals',
                    ],
                ],
            ],
            'userreferencesmanager' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/userreferencesmanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'userreferencesmanager',
                    ],
                ],
            ],
			
            'ajax' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/ajax[/:action]',
                    'defaults' => [
                        'controller' => Controller\AdminAjaxController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
			
            'admin_price_settings' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/price_settings[/:action][/:type]',
                    'defaults' => [
                        'controller' => Controller\AdminPriceSettingsController::class,
                        'action'     => 'index',
                        'type'     => 'country',
                    ],
                ],
            ],
            'admin_masterdata' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata[/:action]',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'mailtemplate',
                    ],
                ],
            ],
            'admin_static_manager' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/static_manager',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'staticmanager',
                    ],
                ],
            ],
            'admin_customs_excise_duty_list' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/customs_excise_duty_list',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'customs',
                        'type'     => 'custom',
                    ],
                ],
            ],
            'admin_report_list' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/report_list',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'reportlist',
                    ],
                ],
            ],
            'admin_message_list' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/message_list',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'messagelist',
                    ],
                ],
            ],
            'admin_country_list' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/country_list',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'countrylist',
                    ],
                ],
            ],
            'admin_country_manager' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/country_manager',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'countrymanager',
                    ],
                ],
            ],
            'admin_language_list' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/language_list',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'languagelist',
                    ],
                ],
            ],
            'admin_language_manager' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/language_manager',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'languagemanager',
                    ],
                ],
            ],
           'mailtemplate' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/mailtemplate',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'mailtemplate',
                    ],
                ],
            ],
            'admin_seekers' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/travels',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'index',
                    ]
                ]
            ],
            'admin_misc' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/misc',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminPriceSettingsController::class,
                        'action'     => 'index',
                    ]
                ]
            ],
            'admin_seekers_people' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seekers',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'seekerService',
                    ]
                ]
            ],
            'admin_seekers_request' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seekersrequests',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'seekerRequestService',
                    ]
                ]
            ],
			'admin_seekers_request_info' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seekersrequests-info',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'seekerRequestServiceInfo',
                    ]
                ]
            ],
			'admin_seekersticket_people' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seekersticket',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'seekersticketService',
                    ]
                ]
            ],
             'admin_traveller_service' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/travellers',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'travellerService',
                    ]
                ]
            ],
			'admin_travelers_request' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/travelersrequests',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'travelerRequestService',
                    ]
                ]
            ],
            'admin_travelers_request_info' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/travelersrequests-info',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'travelerRequestServiceInfo',
                    ]
                ]
            ],
            'admin_travelers_request_dispute' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/dispute',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'disputeService',
                    ]
                ]
            ],
            
            
			'admin_travellerticket_service' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/travellersticket',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'travellersticketService',
                    ]
                ]
            ],
            
             'seekermanager' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seekermanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'seekermanager',
                    ]
                ]
            ],
             'travellermanager' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/travellermanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'travellermanager',
                    ]
                ]
            ],
			'travellerticketmanager' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/travellerticketmanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerController::class,
                        'action'     => 'travellerticketmanager',
                    ]
                ]
            ],
             'travellermanagerWithParam' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/travellermanager?[:action]',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'travellermanager',
                    ]
                ]
            ],

				'travellerenqlist' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/travellerenqlist',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'travellerenqlist',
                    ]
                ]
            ],
			
			'seekerenqlist' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/users/seekerenqlist',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'seekerenqlist',
                    ]
                ]
            ],
			
            'admin-download-file' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/download[/:folder][/:file]',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminAjaxController::class,
                        'action'     => 'download',
                    ]
                ]
            ], 
            'admin-services' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/services',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminTravellerServiceController::class,
                        'action'     => 'index',
                    ]
                ]
            ], 
            'admin-traveller-people' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/traveller/people',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminTravellerServiceController::class,
                        'action'     => 'travellerPeopleService',
                    ]
                ]
            ], 
            'admin-traveller-package' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/traveller/package',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminTravellerServiceController::class,
                        'action'     => 'travellerPackageService',
                    ]
                ]
            ], 
            
             'admin-traveller-project' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/traveller/project',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminTravellerServiceController::class,
                        'action'     => 'travellerProjectService',
                    ]
                ]
            ],
			
			
			'admin-traveller-wishlist' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/traveller/travellerwishlist',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminTravellerServiceController::class,
                        'action'     => 'travellerwishlist',
                    ]
                ]
            ],
			
			'admin-traveller-wishlit-manager' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/travellerwishlistmanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminTravellerServiceController::class,
                        'action'     => 'travellerwishlistmanager',
                    ]
                ]
            ],
			
            
            
            'admin-traveller-service-manager' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/travelleservicermanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminTravellerServiceController::class,
                        'action'     => 'travelleservicermanager',
                    ]
                ]
            ], 
            
            
            
             'admin-seeker-people' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seeker/people',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerServiceController::class,
                        'action'     => 'seekerPeopleService',
                    ]
                ]
            ], 
			
			'admin-seeker-wishlist' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seeker/seekerwishlist',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerServiceController::class,
                        'action'     => 'seekerwishlist',
                    ]
                ]
            ],
			
            'admin-seeker-package' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seeker/package',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerServiceController::class,
                        'action'     => 'seekerPackageService',
                    ]
                ]
            ], 
            
             'admin-seeker-project' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seeker/project',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerServiceController::class,
                        'action'     => 'seekerProjectService',
                    ]
                ]
            ], 
            
            
            'admin-seeker-service-manager' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seekerservicermanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerServiceController::class,
                        'action'     => 'seekerservicermanager',
                    ]
                ]
            ], 
			
			'admin-seeker-wishlit-manager' => [
                'type' => Literal::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/seekerwishlistmanager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminSeekerServiceController::class,
                        'action'     => 'seekerwishlistmanager',
                    ]
                ]
            ],
            
            'admin-payments' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/payments[/:action][/:id]',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminPaymentController::class,
                        'action'     => 'index',
                    ],
                ],
            ],
            
            'admin-payments-history' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/payments/histrory',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminPaymentController::class,
                        'action'     => 'paymentHistory',
                    ],
                ],
            ],
			
			'admin-payout-pendings' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/payments/pending',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminPaymentController::class,
                        'action'     => 'payoutPending',
                    ],
                ],
            ],
             'admin-payments-history-manager' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/payments/histrory-manager',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminPaymentController::class,
                        'action'     => 'paymentHistoryManger',
                    ],
                ],
            ],
            'admin-accept-payments' => [
                'type' => Segment::class,
                // Configure the route itself
                'options' => [
                    'route'    => '/admin/payments/accept-payments',
                    // Define default controller and action to be called when this route is matched
                    'defaults' => [
                        'controller' => Controller\AdminPaymentController::class,
                        'action'     => 'acceptPayments',
                    ],
                ],
            ],

            'admin_testimonial' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/testimonial',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'testimonial',
                    ],
                ],
            ],

            'admin_testimonial_manager' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/testimonial_manager',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'testimonialmanager',
                    ],
                ],
            ],

            'admin_help' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/help',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'help',
                    ],
                ],
            ],

            'admin_help_manager' => [
                'type' => Segment::class,
                'options' => [
                    'route'    => '/admin/masterdata/help_manager',
                    'defaults' => [
                        'controller' => Controller\MasterdataController::class,
                        'action'     => 'helpmanager',
                    ],
                ],
            ],
            'login_as_user' => [
                'type' => Literal::class,
                'options' => [
                    'route'    => '/admin/login_as_user',
                    'defaults' => [
                        'controller' => Controller\AdminUserController::class,
                        'action'     => 'LoginAsUser',
                    ],
                ],
            ],
        ]
    ],
    'adminModules' => [
        'settings' => 'Settings',
        'admin_accounts'=> 'Administrators',
        'user_accounts'	=> 'User Accounts',
		'user_notifications' => 'Notifications',
        'seeker_trips'	=> 'Trips',
        'traveller_service'	=> 'Traveller Services',
        'seeker_service'	=> 'Seeker Services',
        'account_settings'=> 'Account settings',
        'price_settings'=> 'Price Settings',
        'masterdata'    => 'Master Data',
    ],
    'adminMenus' => [
        'admin_accounts' => [
            'My Account' 	=> 'myaccount',
            'Admin Account List' => 'subadmins',
        ],
         
        'user_accounts' => [
            'Add New User' 	=> 'usermanager',
            'User Accounts List' => 'useraccounts',
            'User Address List' => 'users/useraddresses',
            'User Photos List' => 'users/userphotos',
            'User Videos List' => 'users/uservideos',
            'User Trust Verification List' => 'users/usertrustverification',
			'User Reviews List' => 'userreviewslist',
            'User References' => 'users/userreferences',
            'User Referrals' => 'users/userreferrals',
        ],
		'user_notifications' => [
                'Send Notification' 	=> 'users/sendnotification',
			'Notification List' 	=> 'users/notificationlist',
			/*'Notification Type' 	=> 'users/notificationtypelist',*/
			'Traveller Enquiries List' 	=> 'users/travellerenqlist',
			'Seeker Enquiries List' 	=> 'users/seekerenqlist',
			'Chat Message List' 	=> 'users/chatmsglist',
			'Super Traveller List' 	=> 'users/supertravellerlist', 
        ],
        'seeker_trips' => [
            'Seekers Trips' 	=> 'seekers',
	    'Seekers Ticket' 	=> 'seekersticket',
	    'Seeker Requests List' => 'seekersrequests',
            'Traveler Trips' => 'travellers',
	    'Traveler Ticket' => 'travellersticket',
	    'Traveler Requests List' 	=> 'travelersrequests',
            'Disputes' 	=> 'dispute',

        ],
        'traveller_service' => [
            'People Service' 	=> 'traveller/people',
            'Package Service' => 'traveller/package',
            'Product Service' => 'traveller/project',
            'Traveller Wish List' => 'traveller/travellerwishlist',
        ],
        'seeker_service' => [
            'People Service' 	=> 'seeker/people',
            'Package Service' => 'seeker/package',
            'Product Service' => 'seeker/project',
			'Seeker Wish List' => 'seeker/seekerwishlist',
        ],
       
        'account_settings' => [
			'Notification'=> 'payments/notification',
            'Payment Methods '=> 'payments/methods',
            'Payout Methods' => 'payments/payout-methods',
            'Transactions History' => 'payments/transaction-history',
            'Payments List' => 'payments/histrory',
			'Payout Pendings' => 'payments/pending',
		    'Privacy' => 'payments/privacy',
			'Cancelled Account' => 'payments/cancelaccount',


        ],
         
        'price_settings' => [
            'People Service' 	=> 'price_settings/people_service',
            'Package Service' 	=> 'price_settings/package_service',
            'Product Service' 	=> 'price_settings/project_service',
           
        ], 
        'masterdata' => [
            'Add Coupon'    => 'coupons',
            'Coupons List' => 'couponmanager',
            'Mail Templates' 	=> 'masterdata/mailtemplate',
            'Static Manager' 	=> 'masterdata/static_manager',
            'Testimonial List' 	=> 'masterdata/testimonial',
            'Help List' 	    => 'masterdata/help',
            'Customs Excise Duty List' 	=> 'masterdata/customs_excise_duty_list',
            'Report List' 	=> 'masterdata/report_list',
            'Message List' 	=> 'masterdata/message_list',
            'Countries List' 	=> 'masterdata/country_list',
            'Languages List'    => 'masterdata/language_list',
            'Item Categories List' 	=> 'masterdata/item-category-list',
            'Task Categories List' 	=> 'masterdata/task-category-list',
            'Product Categories List' 	=> 'masterdata/product-category-list',

        ]
        
    ]
];
?>