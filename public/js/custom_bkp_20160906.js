
$('ul.slimmenu').slimmenu({
    resizeWidth: '992',
    collapserTitle: 'Main Menu',
    animSpeed: 250,
    indentChildren: true,
    childrenIndenter: ''

});

/* Countdown */

$('.countdown').each(function () {

    var count = $(this);

    $(this).countdown({
        zeroCallback: function (options) {

            var newDate = new Date(),
                    newDate = newDate.setHours(newDate.getHours() +
                            130);

            $(count).attr("data-countdown", newDate);

            $(count).countdown({
                unixFormat: true

            });

        }

    });

});

$('.btn').button();

$("[rel='tooltip']").tooltip();

$('.form-group').each(function () {

    var self = $(this),
            input = self.find('input');

    input.focus(function () {

        self.addClass('form-group-focus');

    })

    input.blur(function () {

        if (input.val()) {

            self.addClass('form-group-filled');

        } else {

            self.removeClass('form-group-filled');

        }

        self.removeClass('form-group-focus');

    });

});






$(".typeahead").autocomplete({
    source: baseurl + "/application/search-airport",
    minLength: 1,
    select: function (event, ui) {

        var element_id = $(this).attr('id');



        if ($('#' + element_id + '_id').length == 0) {

            $('<input type="hidden" name="' + element_id + '_id" id="' + element_id + '_id" value="' + ui.item.ID + '" >').insertAfter('#' + element_id);

            $('<input type="hidden" name="' + element_id + '_code" id="' + element_id + '_code" value="' + ui.item.code + '" >').insertAfter('#' + element_id);

        } else {

            $('#' + element_id + '_id').val(ui.item.ID);

            $('#' + element_id + '_code').val(ui.item.code);

        }

        if (element_id == 'origin') {

            /* alert( element_id ); */

            $('#destination').focus();

        }

        if (element_id == 'destination') {

            $('#date').focus();

        }

        /* $('#elemId').length */



    }

});



/*

 $('.typeahead').typeahead({

 hint: true,

 highlight: true,

 minLength: 3,

 limit: 8

 }, {

 source: function(q, cb) {

 return $.ajax({

 dataType: 'json',

 type: 'get',

 url: baseurl+'/index/search-airport?q=' +

 q,

 chache: false,

 success: function(data) {

 var result = [];

 $.each(data, function(index, val) {

 result.push({

 id: val.ID,

 value: val.name

 });

 });

 cb(result);

 }

 });

 }

 });

 */



/*

 $('.typeahead').typeahead({

 hint: true,

 highlight: true,

 minLength: 3,

 limit: 8

 }, {

 source: function(q, cb) {

 return $.ajax({

 dataType: 'json',

 type: 'get',

 url: 'http://gd.geobytes.com/AutoCompleteCity?callback=?&q=' +

 q,

 chache: false,

 success: function(data) {

 var result = [];

 $.each(data, function(index, val) {

 result.push({

 value: val

 });

 });

 cb(result);

 }

 });

 }

 });

 */



$('input.date-pick, .input-daterange, .date-pick-inline').datepicker({
    todayHighlight: true,
    startDate: '-0d',
    autoclose: true

});

$('input.date-pick, .input-daterange input[name="start"]').datepicker('setDate', 'today');

$('.input-daterange input[name="end"]').datepicker('setDate', '+7d');



$('input.time-pick').timepicker({
    minuteStep: 15,
    showInpunts: false

})

$('input.date-pick-years').datepicker({
    startView: 2

});

$('input#dob').datepicker({
    format: "mm/dd/yyyy",
    startView: 2,
    autoclose: true

});

$(document).ready(function () {

    $('input#date').datepicker({
        format: "mm/dd/yyyy",
        todayHighlight: true,
        startDate: '-0d',
        autoclose: true

    })

            .on('changeDate', function (ev) {

               /* alert("testing glhdjdjn"); */

                $("#service").children("option[value='people']").prop('selected', true).trigger('change');

            });

});



$('.booking-item-price-calc .checkbox label').click(function () {

    var checkbox = $(this).find('input'),
            /* checked = $(checkboxDiv).hasClass('checked'), */

            checked = $(checkbox).prop('checked'),
            price = parseInt($(this).find('span.pull-right').html().replace(
                    '$', '')),
            eqPrice = $('#car-equipment-total'),
            tPrice = $('#car-total'),
            eqPriceInt = parseInt(eqPrice.attr('data-value')),
            tPriceInt = parseInt(tPrice.attr('data-value')),
            value,
            animateInt = function (val, el, plus) {

                value = function () {

                    if (plus) {

                        return el.attr('data-value', val + price);

                    } else {

                        return el.attr('data-value', val - price);

                    }

                };

                return $({
                    val: val

                }).animate({
                    val: parseInt(value().attr('data-value'))

                }, {
                    duration: 500,
                    easing: 'swing',
                    step: function () {

                        if (plus) {

                            el.text(Math.ceil(this.val));

                        } else {

                            el.text(Math.floor(this.val));

                        }

                    }

                });

            };

    if (!checked) {

        animateInt(eqPriceInt, eqPrice, true);

        animateInt(tPriceInt, tPrice, true);

    } else {

        animateInt(eqPriceInt, eqPrice, false);

        animateInt(tPriceInt, tPrice, false);

    }

});

$('div.bg-parallax').each(function () {

    var $obj = $(this);

    if ($(window).width() > 992) {

        $(window).scroll(function () {

            var animSpeed;

            if ($obj.hasClass('bg-blur')) {

                animSpeed = 10;

            } else {

                animSpeed = 15;

            }

            var yPos = -($(window).scrollTop() / animSpeed);

            var bgpos = '50% ' + yPos + 'px';

            $obj.css('background-position', bgpos);

        });

    }

});



$('.nav-drop').dropit();

$('#notificationContainer').dropit();



$(document).ready(function () {





    /* Owl Carousel */

    var owlCarousel = $('#owl-carousel'),
            owlItems = owlCarousel.attr('data-items'),
            owlCarouselSlider = $('#owl-carousel-slider'),
            owlNav = owlCarouselSlider.attr('data-nav');

    /* owlSliderPagination = owlCarouselSlider.attr('data-pagination'); */

    owlCarousel.owlCarousel({
        items: owlItems,
        navigation: true,
        navigationText: ['', '']

    });

    owlCarouselSlider.owlCarousel({
        slideSpeed: 300,
        paginationSpeed: 400,
        /* pagination: owlSliderPagination, */

        singleItem: true,
        navigation: true,
        mouseDrag: false,
        pullDrag: false,
        freeDrag: false,
        navigationText: ['', ''],
        transitionStyle: false,
        autoPlay: 9000

    });



    /* footer always on bottom */

    var docHeight = $(window).height();

    var footerHeight = $('#main-footer').height();

    var footerTop = $('#main-footer').position().top + footerHeight;



    if (footerTop < docHeight) {

        $('#main-footer').css('margin-top', (docHeight - footerTop) + 'px');

    }



    /*$('.owl-item, .top-area').css('height', (docHeight - 20) + 'px');*/

    var sbh = $(".search-container-home").innerHeight();
    var d11 = $(".header-top").innerHeight();
    var sbhN= sbh + d11;
    var ss1 = docHeight - sbhN;
    var ss2 = docHeight - sbh;
    if ($(window).width() > 992) {
        $('.owl-item, .top-area').css('height', ss1 + 'px');
    }else{
        $('.owl-item, .top-area').css('height', (docHeight) + 'px');
    }



    var videoPlayer = $('#videoPlayer');

    $('#videoPlayer').css('height', (docHeight - 60) + 'px');

    $("#videoPlayer").bind("ended", function () {

        /* alert("Thanks for watching..."); */

        setTimeout(closepromo, 1500);

    });



});

/*
$(window).load(function () { resize()});
function resize() {

  var d = $(".header-top").innerHeight();
  var d2 = $(window).height() - d;
    var mu = d+10;
$("#sidebar-wrapper, .overlay").css('top', d);
  $("#sidebar-wrapper").innerHeight(d2)
 $(".hamburger").css('top', mu);
$("body.inner-page-topSpace").css('padding-top', d);



 var wW = $(window).width();
  var br = $(".container").width();
    var mnp = wW - br;
     var mnp1 = mnp/2;
    //$(".hamburger").css('left', wW );
if (mnp1 <= 100) {

      $(".afL-breadcrumb").css('padding-left', '80px');
}else{
     $(".afL-breadcrumb").css('padding-left', '0px');

}


};
window.onresize = resize;
*/

$(window).load(function () { resize()});
function resize() {

  var d = $(".header-top").innerHeight();
  var d2 = $(window).height() - d;
    var mu = d+10;
$("#sidebar-wrapper, .overlay").css('top', d);
  $("#sidebar-wrapper").innerHeight(d2)
 $(".hamburger").css('top', mu);
if ($(window).width() >= 993) {

$("body.inner-page-topSpace, .home-top").css('padding-top', d);
    }else{
       $("body.inner-page-topSpace, .home-top").css('padding-top', '0');

    }



 var wW = $(window).width();
  var br = $(".container").width();
    var mnp = wW - br;
     var mnp1 = mnp/2;

if (mnp1 <= 100) {

      $(".afL-breadcrumb").css('padding-left', '80px');
}else{
     $(".afL-breadcrumb").css('padding-left', '0px');

}


};
window.onresize = resize;


$(window).scroll(function () {
    if ($(window).width() >= 993) {
    var d = $(".header-top").innerHeight();
    var mu = d+10;
    var scroll = $(window).scrollTop();

    var trepHeight = $(window).height() - d;
    if (scroll >= 40) {

        /* $("#sidebar-wrapper").css('top', '60px'); */

        $("#wrapper .overlay").css('top', d);


    } else {

       /* $("#sidebar-wrapper").css('top', '60px'); */

        $("#wrapper .overlay").css('top', d);

        $(".hamburger").css('top', mu);

     }}else{

    var d = $(".header-top").innerHeight();
    var mu = d+10;
    var scroll = $(window).scrollTop();
    var trepHeight = $(window).height() - d;
    if (scroll >= 40) {
var df = $(window).height();
  $("#sidebar-wrapper").innerHeight(df);
        $("#sidebar-wrapper").css('top', '0');

        $("#wrapper .overlay").css('top', '0');


    } else {




     $("#sidebar-wrapper").css('top', d);

        $("#wrapper .overlay").css('top', '0');

        $(".hamburger").css('top', mu);

     }


     };



    if ($(this).scrollTop() > trepHeight) {

        /* $("#videoPlayer").get(0).pause(); */

        $('.trep-toggler').slideUp(50);

        $('.top-area .header-top').removeAttr('style');

        if ($('.trep-toggler').hasClass('is-open')) {

            $('.trep-toggler').removeClass('is-open').addClass('is-closed');

        }

        return false;

    }

});

/* Trepr Features */

$(document).ready(function () {

    /* =========

     $('.trep-features').css('display', 'none');

     $('.trep-feature').click( function() {

     $('.trep-features').show('slow', "swing");

     $('html,body').animate({ scrollTop: '+=2000px' });

     $('.carousel').hide('slow', "swing");

     }),



     $('.trep-features .close').click( function() {

     $('.trep-features').hide('slow', "swing");

     $('.carousel').show('slow', "swing");

     $('html,body').animate({ scrollTop: '+=2000px' });

     }),



     ============ */



    $(".trep-promo").click(function () {

        playpromo();

    });



    $(".promo-close").click(function () {

        closepromo();

    });

});

function playpromo() {

    $('.trep-toggler').slideDown().addClass('is-open');

    $('.top-area .header-top').css('background-color', '#262626');

    $('html,body').animate({scrollTop: '+=-1000px'}, 'slow');

    $("#videoPlayer")[0].play();

}

function closepromo() {

    $("#videoPlayer")[0].pause();

    $('.trep-toggler').slideUp();

    $('html,body').animate({scrollTop: '+=-1000px'}, 'slow');

    $('.top-area .header-top').removeAttr('style');

    if ($('.trep-toggler').hasClass('is-open')) {

        $('.trep-toggler').removeClass('is-open').addClass('is-closed');

    }

}



$("#trepservice").change(function () {

    /* alert($(this).val()); */

    if ($(this).val() === 'people') {
        $('.servicespeople').css('display', 'block');
    } else {
        $('.servicespeople').hide('slow');
    }

    if ($(this).val() === 'package') {
        $('.servicespackage').css('display', 'block');
    } else {
        $('.servicespackage').hide('slow');
    }

    if ($(this).val() === 'project') {
        $('.servicesproject').css('display', 'block');
    } else {
        $('.servicesproject').hide('slow');
    }

});



$(document).ready(function () {

    /* var tcarouselWidth = 350;

     if($(window).width() > 992 ){

     var tcarouselWidth = 960;

     }

     console.log(tcarouselWidth);*/

    $('.carousel').carousel({
        hAlign: 'center',
        vAlign: 'center',
        hMargin: 0.2,
        vMargin: 0,
        frontWidth: 640,
        frontHeight: 490,
        /* carouselWidth: tcarouselWidth, */

        carouselWidth: 930,
        carouselHeight: 490,
        bottom: 0,
        directionNav: false,
        reflection: false,
        shadow: false,
        speed: 500,
        buttonNav: 'none',
        autoplay: true,
        autoplayInterval: 4000,
        pauseOnHover: true,
        mouse: true,
        description: true,
        descriptionContainer: '.description',
        backOpacity: 1,
        before: function (carousel) {},
        after: function (carousel) {}

    });

});



$("#radius").ionRangeSlider({
    type: 'single',
    min: 0,
    max: 1000,
    from: 500,
    prefix: "km",
    /* maxPostfix: "+", */

    prettify: true,
    hasGrid: true,
    /*step: 5,

     grid: true,

     grid_snap: true*/

    onChange: function (obj) {

        console.log(obj);

    }

});

$('.i-check, .i-radio').iCheck({
    checkboxClass: 'i-check',
    radioClass: 'i-radio'

});



$('input#contenders').on('ifChecked', function (event) {

    event.preventDefault();

    $(this).tab('show');

});

$('input#addtrips').on('ifChecked', function (event) {

    event.preventDefault();

    $(this).tab('show');

});

$('input#anydate').on('ifChecked', function (event) {

    event.preventDefault();

    $('.input-daterange #switchDisabledRadio').attr("disabled",
            "disabled");

});

$('input#anydate').on('ifUnchecked', function (event) {

    event.preventDefault();

    $('.input-daterange #switchDisabledRadio').removeAttr("disabled");

});

$('[data-spy="scroll"]').each(function () {

    var $spy = $(this).scrollspy('refresh');

});



$('.booking-item-review-expand').click(function (event) {

    console.log('baz');

    var parent = $(this).parent('.booking-item-review-content');

    if (parent.hasClass('expanded')) {

        parent.removeClass('expanded');

    } else {

        parent.addClass('expanded');

    }

});

$('.stats-list-select > li > .booking-item-rating-stars > li').each(function () {

    var list = $(this).parent(),
            listItems = list.children(),
            itemIndex = $(this).index();

    $(this).hover(function () {

        for (var i = 0; i < listItems.length; i++) {

            if (i <= itemIndex) {

                $(listItems[i]).addClass('hovered');

            } else {

                break;

            }

        }

        $(this).click(function () {

            for (var i = 0; i < listItems.length; i++) {

                if (i <= itemIndex) {

                    $(listItems[i]).addClass('selected');

                } else {

                    $(listItems[i]).removeClass(
                            'selected');

                }

            }

        });

    }, function () {

        listItems.removeClass('hovered');

    });

});



$('.booking-item-container').children('.booking-item').click(function (event) {

    if ($(this).hasClass('active')) {

        $(this).removeClass('active');

        $(this).parent().removeClass('active');

    } else {

        $(this).addClass('active');

        $(this).parent().addClass('active');

        $(this).delay(1500).queue(function () {

            $(this).addClass('viewed')

        });

    }

});



$('.form-group-cc-number input').payment('formatCardNumber');

$('.form-group-cc-date input').payment('formatCardExpiry');

$('.form-group-cc-cvc input').payment('formatCardCVC');

function initialize() {

    if ($('#map-canvas').length) {

        var map,
                service;

        jQuery(function ($) {

            $(document).ready(function () {

                var latlng = new google.maps.LatLng(40.7564971, -73.9743277);

                var myOptions = {
                    zoom: 16,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false

                };

                map = new google.maps.Map(document.getElementById(
                        "map-canvas"), myOptions);

                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map

                });

                marker.setMap(map);

                $('a[href="#google-map-tab"]').on('shown.bs.tab',
                        function (e) {

                            google.maps.event.trigger(map, 'resize');

                            map.setCenter(latlng);

                        });

            });

        });

    }





    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: new google.maps.LatLng(40.7564971, -73.9743277),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP

    });

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(40.7564971, -73.9743277),
        map: map

    });

    var infowindow;

    infowindow = new google.maps.InfoWindow();

}





$('.card-select > li').click(function () {

    self = this;

    $(self).addClass('card-item-selected');

    $(self).siblings('li').removeClass('card-item-selected');

    $('.form-group-cc-number input').click(function () {

        $(self).removeClass('card-item-selected');

    });

});

/* Lighbox gallery */

$('#popup-gallery').each(function () {
    $(this).magnificPopup({
        delegate: 'a.popup-gallery-image',
        type: 'image',
        gallery: {
            enabled: true
        }
    });
});

/* Lighbox image */

/*$('.popup-image').magnificPopup({
    type: 'image'
});*/

/* Lighbox text */

$('.popup-text').magnificPopup({
    removalDelay: 500,
    showCloseBtn: false,
    callbacks: {
        beforeOpen: function () {
            this.st.mainClass = this.st.el.attr('data-effect');
        }
    },
    midClick: true
});

/* Lightbox iframe */

$('.popup-iframe').magnificPopup({
    dispableOn: 700,
    type: 'iframe',
    removalDelay: 160,
    mainClass: 'mfp-fade',
    preloader: false

});

/* Lighbox map */
$('.popup-map').magnificPopup({
    removalDelay: 500,
    focus: '#street_address_1',
    showCloseBtn: false,
    callbacks: {
        beforeOpen: function(){
            this.st.mainClass = this.st.el.attr('data-effect');
        }
    },
    midClick: true
});

$('.form-group-select-plus').each(function () {
    var self = $(this),
            btnGroup = self.find('.btn-group').first(),
            select = self.find('select');
    btnGroup.children('label').last().click(function () {
        btnGroup.addClass('hidden');
        select.removeClass('hidden');
    });
});

/* Responsive videos */
$(document).ready(function () {
    $("body").fitVids();
});



$(function ($) {
    $('#twitter').tweet({
        username: "remtsoy", /* !paste here your twitter username! */
        count: 3
    });
});



$(function ($) {
    $("#twitter-ticker").tweet({
        username: "remtsoy", /* !paste here your twitter username! */
        page: 1,
        count: 20
    });
});



$(document).ready(function () {

    var ul = $('#twitter-ticker').find(".tweet-list");

    var ticker = function () {

        setTimeout(function () {

            ul.find('li:first').animate({
                marginTop: '-4.7em'

            }, 850, function () {

                $(this).detach().appendTo(ul).removeAttr('style');

            });

            ticker();

        }, 5000);

    };

    ticker();

});

$(function () {

    $('#ri-grid').gridrotator({
        rows: 4,
        columns: 8,
        animType: 'random',
        animSpeed: 1200,
        interval: 1200,
        step: 'random',
        preventClick: false,
        maxStep: 2,
        w992: {
            rows: 5,
            columns: 4

        },
        w768: {
            rows: 6,
            columns: 3

        },
        w480: {
            rows: 8,
            columns: 3

        },
        w320: {
            rows: 5,
            columns: 4

        },
        w240: {
            rows: 6,
            columns: 4

        }

    });



});





$(function () {

    $('#ri-grid-no-animation').gridrotator({
        rows: 4,
        columns: 8,
        slideshow: false,
        w1024: {
            rows: 4,
            columns: 6

        },
        w768: {
            rows: 3,
            columns: 3

        },
        w480: {
            rows: 4,
            columns: 4

        },
        w320: {
            rows: 5,
            columns: 4

        },
        w240: {
            rows: 6,
            columns: 4

        }

    });



});



var tid = setInterval(tagline_vertical_slide, 2500);



/*  vertical slide */

function tagline_vertical_slide() {

    var curr = $("#tagline ul li.active");

    curr.removeClass("active").addClass("vs-out");

    setTimeout(function () {

        curr.removeClass("vs-out");

    }, 500);



    var nextTag = curr.next('li');

    if (!nextTag.length) {

        nextTag = $("#tagline ul li").first();

    }

    nextTag.addClass("active");

}



function abortTimer() { /* to be called when you want to stop the timer */

    clearInterval(tid);

}



var trigger = $('.hamburger'),
        overlay = $('.overlay'),
        isClosed = false;



function hamburger_cross() {

    if (isClosed === true) {

        overlay.hide();

        trigger.removeClass('is-open');

        trigger.addClass('is-closed');

        isClosed = false;

    } else {

        overlay.show();

        trigger.removeClass('is-closed');

        trigger.addClass('is-open');

        isClosed = true;

    }

}

trigger.click(function () {

    hamburger_cross();

});

$('[data-toggle="offcanvas"]').click(function () {

    $('#wrapper').toggleClass('toggled');

});



$("input#gotoseeker").on('ifChanged', function (event) {

    window.location = baseurl + '/switchuser/seeker';

});



$("input#gototraveler").on('ifChanged', function (event) {

    window.location = baseurl + '/switchuser/traveller';

});



$('[data-spy="scroll"]').each(function () {

    var $spy = $(this).scrollspy('refresh');

});

$(function () {
    if ($("#add-seeker-people .form-group-select-plus").val() === 0) /* I'm supposing the "Other" option value is 0. */
        $("#yourTextBox").hide();
});

$(".tooltip-large").tooltip({
    delay: {show: 0, hide: 2000}
});



$('.bfh-languages').selectpicker({
    header: 'Select Language',
    title: 'English',
    liveSearchPlaceholder: 'Search your Languages',
    liveSearch: true,
    maxOptions: 5,
    size: 5
});



$('#trepservice').selectpicker({
    header: 'Select a Service',
    title: 'Select a Service',
    size: 5
});



var pacContainerInitialized = false;
$('#street_address_1').keypress(function () {
    if (!pacContainerInitialized) {
        $('.pac-container').css('z-index', '9999');
        pacContainerInitialized = true;
    }
});



$('#myTab a').click(function (e) {

    e.preventDefault();

    $(this).tab('show');

});



/* store the currently selected tab in the hash value */

$("ul.hash-nav-tabs > li > a").on("shown.bs.tab", function (e) {

    var id = $(e.target).attr("href").substr(1);

    window.location.hash = id;

});



/* on load of the page: switch to the currently selected tab */

var hash = window.location.hash;

$('#myTab a[href="' + hash + '"]').tab('show');



$(document).ready(function () {

    $("div#tasklists").hide();

    $("#task_category_1").change(function () {

        var id = $(this).val();

        var dataString = 'id=' + id;

        if (id != "") {

            $("#tasklists").show(1000);

        } else {

            $("#tasklists").hide(1000);

        }

        $.ajax({
            type: "POST",
            url: baseurl + '/trips/getprojectsubcategory',
            data: dataString,
            cache: false,
            success: function (html) {

                $("#additional_requirements_category_1").html(html);

            }

        });

    });

});
