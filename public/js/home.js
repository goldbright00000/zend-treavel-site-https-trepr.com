var homeSliderCarousel;
var testimonialSliderCarousel;

$(document).ready(function(e) {
    e = e || window.event;
    e.cancelBubble = true;


    testimonialSliderCarousel = $("#testimonial-slider").owlCarousel({
        loop: true,
        center: true,
        items: 3,
        margin: 0,
        autoplay: true,
        nav: true,
        autoplayTimeout: 8500,
        autoplayHoverPause: true,
        smartSpeed: 450,
        lazyLoad: true,
        responsive: {
            0: {
                items: 1
            },
            768: {
                items: 2
            },
            1170: {
                items: 3
            }
        },
        onRefreshed: checkTestimonialItems
    });

    function checkTestimonialItems(event) {

        setTimeout(function() {
            var items = event.item.count;

            if (items == 1) {
                $('.owl-testimonial.owl-carousel').data('owl.carousel').options.loop = false;
                $('.owl-testimonial.owl-carousel').trigger('refresh.owl.carousel');
            } else {
                $('.owl-testimonial.owl-carousel').data('owl.carousel').options.loop = true;
                $('.owl-testimonial.owl-carousel').trigger('refresh.owl.carousel');
            }
        }, 1000);
    }

    startCarousel();

    /*if ( $(window).width() > 991 ) {
      startCarousel();
    } else {
      $('.owl-carousel').addClass('off');
      $('.home-slider-block').addClass('off-carousel');
    }*/

    $('.search-bar-home .button-bg-tab .btn').on('click', function() {

        var tadId = $(this).attr('data-tab');

        $('.search-bar-home .button-bg-tab .btn').removeClass('active');

        var tabIndex = $(this).parent().index();
        $('.lg-screen .search-bar-home .button-bg-tab:eq(' + tabIndex + ') .btn').addClass('active');
        $('.sm-screen .search-bar-home .button-bg-tab:eq(' + tabIndex + ') .btn').addClass('active');

        $('.search-inputs .fields').hide();
        $('.search-inputs ' + tadId).show();
    });

    $('.search-btn').on('click', function() {
        var $formElement = $(this).parent().parent().parent();
        homeSearchFormValidate($formElement);
    });


    //Owl slider modal popup code
    $('a.how-it-works').on('click', function() {

        $('.youtube-modal').find('.modal-content iframe').attr('src', baseUrl);

        var baseUrl = 'https://www.youtube.com/embed/';
        var srcId = $(this).data('src');

        $($(this).data("target")).on("show.bs.modal", function(e) {
            $(this).find('.modal-content iframe').attr('src', baseUrl + srcId);

        });

        $($(this).data("target")).on("hidden.bs.modal", function(e) {
            $(this).find('.modal-content iframe').attr('src', baseUrl);
        });

    });

    $('.more-features').on('click', function() {
        $('.features-block').find('.lists:eq(1)').toggleClass('expanded');
        if ($('.features-block').find('.lists:eq(1)').hasClass('expanded')) {
            $('.more-features').text('Less Features');
        } else {
            $('.more-features').text('More Features');
        }

    });

    $('#peopleBtn').magnificPopup({
        items: [{
            src: 'public/img/services/people/seeker/1w.jpg'
        }, {
            src: 'public/img/services/people/seeker/2w.jpg'
        }, {
            src: 'public/img/services/people/seeker/3w.jpg'
        }, {
            src: 'public/img/services/people/seeker/4w.jpg'
        }, {
            src: 'public/img/services/people/seeker/5w.jpg'
        }, {
            src: 'public/img/services/people/seeker/6w.jpg'
        }, {
            src: 'public/img/services/people/seeker/7w.jpg'
        }, {
            src: 'public/img/services/people/seeker/8w.jpg'
        }],
        gallery: {
            enabled: true
        },
        type: 'image' /* this is default type*/
    });
    $('#packageBtn').magnificPopup({
        items: [{
            src: 'public/img/services/package/seeker/1w.jpg'
        }, {
            src: 'public/img/services/package/seeker/2w.jpg'
        }, {
            src: 'public/img/services/package/seeker/3w.jpg'
        }, {
            src: 'public/img/services/package/seeker/4w.jpg'
        }, {
            src: 'public/img/services/package/seeker/5w.jpg'
        }, {
            src: 'public/img/services/package/seeker/6w.jpg'
        }, {
            src: 'public/img/services/package/seeker/7w.jpg'
        }, {
            src: 'public/img/services/package/seeker/8w.jpg'
        }, {
            src: 'public/img/services/package/seeker/9w.jpg'
        }, {
            src: 'public/img/services/package/seeker/10w.jpg'
        }],
        gallery: {
            enabled: true
        },
        type: 'image' /* this is default type */
    });
    $('#productBtn').magnificPopup({
        items: [{
            src: 'public/img/services/project/seeker/1w.jpg'
        }, {
            src: 'public/img/services/project/seeker/2w.jpg'
        }, {
            src: 'public/img/services/project/seeker/3w.jpg'
        }, {
            src: 'public/img/services/project/seeker/4w.jpg'
        }, {
            src: 'public/img/services/project/seeker/5w.jpg'
        }, {
            src: 'public/img/services/project/seeker/6w.jpg'
        }, {
            src: 'public/img/services/project/seeker/7w.jpg'
        }, {
            src: 'public/img/services/project/seeker/8w.jpg'
        }, {
            src: 'public/img/services/project/seeker/10w.jpg'
        }],
        gallery: {
            enabled: true
        },
        type: 'image' /* this is default type */
    });

});

$(window).resize(function() {
    homeSliderCarousel.trigger('destroy.owl.carousel');
    startCarousel();
});

$(window).scroll(function() {

    if ($('.main-banner-wrapper').length != 0) {
        if ($(this).scrollTop() > 100)
            $('.header').addClass('blue-bg');
        else
            $('.header').removeClass('blue-bg');
    }
});

function homeSearchFormValidate(form) {
    var validator = form.validate({
        rules: {
            origin: {
                required: true
            },
            destination: {
                required: true
            },
            date: {
                required: true
            },
            trepservice: {
                required: true
            }
        },
        messages: {
            origin: "Please set origin location",
            destination: "Please set destination location",
            date: "Please set Date",
            trepservice: "Please select a Service",
        },
        errorPlacement: function(error, element) {
            error.html("Please " + element.attr('placeholder')).insertAfter(element);
        },
        submitHandler: function(form) {
            /* do other things for a valid form */
            form.submit();
        }
    });
}

function startCarousel() {
    homeSliderCarousel = $("#homeSliderCarousel").owlCarousel({
        items: 1,
        loop: true,
        nav: true,
        navText: [
            "<i class='fa fa-angle-left' aria-hidden='true'></i>",
            "<i class='fa fa-angle-right' aria-hidden='true'></i>"
        ],
        dots: false,
        callbacks: true,
        itemsDesktop: [1000, 1],
        itemsDesktopSmall: [979, 1],
        itemsTablet: [768, 1],
        navigationText: ["", ""],
        slideSpeed: 300,
        autoplay: true,
        autoplayHoverPause: false,
        autoplayTimeout: 7000,
        animateIn: 'fadeIn', // add this
        animateOut: 'fadeOut', // and this
        onInitialized: animateTitle,
        onTranslated: animateTitle
    });


    homeSliderCarousel.on('changed.owl.carousel', function(event) {
        if (!$('.home-slider-block').hasClass('no-bg'))
            $('.home-slider-block').addClass('no-bg');

        $('.home-slider-block .owl-cap').removeClass('slide-in');
    });
}

function animateTitle(event) {
    var element = event.target;
    var index = event.item.index;
    $('.home-slider-block .owl-cap:eq(' + index + ')').addClass('slide-in');
}

function stopCarousel() {
    homeSliderCarousel.trigger('destroy.owl.carousel');
    $('.home-slider-block .owl-carousel').addClass('off');
}
