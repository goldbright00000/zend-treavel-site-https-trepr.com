
$('ul.slimmenu').slimmenu({
    resizeWidth: '992',
    collapserTitle: 'Main Menu',
    animSpeed: 250,
    indentChildren: true,
    childrenIndenter: ''

});

/* Countdown */
$('.countdown').each(function () {
    var count = $(this);
    $(this).countdown({
        zeroCallback: function (options) {
            var newDate = new Date(),
                    newDate = newDate.setHours(newDate.getHours() + 130);
            $(count).attr("data-countdown", newDate);
            $(count).countdown({
                unixFormat: true
            });
        }
    });
});

$('.btn').button();
$("[rel='tooltip']").tooltip();

$('.form-group').each(function () {
    var self = $(this),
            input = self.find('input');
    input.focus(function () {
        self.addClass('form-group-focus');
    });

    input.blur(function () {
        if (input.val()) {
            self.addClass('form-group-filled');
        } else {
            self.removeClass('form-group-filled');
        }
        self.removeClass('form-group-focus');
    });
});
bindAutoComplete();
function getServiceFee(){
   if($('#service_type').length != 0){
      var service_type = $('#service_type').val();
      if(service_type == 'people'){
         getPeopleServiceFee();
      }else if(service_type == 'package'){
         getPackageServiceFee();
      }else if(service_type == 'project'){
         getProjectServiceFee();
      }

   }
}
function getPeopleServiceFeeSRC(userrole){

  //console.log(userrole);

   var origin_location_country_code = $('#origin_location_country_code').val();
   var destination_location_country_code = $('#destination_location_country_code').val();


   if($('#passengers_count').length != 0){
      var p_count = $('#passengers_count').val();
   }
   else if ($('#passengers_count_more').length != 0)
   p_count = $('#passengers_count_more').val();


   var distance = noofstops = '';
   if($('#nstop_manual').length != 0){
       noofstops = $('#nstop_manual').val();

   }

   if($('#travel_plan_reservation_type').length != 0 && $('#travel_plan_reservation_type').val() == 2){
     origin_location_country_code = $('#unplanned_origin_location_country_code').val();
     destination_location_country_code = $('#unplanned_destination_location_country_code').val();
     var  unplanned_origin_location_id = $('#unplanned_origin_location_id').val();
     var  unplanned_destination_location_id = $('#unplanned_destination_location_id').val();

     getdistance(unplanned_origin_location_id,unplanned_destination_location_id);


   }

   var origin_location_id=$('#origin_location_id').val();
           var destination_location_id=$('#destination_location_id').val();
   getdistance(origin_location_id,destination_location_id);
     distance = $('#distance').val();
     jQuery.ajax({
        type: "POST",
        url: base_url + '/ajax/getpassengersfeesrc',
        dataType: 'json',
        data: 'passengers_count=' + p_count+'&origin_location_country_code=' + origin_location_country_code+'&destination_location_country_code=' + destination_location_country_code+'&noofstops=' + noofstops+'&distance=' + distance+'&userrole='+userrole,
		    async: true,
        success: function (msg) {
          //console.log(msg);

			 //var resOj = $.parseJSON(msg);
       //console.log(resOj);
			// $('#span_people_service_fee').html(resOj.convertedPrice);
             //$('#people_service_fee').val(resOj.orginalPrice);
			$('#srcf_people_service_fee').val(msg.orginalPrice);
			$('#span_people_service_fee').text(msg.convertedPrice);
            $('#people_service_fee').val(msg.convertedPrice);
        }
    });
}
function getPackageServiceFeeSRC(userrole){
    var origin_location_country_code = destination_location_country_code = '';
    if($('#origin_location_country_code').length != 0){
        origin_location_country_code = $('#origin_location_country_code').val();
        destination_location_country_code = $('#destination_location_country_code').val();
    }  var p_count = 1;
   if($('#packages_count').length != 0){

      var trip_people_count =  $('input[name=packages_count]:checked').val();
      if(trip_people_count > 10){
           p_count = $('#packages_count_more').val();
      }else{
           p_count = trip_people_count;
      }
   }
    var  origin_location_id = $('#origin_location_id').val();
     var destination_location_id = $('#destination_location_id').val();
	 var weight_type = $('#weight_type > option:selected').val();

    getdistance(origin_location_id,destination_location_id);
   var item_categories = '';
   $(".category").each(function(idx) {
      var arrId = $(this).attr('id').split('_');
      var suv_cat_id = $('#item_category').val();
      var join_cats = "" + $(this).val() + suv_cat_id ;
	  item_categories = '&item_category_'+idx+'='+suv_cat_id+'&category_'+idx+'='+$(this).val();
      //item_categories.push(join_cats);
   });
   var total_weight = $('#total_weight').val();
   var total_worth = $('#total_worth').val();
   var distance = $('#distance').val();
   jQuery.ajax({
        type: "POST",
        url: base_url + '/ajax/getpackagesfeesrc',
        data: 'origin_location_country_code=' + origin_location_country_code+'&destination_location_country_code=' + destination_location_country_code+item_categories+'&item_weight_1=' + total_weight+'&item_worth_1=' + total_worth+'&distance='+distance+'&userrole='+userrole+'&weight_type='+weight_type,
        success: function (msg) {

                   var resOj = $.parseJSON(msg);
            // $('#span_package_service_fee').html(resOj.convertedPrice);
            // $('#package_service_fee').val(resOj.orginalPrice);
			$('#srcf_package_service_fee').val(resOj.orginalPrice);
			$('#span_package_service_fee').html(resOj.convertedPrice);
            $('#package_service_fee').val(resOj.convertedPrice);

        }
    });

}
function getProjectServiceFeeSRC(userrole){
    var origin_location_country_code = destination_location_country_code = '';
    if($('#origin_location_country_code').length != 0){
        origin_location_country_code = $('#origin_location_country_code').val();
        destination_location_country_code = $('#destination_location_country_code').val();
    }
    var work_category = $('#work_category').val();
     var item_categories =   '';
      $(".product_category").each(function(idx) {
         var arrId = $(this).attr('id').split('_');
         var suv_cat_id = $('#product_additional_requirements_category').val();
         var join_cats = "" + $(this).val() + suv_cat_id ;
        //item_categories.push(join_cats);
		item_categories = '&item_category_'+idx+'='+suv_cat_id+'&category_'+idx+'='+$(this).val();
      });
       var  origin_location_id = $('#origin_location_id').val();
     var destination_location_id = $('#destination_location_id').val();

    getdistance(origin_location_id,destination_location_id);
      var task_category =   [];
      $(".task_category").each(function() {
         var arrId = $(this).attr('id').split('_');
         var suv_cat_id1 = $('#additional_requirements_category').val();
         var join_cats1 = "" + $(this).val() + suv_cat_id1 ;
         task_category.push(join_cats1);
      });
        var  distance = $('#distance').val();
      var total_weight = $('#project_total_weight').val();
      var total_worth = $('#project_total_worth').val();
	  var product_weight_type = $('#product_weight_type > option:selected').val();
      if(work_category == 1){
    jQuery.ajax({
        type: "POST",
        url: base_url + '/ajax/getprojectsfeesrc',
        data: 'origin_location_country_code=' + origin_location_country_code+'&destination_location_country_code=' + destination_location_country_code+item_categories+'&item_weight_1=' + total_weight+'&item_worth_1=' + total_worth+'&work_category='+work_category+'&task_category='+task_category+'&distance='+distance+'&userrole='+userrole+'&product_weight_type'+product_weight_type,

              success: function (msg) {
				  var resOj = $.parseJSON(msg);
				  $('#srcf_project_service_fee').val(resOj.orginalPrice);
			$('#span_project_service_fee').html(resOj.convertedPrice);
            $('#project_service_fee').val(resOj.convertedPrice);

        }
    });
      }
      else
      {
        jQuery.ajax({
        type: "POST",
        url: base_url + '/ajax/getprojectsfeesrc',
        data: 'origin_location_country_code=' + origin_location_country_code+'&destination_location_country_code=' + destination_location_country_code+'&item_categories=' + item_categories+'&work_category='+work_category+'&task_category='+task_category+'&distance='+distance+'&userrole='+userrole,

              success: function (msg) {
                 var resOj = $.parseJSON(msg);
				  $('#srcf_project_service_fee').val(resOj.orginalPrice);
			$('#span_project_service_fee').html(resOj.convertedPrice);
            $('#project_service_fee').val(resOj.convertedPrice);



        }
    });
      }
}
function getPeopleServiceFee(){
   var origin_location_country_code = $('#origin_location_country_code').val();
   var destination_location_country_code = $('#destination_location_country_code').val();
   if($('#passengers_count').length != 0){
      var p_count = 1;
      var trip_people_count =  $('input[name=passengers_count]:checked').val();
      if(trip_people_count > 10){
           p_count = $('#trip_people_count_more').val();
      }else{
           p_count = trip_people_count;
      }
   }else if ($('#passengers_count_more').length != 0) p_count = $('#passengers_count_more').val();
   var distance = noofstops = '';
   if($('#noofstops').length != 0){
       noofstops = $('#noofstops').val();

   }
   if($('#travel_plan_reservation_type').length != 0 && $('#travel_plan_reservation_type').val() == 2){
     origin_location_country_code = $('#unplanned_origin_location_country_code').val();
     destination_location_country_code = $('#unplanned_destination_location_country_code').val();
     var  unplanned_origin_location_id = $('#unplanned_origin_location_id').val();
     var  unplanned_destination_location_id = $('#unplanned_destination_location_id').val();

     getdistance(unplanned_origin_location_id,unplanned_destination_location_id);
   }
     distance = $('#distance').val();
     jQuery.ajax({
        type: "POST",
        url: base_url + '/trips/getpassengersfee',
        data: 'passengers_count=' + p_count+'&origin_location_country_code=' + origin_location_country_code+'&destination_location_country_code=' + destination_location_country_code+'&noofstops=' + noofstops+'&distance=' + distance+'&service_fee='+$('#service_fee').val(),
        success: function (msg) {
           var resOj = $.parseJSON(msg);
            $('#span_people_service_fee').html(resOj.convertedPrice);
            $('#people_service_fee').val(resOj.orginalPrice);
        }
    });
}
function getdistance(orginCode,desCode){
    jQuery.ajax({
        type: "POST",
        url: base_url + '/trips/getDistance',
        async:false,
        data: 'orginCode=' + orginCode+'&desCode=' + desCode,
        success: function (msg) {
            $('#distance').val(msg);
        }
    });
}
function getPackageServiceFee(){
    var origin_location_country_code = destination_location_country_code = '';
    if($('#origin_location_country_code').length != 0){
        origin_location_country_code = $('#origin_location_country_code').val();
        destination_location_country_code = $('#destination_location_country_code').val();
    }  var p_count = 1;
   if($('#packages_count').length != 0){

      var trip_people_count =  $('input[name=packages_count]:checked').val();
      if(trip_people_count > 10){
           p_count = $('#packages_count_more').val();
      }else{
           p_count = trip_people_count;
      }
   }
    var  origin_location_id = $('#origin_location_id').val();
     var destination_location_id = $('#destination_location_id').val();

    getdistance(origin_location_id,destination_location_id);
   var item_categories =   [];
   /* $(".category").each(function() {
      var arrId = $(this).attr('id').split('_');
      var suv_cat_id = $('#item_category_'+arrId[1]).val();
      var join_cats = "" + $(this).val() + suv_cat_id ;
     item_categories.push(join_cats);
   });*/

    $(".category").each(function() {
      var arrId = $(this).attr('id').split('_');
      var suv_cat_id = $('#item_category_'+arrId[1]).val();
      var item_weight = $('#item_weight_'+arrId[1]).val();
       var item_worth = $('#item_worth_'+arrId[1]).val();
      var join_cats = "" + $(this).val() + suv_cat_id+'_'+item_weight+'_'+item_worth ;
     item_categories.push(join_cats);
   });
   var total_weight = $('#total_weight').val();
   var total_worth = $('#total_worth').val();
  var  distance = $('#distance').val();
   jQuery.ajax({
        type: "POST",
        url: base_url + '/trips/getpackagesfee',
        data: 'packages_count=' +p_count+'&origin_location_country_code=' + origin_location_country_code+'&destination_location_country_code=' + destination_location_country_code+'&item_categories=' + item_categories+'&total_weight=' + total_weight+'&total_worth=' + total_worth+'&distance='+distance,
        success: function (msg) {
            var resOj = $.parseJSON(msg);
            $('#span_package_service_fee').html(resOj.convertedPrice);
            $('#package_service_fee').val(resOj.orginalPrice);
         }
    });

}

function getProjectServiceFee(){

    var origin_location_country_code = destination_location_country_code = '';
    if($('#origin_location_country_code').length != 0){
        origin_location_country_code = $('#origin_location_country_code').val();
        destination_location_country_code = $('#destination_location_country_code').val();
    }

    var work_category = $('#work_category').val();
     var item_categories =   [];
      $(".product_category").each(function() {
         var arrId = $(this).attr('id').split('_');
         var suv_cat_id = $('#product_additional_requirements_category_'+arrId[2]).val();
         var item_weight = $('#item_weight_'+arrId[2]).val();
         var item_worth = $('#product_price_'+arrId[2]).val();
         var join_cats = "" + $(this).val() + suv_cat_id+'_'+item_weight+'_'+item_worth ;
        item_categories.push(join_cats);
      });
       var  origin_location_id = $('#origin_location_id').val();
     var destination_location_id = $('#destination_location_id').val();

    getdistance(origin_location_id,destination_location_id);
      var task_category =   [];
      $(".task_category").each(function() {
         var arrId = $(this).attr('id').split('_');
         var suv_cat_id1 = $('#additional_requirements_category_'+arrId[2]).val();

         var cost_willing_to_pay = $('#cost_willing_to_pay_'+arrId[2]).val();
         var join_cats1 = "" + $(this).val() + suv_cat_id1+'_'+cost_willing_to_pay ;
         task_category.push(join_cats1);
      });
        var  distance = $('#distance').val();
      var total_weight = $('#total_weight').val();
      var total_worth = $('#total_worth').val();
    jQuery.ajax({
        type: "POST",
        url: base_url + '/trips/getprojectsfee',
        data: 'origin_location_country_code=' + origin_location_country_code+'&destination_location_country_code=' + destination_location_country_code+'&item_categories=' + item_categories+'&total_weight=' + total_weight+'&total_worth=' + total_worth+'&work_category='+work_category+'&task_category='+task_category+'&distance='+distance,
        success: function (msg) {
             var resOj = $.parseJSON(msg);
            $('#span_project_service_fee').html(resOj.convertedPrice);
            $('#project_service_fee').val(resOj.orginalPrice);

        }
    });
}
function loadMoreoptions(){
    if($('.more_country_sel').length != 0){
        $('.more_country_sel').change(function(){
            if($(this).val() == 'more'){
                $('.more_sel_text').remove();
                $('.more_country_sel').append($('#hidden_country').html());
            }
        });
    }
}
function bindAutoComplete(){



	$(".typeahead").autocomplete({
    source: function( request, response ) {
				$.getJSON( base_url + "/search-airport", request, function( data, status, xhr ) {
					var origin_location_id = $('#origin_location_id').val() || '';
					var destination_location_id = $('#destination_location_id').val() || '';
					$.each(data,function(idx,val){
						if(val.ID == origin_location_id || val.ID == destination_location_id ){
							data.splice(idx,1);
						}
					});
					response(data);
				});
			},
    minLength: 2,
    select: function (event, ui) {
       	/*alert('ui.item.code '+ui.item.code);*/
        var oprevoiusConName = opcountryCode = dprevoiusConName = dpcountryCode = oprevoiusConNamenew = opcountryCodenew = dprevoiusConNamenew = dpcountryCodenew = destination_iata_code = '';
        var element_id = $(this).attr('id');


        $appendOptions = ' <option value="" selected="selected">Select country</option>';
        var uplannedLoc = false;
        var NormalLoc = true;
       if($('#travel_plan_reservation_type').length > 0 && $('#travel_plan_reservation_type').val() == 2){
          uplannedLoc = true;NormalLoc = false;
       }

       if(uplannedLoc === true){
         if($('#unplanned_destination_location_country_code').length > 0){
            dpcountryCode = $('#unplanned_destination_location_country_code').val();
            dprevoiusConName = $('#unplanned_destination_location_country').val();
            if(dprevoiusConName != '')$appendOptions += ' <option value="'+dpcountryCode+'">'+dprevoiusConName+'</option>';
        }
        if($('#unplanned_origin_location_country_code').length > 0){
            dpcountryCode = $('#unplanned_origin_location_country_code').val();
            dprevoiusConName = $('#unplanned_origin_location_country').val();
            if(dprevoiusConName != '')$appendOptions += ' <option value="'+dpcountryCode+'">'+dprevoiusConName+'</option>';
        }
     }else{
       if($('#destination_location_country_code').length > 0){
            dpcountryCode = $('#destination_location_country_code').val();
            dprevoiusConName = $('#destination_location_country').val();
            if(dprevoiusConName != '')$appendOptions += ' <option value="'+dpcountryCode+'">'+dprevoiusConName+'</option>';
        }
         if($('#origin_location_country_code').length > 0){
            opcountryCode = $('#origin_location_country_code').val();
            oprevoiusConName = $('#origin_location_country').val();
            if(oprevoiusConName != '')$appendOptions += ' <option value="'+opcountryCode+'">'+oprevoiusConName+'</option>';
        }
     }
        $appendOptions += '  <option value="more" class="more_sel_text">More</option>';
        $('.more_country_sel').empty();
        $('.more_country_sel').append($appendOptions);

        if ($('#' + element_id + '_id').length == 0) {
            $('<input type="hidden" name="' + element_id + '_id" id="' + element_id + '_id" value="' + ui.item.ID + '" >').insertAfter('#' + element_id);
            $('<input type="hidden" name="' + element_id + '_code" id="' + element_id + '_code" value="' + ui.item.code + '" >').insertAfter('#' + element_id);
            $('<input type="hidden" name="' + element_id + '_iata_code" id="' + element_id + '_iata_code" value="' + ui.item.code + '" >').insertAfter('#' + element_id);

        } else {
            $('#' + element_id + '_id').val(ui.item.ID);
            $('#' + element_id + '_code').val(ui.item.code);
            $('#' + element_id + '_iata_code').val(ui.item.code);

        }
         if ($('#' + element_id + '_country_code').length == 0) {
            $('<input type="hidden" name="' + element_id + '_country_code" id="' + element_id + '_country_code" value="' + ui.item.country_code + '" >').insertAfter('#' + element_id);
            $('<input type="hidden" name="' + element_id + '_country" id="' + element_id + '_country" value="' + ui.item.country + '" >').insertAfter('#' + element_id);
            $('<input type="hidden" name="' + element_id + '_iata_code" id="' + element_id + '_iata_code" value="' + ui.item.code + '" >').insertAfter('#' + element_id);
       } else {
            $('#' + element_id + '_country_code').val(ui.item.country_code);
            $('#' + element_id + '_country').val(ui.item.country);
            $('#' + element_id + '_iata_code').val(ui.item.code);

        }
        if(element_id.indexOf('manual_') == -1){
            var countryCode = ui.item.country_code;
            var countryName = ui.item.country;
            if(opcountryCode != dpcountryCode){
                if(element_id.indexOf('origin_location') > -1){
                   console.log('origin_location');
                   if(opcountryCode != countryCode){
                     if($('#country_short option[value="'+opcountryCode+'"]').length > 0 && opcountryCode != ''){
                         var appendHtml =  $('#country_short option[value="'+opcountryCode+'"]').remove();
                     }
                     if($('#hidden_country option[value="'+opcountryCode+'"]').length == 0 && opcountryCode != ''){
                        console.log('Append Option: 1');
                         $('#hidden_country').append('<option value="'+opcountryCode+'">'+oprevoiusConName+'</option>');
                     }
                   }
                }
                if(element_id.indexOf('destination_location') > -1){
                    console.log('destination_location');

                    if(dpcountryCode != countryCode){
                        if($('#country_short option[value="'+dpcountryCode+'"]').length > 0 && dpcountryCode != ''){

                            var appendHtml =  $('#country_short option[value="'+dpcountryCode+'"]').remove();
                        }
                        if($('#hidden_country option[value="'+dpcountryCode+'"]').length == 0 && dpcountryCode != ''){
                           console.log('Append Option: 2');
                            $('#hidden_country').append('<option value="'+opcountryCode+'">'+dprevoiusConName+'</option>');
                        }
                    }
                }
           }

		   var selArrival = typeof $(selectedFlightData).last().toArray()[0] != 'undefined'? $(selectedFlightData).last().toArray()[0].arrival:'';
			var enterArrival = $('#destination_location_code').val();

			if(selArrival == enterArrival){
				$('#err_different_location').remove();
				$('.next').show();
			} else {
				if(selArrival != ''){
					addFlightMismatchErr();
				}
				if($('#userroleType').val() == 'traveller' || ($('#userroleType').val() == 'seeker' && $('#service_type').val() == 'people' && $('#trip_edit_id').val() == 'undefined')){
					$('.next').hide();
				}
			}

            if($('#country_short option[value="'+countryCode+'"]').length == 0){
                console.log('Append Option: 3');
              /*  // $('#country_short option:first').after('<option value="'+countryCode+'">'+countryName+'</option>');*/
            }
            if($('#hidden_country option[value="'+countryCode+'"]').length > 0){
                $('#hidden_country option[value="'+countryCode+'"]').remove();
            }
            $appendOptions = ' <option value="" selected="selected">Select country</option>';
		var origin_iata_code  = destination_iata_code= '';
       if(uplannedLoc === true){

          if($('#unplanned_origin_location_country_code').length > 0){

            opcountryCodenew = $('#unplanned_origin_location_country_code').val();
            dprevoiusConNamenew = $('#unplanned_origin_location_country').val();
            origin_iata_code = $('#unplanned_origin_location_code').val();
            if(dprevoiusConNamenew != '')$appendOptions += ' <option value="'+opcountryCodenew+'">'+dprevoiusConNamenew+'</option>';
        }
        if($('#unplanned_destination_location_country_code').length > 0){
            dpcountryCodenew = $('#unplanned_destination_location_country_code').val();
            dprevoiusConNamenew = $('#unplanned_destination_location_country').val();
            destination_iata_code = $('#unplanned_destination_location_code').val();
            if(dprevoiusConNamenew != '')$appendOptions += ' <option value="'+dpcountryCodenew+'">'+dprevoiusConNamenew+'</option>';
        }
       }else{

        if($('#origin_location_country_code').length > 0){
            opcountryCodenew = $('#origin_location_country_code').val();
            oprevoiusConNamenew = $('#origin_location_country').val();
            origin_iata_code = $('#origin_location_code').val();
            if(oprevoiusConNamenew != '')$appendOptions += ' <option value="'+opcountryCodenew+'">'+oprevoiusConNamenew+'</option>';
        }
        if($('#destination_location_country_code').length > 0){
            dpcountryCodenew = $('#destination_location_country_code').val();
            dprevoiusConNamenew = $('#destination_location_country').val();
            destination_iata_code = $('#destination_location_code').val();
            if(dprevoiusConNamenew != '')$appendOptions += ' <option value="'+dpcountryCodenew+'">'+dprevoiusConNamenew+'</option>';
        }
     }
        $appendOptions += '  <option value="more" class="more_sel_text">More</option>';
        $('.more_country_sel').empty();
        $('.more_country_sel').append($appendOptions);
            var usedNames = {};
            $("select[name='country_short'] > option").each(function () {
                if(usedNames[this.text]) {
                    $(this).remove();
                } else {
                    usedNames[this.text] = this.value;
                }
            });
            loadMoreoptions();
         if($('#origin_location_country_code').length > 0 && $('#destination_location_country_code').length > 0 && $('#origin_location_country_code').val() != '' && $('#destination_location_country_code').val() != '' ){
            //getServiceFee();
            if($('#displayFlightBtn').length !=0){
               $('#displayFlightBtn').show();
            }

         }
       }
       if($('#trip_route').length != 0){
          var triproute = origin_iata_code+'-'+destination_iata_code;
         $( "#trip_route" ).val( triproute );
     }
       var fetchAirportDetails = false;
        if (element_id == 'origin') {
            $('#destination').focus();
            fetchAirportDetails = true;
        }
        if (element_id == 'destination') {
            $('#date').focus();
             fetchAirportDetails = true;
        }
        if(fetchAirportDetails){
             getFlightStopsForSearch();
        }
        if($('#getlatlangvalue').length != 0)$('#getlatlangvalue').val(1);

    }

});
}

function getFlightStopsForSearch(){
   $('#noofstops').hide();
   if($('#origin_iata_code').length !=0 && $('#destination_iata_code').length !=0 && $('#origin_iata_code').val() != '' &&  $('#destination_iata_code').val() != '' && $('#date').val() != ''){
         var originplace = $('#origin_iata_code').val();
         var destinationplace = $('#destination_iata_code').val();
         var departure_date = $('#date').val();
          if($('.servicestravellerpeople').length != 0)
         $.ajax({
             type: "POST",
             url: base_url + "/trips/oneway",
             data: 'origin=' + originplace + '&destination=' + destinationplace + '&departure_date=' + departure_date,
             success: function(res) {
                $('#noofstops').show();
                 var resObj = $.parseJSON(res);
                  $('#divFlights').html('');
                  $('#loading_flights_msg').hide();
                 if(resObj.result == 'success'){
                    $('#noofstops').html(resObj.optionData);
                     $('#noofstops').selectpicker('refresh');
                   //  $('.servicestravellerpeople').show();
                  }

             }
         });
   }
}
function bindAirlinesAutoComplete(classname){
    $("."+classname).autocomplete({
    source: base_url + "/search-airlines",
    minLength: 1,
    select: function (event, ui) {
        var element_id = $(this).attr('id');
        var arrId = element_id.split('_');
        console.log('arrId[2]'+arrId[2]);
        if ($('#' + element_id + '_org_name').length == 0) {
            $('<input type="hidden" name="' + element_id + '_org_name" id="' + element_id + '_id" value="' + ui.item.airline_name + '" >').insertAfter('#' + element_id);
        } else {
            $('#' + element_id + '_org_name').val(ui.item.ID);
        }
         if ($('#manual_airline_carrier_'+arrId[2]).length != 0) {
            $('#manual_airline_carrier_'+arrId[2]).val(ui.item.airline_carrier);

         }
    }
});
}
/*

 $('.typeahead').typeahead({

 hint: true,

 highlight: true,

 minLength: 3,

 limit: 8

 }, {

 source: function(q, cb) {

 return $.ajax({

 dataType: 'json',

 type: 'get',

 url: base_url+'/index/search-airport?q=' +

 q,

 chache: false,

 success: function(data) {

 var result = [];

 $.each(data, function(index, val) {

 result.push({

 id: val.ID,

 value: val.name

 });

 });

 cb(result);

 }

 });

 }

 });

 */



/*

 $('.typeahead').typeahead({

 hint: true,

 highlight: true,

 minLength: 3,

 limit: 8

 }, {

 source: function(q, cb) {

 return $.ajax({

 dataType: 'json',

 type: 'get',

 url: 'http://gd.geobytes.com/AutoCompleteCity?callback=?&q=' +

 q,

 chache: false,

 success: function(data) {

 var result = [];

 $.each(data, function(index, val) {

 result.push({

 value: val

 });

 });

 cb(result);

 }

 });

 }

 });

 */

function bindDatePicker(classname){
    $('input.'+classname+', .input-daterange, .date-pick-inline').datepicker({
        todayHighlight: true,
        startDate: '-0d',
        autoclose: true
    }).on('changeDate', function(e) {
        if($(e.target).hasClass('service_date')){
            var service_date_id = $(e.target).attr('id'),
                index = service_date_id.lastIndexOf("_"),
                pos = service_date_id.substr(index+1);
            if($(this).val() !== ''){
                updateDateRange(service_date_id,'service_start_date_'+pos,'service_end_date_'+pos, 'date_flexibility'+pos );
            }
        }
        if($(e.target).attr('id') == 'delivery_date'){
            if($(this).val() !== ''){
                updateDateRange('delivery_date','project_start_date','project_end_date', 'date_flexibility' );
            }
        }
    });
    $('.input-daterange input[name="start"]').datepicker('setDate', new Date());
    $('.input-daterange input[name="end"]').datepicker('setDate', '+7d');
}
bindDatePicker('date-pick');

function bindTimePicker(classname){
    $('input.'+classname).timepicker({
        minuteStep: 15,
        showInpunts: false
    });
}
bindTimePicker('time-pick');

$('.input-daterange, .date-pick-inline').datepicker({
    todayHighlight: true,
    autoclose: true
});

$('input.booking_date').datepicker({
    todayHighlight: true,
    autoclose: true,
	endDate: new Date()
});

$('input.date-pick-years').datepicker({
    startView: 2
});

$('input.dob').datepicker({
    format: "dd-mm-yyyy",
    startView: 2,
    autoclose: true,
    orientation: "bottom left"
});

$(document).ready(function () {
    $('input#date').datepicker({
        format: "mm/dd/yyyy",
        todayHighlight: true,
        startDate: '-0d',
        autoclose: true
    }).on('changeDate', function (ev) {
        /* alert("testing glhdjdjn"); */
        $("#service").children("option[value='people']").prop('selected', true).trigger('change');
    });
});

$('.booking-item-price-calc .checkbox label').click(function () {
    var checkbox = $(this).find('input'),
            /* checked = $(checkboxDiv).hasClass('checked'), */
            checked = $(checkbox).prop('checked'),
            price = parseInt($(this).find('span.pull-right').html().replace('$', '')),
            eqPrice = $('#car-equipment-total'),
            tPrice = $('#car-total'),
            eqPriceInt = parseInt(eqPrice.attr('data-value')),
            tPriceInt = parseInt(tPrice.attr('data-value')),
            value,
            animateInt = function (val, el, plus) {
                value = function () {
                    if (plus) {
                        return el.attr('data-value', val + price);
                    } else {
                        return el.attr('data-value', val - price);
                    }
                };
                return $({
                    val: val
                }).animate({
                    val: parseInt(value().attr('data-value'))
                }, {
                    duration: 500,
                    easing: 'swing',
                    step: function () {
                        if (plus) {
                            el.text(Math.ceil(this.val));
                        } else {
                            el.text(Math.floor(this.val));
                        }
                    }
                });
            };

    if (!checked) {
        animateInt(eqPriceInt, eqPrice, true);
        animateInt(tPriceInt, tPrice, true);
    } else {
        animateInt(eqPriceInt, eqPrice, false);
        animateInt(tPriceInt, tPrice, false);
    }
});

$('div.bg-parallax').each(function () {
    var $obj = $(this);
    if ($(window).width() > 992) {
        $(window).scroll(function () {
            var animSpeed;
            if ($obj.hasClass('bg-blur')) {
                animSpeed = 10;
            } else {
                animSpeed = 15;
            }
            var yPos = -($(window).scrollTop() / animSpeed);
            var bgpos = '50% ' + yPos + 'px';
            $obj.css('background-position', bgpos);
        });
    }
});
$('.nav-drop').dropit();
//$('#notificationContainer').dropit();
window.onload = function () {
    setInterval(function () {
        $('.banner-section').css({"padding": "0 0"});
        $('.owl-carouselShow').fadeIn("slow");
        var owl = $('.owl-carousel');
        owl.owlCarousel({
            autoPlay: 3000,
            stopOnHover: true,
            navigation: true,
            lazyLoad: true,
            paginationSpeed: 1000,
            goToFirstSpeed: 2000,
            singleItem: true,
            transitionStyle: "fade"
        });
    }, 1000);

};

$(document).ready(function () {
   /* footer always on bottom */
    var docHeight = $(window).height();
    var footerHeight = $('#main-footer').height();
    var footerTop = $('#main-footer').position().top + footerHeight;
    if (footerTop < docHeight) {
        /*$('#main-footer').css('margin-top', (docHeight - footerTop) + 'px');*/
    }
    var sbh = $(".search-container-home").innerHeight();
    var d11 = $(".header-top").innerHeight();
    var sbhN = sbh + d11;
    var ss1 = docHeight - sbhN;
    var ss2 = docHeight - sbh;
    var videoPlayer = $('#videoPlayer');
    $('#videoPlayer').css('height', (docHeight - d) + 'px');
    $("#videoPlayer").bind("ended", function () {
        setTimeout(closepromo, 1500);
    });
});

$(document).ready(function () {
    //resize();
});
$(window).load(function() {
    //resize();
});
window.onresize = resize;
var topA = $(".top-area").innerHeight();
var d = $(".header-top").innerHeight();
function resize() {
    var d = $(".header-top").innerHeight();
    var d2 = $(window).height() - d;
    var mu = d + 10;
    $("#sidebar-wrapper, .overlay").css('top', d);
    $("#sidebar-wrapper").innerHeight(d2)
    if ($(window).width() >= 991) {
        $("body.inner-page-topSpace").css('padding-top', d);
    } else {
        $("body.inner-page-topSpace").css('padding-top', '0');
    }
    var wW = $(window).width();
    var br = $(".container").width();
    var mnp = wW - br;
    var mnp1 = mnp / 2;
    if (mnp1 <= 100) {
        $(".afL-breadcrumb").css('padding-left', '85px');
    } else {
        $(".afL-breadcrumb").css('padding-left', '0px');
    }
}


$(window).scroll(function () {
    if ($(window).width() >= 992) {
        var d = $(".header-top").innerHeight();
        var mu = d + 10;
        var scroll = $(window).scrollTop();

        var trepHeight = $(window).height() - d;
        if (scroll >= 40) {
            $("#wrapper .overlay").css('top', d);
        } else {
            $("#wrapper .overlay").css('top', d);
        }
    }if ($(window).width() >= 768 && $(window).width() <= 991) {
        var d = $(".header-top").innerHeight();
        var mu = d + 10;
        var scroll = $(window).scrollTop();
        var trepHeight = $(window).height() - d;
        if (scroll >= 40) {
            var df = $(window).height();
            $("#sidebar-wrapper").innerHeight(df);
            $("#sidebar-wrapper").css('top', '0');
            $("#wrapper .overlay").css('top', '0');
        } else {
            $("#sidebar-wrapper").css('top', d);
            $("#wrapper .overlay").css('top', '0');
        }
    } else {
        var d = $(".header-top").innerHeight();
        var mu = d + 10;
        var scroll = $(window).scrollTop();
        var trepHeight = $(window).height() - d;
        if (scroll >= 40) {
            var df = $(window).height();
            $("#sidebar-wrapper").innerHeight(df);
            $("#sidebar-wrapper").css('top', '0');
            $("#wrapper .overlay").css('top', '0');
        } else {
            $("#sidebar-wrapper").css('top', d);
            $("#wrapper .overlay").css('top', '0');
            $(".hamburger").css('top', mu);
        }
    }

    if ($(this).scrollTop() > trepHeight) {
        /* $("#videoPlayer").get(0).pause(); */
        $('.trep-toggler').slideUp(50);
        $('.top-area .header-top').removeAttr('style');
        if ($('.trep-toggler').hasClass('is-open')) {
            $('.trep-toggler').removeClass('is-open').addClass('is-closed');
            $('.header-bg').removeClass('fixed-header');
            $("body.inner-page-topSpace, .home-top").css('padding-top', 0);
            $("html, body").animate({
                scrollTop: topA
            }, 1000);
        return false;
        }
    return false;
}

});

/* Trepr Features */

$(document).ready(function () {

    /* =========

     $('.trep-features').css('display', 'none');

     $('.trep-feature').click( function() {

     $('.trep-features').show('slow', "swing");

     $('html,body').animate({ scrollTop: '+=2000px' });

     $('.carousel').hide('slow', "swing");

     }),



     $('.trep-features .close').click( function() {

     $('.trep-features').hide('slow', "swing");

     $('.carousel').show('slow', "swing");

     $('html,body').animate({ scrollTop: '+=2000px' });

     }),



     ============ */

    $(".trep-promo").click(function () {
        playpromo();
    });

    $(".promo-close").click(function () {
        closepromo();
    });
});

function playpromo() {
    $('.header-bg').addClass('fixed-header');
    $('.trep-toggler').slideDown().addClass('is-open');
    $("body.inner-page-topSpace, .home-top").css('padding-top', d);
    $(".promo-close").css('top', d + 5);
    $('html,body').animate({scrollTop: '+=-1000px'}, 'slow');
    $("#videoPlayer")[0].play();
}

function closepromo() {
    $("body.inner-page-topSpace, .home-top").css('padding-top', 0);
    $(".promo-close").css('top', d);
    $('.header-bg').removeClass('fixed-header');
    $("#videoPlayer")[0].pause();
    $('.trep-toggler').slideUp();
    $('html,body').animate({scrollTop: '+=-1000px'}, 'slow');
    $('.top-area .header-top').removeAttr('style');
    if ($('.trep-toggler').hasClass('is-open')) {
        $('.trep-toggler').removeClass('is-open').addClass('is-closed');

    }
}

$("#trepservice").change(function () {
     /*alert($(this).val());*/
    showRelaventSearchBox($(this).val());
});

function showRelaventSearchBox(service){
     $('.hidesearchoption').hide();
   var looking_for = $('#looking_for').val();
   if (service == 'people') {
        $('.services'+looking_for+'people').css('display', 'block');
        getFlightStopsForSearch();
    } else {
        $('.services'+looking_for+'people').hide('slow');
    }

    if (service == 'package') {
        $('.services'+looking_for+'package').css('display', 'block');
    } else {
        $('.services'+looking_for+'package').hide('slow');
    }

    if (service == 'project') {
       if(looking_for == 'seeker'){
          $('.services1'+looking_for+'project').css('display', 'block');
       }
        $('.services'+looking_for+'project').css('display', 'block');
    } else {
        $('.services'+looking_for+'project').hide('slow');
          $('.services1'+looking_for+'project').hide('slow');
    }
}
$(document).ready(function () {
    $('.carousel').carousel({
        hAlign: 'center',
        vAlign: 'center',
        hMargin: 0.2,
        vMargin: 0,
        frontWidth: 640,
        frontHeight: 490,
        carouselWidth: 930,
        carouselHeight: 490,
        bottom: 0,
        directionNav: false,
        reflection: false,
        shadow: false,
        speed: 500,
        buttonNav: 'none',
        autoplay: true,
        autoplayInterval: 4000,
        pauseOnHover: true,
        mouse: true,
        description: true,
        descriptionContainer: '.description',
        backOpacity: 1,
        before: function (carousel) {},
        after: function (carousel) {}
    });


});

$("#radius").ionRangeSlider({
    type: 'single',
    min: 0,
    max: 1000,
    from: 500,
    prefix: "km",
    grid: true,
    onChange: function (obj) {
        console.log(obj);
    }

});

/*$("#weight_carried").ionRangeSlider({
    type: "single",
    min: 0.5,
    max: 100,
    from: 0.5,
	step: 0.5,
    grid: true,
    onChange: function (data) {
		$("#weight_carrieds").val(data.from);
		var pos = 1;
           $('.volume-box-'+pos).removeClass('active');
            if(data.from <=1){
                $('#icon-doc_'+pos).addClass('active');
            }else if(data.from >=1 && data.from <=8){
                $('#icon-backbag_'+pos).addClass('active');
            }else if(data.from >=8 && data.from <=22){
                $('#icon-suitcase_'+pos).addClass('active');
            }else if(data.from >=22 && data.from <=29){
                $('#icon-package_'+pos).addClass('active');
            }else if(data.from >=29 && data.from <=100){
                $('#icon-biggest-package_'+pos).addClass('active');
            }
        }
});*/

/*$("#weight_accommodate").ionRangeSlider({
    type: "single",
    min: 0.5,
    max: 100,
    from: 0.5,
	step: 0.5,
    grid: true,
    onChange: function (data) {
       var pos = 2;
           $('.volume-box-'+pos).removeClass('active');
            if(data.from <=1){
                $('#icon-doc_'+pos).addClass('active');
            }else if(data.from >=1 && data.from <=8){
                $('#icon-backbag_'+pos).addClass('active');
            }else if(data.from >=8 && data.from <=22){
                $('#icon-suitcase_'+pos).addClass('active');
            }else if(data.from >=22 && data.from <=29){
                $('#icon-package_'+pos).addClass('active');
            }else if(data.from >=29 && data.from <=100){
                $('#icon-biggest-package_'+pos).addClass('active');
            }
        }
});*/

$("#airline_allowed_weight").ionRangeSlider({
    type: "single",
    min: 0.5,
    max: 100,
    from: parseFloat($('#airline_allowed_weight').val()) > 0.5?parseFloat($('#airline_allowed_weight').val()):0.5,
	step: 0.5,
    grid: true,
    onChange: function (data) {
		var pos = 1;
		$('.volume-box-'+pos).removeClass('active');
		if(data.from <=1){
			$('#icon-doc_'+pos).addClass('active');
		}else if(data.from >=1 && data.from <=8){
			$('#icon-backbag_'+pos).addClass('active');
		}else if(data.from >=8 && data.from <=22){
			$('#icon-suitcase_'+pos).addClass('active');
		}else if(data.from >=22 && data.from <=29){
			$('#icon-package_'+pos).addClass('active');
		}else if(data.from >=29 && data.from <=100){
			$('#icon-biggest-package_'+pos).addClass('active');
		}
	}
});
var slider = $("#weight_carried").data("ionRangeSlider");

function loadtraWeightScall(){
	if(typeof slider != 'undefined'){
		slider.update({
			min: 0.5,
			max: 100,
			from: 0.5,
		});
	}
}
$('.negligible_weight').each(function(index){
    var disabledState = false;
    if($(this).is(':checked')){
        disabledState = true;
    }
    loadPackageSlider(index+1,'',disabledState);
});


 /*//loadPackageSlider(1);*/
kToLbs(0.5);
var total_pack_weight =   [];
function loadPackageSlider(pos,prefixTxt,disabledState){
    prefixTxt = prefixTxt || ''; disabledState || false;
    var slider = $("#"+prefixTxt+"item_weight_"+pos).ionRangeSlider({
        type: 'single',
        min: 0.5,
        max: 100,
        from: 0.5,
		step: 0.5,
        /*prefix: "kgs",*/
        postfix: " kg(s)",
        grid: true,
        disable: disabledState,
        onChange: function (data) {
            var leftVal = $("#"+prefixTxt+"item_weight_"+pos).prev().find('.irs-single').css("left");
            splitedLeft = '0px';
            if(parseFloat(leftVal.split('px')[0]) != 0){
                var splitedLeft = (parseFloat(leftVal.split('px')[0])+20)+'px';
            }
            $('#'+prefixTxt+'lbs_item_weight_'+pos).css('left',splitedLeft);
            var total_weight = 0;
            window[prefixTxt+'item_weight_' + pos]  = data.from;
            total_pack_weight[pos] = data.from;
            if(pos != 1)total_weight = $('#'+prefixTxt+'total_weight').val();
            caluclateweightValues();
            var imperial =  kToLbs(data.from);
            $('#'+prefixTxt+'lbs_item_weight_'+pos).html(imperial.pounds +' lbs.')
            $('.volume-box-'+pos).removeClass('active');
            if(data.from <=1){
                $('#icon-doc_'+pos).addClass('active');
            }else if(data.from >=1 && data.from <=8){
                $('#icon-backbag_'+pos).addClass('active');
            }else if(data.from >=8 && data.from <=22){
                $('#icon-suitcase_'+pos).addClass('active');
            }else if(data.from >=22 && data.from <=29){
                $('#icon-package_'+pos).addClass('active');
            }else if(data.from >=29 && data.from <=100){
                $('#icon-biggest-package_'+pos).addClass('active');
            }
        },
        onFinish: function(obj){
        },
        onUpdate: function(obj){
            this.onChange(obj);
        },
        onStart: function (data) {

        },
   });

   var sliderData = slider.data("ionRangeSlider");

   $("#"+prefixTxt+"negligible_weight_"+pos).on('change',function(){
        if($(this).is(':checked')){
            sliderData.update({ disable: true,from:0.5 });
            $(this).parent().addClass('checked');
        }
        else{
            sliderData.update({ disable: false });
            $(this).parent().removeClass('checked');
        }
    });

    $(".negligible_size").each(function(){
        if($(this).is(':checked')){
            $('#'+prefixTxt+'item_length_'+pos+',#'+prefixTxt+'item_width_'+pos+',#'+prefixTxt+'item_height_'+pos).attr('readonly','readonly');
            $('#'+prefixTxt+'item_unit_'+pos).attr('readonly','readonly');
            $(this).parent().addClass('checked');
        }
        else{
            $('#'+prefixTxt+'item_length_'+pos+',#'+prefixTxt+'item_width_'+pos+',#'+prefixTxt+'item_height_'+pos).removeAttr('readonly');
            $('#'+prefixTxt+'item_unit_'+pos).removeAttr('readonly');
            $(this).parent().removeClass('checked');
        }
    });

    $("#"+prefixTxt+"negligible_size_"+pos).on('change',function(){
        if($(this).is(':checked')){
            $('#'+prefixTxt+'item_length_'+pos+',#'+prefixTxt+'item_width_'+pos+',#'+prefixTxt+'item_height_'+pos).attr('readonly','readonly');
            $('#'+prefixTxt+'item_unit_'+pos).attr('readonly','readonly');
            $(this).parent().addClass('checked');
        }
        else{
            $('#'+prefixTxt+'item_length_'+pos+',#'+prefixTxt+'item_width_'+pos+',#'+prefixTxt+'item_height_'+pos).removeAttr('readonly');
            $('#'+prefixTxt+'item_unit_'+pos).removeAttr('readonly');
            $(this).parent().removeClass('checked');
        }
    });

    $('.yesNo input:checkbox').on('click',function() {
         if($(this).is(':checked') === true){
            $(this).parent().addClass('checked');
        }else{
            $('.yesNo input:checkbox[name='+$(this).attr('name')+']').parent().removeClass('checked');
        }
    });
}

function negligibleChanges(){
    var prefixTxt = '';
    $('.yesNo input:checkbox').each(function(){
        if($(this).is(':checked') === true){
            $(this).parent().addClass('checked');
        }else{
            $('.yesNo input:checkbox[name='+$(this).attr('name')+']').parent().removeClass('checked');
        }
    });
    $('.negligible_size').each(function(){
        var data_id = $(this).attr('id'),
                index = data_id.lastIndexOf("_"),
                pos = data_id.substr(index+1);
        if($(this).is(':checked')){
            $('#'+prefixTxt+'item_length_'+pos+',#'+prefixTxt+'item_width_'+pos+',#'+prefixTxt+'item_height_'+pos).attr('readonly','readonly');
            $('#'+prefixTxt+'item_unit_'+pos).attr('readonly','readonly');
            $(this).parent().addClass('checked');
        }
        else{
            $('#'+prefixTxt+'item_length_'+pos+',#'+prefixTxt+'item_width_'+pos+',#'+prefixTxt+'item_height_'+pos).removeAttr('readonly');
            $('#'+prefixTxt+'item_unit_'+pos).removeAttr('readonly');
            $(this).parent().removeClass('checked');
        }
    });
}

function updatePackageSlider(pos,val,prefixTxt){
    prefixTxt = prefixTxt || '';
    var slider = $("#"+prefixTxt+"item_weight_"+pos).data("ionRangeSlider");
    slider.update({
        from: val
    });
}

function caluclateweightValues(){
    var total_weight = 0;
   $('.item_weight').each(function(){
      var id = $(this).attr('id');
      var splitId = id.split('_');
      var element = 'item_weight_'+splitId[2]+'_1';
        if(typeof total_pack_weight[splitId[2]] != "undefined"){
           total_weight += parseFloat(total_pack_weight[splitId[2]]);
      }
      $('#total_weight').val(total_weight);
   });
}


$('.item_worth').on('change',function(){
    calculateTotalWorth();
});
$('.product_price').on('change',function(){
    calculateTotalPrice();
});
function calculateTotalPrice(){
    var total_items_worth = 0;
    $('.product_price').each(function(){
        if(this.value != ''){
           total_items_worth = total_items_worth + parseFloat($(this).val());
        }

    });
    $('#total_worth').val(total_items_worth);
}

function calculateTotalWorth(){
    var total_items_worth = 0;
    $('.item_worth').each(function(){
        if(this.value != ''){
           total_items_worth = total_items_worth + parseFloat($(this).val());
        }
        $('#total_worth').val(total_items_worth);
    });
}

function kToLbs(pK) {
    var nearExact = pK/0.45359237;
    var lbs = Math.floor(nearExact);
    var oz = (nearExact - lbs) * 16;
    return {
        pounds: lbs,
        ounces: oz
    };
}

$('.i-check, .i-radio').iCheck({
    checkboxClass: 'i-check',
    radioClass: 'i-radio'

});

$('input#contenders').on('ifChecked', function (event) {
    event.preventDefault();
    $(this).tab('show');
});

$('input#addtrips').on('ifChecked', function (event) {
    event.preventDefault();
    $(this).tab('show');
});

$('input#anydate').on('ifChecked', function (event) {
    event.preventDefault();
    $('.input-daterange #switchDisabledRadio').attr("disabled", "disabled");
});

$('input#anydate').on('ifUnchecked', function (event) {
    event.preventDefault();
    $('.input-daterange #switchDisabledRadio').removeAttr("disabled");
});

$('[data-spy="scroll"]').each(function () {
    var $spy = $(this).scrollspy('refresh');
});

$('.booking-item-review-expand').click(function (event) {
    console.log('baz');
    var parent = $(this).parent('.booking-item-review-content');
    if (parent.hasClass('expanded')) {
        parent.removeClass('expanded');
    } else {
        parent.addClass('expanded');
    }
});

$('.stats-list-select > li > .booking-item-rating-stars > li').each(function () {
    var list = $(this).parent(),
            listItems = list.children(),
            itemIndex = $(this).index();

    $(this).hover(function () {
        for (var i = 0; i < listItems.length; i++) {
            if (i <= itemIndex) {
                $(listItems[i]).addClass('hovered');
            } else {
                break;
            }
        }
        $(this).click(function () {
            for (var i = 0; i < listItems.length; i++) {
                if (i <= itemIndex) {
                    $(listItems[i]).addClass('selected');
                } else {
                    $(listItems[i]).removeClass(
                            'selected');
                }
            }
        });
    }, function () {
        listItems.removeClass('hovered');
    });
});

$('.booking-item-container').children('.booking-item').click(function (event) {
    if ($(this).hasClass('active')) {
        $(this).removeClass('active');
        $(this).parent().removeClass('active');
    } else {
        $(this).addClass('active');
        $(this).parent().addClass('active');
        $(this).delay(1500).queue(function () {
            $(this).addClass('viewed')
        });
    }
});

$('[data-numeric]').payment('restrictNumeric');
$('.form-group-cc-number input').payment('formatCardNumber');
$('.form-group-cc-date input').payment('formatCardExpiry');
$('.form-group-cc-cvc input').payment('formatCardCVC');

function initialize() {
    if ($('#map-canvas').length) {
        var map, service;
        jQuery(function ($) {
            $(document).ready(function () {
                var latlng = new google.maps.LatLng(40.7564971, -73.9743277);
                var myOptions = {
                    zoom: 16,
                    center: latlng,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    scrollwheel: false
                };
                map = new google.maps.Map(document.getElementById(
                        "map-canvas"), myOptions);

                var marker = new google.maps.Marker({
                    position: latlng,
                    map: map
                });
                marker.setMap(map);
                $('a[href="#google-map-tab"]').on('shown.bs.tab',
                        function (e) {
                            google.maps.event.trigger(map, 'resize');
                            map.setCenter(latlng);
                        }
                );
            });
        });
    }

    var map = new google.maps.Map(document.getElementById('map-canvas'), {
        center: new google.maps.LatLng(40.7564971, -73.9743277),
        zoom: 13,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var marker = new google.maps.Marker({
        position: new google.maps.LatLng(40.7564971, -73.9743277),
        map: map
    });
    var infowindow;
    infowindow = new google.maps.InfoWindow();
}

$('.card-select > li').click(function () {
    self = this;
    $(self).addClass('card-item-selected');
    $(self).siblings('li').removeClass('card-item-selected');
    $('.form-group-cc-number input').click(function () {
        $(self).removeClass('card-item-selected');
    });
});

/* Lighbox gallery */

$('#popup-gallery').each(function () {
    $(this).magnificPopup({
        delegate: 'a.popup-gallery-image',
        type: 'image',
        gallery: {
            enabled: true
        }
    });
});

/* Lighbox image */

$('.popup-image').magnificPopup({
    type: 'image'
});

/* Lighbox text */

$('.popup-text').magnificPopup({
    removalDelay: 500,
    showCloseBtn: false,
    callbacks: {
        beforeOpen: function () {
            this.st.mainClass = this.st.el.attr('data-effect');
        }
    },
    midClick: true
});

/* Lightbox iframe */

$('.popup-iframe').magnificPopup({
    dispableOn: 700,
    type: 'iframe',
    removalDelay: 160,
    mainClass: 'mfp-fade',
    preloader: false

});

/* Lighbox map */
$('.popup-map').magnificPopup({
    removalDelay: 500,
    focus: '#street_address_1',
    showCloseBtn: false,
    callbacks: {
        beforeOpen: function () {
            this.st.mainClass = this.st.el.attr('data-effect');
        }
    },
    midClick: true
});

$('.form-group-select-plus').each(function () {
    var self = $(this),
            btnGroup = self.find('.btn-group').first(),
            select = self.find('select');
    btnGroup.children('label').last().click(function () {
        btnGroup.addClass('hidden');
        select.removeClass('hidden');
    });
});

/* Responsive videos */
$(document).ready(function () {
    $("body").fitVids();
});

$(function ($) {
    $('#twitter').tweet({
        username: "remtsoy", /* !paste here your twitter username! */
        count: 3
    });
});

$(function ($) {
    $("#twitter-ticker").tweet({
        username: "remtsoy", /* !paste here your twitter username! */
        page: 1,
        count: 20
    });
});

$(document).ready(function () {
    var ul = $('#twitter-ticker').find(".tweet-list");
    var ticker = function () {
        setTimeout(function () {
            ul.find('li:first').animate({
                marginTop: '-4.7em'
            }, 850, function () {
                $(this).detach().appendTo(ul).removeAttr('style');
            });
            ticker();
        }, 5000);
    };
    ticker();
});

$(function () {
    $('#ri-grid').gridrotator({
        rows: 4,
        columns: 8,
        animType: 'random',
        animSpeed: 1200,
        interval: 1200,
        step: 'random',
        preventClick: false,
        maxStep: 2,
        w992: {
            rows: 5,
            columns: 4
        },
        w768: {
            rows: 6,
            columns: 3
        },
        w480: {
            rows: 8,
            columns: 3
        },
        w320: {
            rows: 5,
            columns: 4
        },
        w240: {
            rows: 6,
            columns: 4
        }
    });
});

$(function () {
    $('#ri-grid-no-animation').gridrotator({
        rows: 4,
        columns: 8,
        slideshow: false,
        w1024: {
            rows: 4,
            columns: 6
        },
        w768: {
            rows: 3,
            columns: 3
        },
        w480: {
            rows: 4,
            columns: 4
        },
        w320: {
            rows: 5,
            columns: 4
        },
        w240: {
            rows: 6,
            columns: 4
        }
    });
});

var tid = setInterval(tagline_vertical_slide, 2500);

/*  vertical slide */
function tagline_vertical_slide() {
    var curr = $("#tagline ul li.active");
    curr.removeClass("active").addClass("vs-out");
    setTimeout(function () {
        curr.removeClass("vs-out");
    }, 500);

    var nextTag = curr.next('li');
    if (!nextTag.length) {
        nextTag = $("#tagline ul li").first();
    }
    nextTag.addClass("active");
}

function abortTimer() { /* to be called when you want to stop the timer */
    clearInterval(tid);
}

var trigger = $('.hamburger'),
        overlay = $('.overlay'),
        scrollMain = $('html'),
        isClosed = false;

function hamburger_cross() {
    if (isClosed === true) {
        overlay.hide();
        trigger.removeClass('is-open');
        scrollMain.removeClass('scrHidden');
        trigger.addClass('is-closed');
        isClosed = false;

         $('#sidebar-wrapper').niceScroll({
        cursorcolor: "#4493CB",
        cursoropacitymin: 0.3,
        background: "#fff",
        cursorborder: "0",
        autohidemode: false,
        cursorminheight: 30,
        horizrailenabled: true

}).hide();
         $("html").niceScroll({cursorwidth: "3px",
   cursorborder: "3px solid #000",
   cursorborderradius: "0px",
   cursoropacitymin: 0.2,
   cursorminheight: 110,
   cursorfixedheight: false
}).show();
    } else {
		if(typeof overlay.show == 'function'){
        	overlay.show();
		}
        trigger.removeClass('is-closed');
         scrollMain.addClass('scrHidden');
        trigger.addClass('is-open');
        isClosed = true;


       udateScrollBar('sidebar-wrapper')

        $("html").niceScroll({cursorwidth: "3px",
   cursorborder: "3px solid #000",
   cursorborderradius: "0px",
   cursoropacitymin: 0.2,
   cursorminheight: 110,
   cursorfixedheight: false
}).remove();
    }
}

trigger.click(function () {
    hamburger_cross();
});

overlay.click(function () {
    hamburger_cross();
    $('#wrapper').removeClass('toggled');
});

$('[data-toggle="offcanvas"]').click(function () {
    $('#wrapper').toggleClass('toggled');
});

$("input#gotoseeker").on('ifChanged', function (event) {
    window.location = base_url + '/switchuser/seeker';
});

$("input#gototraveler").on('ifChanged', function (event) {
    window.location = base_url + '/switchuser/traveller';
});

$('[data-spy="scroll"]').each(function () {
    var $spy = $(this).scrollspy('refresh');
});

$(function () {
    if ($("#add-seeker-people .form-group-select-plus").val() === 0) /* I'm supposing the "Other" option value is 0. */
        $("#yourTextBox").hide();
});

$(".tooltip-large").tooltip({
    delay: {show: 0, hide: 2000}
});

$('.bfh-languages').selectpicker({
    header: 'Select Language',
    title: 'English',
    liveSearchPlaceholder: 'Search your Languages',
    liveSearch: true,
    maxOptions: 5,
    size: 5
});

$('#trepservice').selectpicker({
    header: 'Select a Service',
    title: 'Select a Service',
    size: 5
});

var pacContainerInitialized = false;
$('#street_address_1').keypress(function () {
    if (!pacContainerInitialized) {
        $('.pac-container').css('z-index', '9999');
        pacContainerInitialized = true;
    }
});

$('#myTab a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
});



/* store the currently selected tab in the hash value */

$("ul.hash-nav-tabs > li > a").on("shown.bs.tab", function (e) {
    var id = $(e.target).attr("href").substr(1);
    window.location.hash = id;
});

/* on load of the page: switch to the currently selected tab */
var hash = window.location.hash;
$('#myTab a[href="' + hash + '"]').tab('show');

$(document).ready(function () {
    $("div#tasklists").hide();
    $(".projectTasks").on('change','.task_category',function () {
    /*$(".task_category").on('change',function () {*/
        var elId = $(this).attr('id');
        var elIndex = elId.lastIndexOf("_");
        var resultIdIndex = elId.substr(elIndex+1);

        var id = $(this).val();
        var dataString = 'id=' + id;
        if (id != "") {
            $("tasklists").show(1000);
        } else {
            $("#tasklists").hide(1000);
        }
        $.ajax({
            type: "POST",
            url: base_url + '/trips/getprojectsubcategory',
            data: dataString,
            cache: false,
            success: function (html) {
                $("#additional_requirements_category_"+resultIdIndex).html(html);
                $("#tasklists_"+resultIdIndex).show();
            }
        });
    });

    $("#work_category").on('change',function () {
    /*$(".task_category").on('change',function () {*/
        var workCategoryVal = $(this).val();
        workCategoryVal = (workCategoryVal == 1)?workCategoryVal:0;
        var dataString = 'id=' + workCategoryVal;
        $.ajax({
            type: "POST",
            url: base_url + '/trips/getprojectcategory',
            data: dataString,
            cache: false,
            success: function (html) {
                if(workCategoryVal == 1){
                    $('.product_category').html(html);
                } else {
                    $('.task_category').html(html);
                }
                var selectDefault = '<option value="">-- Select --</option>';
                $(".product_additional_requirements_category").html(selectDefault);
                $(".additional_requirements_category").html(selectDefault);
            }
        });
    });

    $(".productTasks").on('change','.product_category',function () {
    /*$(".task_category").on('change',function () {*/
        var elId = $(this).attr('id');
        var elIndex = elId.lastIndexOf("_");
        var resultIdIndex = elId.substr(elIndex+1);

        var id = $(this).val();
        var dataString = 'id=' + id;
        $.ajax({
            type: "POST",
            url: base_url + '/trips/getprojectsubcategory',
            data: dataString,
            cache: false,
            success: function (html) {
                $("#product_additional_requirements_category_"+resultIdIndex).html(html);
                $("#product_lists_"+resultIdIndex).show();
            }
        });
    });


     $(".packages_box").on('change','.category',function () {
    /*$(".task_category").on('change',function () {*/
        var elId = $(this).attr('id');
        var elIndex = elId.lastIndexOf("_");
        var resultIdIndex = elId.substr(elIndex+1);
        var id = $(this).val();
        var dataString = 'id=' + id;
        $.ajax({
            type: "POST",
            url: base_url + '/trips/getpackagesubcategory',
            data: dataString,
            cache: false,
            success: function (html) {
                $("#item_category_"+resultIdIndex).html(html);

            }
        });
    });
     $(".packages_boxSRC").on('change','.category',function () {
    /*$(".task_category").on('change',function () {*/
        var elId = $(this).attr('id');
        var elIndex = elId.lastIndexOf("_");
        var resultIdIndex = elId.substr(elIndex+1);
        var id = $(this).val();
        var dataString = 'id=' + id;
        $.ajax({
            type: "POST",
            url: base_url + '/trips/getpackagesubcategory',
            data: dataString,
            cache: false,
            success: function (html) {
                $("#item_category").html(html);

            }
        });
    });
     $(".productTasksSRC").on('change','.product_category',function () {
    /*$(".task_category").on('change',function () {*/
        var elId = $(this).attr('id');
        var elIndex = elId.lastIndexOf("_");
        var resultIdIndex = elId.substr(elIndex+1);

        var id = $(this).val();
        var dataString = 'id=' + id;
        $.ajax({
            type: "POST",
            url: base_url + '/trips/getprojectsubcategory',
            data: dataString,
            cache: false,
            success: function (html) {
                $("#product_additional_requirements_category").html(html);
                $("#product_lists").show();
            }
        });
    });
     $(".projectTasksSRC").on('change','.task_category',function () {
    /*$(".task_category").on('change',function () {*/
        var elId = $(this).attr('id');
        var elIndex = elId.lastIndexOf("_");
        var resultIdIndex = elId.substr(elIndex+1);

        var id = $(this).val();
        var dataString = 'id=' + id;
        if (id != "") {
            $("tasklists").show(1000);
        } else {
            $("#tasklists").hide(1000);
        }
        $.ajax({
            type: "POST",
            url: base_url + '/trips/getprojectsubcategory',
            data: dataString,
            cache: false,
            success: function (html) {
                $("#additional_requirements_category").html(html);
                $("#tasklists").show();
            }
        });
    });
     $("#trepservice").change(function() {
         if(this.value==="people"){
                 $('#origin').prop('placeholder',"Select origin airport or city");
                 $('#destination').prop('placeholder',"Select destination airport or city");
             }
             else if(this.value==="package")
             {
               $('#origin').prop('placeholder',"Select pick up location or airport");
                 $('#destination').prop('placeholder',"select delivery location or airport");
             }
             else
             {
                $('#origin').prop('placeholder',"Select product country or airport");
                 $('#destination').prop('placeholder',"select delivery city or airport");
             }
            });
            $('#unplanned_departure_date').change(function() {
     updateDateRange();
});
});

 /*function negligibleWeightSelected(){
    $('.')
}*/
