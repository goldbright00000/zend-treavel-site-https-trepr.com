/** Stripe payment gateway - START **/
var stripe,card;
$(document).ready(function(e) {
	stripe = Stripe(stripe_public_key);
	var elements = stripe.elements();
	var style = {
				  base: {
					color: '#32325d',
					lineHeight: '18px',
					fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
					fontSmoothing: 'antialiased',
					fontSize: '16px',
					'::placeholder': {
					  color: '#aab7c4'
					}
				  },
				  invalid: {
					color: '#fa755a',
					iconColor: '#fa755a'
				  }
				};
	card = elements.create('card', {style: style,hidePostalCode: true});
	card.mount('#card-element');
	card.addEventListener('change', function(event) {
		var displayError = document.getElementById('card-errors');
		if (event.error) {
			displayError.textContent = event.error.message;
			$('#card-errors').show();
		} else {
			displayError.textContent = '';
			$('#card-errors').hide();
		}
	});
});
function paymentProcessing(){
	$.blockUI({
        message: '<div class="alert alert-success"><p class="text-small">Processing payment...</p></div>',
		baseZ: 999999
    });
}
function paymentCompleted(){
	$.unblockUI();
}
function submitPaymentForm(){
	paymentProcessing();
	stripe.createToken(card).then(function(result) {
		if (result.error) {
			var errorElement = document.getElementById('card-errors');
			errorElement.textContent = result.error.message;
			$('#card-errors').show();
			paymentCompleted();
		} else {
			stripeTokenHandler(result.token);
			$('#card-errors').hide();
		}
	});
}
function callPaymentFn(type,result){
	$('#card-errors').hide();
	if(type == 'seeker_accept' || type == 'traveler_accept'){
		$('#form_process_payment').append('<textarea id="sourceInfo" name="sourceInfo">'+JSON.stringify(result)+'</textarea>');
	} else {
		$('#pay_card_info').append('<textarea id="sourceInfo" name="sourceInfo">'+JSON.stringify(result)+'</textarea>');
	}
				
	if(type == 'traveller'){
		setTimeout(function(){addTravellerTripRequest();},500);
	} else if(type == 'seeker_accept' || type == 'traveler_accept'){
		setTimeout(function(){processPayment();},500);
	} else if(type == 'profile'){
		setTimeout(function(){insertcard();},500);
	} else {
		setTimeout(function(){addSeekerTripRequest();},500);
	}
}
function addPaymentMethod(type){
	if(validatePaymentCardDetailsNew('new')){
		paymentProcessing();
		var ownerInfo = {
		  owner: {
			name: $('#holder_name').val()+' '+$('#holder_last_name').val(),
			address: {
			  postal_code: $('#card_billing_zipcode').val(),
			  country: $('#card_billing_country').val(),
			},
			email: userEmailId
		  },
		};
		stripe.createSource(card,ownerInfo).then(function(result) {
			if (result.error) {
				var errorElement = document.getElementById('card-errors');
				errorElement.textContent = result.error.message;
				$('#card-errors').show();
				paymentCompleted();
			} else {
				callPaymentFn(type,result);
			}
		});
	}
}
function generateStripeSourceToken(type){
	if (validateTravellerCardForm()) {
		if($('#cardtype').val() == 'existing_card'){
			callPaymentFn(type);
			return true;
		}
		paymentProcessing();
		var ownerInfo = {
		  owner: {
			name: $('#holder_name').val()+' '+$('#holder_last_name').val(),
			address: {
			  postal_code: $('#card_billing_zipcode').val(),
			  country: $('#card_billing_country').val(),
			},
			email: userEmailId
		  },
		};
		stripe.createSource(card,ownerInfo).then(function(result) {
			if (result.error) {
				var errorElement = document.getElementById('card-errors');
				errorElement.textContent = result.error.message;
				$('#card-errors').show();
				paymentCompleted();
			} else {
				callPaymentFn(type,result);
			}
		});
	}
}
function stripeTokenHandler(token) {
	var form = document.getElementById('form_process_payment');
	var hiddenInput = document.createElement('input');
	hiddenInput.setAttribute('type', 'hidden');
	hiddenInput.setAttribute('name', 'stripeToken');
	hiddenInput.setAttribute('value', token.id);
	form.appendChild(hiddenInput);
	
	var hiddenTextarea = document.createElement('textarea');
	hiddenTextarea.setAttribute('style', 'display:none');
	hiddenTextarea.setAttribute('name', 'cardInfo');
	hiddenTextarea.setAttribute('value', token.card);
	form.appendChild(hiddenTextarea);
	
  	setTimeout(function(){processPayment();},500);
}
/** Stripe payment gateway - END **/