var after_login_forward = '';
var loggedin_status = '';
var valid_card = true;
var map_fetch;


CustomMarker.prototype = new google.maps.OverlayView();

	function CustomMarker(latlng, map, args,images1) {
		this.latlng = latlng;	
		this.args = args;	
		this.setMap(map);
		console.log(map);
		this.images1=images1;
	}
	
	

CustomMarker.prototype.draw = function() {
	
	var self = this;
	
	var div = this.div;
	
	if (!div) {
	
		div = this.div = document.createElement('div');
		
		div.className = 'marker';
		
		div.style.position = 'absolute';
		div.style.cursor = 'pointer';
		div.style.width = '20px';
		div.style.height = '20px';
		div.style.background = 'blue';
		var img = document.createElement('img');
		  img.src = self.args.image;
		  img.style.width = '100%';
		  img.style.height = '100%';
		  img.style.position = 'absolute';
		  div.appendChild(img);
		if (typeof(self.args.marker_id) !== 'undefined') {
			div.dataset.marker_id = self.args.marker_id;
		}
		
		google.maps.event.addDomListener(div, "click", function(event) {
			alert('You clicked on a custom marker!');			
			google.maps.event.trigger(self, "click");
		});
		
		var panes = this.getPanes();
		panes.overlayImage.appendChild(div);
	}
	
	var point = this.getProjection().fromLatLngToDivPixel(this.latlng);
	
	if (point) {
		div.style.left = (point.x - 10) + 'px';
		div.style.top = (point.y - 20) + 'px';
	}
};

CustomMarker.prototype.remove = function() {
	if (this.div) {
		this.div.parentNode.removeChild(this.div);
		this.div = null;
	}	
};

CustomMarker.prototype.getPosition = function() {
	return this.latlng;	
};










$(document).ready(function() {
    if ($('#date_flexibility').val() != '') {
        updateDateRange();
    }
    if ($('.card_no').length != 0) {
        $('.card_no').validateCreditCard(function(result) {
            console.log(result);
            valid_card = true;
            $('#icon-change').removeClass('glyphicon-remove text-danger');
            $('#icon-change').removeClass('glyphicon-ok text-success');
            if (result.valid === false) {
                valid_card = false;
                $('#icon-change').addClass('glyphicon-remove text-danger');
            } else {
                $('#icon-change').addClass('glyphicon-ok text-success');
            }
        });
    }
    $(".numonly").keydown(function(event) { /* Allow: backspace, delete, tab, escape, and enter */
        if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) { /* let it happen, don't do anything */
            return;
        } else { /* Ensure that it is a number and stop the keypress */
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });
    $(".amountOnly").keydown(function(event) { /* Allow: backspace, delete, tab, escape, and enter */
        if (event.keyCode == 16 || event.keyCode == 110 || event.keyCode == 190 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) { /* let it happen, don't do anything */
            return;
        } else { /* Ensure that it is a number and stop the keypress */
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });
    $(".phoneonly").keydown(function(event) { /* Allow: backspace, delete, tab, escape, and enter */
        if (event.keyCode == 173 || event.keyCode == 107 || event.keyCode == 16 || event.keyCode == 61 || event.keyCode == 109 || event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 || event.keyCode == 13 || event.keyCode == 48 || event.keyCode == 57 || event.keyCode == 32 || (event.keyCode == 65 && event.ctrlKey === true) || (event.keyCode >= 35 && event.keyCode <= 39)) { /* let it happen, don't do anything */
            return;
        } else { /* Ensure that it is a number and stop the keypress */
            if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
                event.preventDefault();
            }
        }
    });
    travelPlanSelect(); /* validatePackageContactForm */
    var logginErrMsg = document.getElementById('logginErrMsg');
   /* var validator = $("#formlogin").validate({
       // errorContainer: logginErrMsg,
        meta: "validate",
		errorElement: "span",
        rules: {
            email: {
                required: true,
                email: true,
				message: 'The email address is required and cannot be empty'
            },
            pass: {
                required: true
            }
        },
        submitHandler: function(form) {
            $("#login-loader").show();
            var forwardto = $("#forwardto").val();
            var email = $("#email").val();
            var password = $("#pass").val();
            var param = "email=" + email + "&password=" + password;
			if(email ==""){
				
			}
			if(password ==""){
				
			}
            jQuery.ajax({
                type: "POST",
                url: baseurl + '/signin',
                data: param,
                success: function(msg) {
                    $("#login-loader").hide();
                    var datamsg = msg.split('@@@@');
                    if (datamsg[1] == 'loginfailed') {
                        $("#email").addClass("error");
                        $("#pass").addClass("error");
                        $("#logginErrMsg").slideDown();
                    } else if (datamsg[1] == 'loginsuccess') {
                        location.reload();
                        if (after_login_forward == 'refresh') {
                            location.reload();
                        }
                        $("#loggedin_img_name").html(datamsg[2]);
                        $("#topLinkLogin").hide();
                        $("#topLinkSingup").hide();
                        $("#loggedin_container").show();
                        $("#wrapper, #switchusersearch").css('display', 'block');
                        $.magnificPopup.close();
                        return false;
                    }
                }
            });
        }
    });*/
	$(document).on('submit','#formlogin',function(e){
		e.preventDefault();
		
		$('#loginErrors').hide();
            var forwardto = $("#forwardto").val();
            var email = $("#email").val();
            var password = $("#pass").val();
            var param = "email=" + email + "&password=" + password;
			if(email ==""){
				$("#emailLabel").show();
				$("#emailLabelInvalid").hide();
				$("#email").css({"background-color":"wheat","border-color":"#ffb400"});
			}
			if(password ==""){
				$("#passLabel").show();
				$("#pass").css({"background-color":"wheat","border-color":"#ffb400"});
			}else{
				$('#pass').css({"background-color":"white","border-color":"#3fb34f"});
			}
			if ($('#email').val() == '') {
				$('#emailLabel').show();
				$('#email').css({"background-color":"wheat","border-color":"#ffb400"});
			} else if ($('#email_address').val() != '' && !validateEmail($('#email').val())) {
				$('#emailLabel').show();
				$('#email').css({"background-color":"wheat","border-color":"#ffb400"});
			}else{
				$('#email').css({"background-color":"white","border-color":"#3fb34f"});
			}
			if(email !=="" && password !==""){
				$("#login-loader").show();
            $.ajax({
                type: "POST",
                url: baseurl + '/signin',
                data: param,
                success: function(msg) {
					if(msg =='@@@@loginfailed@@@@'){
						$('#loginErrors').show();
						$("#email,#pass").css({"background-color":"white","border-color":"#cccccc"});
					}
                    $("#login-loader").hide();
                    var datamsg = msg.split('@@@@');
                    if (datamsg[1] == 'loginfailed') {
                        $("#email").addClass("error");
                        $("#pass").addClass("error");
                        $("#logginErrMsg").slideDown();
                    } else if (datamsg[1] == 'loginsuccess') {
                        location.reload();
                        if (after_login_forward == 'refresh') {
                            location.reload();
                        }
                        $("#loggedin_img_name").html(datamsg[2]);
                        $("#topLinkLogin").hide();
                        $("#topLinkSingup").hide();
                        $("#loggedin_container").show();
                        $("#wrapper, #switchusersearch").css('display', 'block');
                        $.magnificPopup.close();
                        return false;
                    }
                }
            });
			}
	});
    var validator = $("#forgotPasswordBox").validate({ /*errorContainer: signinmsg,*/
        meta: "validate",
        rules: {
            reset_email: {
                required: true,
                email: true,
                remote: baseurl + "/checkuseremail/?from=resetpass"
            }
        },
        messages: {
            reset_email: {
                required: "email address is empty",
                email: "email address is invalid",
                remote: "email address already exist"
            }
        },
        submitHandler: function(form) {
            $('.err_messge').hide();
            var email = $("#reset_email").val();
            var param = "email=" + email;
            jQuery.ajax({
                type: "POST",
                url: baseurl + '/resetpassword',
                data: param,
                success: function(msg) {
                    if (msg == 1) {
                        $('#forgoteErrMsg').slideDown();
                    } else {
                        $('#forgoteSuccMsg').slideDown();
                    }
                }
            });
        }
    }); /*ar newsletter = $("#newslettermsg");*/
    var validator = $("#formnewsletter").validate({ /*errorContainer: newsletter,*/
        meta: "validate",
        rules: {
            name: {
                required: true,
                notEqual: "Full Name"
            },
            email_address: {
                required: true,
                email: true
            }
        },
        submitHandler: function(form) {
            var name = $("#name").val();
            var email_address = $("#email_address").val();
            var param = "email_address=" + email_address + "&name=" + name;
            jQuery.ajax({
                type: "POST",
                url: baseurl + '/application/newsletter',
                data: param,
                success: function(msg) {
                    tb_init('a.thickbox, area.thickbox, input.thickbox');
                    $.unblockUI();
                    loadblockUI('<div class="signInCont-popup" style="top: 0px; width: 400px; height: 100px;"><div class="col2"><div class="signbg"><p style="text-align: center;">' + msg + '</p></div></div></div>');
                    setTimeout('$.unblockUI();', 3000);
                    $("#TB_window").hide();
                    $("#TB_overlay").hide();
                }
            });
        }
    }); /*	$("#verifyme").click(function() {		identityjs.getLoginStatus(function(response) { // Available in the response object are `status` and `authResponse`			if (response.status === 'connected') { // The user has already authorized your application //.api('/me', {}, function(resp){ // returns user data: scopes and data_pending //});			} else { // The user has not authorized your application yet // Opens a new window to request access from the user for the specified scopes				identityjs.login({					scope: "phone email"				}, function(response) {					if (response.status === 'authorized') { // The user authorized your application						identityjs.api('/me', {}, function(resp) { // returns user data: scopes and data_pending                                                 //response = jQuery.parseJSON(resp);                                                 alert(resp.scopes.email.email);                                                    alert(resp.scopes.email.verified);   						});					} else { // User authorization denied, the user probably closed the window					};				});			}		});	});        */
    $("#people_detail").click(function() {
        $("#divPeopleDetail").toggle("slow");
    });
    $("#package_detail").click(function() {
        $("#divPackageDetail").toggle("slow");
    });
    $(".contact_address_type").click(function() {
        clrErr();
        var address_type = $(this).val();
        if (address_type == 'existing_address') {
            $('#existing_user_location').slideDown();
            $('#divNewAddress').slideUp();
            $('#existingAddressView').show();
        } else if (address_type == 'new_address') {
            $('#existing_user_location').slideUp();
            $('#divNewAddress').slideDown();
            $('#existingAddressView').hide();
        }
    });
    $(".seekerPeopleRequest").click(function() {
        var address_type = $(this).val();
        if (address_type == 'existing_trip') {
            $('#existing_trip').slideDown();
            $('#divSeekerPeopleRequestNew').slideUp();
        } else if (address_type == 'new_trip') {
            $('#existing_trip').slideUp();
            $('#divSeekerPeopleRequestNew').slideDown();
        }
    });
    $(".seekerPackageRequest").click(function() {
        var address_type = $(this).val();
        if (address_type == 'existing_trip') {
            $('#existing_package_trip').slideDown();
            $('#divSeekerPackageRequestNew').slideUp();
        } else if (address_type == 'new_trip') {
            $('#existing_package_trip').slideUp();
            $('#divSeekerPackageRequestNew').slideDown();
        }
    });
    $(".seekerProjectRequest").click(function() {
        var address_type = $(this).val();
        if (address_type == 'existing_trip') {
            $('#existing_project_trip').slideDown();
            $('#divSeekerProjectRequestNew').slideUp();
        } else if (address_type == 'new_trip') {
            $('#existing_project_trip').slideUp();
            $('#divSeekerProjectRequestNew').slideDown();
        }
    });
    $(".travellerPeopleRequest").click(function() {
        var address_type = $(this).val();
        if (address_type == 'existing_trip') {
            $('#existing_trip').slideDown();
            $('#divTravellerPeopleRequestNew').slideUp();
        } else if (address_type == 'new_trip') {
            $('#existing_trip').slideUp();
            $('#divTravellerPeopleRequestNew').slideDown();
        }
    });
    $(".travellerPackageRequest").click(function() {
        var address_type = $(this).val();
        if (address_type == 'existing_trip') {
            $('.existing_package_trip').slideDown();
            $('#divTravellerPackageRequestNew').slideUp();
        } else if (address_type == 'new_trip') {
            $('.existing_package_trip').slideUp();
            $('#divTravellerPackageRequestNew').slideDown();
        }
    });
    $(".cardtype").click(function() {
        var card_type = $(this).val();
        if (card_type == 'existing_card') {
            $('#scardinfo').slideDown();
            $('#cardinfo').slideUp();
        } else if (card_type == 'new_card') {
            $('#scardinfo').slideUp();
            $('#cardinfo').slideDown();
        }
    });
    $(".travellerProjectRequest").click(function() {
        var address_type = $(this).val();
        if (address_type == 'existing_trip') {
            $('#existing_project_trip').slideDown();
            $('#divTravellerProjectRequestNew').slideUp();
        } else if (address_type == 'new_trip') {
            $('#existing_project_trip').slideUp();
            $('#divTravellerProjectRequestNew').slideDown();
        }
    });
    $("#trip_type").change(function() {
        var chooseplan = $(this).val(); /*alert(chooseplan);*/
        if (chooseplan === '0') {
            $('#choose_type_one, #choose_type_two').hide('slide', {
                direction: 'down'
            }, 500);
        } else {
            var titleText = (chooseplan == 1) ? 'Enter one way trip details' : 'Enter onward trip Details';
            $('#trip_title').html(titleText);
            $('#choose_type_one').hide('slide', {
                direction: 'down'
            }, 1000);
            $('#choose_type_one').show('slide', {
                direction: 'up'
            }, 500);
        }
    });
    $("#travel_plan_reservation_type").change(function() {
        var chooseplan = $(this).val(); /*alert(chooseplan);*/
        $('#min_text').html('');
        if (chooseplan === '0') {
            $('#choose_type_one, #choose_type_two').hide('slide', {
                direction: 'down'
            }, 500);
        } else if (chooseplan === '1') {
            $('#choose_type_one').show('slide', {
                direction: 'up'
            }, 1000);
            $('#choose_type_two').hide('slide', {
                direction: 'down'
            }, 500);
        } else if (chooseplan === '2') {
            $('#min_text').html('(Minimum)');
            $('#choose_type_two').show('slide', {
                direction: 'up'
            }, 1000);
            $('#choose_type_one').hide('slide', {
                direction: 'down'
            }, 500);
        }
    });
    $('#projectserviceonlocation').modal('show');
    $('#pslocationbtn').click(function() {
        $('#projectserviceonlocation').modal('hide');
    });
    $("#frmAddTrip").submit(function(event) {
        $(this).unbind('submit'); /*event.preventDefault();*/
        var street_address_1 = $("#street_address_1").val();
        var city = $("#city").val();
        var country = $("#country_short option:selected").text();
        var state = $("#state").val();
        var zip_code = $("#postal_code").val();
        var param = street_address_1 + " " + city + " " + state + " " + zip_code + "," + country;
        jQuery.ajax({
            type: "POST",
            url: "//maps.google.com/maps/api/geocode/json?address=" + param + "&sensor=false&region=India",
            data: param,
            async: false,
            success: function(msg) { /*var obj = jQuery.parseJSON( msg );*/
                $("#latitude").val(msg.results[0].geometry.location.lat);
                $("#longitude").val(msg.results[0].geometry.location.lng);
            }
        });
    });
    if ($("#travel_plan_reservation_type").val() == 0) {
        $('#choose_type_one, #choose_type_two').hide('slide', {
            direction: 'down'
        }, 500);
    }
    checkTicketUpOption();
    $(".contact_plan_type").click(function() {
        travelPlanSelect();
    });
});

function travelPlanSelect() {
    clrErr();
    var address_type = $("[name='travel_plan_type']:checked").val(); /*alert(address_type);*/
    if (address_type == 'existing_plan') {
        $('#travel_plan').slideDown();
        $('#travel_plan_exist').slideDown();
        $('#plandetails').slideDown();
        $('#chooseplan').slideUp();
        $('#divNewplan').slideUp();
        $('.chooseTypes').slideUp();
    } else if (address_type == 'new_plan') {
        $('#travel_plan').slideUp();
        $('#travel_plan_exist').slideUp();
        $('#plandetails').slideUp();
        $('#chooseplan').slideDown();
        $('#divNewplan').slideDown();
        if ($('#trip_type').val() == '1' || $('#travel_plan_reservation_type').val() == '1') {
            $('#choose_type_one').show('slide', {
                direction: 'up'
            }, 1000);
        } else if ($('#trip_type').val() == '2' || $('#travel_plan_reservation_type').val() == '2') {
            $('#choose_type_two').show('slide', {
                direction: 'up'
            }, 1000);
        }
    }
}
$('.ticketSendOption').click(function() {
    checkTicketUpOption();
});

function checkTicketUpOption() {
    if ($('input[name=ticketOption]:checked').val() == 'uploadTicket') {
        $('.uploadTicketDiv').show();
        $('.emailTicketDiv').hide();
    } else if ($('input[name=ticketOption]:checked').val() == 'emailTicket') {
        $('.uploadTicketDiv').hide();
        $('.emailTicketDiv').show();
    }
}

function loadblockUI(msg) {
    $.blockUI({
        message: '<div class="alert alert-success"><p class="text-small">' + msg + '</p></div>'
    });
}
var markers = [];

function search_ajax_drop(data, map_center_lat, map_center_long) {
	// var maplarge;
    // var latlng = new google.maps.LatLng(map_center_lat, map_center_long);
    // var myOptions = {
        // center: latlng,
        // zoom: 8,
        // zoomControl: true,
        // zoomControlOptions: {
            // style: google.maps.ZoomControlStyle.SMALL
        // },
        // panControl: true,
        // mapTypeControl: false,
        // scaleControl: false,
        // streetViewControl: false,
        // overviewMapControl: true,
        // mapTypeId: google.maps.MapTypeId.ROADMAP
    // };
    // maplarge = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
	
	// $(data['users']).each(function(index,value){
		// var marklatlng=value['latitude']+","+value['longitude'];
		// var marker = new google.maps.Marker({
			// position: latlng,
			// map: maplarge
		// });
    // marker.setMap(maplarge);
	// })
    // var marker = new google.maps.Marker({
        // position: latlng,
        // map: maplarge
    // });
    // marker.setMap(maplarge); 
	
    $('a[href="#google-map-tab"]').on('shown.bs.tab', function(e) {
        google.maps.event.trigger(maplarge, 'resize');
        maplarge.setCenter(latlng);
    });
    deleteOverlays(); /*var listings = jQuery.parseJSON(data);*/
    var listings = data['users'];
    var arrMoreInfo = data['arrMoreInfo'];
    var image = baseurl + '/public/images/map_star.png';
    var icon_img_path = baseurl + '/public/images/';
    var j = 1;
    var items_count = transition_count = slides_count = 1;
    $('#listings_locator').html(''); /*if(locations.length>1);*/
    var profile_image_path;
    listings_html = '';
    var infowindow = new google.maps.InfoWindow({
        content: ""
    });
    items_count_img = 'tooltip-blue2.png'; /*for (var i = 0; i < locations.length; i++) {*/
    if (data['userrole'] == 'seeker') var search_for_controller = 'traveller';
    else if (data['userrole'] == 'traveller') var search_for_controller = 'seeker';
	var Latlng = new google.maps.LatLng(listings[0]['latitude'], listings[0]['longitude']);
	var mapOptions = {
			zoom: 14,
			center: Latlng,
			disableDefaultUI: true
		}
		
		map_fetch = new google.maps.Map(document.getElementById('map-canvas'), mapOptions);
		
    $.each(listings, function(i, listing) {
        if (items_count < 10) {
            label_number_position_left = 5;
            label_number_position_top = 25;
        } else {
            label_number_position_left = 5;
            label_number_position_top = 20; 
        }
        profile_image_path = baseurl + '/public/uploads/profile_picture/small/' + listing['image'];
        if (listing['image'].indexOf('http') != -1) profile_image_path = listing['image'];
        console.log('Index' + listing['image'].indexOf('http'));
		
		
		var myLatlng = new google.maps.LatLng(listing['latitude'], listing['longitude']);
		

		overlay = new CustomMarker(
			myLatlng, 
			map_fetch,
			{
				marker_id: '123',
				image1:profile_image_path
			}
		);
		
		
		
		
		// marker = new google.maps.Marker({
			// position: new google.maps.LatLng(listing['latitude'], listing['longitude']),
			////map: maplarge,
			// label: '' + items_count + '', 
			////label:items_count,
            // labelAnchor: new google.maps.Point(9, 25),
			// labelClass: "businessDetailPageMapMarkerLabels",
           // /* the CSS class for the label*/ labelInBackground: false
		// });
		// var marker1 = new MarkerWithLabel({
		  //position: new google.maps.LatLng(listing['latitude'], listing['longitude']),
		   // position: new google.maps.LatLng(40, -100),
		   
		   // draggable: false,
		   // map: maplarge,
		   // labelContent: '<span style="font-family:\'Montserrat\'; font-size: 14px; font-weight: normal; color: #FFFFFF;">' + items_count + '</span>',
		   // labelAnchor: new google.maps.Point(9, 25),
		   // labelClass: "businessDetailPageMapMarkerLabels", // the CSS class for the label
		   // labelInBackground: false
		 // });
		////marker.setMap(maplarge); 
		
		// console.log(new google.maps.LatLng(listing['latitude'], listing['longitude']));
        // var marker = new MarkerWithLabel({
            // map: maplarge,
            // position: new google.maps.LatLng(listing['latitude'], listing['longitude']),
            // labelContent: '<span style="font-family:\'Montserrat\'; font-size: 14px; font-weight: normal; color: #FFFFFF;">' + items_count + '</span>',
            // labelAnchor: new google.maps.Point(9, 25),
            // labelClass: "businessDetailPageMapMarkerLabels",
            // /* the CSS class for the label*/ labelInBackground: false
        // });
        // markers.push(marker); 
        if (listing['image_thumb']) img = "<?=$this->ThemeUrl();?>/<?=$this->listingimagepath;?>" + listing['image_thumb'];
        else img = "/images/no-image.jpg";
        var ratings = Math.round(listing['ratings']);
        var ratingsCount = Math.round(listing['ratingsCount']);
        reviews_img = '<img src="' + baseurl + '/public/img/star_' + ratings + '.png">';
        var ID = listing['ID']; /*+arrMoreInfo[ID]+*/
        contentString = '<div id="content' + items_count + '">' + '<div id="siteNotice" class="close-drak-small">' + '</div>' + '<div id="' + listing['ID'] + '" class="listings"><div class="selectmap-category-listout-list">' + '<div class="col-sm-12 row locate-img"><div class="searc-listimg"><img src="' + profile_image_path + ' "  width="100%"></div></div>' + '<div class="col-sm-12 row address-list locate-detail"><h4><a href="' + baseurl + '/' + search_for_controller + '/detail/' + listing['tripid'] + '?service=' + data['service'] + '">' + listing['first_name'] + ' ' + listing['last_name'] + '</a></h4><ul class="map-line">' + '<li class="map-list"><span class="map-list-in">Member Since : </span>' + listing['member_since'] + '</li>' + '<li class="map-list"><span class="map-list-in">Rating : </span>' + reviews_img + '</li>' + '<li class="map-list"><span class="map-list-in">Reviews : </span>' + ratingsCount + '</li>' + '</ul></div>' + '</div>';
        // bindInfoWindow(marker, maplarge, infowindow, contentString);
        listings_html = listings_html + '<li><div class="row"><div class="col-xs-9">';
        listings_html = listings_html + '<h4 class="thumb-list-item-title"><a href="' + baseurl + '/' + search_for_controller + '/detail/' + listing['tripid'] + '?service=' + data['service'] + '"><strong>' + listing['first_name'] + ' ' + listing['last_name'] + '</strong> </a></h4>';
        listings_html = listings_html + arrMoreInfo[ID];
        listings_html = listings_html + '<p class="thumb-list-item-desciption">Member Since: ' + listing['member_since'] + '</p>';
        listings_html = listings_html + '<p class="thumb-list-item-desciption">Rating: ' + reviews_img + '</p>';
        listings_html = listings_html + '<p class="thumb-list-item-desciption">Reviews: ' + ratingsCount + '</p>';
        listings_html = listings_html + '</div><div class="col-xs-3" style=" padding-left: 0"><div class="searc-listimg"><a href="#"><img src="' + profile_image_path + ' " width="80px"></a></div></div></div></li>';
        if (j % 5 == 0) {
            transition_count++;
            slides_count++;
        }
        j++;
        items_count++;
    });
    $('#search_results').html(listings_html);
    $.unblockUI();
}

function clearOverlays() {
    setAllMap(null);
}

function deleteOverlays() {
    clearOverlays();
    markers = [];
}

function setAllMap(maplarge) {
    for (var i = 0; i < markers.length; i++) {
        markers[i].setMap(maplarge);
    }
}

function changeRadius(value) {
    circle.setMap(null);
    if (value == 1) zoom = 13;
    else if (value == 2) zoom = 13;
    else if (value == 3) zoom = 12;
    else if (value == 5) zoom = 12;
    else if (value == 10) zoom = 11;
    else if (value == 20) zoom = 10;
    else zoom = 13;
    var mapOptions = {
        zoom: zoom,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        center: new google.maps.LatLng('<?=$this->map_center_lat;?>', '<?=$this->map_center_long;?>')
    };
    maplarge = new google.maps.Map(document.getElementById('map_canvas'), mapOptions);
    var myLatLng = new google.maps.LatLng('<?=$this->map_center_lat;?>', '<?=$this->map_center_long;?>');
    var beachMarker = new google.maps.Marker({
        position: myLatLng,
        map: maplarge,
        icon: image
    });
    maplarge.setCenter(myLatLng);
    var rad = $('#radius').val();
    var rad = rad * 1600; /* convert to meters if in miles*/
    circle = new google.maps.Circle({
        map: maplarge,
        clickable: false,
        /* metres*/ radius: rad,
        fillColor: '#C1BFEA',
        fillOpacity: .6,
        strokeColor: '#5A58ED',
        strokeOpacity: .4,
        strokeWeight: .8
    }); /* Attach circle to marker*/
    circle.bindTo('center', beachMarker, 'position');
    show_selected_interests();
}

function bindInfoWindow(markers, map, infowindow, strDescription) {
    google.maps.event.addListener(markers, 'click', function() {
        infowindow.setContent(strDescription);
        infowindow.open(map, markers);
    });
}











function search(pageLoad) {
    var current_location = $('#current_location').val();
    var origin = $('#origin').val();
    var destination = $('#destination').val();
    var origin_id = $('#origin_id').val();
    var destination_id = $('#destination_id').val();
    var start_date = $('#date').val();
    var days = $('#days').val();
    var service = $('#service').val();
    var looking_for = $('#looking_for').val();
    var noofstops = $('#noofstops').val();
    var radius = $('#radius').val();
    var airline_name = $('#airline_name').val();
    var flight_number = $('#flight_number').val();
    var search_type = 'ajax';
    var current_latitude = $('#current_latitude').val();
    var current_longitude = $('#current_longitude').val();
    if (pageLoad == 1) {
        radius = '';
        days = '';
    }
    if (current_location == '') current_location = origin;
    var getlatlangvalue = $('#getlatlangvalue').val();
    if (getlatlangvalue == 1 || (current_latitude == '' && current_longitude == '')) {
        getGooleLatLong(origin);
        current_latitude = $('#current_latitude').val();
        current_longitude = $('#current_longitude').val();
    }
    var locationType = $('input[name=locationType]:checked').val();
    var param = "locationType=" + locationType + "&search_type=" + search_type + "&origin_id=" + origin_id + "&destination_id=" + destination_id + "&start_date=" + start_date + "&days=" + days + "&service=" + service + "&looking_for=" + looking_for + "&radius=" + radius + "&airline_name=" + airline_name + "&flight_number=" + flight_number + "&current_latitude=" + current_latitude + "&current_longitude=" + current_longitude;
    jQuery.ajax({
        type: "POST",
        url: baseurl + "/search",
        data: $('#searchForm').serialize(),
        success: function(msg) {
            $.unblockUI();
            var current_latitude = $('#current_latitude').val();
            var current_longitude = $('#current_longitude').val();
            var data = jQuery.parseJSON(msg);
            console.log(data);
            if (data.users === false) {
                $('#search_results').html('<li>No results found.</li>');
                // var latlng = new google.maps.LatLng(current_latitude, current_longitude);
                // var myOptions = {
                    // center: latlng,
                    // zoom: 8,
                    // zoomControl: true,
                    // zoomControlOptions: {
                        // style: google.maps.ZoomControlStyle.SMALL
                    // },
                    // panControl: true,
                    // mapTypeControl: false,
                    // scaleControl: false,
                    // streetViewControl: false,
                    // overviewMapControl: true,
                    // mapTypeId: google.maps.MapTypeId.ROADMAP
                // };
                // maplarge = new google.maps.Map(document.getElementById("map-canvas"), myOptions);
                // var marker = new google.maps.Marker({
                    // position: latlng,
                    // map: maplarge
                // });
                // marker.setMap(maplarge);
                // $('a[href="#google-map-tab"]').on('shown.bs.tab', function(e) {
                    // google.maps.event.trigger(maplarge, 'resize');
                    // maplarge.setCenter(latlng);
                // });
                // deleteOverlays();
            } else {
                search_ajax_drop(data, current_latitude, current_longitude);
            }
        },
        beforeSend: function() {
            loadblockUI('<img style="width:auto;opacity:.5;"  src="' + baseurl + '/public/images/loading.gif" />');
        },
        complete: function() {
            $("html, body").stop().animate({
                scrollTop: 100
            }, '1000', 'swing', function() {});
            $.unblockUI();
        }
    });
}

function searchRelatedTrips() {
    jQuery.ajax({
        type: "POST",
        url: baseurl + "/search",
        data: $('#searchForm').serialize(),
        success: function(msg) {
            $.unblockUI();
            var data = jQuery.parseJSON(msg);
            console.log(data);
            if (data.users === false) {
                $('#search_results').html('<li>No users found.</li>');
            } else {
                search_related_ajax_drop(data);
            }
        },
        beforeSend: function() {},
        complete: function() {}
    });
}

function search_related_ajax_drop(data) { /*var listings = jQuery.parseJSON(data);*/
    var listings = data['users'];
    var arrMoreInfo = data['arrMoreInfo'];
    var image = baseurl + '/public/images/map_star.png';
    var icon_img_path = baseurl + '/public/images/';
    var j = 1;
    var items_count = transition_count = slides_count = 1;
    $('#listings_locator').html(''); /*if(locations.length>1);*/
    var profile_image_path;
    listings_html = '';
    items_count_img = 'tooltip-blue2.png'; /*for (var i = 0; i < locations.length; i++) {*/
    if (data['userrole'] == 'seeker') var search_for_controller = 'traveller';
    else if (data['userrole'] == 'traveller') var search_for_controller = 'seeker';
    $.each(listings, function(i, listing) {
        if (items_count < 10) {
            label_number_position_left = 5;
            label_number_position_top = 25;
        } else {
            label_number_position_left = 5;
            label_number_position_top = 20;
        }
        profile_image_path = baseurl + '/public/uploads/profile_picture/small/' + listing['image'];
        if (listing['image'].indexOf('http') != -1) profile_image_path = listing['image'];
        console.log('Index' + listing['image'].indexOf('http'));
        if (listing['image_thumb']) img = "<?=$this->ThemeUrl();?>/<?=$this->listingimagepath;?>" + listing['image_thumb'];
        else img = "/images/no-image.jpg";
        var ratings = Math.round(listing['ratings']);
        var ratingsCount = Math.round(listing['ratingsCount']);
        reviews_img = '<img src="' + baseurl + '/public/img/star_' + ratings + '.png">';
        var ID = listing['ID']; /*+arrMoreInfo[ID]+*/
        listings_html = listings_html + '<li><div class="row"><div class="col-xs-9">';
        listings_html = listings_html + '<h4 class="thumb-list-item-title"><a href="' + baseurl + '/' + search_for_controller + '/detail/' + listing['tripid'] + '"><strong>' + listing['first_name'] + ' ' + listing['last_name'] + '</strong> </a></h4>';
        listings_html = listings_html + arrMoreInfo[ID];
        listings_html = listings_html + '<p class="thumb-list-item-desciption">Member Since: ' + listing['member_since'] + '</p>';
        listings_html = listings_html + '<p class="thumb-list-item-desciption">Rating: ' + reviews_img + '</p>';
        listings_html = listings_html + '<p class="thumb-list-item-desciption">Reviews: ' + ratingsCount + '</p>';
        listings_html = listings_html + '</div><div class="col-xs-3" style=" padding-left: 0"><div class="searc-listimg"><a href="#"><img src="' + profile_image_path + ' " width="80px"></a></div></div></div></li>';
        if (j % 5 == 0) {
            transition_count++;
            slides_count++;
        }
        j++;
        items_count++;
    });
    $('#search_results').html(listings_html);
    $.unblockUI();
}

function getGooleLatLong(address) {
    var param = address;
    jQuery.ajax({
        type: "POST",
        url: "https://maps.google.com/maps/api/geocode/json?address=" + param + "&sensor=false&region=India",
        data: param,
        async: false,
        success: function(msg) { /*var obj = jQuery.parseJSON( msg );*/
            if (msg.status != 'ZERO_RESULTS') {
                $("#current_latitude").val(msg.results[0].geometry.location.lat);
                $("#current_longitude").val(msg.results[0].geometry.location.lng);
                if ($('#getlatlangvalue').length != 0) $('#getlatlangvalue').val(0);
            }
        }
    });
}

function validateTravellerPepleForm() {
    clrErr();
    $errStatus = true;
    var maleCount = 0,
        femaleCount = 0,
        totalCount = 0,
        totalFMCount = 0;
    if ($('#trip_people_count_more').hasClass('hidden')) {
        totalCount = $('[name="trip_people_count"]:checked').val();
    } else {
        totalCount = $('#trip_people_count_more').val();
    }
    if ($('#trip_people_male_count_more').hasClass('hidden')) {
        maleCount = $('[name="trip_people_male_count"]:checked').val();
    } else {
        maleCount = $('#trip_people_male_count_more').val();
    }
    if ($('#trip_people_female_count_more').hasClass('hidden')) {
        femaleCount = $('[name="trip_people_female_count"]:checked').val();
    } else {
        femaleCount = $('#trip_people_female_count_more').val();
    }
    if (isNaN(maleCount)) {
        maleCount = 1;
    }
    if (isNaN(femaleCount)) {
        femaleCount = 0;
    }
    totalFMCount = parseInt(maleCount) + parseInt(femaleCount);
    if ($('#trip_people_count_more').hasClass('hidden')) {
        if (validateFormFields('trip_people_count', 'Please select number of persons.', 'radio')) $errStatus = false;
    } else {
        if (validateFormFields('trip_people_count_more', 'Please select number of persons.', '')) $errStatus = false;
    }
    if (totalCount > 0) {
        if (totalFMCount != totalCount) {
            setErr('trip_people_female_count', 'Male & Female count should be equal to number of person');
            $errStatus = false;
        }
    }
    if (validateFormFields('trip_comments', 'Please enter the comments to this seeker.', '')) $errStatus = false;
    return $errStatus;
}

function validateTravellerPackageForm() {
    clrErr();
    $errStatus = true;
    if (validateFormFields('airline_allowed_weight', 'Please select weight allowed in airline per baggage.', '')) $errStatus = false;
    if (validateFormFields('weight_carried', 'Please select approx.weight carried by you over all.', '')) $errStatus = false;
    if (validateFormFields('weight_accommodate', 'Please select approx.weight of baggage you can accommodate.', '')) $errStatus = false;
    if (validateFormFields('not_wish_to_carry', 'Please enter items that you don\'t wish to carry.', '')) $errStatus = false;
    if (validateFormFields('comment', 'Please enter the comment\'s to this Seeker.', '')) $errStatus = false;
    return $errStatus;
}

function initAddTripPeople() {
    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav nav-tabs-new',
        'debug': false,
        onShow: function(tab, navigation, index) { /*console.log('onShow');*/ },
        onNext: function(tab, navigation, index) {
            console.log('index index' + index);
            if (index == 1) {
                console.log('validateTravellerTripDetailsForm:' + validateTravellerTripDetailsForm());
                return validateTravellerTripDetailsForm();
            }
            if (index == 2) {
                console.log('validateTravellerPepleForm:' + validateTravellerPepleForm());
                return validatePackageContactForm();
            }
        },
        onPrevious: function(tab, navigation, index) { /*console.log('onPrevious');*/ },
        onLast: function(tab, navigation, index) { /*console.log('onLast');*/ },
        onTabClick: function(tab, navigation, index, newindex) {
            if (newindex == 1) {
                return validateTravellerTripDetailsForm();
            }
            if (newindex == 2) {
                return validatePackageContactForm();
            } /*console.log('onTabClick');*/
        },
        onTabShow: function(tab, navigation, index) {
            if (index == 1) {
                initMap();
            }
            if (index == 2) {
                getServiceFee();
            }
            console.log('index tab show:' + index);
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({
                width: $percent + '%'
            }); /* If it's the last tab then hide the last button and show the finish instead*/
            if ($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }
    });
    $('#rootwizard .finish').click(function() {
        if (validateTravellerTripDetailsForm() == false) {
            $('#rootwizard a[href="#tab3"]').trigger('click');
            return false;
        }
        if (validateTravellerPepleForm() == false) {
            $('#rootwizard a[href="#tab1"]').trigger('click');
            return false;
        }
        if (validatePackageContactForm() == false) {
            $('#rootwizard a[href="#tab2"]').trigger('click');
            return false;
        }
        if (selectedFlightData !== false) {
            $('#selectedFlightData').val(JSON.stringify(selectedFlightData));
        }
        var address_type = $('input[name=contact_address_type]:checked', '#form_addtrippeople_origindetail').val();
        var action = $('#action').val(); /*$('#rootwizard').find("a[href*='tab1']").trigger('click'); //$( "#formAddTripPeople" ).submit();*/
        if (address_type == 'new_address') {
            if ($("#street_address_1").val() != '') {
                loadblockUI('Loading...');
                var street_address_1 = $("#street_address_1").val();
                var city = $("#city").val();
                var country = $("#country_short option:selected").text();
                var state = $("#state").val();
                var zip_code = $("#postal_code").val();
                var param = street_address_1 + " " + city + " " + state + " " + zip_code + "," + country;
                jQuery.ajax({
                    type: "POST",
                    url: "https://maps.google.com/maps/api/geocode/json?address=" + param + "&sensor=false&region=India",
                    data: param,
                    async: false,
                    success: function(msg) { /*var obj = jQuery.parseJSON( msg );*/
                        $("#latitude").val(msg.results[0].geometry.location.lat);
                        $("#longitude").val(msg.results[0].geometry.location.lng);
                        jQuery.ajax({
                            type: "POST",
                            url: baseurl + '/traveller/add-people-service',
                            data: $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#form_addtrippeople_people').serialize(),
                            success: function(msg) {
                                var msgdata = jQuery.parseJSON(msg);
                                if (msgdata.result == 'exist') {
                                    $.unblockUI();
                                    confirmDialog(msgdata.msg, 'Warning', function() {
                                        window.location = baseurl + msgdata.serviceUrl;
                                    });
                                } else {
                                    if ($('[name="ticketOption"]:checked').val() == 'uploadTicket') {
                                        if (msgdata.result == 'success') {
                                            travelFormData = new FormData();
                                            travelFormData.append('file', $('#ticket')[0].files[0]);
                                            travelFormData.append('tripId', msgdata.trip_ID);
                                            travelFormData.append('tripIdNumber', $('#tripIdNumber').val());
                                            $.ajax({
                                                type: 'POST',
                                                url: baseurl + '/traveller/uploadticket',
                                                data: travelFormData,
                                                cache: false,
                                                contentType: false,
                                                processData: false,
                                                success: function(mydata) {
                                                    var resDataObj = jQuery.parseJSON(mydata);
                                                    if (resDataObj.result == 'success') {
                                                        loadblockUI(msgdata.msg);
                                                        if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                                        else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-people-service/' + msgdata.trip_ID + '/return"', 3000);
                                                        else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=people"', 3000);
                                                    }
                                                }
                                            });
                                        }
                                    } else {
                                        loadblockUI(msgdata.msg);
                                        if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                        else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-people-service/' + msgdata.trip_ID + '/return"', 3000);
                                        else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=people"', 3000);
                                    }
                                }
                            }
                        });
                    }
                });
            }
        } else {
            loadblockUI('Loading...');
            jQuery.ajax({
                type: "POST",
                url: baseurl + '/traveller/add-people-service',
                data: $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#form_addtrippeople_people').serialize(),
                success: function(msg) {
                    var msgdata = jQuery.parseJSON(msg);
                    if (msgdata.result == 'exist') {
                        $.unblockUI();
                        confirmDialog(msgdata.msg, 'Warning', function() {
                            window.location = baseurl + msgdata.serviceUrl;
                        });
                    } else {
                        if ($('[name="ticketOption"]:checked').val() == 'uploadTicket') {
                            if (msgdata.result == 'success') {
                                travelFormData = new FormData();
                                travelFormData.append('file', $('#ticket')[0].files[0]);
                                travelFormData.append('tripId', msgdata.trip_ID);
                                travelFormData.append('tripIdNumber', $('#tripIdNumber').val());
                                $.ajax({
                                    type: 'POST',
                                    url: baseurl + '/traveller/uploadticket',
                                    data: travelFormData,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function(mydata) {
                                        var resDataObj = jQuery.parseJSON(mydata);
                                        if (resDataObj.result == 'success') {
                                            loadblockUI(msgdata.msg);
                                            if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                            else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-people-service/' + msgdata.trip_ID + '/return"', 3000);
                                            else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=people"', 3000);
                                        }
                                    }
                                });
                            }
                        } else {
                            loadblockUI(msgdata.msg);
                            if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                            else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-people-service/' + msgdata.trip_ID + '/return"', 3000);
                            else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=people"', 3000);
                        }
                    }
                }
            });
        }
    });
    $("#formAddTripPeople").on("submit", function(event) {
        event.preventDefault();
    });
}

function initAddTripPackage() {
    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav',
        'debug': false,
        onShow: function(tab, navigation, index) { /*console.log('onShow');*/ },
        onNext: function(tab, navigation, index) {
            if (index == 1) {
                return validateTravellerTripDetailsForm();
            }
            if (index == 2) {
                if (validatePackageContactForm() == false) {
                    $('#rootwizard a[href="#tab2"]').trigger('click');
                    return false;
                }
            }
            loadtraWeightScall();
        },
        onPrevious: function(tab, navigation, index) { /*console.log('onPrevious');*/ },
        onLast: function(tab, navigation, index) { /*console.log('onLast');*/ },
        onTabClick: function(tab, navigation, index, newindex) {
            loadtraWeightScall();
            console.log('loadtraWeightScall');
            if (newindex == 1) {
                return validateTravellerTripDetailsForm();
            }
            if (newindex == 2) {
                if (validatePackageContactForm() == false) {
                    $('#rootwizard a[href="#tab2"]').trigger('click');
                    return false;
                }
            } /*console.log('onTabClick');*/
        },
        onTabShow: function(tab, navigation, index) {
            if (index == 1) {
                initMap();
            } /*console.log('onTabShow');*/
            getServiceFee();
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({
                width: $percent + '%'
            }); /* If it's the last tab then hide the last button and show the finish instead*/
            if ($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }
    });
    $('#rootwizard .finish').click(function() {
        if (validateTravellerTripDetailsForm() == false) {
            $('#rootwizard a[href="#tab1"]').trigger('click');
            return false;
        }
        if (validatePackageContactForm() == false) {
            $('#rootwizard a[href="#tab2"]').trigger('click');
            return false;
        }
        if (validateTravellerPackageForm() == false) {
            $('#rootwizard a[href="#tab3"]').trigger('click');
            return false;
        }
        loadtraWeightScall();
        if (selectedFlightData !== false) {
            $('#selectedFlightData').val(JSON.stringify(selectedFlightData));
        }
        var address_type = $('[name="contact_address_type"]:checked').val(); /*$('#rootwizard').find("a[href*='tab1']").trigger('click'); //$( "#formAddTripPeople" ).submit();*/
        if (address_type == 'new_address') {
            if ($("#street_address_1").val() != '') {
                loadblockUI('Loading...');
                var street_address_1 = $("#street_address_1").val();
                var city = $("#city").val();
                var country = $("#country_short option:selected").text();
                var state = $("#state").val();
                var zip_code = $("#postal_code").val();
                var param = street_address_1 + " " + city + " " + state + " " + zip_code + "," + country;
                jQuery.ajax({
                    type: "POST",
                    url: "https://maps.google.com/maps/api/geocode/json?address=" + param + "&sensor=false&region=India",
                    data: param,
                    async: false,
                    success: function(msg) { /*var obj = jQuery.parseJSON( msg );*/
                        $("#latitude").val(msg.results[0].geometry.location.lat);
                        $("#longitude").val(msg.results[0].geometry.location.lng);
                        jQuery.ajax({
                            type: "POST",
                            url: baseurl + '/traveller/add-package-service',
                            data: $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#form_addtrippeople_package').serialize(),
                            success: function(msg) {
                                var msgdata = jQuery.parseJSON(msg);
                                if (msgdata.result == 'exist') {
                                    $.unblockUI();
                                    confirmDialog(msgdata.msg, 'Warning', function() {
                                        window.location = baseurl + msgdata.serviceUrl;
                                    });
                                } else {
                                    if ($('[name="ticketOption"]:checked').val() == 'uploadTicket') {
                                        if (msgdata.result == 'success') {
                                            travelFormData = new FormData();
                                            travelFormData.append('file', $('#ticket')[0].files[0]);
                                            travelFormData.append('tripId', msgdata.trip_ID);
                                            travelFormData.append('tripIdNumber', $('#tripIdNumber').val());
                                            $.ajax({
                                                type: 'POST',
                                                url: baseurl + '/traveller/uploadticket',
                                                data: travelFormData,
                                                cache: false,
                                                contentType: false,
                                                processData: false,
                                                success: function(mydata) {
                                                    var resDataObj = jQuery.parseJSON(mydata);
                                                    if (resDataObj.result == 'success') {
                                                        loadblockUI(msgdata.msg);
                                                        if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                                        else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-package-service/' + msgdata.trip_ID + '/return"', 3000);
                                                        else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=package"', 3000);
                                                    }
                                                }
                                            });
                                        }
                                    } else {
                                        loadblockUI(msgdata.msg);
                                        if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                        else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-package-service/' + msgdata.trip_ID + '/return"', 3000);
                                        else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=package"', 3000);
                                    }
                                }
                            }
                        });
                    }
                });
            }
        } else {
            loadblockUI('Loading...');
            jQuery.ajax({
                type: "POST",
                url: baseurl + '/traveller/add-package-service',
                data: $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#form_addtrippeople_package').serialize(),
                success: function(msg) {
                    var msgdata = jQuery.parseJSON(msg);
                    if (msgdata.result == 'exist') {
                        $.unblockUI();
                        confirmDialog(msgdata.msg, 'Warning', function() {
                            window.location = baseurl + msgdata.serviceUrl;
                        });
                    } else { /* loadblockUI(msgdata.msg);*/
                        if ($('[name="ticketOption"]:checked').val() == 'uploadTicket') {
                            if (msgdata.result == 'success') {
                                travelFormData = new FormData();
                                travelFormData.append('file', $('#ticket')[0].files[0]);
                                travelFormData.append('tripId', msgdata.trip_ID);
                                travelFormData.append('tripIdNumber', $('#tripIdNumber').val());
                                $.ajax({
                                    type: 'POST',
                                    url: baseurl + '/traveller/uploadticket',
                                    data: travelFormData,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function(mydata) {
                                        var resDataObj = jQuery.parseJSON(mydata);
                                        if (resDataObj.result == 'success') {
                                            loadblockUI(msgdata.msg);
                                            if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                            else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-package-service/' + msgdata.trip_ID + '/return"', 3000);
                                            else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=package"', 3000);
                                        }
                                    }
                                });
                            }
                        } else {
                            loadblockUI(msgdata.msg);
                            if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                            else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-package-service/' + msgdata.trip_ID + '/return"', 3000);
                            else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=package"', 3000);
                        }
                    }
                }
            });
        }
    });
}

function validateTravellerProjectForm() {
    clrErr();
    $errStaus = false;
    if (validateFormFields('task', 'Please select a category.', '')) $errStaus = true;
    if (!$('.anyDate').parent('.i-check').hasClass('checked')) {
        if (validateFormFields('project_start_date', 'Please select from date.', '')) $errStaus = true;
        if (validateFormFields('project_end_date', 'Please select to date.', '')) $errStaus = true;
    }
    if (validateFormFields('comment', 'Please enter Comments to the seeker.', '')) $errStaus = true;
    if ($errStaus) {
        return false;
    }
    return true;
}

function initAddTripProject() {
    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav',
        'debug': false,
        onShow: function(tab, navigation, index) { /*console.log('onShow');*/ },
        onNext: function(tab, navigation, index) {
            if (index == 1) {
                return validateTravellerTripDetailsForm();
            }
            if (index == 2) {
                if (validatePackageContactForm() == false) {
                    $('#rootwizard a[href="#tab2"]').trigger('click');
                    return false;
                }
            }
        },
        onPrevious: function(tab, navigation, index) { /*console.log('onPrevious');*/ },
        onLast: function(tab, navigation, index) { /*console.log('onLast');*/ },
        onTabClick: function(tab, navigation, index, newindex) {
            if (newindex == 1) {
                return validateTravellerTripDetailsForm();
            }
            if (newindex == 2) {
                if (validatePackageContactForm() == false) {
                    $('#rootwizard a[href="#tab2"]').trigger('click');
                    return false;
                }
            } /*console.log('onTabClick');*/
        },
        onTabShow: function(tab, navigation, index) {
            if (index == 1) {
                initMap();
            }
            getServiceFee(); /*console.log('onTabShow');*/
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({
                width: $percent + '%'
            }); /* If it's the last tab then hide the last button and show the finish instead*/
            if ($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }
    });
    $('#rootwizard .finish').click(function() {
        if (validateTravellerTripDetailsForm() == false) {
            $('#rootwizard a[href="#tab1"]').trigger('click');
            return false;
        }
        if (validatePackageContactForm() == false) {
            $('#rootwizard a[href="#tab2"]').trigger('click');
            return false;
        }
        if (validateTravellerProjectForm() == false) {
            $('#rootwizard a[href="#tab3"]').trigger('click');
            return false;
        }
        if (selectedFlightData !== false) {
            $('#selectedFlightData').val(JSON.stringify(selectedFlightData));
        }
        var address_type = $('input[name=contact_address_type]:checked', '#form_addtrippeople_origindetail').val(); /*$('#rootwizard').find("a[href*='tab1']").trigger('click'); //$( "#formAddTripPeople" ).submit();*/
        if (address_type == 'new_address') {
            if ($("#street_address_1").val() != '') {
                loadblockUI('Loading...');
                var street_address_1 = $("#street_address_1").val();
                var city = $("#city").val();
                var country = $("#country_short option:selected").text();
                var state = $("#state").val();
                var zip_code = $("#postal_code").val();
                var param = street_address_1 + " " + city + " " + state + " " + zip_code + "," + country;
                jQuery.ajax({
                    type: "POST",
                    url: "https://maps.google.com/maps/api/geocode/json?address=" + param + "&sensor=false&region=India",
                    data: param,
                    async: false,
                    success: function(msg) { /*var obj = jQuery.parseJSON( msg );*/
                        $("#latitude").val(msg.results[0].geometry.location.lat);
                        $("#longitude").val(msg.results[0].geometry.location.lng);
                        jQuery.ajax({
                            type: "POST",
                            url: baseurl + '/traveller/add-project-service',
                            data: $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#form_addtrippeople_project').serialize(),
                            success: function(msg) {
                                var msgdata = jQuery.parseJSON(msg);
                                if (msgdata.result == 'exist') {
                                    $.unblockUI();
                                    confirmDialog(msgdata.msg, 'Warning', function() {
                                        window.location = baseurl + msgdata.serviceUrl;
                                    });
                                } else {
                                    if (msgdata.result == 'success') {
                                        if ($('[name="ticketOption"]:checked').val() == 'uploadTicket') {
                                            travelFormData = new FormData();
                                            travelFormData.append('file', $('#ticket')[0].files[0]);
                                            travelFormData.append('tripId', msgdata.trip_ID);
                                            travelFormData.append('tripIdNumber', $('#tripIdNumber').val());
                                            $.ajax({
                                                type: 'POST',
                                                url: baseurl + '/traveller/uploadticket',
                                                data: travelFormData,
                                                cache: false,
                                                contentType: false,
                                                processData: false,
                                                success: function(mydata) {
                                                    var resDataObj = jQuery.parseJSON(mydata);
                                                    if (resDataObj.result == 'success') {
                                                        loadblockUI(msgdata.msg);
                                                        if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                                        else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-project-service/' + msgdata.trip_ID + '/return"', 3000);
                                                        else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=project"', 3000);
                                                    }
                                                }
                                            });
                                        }
                                    } else {
                                        loadblockUI(msg);
                                        if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                        else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-project-service/' + msgdata.trip_ID + '/return"', 3000);
                                        else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=project"', 3000);
                                    }
                                }
                            }
                        });
                    }
                });
            }
        } else {
            loadblockUI('Loading ...');
            jQuery.ajax({
                type: "POST",
                url: baseurl + '/traveller/add-project-service',
                data: $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#form_addtrippeople_project').serialize(),
                success: function(msg) {
                    var msgdata = jQuery.parseJSON(msg);
                    if (msgdata.result == 'exist') {
                        $.unblockUI();
                        confirmDialog(msgdata.msg, 'Warning', function() {
                            window.location = baseurl + msgdata.serviceUrl;
                        });
                    } else {
                        if ($('[name="ticketOption"]:checked').val() == 'uploadTicket') {
                            if (msgdata.result == 'success') {
                                travelFormData = new FormData();
                                travelFormData.append('file', $('#ticket')[0].files[0]);
                                travelFormData.append('tripId', msgdata.trip_ID);
                                travelFormData.append('tripIdNumber', $('#tripIdNumber').val());
                                $.ajax({
                                    type: 'POST',
                                    url: baseurl + '/traveller/uploadticket',
                                    data: travelFormData,
                                    cache: false,
                                    contentType: false,
                                    processData: false,
                                    success: function(mydata) {
                                        var resDataObj = jQuery.parseJSON(mydata);
                                        if (resDataObj.result == 'success') {
                                            loadblockUI(msgdata.msg);
                                            if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                            else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-project-service/' + msgdata.trip_ID + '/return"', 3000);
                                            else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=project"', 3000);
                                        }
                                    }
                                });
                            }
                        } else {
                            loadblockUI(msgdata.msg);
                            if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                            else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-project-service/' + msgdata.trip_ID + '/return"', 3000);
                            else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=project"', 3000);
                        }
                    }
                }
            });
        }
    });
}

function initAddTripProjectServiceonLocations() {
    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav',
        'debug': false,
        onShow: function(tab, navigation, index) { /*console.log('onShow');*/ },
        onNext: function(tab, navigation, index) {
            if (index == 1) { /* if(validateTravellerTripDetailsForm() == true){ $('#rootwizard a[href="#tab1"]').trigger('click');return false;} */ }
            if (index == 2) { /* if(validatePackageContactForm() == false){ $('#rootwizard a[href="#tab2"]').trigger('click');return false;} */ }
        },
        onPrevious: function(tab, navigation, index) { /*console.log('onPrevious');*/ },
        onLast: function(tab, navigation, index) { /*console.log('onLast');*/ },
        onTabClick: function(tab, navigation, index) { /*console.log('onTabClick');*/ },
        onTabShow: function(tab, navigation, index) { /*console.log('onTabShow');*/
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({
                width: $percent + '%'
            }); /* If it's the last tab then hide the last button and show the finish instead*/
            if ($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }
    });
    $('#rootwizard .finish').click(function() { /*alert('Finished!, Starting over!');*/ /* if(validateTravellerTripDetailsForm() == true){ $('#rootwizard a[href="#tab1"]').trigger('click');return false;} */ /* if(validatePackageContactForm() == false){ $('#rootwizard a[href="#tab2"]').trigger('click');return false;} */
        var address_type = $('[name="contact_address_type"]:checked').val(); /*$('#rootwizard').find("a[href*='tab1']").trigger('click'); //$( "#formAddTripPeople" ).submit();*/
        $('#form_origin_location_address').ajaxForm({
            beforeSubmit: function(e) {},
            success: function(e) {
                var resObj = $.parseJSON(e);
            }
        }).submit();
    });
}

function getLatLangValuesOfAddress() {
    if ($("#street_address_1").val() != '') {
        var street_address_1 = $("#street_address_1").val();
        var city = $("#city").val();
        var country = $("#country_short option:selected").text();
        var state = $("#state").val();
        var zip_code = $("#postal_code").val();
        var param = street_address_1 + " " + city + " " + state + " " + zip_code + "," + country;
        var insDetails = jQuery.ajax({
            type: "POST",
            url: "https://maps.google.com/maps/api/geocode/json?address=" + param + "&sensor=false&region=India",
            data: param,
            async: false,
        }).responseText;
        return insDetails;
    }
    return '';
}

function addRemovePassengers(count) {
    var str_box = '';
    var selected_passengers = Number($('#selected_passengers').val());
    if ($('#one_of_the_passenger').is(':checked') == false) {
        if (selected_passengers < count) {
            count = parseInt(count) - 1;
        }
    } else if ($('#one_of_the_passenger').is(':checked') == true) { /* if(selected_passengers < count){           count =  parseInt(count)+1;        }*/ }
    if (selected_passengers > count) {
        for (i = Number(count) + 1; i <= selected_passengers; i++) {
            $('#passenger_box_' + i).remove();
        }
    } else {
        if (count > 1) {
            for (i = Number(selected_passengers) + 1; i <= count; i++) {
                str_box += '<div class="" id="passenger_box_' + i + '">';
                str_box += '    <div class="panel panel-default">';
                str_box += '        <div class="panel-body">';
                str_box += '            <h5 class="sub-heading text-left">Details of passenger ' + i + '</h5><hr class="title-border">';
                str_box += '            <div class="row">';
                str_box += '            <div class="form-group col-md-6">';
                str_box += '                <label class="control-label">First Name <span class="text-danger">*</span></label>';
                str_box += '                    <input type="text" name="passenger_first_name_' + i + '" id="passenger_first_name_' + i + '" class="form-control passenger_first_name">';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_passenger_first_name_' + i + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '            <div class="form-group col-md-6">';
                str_box += '                <label class="control-label">Last Name <span class="text-danger">*</span></label>';
                str_box += '                    <input type="text" name="passenger_last_name_' + i + '" id="passenger_last_name_' + i + '" class="form-control passenger_last_name">';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_passenger_last_name_' + i + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '            </div>';
                str_box += '            <div class="row">';
                str_box += '            <div class="form-group col-md-6">';
                str_box += '                <label class="control-label">Gender <span class="text-danger">*</span></label>';
                str_box += '                <div class="btn-group" data-toggle="buttons">';
                str_box += '                    <label class="btn btn-default">';
                str_box += '                        <input class="form-control passenger_gender" id="passenger_gender_' + i + '_male" type="radio" name="passenger_gender_' + i + '" value="male" /> Male';
                str_box += '                    </label>';
                str_box += '                    <label class="btn btn-default">';
                str_box += '                        <input class="form-control passenger_gender" id="passenger_gender_' + i + '_female" type="radio" name="passenger_gender_' + i + '" value="female" /> Female';
                str_box += '                    </label>';
                str_box += '                    <label class="btn btn-default">';
                str_box += '                        <input class="form-control passenger_gender" id="passenger_gender_' + i + '_other" type="radio" name="passenger_gender_' + i + '" value="other" /> Other';
                str_box += '                    </label>';
                str_box += '                </div>'; /*str_box += '              <select name="passenger_gender_' + i + '" id="passenger_gender_' + i + '" class="form-control passenger_gender">';                str_box += '                    <option value=""> -- select gender -- </option>';                str_box += '                    <option value="male"> male </option>';                str_box += '                    <option value="female"> female </option>';                str_box += '                </select>';*/
                str_box += '                <p style="margin-bottom: 0px;" id="err_passenger_gender_' + i + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '            <div class="form-group col-md-6">';
                str_box += '                <label class="control-label">Age <span class="text-danger">*</span></label>';
                str_box += '                    <input type="text" name="passenger_age_' + i + '" id="passenger_age_' + i + '" class="form-control numonly passenger_age">';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_passenger_age_' + i + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '            </div>';
                str_box += '            <div class="row">';
                str_box += '            <div class="form-group col-md-12">';
                str_box += '                <ul style="width:100%;clear:both;" id="passenger_attachment_' + i + '" class="imgblock-new"></ul>';
                str_box += '                <input type="hidden" id="uploadCount_' + i + '" name="uploadCount_' + i + '" value="0">';
                str_box += '            </div>';
                str_box += '            </div>';
                str_box += '        </div>';
                str_box += '    </div>';
                str_box += '</div>';
            }
        }
        $('#passenger_box').append(str_box);
        if (count > 1) {
            for (i = Number(selected_passengers) + 1; i <= count; i++) {
                createImageUploader("passenger_attachment_" + i, 0, 5, "", i);
                var htmlContent = $('#hiddenImages_' + i).html();
                $('#dynamicImages_' + i).html(htmlContent);
                $('#hiddenImages_' + i).remove();
            }
        }
    }
    $('#selected_passengers').val(count);
    bindFileStyle();
}

function addRemovePackages(count) {
    var str_box = '';
    var $options = $("#category_1").html();
    var $item_worth_currency_options = $("#item_worth_currency_1").html();
    var selected_packages = $('#selected_packages').val();
    if (selected_packages > count) {
        for (i = Number(count) + 1; i <= selected_packages; i++) {
            $('#packages_box_' + i).remove();
        }
    } else {
        if (count > 1) {
            for (i = Number(selected_packages) + 1; i <= count; i++) {
                str_box += '<div class="col-xs-12" id="packages_box_' + i + '">';
                str_box += '<div class="panel panel-default">';
                str_box += '<div class="panel-body">';
                str_box += '<h5 class="sub-heading text-left">Details of Item ' + i + '</h5>';
                str_box += '<hr class="title-border">';
                str_box += '<div class="row">';
                str_box += ' <div class="form-group col-sm-6">';
                str_box += '<label class="control-label">Item category <span class="text-danger">*</span><a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please select category of item which needs to be transported."></a></label>';
                str_box += '<select class="form-control category"  id="category_' + i + '" name="category_' + i + '">';
                str_box += $options;
                str_box += ' </select>';
                str_box += ' <p style="margin-bottom: 0px;" id="err_category_' + i + '" class="text-danger err_msg"></p>';
                str_box += '  </div>';
                str_box += '   <div class="form-group col-sm-6 ">';
                str_box += ' <label class="control-label">Item Type<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please select exact item that needs to be transported."></a></label>';
                str_box += ' <select class="form-control item_category" id="item_category_' + i + '" name="item_category_' + i + '">';
                str_box += '   <option value="">Select</option>';
                str_box += ' </select>';
                str_box += '  <p style="margin-bottom: 0px;" id="err_item_category_' + i + '" class="text-danger err_msg"></p>';
                str_box += '  </div>';
                str_box += ' </div>';
                str_box += '  <div class="row">';
                str_box += '   <div class="form-group col-sm-12">';
                str_box += '    <label class="control-label">Item Description<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please provide detailed description of the item such as quantity and add the total weight and size etc. so that incase of insurance claim these details will be analyzed."></a></label>';
                str_box += ' <textarea class="form-control vresize item_description" id="item_description_' + i + '" name="item_description_' + i + '" placeholder="Ex. Titan Watch with covered box." rows="3"></textarea>';
                str_box += '  <p style="margin-bottom: 0px;" id="err_item_description_' + i + '" class="text-danger err_msg"></p>';
                str_box += ' </div>';
                str_box += ' </div>';
                str_box += '        <div class="row">';
                str_box += '        <div class="form-group col-xs-8">';
                str_box += '            <label class="control-label" for="approxweight">Approximate weight<a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '            <div class="row">';
                str_box += '            <div class="col-xs-12">';
                str_box += '                  <input type="number item_weight" id="item_weight_' + i + '" name="item_weight_' + i + '" class="form-control item_weight" placeholder="Weight" style="display:none;" />';
                str_box += '                  <p style="margin-bottom: 0px;position: absolute;display: block;" id="lbs_item_weight_' + i + '">1 lbs.</p>  <p style="margin-bottom: 0px; display: block;" id="err_item_weight_' + i + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '           </div>';
                str_box += '        </div>';
                str_box += '        <div class="form-group col-sm-4">';
                str_box += '            <label class="control-label" for="itemsize">Negligible Weight<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="If weight of the item is less than 1 lbs. or 0.5 kg, Please check this option."></a></label>';
                str_box += '            <div class="yesNo" style="margin-top: 1em;">';
                str_box += '                <label class="sliderCheck">';
                str_box += '                    <input type="checkbox" class="negligible_weight" value="1" name="negligible_weight_' + i + '" id="negligible_weight_' + i + '" >';
                str_box += '                    <div class="slider round"></div>';
                str_box += '                </label>';
                str_box += '            </div>';
                str_box += '        </div>';
                str_box += '        </div>';
                str_box += '    <div class = "available-volume">';
                str_box += '    <div class = "volume-box volume-box-' + i + ' active" id="icon-doc_' + i + '">';
                str_box += '    <div class = "volume-box-inner">';
                str_box += '    <span> <img class = "svg" src = "' + baseurl + '/public/img/svg/icon-doc.svg" alt = "Trepr" title = "Trepr" /> </span>';
                str_box += '    <p> Document </p>';
                str_box += '    </div>';
                str_box += '    </div>';
                str_box += '    <div class = "volume-box volume-box-' + i + '" id="icon-backbag_' + i + '" >';
                str_box += '    <div class = "volume-box-inner" >';
                str_box += '    <span> <img class = "svg" src = "' + baseurl + '/public/img/svg/icon-backbag-hand.svg" alt = "Trepr" title = "Trepr" /> </span>';
                str_box += '    <p> Backbag </p>';
                str_box += '    </div>';
                str_box += '    </div>';
                str_box += '    <div class = "volume-box volume-box-' + i + '" id="icon-suitcase_' + i + '">';
                str_box += '    <div class = "volume-box-inner" >';
                str_box += '    <span> <img class = "svg" src = "' + baseurl + '/public/img/svg/icon-suitcase.svg" alt = "Trepr" title = "Trepr" /> </span>';
                str_box += '    <p> Suitcase </p>';
                str_box += '    </div>';
                str_box += '    </div>';
                str_box += '    <div class = "volume-box volume-box-' + i + '" id="icon-package_' + i + '">';
                str_box += '    <div class = "volume-box-inner" >';
                str_box += '    <span> <img class = "svg" src = "' + baseurl + '/public/img/svg/icon-package-normal.svg" alt = "Trepr" title = "Trepr" /> </span>';
                str_box += '    <p> Big parcel case </p>';
                str_box += '    </div>';
                str_box += '    </div>';
                str_box += '   </div>';
                str_box += '   <div class="row">';
                str_box += '       <div class="col-sm-6">';
                str_box += '           <label class="control-label" for="itemsize">Approx. size of item<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please provide size of size so that traveller will decide dimension of the baggage in which item can be carried."></a></label>';
                str_box += '           <div class="row">';
                str_box += '               <div class="col-sm-4 col-xs-3 form-group">';
                str_box += '                   <input type="number" class="form-control item_sizes" placeholder="L" id="item_length_' + i + '" name="item_length_' + i + '" />';
                str_box += '               </div>';
                str_box += '               <div class="col-xs-4 form-group">';
                str_box += '                   <input type="number" class="form-control item_sizes" placeholder="W" id="item_width_' + i + '" name="item_width_' + i + '" />';
                str_box += '               </div>';
                str_box += '               <div class="col-xs-4 form-group">';
                str_box += '                   <input type="number" class="form-control item_sizes" placeholder="H" id="item_height_' + i + '" name="item_height_' + i + '" />';
                str_box += '               </div>';
                str_box += '           </div>';
                str_box += '           <p style="margin-bottom: 0px;" id="err_item_sizes_' + i + '" class="text-danger err_msg"></p>';
                str_box += '       </div>';
                str_box += '       <div class="form-group col-sm-3">';
                str_box += '           <label class="control-label" for="itemsize">Unit of size<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please provide size of size so that traveller will decide dimension of the baggage in which item can be carried."></a></label>';
                str_box += '           <select class="form-control item_unit" name="item_unit_' + i + '"  id="item_unit_' + i + '" style="width:75%; margin-right: 10px;display:inline;">';
                str_box += '               <option value="inch">Inch</option>';
                str_box += '               <option value="mm">mm</option>';
                str_box += '               <option value="cm">cm</option>';
                str_box += '           </select>';
                str_box += '           <span style="line-height: 38px;float: right;">Or</span>';
                str_box += '       </div>';
                str_box += '       <div class="form-group col-sm-3">';
                str_box += '           <label class="control-label" for="itemsize">Negligible size<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="If items such as paper, documents, miniature objects or size that is around a small wall poster (12&quot; x 24&quot;). Please check this option."></a></label>';
                str_box += '           <div class="yesNo">';
                str_box += '               <label class="sliderCheck">';
                str_box += '                   <input type="checkbox" class="negligible_size" value="1" name="negligible_size_' + i + '" id="negligible_size_' + i + '" >';
                str_box += '                   <div class="slider round"></div>';
                str_box += '               </label>';
                str_box += '           </div>';
                str_box += '       </div>';
                str_box += '    </div>';
                str_box += '       <div class="row">';
                str_box += '            <div class="form-group col-sm-6">';
                str_box += '                <label class="control-label">Color</label>';
                str_box += '                    <input type="text" class="form-control color jscolor" aria-label="Color" id="color_' + i + '" name="color_' + i + '" >';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_color_' + i + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '            <div class="form-group col-sm-6">';
                str_box += '                <label class="control-label">Condition<a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                    <select class="form-control condition" id="condition_' + i + '" name="condition_' + i + '">';
                str_box += '                        <option value=""> - select - </option>';
                str_box += '                        <option value="New">New</option>';
                str_box += '                        <option value="Old">Old</option>';
                str_box += '                        <option value="Moderate">Moderate</option>';
                str_box += '                        <option value="Refurbished">Refurbished</option>';
                str_box += '                    </select>';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_condition_' + i + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '        </div>';
                str_box += '        <div class="row">';
                str_box += '            <div class="form-group col-sm-6">';
                str_box += '                <label class="control-label">Quantity<a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                    <input type="text" class="form-control color" aria-label="Color" id="item_quantity_' + i + '" name="item_quantity_' + i + '" >';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_item_quantity_' + i + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '            <div class="form-group col-sm-6">';
                str_box += '                <label class="control-label">Approx. worth of Item<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please select weight of the item so that traveller will decide capacity of baggage in which item can be carried."></a></label>';
                str_box += '                <div class="row">';
                str_box += '                    <div class="col-xs-7">';
                str_box += '                        <input type="number" name="item_worth_' + i + '" id="item_worth_' + i + '"  class="form-control item_worth" aria-label="Amount (to the nearest dollar)">';
                str_box += '                        <p style="margin-bottom: 0px;" id="err_item_worth_' + i + '" class="text-danger err_msg"></p> ';
                str_box += '                    </div>  ';
                str_box += '                    <div class="col-xs-5">';
                str_box += '                        <select class="form-control item_worth_currency" name="item_worth_currency_' + i + '" id="item_worth_currency_' + i + '" >';
                str_box += $item_worth_currency_options;
                str_box += '                        </select>';
                str_box += '                        <p style="margin-bottom: 0px;" id="err_item_worth_currency_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                    </div>';
                str_box += '                </div>';
                str_box += '            </div>';
                str_box += '        </div>';
                str_box += '        <div class="row">';
                str_box += '            <div class="form-group col-sm-6">';
                str_box += '                <label class="control-label">Add picture of item<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please upload picture of item only for identification purpose. NOTE: Photos will be analyzed incase a insurance claim is made."></a></label>';
                str_box += '                <input type="file" class="form-control package_photo" aria-label="" id="package_photo_' + i + '" name="package_photo_' + i + '" accept="image/*">';
                str_box += '                <p style="margin-bottom: 0px;" id="err_package_photo_' + i + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '        </div>';
                str_box += '    </div>';
                str_box += '</div>';
                str_box += '</div>';
            }
        }
        $('#packages_box').append(str_box);
        if (count > 1) {
            for (i = Number(selected_packages) + 1; i <= count; i++) {
                loadPackageSlider(i);
            }
        }
    }
    $('.item_worth').on('change', function() {
        calculateTotalWorth();
    });
    $('#selected_packages').val(count);
    bindColourPicker();
}

function addRemoveProjectsObtainCategory(count, prefixTxt) {
    console.log(count);
    prefixTxt = prefixTxt || '';
    var str_box = '',
        newIndexes = [];
    var $options = $("#product_category_1").html();
    var selected_projects = $('#selected_project_products').val();
    if (selected_projects > count) {
        for (i = Number(count) + 1; i <= selected_projects; i++) {
            $('#projects_box_' + i).remove();
        }
    } else {
        if (count > 1) {
            for (i = Number(selected_projects) + 1; i <= count; i++) {
                str_box += '<div class="col-xs-12" id="projects_box_' + i + '">';
                str_box += '        <div class="panel panel-default">';
                str_box += '                <div class="panel-body">';
                str_box += '        <h5 class="sub-heading text-left">Details of Product ' + i + '</h5><hr class="title-border">';
                str_box += '         <div class="row">';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Product category <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                                <select class="form-control product_category"  id="product_category_' + i + '" name="product_category_' + i + '">';
                str_box += $options;
                str_box += '                                </select>';
                str_box += '                                <p style="margin-bottom: 0px;" id="err_product_category_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                        <div class="form-group col-sm-6 tasklists" id="tasklists_' + i + '">';
                str_box += '                            <label class="control-label">Product type <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                                <select class="form-control product_additional_requirements_category" id="product_additional_requirements_category_' + i + '" name="product_additional_requirements_category_' + i + '">';
                str_box += '                                     <option value="">Select</option>';
                str_box += '                                </select>';
                str_box += '                                <p style="margin-bottom: 0px;" id="err_product_additional_requirements_category_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                                <label class="control-label">Product description <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                                <textarea class="form-control vresize product_description" id="product_description_' + i + '" name="product_description_' + i + '" placeholder="Ex. I need train tickets booked from Mumbai to Chennai on 5th January 2016 in 2nd AC class." rows="3"></textarea>';
                str_box += '                                <p style="margin-bottom: 0px;" id="err_product_description_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Product SKU <a class="icon-info" href="#" data-toggle="tooltip" title="The product reference is often referred to as Catalogue number or SKU number, and it is important because merchants often sell several similar products. You will find this number under each item of the retailers website."></a></label>';
                str_box += '                            <input type="text" class="form-control product_product_sku" aria-label="Product SKU" id="product_product_sku_' + i + '" name="product_product_sku_' + i + '" value="" >';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_product_product_sku_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Website link<a class="icon-info" href="#" data-toggle="tooltip" title="Please provide URL is the link to the specific item that you intend to purchase. Ensure that you copy the correct link with all the required details."></a></label>';
                str_box += '                            <input type="text" class="form-control product_web_site_link" aria-label="http://example.com" id="product_web_site_link_' + i + '" name="product_web_site_link_' + i + '" >';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_product_web_site_link_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="form-group col-sm-8">';
                str_box += '                            <label class="control-label" for="approxweight">Approximate weight<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please select weight of the item so that traveller will decide capacity of baggage in which item can be carried."></a></label>';
                str_box += '                            <div class="row">';
                str_box += '                                <div class="col-sm-12">';
                str_box += '                                    <input type="text" id="' + prefixTxt + 'item_weight_' + i + '" name="item_weight_' + i + '" class="item_weight" style="display:none;" >';
                str_box += '                                    <p style="margin-bottom: 0px;position: absolute;display: block;" id="' + prefixTxt + 'lbs_item_weight_' + i + '"></p> ';
                str_box += '                                    <p style="margin-bottom: 0px;" id="err_' + prefixTxt + 'item_weight_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                                </div>';
                str_box += '                            </div>';
                str_box += '                        </div>';
                str_box += '                        <div class="form-group col-sm-4">';
                str_box += '                            <label class="control-label" for="itemsize">Negligible Weight<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="If weight of the item is less than 1 lbs. or 0.5 kg, Please check this option."></a></label>';
                str_box += '                            <div class="yesNo" style="margin-top: 1em;">';
                str_box += '                                <label class="sliderCheck">';
                str_box += '                                    <input type="checkbox" class="negligible_weight" value="1" name="negligible_weight_' + i + '" id="' + prefixTxt + 'negligible_weight_' + i + '">';
                str_box += '                                    <div class="slider round"></div>';
                str_box += '                                </label>';
                str_box += '                            </div>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="form-group col-sm-12">';
                str_box += '                            <div class="available-volume">';
                str_box += '                                <div class="volume-box volume-box-' + i + ' active" id="icon-doc_' + i + '">';
                str_box += '                                    <div class="volume-box-inner">';
                str_box += '                                        <span><img class="svg" src="' + baseurl + '/public/img/svg/icon-doc.svg" alt="Trepr" title="Trepr" /></span>';
                str_box += '                                        <p>Document</p>';
                str_box += '                                    </div>';
                str_box += '                                </div>';
                str_box += '                                <div class="volume-box volume-box-' + i + '" id="icon-backbag_' + i + '">';
                str_box += '                                    <div class="volume-box-inner">';
                str_box += '                                        <span><img class="svg" src="' + baseurl + '/public/img/svg/icon-backbag-hand.svg" alt="Trepr" title="Trepr" /></span>';
                str_box += '                                        <p>Backbag</p>';
                str_box += '                                    </div>';
                str_box += '                                </div>';
                str_box += '                                <div class="volume-box volume-box-' + i + ' " id="icon-suitcase_' + i + '">';
                str_box += '                                    <div class="volume-box-inner">';
                str_box += '                                        <span><img class="svg" src="' + baseurl + '/public/img/svg/icon-suitcase.svg" alt="Trepr" title="Trepr" /></span>';
                str_box += '                                        <p>Suitcase</p>';
                str_box += '                                    </div>';
                str_box += '                                </div>';
                str_box += '                                <div class="volume-box volume-box-' + i + '" id="icon-package_' + i + '">';
                str_box += '                                    <div class="volume-box-inner">';
                str_box += '                                        <span><img class="svg" src="' + baseurl + '/public/img/svg/icon-package-normal.svg" alt="Trepr" title="Trepr" /></span>';
                str_box += '                                        <p>Big parcel case</p>';
                str_box += '                                    </div>';
                str_box += '                                </div>';
                str_box += '                            </div>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="col-sm-6">';
                str_box += '                            <label class="control-label" for="itemsize">Approx. size of item<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please provide size of size so that traveller will decide dimension of the baggage in which item can be carried."></a></label>';
                str_box += '                            <div class="row">';
                str_box += '                                <div class="col-sm-3 col-xs-4 form-group">';
                str_box += '                                    <input type="number" class="form-control item_sizesL" placeholder="L" id="' + prefixTxt + 'item_length_' + i + '" name="item_length_' + i + '"  />';
                str_box += '                                </div>';
                str_box += '                                <div class="col-xs-4 form-group">';
                str_box += '                                    <input type="number" class="form-control item_sizesW" placeholder="W" id="' + prefixTxt + 'item_width_' + i + '" name="item_width_' + i + '" />';
                str_box += '                                </div>';
                str_box += '                                <div class="col-xs-4 form-group">';
                str_box += '                                    <input type="number" class="form-control item_sizesH" placeholder="H" id="' + prefixTxt + 'item_height_' + i + '" name="item_height_' + i + '" />';
                str_box += '                                </div>';
                str_box += '                            </div>';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_' + prefixTxt + 'item_sizes_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                        <div class="form-group col-sm-3">';
                str_box += '                            <label class="control-label" for="itemsize">Unit of size<a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please provide size of size so that traveller will decide dimension of the baggage in which item can be carried."></a></label>';
                str_box += '                            <select class="form-control item_unit" name="item_unit_' + i + '"  id="' + prefixTxt + 'item_unit_' + i + '"style="width:75%; margin-right: 10px;display:inline;">';
                str_box += '                                <option value="inch">Inch</option>';
                str_box += '                                <option value="mm">mm</option>';
                str_box += '                                <option value="cm">cm</option>';
                str_box += '                            </select>';
                str_box += '                            <span style="line-height: 38px;float: right;">Or</span>';
                str_box += '                        </div>';
                str_box += '                        <div class="form-group col-sm-3">';
                str_box += '                            <label class="control-label" for="itemsize">Negligible size <a class="icon-info" href="javascript:;" data-toggle="tooltip" title="If items such as paper, documents, miniature objects or size that is around a small wall poster (12&quot; x 24&quot;). Please check this option."></a></label>';
                str_box += '                            <div class="yesNo">';
                str_box += '                                <label class="sliderCheck">';
                str_box += '                                    <input type="checkbox" class="negligible_size" value="1" name="negligible_size_' + i + '" id="' + prefixTxt + 'negligible_size_' + i + '">';
                str_box += '                                    <div class="slider round"></div>';
                str_box += '                                </label>';
                str_box += '                            </div>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Color<a class="icon-info" href="#" data-toggle="tooltip" title="Please select a color that best match color of the item to make sure right same item is carried and delivered"></a></label>';
                str_box += '                            <input type="text" class="form-control color jscolor" aria-label="Color" id="product_color_' + i + '" name="product_color_' + i + '" >';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_product_color_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Condition<a class="icon-info" href="#" data-toggle="tooltip" title="Please select exact condition of the item to make sure right item is carried and delivered in same condition"></a></label>';
                str_box += '                            <select class="form-control condition" id="product_condition_' + i + '" name="product_condition_' + i + '">';
                str_box += '                                <option value="">Select</option>';
                str_box += '                                <option value="New">New</option>';
                str_box += '                                <option value="Old">Old</option>';
                str_box += '                                <option value="Moderate">Moderate</option>';
                str_box += '                                <option value="Refurbished">Refurbished</option>';
                str_box += '                            </select>';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_product_condition_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Quantity <a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                            <input type="number" class="form-control item_quantity" aria-label="Quantity" id="item_quantity_' + i + '" name="item_quantity_' + i + '" value="" />';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_item_quantity_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="col-sm-6">';
                str_box += '                            <label class="control-label">Price <a class="icon-info" href="#" data-toggle="tooltip" title="Please enter exact price of the product as on merchant\'s website including all taxes applicable."></a></label>';
                str_box += '                            <div class="row">';
                str_box += '                                <div class="col-sm-6 form-group">';
                str_box += '                                    <input type="number" class="form-control product_price" aria-label="Cost (in dollar)" id="product_price_' + i + '" name="product_price_' + i + '" >';
                str_box += '                                    <p style="margin-bottom: 0px;" id="err_product_price_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                                </div>';
                str_box += '                                <div class="col-sm-6 form-group">';
                str_box += '                                    <select class="form-control product_price_currency"  id="product_price_currency_' + i + '" name="product_price_currency_' + i + '">';
                str_box += '                                        <option value="USD $">USD $</option>';
                str_box += '                                        <option value="EUR &euro;">EUR &euro;</option>';
                str_box += '                                        <option value="GBP &pound;">GBP &pound;</option>';
                str_box += '                                    </select>';
                str_box += '                                    <p style="margin-bottom: 0px;" id="err_product_price_currency_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                                </div>';
                str_box += '                            </div>';
                str_box += '                        </div>';
                str_box += '                         <div class="form-group col-sm-6">';
                str_box += '                            <ul style="width:100%;clear:both;" id="product_attachment_' + i + '" class="imgblock-new product_attachment"></ul>'; /*str_box += '                            <input type="file" class="form-control task_photo" aria-label="" id="product_photo_' + i + '" name="product_photo_' + i + '[]" accept="image/*">';*/
                str_box += '                            <p style="margin-bottom: 0px;" id="err_product_photo_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                </div>';
                str_box += '                </div>';
                str_box += '        </div>';
                str_box += '</div>';
                newIndexes.push(i);
            }
        }
        $('#projects_box').append(str_box);
        $('.product_price').on('change', function() {
            calculateTotalPrice();
        });
        if (count > 1) {
            for (i = Number(selected_projects) + 1; i <= count; i++) {
                loadPackageSlider(i, prefixTxt);
                createImageUploader("product_attachment_" + i, 0, 5, "", i);
                var htmlContent = $('#hiddenImages_' + i).html();
                $('#dynamicImages_' + i).html(htmlContent);
                $('#hiddenImages_' + i).remove();
            }
        }
    }
    $('#selected_project_products').val(count);
    bindFileStyle();
    bindColourPicker();
}

function addRemoveProjects(count) {
    var str_box = '',
        newIndexes = [];
    var $options = $("#task_category_1").html();
    var selected_projects = $('#selected_project_tasks').val();
    if (selected_projects > count) {
        for (i = Number(count) + 1; i <= selected_projects; i++) {
            $('#projects_box_' + i).remove();
        }
    } else {
        if (count > 1) {
            for (i = Number(selected_projects) + 1; i <= count; i++) {
                str_box += '        <div class="col-xs-12" id="projects_box_' + i + '">';
                str_box += '            <div class="panel panel-default">';
                str_box += '                <div class="panel-body">';
                str_box += '                    <h5 class="sub-heading text-left">Details of Task ' + i + '</h5><hr class="title-border">';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Category <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                                <select class="form-control task_category" id="task_category_' + i + '" name="task_category_' + i + '">';
                str_box += $options;
                str_box += '                                </select>';
                str_box += '                                <p style="margin-bottom: 0px;" id="err_task_category_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                        <div class="form-group col-sm-6 tasklists" id="tasklists_' + i + '">';
                str_box += '                            <label class="control-label">Task Lists <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                                <select class="form-control additional_requirements_category" id="additional_requirements_category_' + i + '" name="additional_requirements_category_' + i + '">';
                str_box += '                                     <option value="">Select</option>';
                str_box += '                                </select>';
                str_box += '                                <p style="margin-bottom: 0px;" id="err_additional_requirements_category_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="form-group col-sm-12">';
                str_box += '                                <label class="control-label">Task Description <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                                <textarea class="form-control vresize task_description" id="task_description_' + i + '" name="task_description_' + i + '" placeholder="Ex. I need train tickets booked from Mumbai to Chennai on 5th January 2016 in 2nd AC class." rows="3"></textarea>';
                str_box += '                                <p style="margin-bottom: 0px;" id="err_task_description_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="col-sm-6 form-group">';
                str_box += '                            <label class="control-label">Date of service <span class="text-danger">*</span><a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please provide date of service required."></a></label>';
                str_box += '                            <div class="input-group">';
                str_box += '                                <input class="date-pick form-control service_date" name="service_date_' + i + '" id="service_date_' + i + '" type="text" /><span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>';
                str_box += '                            </div>';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_service_date_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                        <div class="col-sm-6 form-group">';
                str_box += '                            <label class="control-label">Date Flexibility <span class="text-danger">*</span><a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please select a flexible + or - date range from departure date provided."></a></label>';
                str_box += '                            <div class="input-group">';
                str_box += '                                <select class="selectpicker form-control languages-select picker-select date_flexibility" id="date_flexibility_' + i + '" name="date_flexibility_' + i + '" onchange="updateDateRange(\'service_date_' + i + '\',\'service_start_date_' + i + '\',\'service_end_date_' + i + '\', \'date_flexibility_' + i + '\' );">';
                str_box += '                                    <option selected="selected" value="1-d">1 Day</option>';
                str_box += '                                    <option value="2-d">2 Days</option>';
                str_box += '                                    <option value="3-d">3 Days</option>';
                str_box += '                                    <option value="1-w">1 Week</option>';
                str_box += '                                    <option value="2-w">2 Weeks</option>';
                str_box += '                                    <option value="3-w">3 Weeks</option>';
                str_box += '                                    <option value="1-m">1 Month</option>';
                str_box += '                                    <option value="2-m">2 Months</option>';
                str_box += '                                    <option value="3-m">3 Months</option>';
                str_box += '                                </select>';
                str_box += '                                <span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>';
                str_box += '                            </div>';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_date_flexibility_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Dates range</label>';
                str_box += '                            <div class="input-daterange" id="datepicker">';
                str_box += '                                <div class="input-group">';
                str_box += '                                    <input type="text" name="service_start_date_' + i + '" id="service_start_date_' + i + '" class="input-small form-control"  readonly="readonly"/>';
                str_box += '                                    <span class="input-group-addon">to</span>';
                str_box += '                                    <input type="text" name="service_end_date_' + i + '" id="service_end_date_' + i + '" class="input-small form-control" readonly="readonly"/>';
                str_box += '                                    <p style="margin-bottom: 0px;" id="err_service_end_date_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                                </div>';
                str_box += '                            </div>';
                str_box += '                        </div>';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Task duration (days) <span class="text-danger">*</span><a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please mention duration if task requires more than a day effort."></a></label>';
                str_box += '                            <input type="number" class="form-control numonly task_duration" aria-label="" id="task_duration_' + i + '" name="task_duration_' + i + '" >';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_task_duration_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Requires local travel <a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please check this option if local travel is required to complete the task."></a></label>';
                str_box += '                            <div class="input-group">';
                str_box += '                            <span class="input-group-addon"><input type="checkbox" value="1" aria-label="Requires local travel" id="requires_local_travel_' + i + '"  name="requires_local_travel_' + i + '" /></span>';
                str_box += '                            <input type="text" class="form-control"  placeholder="Requires local travel" aria-label="Requires local travel" readonly />';
                str_box += '                            </div>';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_requires_local_travel_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                        <div class="form-group col-sm-6">';
                str_box += '                            <label class="control-label">Type you wish to pay for the task <span class="text-danger">*</span><a class="icon-info" href="javascript:;" data-toggle="tooltip" title="Please select cost type you wish to pay for the task."></a></label>';
                str_box += '                            <label for="pay_method_fixed_' + i + '"><input type="radio" checked="checked" class="pay_method" aria-label="" id="pay_method_fixed_' + i + '" name="payout_type_' + i + '" value="Fixed cost"> Fixed cost</label>';
                str_box += '                            <label for="pay_method_hourly_' + i + '"><input type="radio" class="pay_method" aria-label="" id="pay_method_hourly_' + i + '" name="payout_type_' + i + '" value="Hourly cost"> Hourly cost</label>';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_pay_method_hourly_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="col-sm-6">';
                str_box += '                            <label class="control-label">Cost willing to pay <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title="Please mention cost you would like to pay for task completion."></a></label>';
                str_box += '                            <div class="row">';
                str_box += '                                <div class="col-sm-6 form-group">';
                str_box += '                                    <input type="number" class="form-control cost_willing_to_pay" aria-label="Cost" id="cost_willing_to_pay_' + i + '" name="cost_willing_to_pay_' + i + '" >';
                str_box += '                                    <p style="margin-bottom: 0px;" id="err_cost_willing_to_pay_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                                </div>';
                str_box += '                               <div class="col-sm-6 form-group">';
                str_box += '                                   <select class="form-control currency_of_payment"  id="currency_of_payment_' + i + '" name="currency_of_payment_' + i + '">';
                str_box += '                                       <option value="USD $">USD $</option>';
                str_box += '                                       <option value="EUR &euro;">EUR &euro;</option>';
                str_box += '                                       <option value="GBP &pound;">GBP &pound;</option>';
                str_box += '                                    </select>';
                str_box += '                                    <p style="margin-bottom: 0px;" id="err_currency_of_payment_' + i + '" class="text-danger err_msg"></p>';
                str_box += '                                </div>';
                str_box += '                            </div>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                </div>';
                str_box += '            </div>';
                str_box += '        </div>';
                newIndexes.push(i);
            }
        }
        $('#projects_box_other_category').append(str_box);
        bindDatePicker('date-pick');
    }
    $('#selected_project_tasks').val(count);
    bindFileStyle();
    $('.picker-select').selectpicker();
    $('.service_date').each(function() {
        var service_date_id = $(this).attr('id'),
            index = service_date_id.lastIndexOf("_"),
            pos = service_date_id.substr(index + 1);
        if ($(this).val() !== '') {
            updateDateRange(service_date_id, 'service_start_date_' + pos, 'service_end_date_' + pos, 'date_flexibility_' + pos);
        }
    });
}

function getPassengersFee() {
    $('#people-info').removeClass('in');
}

function SendReportMail(tripid) {
    var report = '1';
    if ($("input[name=reportlisting]:checked").val() == 'trust-04') {
        var report = $("#reportOther").val();
    } else var report = $("input[name=reportlisting]:checked").val();
    if (typeof(report) != "undefined" && report !== null && report !== "") {
        $('.mfp-close').click();
        loadblockUI("Please wait....");
        jQuery.ajax({
            type: "POST",
            url: baseurl + '/traveller/send-report-mail',
            data: 'tripid=' + tripid + '&report=' + report,
            success: function(msg) {
                if (msg) {
                    loadblockUI(msg);
                    setTimeout('$.unblockUI();', 1000);
                } else $('#MailStatus').html("Cannot send your report !");
            }
        });
    } else {
        $('#err_reportlisting').html('Please select the report');
    }
}

function SendReportMailSeeker(tripid) {
    var report = "null";
    if ($("input[name=reportlisting]:checked").val() == 'trust-04') {
        report = $("#reportOther").val();
    } else {
        report = $("input[name=reportlisting]:checked").val();
    }
    if (typeof(report) != "undefined" && report !== null && report !== "") {
        $('.mfp-close').click();
        loadblockUI("Please wait....");
        jQuery.ajax({
            type: "POST",
            url: baseurl + '/seeker/send-report-mail',
            data: 'tripid=' + tripid + '&report=' + report,
            success: function(msg) {
                if (msg) {
                    loadblockUI(msg);
                    setTimeout('$.unblockUI();', 1000);
                } else $('#MailStatus').html("Cannot send your report !");
            }
        });
    } else {
        $('#err_reportlisting').html('Please select the report');
    }
}

function SendMessageMailSeeker(tripid) {
    var message = $("#membermessage").val();
    var subject = $("#membersubject").val();
    if ($("#membersubject").val() && $("#membermessage").val()) {
        $('.mfp-close').click();
        loadblockUI("Please wait....");
        jQuery.ajax({
            type: "POST",
            url: baseurl + '/seeker/send-message-mail',
            data: 'message=' + message + '&subject=' + subject + '&tripid=' + tripid,
            success: function(msg) {
                if (msg) {
                    loadblockUI(msg);
                    setTimeout('$.unblockUI();', 1000);
                } else $('#MailStatusmessage').html("Cannot send your report !");
            }
        });
    } else {
        if (!$("#membersubject").val()) $('#err_membersubject').html('Please enter the subject');
        else $('#err_membersubject').html('');
        if (!$("#membermessage").val()) $('#err_membermessage ').html('Please enter the message');
        else $('#err_membermessage ').html('');
    }
}

function SendMessageMailTraveller(tripid) {
    var message = $("#membermessage").val();
    var subject = $("#membersubject").val();
    if ($("#membersubject").val() && $("#membermessage").val()) {
        $('.mfp-close').click();
        loadblockUI("Please wait....");
        jQuery.ajax({
            type: "POST",
            url: baseurl + '/traveller/send-message-mail',
            data: 'message=' + message + '&subject=' + subject + '&tripid=' + tripid,
            success: function(msg) {
                if (msg) {
                    loadblockUI(msg);
                    setTimeout('$.unblockUI();', 1000);
                } else $('#MailStatusmessage').html("Cannot send your report !");
            }
        });
    } else {
        if (!$("#membersubject").val()) $('#err_membersubject').html('Please enter the subject');
        else $('#err_membersubject').html('');
        if (!$("#membermessage").val()) $('#err_membermessage ').html('Please enter the message');
        else $('#err_membermessage ').html('');
    }
}

function getPacakgesFee() {
    $('#package-info').removeClass('in');
    return false;
    if ($('input[name=seekerPackageRequest]:checked').val() == 'existing_trip') $('#seeker_package_request_id').val(localStorage.getItem('seeker_package_request_id'));
    if ($('input[name=travellerPackageRequest]:checked').val() == 'existing_trip') $('#traveller_package_request_id').val(localStorage.getItem('traveller_package_request_id'));
    var p_count = $('#packages_count').val();
    $('#traveller_people_request_id').val(localStorage.getItem('traveller_people_request_id'));
    var p_count = $('#packages_count').val();
    if ($('#packages_count').length != 0) {
        var trip_people_count = $('input[name=packages_count]:checked').val();
        if (trip_people_count > 10) {
            p_count = $('#packages_count_more').val();
        } else {
            p_count = trip_people_count;
        }
    }
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/trips/getpackagesfee',
        data: 'packages_count=' + p_count,
        success: function(msg) {
            $('#span_package_service_fee').html(msg);
            $('#package_service_fee').val(msg);
            $('#span_total_service_fee').html(Number($('#people_service_fee').val()) + Number($('#package_service_fee').val()) + Number($('#project_service_fee').val()));
            $('#package-info').removeClass('in');
        }
    });
}

function getProjectsFee() {
    $('#project-info').removeClass('in');
    return false;
    if ($('input[name=seekerProjectRequest]:checked').val() == 'existing_trip') $('#seeker_project_request_id').val(localStorage.getItem('seeker_project_request_id'));
    if ($('input[name=travellerProjectRequest]:checked').val() == 'existing_trip') $('#traveller_project_request_id').val(localStorage.getItem('traveller_project_request_id'));
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/trips/getprojectsfee',
        data: 'tasks_count=' + $('#projects_count').val(),
        success: function(msg) {
            $('#span_project_service_fee').html(msg);
            $('#project_service_fee').val(msg);
            $('#span_total_service_fee').html(Number($('#people_service_fee').val()) + Number($('#package_service_fee').val()) + Number($('#project_service_fee').val()));
            $('#project-info').removeClass('in');
        }
    });
}

function addSeekerPeopleRequest() {
    if (validateSeekerPeopleForm()) {
        return false;
    }
    if ($('#trip_orgin_location').val() == '') {
        loadblockUI('Please add orgin location address');
        setTimeout('$.unblockUI();', 3000);
        return false;
    } /* getPassengersFee();*/
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/seeker/add-seeker-people-request',
        data: 'trip_orgin_location=' + $('#trip_orgin_location').val() + '&seeker_trip_id=' + $('#seeker_trip_id').val() + '&traveller_trip_id=' + $("#traveller_trip_id").val() + '&' + $('#fromSeekerPeopleRequest').serialize(),
        success: function(msg) {
            var obj = jQuery.parseJSON(msg);
            $('#seeker_people_request_id').val(obj.seeker_people_request_id);
            if (obj.seeker_trip_id) $('#seeker_trip_id').val(obj.seeker_trip_id);
            else {
                loadblockUI(obj.msg);
                setTimeout('$.unblockUI();', 3000);
            }
        }
    });
}

function addSeekerPackageRequest() {
    if (validatePackageForm() === false) {
        return false;
    }
    if ($('#trip_orgin_location').val() == '') {
        loadblockUI('Please add orgin location address');
        setTimeout('$.unblockUI();', 3000);
        return false;
    }
    getPacakgesFee();
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/seeker/add-seeker-package-request',
        data: 'trip_orgin_location=' + $('#trip_orgin_location').val() + '&seeker_trip_id=' + $('#seeker_trip_id').val() + '&traveller_trip_id=' + $("#traveller_trip_id").val() + '&' + $('#fromSeekerPackageRequest').serialize(),
        success: function(msg) {
            var obj = jQuery.parseJSON(msg);
            $('#seeker_package_request_id').val(obj.seeker_package_request_id);
            if (obj.seeker_trip_id) $('#seeker_trip_id').val(obj.seeker_trip_id);
            else {
                loadblockUI(obj.msg);
                setTimeout('$.unblockUI();', 3000);
            }
        }
    });
}

function addSeekerProjectRequest() {
    if (validateSeekerProjectForm()) {
        return false;
    }
    if ($('#trip_orgin_location').val() == '') {
        loadblockUI('Please add orgin location address');
        setTimeout('$.unblockUI();', 3000);
        return false;
    }
    getProjectsFee();
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/seeker/add-seeker-project-request',
        data: 'trip_orgin_location=' + $('#trip_orgin_location').val() + '&seeker_trip_id=' + $('#seeker_trip_id').val() + '&traveller_trip_id=' + $("#traveller_trip_id").val() + '&' + $('#formSeekerProjectRequest').serialize(),
        success: function(msg) {
            var obj = jQuery.parseJSON(msg);
            $('#seeker_project_request_id').val(obj.seeker_project_request_id);
            if (obj.seeker_trip_id) $('#seeker_trip_id').val(obj.seeker_trip_id);
            else {
                loadblockUI(obj.msg);
                setTimeout('$.unblockUI();', 3000);
            }
        }
    });
}

function addSeekerTripRequest() {
    var loggedin_status = checklogin();
    if (loggedin_status == 'yes') {
        if (checkDocumentStatus()) {
            if ($('#seeker_people_request_id').val() == '' && $('#seeker_package_request_id').val() == '' && $('#seeker_project_request_id').val() == '') {
                loadblockUI('Please add or select your request information to send request');
                setTimeout('$.unblockUI();', 3000);
            } else {
                if (validateTravellerCardForm()) {
                    $.magnificPopup.close();
                    var cardtype = $("#cardtype").val();
                    jQuery.ajax({
                        type: "POST",
                        url: baseurl + '/seeker/add-seeker-trip-request',
                        data: 'traveller_trip_id=' + $("#traveller_trip_id").val() + '&seeker_people_request_id=' + $('#seeker_people_request_id').val() + '&seeker_package_request_id=' + $('#seeker_package_request_id').val() + '&seeker_project_request_id=' + $('#seeker_project_request_id').val() + '&seeker_people_service_id=' + $('#seeker_people_service_id').val() + '&seeker_package_service_id=' + $('#seeker_package_service_id').val() + '&seeker_project_service_id=' + $('#seeker_project_service_id').val() + '&cardtype=' + cardtype + '&existing_card=' + $('.existing_card:checked').val() + '&holder_name=' + $('#holder_name').val() + '&holder_last_name=' + $('#holder_last_name ').val() + '&card_no=' + $('#card_no').val() + '&card_valid=' + $('#card_valid').val() + '&card_cvv=' + $('#card_cvv').val() + '&addUserCard=' + $('#addUserCard').val() + '&card_type=' + $('#card_type').val() + '&payment_card_type=' + $('#payment_card_type').val() + '&issue_date=' + $('#issue_date').val() + '&issue_number=' + $('#issue_number').val() + '&card_billing_country=' + $('#card_billing_country').val() + '&card_billing_zipcode=' + $('#card_billing_zipcode').val() + '&traveller_package_amount=' + $('#traveller_package_amount').val(),
                        success: function(msg) {
                            loadblockUI(msg);
                            setTimeout('$.unblockUI();', 3000);
                        }
                    });
                }
            }
        }
    } else {
        loginlightbox();
        after_login_forward = 'refresh';
    }
}

function approvePeopleRequest(requestid, status) {
    loadblockUI('Loading..');
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/traveller/approve-people-request',
        data: 'tripid=' + $("#tripid").val() + '&requestid=' + requestid + '&status=' + status,
        dataType: 'json',
        success: function(result) {
            if (result['msg_status'] == 'success') {
                loadblockUI(result['msg']);
                setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/receipt/' + result['pay_id'] + '?service=People"', 2000);
            } else {
                loadblockUI(result['msg']);
                setTimeout('$.unblockUI();', 3000);
            }
        }
    });
}

function approvePackageRequest() {
    loadblockUI('Loading..');
    if ($('#status').val() == 2) {
        loadblockUI('Loading...');
    }
    $.magnificPopup.close();
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/traveller/approve-package-request',
        data: $('#form_process_payment').serialize(),
        dataType: 'json',
        success: function(result) {
            if (result['msg_status'] == 'success') {
                loadblockUI(result['msg']);
                setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/receipt/' + result['pay_id'] + '?service=Package"', 2000);
            } else {
                loadblockUI(result['msg']);
                setTimeout('$.unblockUI();', 3000);
            }
        }
    });
}

function saveWishlist(tripid, userrole) {
    loadblockUI('Loading..');
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/seeker/save-wishlist',
        data: 'tripid=' + tripid + '&userrole=' + userrole,
        success: function(result) {
            var jsonObj = jQuery.parseJSON(result);
            if (jsonObj.msg_status == 'success') {
                loadblockUI(jsonObj.msg);
                setTimeout('$.unblockUI();', 2000);
            } else {
                $('#errorpMsg').show();
                $('.errorpMsg').html(jsonObj.msg);
            }
        }
    });
}

function removeWishlist(id) {
    if (confirm("Are you sure you want to delete?")) {
        loadblockUI('Loading..');
        jQuery.ajax({
            type: "POST",
            url: baseurl + '/seeker/save-wishlist',
            data: 'id=' + id + '&action=delete',
            success: function(result) {
                var jsonObj = jQuery.parseJSON(result);
                if (jsonObj.msg_status == 'success') {
                    loadblockUI(jsonObj.msg);
                    setTimeout('$.unblockUI();location.reload();', 2000);
                } else {
                    $('#errorpMsg').show();
                    $('.errorpMsg').html(jsonObj.msg);
                }
            }
        });
    }
}

function approveProjectRequest(requestid, status) {
    loadblockUI('Loading..');
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/traveller/approve-project-request',
        data: 'tripid=' + $("#tripid").val() + '&requestid=' + requestid + '&status=' + status,
        success: function(result) {
            var jsonObj = jQuery.parseJSON(result);
            if (jsonObj.msg_status == 'success') {
                loadblockUI(jsonObj.msg);
                setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/receipt/' + jsonObj.pay_id + '?service=Project"', 2000);
            } else {
                loadblockUI(jsonObj.msg);
                setTimeout('$.unblockUI();', 3000);
            }
        }
    });
}

function getflightpopup() {
    $(document).ready(function() {
        $('#getFlightStops #getflightstauspopup').DataTable();
    });
}

function initAddTripSeekerPeople() {
    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav',
        'debug': false,
        onShow: function(tab, navigation, index) { /*console.log('onShow');*/ },
        onNext: function(tab, navigation, index) {
            if (index == 1) {
                if (validateSeekerPeopleForm()) {
                    return false;
                }
                return true;
            } else if (index == 2) {
                if (validateSeekerTravelDetailsForm()) {
                    return false;
                }
                return true;
            }
        },
        onPrevious: function(tab, navigation, index) { /*console.log('onPrevious');*/ },
        onLast: function(tab, navigation, index) { /*console.log('onLast');*/ },
        onTabClick: function(tab, navigation, index, newindex) {
            if (newindex == 1) {
                if (validateSeekerPeopleForm()) {
                    return false;
                }
                return true;
            } else if (newindex == 2) {
                if (validateSeekerTravelDetailsForm()) {
                    return false;
                }
                return true;
            } /*console.log('onTabClick');*/
        },
        onTabShow: function(tab, navigation, index) {
            $('.jtextfill').textfill({
                maxFontPixels: 36
            });
            if (index == 0) {
                console.log(index);
                $('#travel_service_benefits').show();
                $('#estimated_fee').hide();
            } else if (index == 1 || index == 2) {
                $('#travel_service_benefits').hide();
                $('#estimated_fee').show();
                getServiceFee();
            }
            if (index == 2) {
                initMap();
            }
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({
                width: $percent + '%'
            }); /* If it's the last tab then hide the last button and show the finish instead*/
            if ($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }
    });
    $('#rootwizard .finish').click(function() { /*alert('Finished!, Starting over!');*/
        if (!validateOriginLocationForm()) return false;
        var address_type = $('input[name=contact_address_type]:checked', '#form_origin_location_address').val(); /*$('#rootwizard').find("a[href*='tab1']").trigger('click'); */ /*$( "#formAddTripPeople" ).submit();*/
        if (selectedFlightData !== false) {
            $('#selectedFlightData').val(JSON.stringify(selectedFlightData));
        }
        if (address_type == 'new_address') {
            if ($("#street_address_1").val() != '') {
                var street_address_1 = $("#street_address_1").val();
                var city = $("#city").val();
                var country = $("#country_short option:selected").text();
                var state = $("#state").val();
                var zip_code = $("#postal_code").val();
                var param = street_address_1 + " " + city + " " + state + " " + zip_code + "," + country;
                jQuery.ajax({
                    type: "POST",
                    url: "https://maps.google.com/maps/api/geocode/json?address=" + param + "&sensor=false&region=India",
                    data: param,
                    async: false,
                    success: function(msg) {
                        $("#latitude").val(msg.results[0].geometry.location.lat);
                        $("#longitude").val(msg.results[0].geometry.location.lng);
                    }
                });
            }
        } else {
            loadblockUI('LOADING...');
            $("#formSeekerPeopleRequest").submit();
            jQuery.ajax({
                type: "POST",
                url: baseurl + '/seeker/add-people-service',
                data: $('#formSeekerPeopleRequest').serialize(),
                success: function(response) {
                    var resObj = jQuery.parseJSON(response);
                    if (resObj.result == 'success') {
                        travelFormData = new FormData();
                        travelFormData.append('file', $('#ticket')[0].files[0]);
                        travelFormData.append('tripId', resObj.trip_id);
                        travelFormData.append('tripIdNumber', $('#tripIdNumber').val());
                        $.ajax({
                            type: 'POST',
                            url: baseurl + '/seeker/uploadTicket',
                            data: travelFormData,
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function(mydata) {
                                var resDataObj = jQuery.parseJSON(mydata);
                                if (resDataObj.result == 'success') {
                                    loadblockUI(resObj.msg);
                                    if (resObj.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + resObj.requestURL + '"', 3000);
                                    else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + resObj.trip_id + '&service=people"', 3000);
                                }
                            }
                        });
                    } else if (resDataObj.result == 'failure') {
                        loadblockUI(resDataObj.msg);
                        setTimeout('$.unblockUI();window.location="' + baseurl + '/seeker/add-people-service"', 3000);
                    }
                }
            });
        }
    });
    $("#formSeekerPeopleRequest").on("submit", function(event) {
        event.preventDefault();
    });
    $("#formAddTripPeople").on("submit", function(event) { /*alert( $( this ).serialize() )*/
        event.preventDefault(); /*console.log($(this).serialize());*/
    });
}

function initAddTripSeekerPackage() {
    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav',
        'debug': false,
        onShow: function(tab, navigation, index) { /* console.log('onShow'); */ },
        onNext: function(tab, navigation, index) {
            if (index == 1) return validatePackageForm(); /* console.log('onNext'); */
        },
        onPrevious: function(tab, navigation, index) { /* console.log('onPrevious'); */ },
        onLast: function(tab, navigation, index) { /* console.log('onLast'); */ },
        onTabClick: function(tab, navigation, index, newindex) {
            if (newindex == 1 || newindex == 2) return validatePackageForm(); /* console.log('onTabClick'); */
        },
        onTabShow: function(tab, navigation, index) {
            if (index == 2) {
                initMap();
            }
            if (index == 1) {
                getPackageServiceFee();
            } /* console.log('onTabShow'); */
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({
                width: $percent + '%'
            }); /* If it's the last tab then hide the last button and show the finish instead */
            if ($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }
    });
    $('#rootwizard .finish').click(function() { /*alert('Finished!, Starting over!');*/
        if (validatePackageForm() === false) {
            return false;
        } else if (validatePackageContactForm() === false) {
            return false;
        }
        var address_type = $('input[name=contact_address_type]:checked', '#form_addtrip_origindetail').val(); /*$('#rootwizard').find("a[href*='tab1']").trigger('click');*/ /*$( "#formAddTripPeople" ).submit();*/
        if (address_type == 'new_address') {
            if ($("#street_address_1").val() != '') {
                var street_address_1 = $("#street_address_1").val();
                var city = $("#city").val();
                var country = $("#country_short option:selected").text();
                var state = $("#state").val();
                var zip_code = $("#postal_code").val();
                var param = street_address_1 + " " + city + " " + state + " " + zip_code + "," + country;
                jQuery.ajax({
                    type: "POST",
                    url: "https://maps.google.com/maps/api/geocode/json?address=" + param + "&sensor=false&region=India",
                    data: param,
                    async: false,
                    success: function(msg) { /*var obj = jQuery.parseJSON( msg );*/
                        $("#latitude").val(msg.results[0].geometry.location.lat);
                        $("#longitude").val(msg.results[0].geometry.location.lng);
                        loadblockUI('LOADING...');
                        $('#fromSeekerPackageRequest').submit(); /*  jQuery.ajax({                            type: "POST",                            url: baseurl + '/seeker/add-package-service',                            data: $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#fromSeekerPackageRequest').serialize(),                            success: function (msg) {                                loadblockUI(msg);                                setTimeout('$.unblockUI();window.location="'+baseurl+'/seeker"',3000);                            }                        }); */
                    }
                });
            }
        } else {
            loadblockUI('LOADING...');
            $('#fromSeekerPackageRequest').submit(); /* jQuery.ajax({                type: "POST",                url: baseurl + '/seeker/add-package-service',                data: $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#fromSeekerPackageRequest').serialize(),                success: function (msg) {                    loadblockUI(msg);                    setTimeout('$.unblockUI();window.location="'+baseurl+'/seeker"',3000);                }            });*/
        }
    });
    $("#formAddTripPeople").on("submit", function(event) { /*alert( $( this ).serialize() )*/
        event.preventDefault();
        console.log($(this).serialize());
    });
}

function initAddTripSeekerProject() {
    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav',
        'debug': false,
        onShow: function(tab, navigation, index) { /* console.log('onShow'); */ },
        onNext: function(tab, navigation, index) {
            if (index == 1) {
                if (validateSeekerProjectForm()) {
                    return false;
                }
                return true;
            } else if (index == 2) {
                if (validateSeekerProjectTravelDetailsForm()) {
                    return false;
                }
                return true;
            }
        },
        onPrevious: function(tab, navigation, index) { /* console.log('onPrevious'); */ },
        onLast: function(tab, navigation, index) { /* console.log('onLast'); */ },
        onTabClick: function(tab, navigation, index, newindex) {
            if (newindex == 1) {
                if (validateSeekerProjectForm()) {
                    return false;
                }
                return true;
            } else if (newindex == 2) {
                if (validateSeekerProjectTravelDetailsForm()) {
                    return false;
                }
                return true;
            } /* console.log('onTabClick'); */
        },
        onTabShow: function(tab, navigation, index) {
            if (index == 2) {
                initMap();
            }
            if (index == 1) {
                getProjectServiceFee();
            } /* console.log('onTabShow'); */
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({
                width: $percent + '%'
            }); /* If it's the last tab then hide the last button and show the finish instead */
            if ($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }
    });
    $('#rootwizard .finish').click(function() {
        console.log('finish clicked' + validateOriginLocationForm());
        if (!validateOriginLocationForm()) return false; /*alert('Finished!, Starting over!');*/
        var address_type = $('input[name=contact_address_type]:checked', '#form_addtrip_origindetail').val(); /*$('#rootwizard').find("a[href*='tab1']").trigger('click');        $( "#formAddTripPeople" ).submit();*/
        if (address_type == 'new_address') {
            if ($("#street_address_1").val() != '') {
                var street_address_1 = $("#street_address_1").val();
                var city = $("#city").val();
                var country = $("#country_short option:selected").text();
                var state = $("#state").val();
                var zip_code = $("#postal_code").val();
                var param = street_address_1 + " " + city + " " + state + " " + zip_code + "," + country;
                jQuery.ajax({
                    type: "POST",
                    url: "https://maps.google.com/maps/api/geocode/json?address=" + param + "&sensor=false&region=India",
                    data: param,
                    async: false,
                    success: function(msg) { /*var obj = jQuery.parseJSON( msg );*/
                        $("#latitude").val(msg.results[0].geometry.location.lat);
                        $("#longitude").val(msg.results[0].geometry.location.lng);
                        jQuery.ajax({
                            type: "POST",
                            url: baseurl + '/seeker/add-project-service',
                            data: $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#formSeekerProjectRequest').serialize(),
                            success: function(response) {
                                var resObj = jQuery.parseJSON(response);
                                if (resObj.result == 'success') {
                                    loadblockUI(resObj.msg);
                                    if (resObj.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + resObj.requestURL + '"', 3000);
                                    else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + resObj.trip_id + '&service=project"', 3000);
                                }
                            }
                        });
                    }
                });
            }
        } else {
            jQuery.ajax({
                type: "POST",
                url: baseurl + '/seeker/add-project-service',
                data: $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#formSeekerProjectRequest').serialize(),
                success: function(response) {
                    var resObj = jQuery.parseJSON(response);
                    if (resObj.result == 'success') {
                        loadblockUI(resObj.msg);
                        if (resObj.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + resObj.requestURL + '"', 3000);
                        else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + resObj.trip_id + '&service=project"', 3000);
                    }
                }
            });
        }
    });
    $("#formAddTripProject").on("submit", function(event) { /*alert( $( this ).serialize() )*/
        event.preventDefault();
        console.log($(this).serialize());
    });
}

function addTravellerPeopleRequest() {
    if ($('input[name=seekerPeopleRequest]:checked').val() != 'existing_trip') {
        if (validateTravellerPepleForm() == false) {
            return false;
        }
    }
    getPassengersFee();
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/traveller/add-traveller-people-request',
        data: 'seeker_trip_id=' + $('#seeker_trip_id').val() + '&traveller_trip_id=' + $("#traveller_trip_id").val() + '&' + $('#fromTravellerPeopleRequest').serialize(),
        success: function(msg) {
            var obj = jQuery.parseJSON(msg);
            $('#traveller_people_request_id').val(obj.traveller_people_request_id);
            if (obj.traveller_trip_id) $('#traveller_trip_id').val(obj.traveller_trip_id);
            else {
                loadblockUI(obj.msg);
                setTimeout('$.unblockUI();', 3000);
            }
        }
    });
}

function addTravellerProjectRequest() {
    if ($('input[name=travellerProjectRequest]:checked').val() != 'existing_trip') {
        if (validateTravellerProjectForm() == false) {
            return false;
        }
    } /*  getProjectsFee();*/
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/traveller/add-traveller-project-request',
        data: 'seeker_trip_id=' + $('#seeker_trip_id').val() + '&traveller_trip_id=' + $("#traveller_trip_id").val() + '&' + $('#formTravellerProjectRequest').serialize(),
        success: function(msg) {
            var obj = jQuery.parseJSON(msg);
            $('#traveller_project_request_id').val(obj.traveller_project_request_id);
            if (obj.seeker_trip_id) $('#traveller_trip_id').val(obj.traveller_trip_id);
            else {
                loadblockUI(obj.msg);
                setTimeout('$.unblockUI();', 3000);
            }
        }
    });
}

function addTravellerPackageRequest1() {
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/traveller/add-traveller-package-request',
        data: 'seeker_trip_id=' + $('#seeker_trip_id').val() + '&traveller_trip_id=' + $("#traveller_trip_id").val() + '&' + $('#fromTravellerPackageRequest').serialize(),
        success: function(msg) {
            var obj = jQuery.parseJSON(msg);
            $('#traveller_package_request_id').val(obj.traveller_package_request_id);
            if (obj.traveller_trip_id) $('#traveller_trip_id').val(obj.traveller_trip_id);
            else {
                loadblockUI(obj.msg);
                setTimeout('$.unblockUI();', 3000);
            }
        }
    });
}

function addTravellerPackageRequest() {
    if ($('input[name=seekerPeopleRequest]:checked').val() != 'existing_trip') {
        if (validateTravellerPackageForm() == false) {
            return false;
        }
    } /*getPacakgesFee();*/
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/traveller/add-traveller-package-request',
        data: 'seeker_trip_id=' + $('#seeker_trip_id').val() + '&traveller_trip_id=' + $("#traveller_trip_id").val() + '&' + $('#fromTravellerPackageRequest').serialize(),
        success: function(msg) {
            var obj = jQuery.parseJSON(msg);
            $('#traveller_package_request_id').val(obj.traveller_package_request_id);
            if (obj.traveller_trip_id) $('#traveller_trip_id').val(obj.traveller_trip_id);
            else {
                loadblockUI(obj.msg);
                setTimeout('$.unblockUI();', 3000);
            }
        }
    });
}

function addTravellerTripRequest() {
    var loggedin_status = checklogin();
    if (loggedin_status == 'yes') {
        if (checkDocumentStatus()) {
            if ($('#traveller_people_request_id').val() == '' && $('#traveller_package_request_id').val() == '' && $('#traveller_project_request_id').val() == '') {
                loadblockUI('Please add or select your request information to send request');
                setTimeout('$.unblockUI();', 3000);
            } else {
                if (validateTravellerCardForm()) {
                    $.magnificPopup.close();
                    var cardtype = $("#cardtype").val();
                    jQuery.ajax({
                        type: "POST",
                        url: baseurl + '/traveller/add-traveller-trip-request',
                        data: 'seeker_trip_id=' + $("#seeker_trip_id").val() + '&traveller_people_request_id=' + $('#traveller_people_request_id').val() + '&traveller_package_request_id=' + $('#traveller_package_request_id').val() + '&traveller_project_request_id=' + $('#traveller_project_request_id').val() + '&traveller_people_service_id=' + $('#traveller_people_service_id').val() + '&traveller_package_service_id=' + $('#traveller_package_service_id').val() + '&traveller_project_service_id=' + $('#traveller_project_service_id').val() + '&cardtype=' + cardtype + '&existing_card=' + $('.existing_card:checked').val() + '&holder_name=' + $('#holder_name').val() + '&card_no=' + $('#card_no').val() + '&card_valid=' + $('#card_valid').val() + '&card_cvv=' + $('#card_cvv').val() + '&addUserCard=' + $('#addUserCard').val() + '&card_type=' + $('#card_type').val() + '&payment_card_type=' + $('#payment_card_type').val() + '&issue_date=' + $('#issue_date').val() + '&issue_number=' + $('#issue_number').val() + '&traveller_package_amount=' + $('#traveller_package_amount').val(),
                        success: function(msg) {
                            loadblockUI(msg);
                            setTimeout('$.unblockUI();', 3000);
                        }
                    });
                }
            }
        }
    } else {
        loginlightbox();
        after_login_forward = 'refresh';
    }
}

function checkDocumentStatus() {
    var skip_document_verify = 0;
    if ($('#skip_document_verify').length != 0) {
        skip_document_verify = $('#skip_document_verify').val();
    }
    if (skip_document_verify == 1) return true;
    var document_status = true;
    $.ajax({
        type: "POST",
        url: baseurl + "/seeker/checkdocumentstatus",
        data: '',
        async: false,
        success: function(response) {
            if (response != 0) {
                document_status = false;
                $.unblockUI();
                $('#document-upload-confimation-content').html(response);
                $('#select-type').show();
                $.magnificPopup.open({
                    items: {
                        src: '#document-upload-confimation'
                    },
                    type: 'inline'
                }, 0);
            }
        }
    });
    return document_status;
}

function validateTravellerCardForm() {
    clrErr();
    $cardValidate = true;
    if ($('.existing_card').length != 0) {
        if ($('.existing_card:checked').val() && typeof $('.existing_card:checked').val() !== "undefined") {
            $cardValidate = false;
        }
    }
    if ($('#pay_card_info').length != 0 && $('#pay_card_info').css('display') == 'none') {
        $('#pay_card_info').show();
        $.magnificPopup.open({
            items: {
                src: '#pay_card_info'
            },
            type: 'inline'
        }, 0);
        return false;
    }
    $errStatus = true;
    if ($('.cardtype').length != 0) {
        if ($('#cardtype').is(':checked') === true) {
            $cardValidate = false;
            if (validateFormFields('existing_card', 'Please select a card.', '')) $errStatus = false;
        }
    }
    if ($cardValidate && $('#holder_name').length != 0) {
        var cardType = $.payment.cardType($('#card_no').val());
        if (!$.payment.validateCardNumber($('#card_no').val())) {
            $('#err_card_no').text('Incorrect card number').show();
            $errStatus = false;
        }
        if (!$.payment.validateCardExpiry($('#card_valid').payment('cardExpiryVal'))) {
            $('#err_card_valid').text('Incorrect valid thru').show();
            $errStatus = false;
        }
        if (!$.payment.validateCardCVC($('#card_cvv').val(), cardType)) {
            $('#err_card_cvv').text('Incorrect CVC').show();
            $errStatus = false;
        }
        if (cardType == 'maestro') {
            if (!$.payment.validateCardExpiry($('#issue_date').payment('cardExpiryVal'))) {
                $('#err_issue_date').text('Incorrect issue date').show();
                $errStatus = false;
            }
            if (validateFormFields('issue_number', 'Please enter issue number.', 'number')) $errStatus = false;
        }
        if (validateFormFields('holder_name', 'Please enter the name.', '')) $errStatus = false;
        if (validateFormFields('payment_card_type', 'Please select payment card type.', '')) $errStatus = false;
        if (validateFormFields('card_billing_country', 'Please select country.', '')) $errStatus = false;
        if (validateFormFields('card_billing_zipcode', 'Please enter the zipcode.', '')) $errStatus = false;
    }
    if ($errStatus == true) {
        $('#card_type').val(cardType)
    }
    return $errStatus;
}

function validateCardNo(fieldId) {
    console.log('card_no' + fieldId);
}

function processPayment() {
    var requestService = $('#requestService').val();
    if (validateTravellerCardForm() || $('#status').val() == 2) {
        if (requestService == 'people') {
            approveTravellerPeopleRequest();
        } else if (requestService == 'package') {
            approveTravellerPackageRequest();
        } else if (requestService == 'project') {
            approveTravellerProjectRequest();
        } else if (requestService == 'tpackage') {
            approvePackageRequest();
        }
    }
}

function validatePaymentCardDetails(type) {
    type = type || 'edit';
    clrErr();
    $noError = true;
    var prefixStr = '';
    if (type == 'new') {
        prefixStr = 'new_';
    } else if (type == 'edit') {
        prefixStr = 'e';
    }
    var cardType = $.payment.cardType($('#' + prefixStr + 'card_no').val());
    if (!$.payment.validateCardNumber($('#' + prefixStr + 'card_no').val())) {
        $('#err_' + prefixStr + 'card_no').text('Incorrect card number').show();
        $noError = false;
    }
    if (!$.payment.validateCardExpiry($('#' + prefixStr + 'card_valid').payment('cardExpiryVal'))) {
        $('#err_' + prefixStr + 'card_valid').text('Incorrect valid thru').show();
        $noError = false;
    }
    if (!$.payment.validateCardCVC($('#' + prefixStr + 'card_cvv').val(), cardType)) {
        $('#err_' + prefixStr + 'card_cvv').text('Incorrect CVC').show();
        $noError = false;
    }
    if (cardType == 'maestro') {
        if (!$.payment.validateCardExpiry($('#' + prefixStr + 'issue_date').payment('cardExpiryVal'))) {
            $('#err_' + prefixStr + 'issue_date').text('Incorrect issue date').show();
            $noError = false;
        }
        if (validateFormFields(prefixStr + 'issue_number', 'Please enter issue number.', 'number')) $noError = false;
    }
    if (validateFormFields(prefixStr + 'holder_name', 'Please enter card holder name.', '')) $noError = false;
    if (validateFormFields(prefixStr + 'payment_card_type', 'Please select payment card type.', '')) $noError = false;
    if (validateFormFields(prefixStr + 'card_billing_address', 'Please enter billing address.', '')) $noError = false;
    if (validateFormFields(prefixStr + 'card_billing_country', 'Please select billing country.', '')) $noError = false;
    if (validateFormFields(prefixStr + 'card_billing_zipcode', 'Please enter billing zipcode.', '')) $noError = false;
    if ($noError == true) {
        $('#' + prefixStr + 'card_type').val(cardType)
    }
    return $noError;
}

function approveTravellerPeopleRequest() {
    $('#sucpMsg').hide();
    $('#errorpMsg').hide();
    $('#payment_loading').show();
    if ($('#status').val() == 2) {
        loadblockUI('Loading...');
    }
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/seeker/approve-traveller-people-request',
        data: $('#form_process_payment').serialize(),
        dataType: 'json',
        success: function(json) {
            $('#payment_loading').hide();
            if (json['msg_status'] == 'success') {
                $('#sucpMsg').show();
                $('.sucpMsg').html(json['msg']);
                if ($('#status').val() == 2) {
                    loadblockUI(json['msg']);
                    setTimeout('$.unblockUI();location.reload()', 2000);
                } else {
                    setTimeout('$.unblockUI();window.location="' + baseurl + '/seeker/receipt/' + json['pay_id'] + '?service=People"', 2000);
                }
            } else {
                $('#errorpMsg').show();
                $('.errorpMsg').html(json['msg']);
            }
        }
    });
}

function approveTravellerPackageRequest() {
    $('#sucpMsg').hide();
    $('#errorpMsg').hide();
    $('#payment_loading').show();
    if ($('#status').val() == 2) {
        loadblockUI('Loading...');
    }
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/seeker/approve-traveller-package-request',
        data: $('#form_process_payment').serialize(),
        success: function(result) {
            $('#payment_loading').hide();
            var objJson = jQuery.parseJSON(result);
            if (objJson.msg_status == 'success') {
                $('#sucpMsg').show();
                $('.sucpMsg').html(objJson.msg);
                if ($('#status').val() == 2) {
                    setTimeout('$.unblockUI();location.reload()', 2000);
                } else {
                    setTimeout('$.unblockUI();window.location="' + baseurl + '/seeker/receipt/' + objJson.pay_id + '?service=Package"', 2000);
                }
            } else {
                $('#errorpMsg').show();
                $('.errorpMsg').html(objJson.msg);
            }
        }
    });
}

function approveTravellerProjectRequest() {
    $('#sucpMsg').hide();
    $('#errorpMsg').hide();
    $('#payment_loading').show();
    if ($('#status').val() == 2) {
        loadblockUI('Loading...');
    }
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/seeker/approve-traveller-project-request',
        data: $('#form_process_payment').serialize(),
        success: function(result) {
            var objJson = jQuery.parseJSON(result);
            if (objJson.msg_status == 'success') {
                $('#sucpMsg').show();
                $('.sucpMsg').html(objJson.msg);
                setTimeout('$.unblockUI();window.location="' + baseurl + '/seeker/receipt/' + objJson.pay_id + '?service=Project"', 2000);
            } else {
                $('#errorpMsg').show();
                $('.errorpMsg').html(objJson.msg);
            }
        }
    });
}

function curcyval(a, b) {
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/currencyrate',
        data: "type=" + a + "&asv=" + b,
        success: function(msg) {}
    });
}

function seekerTripHistoryClinkload() {
    $('#loading').show();
    var pg = $('#pagv').val();
    var pg2 = parseInt(pg) + 1;
    $('#pagv').val(pg2);
    $.ajax({
        url: baseurl + '/traveller/mytriphistoryscroll',
        type: "POST",
        data: "actionfunction=showData&page=" + pg2,
        cache: false,
        success: function(response) {
            $('#loading').hide();
            $('.ladtg').hide();
            $('#demoajax').append(response);
        }
    });
}

function currencycountry(cur_country, type) {
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/getcountry',
        data: "cur_country=" + cur_country + "&type=" + type,
        success: function(msg) {
            var obj = jQuery.parseJSON(msg);
            $('.footerCountry.selectpicker').selectpicker();
            $('.footerCountry.selectpicker').selectpicker('val', obj.code);
            $('.footerCountry.selectpicker').selectpicker('refresh');
            $('.footerCurrency.selectpicker').selectpicker();
            $('.footerCurrency.selectpicker').selectpicker('val', obj.currency_code);
            $('.footerCurrency.selectpicker').selectpicker('refresh'); /*$("#footer_country").val(obj.code);            $("#footer_currency").val(obj.currency_code);*/
        }
    });
}

function TravellerrequestClinkload() {
    $('#loading').show();
    var pg = $('#pagv').val();
    var pg2 = parseInt(pg) + 1;
    $('#pagv').val(pg2);
    $.ajax({
        url: baseurl + '/traveller/seekerrequestscroll',
        type: "POST",
        data: "actionfunction=showData&page=" + pg2,
        cache: false,
        success: function(response) {
            $('#loading').hide();
            $('.ladtg').hide();
            $('#demoajax').append(response);
        }
    });
}

function SelectService(divid) {
    var loggedin_status = checklogin();
    if (loggedin_status == 'yes') {
        $('#' + divid).slideToggle();
    } else {
        loginlightbox('');
        after_login_forward = 'refresh';
    }
}

function SeekerPeopleServiceSelect() {}

function SeekerPackageServiceSelect() {
    var loggedin_status = checklogin();
    if (loggedin_status == 'yes') {
        $('#package-info-request').slideToggle();
    } else {
        loginlightbox('checkout');
        after_login_forward = 'refresh';
    }
}

function SeekerProjectServiceSelect() {
    var loggedin_status = checklogin();
    if (loggedin_status == 'yes') {
        $('#project-info-request').slideToggle();
    } else {
        loginlightbox('checkout');
        after_login_forward = 'refresh';
    }
}

function checklogin() {
    var loggedin_status = '';
    $.ajax({
        type: "POST",
        url: baseurl + "/checklogin",
        data: '',
        async: false,
        success: function(msg) {
            var msgdata = msg.split('@SPLIT@');
            loggedin_status = msgdata[1];
        }
    });
    return loggedin_status;
}

function loginlightbox(forwardto) {
    $("#small-login").trigger("click");
}

function getSeekerPeopleServiceNeed(tripid) {
    $.ajax({
        type: "POST",
        url: baseurl + "/seeker/get-seeker-people-service-need",
        data: 'tripid=' + tripid,
        async: false,
        success: function(msg) {
            var msgdata = msg.split('@SPLIT@');
            $('#seeker_people_request_id').val(msgdata[1]);
            $('.span_people_service_fee').html('$' + msgdata[2]);
            $('.span_people_service_fee_text').html(msgdata[3]);
            $('#divPassengersDetail').html(msgdata[4]);
        }
    });
}

function getSeekerPackageServiceNeed(tripid) {
    $.ajax({
        type: "POST",
        url: baseurl + "/seeker/get-seeker-package-service-need",
        data: 'tripid=' + tripid,
        async: false,
        success: function(msg) {
            var msgdata = msg.split('@SPLIT@');
            $('#seeker_package_request_id').val(msgdata[1]);
            $('.span_package_service_fee').html('$' + msgdata[2]);
            $('.span_package_service_fee_text').html(msgdata[3]);
            $('#traveller_package_amount').val(msgdata[4]);
            $('#divPackagesDetail').html(msgdata[5]);
        }
    });
}

function getSeekerProjectServiceNeed(tripid) {
    $.ajax({
        type: "POST",
        url: baseurl + "/seeker/get-seeker-project-service-need",
        data: 'tripid=' + tripid,
        async: false,
        success: function(msg) {
            var msgdata = msg.split('@SPLIT@');
            $('#seeker_project_request_id').val(msgdata[1]);
            $('.span_project_service_fee').html('$' + msgdata[2]);
            $('.span_project_service_fee_text').html(msgdata[3]);
            $('#divProjectsDetail').html(msgdata[4]);
        }
    });
}

function getTravellerPeopleServiceNeed(tripid) {
    $.ajax({
        type: "POST",
        url: baseurl + "/traveller/get-traveller-people-service-need",
        data: 'tripid=' + tripid,
        async: false,
        success: function(msg) {
            var msgdata = msg.split('@SPLIT@');
            $('#traveller_people_request_id').val(msgdata[1]);
            $('#divPassengersDetail').html(msgdata[2]);
        }
    });
}

function getTravellerPackageServiceNeed(tripid) {
    $.ajax({
        type: "POST",
        url: baseurl + "/traveller/get-traveller-package-service-need",
        data: 'tripid=' + tripid,
        async: false,
        success: function(msg) {
            var msgdata = msg.split('@SPLIT@');
            $('#traveller_package_request_id').val(msgdata[1]);
            $('#divPackagesDetail').html(msgdata[2]);
        }
    });
}

function getTravellerProjectServiceNeed(tripid) {
    $.ajax({
        type: "POST",
        url: baseurl + "/traveller/get-traveller-project-service-need",
        data: 'tripid=' + tripid,
        async: false,
        success: function(msg) {
            var msgdata = msg.split('@SPLIT@');
            $('#traveller_project_request_id').val(msgdata[1]);
            $('#divProjectsDetail').html(msgdata[2]);
        }
    });
}
function loadingfunct(){
	$('html').prepend("<div class='loading_string' style='background:rgba(0,0,0,0.5);z-index:10000;width:100%;height:100%;position:fixed;'><div style='margin:20% auto;text-align:center'><img src='"+$('.header-top div .logo img').attr('src')+"'></div></div>");
}
function remloadingfunct(){
	$('.loading_string').remove();
}
function getFlightStops(e) {
	if (validateFlightSatesForm()) {
        e.stopPropagation();
        return false;
    } /*$('#nstop').html('LOADING...');*/
    // loadblockUI('Loading...');
    // $('#loading_flights_msg').show();
	
	loadingfunct();
	//$('#Loading_Modal').modal('show');
    var originplace = $('#origin_location_code').val();
    var destinationplace = $('#destination_location_code').val(); /*var inbounddate= $('#departure_date').val(); */
    var departure_date = $('#departure_date').val();
    var cabinclass = $('#cabin').val();
    $.ajax({
        type: "POST",
        url: baseurl + "/trips/oneway",
        data: 'origin=' + originplace + '&destination=' + destinationplace + '&departure_date=' + departure_date,
        async: false,
        success: function(res) {
            var resObj = $.parseJSON(res);
            $('#divFlights').html('');
            // $('#loading_flights_msg').hide();
			remloadingfunct();
            if (resObj.result == 'success') {
				//$('#Loading_Modal').modal('hide');
                $('#typeOfFlightSelect').val('auto');
                $('#nstop').html(resObj.optionData);
                $('#flights').slideDown();
                $('#add_flight_manually').slideUp();
                $('#showManuallyAddedFlights1').hide();
                $('#showManuallyFlights1').show();
                $('#getFlightFlights1').hide();
                showManuallyFlights(1);
            } else if (resObj.result == 'failure') {
                $('#info-fight-msg').html('Please enter or select details of airlines per flight booking you have made for this journey.');
                $('#typeOfFlightSelect').val('manual');
                $('#add_flight_manually').slideDown();
                $('#flights').slideUp();
                $('#showManuallyAddedFlights1').show();
                $('#showManuallyFlights1').hide();
                $('#getFlightFlights1').hide();
                addRemoveFlightDetails('');
            }
        }
    });
    $.unblockUI();
}

function addRemoveFlightDetails(count) {
    console.log('nstop value: ' + $('#selected_flights').val());
    if (count == '') {
        $('.manual_flight_section').slideUp();
    } else {
        $('.manual_flight_section').slideDown();
        var str_box = '';
        count = (count == 0) ? 1 : parseInt(count) + parseInt(1);
        console.log('count: ' + count);
        if (count > 0) {
            var newNum = 0;
            for (i = 1; i <= count; i++) {
                newNum = i;
                str_box += '<div id="repeatable_' + newNum + '" class="manual_flight_section">';
                str_box += '    <div class="panel panel-default">';
                str_box += '        <div class="panel-body">';
                str_box += '        <h5 class="sub-heading text-left">Details of flight ' + newNum + '</h5><hr class="title-border">';
                str_box += '            <div class="row">';
                str_box += '            <div class="form-group col-sm-6">';
                str_box += '                <label class="control-label">Origin Location <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                    <input class="typeahead form-control manual_origin_location" placeholder="City or Airport Code" type="text" name="manual_origin_location[]" id="manual_origin_location_' + newNum + '" />';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_manual_origin_location_' + newNum + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '            <div class="form-group col-sm-6">';
                str_box += '                <label class="control-label">Destination Location <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                    <input class="typeahead form-control manual_destination_location" placeholder="City or Airport Code" type="text" name="manual_destination_location" id="manual_destination_location_' + newNum + '" />';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_manual_destination_location_' + newNum + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '            </div>';
                str_box += '            <div class="row">';
                str_box += '            <div class="form-group col-sm-6">';
                str_box += '                <label class="control-label">Airline name <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                    <input type="text" name="manual_airline_name[]" id="airline_name_' + newNum + '" class="form-control typeahead manual_airline_name" />';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_manual_airline_name_' + newNum + '" class="text-danger err_msg"></p>';
                str_box += '            </div>';
                str_box += '            <div class="col-sm-6">';
                str_box += '                <label class="control-label">Airline number <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '            <div class="row">';
                str_box += '                <div class="form-group col-sm-6">';
                str_box += '                <div class="input-group">';
                str_box += '                    <span class="input-group-addon">Carrier</span>';
                str_box += '                    <input type="text" name="manual_airline_carrier[]" id="manual_airline_carrier_' + newNum + '" class="form-control manual_airline_carrier" />';
                str_box += '                </div>';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_manual_airline_carrier_' + newNum + '" class="text-danger err_msg"></p>';
                str_box += '                </div>';
                str_box += '                <div class="form-group col-sm-6">';
                str_box += '                <div class="input-group">';
                str_box += '                    <span class="input-group-addon">Number</span>';
                str_box += '                    <input type="text" name="manual_airline_number[]" id="manual_airline_number_' + newNum + '" class="form-control manual_airline_number" />';
                str_box += '                </div>';
                str_box += '                    <p style="margin-bottom: 0px;" id="err_manual_airline_number_' + newNum + '" class="text-danger err_msg"></p>';
                str_box += '                </div>';
                str_box += '                </div>';
                str_box += '            </div>';
                str_box += '            </div>';
                str_box += '            <div class="row">';
                str_box += '            <div class="col-sm-6">';
                str_box += '                <label class="control-label">Departure Date <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="col-sm-6 form-group">';
                str_box += '                            <div class="input-group">';
                str_box += '                                <input class="date-pick form-control manual_departure_date" name="manual_departure_date[]" id="manual_departure_date_' + newNum + '" type="text" /><span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>';
                str_box += '                            </div>';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_manual_departure_date_' + newNum + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                        <div class="col-sm-6 form-group">';
                str_box += '                                    <div class="input-group">';
                str_box += '                                        <input class="time-pick form-control manual_departure_time" name="manual_departure_time[]" id="manual_departure_time_' + newNum + '" type="text" /><span class="input-group-addon"><i class="fa fa-clock-o fa-fw"></i></span>';
                str_box += '                                    </div>';
                str_box += '                                    <p style="margin-bottom: 0px;" id="err_manual_departure_time_' + newNum + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '            </div>';
                str_box += '            <div class="col-sm-6">';
                str_box += '                <label class="control-label">Arrival date <span class="text-danger">*</span><a class="icon-info" href="#" data-toggle="tooltip" title=""></a></label>';
                str_box += '                    <div class="row">';
                str_box += '                        <div class="col-sm-6 form-group">';
                str_box += '                            <div class="input-group">';
                str_box += '                                <input class="date-pick form-control manual_arrival_date" name="manual_arrival_date[]" id="manual_arrival_date_' + newNum + '" type="text" /><span class="input-group-addon"><i class="fa fa-calendar fa-fw"></i></span>';
                str_box += '                            </div>';
                str_box += '                            <p style="margin-bottom: 0px;" id="err_manual_arrival_date_' + newNum + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                        <div class="col-sm-6 form-group">';
                str_box += '                                    <div class="input-group">';
                str_box += '                                        <input class="time-pick form-control manual_arrival_time" name="manual_arrival_time[]" id="manual_arrival_time_' + newNum + '" type="text" /><span class="input-group-addon"><i class="fa fa-clock-o fa-fw"></i></span>';
                str_box += '                                    </div>';
                str_box += '                                    <p style="margin-bottom: 0px;" id="err_manual_arrival_time_' + newNum + '" class="text-danger err_msg"></p>';
                str_box += '                        </div>';
                str_box += '                    </div>';
                str_box += '                    </div>';
                str_box += '            </div>';
                str_box += '        </div>';
                str_box += '    </div>';
                str_box += '</div>';
            }
        }
        $('#flightSectionBox').html(str_box);
        bindAutoComplete('typeahead');
        bindAirlinesAutoComplete('manual_airline_name');
        bindDatePicker('date-pick');
        bindTimePicker('time-pick');
    }
}

function getFlights() {
    var stops = $('#nstop').val();
    $.ajax({
        type: "POST",
        url: baseurl + "/trips/getflights",
        data: 'stops=' + stops,
        async: false,
        success: function(msg) {
            $('#divFlights').html(msg);
            $('#getFlightStops #getflightstauspopup').DataTable();
        }
    });
}

function getSearchFlights(stops) {
    $.ajax({
        type: "POST",
        url: baseurl + "/trips/getSearchflights",
        data: 'stops=' + stops,
        async: false,
        success: function(msg) {
            $('#origin_route').html(msg);
            $('#origin_route').selectpicker('refresh');
        }
    });
}
var selectedFlightData = false; /* For manually auto selected using travel payout flights */
function selectFlight(flight_key, proposal_key) {
    var service_type = $('#service_type').val();
    $.ajax({
        type: "POST",
        url: baseurl + "/trips/get-selected-flight",
        data: 'flight_key=' + flight_key + '&proposal_key=' + proposal_key + "&service_type=" + service_type + "&flightSelectionType=auto",
        async: false,
        success: function(msg) {
            var resObj = $.parseJSON(msg);
            $('#divSelectedFlight').html(resObj.flightTableHtml);
            $('.serviceFee').html('$' + resObj.service_fee);
            $('#service_fee').val(resObj.service_fee);
            $('#distance').val(resObj.distance);
            $("#proposal_key").val(proposal_key);
            $("#trip_route").val(resObj.trip_route);
            $(".close").trigger("click");
            $("#flight_key").val(flight_key);
            if ($('#noofstops').length != 0) $('#noofstops').val($('#nstop').val());
            selectedFlightData = resObj.flightDetails;
            getServiceFee();
            if ($('#tripOtherDetails').length != 0) {
                $('#tripOtherDetails').show();
            }
        }
    });
} /* For manually added flights */
function showManuallyAddedFlights(formName) {
    formName = formName || 'formSeekerPeopleRequest';
    console.log(validateFlightManualAddForm());
    if (validateFlightManualAddForm()) return false;
    jQuery.ajax({
        type: "POST",
        url: baseurl + '/trips/get-selected-flight',
        data: $('#' + formName).serialize() + "&flightSelectionType=manual&service_type=" + $('#service_type').val(),
        success: function(msg) {
            var resObj = $.parseJSON(msg);
            $('#divSelectedFlight').html(resObj.flightTableHtml);
            $('.serviceFee').html('$' + resObj.service_fee);
            $('#service_fee').val(resObj.service_fee);
            $('#distance').val(resObj.distance);
            $("#proposal_key").val('');
            $(".close").trigger("click");
            $("#flight_key").val('');
            getServiceFee();
            selectedFlightData = resObj.flightDetails;
            if ($('#tripOtherDetails').length != 0) {
                $('#tripOtherDetails').show();
            }
        }
    });
}

function showManuallyFlights(value) {
    if (value == 0) {
        var typeOfFlightSelect = $('#typeOfFlightSelect').val();
        if (typeOfFlightSelect == 'auto') {
            $('#info-fight-msg').html('Please add your filghts manually.');
            $('#typeOfFlightSelect').val('manual');
            $('#add_flight_manually').slideDown();
            $('#flights').slideUp();
            $('#showManuallyAddedFlights1').show();
            $('#showManuallyFlights1').hide();
            $('#getFlightFlights1').show();
            addRemoveFlightDetails('');
        }
    }
}

function addressbookmap() { /*console.log('function called');    $("#street_address_1").geocomplete({        map: ".map_canvas",        location: [52.4078567, 16.92718339999999], /* is it centring the map, no pin thought*/ /*details: "#form_origin_location_address",        types: ["geocode", "establishment"]    });        /* var cnt=  $("#street_address_1").geocomplete("find", "country");    alert(cnt);*/ /*$("input[name=country]").change(function() {        alert(this.value);        $("#country").val(this.value);    });*/ }
var confirmDialog = function(message, headline, cb) {
    var dialog = '<div class="dialog confirm mfp-with-anim" role="dialog"><div class="modal-dialog"><div class="modal-content modal-content-new">';
    if (headline) {
        dialog += '<div class="head-wrap">' + '<h4 class="tab-heading social-signup-title">' + headline + '</h4><button class="mfp-close" type="button" title="%title%">×</button>' + '</div>'; /*dialog += '<h2>' + headline + '</h2>';*/
    }
    dialog += '<div class="modal-body text-center"><p>' + message + '</p>';
    dialog += '<div class="actions">';
    dialog += '<button type="button" class="btn btn-primary mg-right5 btn-submit">Yes</button>&nbsp;&nbsp;';
    dialog += '<button type="button" class="btn btn-black btn-cancel">No</button> ';
    dialog += '</div>';
    dialog += '</div>';
    dialog += '</div></div></div>';
    $.magnificPopup.open({
        modal: true,
        items: {
            src: dialog,
            type: 'inline'
        },
        callbacks: {
            open: function() {
                var $content = $(this.content);
                $content.on('click', '.btn-submit', function() {
                    if (typeof cb == 'function') {
                        cb();
                    }
                    $.magnificPopup.close();
                    $(document).off('keydown', keydownHandler);
                });
                $content.on('click', '.btn-cancel', function() {
                    $.magnificPopup.close();
                    $(document).off('keydown', keydownHandler);
                });
                var keydownHandler = function(e) {
                    if (e.keyCode == 13) {
                        $content.find('.btn-submit').click();
                        return false;
                    } else if (e.keyCode == 27) {
                        $content.find('.btn-cancel').click();
                        return false;
                    }
                };
                $(document).on('keydown', keydownHandler);
            }
        }
    });
};

/*function validateSignUpForm() {
    var isError = false;
    $(".form-group").removeClass("has-success has-warning has-error");
    $(".err_msg").html("");
    if ($('#first_name').val() == '' || $('#first_name').val() == undefined) {
        setMsg('first_name', 'Please enter first name!');
        isError = true;
    }
    if ($('#last_name').val() == '' || $('#last_name').val() == undefined) {
        setMsg('last_name', 'Please enter last name!');
        isError = true;
    }
    if ($('#email_address').val() == '' || $('#email_address').val() == undefined) {
        setMsg('email_address', 'Please enter email address!');
        isError = true;
    } else if ($('#email_address').val() != '' && !validateEmail($('#email_address').val())) {
        setMsg('email_address', 'Please enter valid email address!');
        isError = true;
    }
    if ($('#password').val() == '' || $('#password').val() == undefined) {
        setMsg('password', 'Please enter password!');
        isError = true;
    }
    if ($('#dob').val() == '' || $('#dob').val() == undefined) {
        setMsg('dob', 'Please select date of birth!');
        isError = true;
    } else {
        var enteredValue = $('#dob');
        var enteredAge = getAge(enteredValue.val());
        if (parseInt(enteredAge) < 18) {
            setMsg('dob', 'Your age should have above 18 to complete signup process!');
            isError = true;
        }
    }
    if (isError == true) {
        return false;
    } else {
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var email = $("#email_address").val();
        var password = $("#password").val();
        var dob = $("#dob").val();
        var subscribe = $("input[name='subscribe']:checked").val();
        var userReferrer = $("#userReferrer").val();
        var param = "first_name=" + first_name + "&last_name=" + last_name + "&email=" + email + "&password=" + password + "&dob=" + dob + "&subscribe=" + subscribe;
        if (userReferrer != '') {
            param += "&referrer=" + userReferrer;
        }
        $("#signup-loader").show();
        $.ajax({
            type: "POST",
            url: baseurl + '/signup',
            data: param,
            success: function(msg) {
                var datamsg = msg.split("@SPLIT@");
                if (datamsg[1] != 'EmailExist') {
                    $("#loggedin_img_name").html(datamsg[1]);
                    $("#topLinkLogin").hide();
                    $("#topLinkSingup").hide();
                    $("#loggedin_container").show();
                    $.magnificPopup.close();
                    window.location = baseurl + '/dashboard';
                } else {
                    $("#signup-loader").hide();
                    $("#signupErrMsg").slideDown();
                }
            }
        });
    }
}*/
function removeBack(id){
	$('#'+id).css({"background-color":"white","border-color":"#3fb34f !important"});
	$('#'+id+'Label').hide();
	if(id =='email_address'){$('#email_address1Label').hide();}
	if(id == 'dobDay'){$('#dobDay').css({"background-color":"white","border-color":"#3fb34f"});$('#dobLabel').hide();}
	if(id == 'dobMonth'){$('#dobMonth').css({"background-color":"white","border-color":"#3fb34f"});$('#dobLabel').hide();}
	if(id == 'dobYear'){$('#dobYear').css({"background-color":"white","border-color":"#3fb34f"});$('#dobLabel').hide();}
}
function checkValue(id){
	//var len = $("#"+id).val().length;
	//if(len > 0){
		if(id =='email'){$('#emailLabelInvalid').hide();}
		$('#'+id+'Label').hide();
		$("#"+id).css({"background-color":"white","border-color":"#3fb34f"});
	//}
}
function validateSignUpForm() {
    if ($('#first_name').val() == '') {
        $('#first_nameLabel').show();
		$('#first_name').css({"background-color":"wheat","border-color":"#ffb400"});
    }else{
		$('#first_name').css({"background-color":"white","border-color":"#3fb34f"});
	}
    if ($('#last_name').val() == '') {
        $('#last_nameLabel').show();
		$('#last_name').css({"background-color":"wheat","border-color":"#ffb400"});
    }else{
		$('#last_name').css({"background-color":"white","border-color":"#3fb34f"});
	}
    if ($('#email_address').val() == '') {
        $('#email_addressLabel').show();
		$('#email_address').css({"background-color":"wheat","border-color":"#ffb400"});
    } else if ($('#email_address').val() != '' && !validateEmail($('#email_address').val())) {
        $('#email_address1Label').show();
		$('#email_address').css({"background-color":"wheat","border-color":"#ffb400"});
    }else{
		$('#email_address').css({"background-color":"white","border-color":"#3fb34f"});
	}
    if ($('#password').val() == '') {
        $('#passwordLabel').show();
		$('#password').css({"background-color":"wheat","border-color":"#ffb400"});
    }else{
		$('#password').css({"background-color":"white","border-color":"#3fb34f"});
	}
    if ($('#dob').val() == '') {
        $('#dobLabel').show();
		$('#dob').css({"background-color":"wheat","border-color":"#ffb400"});
    }else{
		$('#dob').css({"background-color":"white","border-color":"#3fb34f"});
	}
	if ($('#gender').val() == '') {
        $('#genderLabel').show();
		$('#gender').css({"background-color":"wheat","border-color":"#ffb400"});
    }else{
		$('#gender').css({"background-color":"white","border-color":"#3fb34f"});
	}
	if ($('#location').val() == '') {
        $('#locationLabel').show();
		$('#location').css({"background-color":"wheat","border-color":"#ffb400"});
    }else{
		$('#location').css({"background-color":"white","border-color":"#3fb34f"});
	}
	if ($('#dobDay').val() == '' || $('#dobMonth').val() == '' || $('#dobYear').val() == '') {
        $('#dobLabel').show();
		if($('#dobDay').val() == ''){$('#dobDay').css({"background-color":"wheat","border-color":"#ffb400"});}
		if($('#dobMonth').val() == ''){$('#dobMonth').css({"background-color":"wheat","border-color":"#ffb400"});}
		if($('#dobYear').val() == ''){$('#dobYear').css({"background-color":"wheat","border-color":"#ffb400"});}
	}else{
		//$('#first_name').css({"background-color":"wheat","border-color":"#ffb400"});
	}
	
    
        var first_name = $("#first_name").val();
        var last_name = $("#last_name").val();
        var email = $("#email_address").val();
        var password = $("#password").val();
        var dob = $("#dobMonth > option:selected").val()+'/'+$("#dobDay > option:selected").val()+'/'+$("#dobYear > option:selected").val();
		var gender = $("#gender > option:selected").val();
		var location = $("#location").val();
        var subscribe = $("input[name='subscribe']:checked").val();
        var userReferrer = $("#userReferrer").val();
        //var param = "first_name=" + first_name + "&last_name=" + last_name + "&email=" + email + "&password=" + password + "&dob=" + dob + "&subscribe=" + subscribe;
		var param = "first_name=" + first_name + "&last_name=" + last_name + "&email=" + email + "&password=" + password + "&dob=" + dob + "&subscribe=" + subscribe + "&gender=" + gender;
        if (userReferrer != '') {
            param += "&referrer=" + userReferrer;
        }
		if (password.match(/([a-zA-Z])/) && password.match(/([0-9])/) && password.match(/([!,%,&,@,#,$,^,*,?,_,~])/)){ 
		var strength = 1;
		}else{
		$('#password12Label').show();
		$('#password').css({"background-color":"wheat","border-color":"#ffb400"});
		}
		if (first_name !=="" && last_name !=="" && email !=="" && password !=="" && dob !=="" && gender !=="" && location !=="" && strength == 1){
        $("#signup-loader").show();
        $.ajax({
            type: "POST",
            url: baseurl + '/signup',
            data: param,
            success: function(msg) {
                var datamsg = msg.split("@SPLIT@");
                if (datamsg[1] != 'EmailExist') {
                    $("#loggedin_img_name").html(datamsg[1]);
                    $("#topLinkLogin").hide();
                    $("#topLinkSingup").hide();
                    $("#loggedin_container").show();
                    $.magnificPopup.close();
                    window.location = baseurl + '/dashboard';
                } else {
                    $("#signup-loader").hide();
                    $("#signupErrMsg").slideDown();
                }
            }
        });
    }
}
function validatePackageContactForm() {
    clrErr();
    $errStaus = false;
    if ($('#contact_address_type_1').length != 0) {
        if ($('#contact_address_type_1').is(':checked')) {
            if (validateFormFields('existing_user_location', 'Please select a contact address.', '')) $errStaus = true;
        }
    }
    if ($('#contact_address_type').length != 0) {
        if ($('#contact_address_type').is(':checked')) {
            if (validateFormFields('country_short', 'Please select a country.', '')) $errStaus = true;
            if (validateFormFields('street_address_1', 'Please enter the address line 1.', '')) $errStaus = true; /*  if (validateFormFields('street_address_2', 'Please enter the address line 2.', '')) $errStaus = true;*/
            if (validateFormFields('city', 'Please enter the city.', '')) $errStaus = true;
            if (validateFormFields('state', 'Please enter the state.', '')) $errStaus = true;
            if (validateFormFields('postal_code', 'Please enter the postal code.', '')) $errStaus = true;
        }
    }
    if ($errStaus) {
        return false;
    }
    return true;
}

function validatePackageForm() {
    clrErr();
    $errStaus = false;
    if ($('.item_name').length != 0) {
        $('.item_name').each(function() {
            var item_name_id = $(this).attr('id');
            if (validateFormFields(item_name_id, 'Please enter the item name.', '')) $errStaus = true;
        });
    }
    if ($('.item_description').length != 0) {
        $('.item_description').each(function() {
            var item_description_id = $(this).attr('id');
            if (validateFormFields(item_description_id, 'Please enter the item description.', '')) $errStaus = true;
        });
    }
    if ($('.item_worth').length != 0) {
        $('.item_worth').each(function() {
            var item_worth_id = $(this).attr('id');
            if (validateFormFields(item_worth_id, 'Please enter the Approx. worth of Item.', '')) $errStaus = true;
        });
    }
    if ($('.item_weight').length != 0) {
        $('.item_weight').each(function() {
            var item_weight_id = $(this).attr('id');
            if (validateFormFields(item_weight_id, 'Please enter the Approximate weight.', '')) $errStaus = true;
        });
    } /*  if($('.item_sizes').length != 0){        $('.item_sizes').each(function(){            var item_length_id = $(this).attr('id');            if (validateFormFields(item_length_id, 'Please enter the item sizes.', '')) $errStaus = true;        });    }*/
    if (validateFormFields('total_weight', 'Please enter the total weight.', '')) $errStaus = true;
    if (validateFormFields('total_worth', 'Please enter the total worth.', '')) $errStaus = true; /*if (validateFormFields('contact_name', 'Please enter the contact name.', '')) $errStaus = true;    if (validateFormFields('contact_number', 'Please enter the contact number.', '')) $errStaus = true;*/
    if ($('#contact_email_address').val() != '') {
        if (validateFormFields('contact_email_address', 'Please enter the email address.', 'validEmail')) $errStaus = true;
    }
    if ($('#comments').length != 0)
        if (validateFormFields('comments', 'Please enter the Comments to Travelers.', '')) $errStaus = true;
    if ($('#comments1').length != 0)
        if (validateFormFields('comments1', 'Please enter comments.', '')) $errStatus = true; /* item_worth */
    if ($errStaus) {
        $('#rootwizard a[href="#package_tab1"]').trigger('click');
        return false;
    }
    return true;
}

function validateSeekerPeopleForm() {
    clrErr();
    $errStatus = false;
    if ($('.passenger_first_name').length != 0) {
        $('.passenger_first_name').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please enter the first name.', '')) $errStatus = true;
        });
    }
    if ($('.passenger_last_name').length != 0) {
        $('.passenger_last_name').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please enter the last name.', '')) $errStatus = true;
        });
    }
    if ($('.passenger_gender').length != 0) {
        $('.passenger_gender').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please select gender.', '')) $errStatus = true;
        });
    }
    if ($('.passenger_age').length != 0) {
        $('.passenger_age').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please enter age.', '')) $errStatus = true;
        });
    } /*if (validateFormFields('contact_name', 'Please enter contact name.', '')) $errStatus = true;    if (validateFormFields('contact_number', 'Please enter contact number.', '')) $errStatus = true;*/
    if ($('#email_address').val() != '') {
        if (validateFormFields('email_address', 'Please enter email address.', 'validEmail')) $errStatus = true;
    }
    if ($('#comments').length != 0)
        if (validateFormFields('comments', 'Please enter comments.', '')) $errStatus = true;
    if ($('#comments2').length != 0)
        if (validateFormFields('comments2', 'Please enter comments.', '')) $errStatus = true;
    return $errStatus;
}

function validateSeekerProjectTravelDetailsForm() {
    clrErr();
    $errStatus = false;
    var action = $('#action').val(); /* unplanned */
    if (validateFormFields('origin_location', 'Please enter product location.', '')) $errStatus = true;
    if (validateFormFields('destination_location', 'Please enter delivery location.', '')) $errStatus = true;
    if (validateFormFields('delivery_date', 'Please enter delivery date.', '')) $errStatus = true;
    return $errStatus;
}

function validateSeekerTravelDetailsForm() {
    clrErr();
    $errStatus = false;
    var action = $('#action').val();
    if ($('[name="travel_plan_type"]:checked').val() == 'existing_plan') {
        if (validateFormFields('travel_plan', 'Please select a travel plan.', '')) $errStatus = true;
    } else {
        if (validateFormFields('travel_plan_reservation_type', 'Please choose ticket reservation type.', '')) $errStatus = true;
        if ($('#travel_plan_reservation_type').val() == 1) { /* booked */
            if (validateFormFields('origin_location', 'Please enter orgin location.', '')) $errStatus = true;
            if (validateFormFields('destination_location', 'Please enter destination location.', '')) $errStatus = true;
            if (validateFormFields('departure_date', 'Please enter depature date.', '')) $errStatus = true;
            if ($('#travel_agency_url').val() != '') {
                if (validateFormFields('travel_agency_url', 'Please enter valid url.', 'validUrl')) $errStatus = true;
            }
            if ($('#agency_email').val() != '') {
                if (validateFormFields('agency_email', 'Please enter valid email address.', 'validEmail')) $errStatus = true;
            } /*if($('#supplier_url').val() != ''){                if (validateFormFields('supplier_url', 'Please enter valid url.', 'validUrl')) $errStatus = true;            }             if($('#supplier_email').val() != ''){                if (validateFormFields('supplier_email', 'Please enter valid email address.', 'validEmail')) $errStatus = true;            }*/
            if ($('#booking_site_url').val() != '') {
                if (validateFormFields('booking_site_url', 'Please enter valid url.', 'validUrl')) $errStatus = true;
            }
            if ($('#booking_site_email').val() != '') {
                if (validateFormFields('booking_site_email', 'Please enter valid email address.', 'validEmail')) $errStatus = true;
            }
        } else if ($('#travel_plan_reservation_type').val() == 2) { /* unplanned */
            if (validateFormFields('unplanned_origin_location', 'Please enter origin location.', '')) $errStatus = true;
            if (validateFormFields('unplanned_destination_location', 'Please enter destination location.', '')) $errStatus = true;
            if (validateFormFields('unplanned_departure_date', 'Please enter depature date.', '')) $errStatus = true;
            if (validateFormFields('project_start_date', 'Please select project start date.', '')) $errStatus = true;
            if (validateFormFields('project_end_date', 'Please select project end date.', '')) $errStatus = true;
            if (validateFormFields('date_flexibility', 'Please select date flexibility.', '')) $errStatus = true;
        }
    }
    return $errStatus;
}

function validateTravellerTripDetailsForm() {
    clrErr();
    $errStatus = false;
    var action = $('#action').val();
    if ($('[name="travel_plan_type"]:checked').val() == 'existing_plan' && action == 'add') {
        if (validateFormFields('travel_plan', 'Please select a travel plan.', '')) $errStatus = true;
    } else {
        if (action == 'add')
            if (validateFormFields('trip_type', 'Please choose trip type.', '')) $errStatus = true;
        if (validateFormFields('origin_location', 'Please enter orgin location.', '')) $errStatus = true;
        if (validateFormFields('destination_location', 'Please enter destination location.', '')) $errStatus = true;
        if (validateFormFields('departure_date', 'Please enter depature date.', '')) $errStatus = true; /*if (validateFormFields('arrival_date', 'Please enter arrival date.', '')) $errStatus = true;*/
        if ($('#tripOtherDetails').css('display') != 'none') {
            if ($('[name="ticketOption"]:checked').val() == 'uploadTicket') {
                if (action == 'add')
                    if (validateFormFields('ticket', 'Please upload ticket image.', '')) $errStatus = true;
                if ($('#ticket').val() != '') {
                    var ext = $('#ticket').val().split('.').pop().toLowerCase();
                    var allow = new Array('gif', 'png', 'jpg', 'jpeg');
                    if ($.inArray(ext, allow) == -1) {
                        setErr('ticket', 'Please select valid image!');
                        $errStaus = true;
                    }
                }
            }
        }
        if ($('#travel_agency_url').val() != '') {
            if (validateFormFields('travel_agency_url', 'Please enter valid url.', 'validUrl')) $errStatus = true;
        }
        if ($('#agency_email').val() != '') {
            if (validateFormFields('agency_email', 'Please enter valid email address.', 'validEmail')) $errStatus = true;
        } /*if($('#supplier_url').val() != ''){            if (validateFormFields('supplier_url', 'Please enter valid url.', 'validUrl')) $errStatus = true;        }         if($('#supplier_email').val() != ''){            if (validateFormFields('supplier_email', 'Please enter valid email address.', 'validEmail')) $errStatus = true;        }*/
        if ($('#booking_site_url').val() != '') {
            if (validateFormFields('booking_site_url', 'Please enter valid url.', 'validUrl')) $errStatus = true;
        }
        if ($('#booking_site_email').val() != '') {
            if (validateFormFields('booking_site_email', 'Please enter valid email address.', 'validEmail')) $errStatus = true;
        }
    }
    console.log('$errStatus' + $errStatus);
    if ($errStatus === false) return true;
    return false;
}

function validateOriginLocationForm() {
    console.log('function called');
    clrErr();
    $errStaus = false;
    if ($('[name="contact_address_type"]:checked').val() == 'existing_address') {
        if (validateFormFields('existing_user_location', 'Please select existing address.', '')) $errStaus = true;
    } else {
        if (validateFormFields('country_short', 'Please select country.', '')) $errStaus = true;
        if (validateFormFields('street_address_1', 'Please enter address line 1.', '')) $errStaus = true; /*  if (validateFormFields('street_address_2', 'Please enter address line 2.', '')) $errStaus = true;*/
        if (validateFormFields('city', 'Please enter city.', '')) $errStaus = true;
        if (validateFormFields('state', 'Please enter state.', '')) $errStaus = true;
        if (validateFormFields('postal_code', 'Please enter zipcode.', '')) $errStaus = true; /*if (validateFormFields('postal_code', 'Please enter zipcode.', 'validEmail')) $errStaus = true;*/
    }
    if ($errStaus) {
        $('#rootwizard a[data-tab-name="origin_location_address"]').trigger('click');
        return false;
    }
    return true;
}

function validateFlightSatesForm() {
    clrErr();
    $errStatus = false;
    if (validateFormFields('origin_location', 'Please enter orgin location.', '')) $errStatus = true;
    if (validateFormFields('destination_location', 'Please enter destination location.', '')) $errStatus = true;
    if (validateFormFields('departure_date', 'Please enter depature date.', '')) $errStatus = true;
    return $errStatus;
}

function validateFlightManualAddForm() {
    clrErr();
    $errStatus = false;
    if (validateFormFields('nstop_manual', 'Please select number of stops.', 'select')) $errStatus = true;
    if ($('.manual_airline_name').length != 0) {
        $('.manual_airline_name').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please enter airline name.', '')) $errStatus = true;
        });
    }
    if ($('.manual_airline_carrier').length != 0) {
        $('.manual_airline_carrier').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please enter airline carrier.', '')) $errStatus = true;
        });
    }
    if ($('.manual_airline_number').length != 0) {
        $('.manual_airline_number').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please enter airline number.', '')) $errStatus = true;
        });
    }
    if ($('.manual_departure_date').length != 0) {
        $('.manual_departure_date').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please select departure date.', '')) $errStatus = true;
        });
    }
    if ($('.manual_departure_time').length != 0) {
        $('.manual_departure_time').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please select departure time.', '')) $errStatus = true;
        });
    }
    if ($('.manual_arrival_date').length != 0) {
        $('.manual_arrival_date').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please select arrival date.', '')) $errStatus = true;
        });
    }
    if ($('.manual_arrival_time').length != 0) {
        $('.manual_arrival_time').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please select arrival time.', '')) $errStatus = true;
        });
    }
    if ($('.manual_arrival_time').length != 0) {
        $('.manual_arrival_time').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please select arrival time.', '')) $errStatus = true;
        });
    }
    if ($('.manual_origin_location').length != 0) {
        $('.manual_origin_location').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please enter origin location.', '')) $errStatus = true;
        });
    }
    if ($('.manual_destination_location').length != 0) {
        $('.manual_destination_location').each(function() {
            var item_length_id = $(this).attr('id');
            if (validateFormFields(item_length_id, 'Please enter destination location.', '')) $errStatus = true;
        });
    }
    return $errStatus;
}

function bindDatePicker(classname) {
    $('input.' + classname + ', .input-daterange, .date-pick-inline').datepicker({
        todayHighlight: true,
        startDate: '-0d',
        autoclose: true
    }).on('changeDate', function(e) {
        if ($(e.target).hasClass('service_date')) {
            var service_date_id = $(e.target).attr('id'),
                index = service_date_id.lastIndexOf("_"),
                pos = service_date_id.substr(index + 1);
            if ($(this).val() !== '') {
                updateDateRange(service_date_id, 'service_start_date_' + pos, 'service_end_date_' + pos, 'date_flexibility_' + pos);
            }
        }
    });
    $('.input-daterange input[name="start"]').datepicker('setDate', new Date());
    $('.input-daterange input[name="end"]').datepicker('setDate', '+7d');
}
bindDatePicker('date-pick');

function validateSeekerProjectForm() {
    clrErr();
    $errStatus = false;
    if ($('#work_category').val() == '1') {
        if ($('.product_category').length != 0) {
            $('.product_category').each(function() {
                var item_length_id = $(this).attr('id');
                if (validateFormFields(item_length_id, 'Please select product category.', '')) $errStatus = true;
            });
        }
        if ($('.product_additional_requirements_category').length != 0) {
            $('.product_additional_requirements_category').each(function() {
                var item_length_id = $(this).attr('id');
                if (validateFormFields(item_length_id, 'Please select product type.', '')) $errStatus = true;
            });
        }
        if ($('.product_description').length != 0) {
            $('.product_description').each(function() {
                var item_length_id = $(this).attr('id');
                if (validateFormFields(item_length_id, 'Please enter product description.', '')) $errStatus = true;
            });
        }
        if ($('.product_web_site_link').length != 0) {
            $('.product_web_site_link').each(function() {
                var item_length_id = $(this).attr('id');
                if ($(this).val() != '') {
                    if (validateFormFields(item_length_id, 'Please enter valid website link.', 'validUrl')) $errStatus = true;
                }
            });
        }
    } else {
        if ($('.task_category').length != 0) {
            $('.task_category').each(function() {
                var item_length_id = $(this).attr('id');
                if (validateFormFields(item_length_id, 'Please select category.', '')) $errStatus = true;
            });
        }
        if ($('.additional_requirements_category').length != 0) {
            $('.additional_requirements_category').each(function() {
                var item_length_id = $(this).attr('id');
                if (validateFormFields(item_length_id, 'Please select task list.', '')) $errStatus = true;
            });
        }
        if ($('.task_description').length != 0) {
            $('.task_description').each(function() {
                var item_length_id = $(this).attr('id');
                if (validateFormFields(item_length_id, 'Please enter task description.', '')) $errStatus = true;
            });
        }
        if ($('#email_address').val() != '') {
            if (validateFormFields('email_address', 'Please enter email address.', 'validEmail')) $errStatus = true;
        }
        if (validateFormFields('comments', 'Please enter comments.', '')) $errStatus = true;
    }
    console.log($errStatus);
    return $errStatus;
}

function composeEmail() {
    clrErr();
    $errStatus = false;
    if (validateFormFields('subject', 'Please enter the subject.', '')) $errStatus = true;
    if (validateFormFields('message', 'Please enter your message.', '')) $errStatus = true;
    if ($errStatus === false) {
        $.ajax({
            type: "POST",
            url: baseurl + '/traveller/composeEmail',
            data: $('#form_compose_mail').serialize(),
            success: function(msg) {
                $('#message').val('');
                $('#new-compose-dialog').modal('toggle');
                $.unblockUI();
                loadblockUI(msg);
                setTimeout('$.unblockUI();', 3000);
            }
        });
    }
} /*$('#date_flexibility').change(function(){    updateDateRange();});*/
$('#unplanned_departure_date').change(function() {
    updateDateRange();
});

function updateDateRange(dateElId, startEl, endEl, currElId) {
    dateElId = dateElId || 'unplanned_departure_date';
    startEl = startEl || 'project_start_date';
    endEl = endEl || 'project_end_date';
    currElId = currElId || 'date_flexibility';
    if ($('#' + currElId).val() != undefined && $('#' + dateElId).val() != undefined) {
        var futureDate = addMinusDays($('#' + dateElId).val(), $('#' + currElId).val(), '+'),
            prevDate = addMinusDays($('#' + dateElId).val(), $('#' + currElId).val(), '-'),
            futureDateFormatted = ('0' + futureDate.getDate()).slice(-2) + '-' + (futureDate.getMonth() + 1) + '-' + futureDate.getFullYear(),
            prevDateFormatted = ('0' + prevDate.getDate()).slice(-2) + '-' + (prevDate.getMonth() + 1) + '-' + prevDate.getFullYear();
        if ($('#' + dateElId).val() != '') {
            $('#' + startEl).val(prevDateFormatted);
            $('#' + endEl).val(futureDateFormatted);
        }
    }
}

function addMinusDays(date, days, operator) { /* days = '1-d'|1-m|1-w */
    var thisDate = new Date(),
        daysArr = [];
    daysArr = days.split('-');
    var result = new Date(date),
        addedDate = 0,
        finalDate = new Date();
    if (operator == '+') {
        if (daysArr[1] == 'd') {
            addedDate = parseInt(result.getDate()) + parseInt(daysArr[0]);
            result.setDate(addedDate);
        } else if (daysArr[1] == 'w') {
            var daysToAdd = daysArr[0] * 7;
            addedDate = parseInt(result.getDate()) + parseInt(daysToAdd);
            result.setDate(addedDate);
        } else if (daysArr[1] == 'm') {
            result.setMonth((result.getMonth() + 1) + daysArr[0]);
        }
        finalDate = result;
    } else if (operator == '-') {
        if (daysArr[1] == 'd') {
            addedDate = parseInt(result.getDate()) - parseInt(daysArr[0]);
            result.setDate(addedDate);
        } else if (daysArr[1] == 'w') {
            var daysToAdd = daysArr[0] * 7;
            addedDate = parseInt(result.getDate()) - parseInt(daysToAdd);
            result.setDate(addedDate);
        } else if (daysArr[1] == 'm') {
            result.setMonth((result.getMonth() + 1) - daysArr[0]);
        }
        var currentDate = new Date();
        if (result < currentDate) {
            finalDate = currentDate;
        } else {
            finalDate = result;
        }
    }
    return finalDate;
}

function addMinusDaysOld(date, days, operator) {
    var thisDate = new Date();
    var result = new Date(date),
        addedDate = 0,
        finalDate = new Date();
    if (operator == '+') {
        addedDate = parseInt(result.getDate()) + parseInt(days);
        result.setDate(addedDate);
        finalDate = result;
    } else if (operator == '-') {
        addedDate = parseInt(result.getDate()) - parseInt(days);
        result.setDate(addedDate);
        var currentDate = new Date();
        if (result < currentDate) {
            finalDate = currentDate;
        } else {
            finalDate = result;
        }
    }
    return finalDate;
}

function checkFightStatus(flightInfo) {
    loadblockUI('Loading...');
    $.ajax({
        url: baseurl + '/seeker/checkflightstatus',
        type: 'POST',
        data: 'flightInfo=' + flightInfo,
        cache: false,
        success: function(response) {
            $.unblockUI();
            $('#flight_status_loading').hide();
            $('#flight_status_content').html(response);
            $.magnificPopup.open({
                items: {
                    src: '#check-flight-status'
                },
                type: 'inline'
            }, 0);
        }
    });
}

function getPlanDetails(id) {
    if (id == '') {
        $('#plandetails').html('');
        return false;
    }
    $.ajax({
        url: baseurl + '/seeker/getplandetails',
        type: 'POST',
        data: 'id=' + id,
        cache: false,
        success: function(response) {
            var datamsg = response.split('@@SPLIT@@');
            $('#origin_location_country_code').val(datamsg[4]);
            $('#destination_location_country_code').val(datamsg[3]);
            $('#distance').val(datamsg[1]);
            $('#noofstops').val(datamsg[2]);
            $('#plandetails').html(datamsg[5]);
            $('#accordion_ajax').accordion();
            getServiceFee();
        }
    });
}
$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();
});
$(document).ready(function() {
    scrollSide();
    $("#sidebar-wrapper").scroll(function() {
        udateScrollBar('sidebar-wrapper');
    });
});

function udateScrollBar(elementId) {
    $("#" + elementId).getNiceScroll().resize();
    $('#' + elementId).niceScroll({
        cursorcolor: "#4493CB",
        cursoropacitymin: 0.3,
        background: "#fff",
        cursorborder: "0",
        autohidemode: false,
        cursorminheight: 30,
        horizrailenabled: true
    }).show().updateScrollBar();
}

function scrollSide2() {
    niceScroll = $('#sidebar-wrapper').niceScroll({
        cursorcolor: "#4493CB",
        cursoropacitymin: 0.3,
        background: "#fff",
        cursorborder: "0",
        autohidemode: false,
        cursorminheight: 30,
        horizrailenabled: true
    });
    niceScroll = $('#sidebar-wrapper').getNiceScroll().resize();
}

function scrollSide() {
    $("html").niceScroll({
        cursorwidth: "3px",
        cursorborder: "3px solid #000",
        cursorborderradius: "0px",
        cursoropacitymin: 0.2,
        cursorminheight: 110,
        cursorfixedheight: false
    });
}

function validateBTProfileForm(validate) {
    clrErr();
    $errStatus = false;
    if (validateFormFields('email', 'Please enter email address.', 'validEmail')) $errStatus = true;
    if (validate == 1) {
        if (validateFormFields('password', 'Please enter password.', '')) $errStatus = true;
    }
    if (validateFormFields('firstname', 'Please enter firstname.', '')) $errStatus = true;
    if (validateFormFields('phone', 'Please enter the phone number.', '')) $errStatus = true;
    if (validateFormFields('brithday', 'Please enter date of birth.', '')) $errStatus = true;
    if ($errStatus) {
        return false;
    } else {
        loadblockUI('Loading...');
        $.ajax({
            type: "POST",
            url: baseurl + '/signupBT',
            data: $('#proffsave').serialize(),
            success: function(msg) {
                var datamsg = msg.split("@SPLIT@");
                if (datamsg[1] != 'EmailExist') {
                    $("#loggedin_img_name").html(datamsg[1]);
                    $("#topLinkLogin").hide();
                    $("#topLinkSingup").hide();
                    $("#loggedin_container").show();
                    window.location = baseurl + '/becometraveler/identify';
                } else {
                    $.unblockUI();
                    loadblockUI(datamsg[0]);
                    setTimeout('$.unblockUI();', 3000);
                }
            }
        });
    }
}

function validateBTTravelForm(action) {
    if (validateTravellerTripDetailsForm()) {
        if (selectedFlightData !== false) {
            $('#selectedFlightData').val(JSON.stringify(selectedFlightData));
        }
        jQuery.ajax({
            type: "POST",
            url: baseurl + '/traveller/addBecameTravellerService',
            data: 'action=' + action + '&' + $('#form_travel_details').serialize() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#form_addtrippeople_people').serialize(),
            success: function(msg) {
                var msgdata = jQuery.parseJSON(msg);
                if (msgdata.result == 'exist') {
                    $.unblockUI();
                    confirmDialog(msgdata.msg, 'Warning', function() {
                        window.location = baseurl + msgdata.serviceUrl;
                    });
                } else {
                    if ($('[name="ticketOption"]:checked').val() == 'uploadTicket') {
                        if (msgdata.result == 'success') {
                            travelFormData = new FormData();
                            travelFormData.append('file', $('#ticket')[0].files[0]);
                            travelFormData.append('tripId', msgdata.trip_ID);
                            travelFormData.append('tripIdNumber', $('#tripIdNumber').val());
                            $.ajax({
                                type: 'POST',
                                url: baseurl + '/traveller/uploadticket',
                                data: travelFormData,
                                cache: false,
                                contentType: false,
                                processData: false,
                                success: function(mydata) {
                                    var resDataObj = jQuery.parseJSON(mydata);
                                    if (resDataObj.result == 'success') {
                                        loadblockUI(msgdata.msg);
                                        if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                        else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-people-service/' + msgdata.trip_ID + '/return"', 3000);
                                        else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=people"', 3000);
                                    }
                                }
                            });
                        }
                    } else {
                        loadblockUI(msgdata.msg);
                        if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                        else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-people-service/' + msgdata.trip_ID + '/return"', 3000);
                        else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=people"', 3000);
                    }
                }
            }
        });
    }
}

function initBecameTravellerTrip() {
    var service_type = $('#service_type').val();
    $('#rootwizard').bootstrapWizard({
        'tabClass': 'nav nav-tabs-new',
        'debug': false,
        onShow: function(tab, navigation, index) { /*console.log('onShow');*/ },
        onNext: function(tab, navigation, index) {
            console.log('index index' + index);
            service_type = $('#service_type').val();
            if (index == 1) {
                return validateBtServiceForm();
            }
            if (index == 2) {
                if (service_type == 'people') return validateTravellerPepleForm();
                if (service_type == 'package') return validateTravellerPackageForm();
                if (service_type == 'project') return validateTravellerProjectForm();
            }
        },
        onPrevious: function(tab, navigation, index) { /*console.log('onPrevious');*/ },
        onLast: function(tab, navigation, index) { /*console.log('onLast');*/ },
        onTabClick: function(tab, navigation, index, newindex) {
            service_type = $('#service_type').val();
            if (newindex == 1) {
                return validateBtServiceForm();
            }
            if (newindex == 2) {
                if (service_type == 'people') return validateTravellerPepleForm();
                if (service_type == 'package') return validateTravellerPackageForm();
                if (service_type == 'project') return validateTravellerProjectForm();
            } /*console.log('onTabClick');*/
        },
        onTabShow: function(tab, navigation, index) {
            if (index == 2) {
                initMap();
            }
            if (index == 1) {
                getServiceFee();
            }
            console.log('index tab show:' + index);
            var $total = navigation.find('li').length;
            var $current = index + 1;
            var $percent = ($current / $total) * 100;
            $('#rootwizard .progress-bar').css({
                width: $percent + '%'
            }); /* If it's the last tab then hide the last button and show the finish instead*/
            if ($current >= $total) {
                $('#rootwizard').find('.pager .next').hide();
                $('#rootwizard').find('.pager .finish').show();
                $('#rootwizard').find('.pager .finish').removeClass('disabled');
            } else {
                $('#rootwizard').find('.pager .next').show();
                $('#rootwizard').find('.pager .finish').hide();
            }
        }
    });
    $('#rootwizard .finish').click(function() {
        service_type = $('#service_type').val();
        if (validateBtServiceForm() == false) {
            $('#rootwizard a[href="#tab2"]').trigger('click');
            return false;
        }
        if (service_type == 'people')
            if (validateTravellerPepleForm() == false) {
                $('#rootwizard a[href="#tab1"]').trigger('click');
                return false;
            }
        if (service_type == 'package')
            if (validateTravellerPackageForm() == false) {
                $('#rootwizard a[href="#tab1"]').trigger('click');
                return false;
            }
        if (service_type == 'project')
            if (validateTravellerProjectForm() == false) {
                $('#rootwizard a[href="#tab1"]').trigger('click');
                return false;
            }
        if (validatePackageContactForm() == false) {
            $('#rootwizard a[href="#tab2"]').trigger('click');
            return false;
        }
        if (selectedFlightData !== false) {
            $('#selectedFlightData').val(JSON.stringify(selectedFlightData));
        }
        var address_type = $('input[name=contact_address_type]:checked', '#form_addtrippeople_origindetail').val();
        var action = $('#action').val();
        if (address_type == 'new_address') {
            if ($("#street_address_1").val() != '') {
                loadblockUI('Loading...');
                var street_address_1 = $("#street_address_1").val();
                var city = $("#city").val();
                var country = $("#country_short option:selected").text();
                var state = $("#state").val();
                var zip_code = $("#postal_code").val();
                var param = street_address_1 + " " + city + " " + state + " " + zip_code + "," + country;
                jQuery.ajax({
                    type: "POST",
                    url: "https://maps.google.com/maps/api/geocode/json?address=" + param + "&sensor=false&region=India",
                    data: param,
                    async: false,
                    success: function(msg) {
                        $("#latitude").val(msg.results[0].geometry.location.lat);
                        $("#longitude").val(msg.results[0].geometry.location.lng);
                        jQuery.ajax({
                            type: "POST",
                            url: baseurl + '/traveller/addBecameTravellerService',
                            data: 'action=service&tripId=' + $('#tripId').val() + '&service_type=' + $('#service_type').val() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#form_addtrippeople_people').serialize() + '&' + $('#form_addtrippeople_project').serialize() + '&' + $('#form_addtrippeople_package').serialize(),
                            success: function(msg) {
                                var msgdata = jQuery.parseJSON(msg);
                                if (msgdata.result == 'exist') {
                                    $.unblockUI();
                                    confirmDialog(msgdata.msg, 'Warning', function() {
                                        window.location = baseurl + msgdata.serviceUrl;
                                    });
                                } else {
                                    loadblockUI(msgdata.msg);
                                    if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                                    else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-people-service/' + msgdata.trip_ID + '/return"', 3000);
                                    else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=people"', 3000);
                                }
                            }
                        });
                    }
                });
            }
        } else {
            loadblockUI('Loading...');
            jQuery.ajax({
                type: "POST",
                url: baseurl + '/traveller/addBecameTravellerService',
                data: 'action=service&tripId=' + $('#tripId').val() + '&service_type=' + $('#service_type').val() + '&' + $('#form_origin_location_address').serialize() + '&' + $('#form_addtrippeople_people').serialize() + '&' + $('#form_addtrippeople_project').serialize() + '&' + $('#form_addtrippeople_package').serialize(),
                success: function(msg) {
                    var msgdata = jQuery.parseJSON(msg);
                    if (msgdata.result == 'exist') {
                        $.unblockUI();
                        confirmDialog(msgdata.msg, 'Warning', function() {
                            window.location = baseurl + msgdata.serviceUrl;
                        });
                    } else {
                        loadblockUI(msgdata.msg);
                        if (msgdata.requestURL) setTimeout('$.unblockUI();window.location="' + baseurl + '/' + msgdata.requestURL + '"', 3000);
                        else if (msgdata.trip_ID) setTimeout('$.unblockUI();window.location="' + baseurl + '/traveller/add-people-service/' + msgdata.trip_ID + '/return"', 3000);
                        else setTimeout('$.unblockUI();window.location="' + baseurl + '/search?trip=' + msgdata.seacrhAction + '&service=people"', 3000);
                    }
                }
            });
        }
    });
    $("#formAddTripPeople").on("submit", function(event) {
        event.preventDefault();
    });
}

function validateBtServiceForm() {
    clrErr();
    $errStatus = false;
    if (validateFormFields('service_type', 'Please select service.', '')) $errStatus = true;
    if ($errStatus) {
        return false;
    } else {
        return true;
    }
}

$('.btn-group-select-num.female-count').on('click','.female_count_select',function(){
		setTimeout(function() {
			var trip_people_count =  parseInt( $('[name="trip_people_count"]:checked').val());
			var this_count=parseInt( $('[name="trip_people_female_count"]:checked').val());
			var diff=trip_people_count-this_count;
			$("input[name='trip_people_male_count']").parent('label').removeAttr('disabled');
			var a;
			for(a= diff+1; a<= 11; a++){
				console.log(a);
				$("input[name='trip_people_male_count'][value=" + a + "]").parent('label').attr("disabled", "disabled");                
			}
		 }.bind(this), 10);
			
	});
	$('.btn-group-select-num').on('click','.male_count_select',function(){
		setTimeout(function() {
			var trip_people_count =  parseInt( $('[name="trip_people_count"]:checked').val());
			var this_count=parseInt( $('[name="trip_people_male_count"]:checked').val());
			var diff=trip_people_count-this_count;
			$("input[name='trip_people_female_count']").parent('label').removeAttr('disabled');
			var a;
			
			for(a= diff+1; a<= 11; a++){
				
				$("input[name='trip_people_female_count'][value=" + a + "]").parent('label').attr("disabled", "disabled");               
			}
        }.bind(this), 10);
		
		
		

		
	});