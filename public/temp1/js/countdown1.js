/**
 * jQuery Countdown
 *
 * Provides a javascript countdown timer based on the data-countdown attribute or by
 * passing a schedule of events via options. It will default to data-countdown if the
 * attribute is present, then the next day in a schedule if you pass one, and
 * then it will fail silently as last result if neither of these are present.
 *
 * @author Kaleb Heitzman <kaleblex@gmail.com>
 * @website http://github.com/kaleblex/jquery-countdown.js
 * @created March 12, 2013
 *
 */
;$(document).ready(function () {var validator = $("#formlogin").validate({});});