<?php

/**
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */
// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

return
		array(
			"base_url" => "http://trepr.co.uk/myprofile/callbackAuth",
                        /*"base_url" => "http://local/trepr/site_beta/public/myprofile/callbackAuth",*/
			"providers" => array(
				// openid providers
				"OpenID" => array(
					"enabled" => true
				),
				"Yahoo" => array(
					"enabled" => false,
					"keys" => array("key" => "", "secret" => ""),
				),
				"AOL" => array(
					"enabled" => false
				),
				"Google" => array(
					"enabled" => true,
					"keys" => array("id" => "626346697140-c3vcf69bh7k4eceltd1rjutfd5s15al0.apps.googleusercontent.com", "secret" => "HKtPho7ydi_S04MQcFZmnBgz"),
					"scope"           => "https://www.googleapis.com/auth/userinfo.profile ". // optional
                               "https://www.googleapis.com/auth/userinfo.email"   , // optional
				),
				"Facebook" => array(
					"enabled" => true,
					"keys" => array("id" => "844394145662070", "secret" => "c2791bcab0338117c95140d3f8b3f15b"),
					"trustForwarded" => true
				),
				// "Facebook" => array(
					// "enabled" => true,
					// "keys" => array("id" => "344693749282003", "secret" => "64b00cffef8b647bb6e134ccca7aa6f7"),
					// "trustForwarded" => true
				// ), 
				"Twitter" => array(
					"enabled" => true,
					"keys" => array("key" => "oUW0PDblxssz7Hse3Dk4Xa0kR", "secret" => "VvRfWe2frsvG79Ic3RteVLQ97XzGcUCgTxMmDPqqU9o3wcpV53"),
					"includeEmail" => false
				),
				// windows live
				"Live" => array(
					"enabled" => false,
					"keys" => array("id" => "", "secret" => "")
				),
				"LinkedIn" => array(
					"enabled" => true,
					"keys" => array("key" => "78l0zizo9aqsd6", "secret" => "cRHrbTPShEHrEhm4")
				),
				"Foursquare" => array(
					"enabled" => false,
					"keys" => array("id" => "", "secret" => "")
				),
			),
			// If you want to enable logging, set 'debug_mode' to true.
			// You can also set it to
			// - "error" To log only error messages. Useful in production
			// - "info" To log info and error messages (ignore debug messages)
			"debug_mode" => true,
			// Path to file writable by the web server. Required if 'debug_mode' is not false
			"debug_file" => "debug.php",
);
