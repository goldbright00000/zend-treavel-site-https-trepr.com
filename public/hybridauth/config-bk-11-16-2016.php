<?php

/**
 * HybridAuth
 * http://hybridauth.sourceforge.net | http://github.com/hybridauth/hybridauth
 * (c) 2009-2015, HybridAuth authors | http://hybridauth.sourceforge.net/licenses.html
 */
// ----------------------------------------------------------------------------------------
//	HybridAuth Config file: http://hybridauth.sourceforge.net/userguide/Configuration.html
// ----------------------------------------------------------------------------------------

return
		array(
			"base_url" => "http://trepr.co.uk/demo/myprofile/callbackAuth",
                        /*"base_url" => "http://local/trepr/site_beta/public/myprofile/callbackAuth",*/
			"providers" => array(
				// openid providers
				"OpenID" => array(
					"enabled" => true
				),
				"Yahoo" => array(
					"enabled" => false,
					"keys" => array("key" => "", "secret" => ""),
				),
				"AOL" => array(
					"enabled" => false
				),
				"Google" => array(
					"enabled" => true,
					"keys" => array("id" => "626346697140-c3vcf69bh7k4eceltd1rjutfd5s15al0.apps.googleusercontent.com", "secret" => "HKtPho7ydi_S04MQcFZmnBgz"),
				),
				"Facebook" => array(
					"enabled" => true,
					"keys" => array("id" => "1729839597282171", "secret" => "11ce794c85a6907dcbdc9eb1c2da38e1"),
					"trustForwarded" => true
				),
				"Twitter" => array(
					"enabled" => true,
					"keys" => array("key" => "oUW0PDblxssz7Hse3Dk4Xa0kR", "secret" => "VvRfWe2frsvG79Ic3RteVLQ97XzGcUCgTxMmDPqqU9o3wcpV53"),
					"includeEmail" => false
				),
				// windows live
				"Live" => array(
					"enabled" => false,
					"keys" => array("id" => "", "secret" => "")
				),
				"LinkedIn" => array(
					"enabled" => true,
					"keys" => array("key" => "78l0zizo9aqsd6", "secret" => "cRHrbTPShEHrEhm4")
				),
				"Foursquare" => array(
					"enabled" => false,
					"keys" => array("id" => "", "secret" => "")
				),
			),
			// If you want to enable logging, set 'debug_mode' to true.
			// You can also set it to
			// - "error" To log only error messages. Useful in production
			// - "info" To log info and error messages (ignore debug messages)
			"debug_mode" => true,
			// Path to file writable by the web server. Required if 'debug_mode' is not false
			"debug_file" => "debug.php",
);
