function currencycountry(cur_country, type) {
    jQuery.ajax({
        type: "POST",
        url: siteURL + 'getcountry',
        data: "cur_country=" + cur_country + "&type=" + type,
        success: function (msg) {
            var obj = jQuery.parseJSON(msg);
        }
    });
}
$(window).load(function(){
	$(".typeahead").autocomplete({
		source: siteURL + "search-airport",
		minLength: 1,
		select: function (event, ui) {
			var element_id = $(this).attr('id');
			if ($('#' + element_id + '_id').length == 0) {
				$('<input type="hidden" name="' + element_id + '_id" id="' + element_id + '_id" value="' + ui.item.ID + '" >').insertAfter('#' + element_id);
				$('<input type="hidden" name="' + element_id + '_code" id="' + element_id + '_code" value="' + ui.item.code + '" >').insertAfter('#' + element_id);
				$('<input type="hidden" name="' + element_id + '_iata_code" id="' + element_id + '_iata_code" value="' + ui.item.code + '" >').insertAfter('#' + element_id);
			} else {
				$('#' + element_id + '_id').val(ui.item.ID);
				$('#' + element_id + '_code').val(ui.item.code);
				$('#' + element_id + '_iata_code').val(ui.item.code);
			}
			if ($('#' + element_id + '_country_code').length == 0) {
				$('<input type="hidden" name="' + element_id + '_country_code" id="' + element_id + '_country_code" value="' + ui.item.country_code + '" >').insertAfter('#' + element_id);
				$('<input type="hidden" name="' + element_id + '_country" id="' + element_id + '_country" value="' + ui.item.country + '" >').insertAfter('#' + element_id);
				$('<input type="hidden" name="' + element_id + '_iata_code" id="' + element_id + '_iata_code" value="' + ui.item.code + '" >').insertAfter('#' + element_id);
			} else {
				$('#' + element_id + '_country_code').val(ui.item.country_code);
				$('#' + element_id + '_country').val(ui.item.country);
				$('#' + element_id + '_iata_code').val(ui.item.code);
			}
		}
	});
	$('.datepicker').datepicker({
		todayHighlight: true,
        startDate: '-0d',
        autoclose: true
	});
});