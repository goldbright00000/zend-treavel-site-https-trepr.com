



ALTER TABLE `notifications` CHANGE `notification_type` `notification_type` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

FTP & cPanel
166.62.10.182
22
trepr108429201
Content18#


https://trepr.co.uk/admin
admin@trepr.com
admin123


https://trepr.co.uk
ranmagasasi@gmail.com
123456

ranmagasasi@gmail.com
kankanamitra01@gmail.com
lithipora@gmail.com
monikshingh01@gmail.com
123456

treprltd@gmail.com
Pay#pal15


ALTER TABLE `notifications` ADD `admin_redirect` TEXT NULL DEFAULT NULL AFTER `app_redirect`;

ALTER TABLE `trips` ADD `travel_plan_reservation_type` TINYINT(1) NULL DEFAULT NULL AFTER `user_location`;


ALTER TABLE `seeker_people` ADD `male_count` VARCHAR( 10 ) NOT NULL DEFAULT '0' AFTER `one_of_the_passenger` ,
ADD `female_count` VARCHAR( 10 ) NOT NULL DEFAULT '0' AFTER `male_count` ,
ADD `above_60` VARCHAR( 10 ) NOT NULL DEFAULT '0' AFTER `female_count` ,
ADD `below_6` VARCHAR( 10 ) NOT NULL DEFAULT '0' AFTER `above_60` ;

ALTER TABLE `seeker_trips` ADD `traveller_trip_id` INT(11) NOT NULL AFTER `trip_id_number`;
ALTER TABLE `users_reviews` ADD `behavior` VARCHAR(30) NOT NULL AFTER `rating`, ADD `promptness` VARCHAR(30) NOT NULL AFTER `behavior`, ADD `cooperation` VARCHAR(30) NOT NULL AFTER `promptness`, ADD `friendliness` VARCHAR(30) NOT NULL AFTER `cooperation`, ADD `support` VARCHAR(30) NOT NULL AFTER `friendliness`, ADD `communication` VARCHAR(30) NOT NULL AFTER `support`, ADD `flexibility` VARCHAR(30) NOT NULL AFTER `communication`, ADD `status` INT(2) NOT NULL COMMENT '6 - completed, 7 - cancelled' AFTER `flexibility`;
ALTER TABLE `users_reviews` ADD `seeker_trip_id` INT(30) NOT NULL AFTER `trip_id`;

ALTER TABLE `trips` ADD `is_expired` INT(1) NOT NULL AFTER `trip_status`;
ALTER TABLE `seeker_trips` ADD `is_expired` INT(1) NOT NULL AFTER `trip_status`;

CREATE TABLE `seeker_request_wishlist` ( `wishlist_id` INT(11) NOT NULL AUTO_INCREMENT , `trip_id` INT(11) NOT NULL , `seeker_trip_id` INT(11) NOT NULL , `request_id` INT(11) NOT NULL , `request_type` VARCHAR(255) NOT NULL , `is_wishlist` INT(2) NOT NULL , `created_on` DATETIME NOT NULL , `modified_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`wishlist_id`)) ENGINE = InnoDB;
CREATE TABLE `traveller_request_wishlist` ( `wishlist_id` INT(11) NOT NULL AUTO_INCREMENT , `trip_id` INT(11) NOT NULL , `seeker_trip_id` INT(11) NOT NULL , `request_id` INT(11) NOT NULL , `request_type` VARCHAR(255) NOT NULL , `is_wishlist` INT(2) NOT NULL , `created_on` DATETIME NOT NULL , `modified_on` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP , PRIMARY KEY (`wishlist_id`)) ENGINE = InnoDB;

ALTER TABLE `message_list` ADD `seeker_trip_id` INT(11) NOT NULL AFTER `message_tripid`;
ALTER TABLE `message_list` ADD `traveller_trip_id` INT(11) NOT NULL AFTER `message_tripid`;

ALTER TABLE `travaller_inquires` ADD `traveller_trip_id` INT(11) NOT NULL AFTER `sender_id`, ADD `seeker_trip_id` INT(11) NOT NULL AFTER `traveller_trip_id`;
ALTER TABLE `seeker_inquires` ADD `traveller_trip_id` INT(11) NOT NULL AFTER `sender_id`, ADD `seeker_trip_id` INT(11) NOT NULL AFTER `traveller_trip_id`;

ALTER TABLE `seeker_trips` ADD `conversion_rate` DOUBLE( 10, 2 ) NOT NULL AFTER `total_cost`;
ALTER TABLE `seeker_trips` CHANGE `total_cost` `total_cost` DOUBLE(10, 2) NOT NULL DEFAULT '0';
ALTER TABLE `seeker_trips` CHANGE `total_cost` `total_cost` VARCHAR(14) NOT NULL DEFAULT '0.00';

ALTER TABLE `trips` ADD `conversion_rate` DOUBLE( 10, 2 ) NOT NULL AFTER `total_cost`;

ALTER TABLE `user_payments` ADD `updated_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `pay_added`;

ALTER TABLE `seeker_trips` ADD `traveller_trip_id` INT(11) NOT NULL AFTER `trip_id_number`;

ALTER TABLE `trips` CHANGE `total_cost` `total_cost` VARCHAR(14) NOT NULL DEFAULT '0.00';

ALTER TABLE `users_user` CHANGE `dob` `dob` DATE NOT NULL;

ALTER TABLE `trips` ADD `is_cmp_notification_sent` INT(1) NOT NULL DEFAULT '0' AFTER `is_expired`;
ALTER TABLE `seeker_trips` ADD `is_cmp_notification_sent` INT(1) NOT NULL DEFAULT '0' AFTER `is_expired`;

CREATE TABLE IF NOT EXISTS `coupons` (
`id` int(20) NOT NULL,
  `coupon_code` varchar(20) NOT NULL,
  `offer_percentage` int(10) NOT NULL,
  `status` tinyint(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

ALTER TABLE `users_user_locations` ADD `tripId` INT(50) NOT NULL AFTER `ID`;

ALTER TABLE `seeker_project_tasks` ADD `purchase_type` VARCHAR(11) NOT NULL AFTER `currency_of_payment`, ADD `store_name` TEXT NOT NULL AFTER `purchase_type`, ADD `store_address` TEXT NOT NULL AFTER `store_name`;

ALTER TABLE `traveller_projects` ADD `item_weight` VARCHAR(255) NOT NULL AFTER `item_worth_currency`;

ALTER TABLE `traveller_packages` ADD `weight_carried_unit` VARCHAR(10) NOT NULL AFTER `weight_carried`;

ALTER TABLE `traveller_packages` ADD `weight_accommodate_unit` VARCHAR(10) NOT NULL AFTER `weight_accommodate`;

ALTER TABLE `traveller_projects` ADD `item_weight_unit` VARCHAR(10) NOT NULL AFTER `item_weight`;

ALTER TABLE `traveller_projects` CHANGE `product_category` `product_category` VARCHAR(255) NOT NULL, CHANGE `product_type` `product_type` VARCHAR(255) NOT NULL DEFAULT '0';

ALTER TABLE `trips` ADD `booking_cost` VARCHAR(100) NOT NULL AFTER `reservation_is_purchased`, ADD `booking_cost_currency` VARCHAR(100) NOT NULL AFTER `booking_cost`;

ALTER TABLE `seeker_trips` ADD `booking_cost` VARCHAR(100) NOT NULL AFTER `reservation_is_purchased`, ADD `booking_cost_currency` VARCHAR(100) NOT NULL AFTER `booking_cost`;


--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `ID` smallint(5) UNSIGNED NOT NULL,
  `testimonial_Name` varchar(50) NOT NULL,
  `testimonial_Image` text NOT NULL,
  `testimonial_Description` text NOT NULL,
  `testimonial_Position` varchar(100) CHARACTER SET utf8 DEFAULT NULL,
  `active` int(11) NOT NULL DEFAULT '1',
  `deleted` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `testimonials` (`ID`, `testimonial_Name`, `testimonial_Image`, `testimonial_Description`, `testimonial_Position`, `active`, `deleted`) VALUES
(1, 'ANGELINA MUSK', 'img-1.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.', 'CEO-Cloud Edge Technologies,Nigeria', 1, 0),
(2, 'ANGELINA MUSK', 'img-2.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.', 'CEO-Cloud Edge Technologies,Nigeria', 1, 0),
(3, 'ANGELINA MUSK', 'img-3.jpg', 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris.', 'CEO-Cloud Edge Technologies,Nigeria', 1, 0);

ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`ID`);

ALTER TABLE `testimonials`
  MODIFY `ID` smallint(5) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4; 

  ALTER TABLE `users_user` CHANGE `dob` `dob` DATE NOT NULL;

ALTER TABLE `users_user_locations` ADD `active` INT(1) NOT NULL DEFAULT '1' AFTER `country_fullname`;

ALTER TABLE `user_photos` ADD `active` INT(1) NOT NULL DEFAULT '1' AFTER `created`;

ALTER TABLE `user_videos` ADD `active` INT(1) NOT NULL DEFAULT '1' AFTER `created`;

ALTER TABLE `users_user` ADD `stripe_id` TEXT NOT NULL AFTER `approved_id`;

ALTER TABLE `users_user_card` ADD `stripe_src_id` VARCHAR(300) NOT NULL AFTER `default_card`;

ALTER TABLE `core_country` ADD `payment_currency` VARCHAR(10) NOT NULL AFTER `currency_symbol`;

ALTER TABLE `core_country` CHANGE `currency_code` `original_currency_code` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `core_country` CHANGE `payment_currency` `currency_code` VARCHAR(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL;

ALTER TABLE `users_question_answer` CHANGE `qa_id` `qa_id` BIGINT(20) NOT NULL AUTO_INCREMENT;

ALTER TABLE `users_user_card` ADD `invisibile` INT(11) NOT NULL DEFAULT '0' AFTER `stripe_src_id`;


18/1/19

ALTER TABLE `traveller_projects` ADD `is_completed` INT(1) NOT NULL DEFAULT '0' AFTER `comment`;

ALTER TABLE `traveller_people` ADD `is_completed` INT(1) NOT NULL DEFAULT '0' AFTER `comment`;

ALTER TABLE `traveller_packages` ADD `is_completed` INT(1) NOT NULL DEFAULT '0' AFTER `comment`;

ALTER TABLE `trips` ADD `is_completed` INT(1) NOT NULL DEFAULT '0' AFTER `origin_longitude`;

23/2/19

ALTER TABLE `myaccount_notification` ADD `personal_phone_number` INT(1) NOT NULL AFTER `login_notif`, ADD `personal_email_address` INT(1) NOT NULL AFTER `personal_phone_number`, ADD `personal_physical_address` INT(1) NOT NULL AFTER `personal_email_address`;

