#!/bin/bash
mkdir -p /var/www/vhosts/trepr/public
sudo chmod -R 755 /var/www/vhosts/trepr/public
sudo chmod -R 777 /var/www/vhosts/trepr/public/public/uploads
sudo chown -R www-data:www-data /var/www/vhosts/trepr